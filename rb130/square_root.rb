def square_root(value)
  return nil if value.negative?
  Math.sqrt(value).round
end
