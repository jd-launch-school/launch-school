require 'minitest/autorun'
require_relative 'text'

class TextTest < MiniTest::Test
  def setup
    @file = File.open('sample_text.txt', "r")
    @file_contents = @file.read
    @text = Text.new(@file_contents)
  end

  def test_swap
    swapped = @file_contents.gsub('a', 'e')
    assert_equal swapped, @text.swap('a', 'e')
  end

  def test_word_count
    count = @file_contents.split.count
    assert_equal count, @text.word_count
  end

  def teardown
    @file.close
  end
end
