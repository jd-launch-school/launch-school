require 'minitest/autorun'

require_relative 'transaction'

class TransactionTest < MiniTest::Test
  def setup
    @transaction = Transaction.new(10)
    @amount_paid = '12.0'
  end

  def simulate_stdin(*input)
    io = StringIO.new
    io.puts(input)
    io.rewind

    $stdin = io
    yield
  ensure
    $stdin = STDIN
  end

  def test_prompt_for_payment_sufficient
    capture_io { simulate_stdin(@amount_paid) { @transaction.prompt_for_payment } }
    assert_equal @amount_paid.to_i, @transaction.amount_paid
  end

  def test_prompt_for_payment_insufficient
    result = capture_io do
      simulate_stdin('9', @amount_paid) { @transaction.prompt_for_payment }
    end

    assert_match /.*That is not.*/, result.first
  end

  def test_prompt_for_payment_negative
    result = capture_io do
      simulate_stdin('-9', @amount_paid) { @transaction.prompt_for_payment }
    end

    assert_match /.*That is not.*/, result.first
  end
end
