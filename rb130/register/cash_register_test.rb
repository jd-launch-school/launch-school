require 'minitest/autorun'

require_relative 'cash_register'
require_relative 'transaction'

class CashRegisterTest < MiniTest::Test
  def setup
    @cash_register = CashRegister.new(1000)
    @transaction = Transaction.new(10)
  end

  def test_accept_money
    @transaction.amount_paid = 10
    previous_amount = @cash_register.total_money
    @cash_register.accept_money(@transaction)
    current_amount = @cash_register.total_money

    assert_equal previous_amount + 10, current_amount 
  end

  def test_change
    @transaction.amount_paid = 12
    change = 2
    assert_equal change, @cash_register.change(@transaction)
  end

  def test_give_receipt
    @transaction.amount_paid = 12
    item_cost = 10
    assert_output("You've paid $#{item_cost}.\n") { @cash_register.give_receipt(@transaction) }
  end
end
