class Todo
  DONE_MARKER = 'X'
  UNDONE_MARKER = ' '

  attr_accessor :title, :description, :done

  def initialize(title, description='')
    @title = title
    @description = description
    @done = false
  end

  def done!
    self.done = true
  end

  def done?
    done
  end

  def undone!
    self.done = false
  end

  def to_s
    "[#{done? ? DONE_MARKER : UNDONE_MARKER}] #{title}"
  end

  def ==(otherTodo)
    title == otherTodo.title &&
      description == otherTodo.description &&
      done == otherTodo.done
  end
end

class TodoList
  attr_accessor :title

  def initialize(title)
    @title = title
    @todos = []
  end

  def <<(item)
    raise TypeError, "Can only add Todo objects" unless item.instance_of? Todo
    todos.push(item)
  end
  alias_method :add, :<<

  def size
    todos.size
  end

  def first
    todos.first
  end

  def last
    todos.last
  end

  def to_a
    todos
  end

  def done?
    todos.all?(&:done)
  end

  def item_at(index)
    raise IndexError, "No item at that index" unless todos[index]
    todos[index]
  end

  def mark_done_at(index)
    item_at(index).done!
  end

  def mark_undone_at(index)
    item_at(index).undone!
  end

  def done!
    todos.each(&:done!)
  end

  def shift
    todos.shift
  end

  def pop
    todos.pop
  end

  def remove_at(index)
    todos.delete(item_at(index))
  end

  def to_s
    string = "\n---- #{title} ----\n"
    string << todos.map(&:to_s).join("\n")
    string
  end

  def each
    todos.each { |item| yield(item) }
    self
  end

  def select
    temp_list = TodoList.new(title)
    each { |item| temp_list.add(item) if yield(item) }
    temp_list
  end

  # takes a string as argument, and returns the first `Todo` object that matches the argument. Return `nil` if no todo is found.
  def find_by_title(string)
    select { |item| item.title == string }.first
  end

  # returns new `TodoList` object containing only the done items
  def all_done
    select { |item| item.done? }
  end

  # returns new `TodoList` object containing only the not done items
  def all_not_done
    select { |item| !item.done? }
  end

  # takes a string as argument, and marks the first `Todo` object that matches the argument as done.
  def mark_done(string)
    each { |item| item.done! if item.title == string }
    # alternative: find_by_title(title) && find_by_title(title).done!
  end

  # mark every todo as done
  def mark_all_done
    each { |item| item.done! }
  end

  # mark every todo as not done
  def mark_all_undone
    each { |item| item.undone! }
  end

  private

  attr_reader :todos
end
