require 'simplecov'
SimpleCov.start

require 'minitest/autorun'

require_relative 'todolist'

class TodoListTest < MiniTest::Test

  def setup
    @todo1 = Todo.new("Buy milk")
    @todo2 = Todo.new("Clean room")
    @todo3 = Todo.new("Go to gym")
    @todos = [@todo1, @todo2, @todo3]

    @list = TodoList.new("Today's Todos")
    @list.add(@todo1)
    @list.add(@todo2)
    @list.add(@todo3)
  end

  def test_to_a
    assert_equal(@todos, @list.to_a)
  end

  def test_size
    assert_equal(3, @list.size)
  end

  def test_first
    assert_equal(@todo1, @list.first)
  end

  def test_last
    assert_equal(@todo3, @list.last)
  end

  def test_shift_return_value
    result = @list.shift
    assert_equal(@todo1, result)
  end

  def test_shift_remaining_list_items
    @list.shift
    assert_equal([@todo2, @todo3], @list.to_a)
  end

  def test_pop_return_value
    result = @list.pop
    assert_equal(@todo3, result)
  end

  def test_pop_remaining_list_items
    @list.pop
    assert_equal([@todo1, @todo2], @list.to_a)
  end

  def test_none_done
    assert_equal(false, @list.done?)
  end

  def test_mark_all_done
    @list.mark_all_done
    assert_equal(true, @list.done?)
  end

  def test_mark_all_undone
    @list.mark_all_undone
    assert_equal(false, @list.done?)
  end

  def test_TypeError_raised_for_integer
    assert_raises(TypeError) { @list.add(1) }
  end

  def test_TypeError_raised_for_float
    assert_raises(TypeError) { @list.add(1.1) }
  end

  def test_TypeError_raised_for_string
    assert_raises(TypeError) { @list.add('string') }
  end

  def test_TypeError_raised_for_array
    assert_raises(TypeError) { @list.add([]) }
  end

  def test_TypeError_raised_for_hash
    assert_raises(TypeError) { @list.add({}) }
  end

  def test_shovel_operator
    @todo4 = Todo.new('Test Todo')
    @list << @todo4
    @todos << @todo4

    assert_equal(@todos, @list.to_a)
  end

  def test_add
    @todo4 = Todo.new('Test Todo')
    @list.add @todo4
    @todos << @todo4

    assert_equal(@todos, @list.to_a)
  end

  def test_item_at
    assert_equal(@todo2, @list.item_at(1))
  end

  def test_IndexError_for_item_at
    assert_raises(IndexError) { @list.item_at(4) }
  end

  def test_mark_done_at
    @list.mark_done_at(1)
    assert_equal(false, @todo1.done?)
    assert_equal(true, @todo2.done?)
    assert_equal(false, @todo3.done?)
  end

  def test_IndexError_for_mark_done_at
    assert_raises(IndexError) { @list.mark_done_at(4) }
  end

  def test_mark_undone_at
    @list.done!
    @list.mark_undone_at(1)
    assert_equal(true, @todo1.done?)
    assert_equal(false, @todo2.done?)
    assert_equal(true, @todo3.done?)
  end

  def test_IndexError_for_mark_undone_at
    assert_raises(IndexError) { @list.mark_undone_at(4) }
  end

  def test_done
    @list.done!
    assert_equal(true, @list.done?)
  end

  def test_remove_at
    @list.remove_at(1)
    assert_equal([@todo1, @todo3], @list.to_a)
  end

  def test_IndexError_for_remove_at
    assert_raises(IndexError) { @list.remove_at(4) }
  end

  def test_to_s
    string = <<~OUTPUT.chomp

---- Today's Todos ----
[ ] Buy milk
[ ] Clean room
[ ] Go to gym
OUTPUT

    assert_equal(string, @list.to_s)
  end

  def test_to_s_one_done
    @list.mark_done_at(1)
    string = <<~OUTPUT.chomp

---- Today's Todos ----
[ ] Buy milk
[X] Clean room
[ ] Go to gym
OUTPUT

    assert_equal(string, @list.to_s)
  end

  def test_to_s_all_done
    @list.done!
    string = <<~OUTPUT.chomp

---- Today's Todos ----
[X] Buy milk
[X] Clean room
[X] Go to gym
OUTPUT

    assert_equal(string, @list.to_s)
  end

  def test_each_checking_output
    string = <<~OUTPUT
[ ] Buy milk
[ ] Clean room
[ ] Go to gym
OUTPUT
    assert_output(string) { @list.each { |todo| puts todo.to_s } }
  end

  def test_each_recommended
    result = []
    @list.each { |todo| result << todo }
    assert_equal([@todo1, @todo2, @todo3], result)
  end

  def test_each_return_value
    result = @list.each(&:done?)
    assert_equal(@list, result)
  end

  def test_select
    @todo2.done!
    list = TodoList.new(@list.title)
    list.add(@todo2)

    result = @list.select { |todo| todo.done? }

    assert_equal(list.title, @list.title)
    assert_equal(list.to_s, result.to_s)
  end

  def test_find_by_title
    assert_equal(@todo1, @list.find_by_title('Buy milk'))
  end

  def test_all_done
    @todo2.done!
    list = TodoList.new(@list.title)
    list.add(@todo2)

    result = @list.all_done

    assert_equal(list.title, @list.title)
    assert_equal(list.to_s, result.to_s)
  end

  def test_all_not_done
    @todo2.done!
    list = TodoList.new(@list.title)
    list.add(@todo1)
    list.add(@todo3)

    result = @list.all_not_done

    assert_equal(list.title, @list.title)
    assert_equal(list.to_s, result.to_s)
  end

  def test_mark_done
    list = TodoList.new(@list.title)
    list.add(@todo1)
    list.add(@todo2)
    list.add(@todo3)
    list.item_at(1).done!

    result = @list.mark_done('Buy milk')

    assert_equal(list.title, @list.title)
    assert_equal(list.to_s, result.to_s)
  end
end
