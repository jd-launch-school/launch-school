* Class Methods
https://launchschool.com/books/oo_ruby/read/classes_and_objects_part2#classmethods

#+begin_src ruby :results output
  class GoodDog
    attr_accessor :name, :height, :weight

    def initialize(n, h, w)
      @name = n
      @height = h
      @weight = w
    end

    def self.what_am_i
      "I'm a GoodDog class!"
    end

    def speak
      "#{name} says arf!"
    end

    def change_info(n, h, w)
      self.name = n
      self.height = h
      self.weight = w
    end

    def info
      "#{name} weighs #{weight} and is #{height} tall."
    end
  end

  puts GoodDog.what_am_i

  sparky = GoodDog.new("Sparky", "12 inches", "10 lbs")
  puts sparky.info

  sparky.change_info("Spartacus", "24 inches", "45 lbs")
  puts sparky.info
#+end_src

#+RESULTS:
: I'm a GoodDog class!
: Sparky weighs 10 lbs and is 12 inches tall.
: Spartacus weighs 45 lbs and is 24 inches tall.

* Class Variables
https://launchschool.com/books/oo_ruby/read/classes_and_objects_part2#classvariables

#+begin_src ruby :results output
  class GoodDog
    @@number_of_dogs = 0

    def initialize
      @@number_of_dogs += 1
    end

    def self.total_number_of_dogs
      @@number_of_dogs
    end
  end

  puts GoodDog.total_number_of_dogs

  dog1 = GoodDog.new
  dog2 = GoodDog.new

  puts GoodDog.total_number_of_dogs
#+end_src

#+RESULTS:
: 0
: 2

* Constants
https://launchschool.com/books/oo_ruby/read/classes_and_objects_part2#constants

#+begin_src ruby :results output
  class GoodDog
    DOG_YEARS = 7

    attr_accessor :name, :age

    def initialize(n, a)
      self.name = n
      self.age = a * DOG_YEARS
    end
  end

  sparky = GoodDog.new("Sparky", 4)
  puts sparky.age
#+end_src

#+RESULTS:
: 28

* The ~to_s~ Method
#+begin_src ruby :results output
  class GoodDog
    DOG_YEARS = 7

    attr_accessor :name, :age

    def initialize(n, a)
      @name = n
      @age = a * DOG_YEARS
    end

    def to_s
      "This dog's name is #{name} and it is #{age} in dog years."
    end
  end

  sparky = GoodDog.new("Sparky", 4)
  puts sparky
  p sparky
  puts "Can you include #{sparky} here? We'll see."
#+end_src

#+RESULTS:
: This dog's name is Sparky and it is 28 in dog years.
: #<GoodDog:0x00005626832cf768 @name="Sparky", @age=28>
: Can you include This dog's name is Sparky and it is 28 in dog years. here? We'll see.

* More About ~self~
#+begin_src ruby :results output
  class GoodDog
    attr_accessor :name, :height, :weight

    def initialize(n, h, w)
      self.name   = n
      self.height = h
      self.weight = w
    end

    def change_info(n, h, w)
      self.name   = n
      self.height = h
      self.weight = w
    end

    def info
      "#{self.name} weighs #{self.weight} and is #{self.height} tall"
    end

    def what_is_self
      self
    end
  end

  sparky = GoodDog.new('Sparky', '12 inches', '10 lbs')
  p sparky.what_is_self
#+end_src

#+RESULTS:
: #<GoodDog:0x0000559d8734e070 @name="Sparky", @height="12 inches", @weight="10 lbs">

#+begin_src ruby :results output
  class GoodDog
    @@number_of_dogs = 0

    def initialize
      @@number_of_dogs += 1
    end

    def self.total_number_of_dogs
      @@number_of_dogs
    end

    puts self
  end

  puts GoodDog.total_number_of_dogs

  dog1 = GoodDog.new
  dog2 = GoodDog.new

  puts GoodDog.total_number_of_dogs
#+end_src

#+RESULTS:
: GoodDog
: 0
: 2
