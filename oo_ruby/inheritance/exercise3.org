Inheritance
https://launchschool.com/books/oo_ruby/read/inheritance#exercises
Exercise 3

Create a module that you can mix in to ONE of your subclasses that describes a behavior unique to that subclass.

#+begin_src ruby :results output
module Loadable
  def weight_of_load
    "You're carrying 40 tons."
  end
end

class Vehicle
  attr_accessor :color
  attr_reader :year
  attr_reader :model

  @@num_objects = 0

  def initialize(year, model, color)
    @year = year
    @model = model
    @color = color
    @current_speed = 0
    @@num_objects += 1
  end

  def self.show_num_objects
    puts "#{@@num_objects} have been created."
  end

  def self.gas_mileage(miles, gallons)
    puts "Your car gets #{miles / gallons} miles per gallon."
  end

  def speed_up(number)
    @current_speed += number
    puts "You push the gas and accelerate #{number} mph."
  end

  def brake(number)
    @current_speed -= number
    puts "You push the brake and decelerate #{number} mph."
  end

  def current_speed
    puts "You are now going #{@current_speed} mph."
  end

  def shut_down
    @current_speed = 0
    puts "Let's park this bad boy!"
  end

  def spray_paint(new_color)
    self.color = new_color
    puts "You painted your car #{color}."
  end
end

class MyCar < Vehicle
  CAR = "This is a car."

  def to_s
    "My car is a #{color} #{year} #{model}."
  end
end

class MyTruck < Vehicle
  TRUCK = "This is a truck."

  include Loadable

  def to_s
    "My truck is a #{color} #{year} #{model}."
  end
end

lumina = MyCar.new(1997, 'chevy lumina', 'white')
puts lumina

truck = MyTruck.new(2000, 'chevy ranger', 'black')
puts truck
puts truck.weight_of_load

Vehicle.show_num_objects
#+end_src

#+RESULTS:
: My car is a white 1997 chevy lumina.
: My truck is a black 2000 chevy ranger.
: You're carrying 40 tons.
: 2 have been created.

