--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1 (Ubuntu 13.1-1.pgdg20.04+1)
-- Dumped by pg_dump version 13.1 (Ubuntu 13.1-1.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: employees; Type: TABLE; Schema: public; Owner: temp
--

CREATE TABLE public.employees (
    first_name character varying(100),
    last_name character varying(100),
    department character varying(100) DEFAULT 'unassigned'::character varying NOT NULL,
    vacation_remaining integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.employees OWNER TO temp;

--
-- Name: weather; Type: TABLE; Schema: public; Owner: temp
--

CREATE TABLE public.weather (
    date date NOT NULL,
    low integer NOT NULL,
    high integer NOT NULL,
    rainfall numeric(4,3) DEFAULT 0
);


ALTER TABLE public.weather OWNER TO temp;

--
-- Data for Name: employees; Type: TABLE DATA; Schema: public; Owner: temp
--

COPY public.employees (first_name, last_name, department, vacation_remaining) FROM stdin;
Leonardo	Ferreira	finance	14
Sara	Mikaelsen	operations	14
Lian	Ma	marketing	13
Haiden	Smith	unassigned	0
\.


--
-- Data for Name: weather; Type: TABLE DATA; Schema: public; Owner: temp
--

COPY public.weather (date, low, high, rainfall) FROM stdin;
2016-03-07	29	32	0.000
2016-03-08	23	31	0.000
2016-03-09	17	28	0.000
2016-03-01	34	43	0.117
2016-03-02	32	44	0.117
2016-03-03	31	47	0.156
2016-03-04	33	42	0.078
2016-03-05	39	46	0.273
2016-03-06	32	43	0.078
\.


--
-- PostgreSQL database dump complete
--

