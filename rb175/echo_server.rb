require "socket"

server = TCPServer.new("localhost", 3003)
loop do
  client = server.accept

  request_line = client.gets.split
  address = client.peeraddr(:hostname)

  client.puts "HTTP/1.1 200 OK\r\n\r\n" # valid response for Chrome
  puts request_line.join(' ')
  puts address.inspect

  client.puts "HTTP Method: #{request_line[0]}"
  client.puts "Path: #{request_line[1]}"
  client.puts "HTTP Version: #{request_line[2]}"
  client.puts "Client port: #{address[1]}"
  client.close
end
