require "socket"

def parse_request(request_line)
  http_method, path, http_version = request_line.split
  params = path.delete("?/").split('&').to_h { |param| param.split('=') }

  [http_method, path, http_version, params]
end

server = TCPServer.new("localhost", 3003)
puts "Listening on port: #{server.addr[1]}"

loop do
  client = server.accept
  request_line = client.gets
  next if !request_line || request_line =~ /favicon/  # handle extra browser request in Chrome

  http_method, path, http_version, params = parse_request(request_line)
  address = client.peeraddr(:hostname)

  client.puts "HTTP/1.1 200 OK\r\n\r\n" # valid response for Chrome

  client.puts "HTTP Method: #{http_method}"
  client.puts "HTTP Version: #{http_version}"
  client.puts "Client port: #{address[1]}"
  client.puts "Path: #{path}"
  client.puts "Params: #{params}"

  params["rolls"].to_i.times do |index|
    client.puts "Dice roll #{index + 1}: #{rand(params["sides"].to_i) + 1}"
  end

  client.close
end
