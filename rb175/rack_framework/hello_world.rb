require_relative 'advice'

class HelloWorld
  def call(env)
    case env['REQUEST_PATH']
    when '/'
      [
        '200',
        { 'Content-Type' => 'text/html' },
        [ erb(:index) ]
      ]
    when '/advice'
      piece_of_advice = Advice.new.generate
      [
        '200',
        { 'Content-Type' => 'text/html' },
        [ erb(:advice) ]
      ]
    when '/info'
      [
        '200',
        { 'Content-Type' => 'text/html' },
        [ erb(:env) ]
      ]
    else
      [
        '404',
        { 'Content-Type' => 'text/html', 'Content-Length' => '48' },
        [ erb(:not_found) ]
      ]
    end
  end

  private

  def erb(view)
    content = File.read("views/#{view}.erb")
    ERB.new(content).result
  end
end
