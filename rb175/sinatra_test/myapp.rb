require 'sinatra'
require 'sinatra/reloader'
require_relative 'advice'

get '/' do
  erb :index
end

get '/info' do
  erb :env, :locals => { message: env }
end

get '/advice' do
  piece_of_advice = Advice.new.generate
  erb :advice, :locals => { message: piece_of_advice }
end
