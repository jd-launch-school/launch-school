require "socket"

def parse_request(request_line)
  http_method, path, http_version = request_line.split
  params = path.delete("?/").split('&').to_h { |param| param.split('=') }

  [http_method, path, http_version, params]
end

server = TCPServer.new("localhost", 3003)
puts "Listening on port: #{server.addr[1]}"

loop do
  client = server.accept
  request_line = client.gets
  next if !request_line || request_line =~ /favicon/  # handle extra browser request in Chrome

  http_method, path, http_version, params = parse_request(request_line)
  address = client.peeraddr(:hostname)

  client.puts "HTTP/1.1 200 OK" # valid response for Chrome
  client.puts "Content-Type: text/html\r\n\r\n"

  client.puts """<html>
     <head>
       <title>Dice Roller</title>
     </head>

     <body>
       <h1>Dice Roller</h1>
       <h2>HTTP Request Info</h2>
       <p>Request Line: <code>#{request_line}</code></p>
       <p>HTTP Method: <code>#{http_method}</code></p>
       <p>HTTP Version: <code>#{http_version}</code></p>
       <p>Client port: <code>#{address[1]}</code></p>
       <p>Path: <code>#{path}</code></p>
       <p>Params: <code>#{params}</code></p>"""

  client.puts "<h2>Rolls</h2>"
  params['rolls'].to_i.times do |index|
    client.puts "<p>Dice roll #{index + 1}: #{rand(params["sides"].to_i) + 1}</p>"
  end

  client.puts "</body></html>"
  client.close
end
