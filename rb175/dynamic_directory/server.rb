require 'sinatra'
require 'sinatra/reloader'
require 'tilt/erubis'

get '/' do
  @title = "/public"
  @files = Dir.glob('*', base: "public").sort
  @files.reverse! if params['sort'] == 'descending'
  @params = params
  erb :home
end

get '/file*' do
  number = params['splat'].first
  @file = "file#{number}.txt"
  @title = @file
  erb :file
end
