require "socket"

def parse_request(request_line)
  http_method, path, http_version = request_line.split
  params = path.delete("?/").split('&').to_h { |param| param.split('=') }

  [http_method, path, http_version, params]
end

server = TCPServer.new("localhost", 3003)
puts "Listening on port: #{server.addr[1]}"

loop do
  client = server.accept
  request_line = client.gets
  next if !request_line || request_line =~ /favicon/  # handle extra browser request in Chrome

  http_method, path, http_version, params = parse_request(request_line)
  address = client.peeraddr(:hostname)
  number = params["number"].to_i

  client.puts "HTTP/1.1 200 OK" # valid response for Chrome
  client.puts "Content-Type: text/html\r\n\r\n"

  if params["quit"]
    client.puts "<html><head><title>Counter</title></head><body><h1>Goodbye!</h1></body></html>"
    break
  end

  client.puts """
     <html>
       <head>
         <title>Counter</title>
       </head>

       <body>
         <h1>Counter</h1>
         <h2>HTTP Request Info</h2>
         <p>Request Line: <code>#{request_line}</code></p>
         <p>HTTP Method: <code>#{http_method}</code></p>
         <p>HTTP Version: <code>#{http_version}</code></p>
         <p>Client port: <code>#{address[1]}</code></p>
         <p>Path: <code>#{path}</code></p>
         <p>Params: <code>#{params}</code></p>

         <h2>Counter App</h2>
         <p>Current Number: #{number}</p>
         <p><a href='?number=#{number + 1}'>Increment (+1)</a></p>
         <p><a href='?number=#{number - 1}'>Decrement (-1)</a></p>

         <h2>Commands</h2>
         <p><a href='?number=0'>Reset counter to zero</a></p>
         <p><a href='?quit=true'>Stop server</a></p>
  """
  client.puts "</body></html>"
  client.close
end
