require "sinatra"
require "sinatra/reloader"
require "tilt/erubis"

before do
  @toc = File.readlines "data/toc.txt"
end

helpers do
  def in_paragraphs(text)
    text.split("\n\n").map.with_index { |paragraph, index| "<p id=#{index}>#{paragraph}</p>" }.join
  end

  def highlight(text)
    text.gsub(/(#{params[:query]})/, '<strong>\1</strong>')

    # Can also use:
    # term = params[:query]
    # text.gsub(term, %(<strong>#{term}</strong>))
  end
end

not_found do
  redirect "/"
end

get "/" do
  @title = "The Adventures of Sherlock Holmes"
  erb :home
end

get "/chapters/:number" do
  number = params[:number].to_i
  chapter_name = @toc[number - 1]

  redirect "/" unless (1..@toc.size).cover? number

  @title = "Chapter #{number}: #{chapter_name}"
  @chapter = File.read "data/chp#{number}.txt"

  erb :chapter
end

get "/search" do
  @results = search
  erb :search
end

private

def matching_paragraphs(contents)
  contents.split("\n\n").each_with_index.reduce([]) do |array, (paragraph, index)|
    array << [index, paragraph] if paragraph.match?(params[:query])
    array
  end
end

def search
  @toc.each_with_index.reduce({}) do |hash, (title, index)|
    contents = File.read "data/chp#{index + 1}.txt"
    title.strip!

    paragraphs = matching_paragraphs(contents)

    hash[title] = [index + 1, paragraphs] unless paragraphs.empty?
    hash
  end
end
