require "sinatra"
require "sinatra/reloader"
require "tilt/erubis"
require 'yaml'

before do
  @users = YAML.load_file('users.yml')
end

helpers do
  def count_interests
    @users.reduce(0) { |sum, cur| sum + @users[cur.first][:interests].size }
  end
end

# matches '/' and '/users'
get /\/|\/users/ do
  @title = "Users"
  @names_of_users = @users.keys.map { |user| user.to_s }
  erb :home
end

get "/users/:name" do
  @name = params[:name].to_sym
  @title = @name.to_s.capitalize

  halt 404 unless @users.keys.include?(@name)

  erb :user
end

not_found do
  @title = "404 Not Found"
  erb :not_found
end
