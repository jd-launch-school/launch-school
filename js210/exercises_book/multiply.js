function getNumber(prompt) {
  let rlSync = require('readline-sync');
  let num = parseFloat(rlSync.question(prompt));
  return num
}

function multiply(first, second) {
  return first * second;
}

let first = getNumber('Enter the first number: ');
let second = getNumber('Enter the second number: ');
console.log(`${first} * ${second} = ${multiply(first, second)}`);
