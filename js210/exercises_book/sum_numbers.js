let rlSync = require('readline-sync');

let first = Number(rlSync.question('Enter the first number\n'));
let second = Number(rlSync.question('Enter the second number\n'));
let operation = rlSync.question('Enter the operation (+ | - | * | /)\n');
let result = 0;
let valid = true;

if (operation === '+') {
  operation = 'plus';
  result = first + second;
} else if (operation === '-') {
  operation = 'minus';
  result = first - second;
} else if (operation === '*') {
  operation = 'times';
  result = first * second;
} else if (operation === '/') {
  operation = 'divided by';
  result = first / second;
} else {
  valid = false
}

if (valid) {
  console.log(`${first} ${operation} ${second} is equal to ${result}.`);
} else {
  console.log('That is not a valid operation.');
}
