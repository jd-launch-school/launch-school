let name = prompt("What's your name?");
console.log(`Good Morning, ${name}.`);

let first = Number(prompt('Enter the first number'));
let second = Number(prompt('Enter the second number'));
let operation = prompt('Enter the operation (+ | - | * | /)');
let result = 0;
let valid = true;

if (operation === '+') {
  operation = 'plus';
  result = first + second;
} else if (operation === '-') {
  operation = 'minus';
  result = first - second;
} else if (operation === '*') {
  operation = 'times';
  result = first * second;
} else if (operation === '/') {
  operation = 'divided by';
  result = first / second;
} else {
  valid = false
}

if (valid) {
  console.log(`${first} ${operation} ${second} is equal to ${result}.`);
} else {
  console.log('That is not a valid operation.');
}
