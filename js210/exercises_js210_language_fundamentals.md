# JavaScript Basics
([Launch School Reference](https://launchschool.com/exercise_sets/c39a2eed))

## Building Strings
([Launch School Reference](https://launchschool.com/exercises/b39951cf))

The intention of the program below is to output a paragraph. Copy and paste the program into a JavaScript console (e.g., from the Chrome Developer Tools), then run it. Is the output what you expected? Are there any bugs/errors?

```js
const paragraph = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sed \
                ligula at risus vulputate faucibus. Aliquam venenatis nibh ut justo dignissim \
                dignissim. Proin dictum purus mollis diam auctor sollicitudin. Ut in bibendum \
                ligula. Suspendisse quam ante, dictum aliquam tristique id, porttitor pulvinar \
                diam. Maecenas blandit aliquet ipsum. Integer vitae sapien sed nulla rutrum \   
                hendrerit ac a urna. Interdum et malesuada fames ac ante ipsum primis in faucibus.';

console.log(paragraph);
```

### Solution
The program does not run. There are bugs/errors.

You might not get an error if you copy and paste the code into your own editor, especially if you have set it up to strip trailing whitespace.

First of all, there is a `SyntaxError` in assigning the paragraph to the `paragraph` variable. It is not easily visible, but there are spaces at the end of line 5 following the backslash (`\`) character. The purpose of the backslash is to escape the newline character at the end of the line. However, the extra whitespace prevents this from happening, causing a `SyntaxError` to be raised.

The program still has a bug, even after removing the extra whitespace on line 5: there are extra spaces in the middle of some of the sentences. This is because the program considers the whole string to be continuous, so the spaces at the beginning of each line (lines 2 to 6) are interpreted as part of the string.

## Conditionals Part 1
([Launch School Reference](https://launchschool.com/exercises/33151343))

Go over the following program. What does it log to the console at lines 7, 11, 15, and 19? Is it what you expected? Why or why not?

```js
const myBoolean = true;
const myString = 'hello';
const myArray = [];
const myOtherString = '';

if (myBoolean) {
  console.log('Hello');
}

if (!myString) {
  console.log('World');
}

if (!!myArray) {
  console.log('World');
}

if (myOtherString || myArray) {
  console.log('!');
}
```

### Solution
Line 7: `Hello`
Line 11: no output
Line 15: `World`
Line 19: `!`

## Conditionals Part 2
([Launch School Reference](https://launchschool.com/exercises/6cb663c8))

One of the ways to manage the flow of a program is through the use of conditionals. Go over the code below and specify how many unique execution paths are possible.

```js
if (condition1) {
  // ...
  if (condition2) {
    // ...
  } else {
    // ...
  }
} else {
  // ...
  if (condition4) {
    // ...
    if (condition5) {
    // ...
    }
  }
}
```

### Solution
Path 1: `condition1` --> `condition2`
Path 2: `condition1` --> not `condition2`
Path 3: not `condition1`
Path 4: not `condition1` --> `condition4`
Path 5: not `condition1` --> `condition4` --> `condition5`

## String Assignment
([Launch School Reference](https://launchschool.com/exercises/67a51bac))

Take a look at the following code:

```js
let name = 'Bob';
const saveName = name;
name = 'Alice';
console.log(name, saveName);
```

What does this code log to the console? Think about it for a moment before continuing.

If you said that this code logged:

```
Alice Bob
```

you would be 100% correct, and the answer should come as no surprise. Now let's look at something slightly different:

```js
const name = 'Bob';
const saveName = name;
name.toUpperCase();
console.log(name, saveName);
```

What does this code log? Can you explain these results?

### Solution
The output will be `Bob Bob`. This demonstrates that primitive values, like strings, are immutable. On line 3, the value of the constant `name` is coerced to a String object and then `String.prototype.toUpperCase` converts each character to uppercase. After the conversion, the String object is removed leaving only the new uppercase string primitive value. However, that value is not stored anywhere or used. So, on line 4 when we output the values of the `name` and `saveName` variables, their initial values are logged the console.

## Arithmetic Integer
([Launch School Reference](https://launchschool.com/exercises/df62e4d1))

Write a program that prompts the user for two positive integers, and then prints the results of the following operations on those two numbers: addition, subtraction, product, quotient, remainder, and power. Do not worry about validating the input.

Example:

```
==> Enter the first number:
23
==> Enter the second number:
17
==> 23 + 17 = 40
==> 23 - 17 = 6
==> 23 * 17 = 391
==> 23 / 17 = 1
==> 23 % 17 = 6
==> 23 ** 17 = 1.4105003956066297e+23
```

The final output above shows how JavaScript displays numbers that are too large for its Number type. The number represents approximately 1.41 * 100,000,000,000,000,000,000,000 using what is called floating-point notation. If you want to see what the exact value is, you can use JavaScript's `BigInt` type by appending an `n` to both 23 and 17:

```
> 23n ** 17n    // 141050039560662968926103n
```

### Solution
```js
const rlSync = require('readline-sync');
let first = Number(rlSync.question("Enter the first number:\n"));
let second = Number(rlSync.question("Enter the second number:\n"));

// --- OR ---
// console.log("Enter the first number:");
// let firstNumber = Number(readlineSync.prompt());

// console.log("Enter the second number:");
// let secondNumber = Number(readlineSync.prompt());

console.log(`${first} + ${second} = ${first + second}`)
console.log(`${first} - ${second} = ${first - second}`)
console.log(`${first} * ${second} = ${first * second}`)
console.log(`${first} / ${second} = ${Math.round(first / second)}`) // or Math.floor()
console.log(`${first} % ${second} = ${first % second}`)
console.log(`${first} ** ${second} = ${first ** second}`)           // or Math.pow()
```

## Counting the Number of Characters
([Launch School Reference](https://launchschool.com/exercises/7472ee00))

In this exercise, you will write a program that asks the user for a phrase, then outputs the number of characters in that phrase. Go over the [documentation for String](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) to find an appropriate method to use.

Examples:

```
Please enter a phrase: walk
There are 4 characters in "walk".

Please enter a phrase: walk, don't run
There are 15 characters in "walk, don't run".
```

### Solution
```js
const rlSync = require('readline-sync');
let phrase = rlSync.question("Please enter a phrase: ");
console.log(`There are ${phrase.length} characters in "${phrase}".`)
```

### Further Exploration
The solution counts all the characters in the phrase, including spaces. Refactor it so that it ignores spaces.

Since the refactored program shouldn't count spaces, you can use the `String.prototype.replace` method to return a new string with all spaces removed from it. Go over the documentation to see how you can do this. The thing to note here is that the first argument passed to replace can be a regular expression pattern.

As an added challenge, further refactor the solution so that it only counts alphabetic characters.

#### Don't count spaces
```js
const rlSync = require('readline-sync');
let phrase = rlSync.question("Please enter a phrase: ");
console.log(`There are ${phrase.replace(/\s/g, '').length} non-space characters in "${phrase}".`)
```

#### Only alphabetic characters
```js
const rlSync = require('readline-sync');
let phrase = rlSync.question("Please enter a phrase: ");
let regex = /[^a-z]/ig;
console.log(`There are ${phrase.replace(regex, '').length} alphabetic characters in "${phrase}".`)
```

## Convert a String to a Number
([Launch School Reference](https://launchschool.com/exercises/a6a29fea))

For this exercise we're going to learn more about type conversion by implementing our own `parseInt` function that converts a string of numeric characters (including an optional plus or minus sign) to a number.

The function takes a string of digits as an argument, and returns the appropriate number. Do not use any of the built-in functions for converting a string to a number type.

For now, do not worry about leading `+` or `-` signs, nor should you worry about invalid characters; assume all characters will be numeric.

Examples:

```js
stringToInteger('4321');      // 4321
stringToInteger('570');       // 570
```

### Solution
```js
/*
 * PROBLEM:
 * - INPUT: string
 *   + only numeric characters
 *   + no invalid characters
 *   + no + or - signs
 * - OUTPUT: number
 *
 * ALGORITHM:
 * - create an object with strings as keys and numbers as values
 * - declare 'result' variable initialized to 0
 * - LOOP using 'for'
 *   + let index = 0
 *   + loop while index is less than length of the string
 *   + increment index by 1
 *     - get value of character from object
 *     - multiply 10 by current result, then add the value of the character and assign this to result
 * - return result
 */

const DIGITS = { '0': 0, '1': 1, '2': 2,
                 '3': 3, '4': 4, '5': 5,
                 '6': 6, '7': 7, '8': 8,
                 '9': 9 }

function stringToInteger(string) {
  let result = 0;

  for (let index = 0; index < string.length; index++) {
    let value = DIGITS[string[index]];
    result = 10 * result + value;
  }

  return result;
}

console.log(stringToInteger('4321') === 4321);
console.log(stringToInteger('570') === 570);
```

### Solution using exponents
```js
let stringToInteger = function convertStringToInteger(string) {
  const DIGITS = { '0': 0, '1': 1, '2': 2, '3': 3,
                   '4': 4, '5': 5, '6': 6,
                   '7': 7, '8': 8, '9': 9 }

  let result = 0;

  for (let counter = 1; counter < string.length; counter++) {
    let digitIndex = string[counter - 1];
    result += DIGITS[digitIndex] * (10 ** (string.length - counter));
  }

  console.log(result);
}


console.log(stringToInteger('4321'));   // 4321
console.log(stringToInteger('570'));    // 570
```

## Convert a String to a Signed Number
([Launch School Reference](https://launchschool.com/exercises/38861cc1))

The previous exercise mimics the `parseInt` function to a lesser extent. In this exercise, you're going to extend that function to work with signed numbers.

Write a function that takes a string of digits and returns the appropriate number as an integer. The string may have a leading `+` or `-` sign; if the first character is a `+`, your function should return a positive number; if it is a `-`, your function should return a negative number. If there is no sign, return a positive number.

You may assume the string will always contain a valid number.

Examples:

```js
stringToSignedInteger('4321');      // 4321
stringToSignedInteger('-570');      // -570
stringToSignedInteger('+100');      // 100
```

### Solution
```js
function stringToSignedInteger(string) {
  const digits = { '0': 0,
                   '1': 1, '2': 2, '3': 3,
                   '4': 4, '5': 5, '6': 6,
                   '7': 7, '8': 8, '9': 9 }

  let result = 0;

  for (let index = 0; index < string.length; index++) {
    if (['-', '+'].includes(string[index])) continue;

    let value = digits[string[index]];
    result = 10 * result + value;
  }

  if (string[0] === '-') result *= -1;
  return result;
}


console.log(stringToSignedInteger('4321') === 4321);
console.log(stringToSignedInteger('-570') === -570);
console.log(stringToSignedInteger('+100') === 100);
```

## Convert a Number to a String
([Launch School Reference](https://launchschool.com/exercises/4a1cd983))

In the previous two exercises, you developed functions that convert simple numeric strings to signed integers. In this exercise and the next, you're going to reverse those functions.

You will learn more about converting strings to numbers by writing a function that takes a positive integer or zero, and converts it to a string representation.

You may not use any of the standard conversion functions available in JavaScript, such as `String()`, `Number.prototype.toString`, or an expression such as `'' + number`. Your function should do this the old-fashioned way and construct the string by analyzing and manipulating the number.

Examples:

```js
integerToString(4321);      // "4321"
integerToString(0);         // "0"
integerToString(5000);      // "5000"
```

### Solution
```js
/*
 * PROBLEM
 * - INPUT: number
 * - OUTPUT: string
 * - RULES:
 *   + cannot use built-in conversion functions
 *
 * ALGORITHM
 * - initialize `numbers` variable to the string '0123456789'
 * - initialize `result` variable to an empty array
 * - guard clause to see IF given number is equal to zero
 *   - return `result` concatenated with '0'
 * - LOOP until the given number is equal to zero
 *   - get the ones digit `number % 10`
 *   - get string value at the index of the ones digit
 *   - prepend ones digit string value to `result`
 *   - reassign `number` to `Math.floor(number / 10)`
 * - return joined `result`
 */

function integerToString(number) {
  const numbers = '0123456789';
  let result = [];

  if (number === 0) return result += '0';

  while (number > 0) {
    result.unshift(numbers[number % 10]);
    number = Math.floor(number / 10);
  }

  return result.join('');
}


console.log(integerToString(4321) === '4321');
console.log(integerToString(0) === '0');
console.log(integerToString(5000) === '5000');
```

## Convert a Signed Number to a String
([Launch School Reference](https://launchschool.com/exercises/cfd1f1ac))

 In the previous exercise, you reimplemented a function that converts non-negative numbers to strings. In this exercise, you're going to extend that function by adding the ability to represent negative numbers.

You may not use any of the standard conversion functions available in JavaScript, such as `String()`, `Number.prototype.toString`, or an expression such as `'' + number`. You may, however, use the `integerToString` function from the previous exercise.

Examples:

```
signedIntegerToString(4321);      // "+4321"
signedIntegerToString(-123);      // "-123"
signedIntegerToString(0);         // "0"
```

### Solution
```js
function integerToString(number) {
  const numbers = '0123456789';
  let result = [];

  if (number === 0) return result += '0';

  while (number > 0) {
    result.unshift(numbers[number % 10]);
    number = Math.floor(number / 10);
  }

  return result.join('');
}

function signedIntegerToString(number) {
  if (number === 0) return '0';
  if (number < 0) return `-${integerToString(number * -1)}`;

  return `+${integerToString(number)}`;
}


console.log(signedIntegerToString(4321) === '+4321');
console.log(signedIntegerToString(0) === '0');
console.log(signedIntegerToString(5000) === '+5000');
console.log(signedIntegerToString(-123) === '-123');
```

# Functions
([Launch School Reference](https://launchschool.com/exercise_sets/4f20da02))

## Local vs Global Part 1
([Launch School Reference](https://launchschool.com/exercises/a00f2b05))

What will the following code log to the console and why? Don't run the code until after you have tried to answer.

```js
var myVar = 'This is global';

function someFunction() {
  var myVar = 'This is local';
}

someFunction();
console.log(myVar);
```

### Solution
This will log `This is global` to the console.

JavaScript uses lexical scope. This means that it determines the scope of a variable by the structure of the program.

The `myVar` variable declaration on line 4 shadows the `myVar` variable declaration on line 1, creating a second `myVar` variable within the local inner scope of the `someFunction` function. When execution returns from the `someFunction` function, this second `myVar` variable is garbage collected since it is not returned from the function or used.

When we log the value of `myVar` to the console on line 8, the `myVar` declaration on line one is the only `myVar` variable in the global outer scope, so the string `This is global` is logged to the console. At this point, the `myVar` variable declared within the `someFunction` function no longer exists.

## Local vs Global Part 2
([Launch School Reference](https://launchschool.com/exercises/45ba0e0d))

What will the following code log to the console and why? Don't run the code until after you have tried to answer.

```js
var myVar = 'This is global';

function someFunction() {
  var myVar = 'This is local';
  console.log(myVar);
}

someFunction();
```

### Solution
This will log `This is local` to the console.

The `myVar` variable declaration on line 4 shadows the `myVar` variable declaration on line 1, creating a second `myVar` variable within the local inner scope of the `someFunction` function. When we log the value of `myVar` from within the `someFunction` function, the string `This is local` is logged to the console.

## Local vs Global Part 3
([Launch School Reference](https://launchschool.com/exercises/896063f6))

What will the following code log to the console and why? Don't run the code until after you have tried to answer.

```js
var myVar = 'This is global';

function someFunction() {
  myVar = 'This is local';
}

someFunction();
console.log(myVar);
```

### Solution
This will log `This is local` to the console.

The `myVar` variable declaration on line 1 places the `myVar` variable in the global outer scope. Variables declared in an outer scope are accessible and modifiable from within the local inner scope of functions and blocks. In this case, there is only one `myVar` variable. So, when we reassign the `myVar` variable on line 4, this points the `myVar` variable to the string `This is local`. This is the value that is logged to the console on line 8.

## Variable Lookup
([Launch School Reference](https://launchschool.com/exercises/3ff6d7e1))

What will the following code log to the console and why? Don't run the code until after you have tried to answer.

```js
var myVar = 'This is global';

function someFunction() {
  console.log(myVar);
}

someFunction();
```

### Solution
This will log `This is global` to the console.

The `myVar` variable declaration on line 1 places the `myVar` variable in the global outer scope. Variables declared in an outer scope are accessible and modifiable from within the local inner scope of functions and blocks. When we invoke the `someFunction` function and log the value of `myVar`, JavaScript looks for a `myVar` variable within the inner scope of the function. It doesn't find one, so it looks to next outer scope, which is the global scope in this case. It finds the `myVar` declaration on line 1 in the outer global scope and logs `This is global` to the console.

## Variable Scope
([Launch School Reference](https://launchschool.com/exercises/dcb19d63))

What will the following code log to the console and why? Don't run the code until after you have tried to answer.

```js
function someFunction() {
  myVar = 'This is global';
}

someFunction();
console.log(myVar);
```

### Solution
This will log `This is global` to the console.

When we call the `someFunction` function on line 5, we declare a global variable `myVar` and initialize it to the string `This is global`. Since we do not use `var`, `let`, `const`, or `function` to declare this variable, and since there is no other declaration of a variable with the same name in this or any outer scope, JavaScript binds `myVar` to be a property of the global object. This makes `myVar` act almost like it was globally declared, so it is accessible from the outer scope. Therefore, when we log the value of `myVar` to the console on line 6, JavaScript finds the `myVar` variable bound to the global object on line 2 and prints `This is global` to the screen.

## Arguments Part 1
([Launch School Reference](https://launchschool.com/exercises/84e2101a))

What will the following code log to the console and why? Don't run the code until after you have tried to answer.

```js
let a = 7;

function myValue(b) {
  b += 10;
}

myValue(a);
console.log(a);
```

### Solution
This will log `7` to the console.

When we pass in the variable `a` to the `myValue` function as an argument, the local variable `b` is declared within the function, initialized to the number `7`, and reassigned to the number value `17`. This has no effect on the `a` variable in the outer scope since `7` is an immutable primitive value. JavaScript stores primitive values in variables (i.e. pass by value), but uses pointers for non-primitive values like arrays and objects (i.e. pass by reference). Therefore, when we log the value of the `a` variable to the console on line 8, `7` is printed to the screen.

## Arguments Part 2
([Launch School Reference](https://launchschool.com/exercises/ae3a4c99))

What will the following code log to the console and why? Don't run the code until after you have tried to answer.

```js
let a = 7;

function myValue(a) {
  a += 10;
}

myValue(a);
console.log(a);
```

### Solution
This will log `7` to the console.

When we pass in the variable `a` to the `myValue` function as an argument, a new local variable `a` is declared within the function, initialized to the number `7`, and reassigned to the number value `17`. The new local variable within the function shadows the variable `a` declared in the outer global scope. The reassignment has no effect on the `a` variable in the outer scope. Therefore, when we log the value of the `a` variable to the console on line 8, `7` is printed to the screen.

## Arguments Part 3
([Launch School Reference](https://launchschool.com/exercises/ef9ad607))

What will the following code log to the console and why? Don't run the code until after you have tried to answer.

```js
let a = [1, 2, 3];

function myValue(b) {
  b[2] += 7;
}

myValue(a);
console.log(a);
```

### Solution
This will log `[1, 2, 10]` to the console.

When we pass in the variable `a` to the `myValue` function as an argument, the local variable `b` is declared within the function, pointing to the array object `[1, 2, 3]`. In JavaScript, arrays and objects are mutable. JavaScript stores primitive values in variables (i.e. pass by value), but uses pointers for non-primitive values like arrays and objects (i.e. pass by reference). When we add `7` to the value at the third index of the array (`7 + 3`) for a total of `10` and reassign the number `10` to the third index of the array object, both the `b` variable within the function and the `a` variable in the outer scope continue to point to the modified array object. Therefore, when we log the value of the `a` variable to the console on line 8, `[1, 2, 10]` is printed to the screen.

## Variable Declarations
([Launch School Reference](https://launchschool.com/exercises/e7cca2b0))

What will the following code log to the console and why? Don't run the code until after you have tried to answer.

```js
console.log(a);

var a = 1;
```

### Solution
This will log `undefined` to the console.

This is an example of variable hoisting. Variables declared with `var` will be hoisted, but their initialization is not hoisted, i.e. anything after the assignment operator `=` is not hoisted. When variables declared with `var` are hoisted, they are initialized to `undefined`. So, when we log the value of the `a` variable to the console before we assign the number `1` to the `a` variable, `undefined` is printed to the screen.

After hoisting, this code is similar to:

```js
var a;
console.log(a);

a = 1;
```

## Function Declarations
([Launch School Reference](https://launchschool.com/exercises/8c20053b))

What will the following code log to the console and why? Don't run the code until after you have tried to answer.

```js
logValue();

function logValue() {
  console.log('Hello, world!');
}
```

### Solution
This will log `Hello, world!` to the console.

This is an example of function hoisting. JavaScript hoists function declarations, both the function name and body, to the top of the scope, so even though we invoke the function before we define the function lexically, JavaScript finds the function during the creation phase and makes it available as needed.

After hoisting, this code is similar to:

```js
function logValue() {
  console.log('Hello, world!');
}

logValue();
```

### Further Exploration
Let's refactor the code a bit. What does it log now? What is the hoisted equivalent of this code?

```js
var logValue = 'foo';

function logValue() {
  console.log('Hello, world!');
}

console.log(typeof logValue);
```

#### Solution
This will log `string` to the console.

With hoisting, JavaScript first hoists all of the functions. Then, it hoists the variables. Since we reassign the variable `logValue` to a string after the function is hoisted, `string` is logged to the console as the type of the `logValue` variable.

After hoisting, this code is similar to:

```js
function logValue() {
  console.log('Hello, world!');
}

var logValue;    // this is ignored since logValue has already been declared
logValue = 'foo';

console.log(typeof logValue);
```
# Arrays
([Launch School Reference](https://launchschool.com/exercise_sets/75d23811))

## Array Copy Part 1
([Launch School Reference](https://launchschool.com/exercises/c888987d))

Read through the code shown below. What does it log to the console at lines 6 and 10?

```
let myArray = [1, 2, 3, 4];
const myOtherArray = myArray;

myArray.pop();
console.log(myArray);
console.log(myOtherArray);

myArray = [1, 2];
console.log(myArray);
console.log(myOtherArray);
```

### Solution
Lines 6 and 10 will output:

```
[1, 2, 3]
[1, 2, 3]
```

This demonstrates the pass-by-reference nature of arrays in JavaScript. When we assign the `[1, 2, 3, 4]` array literal to the `myArray` variable on line 1 and then assign `myArray` to `myOtherArray`, both variables reference to the same array in memory. When we call `pop` on `myArray`, this removes and returns the last element of the array that both `myArray` and `myOtherArray` reference. Therefore, when we log the value of `myOtherArray` on line 6, the modified array `[1, 2, 3]` is printed to the screen.

Then, on line 8, we reassign the `myArray` variable to a new array `[1, 2]`. This does not affect `myOtherArray` because it still references the modified array from earlier. Therefore, when we log the value of `myOtherArray` on line 10, the modified array `[1, 2, 3]` is printed to the screen again.

## Array Copy Part 2
([Launch School Reference](https://launchschool.com/exercises/08ff4ec8))

In the previous exercise, the value of the reference gets copied. For this exercise, only the values of the array should be copied, but not the reference. Implement two alternative ways of doing this.

Here is the code from the previous exercise:

```js
let myArray = [1, 2, 3, 4];
const myOtherArray = myArray;

myArray.pop();
console.log(myOtherArray);

myArray = [1, 2];
console.log(myOtherArray);
```

### Solution using `slice`, `filter`, `map`, `concat`, `from`, or spread `...` syntax
```js
let myArray = [1, 2, 3, 4];
const myOtherArray = myArray.slice();
// const myOtherArray = myArray.filter(elem => true);
// const myOtherArray = myArray.map(elem => elem);
// const myOtherArray = [].concat(myArray);  // or ---> myArray.concat();
// const myOtherArray = Array.from(myArray);
// const myOtherArray = [...myArray];

myArray.pop();
console.log(myOtherArray);

myArray = [1, 2];
console.log(myOtherArray);
```

### Solution using `for` loop
```js
let myArray = [1, 2, 3, 4];
const myOtherArray = [];

for (let index = 0; index < myArray.length; index++) {
  myOtherArray.push(myArray[index]);
}

myArray.pop();
console.log(myOtherArray);

myArray = [1, 2];
console.log(myOtherArray);
```

## Array Concat Part 1
([Launch School Reference](https://launchschool.com/exercises/ae2ed0d3))

In this exercise, you will learn more about Arrays by implementing your own version of the `Array.prototype.concat` method. Write a function that returns a new array composed of all values from the first array argument and the second array or value argument. Take note of the following specifications when writing your `concat` function.

- The first argument will always be an array.
- The second argument can be either an array or another value.
- The function should return a new array.
- The elements in the new array should be in the same order as they appear in the arguments.
- The function should copy any object references from the arguments into the new array — i.e., if you make a change to a reference object from one of the arguments after calling `concat`, those changes should show up in the output array as well.
- The function should copy the values of primitives (e.g., strings, numbers, and booleans).

Examples:

```js
function concat(array1, secondArgument) {
  // ...
}

concat([1, 2, 3], [4, 5, 6]);          // [1, 2, 3, 4, 5, 6]
concat([1, 2], 3);                     // [1, 2, 3]
concat([2, 3], ['two', 'three']);      // [2, 3, "two", "three"]
concat([2, 3], 'four');                // [2, 3, "four"]


const obj = { a: 2, b: 3 };
const newArray = concat([2, 3], obj);
newArray;                              // [2, 3, { a: 2, b: 3 }]
obj.a = 'two';
newArray;                              // [2, 3, { a: "two", b: 3 }]

const arr1 = [1, 2, 3];
const arr2 = [4, 5, obj];
const arr3 = concat(arr1, arr2);
arr3;                                  // [1, 2, 3, 4, 5, { a: "two", b: 3 }]
obj.b = 'three';
arr3;                                  // [1, 2, 3, 4, 5, { a: "two", b: "three" }]

arr3[5].b = 3;                         // or, `arr3[5]['b'] = 3;`
obj;                                   // { a: "two", b: 3 }
```

### Solution using spread `...` syntax
```js
function concat(array1, secondArgument) {
  let newArray = array1.slice();

  if (Array.isArray(secondArgument)) {
    newArray.push(...secondArgument);
  } else {
    newArray.push(secondArgument);
  }

  return newArray;
}

console.log(concat([1, 2, 3], [4, 5, 6]));          // [1, 2, 3, 4, 5, 6]
console.log(concat([1, 2], 3));                     // [1, 2, 3]
console.log(concat([2, 3], ['two', 'three']));      // [2, 3, "two", "three"]
console.log(concat([2, 3], 'four'));                // [2, 3, "four"]


const obj = { a: 2, b: 3 };
const newArray = concat([2, 3], obj);
console.log(newArray);                              // [2, 3, { a: 2, b: 3 }]
obj.a = 'two';
console.log(newArray);                              // [2, 3, { a: "two", b: 3 }]

const arr1 = [1, 2, 3];
const arr2 = [4, 5, obj];
const arr3 = concat(arr1, arr2);
console.log(arr3);                                  // [1, 2, 3, 4, 5, { a: "two", b: 3 }]
obj.b = 'three';
console.log(arr3);                                  // [1, 2, 3, 4, 5, { a: "two", b: "three" }]
console.log(arr1, arr2);                            // [ 1, 2, 3 ] [ 4, 5, { a: 'two', b: 'three' } ]

console.log(arr3[5].b = 3);                         // or, `arr3[5]['b'] = 3;`
console.log(obj);                                   // { a: "two", b: 3 }
```

### Solution using `forEach`
```js
function concat(array1, secondArgument) {
  let newArray = array1.slice();

  if (Array.isArray(secondArgument)) {
    secondArgument.forEach(elem => newArray.push(elem));
  } else {
    newArray.push(secondArgument);
  }

  return newArray;
}

```

## Array Concat Part 2
([Launch School Reference](https://launchschool.com/exercises/a5099dc9))

The `concat` function from the previous exercise could only concatenate a maximum of two arrays. For this exercise, you are going to extend that functionality. Refactor the `concat` function to allow for the concatenation of two or more arrays or values.

Examples:

```
concat([1, 2, 3], [4, 5, 6], [7, 8, 9]);    // [1, 2, 3, 4, 5, 6, 7, 8, 9]
concat([1, 2], 'a', ['one', 'two']);        // [1, 2, "a", "one", "two"]
concat([1, 2], ['three'], 4);               // [1, 2, "three", 4]
```

### Solution
```js
function concat(...args) {
  let newArray = [];

  args.forEach(arg => {
    if (Array.isArray(arg)) {
      // newArray.push(...arg);
      // or...
      arg.forEach(elem => newArray.push(elem));
    } else {
      newArray.push(arg);
    }
  });

  return newArray;
}

console.log(concat([1, 2, 3], [4, 5, 6], [7, 8, 9]));    // [1, 2, 3, 4, 5, 6, 7, 8, 9]
console.log(concat([1, 2], 'a', ['one', 'two']));        // [1, 2, "a", "one", "two"]
console.log(concat([1, 2], ['three'], 4));               // [1, 2, "three", 4]
```

### Solution using `flat`
```js
function concat(...args) {
  return args.flat();
}
```

## Array Pop and Push
([Launch School Reference](https://launchschool.com/exercises/8e823938))

In this exercise, you will implement your own version of two Array methods: `Array.prototype.pop` and `Array.prototype.push`. The pop method removes the last element from an array and returns that element. If `pop` is called on an empty array, it returns `undefined`. The `push` method, on the other hand, adds one or more elements to the end of an array and returns the new length of the array.

Examples:

```js
// pop
const array1 = [1, 2, 3];
pop(array1);                        // 3
console.log(array1);                // [1, 2]
pop([]);                           // undefined
pop([1, 2, ['a', 'b', 'c']]);      // ["a", "b", "c"]

// push
const array2 = [1, 2, 3];
push(array2, 4, 5, 6);              // 6
console.log(array2);                // [1, 2, 3, 4, 5, 6]
push([1, 2], ['a', 'b']);          // 3
push([], 1);                       // 1
push([]);                          // 0
```

### Solution
```js
function pop(array) {
  if (array.length === 0) return undefined;

  let lastElement = array[array.length - 1];
  array.length -= 1;

  return lastElement;
}

function push(array, ...args) {
  args.forEach(arg => array[array.length] = arg);
  return array.length;
}

// pop
const array1 = [1, 2, 3];
console.log(pop(array1));                       // 3
console.log(array1);                            // [1, 2]
console.log(pop([]));                           // undefined
console.log(pop([1, 2, ['a', 'b', 'c']]));      // ["a", "b", "c"]

// push
const array2 = [1, 2, 3];
console.log(push(array2, 4, 5, 6));             // 6
console.log(array2);                            // [1, 2, 3, 4, 5, 6]
console.log(push([1, 2], ['a', 'b']));          // 3
console.log(push([], 1));                       // 1
console.log(push([]));                          // 0
```

### Solution using `splice`
```js
function pop(array) {
  let lastElement = array[array.length - 1];
  array.splice(array.length - 1);
  return lastElement;
}
```

## Array and String Reverse
([Launch School Reference](https://launchschool.com/exercises/ef1f43c3))

In this exercise, you will implement your own version of the `Array.prototype.reverse` method. Your implementation should differ from the built-in method in the following two ways:

- It should accept either a string or an array as an argument.
- It should return a new string or array.

Examples:

```js
function reverse(inputForReversal) {
  // ...
}

reverse('Hello');           // "olleH"
reverse('a');               // "a"
reverse([1, 2, 3, 4]);      // [4, 3, 2, 1]
reverse([]);                // []

const array = [1, 2, 3];
reverse(array);             // [3, 2, 1]
array;                      // [1, 2, 3]
```

### Solution
```js
function reverse(inputForReversal) {
  const isArray = Array.isArray(inputForReversal);
  let output = isArray ? [] : '';

  for (let index = inputForReversal.length - 1; index >= 0; index--) {
    let currentItem = inputForReversal[index];
    isArray ? output.push(currentItem) : output += currentItem;
  }

  return output;
}

console.log(reverse('Hello'));           // "olleH"
console.log(reverse('a'));               // "a"
console.log(reverse([1, 2, 3, 4]));      // [4, 3, 2, 1]
console.log(reverse([]));                // []

const array = [1, 2, 3];
console.log(reverse(array));             // [3, 2, 1]
console.log(array);                      // [1, 2, 3]
```

### Solution using multiple functions
```js
function reverseArray(array) {
  let output = [];

  for (let index = array.length - 1; index >= 0; index--) {
    output.push(array[index]);
  }

  return output;
}

function reverseString(string) {
  let output = '';

  for (let index = string.length - 1; index >= 0; index--) {
    output += string[index];
  }

  return output;
}


function reverse(inputForReversal) {
  const isArray = Array.isArray(inputForReversal);
  return isArray ? reverseArray(inputForReversal) : reverseString(inputForReversal);
}
```

### Best Solution (only check for array when returning)
```js
function reverse(input) {
  let output = [];

  for (let index = input.length - 1; index >= 0; index--) {
    output.push(input[index]);
  }

  return Array.isArray(input) ? output : output.join('');
}
```

## Array Shift and Unshift
([Launch School Reference](https://launchschool.com/exercises/93e3185d))

In this exercise, you will implement your own versions of the `Array.prototype.shift` and `Array.prototype.unshift` methods. These methods manipulate the contents of an array starting from the first index.

The `shift` method removes the first element from an array and returns that element; if the array is empty, `shift` returns `undefined`. The `unshift` method adds one or more elements to the start of an array and returns the new length of the array. Both methods mutate the original array.

Examples:

```js
shift([1, 2, 3]);                // 1
shift([]);                       // undefined
shift([[1, 2, 3], 4, 5]);        // [1, 2, 3]

unshift([1, 2, 3], 5, 6);        // 5
unshift([1, 2, 3]);              // 3
unshift([4, 5], [1, 2, 3]);      // 3

const testArray = [1, 2, 3];
shift(testArray);                // 1
testArray;                       // [2, 3]
unshift(testArray, 5);           // 3
testArray;                       // [5, 2, 3]
```

### Solution with `splice`
```js
function shift(array) {
  if (array.length === 0) return undefined;
  return array.splice(0, 1).pop();
}

function unshift(array, ...args) {
  for (let index = 0; index < args.length; index++) {
    array.splice(index, 0, args[index]);
  }

  return array.length;
}

console.log(shift([1, 2, 3]));                // 1
console.log(shift([]));                       // undefined
console.log(shift([[1, 2, 3], 4, 5]));        // [1, 2, 3]

console.log(unshift([1, 2, 3], 5, 6));        // 5
console.log(unshift([1, 2, 3]));              // 3
console.log(unshift([4, 5], [1, 2, 3]));      // 3

const testArray = [1, 2, 3];
console.log(shift(testArray));                // 1
console.log(testArray);                       // [2, 3]
console.log(unshift(testArray, 5));           // 3
console.log(testArray);                       // [5, 2, 3]
console.log(unshift(testArray, 6, 7));        // 5
console.log(testArray);                       // [6, 7, 5, 2, 3]
```

### Solution without `splice`
```js
function shift(array) {
  if (array.length === 0) return undefined;

  let result = array[0];

  for (let index = 0; index < array.length - 1; index++) {
    array[index] = array[index + 1];
  }

  array.length -= 1;
  return result;
}

function unshift(array, ...args) {
  for (let argsIndex = args.length - 1; argsIndex >= 0; argsIndex--) {
    for (let arrayIndex = array.length; arrayIndex >= 1; arrayIndex--) {
      array[arrayIndex] = array[arrayIndex - 1];
    }

    array[0] = args[argsIndex];
  }

  return array.length;
}

console.log(shift([1, 2, 3]));                // 1
console.log(shift([]));                       // undefined
console.log(shift([[1, 2, 3], 4, 5]));        // [1, 2, 3]

console.log(unshift([1, 2, 3], 5, 6));        // 5
console.log(unshift([1, 2, 3]));              // 3
console.log(unshift([4, 5], [1, 2, 3]));      // 3

const testArray = [1, 2, 3];
console.log(shift(testArray));                // 1
console.log(testArray);                       // [2, 3]
console.log(unshift(testArray, 5));           // 3
console.log(testArray);                       // [5, 2, 3]
console.log(unshift(testArray, 6, 7));        // 5
console.log(testArray);                       // [6, 7, 5, 2, 3]
```

## Array Slice and Splice
([Launch School Reference](https://launchschool.com/exercises/e8d8cc55))

In this exercise, you will implement your own versions of the `Array.prototype.slice` and `Array.prototype.splice` methods according to the following specifications.

The `slice` function takes three arguments: an array, and two integers representing a `begin` and an `end` index. The function returns a new array containing the extracted elements starting from `begin` up to but not including `end`. `slice` does not mutate the original array.

The `splice` function changes the contents of an array by deleting existing elements and/or adding new elements. The function takes the following arguments: an array, a `start` index, a `deleteCount`, and zero or more elements to be added. The function removes `deleteCount` number of elements from the array, beginning at the `start` index. If any optional element arguments are provided, `splice` inserts them into the array beginning at the `start` index. The function returns a new array containing the deleted elements, or an empty array (`[]`) if no elements are deleted. `splice` mutates the original array.

Additional specifications:

`slice`:

- The values of `begin` and `end` will always be integers greater than or equal to `0`.
- If the value of `begin` or `end` is greater than the length of the array, set it to equal the length.

`splice`:

- The values of `start` and `deleteCount` will always be integers greater than or equal to `0`.
- If the value of `start` is greater than the length of the array, set it to equal the length.
- If the value of `deleteCount` is greater than the number of elements from `start` up to the end of the array, set `deleteCount` to the difference between the array's length and `start`.
- Takes optional arguments for elements to add to the array; denoted by the parameters `element1` up to `elementN`. If no elements to add are provided, `splice` only removes elements from the array.

Examples:

```js
function slice(array, begin, end) {
  // ...
}

slice([1, 2, 3], 1, 2);               // [2]
slice([1, 2, 3], 2, 0);               // []
slice([1, 2, 3], 5, 1);               // []
slice([1, 2, 3], 0, 5);               // [1, 2, 3]

const arr1 = [1, 2, 3];
slice(arr1, 1, 3);                     // [2, 3]
arr1;                                  // [1, 2, 3]

function splice(array, start, deleteCount, element1, elementN) {
  // ...
}

splice([1, 2, 3], 1, 2);              // [2, 3]
splice([1, 2, 3], 1, 3);              // [2, 3]
splice([1, 2, 3], 1, 0);              // []
splice([1, 2, 3], 0, 1);              // [1]
splice([1, 2, 3], 1, 0, 'a');         // []

const arr2 = [1, 2, 3];
splice(arr2, 1, 1, 'two');             // [2]
arr2;                                  // [1, "two", 3]

const arr3 = [1, 2, 3];
splice(arr3, 1, 2, 'two', 'three');    // [2, 3]
arr3;                                  // [1, "two", "three"]

const arr4 = [1, 2, 3];
splice(arr4, 1, 0);                    // []
splice(arr4, 1, 0, 'a');               // []
arr4;                                  // [1, "a", 2, 3]

const arr5 = [1, 2, 3];
splice(arr5, 0, 0, 'a');               // []
arr5;                                  // ["a", 1, 2, 3]
```

### Solution
```js
function slice(array, begin, end) {
  let newArray = [];

  if (begin > array.length) begin = array.length;
  if (end > array.length) end = array.length;

  for (let index = begin; index < end; index++) {
    newArray.push(array[index]);
  }

  return newArray;
}

console.log(slice([1, 2, 3], 1, 2));               // [2]
console.log(slice([1, 2, 3], 2, 0));               // []
console.log(slice([1, 2, 3], 5, 1));               // []
console.log(slice([1, 2, 3], 0, 5));               // [1, 2, 3]

const arr1 = [1, 2, 3];
console.log(slice(arr1, 1, 3));                     // [2, 3]
console.log(arr1);                                  // [1, 2, 3]

function splice(array, start, deleteCount, ...elements) {
  let deletedElements = [];

  if (start > array.length) start = array.length;
  if (deleteCount > array.length - 1 - start) deleteCount = array.length - start;

  for (let index = start; index < start + deleteCount; index++) {
    deletedElements.push(array[index]);
  }

  if (elements.length === 0 && deleteCount !== 0) {
    for (let index = start; index < array.length; index++) {
      array[index] = array[index + 1];
    }

    array.length -= deleteCount;
  } else if (elements.length !== 0 && deleteCount !== 0) {
    let index = start;

    elements.forEach(elem => {
      array[index] = elem;
      index += 1;
    })
  } else if (elements.length !== 0 && deleteCount === 0) {
    for (let index = array.length; index > start; index--) {
      array[index] = array[index - 1];
    }

    array[start] = elements[0];
  }

  return deletedElements;
}

console.log(splice([1, 2, 3], 1, 2));              // [2, 3]
console.log(splice([1, 2, 3], 1, 3));              // [2, 3]
console.log(splice([1, 2, 3], 1, 0));              // []
console.log(splice([1, 2, 3], 0, 1));              // [1]
console.log(splice([1, 2, 3], 1, 0, 'a'));         // []

const arr2 = [1, 2, 3];
console.log(splice(arr2, 1, 1, 'two'));             // [2]
console.log(arr2);                                  // [1, "two", 3]

const arr3 = [1, 2, 3];
console.log(splice(arr3, 1, 2, 'two', 'three'));    // [2, 3]
console.log(arr3);                                  // [1, "two", "three"]

const arr4 = [1, 2, 3];
console.log(splice(arr4, 1, 0));                    // []
console.log(splice(arr4, 1, 0, 'a'));               // []
console.log(arr4);                                  // [1, "a", 2, 3]

const arr5 = [1, 2, 3];
console.log(splice(arr5, 0, 0, 'a'));               // []
console.log(arr5);                                  // ["a", 1, 2, 3]
```

### Solution for `splice` using copy of array
```js
function splice(array, start, deleteCount, ...elements) {
  if (start > array.length) start = array.length;
  if (deleteCount > array.length - 1 - start) deleteCount = array.length - start;

  let arrayCopy = slice(array, 0, array.length);
  array.length = array.length + elements.length - deleteCount;

  for (let index = 0; index < elements.length; index++) {
    array[start + index] = elements[index];
  }

  let numberOfElementsToCopy = arrayCopy.length - (start + deleteCount);

  for (let index = 0; index < numberOfElementsToCopy; index++) {
    array[start + elements.length + index] = arrayCopy[start + deleteCount + index];
  }

  return slice(arrayCopy, start, start + deleteCount);
}
```

### Better Solution
```js
function splice(array, start, deleteCount, ...elements) {
  if (start > array.length) start = array.length;
  if (deleteCount > array.length - 1 - start) deleteCount = array.length - start;

  let startSegment = slice(array, 0, start);
  let endSegment = slice(array, start + deleteCount, array.length);
  let deletedElements = slice(array, start, start + deleteCount);

  array.length = 0;
  array.push(...startSegment, ...elements, ...endSegment);

  return deletedElements;
}
```

## Oddities
([Launch School Reference](https://launchschool.com/exercises/9d44cd7d))

The `oddities` function takes an array as an argument and returns a new array containing every other element from the input array. The values in the returned array are the first (index `0`), third, fifth, and so on, elements of the input array. The program below uses the array returned by oddities as part of a comparison. Can you explain the results of these comparisons?

Examples:

```js
function oddities(array) {
  const oddElements = [];

  for (let i = 0; i < array.length; i += 2) {
    oddElements.push(array[i]);
  }

  return oddElements;
}

oddities([2, 3, 4, 5, 6]) === [2, 4, 6];      // false
oddities(['abc', 'def']) === ['abc'];         // false
```

### Solution
There is nothing wrong with the `oddities` function. Technically, it outputs an array with the same values as the expected array for each test case. However, when comparing two arrays with strict equality, the two arrays must reference the same array object in memory in order for strict equality to return `true`. In this case, we're comparing the array in memory returned from the function with another array with similar values. Therefore, the comparison is `false` since the two arrays do not reference the same array in memory.

## Array Comparison
([Launch School Reference](https://launchschool.com/exercises/af3ae23e))

The array comparison function that we implemented in the Arrays lesson (Alternate link if the previous link doesn't work) implicitly assumed that when comparing two arrays, any matching values must also have matching index positions. The objective of this exercise is to reimplement the function so that two arrays containing the same values—but in a different order—are considered equal. For example, `[1, 2, 3]` === `[3, 2, 1]` should return `true`.

Examples:

```js
function areArraysEqual(array1, array2) {
  // ...
}

areArraysEqual([1, 2, 3], [1, 2, 3]);                  // true
areArraysEqual([1, 2, 3], [3, 2, 1]);                  // true
areArraysEqual(['a', 'b', 'c'], ['b', 'c', 'a']);      // true
areArraysEqual(['1', 2, 3], [1, 2, 3]);                // false
areArraysEqual([1, 1, 2, 3], [3, 1, 2, 1]);            // true
areArraysEqual([1, 2, 3, 4], [1, 1, 2, 3]);            // false
areArraysEqual([1, 1, 2, 2], [4, 2, 3, 1]);            // false
areArraysEqual([1, 1, 2], [1, 2, 2]);                  // false
areArraysEqual([1, 1, 1], [1, 1]);                     // false
areArraysEqual([1, 1], [1, 1]);                        // true
```

### Solution
```js
function areArraysEqual(array1, array2) {
  if (array1.length !== array2.length) return false;

  let array2Copy = array2.slice();

  for (let index = 0; index < array1.length; index++) {
    let matchingIndex = array2Copy.indexOf(array1[index]);
    if (matchingIndex === -1) return false;
    array2Copy.splice(matchingIndex, 1);
  }

  return true;
}

console.log(areArraysEqual([1, 2, 3], [1, 2, 3]) === true);
console.log(areArraysEqual([1, 2, 3], [3, 2, 1]) === true);
console.log(areArraysEqual(['a', 'b', 'c'], ['b', 'c', 'a']) === true);
console.log(areArraysEqual(['1', 2, 3], [1, 2, 3]) === false);
console.log(areArraysEqual([1, 1, 2, 3], [3, 1, 2, 1]) === true);
console.log(areArraysEqual([1, 2, 3, 4], [1, 1, 2, 3]) === false);
console.log(areArraysEqual([1, 1, 2, 2], [4, 2, 3, 1]) === false);
console.log(areArraysEqual([1, 1, 2], [1, 2, 2]) === false);
console.log(areArraysEqual([1, 1, 1], [1, 1]) === false);
console.log(areArraysEqual([1, 1], [1, 1]) === true);
```

# Objects
([Launch School Reference](https://launchschool.com/exercise_sets/7912d84c))

## Literal
([Launch School Reference](https://launchschool.com/exercises/5b3fd7f1))

Identify the bug in the following code. Don't run the code until after you've tried to answer.

```js
const myObject = {
  a: 'name',
  'b': 'test',
  123: 'c',
  1: 'd',
};

myObject[1];
myObject[a];
myObject.a;
```

### Solution
`myObject[a]` will raise a `ReferenceError` exception because this is expecting to return the value in the object with the key that matches the value of the variable `a`. Since the variable `a` has not been declared, a `ReferenceError` is raised.

To access the value of a property using bracket notation (e.g., `object[someKey]`), the operand inside the brackets that references the property name (key) must be a string value. If the operand is a number, JavaScript converts it to a string using the `Number.prototype.toString()` method. If the operand is a variable, JavaScript looks up the value (converting it to a string if necessary), then uses it as a key to get the corresponding property value. The expression `myObject[a]` raises a `ReferenceError` because JavaScript cannot find the value of the variable `a`, since it has not been declared.

## Literal Method
([Launch School Reference](https://launchschool.com/exercises/fe5b14b3))

In the following code, a user creates a `person` object literal and defines two methods for returning the person's first and last names. What is the result when the user tries out the code on the last line?

```js
const person = {
  firstName() {
    return 'Victor';
  },
  lastName() {
    return 'Reyes';
  },
};

console.log(`${person.firstName} ${person.lastName}`);
```

### Solution
```js
firstName() {
    return 'Victor';
  } lastName() {
    return 'Reyes';
  }
```

Just like functions, object literal methods must be called by appending parentheses (e.g., `person.firstName()`) in order to be executed. Functions are first class objects, so referencing the function name without the parentheses would return the function itself, not the return value of the function.

## Mutation
([Launch School Reference](https://launchschool.com/exercises/8765187b))

What will the following code log to the console and why? Don't run the code until after you have tried to answer.

```js
const array1 = ['Moe', 'Larry', 'Curly', 'Shemp', 'Harpo', 'Chico', 'Groucho', 'Zeppo'];
const array2 = [];

for (let i = 0; i < array1.length; i += 1) {
  array2.push(array1[i]);
}

for (let i = 0; i < array1.length; i += 1) {
  if (array1[i].startsWith('C')) {
    array1[i] = array1[i].toUpperCase();
  }
}

console.log(array2);
```

### Solution
This will output:

```js
['Moe', 'Larry', 'Curly', 'Shemp', 'Harpo', 'Chico', 'Groucho', 'Zeppo']
```

Even though `array1` is an object and is mutable, its elements are string primitives and are immutable. Since each string element of `array1` is pushed to `array2`, both arrays reference different string objects that happen to have the same values. Then, when we reassign some of the elements of `array1` and make them uppercase, this assigns yet another string object to the indexes of `array1` that get changed. Ultimately, the elements of `array2` are unaffected.

### Further Exploration 1
What would happen if an object literal was part of the `array1` elements pushed to `array2`? Would changes made to the object literal in `array1` be reflected in `array2`?

#### Solution
Yes. `array1` and `array2` would both reference the same object in memory, so if that object was mutated, the changes would be reflected in both arrays.

Example:
```js
const array1 = ['test', { a: 1, b: 2 }];
const array2 = [];

for (let i = 0; i < array1.length; i += 1) {
  array2.push(array1[i]);
}

console.log(array1);
console.log(array2);

array1[1].a = 3;
array1[0] = 'newString';

console.log(array1);
console.log(array2);
```

### Further Exploration 2
How would you change the code so that any changes made to `array1` in the second `for` loop get reflected to `array2`?

#### Solution
```js
const array1 = ['Moe', 'Larry', 'Curly', 'Shemp', 'Harpo', 'Chico', 'Groucho', 'Zeppo'];
const array2 = [];

for (let i = 0; i < array1.length; i += 1) {
  array2.push(array1[i]);
}

for (let i = 0; i < array1.length; i += 1) {
  if (array1[i].startsWith('C')) {
    array1[i] = array1[i].toUpperCase();
    array2[i] = array2[i].toUpperCase();
  }
}

console.log(array2);
```

#### Solution referencing same array
```js
const array1 = ['Moe', 'Larry', 'Curly', 'Shemp', 'Harpo', 'Chico', 'Groucho', 'Zeppo'];
const array2 = array1;

for (let i = 0; i < array1.length; i += 1) {
  if (array1[i].startsWith('C')) {
    array1[i] = array1[i].toUpperCase();
    array2[i] = array2[i].toUpperCase();
  }
}

console.log(array2);
```

## Dynamic
([Launch School Reference](https://launchschool.com/exercises/26fcee3b))

What will the following code log to the console and why? Don't run the code until after you have tried to answer.

```js
const myObject = {
  prop1: '123',
  prop2: '234',
  'prop 3': '345',
};

const prop2 = '456';
myObject['prop2'] = '456';
myObject[prop2] = '678';

console.log(myObject[prop2]);
console.log(myObject.prop2);
```

### Solution
The output will be:

```js
678
456
```

This demonstrates the creation of new properties by assigning a value to a key that doesn't previously exist within an object. It also shows that it's possible to use an expression to create and access properties.

The first assignment on line 8 `myObject['prop2'] = '456'` assigns the string `456` to the `prop2` key of `myObjecct`. The second assignment on line 9 `myObject[prop2] = '678'` creates a new key based on the value of the `prop2` variable declared on line 7 `456` and assigns to it the string `678`.

Then, when we log the value of the key that matches the value of the `prop2` variable declared on line 7, `678` is printed to the screen. Finally, we log the value of the `prop2` key in `myObject`, printing `456` to the screen.

### Further Exploration
Here is another example. What do you think will be logged to the console this time, and why?

```js
const myObj = {};
myObj[myFunc()] = 'hello, ';

function myFunc() {
  return 'funcProp';
}

console.log(myObj);
myObj[myFunc()] = 'world!';
console.log(myObj);
```

#### Solution
The output will be:

```js
{ funcProp: 'hello, ' }
{ funcProp: 'world!' }
```

On line 2, we create a new key with the return value of the `myFunc` function `funcProp` and assign its value to the string `hello, `. So, line 8 logs `hello, ` to the console.

Then, we reassign the value of the key that matches the `myFunc` return value `funcProp` to the string `world!`. Lin 10 logs `world!` to the console.

## Array Object Part 1
([Launch School Reference](https://launchschool.com/exercises/855a2927))


What will the following code log to the console and why? Don't run the code until after you have tried to answer.

```js
const myArray = ['a', 'b', 'c'];

console.log(myArray[0]);
console.log(myArray[-1]);

myArray[-1] = 'd';
myArray['e'] = 5;
myArray[3] = 'f';

console.log(myArray[-1]);
console.log(myArray['e']);
console.log(myArray);
```

### Solution
The output will be:

```js
a
undefined
d
5
[ 'a', 'b', 'c', 'f', '-1': 'd', e: 5 ]
```

On lines 6 and 7, we add properties to the array with `-1` and `e` keys respectively. Line 8 adds a new element to the array at index `3`.

## Array Object Part 2
([Launch School Reference](https://launchschool.com/exercises/70e15061))

A user wrote a function that takes an array as an argument and returns its average. Given the code below, the user expects the average function to return `5`. Is the user's expectation correct? Why or why not?

```js
const myArray = [5, 5];
myArray[-1] = 5;
myArray[-2] = 5;

function average(array) {
  let sum = 0;

  for (let i = -2; i < array.length; i += 1) {
    sum += array[i];
  }

  return sum / array.length;
}

average(myArray);
```

### Solution
The function will return `10`. Since the array's length only includes non-negative integer indexed elements, the array's length will be `2`, only counting the two `5` elements in `[5, 5]`. The loop then iterates four times, adding `5` to the `sum` each time for a total of `20`. Therefore, the expression in the `return` statement evaluates `20 / 10`, returning `10`.

### Further Exploration
Refactor the average function so that it returns the result that the user expected, `5`.

#### Solution using `Object.keys`
```js
const myArray = [5, 5];
myArray[-1] = 5;
myArray[-2] = 5;

function average(array) {
  let sum = 0;

  for (let i = -2; i < array.length; i += 1) {
    sum += array[i];
  }

  return sum / Object.keys(array).length;
}

average(myArray);
```

#### Solution (add `2` to `array.length`)
```js
const myArray = [5, 5];
myArray[-1] = 5;
myArray[-2] = 5;

function average(array) {
  let sum = 0;

  for (let i = -2; i < array.length; i += 1) {
    sum += array[i];
  }

  return sum / (array.length + 2);
}

average(myArray);
```

## What's my Bonus
([Launch School Reference](https://launchschool.com/exercises/0d6451f1))

The `calculateBonus` function calculates the bonus for a given salary. It makes use of two arguments for determining the bonus: a salary amount and a boolean switch. If the boolean is `true`, the bonus should be half of the salary; otherwise the bonus should be `0`. Fill in the blanks in the function so that it will work, then explain why it works.

Examples:

```js
function calculateBonus() {
  return _________[1] ? _________[0] / 2 : 0;
}

console.log(calculateBonus(2800, true) === 1400);
console.log(calculateBonus(1000, false) === 0);
console.log(calculateBonus(50000, true) === 25000);
```

### Solution
```js
function calculateBonus() {
  return arguments[1] ? arguments[0] / 2 : 0;
}

console.log(calculateBonus(2800, true) === 1400);
console.log(calculateBonus(1000, false) === 0);
console.log(calculateBonus(50000, true) === 25000);
```

Every function has an `arguments` object available locally that's similar to an array containing every argument passed into the function. In this example, we access each element of the `arguments` object.

## The End is Near But Not Here
([Launch School Reference](https://launchschool.com/exercises/9a7cb86f))

The `penultimate` function takes a string as an argument and returns the next-to-last word in the string. The function assumes that "words" are any sequence of non-whitespace characters. The function also assumes that the input string will always contain at least two words. The penultimate function in the example below does not return the expected result. Run the code below, check the current result, identify the problem, explain what the problem is, and provide a working solution.

Examples:

```
function penultimate(string) {
  return string.split(' ')[-2];
}

console.log(penultimate('last word') === 'last');
console.log(penultimate('Launch School is great!') === 'is');
```

### Solution
Using bracket notation with anything except a non-negative integer looks for a property within the array with the matching key, not an element of the array. In this case, JavaScript will return the value of the `'-2'` key. Therefore, the code will always output `undefined` because no array of words will have a `'-2'` key.

In order to fix this, we need to subtract the length of the array of words by `2`:

```js
function penultimate(string) {
  words = string.split(' ');
  return words[words.length - 2];
}

console.log(penultimate('last word') === 'last');
console.log(penultimate('Launch School is great!') === 'is');
```

Or, use `slice`:

```js
function penultimate(string) {
  return string.split(' ').slice(-2, -1)[0];
}
```

## After Midnight Part 1
([Launch School Reference](https://launchschool.com/exercises/d066934b))

We can use the number of minutes before or after midnight to represent the time of day. If the number of minutes is positive, the time is after midnight. If the number of minutes is negative, the time is before midnight.

The `timeOfDay` function shown below takes a time argument using this minute-based format, and returns the time of day in 24-hour format (`"hh:mm"`). Reimplement the function using JavaScript's `Date` object.

Examples:

```js
console.log(timeOfDay(0) === '00:00');
console.log(timeOfDay(-3) === '23:57');
console.log(timeOfDay(35) === '00:35');
console.log(timeOfDay(-1437) === '00:03');
console.log(timeOfDay(3000) === '02:00');
console.log(timeOfDay(800) === '13:20');
console.log(timeOfDay(-4231) === '01:29');
```

Note: Disregard Daylight Saving Time, Standard Time, and other complications.

```js
const MINUTES_PER_HOUR = 60;
const HOURS_PER_DAY = 24;
const MINUTES_PER_DAY = HOURS_PER_DAY * MINUTES_PER_HOUR;

function timeOfDay(deltaMinutes) {
  deltaMinutes = deltaMinutes % MINUTES_PER_DAY;
  if (deltaMinutes < 0) {
    deltaMinutes = MINUTES_PER_DAY + deltaMinutes;
  }

  let hours = Math.floor(deltaMinutes / MINUTES_PER_HOUR);
  let minutes = deltaMinutes % MINUTES_PER_HOUR;

  return `${padWithZeroes(hours, 2)}:${padWithZeroes(minutes, 2)}`;
}

function padWithZeroes(number, length) {
  let numberString = String(number);

  while (numberString.length < length) {
    numberString = `0${numberString}`;
  }

  return numberString;
}
```

### Solution
```js
function padWithZeros(number) {
  return number < 10 ? `0${number}` : number;
}

function timeOfDay(deltaMinutes) {
  date = new Date(2000, 0, 1, -5, deltaMinutes);

  hours = padWithZeros(date.getUTCHours());
  minutes = padWithZeros(date.getMinutes());

  return `${hours}:${minutes}`;
}


console.log(timeOfDay(0) === '00:00');
console.log(timeOfDay(-3) === '23:57');
console.log(timeOfDay(35) === '00:35');
console.log(timeOfDay(-1437) === '00:03');
console.log(timeOfDay(3000) === '02:00');
console.log(timeOfDay(800) === '13:20');
console.log(timeOfDay(-4231) === '01:29');
```

### Recommended Solution
```js
const MILLISECONDS_PER_MINUTE = 60000;

function timeOfDay(deltaMinutes) {
  const midnight = new Date('1/1/2000 00:00');
  const afterMidnight = new Date(midnight.getTime() + deltaMinutes * MILLISECONDS_PER_MINUTE);
  const hours = padWithZeroes(afterMidnight.getHours(), 2);
  const minutes = padWithZeroes(afterMidnight.getMinutes(), 2);

  return `${hours}:${minutes}`;
}

function padWithZeroes(number, length) {
  let numberString = String(number);

  while (numberString.length < length) {
    numberString = `0${numberString}`;
  }

  return numberString;
}
```

## After Midnight Part 2
([Launch School Reference](https://launchschool.com/exercises/05aadd94))


As seen in the previous exercise, the time of day can be represented as the number of minutes before or after midnight. If the number of minutes is positive, the time is after midnight. If the number of minutes is negative, the time is before midnight.

The two functions below do the reverse of the previous exercise. They take a 24-hour time argument and return the number of minutes before or after midnight, respectively. Both functions should return a value between `0` and `1439` (inclusive). Refactor the functions using the `Date` object.

Examples:

```js
console.log(afterMidnight('00:00') === 0);
console.log(beforeMidnight('00:00') === 0);
console.log(afterMidnight('12:34') === 754);
console.log(beforeMidnight('12:34') === 686);
```

Note: Disregard Daylight Saving Time, Standard Time, and other irregularities.

```js
const MINUTES_PER_HOUR = 60;
const HOURS_PER_DAY = 24;
const MINUTES_PER_DAY = HOURS_PER_DAY * MINUTES_PER_HOUR;

function afterMidnight(timeStr) {
  const timeComponents = timeStr.split(':');
  const hours = parseInt(timeComponents[0], 10);
  const minutes = parseInt(timeComponents[1], 10);

  return hours * MINUTES_PER_HOUR + minutes;
}

function beforeMidnight(timeStr) {
  let deltaMinutes = MINUTES_PER_DAY - afterMidnight(timeStr);
  if (deltaMinutes === MINUTES_PER_DAY) {
    deltaMinutes = 0;
  }

  return deltaMinutes;
}
```

### Solution
```js
const MILLISECONDS_PER_MINUTE = 60000;
const MINUTES_PER_DAY = 1440;

function afterMidnight(timeStr) {
  midnight = new Date(2000, 0, 1);
  date = new Date(`jan 1, 2000 ${timeStr}`);

  return (date - midnight) / MILLISECONDS_PER_MINUTE;
}

function beforeMidnight(timeStr) {
  if (timeStr === '00:00') return 0;
  return MINUTES_PER_DAY - afterMidnight(timeStr);
}

console.log(afterMidnight('00:00') === 0);
console.log(beforeMidnight('00:00') === 0);
console.log(afterMidnight('12:34') === 754);
console.log(beforeMidnight('12:34') === 686);
```

### Solution converting hours to minutes
```js
const MILLISECONDS_PER_MINUTE = 60000;
const MINUTES_PER_HOUR = 60;
const MINUTES_PER_DAY = 1440;

function afterMidnight(timeStr) {
  date = new Date(`jan 1, 2000 ${timeStr}`);
  return ((date.getHours()) * MINUTES_PER_HOUR) + date.getMinutes();
}

function beforeMidnight(timeStr) {
  if (timeStr === '00:00') return 0;
  return MINUTES_PER_DAY - afterMidnight(timeStr);
}

console.log(afterMidnight('00:00') === 0);
console.log(beforeMidnight('00:00') === 0);
console.log(afterMidnight('12:34') === 754);
console.log(beforeMidnight('12:34') === 686);
```

# Medium 1
([Launch School Reference](https://launchschool.com/exercise_sets/41f68c21))

## Logical Operation
([Launch School Reference](https://launchschool.com/exercises/6f50a742))

What will each line of the following code return? Don't run the code until after you have tried to answer.

```js
console.log(false && undefined);
console.log(false || undefined);
console.log((false && undefined) || (false || undefined));
console.log((false || undefined) || (false && undefined));
console.log((false && undefined) && (false || undefined));
console.log((false || undefined) && (false && undefined));
console.log('a' || (false && undefined) || '');
console.log((false && undefined) || 'a' || '');
console.log('a' && (false || undefined) && '');
console.log((false || undefined) && 'a' && '');
```

### Solution
```
false
undefined
undefined
false
false
undefined
a
a
undefined
undefined
```

The return value of a logical expression can be determined by iteratively evaluating the expression from left to right until it results in a single value. For example:

```js
((false && undefined) || 'a' || '');
(false || 'a' || '');
('a' || '');
('a');

------

('a' && (false || undefined) && '');
('a' && undefined && '');
(undefined && '');
(undefined);

------

((false || undefined) || (false && undefined));
(undefined || false);
(false);
```

## Conditional Loop
([Launch School Reference](https://launchschool.com/exercises/c87f01cc))

The following program is expected to log each number between `0` and `9` (inclusive) that is a multiple of `3`. Read through the code shown below. Will it produce the expected result? Why or why not? If not, how can it be fixed?

```js
let i = 0;
while (i < 10) {
  if (i % 3 === 0) {
    console.log(i);
  } else {
    i += 1;
  }
}
```

### Solution
This will cause an infinite loop, logging `0` to the console each time. This is because `i` does not get incremented and never changes from `0`. The conditional `i % 3 === 0` always evaluates to `true` so the `else` clause is never executed. This can be fixed with by moving the code that increments `i` outside of the `if` statement:

```js
let i = 0;
while (i < 10) {
  if (i % 3 === 0) {
    console.log(i);
  }
  i += 1;
}
```

## Multiplication Table
([Launch School Reference](https://launchschool.com/exercises/e22c8342))

The following program is expected to log a multiplication table for the numbers from `1` to `10` to the console. Read through the code shown below. Will it produce the expected result? Why or why not? If not, how can it be fixed?

```js
function padLeft(number) {
  const stringNumber = String(number);
  switch (stringNumber.length) {
    case 1:  return `  ${stringNumber}`;
    case 2:  return ` ${stringNumber}`;
    default: return stringNumber;
  }
}

for (let i = 1; i < 10; i += 1) {
  let row = '';
  for (let j = 1; j <= 10; j += 1) {
    row += `${padLeft(i * j)} `;
  }

  console.log(row);
}
```

### Solution
This does not produce the expected result. This code only generates the table for numbers between `1` and `9`. The first `for` loop stops iterating when `i` is `10`, so the 10th row is never logged to the console. To fix this, we can change `i < 10` to `i <= 10`:

```js
for (let i = 1; i <= 10; i += 1) {
  let row = '';
  for (let j = 1; j <= 10; j += 1) {
    row += `${padLeft(i * j)} `;
  }

  console.log(row);
}
```

## Selected Columns
([Launch School Reference](https://launchschool.com/exercises/008dee74))

The `getSelectedColumns` function selects and extracts specific columns to a new array. Currently, the function is not producing the expected result. Go over the function and the sample runs shown below. What do you think the problem is?

```js
function getSelectedColumns(numbers, cols) {
  var result = [];

  for (var i = 0, length = numbers.length; i < length; i += 1) {
    for (var j = 0, length = cols.length; j < length; j += 1) {
      if (!result[j]) {
        result[j] = [];
      }

      result[j][i] = numbers[i][cols[j]];
    }
  }

  return result;
}

// given the following arrays of number arrays
const array1 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
const array2 = [[1, 2, 3], [1, 2, 3], [1, 2, 3]];

// `array1` in row/column format
// [[1, 2, 3],
//  [4, 5, 6],
//  [7, 8, 9]]

getSelectedColumns(array1, [0]);     // [[1]];            expected: [[1, 4, 7]]
getSelectedColumns(array1, [0, 2]);  // [[1, 4], [3, 6]]; expected: [[1, 4, 7], [3, 6, 9]]
getSelectedColumns(array2, [1, 2]);  // [[2, 2], [3, 3]]; expected: [[2, 2, 2], [3, 3, 3]]
```

### Solution
The problem is that the variable `length` is reassigned to a new variable in the second loop. Declaring `length` with the `var` keyword gives it function scope. So, instead of having two different `length` variables in each `for` loop scope, we have only one `length` variable for the entire function. We can fix this by using `let` instead of `var`:

```js
function getSelectedColumns(numbers, cols) {
  var result = [];

  for (let i = 0, length = numbers.length; i < length; i += 1) {
    for (let j = 0, length = cols.length; j < length; j += 1) {
      if (!result[j]) {
        result[j] = [];
      }

      result[j][i] = numbers[i][cols[j]];
    }
  }

  return result;
}
```

## Counter
([Launch School Reference](https://launchschool.com/exercises/75bff2a0))

What will the following code snippets log?

### Code Snippet 1
```js
var counter = 5;
var rate = 3;
console.log('The total value is ' + String(counter * rate));

function counter(count) {
  // ...
}
```

#### Solution
This will output:

```
The total value is 15
```

After hoisting, this is equivalent to:

```js
function counter(count) {
  // ...
}

var rate;

counter = 5;
rate = 3;

console.log('The total value is ' + String(counter * rate));
```

During the creation phase, JavaScript hoists functions before variables. When `var` variables are hoisted, JavaScript gives them an initial value of `undefined`. Then, during the execution phase, JavaScript assigns `5` to `counter` and `3` to `rate`. The expression that is concatenated to the string within the `console.log` statement evaluates to `15`.


### Code Snippet 2
```js
function counter(count) {
  // ...
}

console.log('The total value is ' + String(counter * rate));

var counter = 5;
var rate = 3;
```

#### Solution
This will output:

```
The total value is NaN
```

After hoisting, this is equivalent to:
```js
function counter(count) {
  // ...
}

var rate;

console.log('The total value is ' + String(counter * rate));

var counter = 5;
var rate = 3;
```

During the creation phase, JavaScript hoists functions before variables. In this case, when we declare the `counter` variable using `var`, JavaScript ignores this declaration because the `counter` variable has already been declared. JavaScript hoists the variables but does not assign values to the variables, so `rate` is hoisted but is assigned to `undefined`. When the `console.log` statement is executed in the execution phase, it happens before the `counter` and `rate` variables have been reassigned.

At the beginning of the execution phase, the variable `rate` is `undefined` while the variable `counter` is assigned to a function that returns `undefined`. The expression that gets concatenated to the string within the `console.log` statement is evaluated to `[Function: counter] * undefined`.

When two non-integer operands are multiplied, each operand is implicitly coerced into a number. For the `counter` variable that refers to a function, when we convert a function to a number, it returns `NaN`. For the `rate` variable, when `undefined` is converted to a number, JavaScript returns `NaN`. So, the expression that  `NaN * NaN` which returns `NaN`.

### Code Snippet 3
```js
var counter = 5;
var rate = 3;

function counter(count) {
  // ...
}

console.log('The total value is ' + String(counter * rate));
```

#### Solution
This will output:

```
The total value is 15
```

After hoisting, this is equivalent to:
```js
function counter(count) {
  // ...
}

var rate;

counter = 5;
rate = 3;

console.log('The total value is ' + String(counter * rate));
```

In this case, during the creation phase, JavaScript declares the `counter` and `rate` variables. `counter` references the function and `rate` defaults to `undefined`. When `var` variables are hoisted, JavaScript gives them an initial value of `undefined`.

Then, during the execution phase, JavaScript assigns `5` to `counter` and `3` to `rate`. Finally, the expression that's concatenated to the string in the `console.log` statement evaluates to `15`.

### Code Snippet 4
```js
let counter = 5;
let rate = 3;

function counter(count) {
  // ...
}

console.log('The total value is ' + String(counter * rate));
```

#### Solution
This will raise a `SyntaxError` because the `counter` variable already exists when we attempt to declare it again when by defining the function. Syntax errors generally occur before the creation phase, so this code is never hoisted.

### Recommended Solution
The fundamental concepts exemplified in this exercise are **hoisting**, **variable declarations**, and **function declarations**.

The key thing to note about **function declarations** is that, like variable declarations, they result in the creation of a variable. A function declaration creates a variable with the same name as the function name.

Recall that **hoisting** works differently depending on the type of declaration. With function declarations, both the function name and body are hoisted, whereas, with variable declarations, only the variable name is hoisted but not the assignment.

Finally, although function and variable **declarations** are both hoisted, function declarations are hoisted first; any succeeding variable declarations with the same name (e.g., `counter`) are seen as duplicates. With `var` declarations these duplicates are ignored, but `let` declarations work a little differently.

Notice that after **hoisting**, the first and third code snippets are effectively the same. Notice also, that in the first three code snippets, the variable declaration for `counter` is effectively overwritten by the function declaration with the same name.

The differentiating factor for the second code snippet is the position of the call to `console.log` relative to the variable assignments; since the assignments happen after the `console.log` call, the expression `counter * rate` evaluates to `NaN`.

You can't declare a variable multiple times when one or more of those declarations use `let` or `const`. Since snippet 4 declares `counter` using `let`, it raises a `SyntaxError`. However, the error occurs on line 4 -- the function declaration -- from the original code. Since `SyntaxErrors` usually occur during the creation phase, hoisting has no direct effect on the behavior. Therefore, we have omitted the hoisted code snippet for snippet 4. The syntax error will occur before hoisting takes place.

## Logger
([Launch School Reference](https://launchschool.com/exercises/6cf60443))

Read through the following code. Why will it log 'debugging'?

```js
function debugIt() {
  const status = 'debugging';
  function logger() {
    console.log(status);
  }

  logger();
}

debugIt();
```

### Solution
On line 4, the `status` variable has a value of `'debugging'` because of JavaScript's lexical scoping rules.

The `debugIt` function defines a local variable named `status` and a function named `logger`. `logger` is an inner (nested) function, so it has access to any variables declared in the scope of its outer (parent) function, `debugIt`, due to lexical scoping rules.

## Invoice
([Launch School Reference](https://launchschool.com/exercises/625d5fdb))

The `invoiceTotal` function in the code below computes the total amount for an invoice containing four "line items". How can you refactor the function so that it will work with invoices containing any number of line items?

```js
function invoiceTotal(amount1, amount2, amount3, amount4) {
  return amount1 + amount2 + amount3 + amount4;
}

invoiceTotal(20, 30, 40, 50);          // works as expected
invoiceTotal(20, 30, 40, 50, 40, 40);  // does not work; how can you make it work?
```

### Solution
```js
function invoiceTotal(...args) {
  return args.reduce((prev, cur) => prev + cur);
}

// or ...

function invoiceTotal(...args) {
  let result = 0;

  args.forEach(arg => result += arg);

  return result;
}


console.log(invoiceTotal(20, 30, 40, 50) === 140);          // works as expected
console.log(invoiceTotal(20, 30, 40, 50, 40, 40) === 220);  // does not work; how can you make it work?
```

## Product of Sums
([Launch School Reference](https://launchschool.com/exercises/e02467c0))

The `productOfSums` function shown below is expected to return the product of the sums of two arrays of numbers. Read through the following code. Will it produce the expected result? Why or why not? If not, how can it be fixed?

```js
function productOfSums(array1, array2) {
  let result = total(array1) * total(array2);
  return result;
}

function total(numbers) {
  let sum;

  for (let i = 0; i < numbers.length; i += 1) {
    sum += numbers[i];
  }

  sum;
}
```

### Solution
This will not work because the return value for the `total` function will always be `undefined` since there is no explicit `return` statement. `result` will always be `undefined * undefined`, which evaluates to `NaN`.

Also, since the `sum` variable is declared but not initialized to any value, it is initialized to `undefined` by default. When we try to add each number in each array to the value of `sum`, we are adding `undefined` to a number (`undefined + numbers[i]`), which evaluates to `NaN`.

To fix this, return the value of `sum` from the `total` function.

# Medium 2
([Launch School Reference](https://launchschool.com/exercise_sets/646ece8b))

## Defaults
([Launch School Reference](https://launchschool.com/exercises/84836620))

The `processOrder` function shown below accepts the following arguments: `price`, `quantity`, `discount`, `serviceCharge` and `tax`. Any arguments other than `price` may be omitted when calling the function, in which case, default values will be assigned.

```js
function processOrder(price, quantity, discount, serviceCharge, tax) {
  if (!quantity) {
    quantity = 1;
  }

  if (!discount) {
    discount = 0;
  }

  if (!serviceCharge) {
    serviceCharge = 0.1;
  }

  if (!tax) {
    tax = 0.15;
  }

  return (price * quantity) * (1 - discount) * (1 + serviceCharge) * (1 + tax);
}

processOrder(100);      // 126.5
```

This function uses conditional statements to test whether arguments have been omitted. When an argument is omitted, JavaScript automatically initializes it to a value of `undefined`. The function takes advantage of this behavior by setting `undefined` arguments to a default value.

The following variation of the `processOrder` function has the same behavior as the first:

```js
function processOrder(price, quantity, discount, serviceCharge, tax) {
  quantity = quantity || 1;
  discount = discount || 0;
  serviceCharge = serviceCharge || 0.1;
  tax = tax || 0.15;

  return (price * quantity) * (1 - discount) * (1 + serviceCharge) * (1 + tax);
}
```

However, both of these solutions have a limitation that can lead to an incorrect result for certain inputs. What is this limitation and how does it affect the result?

### Solution
The problem is that if we want some arguments to be zero, the code will change those arguments into their defaults instead of accepting the argument as zero. For example, if `quantity` is set to zero, the code will change it to `1`. To fix this, we can check to see if the argument is `undefined` before assigning a default:

```js
function processOrder(price, quantity, discount, serviceCharge, tax) {
  quantity = quantity === undefined ? 1 : quantity;
  discount = discount === undefined ? 0 : discount;
  serviceCharge = serviceCharge === undefined ? 0.1 : serviceCharge;
  tax = tax === undefined ? 0.15 : tax;

  return Number(((price * quantity) * (1 - discount) * (1 + serviceCharge) * (1 + tax)).toFixed(2));
}
```

## Equal
([Launch School Reference](https://launchschool.com/exercises/7e01ecdd))

Read through the following code. Currently, it does not log the expected result. Explain why this happens, then refactor the code so that it works as expected.

```js
const person = { name: 'Victor' };
const otherPerson = { name: 'Victor' };

console.log(person === otherPerson);    // false -- expected: true
```

### Solution
When we use strict equality to compare two objects, both objects must be the same object in memory in order for strict equality to return `true`. In this case, `person` points to one object with `name: 'Victor'` as its key/value pair while `otherPerson` points to another object with `name: 'Victor'` as its key/value pair. These are two different objects.

To fix this, we can assign `otherPerson` to point to the `person` object, if we want them both to reference the same object. Or, if we want to check if the value of the `name` properties are the same, we can compare the `name` properties of both objects:

```js
const person = { name: 'Victor' };
const otherPerson = person;

console.log(person === otherPerson);
```

```js
const person = { name: 'Victor' };
const otherPerson = { name: 'Victor' };

console.log(person.name === otherPerson.name);
```

## Amount Payable
([Launch School Reference](https://launchschool.com/exercises/e6df5e60))

What does the following code log? Why is this so?

```js
let startingBalance = 1;
const chicken = 5;
const chickenQuantity = 7;

function totalPayable(item, quantity) {
  return startingBalance + (item * quantity);
}

startingBalance = 5;
console.log(totalPayable(chicken, chickenQuantity));

startingBalance = 10;
console.log(totalPayable(chicken, chickenQuantity));
```

### Solution
This will output:

```
40
45
```

This is an example of a closure. When we define `totalPayable`, this function retains access to variables defined in the global scope. So, when those variables are reassigned, the `totalPayable` function can detect those changes and update appropriately. In this case, `startingBalance` is reassigned to `5` before the first `console.log`, logging `40` and then reassigned to `10` before the second `console.log`, logging `45`.

## Caller
([Launch School Reference](https://launchschool.com/exercises/4b0d0727))

The `doubler` function in the code below takes two arguments: a `number` to double and return, and a string containing the name of the function's `caller`.

```js
function doubler(number, caller) {
  console.log(`This function was called by ${caller}.`);
  return number + number;
}

doubler(5, 'Victor');                   // returns 10
// logs:
// This function was called by Victor.
```

Write a `makeDoubler` function that takes a `caller` name as an argument, and returns a function that has the same behavior as `doubler`, but with a preset `caller`.

Example:

```js
const doubler = makeDoubler('Victor');
doubler(5);                             // returns 10
// logs:
// This function was called by Victor.
```

### Solution
```js
function makeDoubler(caller) {
  return function (number) {
    console.log(`This function was called by ${caller}.`);
    return number + number;
  }
}
```

This solution leverages that functions in JavaScript are first-class. The returned anonymous function expression assigned to the `doubler` variable still retains access to the `caller` variable in its closure, even after the `makeDoubler` function returns.

## What's My Value?
([Launch School Reference](https://launchschool.com/exercises/4cdaf382))

What will the following program log to the console? Can you explain why?

```js
const array = ['Apples', 'Peaches', 'Grapes'];

array[3.4] = 'Oranges';
console.log(array.length);
console.log(Object.keys(array).length);

array[-2] = 'Watermelon';
console.log(array.length);
console.log(Object.keys(array).length);
```

### Solution
This will output:

```
3
4
3
5
```

The length property doesn't include array properties in its value. It only looks at array elements that have a non-negative integer index. So, for both `console.log` statements where we log the value of the `length` property, the last index plus `1` is returned.

`Object.keys` returns an array of all keys: non-negative integer index keys as well as all others. So, when we add the `3.4` property, this adds a key to the array, so `Object.keys` returns `4` keys. When we add the `-2` property, this adds another key to the array, so `Object.keys` now returns `5` keys.

## Length
([Launch School Reference](https://launchschool.com/exercises/879c5e23))

Read through the code below. What values will be logged to the console? Can you explain these results?

```js
const languages = ['JavaScript', 'Ruby', 'Python'];
console.log(languages);
console.log(languages.length);

languages.length = 4;
console.log(languages);
console.log(languages.length);

languages.length = 1;
console.log(languages);
console.log(languages.length);

languages.length = 3;
console.log(languages);
console.log(languages.length);

languages.length = 1;
languages[2] = 'Python';
console.log(languages);
console.log(languages[1]);
console.log(languages.length);
```

### Solution
```js
const languages = ['JavaScript', 'Ruby', 'Python'];
console.log(languages);             // [ 'JavaScript', 'Ruby', 'Python' ]
console.log(languages.length);      // 3

languages.length = 4;
console.log(languages);             // [ 'JavaScript', 'Ruby', 'Python', <1 empty item> ]
console.log(languages.length);      // 4

languages.length = 1;
console.log(languages);             // [ 'JavaScript' ]
console.log(languages.length);      // 1

languages.length = 3;
console.log(languages);             // [ 'JavaScript', <2 empty items> ]
console.log(languages.length);      // 3

languages.length = 1;
languages[2] = 'Python';
console.log(languages);             // [ 'JavaScript', <1 empty item>, 'Python' ]
console.log(languages[1]);          // undefined
console.log(languages.length);      // 3
```

Setting the length of the array so that it is less than the current length, the array is truncated and any values past the new length are removed. Setting the length of the array so that it is greater than the current length adds empty items to the array. If an element is added at an index that's past the last existing index, any indexes between are added as `<empty items>` and the length of the array is set to the last index plus `1`. When the value of an `<empty item>` is returned, it is `undefined`.

Arrays with empty slots are also known as sparse arrays.

## The Red Pill
([Launch School Reference](https://launchschool.com/exercises/e61d13f8))

Read through the code below and determine what will be logged. You may refer to the [ASCII Table](http://www.ascii-code.com/) to look up character code values.

```js
function one() {
  function log(result) {
    console.log(result);
  }

  function anotherOne(...args) {
    let result = '';
    for (let i = 0; i < args.length; i += 1) {
      result += String.fromCharCode(args[i]);
    }

    log(result);
  }

  function anotherAnotherOne() {
    console.log(String.fromCharCode(87, 101, 108, 99, 111, 109, 101));
    anotherOne(116, 111);
  }

  anotherAnotherOne();
  anotherOne(116, 104, 101);
  return anotherOne;
}

one()(77, 97, 116, 114, 105, 120, 33);
```

### Solution
This will output:

```
Welcome
to
the
Matrix!
```

This code has a total of eight function calls:

```
1: one();
2: anotherAnotherOne();                         // Welcome
3: anotherOne(116, 111);
4: log(result);                                 // to
5: anotherOne(116, 104, 101);
6: log(result);                                 // the
7: anotherOne(77, 97, 116, 114, 105, 120, 33);
8: log(result);                                 // Matrix!
```

# Debugging
([Launch School Reference](https://launchschool.com/exercise_sets/cefefb80))

## Hello Friends!
([Launch School Reference](https://launchschool.com/exercises/0cedcb76))

You have written basic functions to display a random greeting to any number of friends upon each invocation of greet. Upon invoking the greet function, however, the output is not as expected. Figure out why not and fix the code.

```js
function randomGreeting() {
  const words = ['Hello', 'Howdy', 'Hi', 'Hey there', 'What\'s up',
               'Greetings', 'Salutations', 'Good to see you'];

  const idx = Math.floor(Math.random() * words.length);

  words[idx];
}

function greet(...args) {
  const names = Array.prototype.slice.call(args);

  for (let i = 0; i < names.length; i++) {
    const name = names[i];
    const greeting = randomGreeting;

    console.log(`${greeting}, ${name}!`);
  }
}

greet('Hannah', 'Jose', 'Beatrix', 'Julie', 'Ian');
```

### Solution
There is no `return` statement in the `randomGreeting` method definition, so `undefined` is always returned. Also, on line 17, we're interpolating `greeting` as a function value, rather than calling the function with `greeting()`.

```js
function randomGreeting() {
  const words = ['Hello', 'Howdy', 'Hi', 'Hey there', 'What\'s up',
               'Greetings', 'Salutations', 'Good to see you'];

  const idx = Math.floor(Math.random() * words.length);

  return words[idx];
}

function greet(...args) {
  const names = Array.prototype.slice.call(args);

  for (let i = 0; i < names.length; i++) {
    const name = names[i];
    const greeting = randomGreeting;

    console.log(`${greeting()}, ${name}!`);
  }
```

## Includes False
([Launch School Reference](https://launchschool.com/exercises/58d819c0))

Caroline has written a very simple function, `includesFalse`, that searches a list of primitives for the boolean `false`. If `false` is found, the function immediately returns `true`. If no occurrence of `false` has been found after iterating through the entire array, the function returns `false`.

Caroline's last method invocation of `includesFalse` (line 15) doesn't return what she expects. Why is that? Fix the code so that it behaves as intended.

```js
function includesFalse(list) {
  for (let i = 0; i < list.length; i++) {
    let element = list[i];

    if (element == false) {
      return true;
    }
  }

  return false;
}
                                                                  // returns:
includesFalse([8, null, 'abc', true, 'launch', 11, false]);       // true
includesFalse(['programming', undefined, 37, 64, true, 'false']); // false
includesFalse([9422, 'lambda', true, 0, '54', null]);             // true
```

### Solution
When comparing elements with the abstract equality operator (`==`) and one operand is a boolean, the boolean is coerced into a number: `1` for `true` and `0` for `false`. So, on line 5, when we compare each element to `false` in the expression `element == false` with non-strict equality, for non-boolean values, it's as if we're comparing the element to `0`. So in that last example, `0 == false` will evaluate to `0 == 0`, which is `true`. To fix this, we should use strict equality, `element === false`. With strict equality, the expression will only return `true` if both operands are the same type.

## Place A Bet
([Launch School Reference](https://launchschool.com/exercises/d9a41e96))

The function `placeABet` below accepts a guess from the user between 1 and 25. The function should determine a winning number and return a message to the user indicating whether he/she entered a winning guess. When you try to invoke `placeABet`, an error is raised. Fix the bug and explain what caused it.

```js
const readlineSync = require('readline-sync');

function placeABet(guess) {
  (function generateRandomInt() {
    return Math.floor(Math.random() * 25) + 1;
  });

  const winningNumber = generateRandomInt();

  if (guess < 1 || guess > 25) {
    return 'Invalid guess. Valid guesses are between 1 and 25.';
  }

  if (guess === winningNumber) {
    return "Congratulations, you win!";
  } else {
    return "Wrong-o! You lose.";
  }
}

const userGuess = parseInt(readlineSync.question('Input a guess between 1-25'), 10);
console.log(placeABet(userGuess));
```

### Solution
A `ReferenceError` is thrown, `generateRandomInt is not defined`. This is because there are parenthesis around the `generateRandomInt` function, making it a function expression instead of a function declaration. Even though the function expression is named, that name is only available from within the function. There are two ways to fix this. One, is to remove the parenthesis to make it a function declaration:

```js
  function generateRandomInt() {
    return Math.floor(Math.random() * 25) + 1;
  };
```

The other is to assign the expression to a variable named `generateRandomInt` and make it a named function expression:

```js
  const generateRandomInt = (function generateRandomInt() {
    return Math.floor(Math.random() * 25) + 1;
  });
```

## Picky Museum Filter
([Launch School Reference](https://launchschool.com/exercises/ca42a439))

We love to visit museums if they are about science or computers. We're undecided when it comes to modern art, and would rather not go in most cases. However, we're willing to go to any modern art museum that is about Andy Warhol (we like him!) or that is located in nearby Amsterdam. We'd rather skip any other museums.

We tried to implement these preferences in a function, so we can automatically sort through long lists of museums and find the ones that sound interesting. However, our Boolean check is flawed, as it fails some of our test cases. Can you fix it?

```js
function wantToVisit(museum, city) {
  return museum.includes('Computer')
      || museum.includes('Science')
      && !(
        museum.includes('Modern')
        && museum.includes('Art')
        && museum.includes('Andy Warhol')
        || city === 'Amsterdam'
      );
}

// Tests (should all print 'true')

console.log(wantToVisit('Computer Games Museum', 'Berlin') === true);
console.log(wantToVisit('National Museum of Nature and Science', 'Tokyo') === true);
console.log(wantToVisit('Museum of Modern Art', 'New York') === false);
console.log(wantToVisit('El Paso Museum of Archaeology', 'El Paso') === false);
console.log(wantToVisit('NEMO Science Museum', 'Amsterdam') === true);
console.log(wantToVisit('National Museum of Modern Art', 'Paris') === false);
console.log(wantToVisit('Andy Warhol Museum of Modern Art', 'Medzilaborce') === true);
console.log(wantToVisit('Moco: Modern Contemporary Art', 'Amsterdam') === true);
console.log(wantToVisit('Van Gogh Museum', 'Amsterdam') === false);
console.log(wantToVisit('Andy Warhol Museum', 'Melbourne') === false);
```

### Solution
```js
function wantToVisit(museum, city) {
  return (museum.includes('Science') || museum.includes('Computer'))
    || ((museum.includes('Modern') && museum.includes('Art'))
      && (museum.includes('Andy Warhol') || city === 'Amsterdam'));
}
```

### Solution rewritten to be more readable
```js
function wantToVisit(museum, city) {
  function contains(string, substring) {
    return string.toLowerCase().match(substring.toLowerCase()) != null;
  }

  const aboutComputers = contains(museum, 'Computer');
  const aboutScience = contains(museum, 'Science');
  const aboutModernArt = contains(museum, 'Modern') && contains(museum, 'Art');
  const aboutAndyWarhol = contains(museum, 'Andy Warhol');
  const inAmsterdam = (city === 'Amsterdam');

  const mustGo =  aboutComputers || aboutScience;
  const worthAnException = aboutModernArt && (aboutAndyWarhol || inAmsterdam);
  return mustGo || worthAnException;
}
```

## Shop Transactions
([Launch School Reference](https://launchschool.com/exercises/f4a6dcbd))

Todd wrote some simple code in an attempt to log his shop's financial transactions. Each time he makes a sale or spends money on inventory, he logs that deposit or withdrawal via the `logTransaction` function. His code also intends to ensure that each transaction logged is a valid numerical amount. At the end of each day, he displays his total gain or loss for the day with `transactionTotal`.

Test out the code yourself. Can you spot the problem and fix it?

```js
const readlineSync = require('readline-sync');
const transactionLog = [];

function processInput(input) {
  const numericalData = parseFloat(input);

  if (Number.isNaN(numericalData)) {
    throw (new Error('Data could not be converted to numerical amount.'));
  }

  return numericalData;
}

function logTransaction() {
  let data = readlineSync.question('Please enter the transaction amount: ');

  try {
    data = processInput(data);
    transactionLog.push(data);

    console.log('Thank you. Data accepted.');
  } catch {
    console.log(error.message);
  }
}

function transactionTotal() {
  let total = 0;

  for (let i = 0; i < transactionLog.length; i++) {
    total += transactionLog[i];
  }

  return total;
}

logTransaction();
logTransaction();
logTransaction();

console.log(transactionTotal());
```

### Solution
We don't set the `catch` block correctly. It should be:

```js
  } catch (error) {
    console.log(error.message);
  }
```

### Solution that's simpler without `try/catch`
```js
const readlineSync = require('readline-sync');
const transactionLog = [];

function processInput(input) {
  return parseFloat(input);
}

function logTransaction() {
  let data = readlineSync.question('Please enter the transaction amount: ');

  data = processInput(data);

  if (data) {
    transactionLog.push(data);
    console.log('Thank you. Data accepted.');
  } else {
    console.log('Data could not be converted to numerical amount.')
  }
}

function transactionTotal() {
  return transactionLog.reduce((acc, cur) => acc + cur);
}

logTransaction();
logTransaction();
logTransaction();

console.log(transactionTotal());
```

## Full Moon
([Launch School Reference](https://launchschool.com/exercises/6d9a367b))

Run the following code. Why is every warning displayed twice? Change the code so that each warning is displayed only once, as intended.

```js
const species = ['wolf', 'human', 'wasp', 'squirrel', 'weasel', 'dinosaur'];
const isMidnight = true;
const isFullmoon = true;

function isTransformable(species) {
  return species[0] === 'w';
}

function transform(species) {
  return `were${species}`;
}

for (let index = 0; index < species.length; index++) {
  const thisSpecies = species[index];
  var newSpecies;

  if (isMidnight && isFullmoon && isTransformable(thisSpecies)) {
    newSpecies = transform(thisSpecies);
  }

  if (newSpecies) {
    console.log(`Beware of the ${newSpecies}!`);
  }
}
```

### Solution
Using `var` to declare the `newSpecies` variable makes it globally accessible because variables declared with `var` have function scope and in this case, the function is the global scope. So, after we check the first element and set `newSpecies` to something other than `undefined`, this carries over until it is reassigned. To fix this, we can use `let` instead of `var` so the variable will have block scope:

```js
let newSpecies;
```

## Space Design
([Launch School Reference](https://launchschool.com/exercises/e370c17e))

We're putting together some information about our new company Space Design. Not all roles have been filled yet. But although we have a CEO and Scrum Master, displaying them shows `undefined`. Why is that?

```js
// Roles (salary still to be determined)

const ceo = {
  tasks: ['company strategy', 'resource allocation', 'performance monitoring'],
  salary: 0,
};

const developer = {
  tasks: ['turn product vision into code'],
  salary: 0,
};

const scrumMaster = {
  tasks: ['organize scrum process', 'manage scrum teams'],
  salary: 0,
};

// Team -- we're hiring!

const team = {};

team[ceo] = 'Kim';
team[scrumMaster] = 'Alice';
team[developer] = undefined;

const company = {
  name: 'Space Design',
  team,
  projectedRevenue: 500000,
};

console.log(`----{ ${company.name} }----`);
console.log(`CEO: ${company.team[ceo]}`);
console.log(`Scrum master: ${company.team[scrumMaster]}`);
console.log(`Projected revenue: $${company.projectedRevenue}`);

// ----{ Space Design }----
// CEO: undefined
// Scrum master: undefined
// Projected revenue: $500000
```

### Solution
Most relevant here is that when we use bracket notation to assign or access an object property, the expression inside the brackets must be a string value; if it is not, JavaScript will convert it into one. Our original code unintentionally references the variables `ceo`, `scrumMaster`, and `developer` rather than passing in strings. JavaScript looks up the values assigned to those variables, and since the values are objects, converts them to strings. The string representation of all three values is `[object Object]`, so on lines 22-24 we were actually repeatedly re-assigning the value of `team['[object Object]']`. The last assignment, on line 24, assigned it to undefined. As a result, the value of team ended up as follows:

```js
{ '[object Object]': undefined }
```

## Expensive Study Preparation
([Launch School Reference](https://launchschool.com/exercises/13d2a4a8))

We make a few purchases to prepare for our study session, but the amount charged upon checkout seems too high. Run the following code and find out why we are charged incorrectly.

```js
// The shopping cart is a list of items, where each item
// is an object { name: <string>, amount: <number> }.
let shoppingCart = [];

// Currently available products with prices.
const prices = {
  'notebook': 9.99,
  'pencil': 1.70,
  'coffee': 3.00,
  'smoothie': 2.10,
};

function price({name}) {
  console.log(name);
  return prices[name];
}

// Adding an item to the shopping cart.
// The amount is optional and defaults to 1.
// If the item is already in the cart, its amount is updated.
function updateCart(name, amount) {
  amount = amount || 1;

  let alreadyInCart = false;

  for (let i = 0; i < shoppingCart.length; i += 1) {
    let item = shoppingCart[i];

    if (item.name === name) {
      item.amount = amount;
      alreadyInCart = true;
      break;
    }
  }

  if (!alreadyInCart) {
    shoppingCart.push({ name, amount });
  }
}

// Calculating the price for the whole shopping cart.
function total() {
  let total = 0;

  for (let i = 0; i < shoppingCart.length; i += 1) {
    let item = shoppingCart[i];

    total += (item.amount * price(item));
  }

  return total.toFixed(2);
}

function pay() {
  // omitted

  console.log(`You have been charged $${total()}.`);
}

function checkout() {
  pay();
  shoppingCart = [];
}

// Example purchase.

updateCart('notebook');
updateCart('pencil', 2);
updateCart('coffee', 1);
// "Oh, wait, I do have pencils..."
updateCart('pencil', 0);

checkout();
// You have been charged $14.69.
```

### Solution
In JavaScript, `0` is falsey. So, when we check for an `amount` value on line 21 `amount = amount || 1`, when we zero out the amount of something, as we do on line 70, `amount` will be set to `1` instead of `0`. To fix this, we can use a default parameter value:

```js
function updateCart(name, amount = 1) {
```

## Stuck on Page 18
([Launch School Reference](https://launchschool.com/exercises/7488cf72))

The following code is a simplified (and not so serious) model of how we read a software development book. But running this code results in a stack overflow. What is wrong?

```js
const totalPages = 364;
let energy = 100;

function read() {
  let currentPage = 1;

  while (energy > 0 && currentPage < totalPages) {
    currentPage += 1;
    energy -= (5 + currentPage * 0.1);
  }

  console.log(`Made it to page ${String(currentPage)}.`);

  // Drink a cup of coffee.
  energy = 100;

  // Continue reading.
  if (currentPage < totalPages) {
    read();
  } else {
    console.log('Done!');
  }
}

read();
```

### Solution
The problem is that each time the `read` function is called, JavaScript creates a new scope and resets the `currentPage` variable back to `1`. This process will never end. Declaring variables with `let` gives the variables block scope, which in this case is within the `read` function. To fix this, we can move the `currentPage` variable declaration outside of the `read` function:

```js
let currentPage = 1;

function read() {
  while (energy > 0 && currentPage < totalPages) {
  // ... ommitted
```
