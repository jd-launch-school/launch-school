# Easy 1
## Odd Numbers
([Launch School Reference](https://launchschool.com/exercises/dfef7e9a))

Log all odd numbers from `1` to `99`, inclusive, to the console, with each number on a separate line.

### Solution
```js
for (let counter = 1; counter <= 99; counter += 2) {
  console.log(counter);
}
```

### Solution checking for odd
```js
for (let counter = 1; counter <= 99; counter++) {
  if (counter % 2 === 1) {
    console.log(counter);
  }
}
```

### Further Exploration
Repeat this exercise with a technique different from the one that you used, and different from the one provided. Also consider adding a way for the user to specify the limits of the odd numbers logged to the console.

#### Solution
```js
let rlSync = require('readline-sync');
let counter = Number(rlSync.question('What is the starting number? '));
let end = Number(rlSync.question('What is the ending number? '));

if (counter % 2 === 0) counter += 1;

while (counter <= end) {
  console.log(counter);
  counter += 2;
}
```

## Even Numbers
([Launch School Reference](https://launchschool.com/exercises/88396929))

Log all even numbers from `1` to `99`, inclusive, to the console, with each number on a separate line.

### Solution
```js
for (let counter = 2; counter <= 98; counter += 2) console.log(counter);
```

### Solution using continue
```js
for (let i = 1; i < 100; i += 1) {
  if (i % 2 === 1) {
    continue;
  }

  console.log(i);
}
```

## How Big is the Room
([Launch School Reference](https://launchschool.com/exercises/10323cf5))

Build a program that asks the user to enter the length and width of a room in meters, and then logs the area of the room to the console in both square meters and square feet.

Note: 1 square meter == 10.7639 square feet

Do not worry about validating the input at this time. Use the `readlineSync.prompt` method to collect user input.

Example:

```js
Enter the length of the room in meters:
10
Enter the width of the room in meters:
7
The area of the room is 70.00 square meters (753.47 square feet).
```

### Solution
```js
let rlSync = require('readline-sync');
const SQMETERS_TO_SQFEET = 10.7639;

console.log('What is the length of the room?');
let length = Number(rlSync.prompt());

console.log('What is the width of the room?');
let width = Number(rlSync.prompt());

let areaMeters = length * width;
let areaFeet = (areaMeters * SQMETERS_TO_SQFEET).toFixed(2);

console.log(`The area of the room is ${areaMeters} square meters (${areaFeet} square feet).`)
```

### Further Exploration
Modify the program so that it asks the user for the input type (meters or feet). Compute for the area accordingly, and log it and its conversion in parentheses.

```js
let rlSync = require('readline-sync');
const SQMETERS_TO_SQFEET = 10.7639;

console.log('Are you using (m)eters or (f)eet?');
let unit = rlSync.prompt() === 'f' ? 'feet' : 'meters';

console.log(`What is the length of the room in ${unit}?`);
let length = Number(rlSync.prompt());

console.log(`What is the width of the room in ${unit}?`);
let width = Number(rlSync.prompt());

if (unit === 'meters') {
  let areaMeters = length * width;
  let areaFeet = (areaMeters * SQMETERS_TO_SQFEET).toFixed(2);

  console.log(`The area of the room is ${areaMeters} square meters (${areaFeet} square feet).`)
} else {
  let areaFeet = length * width;
  let areaMeters = (areaFeet / SQMETERS_TO_SQFEET).toFixed(2);

  console.log(`The area of the room is ${areaFeet} square feet (${areaMeters} square meters).`)
}
```

## Tip Calculator
([Launch School Reference](https://launchschool.com/exercises/9d6492cd))

Create a simple tip calculator. The program should prompt for a bill amount and a tip rate. The program must compute the tip, and then log both the tip and the total amount of the bill to the console. You can ignore input validation and assume that the user will put in numbers.

Example:

```
What is the bill? 200
What is the tip percentage? 15

The tip is $30.00
The total is $230.00
```

### Solution
```js
let rlSync = require('readline-sync');

let bill = parseFloat(rlSync.question('What is the bill? '));
let tipPercentage = parseFloat(rlSync.question('What is the tip percentage? '));

let tip = bill * (tipPercentage / 100);
let total = bill + tip;

console.log(`The tip is $${tip.toFixed(2)}`);
console.log(`The total is $${total.toFixed(2)}`);
```

## Sum or Product of Consecutive Integers
([Launch School Reference](https://launchschool.com/exercises/cf7fce81))

Write a program that asks the user to enter an integer greater than `0`, then asks if the user wants to determine the sum or the product of all numbers between `1` and the entered integer, inclusive.

Examples:

```js
Please enter an integer greater than 0: 5
Enter "s" to compute the sum, or "p" to compute the product. s

The sum of the integers between 1 and 5 is 15.

Please enter an integer greater than 0: 6
Enter "s" to compute the sum, or "p" to compute the product. p

The product of the integers between 1 and 6 is 720.
```

### Solution
```js
let rlSync = require('readline-sync');

let integer = parseInt(rlSync.question('Please enter an integer greater than 0: '), 10);
let operation = rlSync.question("Enter 's' to copmute the sum, or 'p' to compute the prduct: ");

let sum = 0;
let product = 1;
for (let index = 1; index <= integer; index++) {
  sum += index;
  product *= index;
}

if (operation === 's') {
  console.log(`The sum of the integers between 1 and ${integer} is ${sum}.`);
} else {
  console.log(`The producct of the integers between 1 and ${integer} is ${product}.`);
}
```

### Recommended Solution
```js
function computeSum(number) {
  let total = 0;

  for (let i = 1; i <= number; i += 1) {
    total += i;
  }

  return total;
}

function computeProduct(number) {
  let total = 1;

  for (let i = 1; i <= number; i += 1) {
    total *= i;
  }

  return total;
}

const number = parseInt(prompt('Please enter an integer greater than 0'), 10);
const operation = prompt('Enter "s" to compute the sum, or "p" to compute the product.');

if (operation === 's') {
  let sum = String(computeSum(number));
  console.log(`The sum of the integers between 1 and ${String(number)} is ${sum}.`);
} else if (operation === 'p') {
  let product = String(computeProduct(number));
  console.log(`The product of the integers between 1 and ${String(number)} is ${product}.`);
} else {
  console.log('Oops. Unknown operation.');
}
```

### Further Exploration
What if the input was an array of integers instead of just a single integer? How would your `computeSum` and `computeProduct` functions change?

#### Solution
```js
let rlSync = require('readline-sync');

let array = [2, 65, 1, 7];
let operation = rlSync.question("Enter 's' to copmute the sum, or 'p' to compute the prduct: ");

let sum = 0;
let product = 1;

array.forEach(num => {
  sum += num;
  product *= num;
});

if (operation === 's') {
  console.log(`The sum of the integers [${array.join(', ')}] is ${sum}.`);
} else {
  console.log(`The producct of the integers [${array.join(', ')}] is ${product}.`);
}
```

### Further Exploration 2
Given that the input is an array, how might you make use of the `Array.prototype.reduce()` method?

```js
let rlSync = require('readline-sync');

let array = [2, 65, 1, 7];
let operation = rlSync.question("Enter 's' to copmute the sum, or 'p' to compute the prduct: ");

let sum = array.reduce((sum, num) => sum + num);
let product = array.reduce((sum, num) => sum * num);

if (operation === 's') {
  console.log(`The sum of the integers [${array.join(', ')}] is ${sum}.`);
} else {
  console.log(`The producct of the integers [${array.join(', ')}] is ${product}.`);
}
```

## Short Long Short
([Launch School Reference](https://launchschool.com/exercises/3c9aad3b))

Write a function that takes two strings as arguments, determines the length of the two strings, and then returns the result of concatenating the shorter string, the longer string, and the shorter string once again. You may assume that the strings are of different lengths.

```js
Examples:

shortLongShort('abc', 'defgh');    // "abcdefghabc"
shortLongShort('abcde', 'fgh');    // "fghabcdefgh"
shortLongShort('', 'xyz');         // "xyz"
```

### Solution
```js
function shortLongShort(first, second) {
  if (first.length > second.length) {
    return second + first + second;
  } else {
    return first + second + first;
  }
}

console.log(shortLongShort('abc', 'defgh') === 'abcdefghabc');
console.log(shortLongShort('abcde', 'fgh') === 'fghabcdefgh');
console.log(shortLongShort('', 'xyz') === 'xyz');
```

## Leap Years Part 1
([Launch School Reference](https://launchschool.com/exercises/480e9f98))

In the modern era under the Gregorian Calendar, leap years occur in every year that is evenly divisible by 4, unless the year is also divisible by 100. If the year is evenly divisible by 100, then it is not a leap year, unless the year is also evenly divisible by 400.

Assume this rule is valid for any year greater than year 0. Write a function that takes any year greater than 0 as input and returns `true` if the year is a leap year or `false` if it is not a leap year.

Examples:

```js
isLeapYear(2016);      // true
isLeapYear(2015);      // false
isLeapYear(2100);      // false
isLeapYear(2400);      // true
isLeapYear(240000);    // true
isLeapYear(240001);    // false
isLeapYear(2000);      // true
isLeapYear(1900);      // false
isLeapYear(1752);      // true
isLeapYear(1700);      // false
isLeapYear(1);         // false
isLeapYear(100);       // false
isLeapYear(400);       // true
```

### Solution
```js
function isLeapYear(year) {
  if (year <= 0) return 'Year must be greater than 0.';

  let divisibleBy400 = year % 400 === 0;
  let divisibleBy100 = year % 100 === 0;
  let divisibleBy4 = year % 4 === 0;

  return divisibleBy400 || (divisibleBy4 && !divisibleBy100);
}

console.log(isLeapYear(0) === 'Year must be greater than 0.');
console.log(isLeapYear(2016) === true);
console.log(isLeapYear(2015) === false);
console.log(isLeapYear(2100) === false);
console.log(isLeapYear(2400) === true);
console.log(isLeapYear(240000) === true);
console.log(isLeapYear(240001) === false);
console.log(isLeapYear(2000) === true);
console.log(isLeapYear(1900) === false);
console.log(isLeapYear(1752) === true);
console.log(isLeapYear(1700) === false);
console.log(isLeapYear(1) === false);
console.log(isLeapYear(100) === false);
console.log(isLeapYear(400) === true);
```

### Further Exploration 1
The order in which you perform tests for a leap year calculation is important. For what years will `isLeapYear` fail if you rewrite it as shown below?

```js
function isLeapYear(year) {
  if (year % 100 === 0) {
    return false;
  } else if (year % 400 === 0) {
    return true;
  } else {
    return year % 4 === 0;
  }
}
```

#### Solution
This will return `false` for any leap year that is divisible by 400, since years divisible by 400 are also divisible by 100. The second branch of the `if` statement will never be evaluated.

### Further Exploration 2
Can you rewrite `isLeapYear` to perform its tests in the opposite order of the above solution? That is, test whether the year is divisible by 4 first, then, if necessary, test whether it is divisible by 100, and finally, if necessary, test whether it is divisible by 400. Is this solution simpler or more complex than the original solution?

#### Solution
```js
function isLeapYear(year) {
  if (year % 4 === 0) {
    if (year % 100 === 0) {
      if (year % 400 === 0) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  return false;
}
```

This solution is more complex since we have to nest `if` statements.

## Leap Years Part 2
([Launch School Reference](https://launchschool.com/exercises/1a55612e))

This is a continuation of the previous exercise.

The British Empire adopted the Gregorian Calendar in 1752, which was a leap year. Prior to 1752, they used the Julian Calendar. Under the Julian Calendar, leap years occur in any year that is evenly divisible by 4.

Using this information, update the function from the previous exercise to determine leap years both before and after 1752.

Examples:

```js
isLeapYear(2016);      // true
isLeapYear(2015);      // false
isLeapYear(2100);      // false
isLeapYear(2400);      // true
isLeapYear(240000);    // true
isLeapYear(240001);    // false
isLeapYear(2000);      // true
isLeapYear(1900);      // false
isLeapYear(1752);      // true
isLeapYear(1700);      // true
isLeapYear(1);         // false
isLeapYear(100);       // true
isLeapYear(400);       // true
```

### Solution
```js
function isLeapYear(year) {
  if (year <= 0) return 'Year must be greater than 0.';

  let divisibleBy400 = year % 400 === 0;
  let divisibleBy100 = year % 100 === 0;
  let divisibleBy4 = year % 4 === 0;

  return year < 1752 ? divisibleBy4 : divisibleBy400 || (divisibleBy4 && !divisibleBy100);
}

console.log(isLeapYear(2016) === true);
console.log(isLeapYear(2015) === false);
console.log(isLeapYear(2100) === false);
console.log(isLeapYear(2400) === true);
console.log(isLeapYear(240000) === true);
console.log(isLeapYear(240001) === false);
console.log(isLeapYear(2000) === true);
console.log(isLeapYear(1900) === false);
console.log(isLeapYear(1752) === true);
console.log(isLeapYear(1700) === true);
console.log(isLeapYear(1) === false);
console.log(isLeapYear(100) === true);
console.log(isLeapYear(400) === true);
```

## Multiples of 3 and 5
([Launch School Reference](https://launchschool.com/exercises/d543b302))

Write a function that computes the sum of all numbers between 1 and some other number, inclusive, that are multiples of `3` or `5`. For instance, if the supplied number is `20`, the result should be `98` (`3 + 5 + 6 + 9 + 10 + 12 + 15 + 18 + 20`).

You may assume that the number passed in is an integer greater than `1`.

Examples:

```js
multisum(3);       // 3
multisum(5);       // 8
multisum(10);      // 33
multisum(1000);    // 234168
```

### Solution
```js
function multisum(num) {
  let sum = 0;

  for (let multiple = 1; multiple <= num; multiple++) {
    if (multiple % 3 === 0 || multiple % 5 === 0) sum += multiple;
  }

  return sum;
}

console.log(multisum(3) === 3);
console.log(multisum(5) === 8);
console.log(multisum(10) === 33);
console.log(multisum(20) === 98);
console.log(multisum(1000) === 234168);
```

### Recommended Solution
```js
function isMultiple(number, divisor) {
  return number % divisor === 0;
}

function multisum(maxValue) {
  let sum = 0;

  for (let number = 1; number <= maxValue; number += 1) {
    if (isMultiple(number, 3) || isMultiple(number, 5)) {
      sum += number;
    }
  }

  return sum;
}
```

## UTF-16 String Value
([Launch School Reference](https://launchschool.com/exercises/75158b71))

Write a function that determines and returns the UTF-16 string value of a string passed in as an argument. The UTF-16 string value is the sum of the UTF-16 values of every character in the string. (You may use `String.prototype.charCodeAt()` to determine the UTF-16 value of a character.)

Examples:

```js
utf16Value('Four score');         // 984
utf16Value('Launch School');      // 1251
utf16Value('a');                  // 97
utf16Value('');                   // 0

// The next three lines demonstrate that the code
// works with non-ASCII characters from the UTF-16
// character set.
const OMEGA = "\u03A9";             // UTF-16 character 'Ω' (omega)
utf16Value(OMEGA);                  // 937
utf16Value(OMEGA + OMEGA + OMEGA);  // 2811
```

### Solution
```js
function utf16Value(utfString) {
  let sum = 0;

  for (let index = 0; index < utfString.length; index++) {
    sum += utfString[index].charCodeAt(0);
  }

  return sum;
}


console.log(utf16Value('Four score') === 984);
console.log(utf16Value('Launch School') === 1251);
console.log(utf16Value('a') === 97);
console.log(utf16Value('') === 0);

// The next three lines demonstrate that the code
// works with non-ASCII characters from the UTF-16
// character set.
const OMEGA = "\u03A9";             // UTF-16 character 'Ω' (omega)
console.log(utf16Value(OMEGA) === 937);
console.log(utf16Value(OMEGA + OMEGA + OMEGA) === 2811);
```

# Easy 2
## Ddaaiillyy ddoouubbllee
([Launch School Reference](https://launchschool.com/exercises/5f3323e6))

Write a function that takes a string argument and returns a new string that contains the value of the original string with all consecutive duplicate characters collapsed into a single character.

Examples:

```js
crunch('ddaaiillyy ddoouubbllee');    // "daily double"
crunch('4444abcabccba');              // "4abcabcba"
crunch('ggggggggggggggg');            // "g"
crunch('a');                          // "a"
crunch('');                           // ""
```

### Solution
```js
function crunch(string) {
  let result = '';

  for (let index = 0; index < string.length; index++) {
    if (result[result.length - 1] === string[index]) continue;
    result += string[index];
  }

  return result;
}

console.log(crunch('ddaaiillyy ddoouubbllee') === 'daily double');
console.log(crunch('4444abcabccba') === '4abcabcba');
console.log(crunch('ggggggggggggggg') === 'g');
console.log(crunch('a') === 'a');
console.log(crunch('') === '');
```

### Recommended Solution
```js
function crunch(text) {
  let index = 0;
  let crunchText = '';

  while (index <= text.length - 1) {
    if (text[index] !== text[index + 1]) {
      crunchText += text[index];
    }

    index += 1;
  }

  return crunchText;
}
```

### Further Exploration 1
You may have noticed that the solution continues iterating until index points to the last character in the string, which means that `text[index + 1]` is beyond the end of the string during the last iteration. Why does it do this? What happens if we stop iterating when `index` is equal to `text.length - 1`?


#### Solution
When we attempt to retrieve an index of a string that doesn't exist, JavaScript returns `undefined`. This will always be unequal to the last character of the string, so the last character of the string will always be concatenated to the result.

I'm not sure about the second question since it seems the code already stops iterating when `index` is equal to `text.length - 1`. However, if we stopped iterating when `index` is equal to `text.length`, then we would have `undefined` concatenated to our result. This happens because the last index to be evaluated would be beyond the end of the string and would return `undefined` and then `index + 1` would also return `undefined`. This satisfies our `if` statement condition, concatenating `undefined` to the result.

### Further Exploration 2
It's also possible to solve this using regular expressions. For a nice challenge, give this a try with regular expressions. Can you think of any other solutions that don't use regular expressions?

```js
function crunch(string) {
  const REGEX = /(.)\1+/g;
  return string.replace(REGEX, "$1");
}

console.log(crunch('ddaaiillyy ddoouubbllee') === 'daily double');
console.log(crunch('4444abcabccba') === '4abcabcba');
console.log(crunch('ggggggggggggggg') === 'g');
console.log(crunch('a') === 'a');
console.log(crunch('') === '');
```

## Bannerizer
([Launch School Reference](https://launchschool.com/exercises/1be5b58c))

Write a function that will take a short line of text, and write it to the console log within a box.

Examples:

```js
logInBox('To boldly go where no one has gone before.');
```

will log on the console:

```
+--------------------------------------------+
|                                            |
| To boldly go where no one has gone before. |
|                                            |
+--------------------------------------------+
```

```js
logInBox('');
+--+
|  |
|  |
|  |
+--+
```

You may assume that the output will always fit in your browser window.

### Solution
```js
function logInBox(string) {
  console.log(`+${'-'.repeat(string.length + 2)}+`);
  console.log(`| ${' '.repeat(string.length)} |`);
  console.log(`| ${string} |`)
  console.log(`| ${' '.repeat(string.length)} |`);
  console.log(`+${'-'.repeat(string.length + 2)}+`);
}

logInBox('test');
logInBox('To boldly go where no one has gone before.');
logInBox('');
```

### Recommended Solution
```js
function logInBox(message) {
  const horizontalRule = `+${repeatChar('-', message.length + 2)}+`;
  const emptyLine = `|${repeatChar(' ', message.length + 2)}|`;

  console.log(horizontalRule);
  console.log(emptyLine);
  console.log(`| ${message} |`);
  console.log(emptyLine);
  console.log(horizontalRule);
}

function repeatChar(char, times) {
  let repeated = '';
  while (repeated.length < times) {
    repeated += char;
  }

  return repeated;
}
```

### Further Exploration
Modify this function so that it truncates the `message` if it doesn't fit inside a maximum width provided as a second argument (the width is the width of the box itself). You may assume no maximum if the second argument is omitted.

#### Solution
```js
function logInBox(string, boxWidth = string.length) {
  if (boxWidth > string.length) boxWidth = string.length;

  console.log(`+${'-'.repeat(boxWidth + 2)}+`);
  console.log(`| ${' '.repeat(boxWidth)} |`);
  console.log(`| ${string.slice(0, boxWidth)} |`)
  console.log(`| ${' '.repeat(boxWidth)} |`);
  console.log(`+${'-'.repeat(boxWidth + 2)}+`);
}

logInBox('testing something', 6);
logInBox('To boldly go where no one has gone before.', 15);
logInBox('');
```

### Further Exploration 2
For a real challenge, try word wrapping messages that are too long to fit, so that they appear on multiple lines but are still contained within the box.

#### Solution
```js
function logInBox(string, boxWidth = string.length) {
  console.log(`+${'-'.repeat(boxWidth + 2)}+`);
  console.log(`| ${' '.repeat(boxWidth)} |`);

  let start = 0;
  for (let index = boxWidth; start < string.length; index += boxWidth) {
    let stringSnippet = string.slice(start, index);
    let extraSpaces = boxWidth - stringSnippet.length;

    console.log(`| ${stringSnippet}${' '.repeat(extraSpaces)} |`);
    start = index;
  }

  console.log(`| ${' '.repeat(boxWidth)} |`);
  console.log(`+${'-'.repeat(boxWidth + 2)}+`);
}

logInBox('testing something', 6);
logInBox('To boldly go where no one has gone before.', 20);
logInBox('');
```

## Stringy Strings
([Launch School Reference](https://launchschool.com/exercises/cfc94245))

Write a function that takes one argument, a positive integer, and returns a string of alternating `'1's` and `'0's`, always starting with a `'1'`. The `length` of the string should match the given integer.

Examples:

```js
stringy(6);    // "101010"
stringy(9);    // "101010101"
stringy(4);    // "1010"
stringy(7);    // "1010101"
```

### Solution
```js
function stringy(num) {
  let string = '';

  for (let index = 1; index <= num; index++) {
    string += index % 2 === 1 ? '1' : '0';
  }

  return string;
}

console.log(stringy(6) === '101010');
console.log(stringy(9) === '101010101');
console.log(stringy(4) === '1010');
console.log(stringy(7) === '1010101');
```

## Fibonacci Number Location by Length
([Launch School Reference](https://launchschool.com/exercises/b63d5e66))

The Fibonacci series is a series of numbers (`1, 1, 2, 3, 5, 8, 13, 21, ...`) such that the first two numbers are `1` by definition, and each subsequent number is the sum of the two previous numbers. Fibonacci numbers often appear in mathematics and nature.

Computationally, the Fibonacci series is a simple series, but the results grow at an incredibly rapid rate. For example, the 100th Fibonacci number is 354,224,848,179,261,915,075 — that's enormous, especially considering that it takes six iterations just to find the first 2-digit Fibonacci number.

Write a function that calculates and returns the index of the first Fibonacci number that has the number of digits specified by the argument. (The first Fibonacci number has an index of `1`.)

You may assume that the argument is always an integer greater than or equal to `2`.

----- Aside -----
JavaScript's normal `Number` type can represent integers accurate up to the value of `Number.MAX_SAFE_INTEGER`, which is the 16-digit value `9007199254740991`. Any integer larger than that value loses accuracy. For instance, the following code outputs `1`, not `2` as you may expect:

```js
console.log(Number.MAX_SAFE_INTEGER + 2 - Number.MAX_SAFE_INTEGER);
```

We'll be working with much larger numbers in this problem. Fortunately, JavaScript now supports a `BigInt` type that lets you work with massive integers, limited only by the amount of memory available to your program, and the time you can devote to waiting for an answer.

To use `BigInt` integers in your solution, simply append the letter `n` to any numbers you use in your solution: `1n`, `1234567890123456789012345678901234567890n`, and so on. JavaScript will take care of the rest.
----- End Aside -----

Examples:

```js
findFibonacciIndexByLength(2n) === 7n;    // 1 1 2 3 5 8 13
findFibonacciIndexByLength(3n) === 12n;   // 1 1 2 3 5 8 13 21 34 55 89 144
findFibonacciIndexByLength(10n) === 45n;
findFibonacciIndexByLength(16n) === 74n;
findFibonacciIndexByLength(100n) === 476n;
findFibonacciIndexByLength(1000n) === 4782n;
findFibonacciIndexByLength(10000n) === 47847n;

// The last example may take a minute or so to run.
```

### Solution
```js
function findFibonacciIndexByLength(length) {
  const Previous = [1n, 1n];
  let index = 1n;

  while (true) {
    let next = Previous[0] + Previous[1];

    if (BigInt(String(next).length) === length) return index + 2n;

    Previous[0] = Previous[1];
    Previous[1] = next;
    index += 1n;
  }
}
```

### Recommended Solution
```js
function findFibonacciIndexByLength(length) {
  let first = 1n;
  let second = 1n;
  let count = 2n;
  let fibonacci;

  do {
    fibonacci = first + second;
    count += 1n;
    first = second;
    second = fibonacci;
  } while (String(fibonacci).length < length);

  return count;
}
```

## Right Triangles
([Launch School Reference](https://launchschool.com/exercises/6948ddb4))

Write a function that takes a positive integer, `n`, as an argument and logs a right triangle whose sides each have `n` stars. The hypotenuse of the triangle (the diagonal side in the images below) should have one end at the lower-left of the triangle, and the other end at the upper-right.

Examples:

```
triangle(5);

    *
   **
  ***
 ****
*****

triangle(9);

        *
       **
      ***
     ****
    *****
   ******
  *******
 ********
*********
```

### Solution
```js
function triangle(rows) {
  for (let stars = 1; stars <= rows; stars++) {
    console.log(`${' '.repeat(rows - stars)}${'*'.repeat(stars)}`);
  }
}

triangle(5);
triangle(9);
```

### Recommended Solution
```js
function triangle(height) {
  let stars = 1;
  let spaces = height - 1;

  for (let i = 0; i < height; i += 1) {
    console.log(repeat(' ', spaces) + repeat('*', stars));
    stars += 1;
    spaces -= 1;
  }
}

function repeat(char, count) {
  let repeated = '';

  for (let i = 0; i < count; i += 1) {
    repeated += char;
  }

  return repeated;
}
```

## Madlibs
([Launch School Reference](https://launchschool.com/exercises/85e333ca))

Madlibs is a simple game where you create a story template with "blanks" for words. You, or another player, then construct a list of words and place them into the story, creating an often silly or funny story as a result.

Create a simple madlib program that prompts for a noun, a verb, an adverb, and an adjective, and injects them into a story that you create.

Example:

```
Enter a noun: dog
Enter a verb: walk
Enter an adjective: blue
Enter an adverb: quickly

// console output
Do you walk your blue dog quickly? That's hilarious!
```

### Solution
```js
const readlineSync = require("readline-sync");

const noun = readlineSync.question('Enter a noun: ');
const verb = readlineSync.question('Enter a verb: ');
const adjective = readlineSync.question('Enter a adjective: ');
const adverb = readlineSync.question('Enter a adverb: ');

console.log(`Do you ${verb} your ${adjective} ${noun} ${adverb}? That's hilarious!`)
```

## Double Doubles
([Launch School Reference](https://launchschool.com/exercises/176497b0))

A double number is an even-length number whose left-side digits are exactly the same as its right-side digits. For example, `44`, `3333`, `103103`, and `7676` are all double numbers, whereas `444`, `334433`, and `107` are not.

Write a function that returns the number provided as an argument multiplied by two, unless the argument is a double number; otherwise, return the double number as-is.

Examples:

```js
twice(37);          // 74
twice(44);          // 44
twice(334433);      // 668866
twice(444);         // 888
twice(107);         // 214
twice(103103);      // 103103
twice(3333);        // 3333
twice(7676);        // 7676
```

### Solution
```js
function twice(num) {
  let string = String(num);
  let half = string.length / 2
  let firstHalf = string.slice(0, half);
  let secondHalf = string.slice(half, string.length);

  return firstHalf === secondHalf ? num : num * 2;
}


console.log(twice(37) === 74);
console.log(twice(44) === 44);
console.log(twice(334433) === 668866);
console.log(twice(444) === 888);
console.log(twice(107) === 214);
console.log(twice(103103) === 103103);
console.log(twice(3333) === 3333);
console.log(twice(7676) === 7676);
```

## Grade Book
([Launch School Reference](https://launchschool.com/exercises/643d5655))

Write a function that determines the mean (average) of the three scores passed to it, and returns the letter associated with that grade.

Numerical score letter grade list:

- 90 <= score <= 100: 'A'
- 80 <= score < 90: 'B'
- 70 <= score < 80: 'C'
- 60 <= score < 70: 'D'
- 0 <= score < 60: 'F'

Tested values are all between 0 and 100. There is no need to check for negative values or values greater than 100.

Examples:

```js
getGrade(95, 90, 93);    // "A"
getGrade(50, 50, 95);    // "D"
```

### Solution
```js
function getGrade(first, second, third) {
  const AVERAGE_SCORE = (first + second + third) / 3;

  if (AVERAGE_SCORE >= 90 && AVERAGE_SCORE <= 100) {
    return 'A';
  } else if (AVERAGE_SCORE >= 80 && AVERAGE_SCORE < 90) {
    return 'B';
  } else if (AVERAGE_SCORE >= 70 && AVERAGE_SCORE < 80) {
    return 'C';
  } else if (AVERAGE_SCORE >= 60 && AVERAGE_SCORE < 70) {
    return 'D';
  } else if (AVERAGE_SCORE < 60) {
    return 'F';
  }
}

console.log(getGrade(95, 90, 93) === 'A');    // "A"
console.log(getGrade(50, 50, 95) === 'D');    // "D"
```

## Clean Up the Words
([Launch School Reference](https://launchschool.com/exercises/1a7f4ec6))

Given a string that consists of some words and an assortment of non-alphabetic characters, write a function that returns that string with all of the non-alphabetic characters replaced by spaces. If one or more non-alphabetic characters occur in a row, you should only have one space in the result (i.e., the result string should never have consecutive spaces).

Example:

```js
cleanUp("---what's my +*& line?");    // " what s my line "
```

### Solution
```js
function cleanUp(string) {
  return string.replace(/\W{1,}/gi, ' ')
}

console.log(cleanUp("---what's my +*& line?") ===" what s my line ");
```

### Recommended Solution
```js
function cleanUp(string) {
  let result = '';

  for (let index = 0; index < string.length; index++) {
    if (string[index].match(/\w/ig)) {
      result += string[index];
    } else if (result[result.length -1] !== ' ') {
      result += ' ';
    }
  }

  return result;
}

console.log(cleanUp("---what's my +*& line?") ===" what s my line ");
```

## What Century is That
([Launch School Reference](https://launchschool.com/exercises/063ab637))

Write a function that takes a year as input and returns the century. The return value should be a string that begins with the century number, and ends with `'st'`, `'nd'`, `'rd'`, or `'th'` as appropriate for that number.

New centuries begin in years that end with `01`. So, the years 1901 - 2000 comprise the 20th century.

Examples:

```js
century(2000);        // "20th"
century(2001);        // "21st"
century(1965);        // "20th"
century(256);         // "3rd"
century(5);           // "1st"
century(10103);       // "102nd"
century(1052);        // "11th"
century(1127);        // "12th"
century(11201);       // "113th"
```

### Solution
```js
function addSuffix(year) {
  if (   year % 100 === 11
      || year % 100 === 12
      || year % 100 === 13) return String(year) + 'th';

  switch (year % 10) {
    case 1: return String(year) + 'st';
    case 2: return String(year) + 'nd';
    case 3: return String(year) + 'rd';
    default: return String(year) + 'th';
  }
}

function century(year) {
  if (year % 10 === 0) {
    return addSuffix(year / 100);
  } else {
    return addSuffix(Math.floor(year / 100) + 1);
  }
}

console.log(century(2000) === "20th");
console.log(century(2001) === "21st");
console.log(century(1965) === "20th");
console.log(century(256) === "3rd");
console.log(century(5) === "1st");
console.log(century(10103) === "102nd");
console.log(century(1052) === "11th");
console.log(century(1127) === "12th");
console.log(century(11201) === "113th");
```

### Recommended Solution
```js
function century(year) {
  let centuryNumber = Math.floor(year / 100) + 1;

  if (year % 100 === 0) {
    centuryNumber -= 1;
  }

  return String(centuryNumber) + centurySuffix(centuryNumber);
}

function centurySuffix(centuryNumber) {
  if (catchWithTh(centuryNumber % 100)) {
    return 'th';
  }

  let lastDigit = centuryNumber % 10;
  switch (lastDigit) {
    case 1: return 'st';
    case 2: return 'nd';
    case 3: return 'rd';
    default: return 'th';
  }
}

function catchWithTh(lastTwo) {
  return lastTwo === 11 || lastTwo === 12 || lastTwo === 13;
}
```

# Easy 3
([Launch School Reference](https://launchschool.com/exercise_sets/829c41b0))

## How Old is Teddy
([Launch School Reference](https://launchschool.com/exercises/854c55e7))

Build a program that randomly generates Teddy's age, and logs it to the console. Have the age be a random number between `20` and `200` (inclusive).

Example Output:

```
Teddy is 69 years old!
```

### Solution
```js
function randomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

let age = randomNumber(20, 200);
console.log(`Teddy is ${age} years old!`)
```

### Further Exploration 1
The `randomBetween` function used the `Math.floor()` method. Would it make a difference if the `Math.round()` method was used instead?

#### Solution
If we used `Math.round()`, then `201` would be a potential value returned from the function because any value over `200.5` will be rounded to `201`, which is greater than `200`. If we wanted to use `Math.round()`, we would have to pick a random value from `180` values instead of `181`:

```js
function randomNumber(min, max) {
  return Math.round(Math.random() * (max - min)) + min;
}
```

### Further Exploration 2
Also, how can we make the function more robust? What if the user inadvertently gave the inputs in reverse order (i.e., the value passed to `min` was greater than `max`)?

```js
function randomNumber(min, max) {
  if (max < min) [min, max] = [max, min];
  return Math.round(Math.random() * (max - min)) + min;
}
```

## Searching 101
([Launch School Reference](https://launchschool.com/exercises/3e06242a))

Write a program that solicits six numbers from the user and logs a message that describes whether the sixth number appears among the first five numbers.

Examples:

```
Enter the 1st number: 25
Enter the 2nd number: 15
Enter the 3rd number: 20
Enter the 4th number: 17
Enter the 5th number: 23
Enter the last number: 17

The number 17 appears in [25, 15, 20, 17, 23].

-----

Enter the 1st number: 25
Enter the 2nd number: 15
Enter the 3rd number: 20
Enter the 4th number: 17
Enter the 5th number: 23
Enter the last number: 18

The number 18 does not appear in [25, 15, 20, 17, 23].
```

### Solution
```js
const readlineSync = require("readline-sync");
const numbers = [];

numbers.push(readlineSync.question('Enter the 1st number: '));
numbers.push(readlineSync.question('Enter the 2nd number: '));
numbers.push(readlineSync.question('Enter the 3rd number: '));
numbers.push(readlineSync.question('Enter the 4th number: '));
numbers.push(readlineSync.question('Enter the 5th number: '));

const lastNumber = readlineSync.question('Enter the last number: ');

const inNumbers = numbers.includes(lastNumber);
console.log(`The number ${lastNumber} ${inNumbers ? 'appears' : 'does not appear'} in [${numbers.join(', ')}].`)
```

### Further Exploration
What if the problem was looking for a number that satisfies some condition (e.g., a number greater than `25`), instead of a specific number? Would the current solution still work? Why or why not? Think about this first before scrolling down.

#### Solution
No. `includes` only checks to see if an array contains an element, not whether an array has values greater than a given number.

```js
const readlineSync = require("readline-sync");
const numbers = [];

numbers.push(readlineSync.question('Enter the 1st number: '));
numbers.push(readlineSync.question('Enter the 2nd number: '));
numbers.push(readlineSync.question('Enter the 3rd number: '));
numbers.push(readlineSync.question('Enter the 4th number: '));
numbers.push(readlineSync.question('Enter the 5th number: '));

const lastNumber = readlineSync.question('Enter the last number: ');
const inNumbers = numbers.filter(num => num > lastNumber);

if (inNumbers.length === 0) {
  console.log(`There is no number greater than ${lastNumber}.`);
} else {
  console.log(`The numbers [${inNumbers.join(', ')}] are greater than ${lastNumber}.`);
}
```

#### Solution using `some`
```js
const readlineSync = require("readline-sync");
const numbers = [];

numbers.push(readlineSync.question('Enter the 1st number: '));
numbers.push(readlineSync.question('Enter the 2nd number: '));
numbers.push(readlineSync.question('Enter the 3rd number: '));
numbers.push(readlineSync.question('Enter the 4th number: '));
numbers.push(readlineSync.question('Enter the 5th number: '));

const lastNumber = readlineSync.question('Enter the last number: ');
const inNumbers = numbers.some(num => num > lastNumber);

if (inNumbers) {
  const match = numbers.filter(num => num > lastNumber);
  console.log(`The numbers [${match.join(', ')}] are greater than ${lastNumber}.`);
} else {
  console.log(`There is no number greater than ${lastNumber}.`);
}
```

## When Will I Retire
([Launch School Reference](https://launchschool.com/exercises/b60eaa7f))

Build a program that logs when the user will retire and how many more years the user has to work until retirement.

Example:

```js
What is your age? 30
At what age would you like to retire? 70

It's 2017. You will retire in 2057.
You have only 40 years of work to go!
```

### Solution
```js
const readlineSync = require('readline-sync');
const age = Number(readlineSync.question('What is your age? '));
const retireAge = Number(readlineSync.question('At what age would you like to retire? '));

const yearsLeft = retireAge - age;
const currentYear = new Date().getFullYear();

console.log(`It's ${currentYear}. You will retire in ${currentYear + (yearsLeft)}`);
console.log(`You have only ${yearsLeft} years of work to go!`);
```

### Further Exploration
What would happen if the `new` keyword wasn't used in defining the `today` variable on line 4?

#### Solution
`today` would be assigned to a string representation of the current date and time as opposed to a Date object. Then, a `TypeError` exception will be raised when we try to call `getFullYear()` on the `today` string.

## Palindromic Strings Part 1
([Launch School Reference](https://launchschool.com/exercises/f030a076))

Write a function that returns `true` if the string passed as an argument is a palindrome, or `false` otherwise. A palindrome reads the same forwards and backwards. For this problem, the case matters and all characters matter.

Examples:

```js
console.log(isPalindrome('madam') === true);
console.log(isPalindrome('Madam') === false);
console.log(isPalindrome("madam i'm adam") === false);
console.log(isPalindrome('356653') === true);
```

### Solution
```js
function isPalindrome(string) {
  return string === string.split('').reverse().join('');
}

console.log(isPalindrome('madam') === true);
console.log(isPalindrome('Madam') === false);
console.log(isPalindrome("madam i'm adam") === false);
console.log(isPalindrome('356653') === true);
```

## Palindromic Strings Part 2
([Launch School Reference](https://launchschool.com/exercises/2a2dbd46))

Write another function that returns `true` if the string passed as an argument is a palindrome, or `false` otherwise. This time, however, your function should be case-insensitive, and should ignore all non-alphanumeric characters. If you wish, you may simplify things by calling the `isPalindrome` function you wrote in the previous exercise.

Examples:

```js
console.log(isRealPalindrome('madam') === true);
console.log(isRealPalindrome('Madam') === true);
console.log(isRealPalindrome("Madam, I'm Adam") === true);
console.log(isRealPalindrome('356653') === true);
console.log(isRealPalindrome('356a653') === true);
console.log(isRealPalindrome('123ab321') === false);
```

### Solution
```js
function isPalindrome(string) {
  return string === string.split('').reverse().join('');
}

function isRealPalindrome(string) {
  let regex = /[a-z0-9]/ig;
  return isPalindrome(string.match(regex).join('').toLowerCase());
}

console.log(isRealPalindrome('madam') === true);
console.log(isRealPalindrome('Madam') === true);
console.log(isRealPalindrome("Madam, I'm Adam") === true);
console.log(isRealPalindrome('356653') === true);
console.log(isRealPalindrome('356a653') === true);
console.log(isRealPalindrome('123ab321') === false);
```

### Recommended Solution
```js
function isRealPalindrome(string) {
  string = removeNonLetterNumbers(string.toLowerCase());
  return isPalindrome(string);
}

function removeNonLetterNumbers(string) {
  let result = '';

  for (let i = 0; i < string.length; i += 1) {
    if (isLetter(string[i]) || isNumber(string[i])) {
      result += string[i];
    }
  }

  return result;
}

function isLetter(char) {
  return char >= 'a' && char <= 'z';
}

function isNumber(char) {
 return char >= '0' && char <= '9';
}
```

## Palindromic Number
([Launch School Reference](https://launchschool.com/exercises/d8f7c48a))

Write a function that returns `true` if its integer argument is palindromic, or `false` otherwise. A palindromic number reads the same forwards and backwards.

Examples:

```js
console.log(isPalindromicNumber(34543) === true);
console.log(isPalindromicNumber(123210) === false);
console.log(isPalindromicNumber(22) === true);
console.log(isPalindromicNumber(5) === true);
```

### Solution
```js
function isPalindrome(string) {
  return string === string.split('').reverse().join('');
}

function isPalindromicNumber(number) {
  return isPalindrome(String(number));
}

console.log(isPalindromicNumber(34543) === true);
console.log(isPalindromicNumber(123210) === false);
console.log(isPalindromicNumber(22) === true);
console.log(isPalindromicNumber(5) === true);
```

### Further Exploration
Suppose the number argument begins with one or more `0`s. Will the solution still work? Why or why not? Is there any way to address this?

#### Solution
No, the solution will not work. In JavaScript, Decimal numbers that start with zero,that are followed by another decimal digit, and all remaining digits are less than `8`, the number will be interpreted as an octal number. For other numbers, JavaScript will remove the leading zeros.

To fix this, we can check for leading and following zeros before checking if it's a palindrome. If there are the same amount of leading and following zeros, we can remove them from the number using `splice` and then check if the remaining digits are a palindrome.

## Running Totals
([Launch School Reference](https://launchschool.com/exercises/f3a0ccc0))

Write a function that takes an array of numbers and returns an array with the same number of elements, but with each element's value being the running total from the original array.

Examples:

```js
console.log(runningTotal([2, 5, 13]));             // [2, 7, 20]
console.log(runningTotal([14, 11, 7, 15, 20]));    // [14, 25, 32, 47, 67]
console.log(runningTotal([3]));                    // [3]
console.log(runningTotal([]));                     // []
```

### Solution using `for`
```js
function runningTotal(array) {
  let result = [];
  let sum = 0;

  for (let index = 0; index < array.length; index++) {
    sum += array[index];
    result.push(sum);
  }

  return result;
}
```

### Solution using `map`
```js
function runningTotal(array) {
  let sum = 0;
  return array.map(num => sum += num);
}
```

### Solution using `reduce`
```js
function runningTotal(array) {
  let sum = 0;

  return array.reduce((prev, cur) => {
    prev.push(sum += cur);
    return prev;
  }, []);
}
```

## Letter Swap
([Launch School Reference](https://launchschool.com/exercises/c86ceb32))

Given a string of words separated by spaces, write a function that swaps the first and last letters of every word.

You may assume that every word contains at least one letter, and that the string will always contain at least one word. You may also assume that each string contains nothing but words and spaces, and that there are no leading, trailing, or repeated spaces.

Examples:

```js
console.log(swap('Oh what a wonderful day it is') === 'hO thaw a londerfuw yad ti si');
console.log(swap('Abcde') === 'ebcdA');
console.log(swap('a') === 'a');
```

### Solution
```js
/*
  * PROBLEM
  * - INPUT: string
  * - OUTPUT: string with first and last letters of each word swapped
  *
  * ALGORITHM
  * - split the string into an array of words
  * - LOOP over the array
  *   - split the word into an array of characters
*     - swap the first and last letter of the array of characters
*       - save the first letter
*       - replace first letter with last letter
*       - replace last letter with saved first letter
  *   - join the array of characters back into a string
  * - join the array of words with spaces as the separator
  * - return the resulting string
 */

function swap(string) {
  const stringArray = string.split(' ');

  let result = stringArray.map(word => {
    const characters = word.split('');
    const firstChar = characters.shift();
    const lastChar = characters.pop() || '';

    return lastChar + characters.join('') + firstChar;
  });

  return result.join(' ');
}

console.log(swap('Oh what a wonderful day it is') === 'hO thaw a londerfuw yad ti si');
console.log(swap('Abcde') === 'ebcdA');
console.log(swap('a') === 'a');
```

### Better Solution using `slice`
```js
function swap(string) {
  const stringArray = string.split(' ');

  let result = stringArray.map(word => {
    if (word.length === 1) return word;

    return word[word.length - 1] + word.slice(1, -1) + word[0];
  });

  return result.join(' ');
}
```

### Solution using destructuring assignment
```js
function swap(string) {
  const stringArray = string.split(' ');

  let result = stringArray.map(word => {
    const characters = word.split('');
    [characters[0], characters[characters.length - 1]] = [characters[characters.length - 1], characters[0]].join('');
    return characters.join('');
  });

  return result.join(' ');
}

console.log(swap('Oh what a wonderful day it is') === 'hO thaw a londerfuw yad ti si');
console.log(swap('Abcde') === 'ebcdA');
console.log(swap('a') === 'a');
```

## Letter Counter Part 1
([Launch School Reference](https://launchschool.com/exercises/109b1371))

Write a function that takes a string consisting of one or more space separated words and returns an object that shows the number of words of different sizes.

Words consist of any sequence of non-space characters.

Examples:

```js
console.log(wordSizes('Four score and seven.'));                       // { "3": 1, "4": 1, "5": 1, "6": 1 }
console.log(wordSizes('Hey diddle diddle, the cat and the fiddle!'));  // { "3": 5, "6": 1, "7": 2 }
console.log(wordSizes("What's up doc?"));                              // { "2": 1, "4": 1, "6": 1 }
console.log(wordSizes(''));                                            // {}
```

### Solution
```js
/*
  * PROBLEM
  * - INPUT: string
  * - OUTPUT: object showing number of words of each size
  *
  * ALGORITHM
  * - declare 'counts' variable initialized to an empty object
  * - check if given string is an empty string, if so, return the empty string
  * - split given string into an array of words
  * - LOOP over array of words
  *   - IF size of current word is a key in the 'counts' object
  *     - add 1 to the value
  *   - ELSE
  *     - create new key with value 1
  * - RETURN 'counts'
 */

function wordSizes(string) {
  let counts = {};
  if (string === '') return counts;

  string.split(' ').forEach(word => {
    counts[word.length] ? counts[word.length] += 1 : counts[word.length] = 1;

    // OR ...
    counts[wordSize] = counts[wordSize] || 0;
    counts[wordSize] += 1;
  });

  return counts;
}

console.log(wordSizes('Four score and seven.'));                       // { "3": 1, "4": 1, "5": 1, "6": 1 }
console.log(wordSizes('Hey diddle diddle, the cat and the fiddle!'));  // { "3": 5, "6": 1, "7": 2 }
console.log(wordSizes("What's up doc?"));                              // { "2": 1, "4": 1, "6": 1 }
console.log(wordSizes(''));                                            // {}
```

## Letter Counter Part 2
([Launch School Reference](https://launchschool.com/exercises/5401a895))

Modify the `wordSizes` function from the previous exercise to exclude non-letters when determining word size. For instance, the word size of `"it's"` is `3`, not `4`.

Examples:

```
console.log(wordSizes('Four score and seven.'));                       // { "3": 1, "4": 1, "5": 2 }
console.log(wordSizes('Hey diddle diddle, the cat and the fiddle!'));  // { "3": 5, "6": 3 }
console.log(wordSizes("What's up doc?"));                              // { "5": 1, "2": 1, "3": 1 }
console.log(wordSizes(''));                                            // {}
```

### Solution
```js
function wordSizes(string) {
  let counts = {};
  if (string === '') return counts;

  string.split(' ').forEach(word => {
    word = word.match(/[a-z0-9]/gi).join('');
    counts[word.length] ? counts[word.length] += 1 : counts[word.length] = 1;
  });

  return counts;
}

console.log(wordSizes('Four score and seven.'));                       // { "3": 1, "4": 1, "5": 2 }
console.log(wordSizes('Hey diddle diddle, the cat and the fiddle!'));  // { "3": 5, "6": 3 }
console.log(wordSizes("What's up doc?"));                              // { "5": 1, "2": 1, "3": 1 }
console.log(wordSizes(''));                                            // {}
```

### Recommended Solution
```js
function wordSizes(words) {
  const wordsArray = words.split(' ');
  const count = {};

  for (let i = 0; i < wordsArray.length; i += 1) {
    let cleanWordSize = removeNonLetters(wordsArray[i].toLowerCase()).length;
    if (cleanWordSize === 0) {
      continue;
    }

    count[cleanWordSize] = count[cleanWordSize] || 0;
    count[cleanWordSize] += 1;
  }

  return count;
}

function removeNonLetters(string)  {
  let result = '';

  for (let i = 0; i < string.length; i += 1) {
    if (isLetter(string[i])) {
      result += string[i];
    }
  }

  return result;
}

function isLetter(char) { return char >= 'a' && char <= 'z' }
```

# Easy 4
([Launch School Reference](https://launchschool.com/exercise_sets/b1647500))

## Cute Angles (TODO)
([Launch School Reference](https://launchschool.com/exercises/50faecbd))

```js
/*
Write a function that takes a floating point number representing an angle between 0 and 360 degrees and returns a string representing that angle in degrees, minutes, and seconds. You should use a degree symbol (`˚`) to represent degrees, a single quote (`'`) to represent minutes, and a double quote (`"`) to represent seconds. There are 60 minutes in a degree, and 60 seconds in a minute.
*/

console.log(dms(30));        // 30°00'00
console.log(dms(76.73));     // 76°43'48
console.log(dms(254.6));     // 254°35'59
console.log(dms(93.034773)); // 93°02'05
console.log(dms(0));         // 0°00'00
console.log(dms(360));       // 360°00'00 or 0°00'00
```

Note: your results may differ slightly depending on how you round values, but should generally be within a second or two of the results shown.

### Solution
```js
```

### Recommended Solution
```js
const DEGREE = '\xB0';
const MINUTES_PER_DEGREE = 60;
const SECONDS_PER_MINUTE = 60;
const SECONDS_PER_DEGREE = MINUTES_PER_DEGREE * SECONDS_PER_MINUTE;

function dms(degreesFloat) {
  const degreesInt = Math.floor(degreesFloat);
  const minutes = Math.floor((degreesFloat - degreesInt) * MINUTES_PER_DEGREE);
  const seconds = Math.floor(
    (degreesFloat - degreesInt - (minutes / MINUTES_PER_DEGREE)) *
    SECONDS_PER_DEGREE
  );

  return `${String(degreesInt) + DEGREE + padZeroes(minutes)}'${padZeroes(seconds)}"`;
}

function padZeroes(number) {
  const numString = String(number);
  return numString.length < 2 ? (`0${numString}`) : numString;
}
```

### Further Exploration (TODO)

The current solution implementation only works with positive numbers in the range of `0` to `360` (inclusive). Can you refactor it so that it works with any positive or negative number?

Our solution returns the following results for inputs outside the range 0-360:

```js
dms(-1);   // -1°00'00"
dms(400);  // 400°00'00"
dms(-40);  // -40°00'00"
dms(-420); // 420°00'00"
```

Since degrees are normally restricted to the range 0-360, can you modify the code so it returns a value in the appropriate range when the input is less than 0 or greater than 360?

```js
dms(-1);   // 359°00'00"
dms(400);  // 40°00'00"
dms(-40);  // 320°00'00"
dms(-420); // 60°00'00"
```

#### Solution
```js
```

## Combining Arrays (TODO)
([Launch School Reference](https://launchschool.com/exercises/8c72838d))

Write a function that takes two arrays as arguments and returns an array containing the union of the values from the two. There should be no duplication of values in the returned array, even if there are duplicates in the original arrays. You may assume that both arguments will always be arrays.

Example:

```js
console.log(union([1, 3, 5], [3, 6, 9]));    // [1, 3, 5, 6, 9]
```

### Solution
```js
/*
  * PROBLEM
  * - INPUT: Two arrays of numbers
  * - OUTPUT: array, two arrays combined with duplicates removed
  *
  * ALGORITHM
  * - declare 'result' variable initialized to an empty array
  * - LOOP through arguments
  *   - LOOP through current argument
  *     - IF current number is not in 'result'
  *       - append current number to 'result'
  * - RETURN 'result'
*/

function union(...args) {
  const result = [];

  args.forEach(array => {
    array.forEach(num => {
      if (!result.includes(num)) {
        result.push(num);
      }
    });
  });

  return result;
}

console.log(union([1, 3, 5], [3, 6, 9])); // [1, 3, 5, 6, 9]
console.log(union([1, 3, 5], [10, 3, 6, 9]));  // [1, 3, 5, 10, 6, 9]
console.log(union([1, 3, 5], [10, 3, 6, 9], [4]));// [1, 3, 5, 10, 6, 9, 4]
console.log(union([1], [2], [1], [3], [6], [2], [0]));
```

### Solution using Set, Rest Operator, and Spread Operator
```js
function union(...args) {
  let result = [];

  args.forEach(array => {
    result.push(...array);
  });

  result = new Set(result);
  return [...result];
}
```

### Better Solution using Set, Rest Operator, and Spread Operator
```js
function union(...args) {
  return [...new Set(args.flat())];
}
```

## Halvsies
([Launch School Reference](https://launchschool.com/exercises/e943020f))

Write a function that takes an array as an argument and returns an array that contains two elements, each of which is an array. Put the first half of the original array elements in the first element of the return value, and put the second half in the second element. If the original array contains an odd number of elements, place the middle element in the first half array.

Examples:

```js
console.log(halvsies([1, 2, 3, 4]));       // [[1, 2], [3, 4]]
console.log(halvsies([1, 5, 2, 4, 3]));    // [[1, 5, 2], [4, 3]]
console.log(halvsies([5]));                // [[5], []]
console.log(halvsies([]));                 // [[], []]
```

### Solution
```js
/*
  * PROBLEM
  * - INPUT: array
  * - OUTPUT: array of two arrays
  *   - first array is first half
  *     - if odd number of elements, middle element goes in first half
  *   - second array is second half
  *
  * ALGORITHM
  * - declare 'half' variable initialized to result of length
  *   of the array divided by 2 rounded up
  * - return array containing:
  *   - sliced array using '0, half'
  *   - sliced array using 'half'
*/

function halvsies(array) {
  const half = Math.ceil(array.length / 2);
  const firstHalf = array.slice(0, half);
  const secondHalf = array.slice(half);

  return [firstHalf, secondHalf];
}
```

## Find the Duplicate
([Launch School Reference](https://launchschool.com/exercises/ee0a5888))

Given an unordered array and the information that exactly one value in the array occurs twice (every other value occurs exactly once), determine which value occurs twice. Write a function that will find and return the duplicate value that is in the array.

Examples:

```js
console.log(findDup([1, 5, 3, 1]));                   // 1
console.log(findDup([
         18,  9, 36, 96, 31, 19, 54, 75, 42, 15,
         38, 25, 97, 92, 46, 69, 91, 59, 53, 27,
         14, 61, 90, 81,  8, 63, 95, 99, 30, 65,
         78, 76, 48, 16, 93, 77, 52, 49, 37, 29,
         89, 10, 84,  1, 47, 68, 12, 33, 86, 60,
         41, 44, 83, 35, 94, 73, 98,  3, 64, 82,
         55, 79, 80, 21, 39, 72, 13, 50,  6, 70,
         85, 87, 51, 17, 66, 20, 28, 26,  2, 22,
         40, 23, 71, 62, 73, 32, 43, 24,  4, 56,
          7, 34, 57, 74, 45, 11, 88, 67,  5, 58]));    // 73
```

### Solution
```js
/*
  * PROBLEM
  * - INPUT: array
  * - OUTPUT: repeated element
  * - RULES:
  *   - input array can be unordered
  *   - only one value occurs twice
  *
  * ALGORITHM
  * - declare 'tempArray' variable initialized to an empty array
  * - LOOP through given array
  *   - IF 'tempArray' includes the current element
  *     - return the current element
  *   - ELSE
  *     - append current element to 'tempArray'
 */

function findDup(array) {
  const tempArray = [];
  for (let index = 0; index < array.length; index++) {
    if (tempArray.includes(array[index])) return array[index];
    tempArray.push(array[index]);
  }
}
```

## Combine Two Lists
([Launch School Reference](https://launchschool.com/exercises/d4132bc9))

Write a function that combines two arrays passed as arguments, and returns a new array that contains all elements from both array arguments, with each element taken in alternation.

You may assume that both input arrays are non-empty, and that they have the same number of elements.

Example:

```js
console.log(interleave([1, 2, 3], ['a', 'b', 'c']));    // [1, "a", 2, "b", 3, "c"]
```

### Solution
```js
/*
  * PROBLEM
  * - INPUT: two or more arrays
  * - OUTPUT: one array with alternating items from given arrays
  * - RULES:
  *   - all input arrays will be non-empty
  *   - all input arrays will have the same number of elements
  *
  * ALGORITHM
  * - declare 'result' variable initialized to an empty array
  * - LOOP using a for loop and length of one of the given arrays
  *   - LOOP through arguments
  *     - append the index of the current argument to 'result'
  * - return 'result'
 */

function interleave(...args) {
  const result = [];

  for (let index = 0; index < args[0].length; index++) {
    args.forEach(array => result.push(array[index]));
  }

  return result;
}

console.log(interleave([1, 2, 3], ['a', 'b', 'c']));    // [1, "a", 2, "b", 3, "c"]
console.log(interleave([1, 2, 3], ['a', 'b', 'c'], ['d', 5, 'f']));    // [1, "a", "d", 2, "b", 5, 3, "c", "f"]
```

### Recommended Solution
```js
function interleave(array1, array2) {
  const newArray = [];

  for (let i = 0; i < array1.length; i += 1) {
    newArray.push(array1[i], array2[i]);
  }

  return newArray;
}
```

## Multiplicative Average
([Launch School Reference](https://launchschool.com/exercises/151f2162))

Write a function that takes an array of integers as input, multiplies all of the integers together, divides the result by the number of entries in the array, and returns the result as a string with the value rounded to three decimal places.

Examples:

```js
console.log(showMultiplicativeAverage([3, 5]) === '7.500');
console.log(showMultiplicativeAverage([2, 5, 7, 11, 13, 17]) === '28361.667');
```

### Solution
```js
/*
  * PROBLEM
  * - INPUT: array of numbers
  * - OUTPUT: string representing average of numbers in array
  *   - value rounded to three decimal places
  *
  * ALGORITHM:
  * - initialize 'result' to return value of reduce
  * - LOOP through array
  *   - multiply previous and current numbers
  *   - return total
  * - divide 'result' by the length of the array
  * - use 'toFixed()' to either add zeros or truncate to three
  *   decimal places
  * - return result
 */

function showMultiplicativeAverage(array) {
  let result = array.reduce((prev, cur) => prev * cur);
  return (result / array.length).toFixed(3);
}

console.log(showMultiplicativeAverage([3, 5]) === '7.500');
console.log(showMultiplicativeAverage([2, 5, 7, 11, 13, 17]) === '28361.667');
```

## Multiply Lists
([Launch School Reference](https://launchschool.com/exercises/7369178f))

Write a function that takes two array arguments, each containing a list of numbers, and returns a new array that contains the product of each pair of numbers from the arguments that have the same index. You may assume that the arguments contain the same number of elements.

Example:

```js
console.log(multiplyList([3, 5, 7], [9, 10, 11]));    // [27, 50, 77]
console.log(multiplyList([3, 5, 7], [9, 10, 11], [1, 2, 3]));    // [27, 100, 231]
```

### Solution
```js
/*
  * PROBLEM
  * - INPUT: two or more arrays
  * - OUTPUT: array with product of each pair of numbers by index
  * - RULES:
  *   - each array argument will be the same length
  *
  * ALGORITHM:
  * - declare 'result' initialized to an array containing the same
  *   elements as the first array argument
  * - LOOP up to the length of the first array argument
  *   - LOOP through the arguments start at the 2nd argument
  *     - multiply the value at the index of the current argument
  *       by the value of the index in 'result'
  *     - set 'result' to this new value
  * - return 'result'
 */

function multiplyList(...args) {
  const result = new Array(...args[0]);

  // OR ...
  // const result = args[0].slice();

  for (let index = 0; index < args[0].length; index++) {
    for (let argsIndex = 1; argsIndex < args.length; argsIndex++) {
      result[index] *= args[argsIndex][index];
    }
  }

  return result;
}

console.log(multiplyList([3, 5, 7], [9, 10, 11]));    // [27, 50, 77]
console.log(multiplyList([3, 5, 7], [9, 10, 11], [1, 2, 3]));    // [27, 100, 231]
```

### Recommended Solution
```js
function multiplyList(numbers1, numbers2) {
  const result = [];

  for (let i = 0; i < numbers1.length; i += 1) {
    result.push(numbers1[i] * numbers2[i]);
  }

  return result;
}
```

## Digits List
([Launch School Reference](https://launchschool.com/exercises/c8ceb86c))

Write a function that takes one argument, a positive integer, and returns a list of the digits in the number.

Examples:

```js
console.log(digitList(12345));       // [1, 2, 3, 4, 5]
console.log(digitList(7));           // [7]
console.log(digitList(375290));      // [3, 7, 5, 2, 9, 0]
console.log(digitList(444));         // [4, 4, 4]
```

### Solution
```js
function digitList(num) {
  return String(num).split('').map(num => Number(num));
}

console.log(digitList(12345));       // [1, 2, 3, 4, 5]
console.log(digitList(7));           // [7]
console.log(digitList(375290));      // [3, 7, 5, 2, 9, 0]
console.log(digitList(444));         // [4, 4, 4]
```

### Recommended Solution (without `map`)
```js
function digitList(number) {
  const numberStringArray = String(number).split('');
  const numberArray = [];

  for (let i = 0; i < numberStringArray.length; i += 1) {
    numberArray.push(parseInt(numberStringArray[i], 10));
  }

  return numberArray;
}
```

## How Many
([Launch School Reference](https://launchschool.com/exercises/2b9f8b47))

Write a function that counts the number of occurrences of each element in a given array. Once counted, log each element alongside the number of occurrences.

Example:

```js
const vehicles = ['car', 'car', 'truck', 'car', 'SUV', 'truck',
                'motorcycle', 'motorcycle', 'car', 'truck'];

countOccurrences(vehicles);

// console output
// car => 4
// truck => 3
// SUV => 1
// motorcycle => 2
```

### Solution
```js
/*
  * PROBLEM:
  * - INPUT: array
  * - OUTPUT: string
  *   - value => number of occurences
  *
  * ALGORITHM:
  * - declare 'counts' to empty object
  * - LOOP through given array (forEach)
  *   - set the value of the key that matches the array element to
  *     either the current value of the key or 0
  *   - increment key's current value by 1
  * - get array of the keys in 'counts'
  * - LOOP through the array
  *   - log the current key, '=>', value of the key in 'counts'
 */

function countOccurrences(array) {
  const counts = {};

  array.forEach(elem => {
    counts[elem] = counts[elem] || 0
    counts[elem] += 1;
  });

  logCounts(counts);
}

function logCounts(counts) {
  Object.keys(counts).forEach(key => {
    console.log(`${key} => ${counts[key]}`);
  });
}

const vehicles = ['car', 'car', 'truck', 'car', 'SUV', 'truck',
                'motorcycle', 'motorcycle', 'car', 'truck'];

countOccurrences(vehicles);

// console output
// car => 4
// truck => 3
// SUV => 1
// motorcycle => 2
```

## Array Average
([Launch School Reference](https://launchschool.com/exercises/6774169b))

Write a function that takes one argument, an array containing integers, and returns the average of all the integers in the array, rounded down to the integer component of the average. The array will never be empty, and the numbers will always be positive integers.

Examples:

```js
console.log(average([1, 5, 87, 45, 8, 8]) === 25);
console.log(average([9, 47, 23, 95, 16, 52]) === 40);
```

### Solution
```js
function average(array) {
  return Math.floor(array.reduce((prev, cur) => prev + cur) / array.length);
}

console.log(average([1, 5, 87, 45, 8, 8]) === 25);
console.log(average([9, 47, 23, 95, 16, 52]) === 40);
```

# Easy 5
([Launch School Reference](https://launchschool.com/exercise_sets/605aaeb8))

## Double Char Part 1
([Launch School Reference](https://launchschool.com/exercises/478ca82d))

Write a function that takes a string, doubles every character in the string, and returns the result as a new string.

Examples:

```js
console.log(repeater('Hello') === "HHeelllloo");
console.log(repeater('Good job!') === "GGoooodd  jjoobb!!");
console.log(repeater('') === "");
```

### Solution
```js
function repeater(array) {
  let result = '';

  array.split('').forEach(char => {
    for (let repeat = 1; repeat <= 2; repeat++) {
      result += char;
    }

    // OR ...
    // result += char;
    // result += char;
  });

  return result;
}

console.log(repeater('Hello') === "HHeelllloo");
console.log(repeater('Good job!') === "GGoooodd  jjoobb!!");
console.log(repeater('') === "");
```

### Recommended Solution
```js
function repeater(string) {
  const stringArray = [];

  for (let i = 0; i < string.length; i += 1) {
    stringArray.push(string[i], string[i]);
  }

  return stringArray.join('');
}
```

## Double Char Part 2
([Launch School Reference](https://launchschool.com/exercises/5196dead))

Write a function that takes a string, doubles every consonant character in the string, and returns the result as a new string. The function should not double vowels (`'a'`,`'e'`,`'i'`,`'o'`,`'u'`), digits, punctuation, or whitespace.

Examples:

```js
console.log(doubleConsonants('String') === "SSttrrinngg");
console.log(doubleConsonants('Hello-World!') === "HHellllo-WWorrlldd!");
console.log(doubleConsonants('July 4th') === "JJullyy 4tthh");
console.log(doubleConsonants('') === "");
```

### Solution
```js
function doubleConsonants(array) {
  let result = '';

  array.split('').forEach(char => {
    if (char.match(/[a-z]/i) && char.match(/[^aeiou]/)) {
      result += char;
    }

    result += char;
  });

  return result;
}

console.log(doubleConsonants('String') === "SSttrrinngg");
console.log(doubleConsonants('Hello-World!') === "HHellllo-WWorrlldd!");
console.log(doubleConsonants('July 4th') === "JJullyy 4tthh");
console.log(doubleConsonants('') === "");
```

### Solution using `replace`
```js
function doubleConsonants(string) {
  return string.replace(/([^aeiou\W_\d])/ig, '$1$1');
}
```

## Reverse Number
([Launch School Reference](https://launchschool.com/exercises/b1be2705))

Write a function that takes a positive integer as an argument and returns that number with its digits reversed.

Examples:

```js
console.log(reverseNumber(12345) === 54321);
console.log(reverseNumber(12213) === 31221);
console.log(reverseNumber(456) === 654);
console.log(reverseNumber(12000) === 21);
console.log(reverseNumber(1) === 1);
```

### Solution
```js
function reverseNumber(num) {
  return Number(String(num).split('').reverse().join(''));
}

console.log(reverseNumber(12345) === 54321);
console.log(reverseNumber(12213) === 31221);
console.log(reverseNumber(456) === 654);
console.log(reverseNumber(12000) === 21);
console.log(reverseNumber(1) === 1);
```

## Get the Middle Character
([Launch School Reference](https://launchschool.com/exercises/91f90abf))

Write a function that takes a non-empty string argument and returns the middle character(s) of the string. If the string has an odd `length`, you should return exactly one character. If the string has an even `length`, you should return exactly two characters.

Examples:

```js
console.log(centerOf('I Love JavaScript') === 'a');
console.log(centerOf('Launch School') === ' ');
console.log(centerOf('Launch') === 'un');
console.log(centerOf('Launchschool') === 'hs');
console.log(centerOf('x') === 'x');
```

### Solution
```js
function centerOf(string) {
  const len = string.length;
  const center = Math.floor(len / 2);

  if (len % 2 === 1) {
    return string[center];
  } else {
    return string[center - 1] + string[center];
  }
}
```

## Always Return Negative
([Launch School Reference](https://launchschool.com/exercises/e0c7629c))

Write a function that takes a number as an argument. If the argument is a positive number, return the negative of that number. If the argument is a negative number, return it as-is.

Examples:

```js
console.log(negative(5) === -5);
console.log(negative(-3) === -3);
console.log(negative(0) === -0);
```

### Solution using ternary operator
```js
function negative(num) {
  return (num < 0) ? num : -num;
}
```

### Solution using `Math.abs()`
```js
function negative(num) {
  return Math.abs(num) * -1;
}
```

## Counting Up
([Launch School Reference](https://launchschool.com/exercises/f354dc14))

Write a function that takes an integer argument and returns an array containing all integers between `1` and the argument (inclusive), in ascending order.

You may assume that the argument will always be a positive integer.

Examples:

```js
console.log(sequence(5));    // [1, 2, 3, 4, 5]
console.log(sequence(3));    // [1, 2, 3]
console.log(sequence(1));    // [1]
```

### Solution
```js
function sequence(num) {
  const result = [];

  for (let counter = 1; counter <= num; counter++) {
    result.push(counter);
  }

  return result;
}
```

## Name Swapping
([Launch School Reference](https://launchschool.com/exercises/e284a929))

Write a function that takes a string argument consisting of a first name, a space, and a last name, and returns a new string consisting of the last name, a comma, a space, and the first name.

Examples:

```js
console.log(swapName('Joe Roberts') === "Roberts, Joe");
```

### Solution
```js
function swapName(string) {
  return string.split(' ').reverse().join(', ');
}
```

### Further Exploration
What if the person had more than one first name? Refactor the current solution so that it can accommodate this.

#### Solution
```js
function swapName(string) {
  array = string.split(' ')
  return array[array.length - 1] + ', ' + array.slice(0, -1).join(' ');
}

console.log(swapName('Joe Bob Roberts') === "Roberts, Joe Bob");
console.log(swapName('Joe Bob Cathy Roberts') === "Roberts, Joe Bob Cathy");
```

## Sequence Count
([Launch School Reference](https://launchschool.com/exercises/ee15e5a3))

Create a function that takes two integers as arguments. The first argument is a `count`, and the second is the starting number of a sequence that your function will create. The function should return an array containing the same number of elements as the `count` argument. The value of each element should be a multiple of the starting number.

You may assume that the `count` argument will always be an integer greater than or equal to `0`. The starting number can be any integer. If the `count` is `0`, the function should return an empty array.

Examples:

```js
console.log(sequence(5, 1));          // [1, 2, 3, 4, 5]
console.log(sequence(4, -7));         // [-7, -14, -21, -28]
console.log(sequence(3, 0));          // [0, 0, 0]
console.log(sequence(0, 1000000));    // []
```

### Solution
```js
/*
  * PROBLEM
  * - INTPUT: two integers, representing the count and start of the sequence
  * - OUTPUT: Array with length equal to count
  * - RULES:
  *   - value of each element in the result array should be a multiple of the starting number
  *   - count will always be an integer greater than or equal to 0
  *   - if count is 0, return an empty array
  *
  * ALGORITM
  * - declare 'result' variable initialized to an empty array
  * - LOOP while the length of result is less than 'count'
  *   - IF the length of 'result' is equal to zero, add 'start' to 'result'
  *   - ELSE add 'start' to the last element of 'result'
  * - return 'result'
 */

function sequence(count, start) {
  const result = [];

  while (result.length < count) {
    if (result.length === 0) {
      result.push(start);
    } else {
      result.push(start + result[result.length - 1]);
    }
  }

  return result;
}
```

### Recommended Solution
```js
function sequence(count, startNum) {
  const result = [];
  let sum = 0;

  for (let i = 0; i < count; i += 1) {
    result.push(sum += startNum);
  }

  return result;
}
```

## Reverse It Part 1
([Launch School Reference](https://launchschool.com/exercises/c02bd262))

Write a function that takes a string argument and returns a new string containing the words from the string argument in reverse order.

Examples:

```js
console.log(reverseSentence('') === "");
console.log(reverseSentence('Hello World') === "World Hello");
console.log(reverseSentence('Reverse these words') === "words these Reverse");
```

### Solution
```js
function reverseSentence(string) {
  return string.split(' ').reverse().join(' ');
}
```

## Reverse It Part 2
([Launch School Reference](https://launchschool.com/exercises/2d0aaa14))

Write a function that takes a string argument containing one or more words and returns a new string containing the words from the string argument. All five-or-more letter words should have their letters in reverse order. The string argument will consist of only letters and spaces. Words will be separated by a single space.

Examples:

```js
console.log(reverseWords('Professional') === "lanoisseforP");
console.log(reverseWords('Walk around the block') === "Walk dnuora the kcolb");
console.log(reverseWords('Launch School') === "hcnuaL loohcS");
```

### Solution
```js
function reverseWords(string) {
  return string.split(' ').map(word => reverseWord(word)).join(' ');
}

function reverseWord(word) {
  return word.length >=5 ? word.split('').reverse().join('') : word;
}
```

### Recommended Solution
```js
function reverseWords(string) {
  const words = string.split(' ');
  const reversedWords = [];

  for (let i = 0; i < words.length; i += 1) {
    let currentWord = words[i];
    if (currentWord.length >= 5) {
      reversedWords.push(currentWord.split('').reverse().join(''));
    } else {
      reversedWords.push(currentWord);
    }
  }

  return reversedWords.join(' ');
}
```

# List Processing
[Launch School Reference](https://launchschool.com/exercise_sets/8023101b)

## Sum of Digits
[Launch School Reference](https://launchschool.com/exercises/a887cdb7)

Write a function that takes one argument, a positive integer, and returns the sum of its digits. Do this without using `for`, `while`, or `do...while` loops - instead, use a series of method calls to perform the sum.

Examples:

```js
sum(23);           // 5
sum(496);          // 19
sum(123456789);    // 45
```

### Solution
```js
/*
 * convert the number to a string | string | conversion | String()
 * split string into an array of numbers | array | split | split()
 * get the sum of all array elements | number | reduction | reduce()
 */

function sum(number) {
  return String(number).split('').reduce((sum, cur) => sum + Number(cur), 0);
}

console.log(sum(23));           // 5
console.log(sum(23));           // 5
console.log(sum(496));          // 19
console.log(sum(123456789));    // 45
```

## Alphabetical Numbers
[Launch School Reference](https://launchschool.com/exercises/204b3efa)

Write a function that takes an array of integers between `0` and `19` and returns an array of those integers sorted based on the English word for each number:

> zero, one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen, fifteen, sixteen, seventeen, eighteen, nineteen

Do not mutate the argument.

Example:

```js
alphabeticNumberSort(
   [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]);
// [8, 18, 11, 15, 5, 4, 14, 9, 19, 1, 7, 17, 6, 16, 10, 13, 3, 12, 2, 0]
```

### Solution
```js
/*
 * convert each number element to its string word | array of strings | transformation | map()
 * sort the string elements alphabetically | array of strings | sort()
 * convert each string word back to a number | array of numbers | transformation | map()
 */

function alphabeticNumberSort(array) {
  const numberStrings = [
    'zero', 'one', 'two', 'three', 'four',
    'five', 'six', 'seven', 'eight', 'nine',
    'ten', 'eleven', 'twelve', 'thirteen',
    'fourteen', 'fifteen', 'sixteen',
    'seventeen', 'eighteen', 'nineteen'
  ]

  let strings = array.map(number => numberStrings[number]).sort();
  return strings.map(string => numberStrings.indexOf(string));
}

let testArray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19];

console.log(alphabeticNumberSort(testArray));
// [8, 18, 11, 15, 5, 4, 14, 9, 19, 1, 7, 17, 6, 16, 10, 13, 3, 12, 2, 0]

console.log(testArray);

console.log(alphabeticNumberSort([3, 5, 9, 0]));
```

### Recommended Solution
```js
function wordSort(num1, num2) {
  const NUMBER_WORDS = ['zero', 'one', 'two', 'three', 'four', 'five',
                        'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
                        'twelve', 'thirteen', 'fourteen', 'fifteen',
                        'sixteen', 'seventeen', 'eighteen', 'nineteen'];

  if (NUMBER_WORDS[num1] > NUMBER_WORDS[num2]) {
    return 1;
  } else if (NUMBER_WORDS[num1] < NUMBER_WORDS[num2]) {
    return -1;
  } else {
    return 0;
  }
}

function alphabeticNumberSort(array) {
  return [...array].sort(wordSort);
}
```

## Multiply All Pairs
[Launch School Reference](https://launchschool.com/exercises/60b9ea01)

Write a function that takes two array arguments, each containing a list of numbers, and returns a new array containing the products of all combinations of number pairs that exist between the two arrays. The returned array should be sorted in ascending numerical order.

You may assume that neither argument will be an empty array.

Example:

```js
multiplyAllPairs([2, 4], [4, 3, 1, 2]);    // [2, 4, 4, 6, 8, 8, 12, 16]
```

### Solution
```js
function multiplyAllPairs(first, second) {
  let result = [];

  first.forEach(firstNumber => {
    second.forEach(secondNumber => {
      result.push(firstNumber * secondNumber);
    });
  });

  // return result.sort((a, b) => a < b ? -1 : 1);
  return result.sort((a, b) => a - b);
}
console.log(multiplyAllPairs([2, 4], [4, 3, 1, 2]));    // [2, 4, 4, 6, 8, 8, 12, 16]
```

## Sum of Sums
[Launch School Reference](https://launchschool.com/exercises/c0a044dd)

Write a function that takes an array of numbers and returns the sum of the sums of each leading subsequence in that array. Examine the examples to see what we mean. You may assume that the array always contains at least one number.

Examples:

```js
console.log(sumOfSums([3, 5, 2]));        // (3) + (3 + 5) + (3 + 5 + 2) --> 21
console.log(sumOfSums([1, 5, 7, 3]));     // (1) + (1 + 5) + (1 + 5 + 7) + (1 + 5 + 7 + 3) --> 36
console.log(sumOfSums([4]));              // 4
console.log(sumOfSums([1, 2, 3, 4, 5]));  // 35
```

### Solution
```js
function sumOfSums(array) {
  let result = 0;

  for (let index = 0; index < array.length; index += 1) {
    result += array.slice(0, index + 1)
    .reduce((sum, cur) => sum + cur);
  }

  return result;
}

console.log(sumOfSums([3, 5, 2]));        // (3) + (3 + 5) + (3 + 5 + 2) --> 21
console.log(sumOfSums([1, 5, 7, 3]));     // (1) + (1 + 5) + (1 + 5 + 7) + (1 + 5 + 7 + 3) --> 36
console.log(sumOfSums([4]));              // 4
console.log(sumOfSums([1, 2, 3, 4, 5]));  // 35
```

### Recommended Solution
```js
function sumOfSums(numbers) {
  return numbers.map((number, idx) => numbers.slice(0, idx + 1)
                                             .reduce((sum, value) => sum + value))
                                             .reduce((sum, value) => sum + value);
}
```

## Leading Substrings
[Launch School Reference](https://launchschool.com/exercises/91427112)

Write a function that takes a string argument and returns a list of substrings of that string. Each substring should begin with the first letter of the word, and the list should be ordered from shortest to longest.

Examples:

```js
console.log(leadingSubstrings('abc'));      // ["a", "ab", "abc"]
console.log(leadingSubstrings('a'));        // ["a"]
console.log(leadingSubstrings('xyzzy'));    // ["x", "xy", "xyz", "xyzz", "xyzzy"]
```

### Solution
```js
function leadingSubstrings(string) {
  let substrings = [];

  for (let index = 0; index < string.length; index += 1) {
    substrings.push(string.slice(0, index + 1));
  }

  return substrings;
}

console.log(leadingSubstrings('abc'));      // ["a", "ab", "abc"]
console.log(leadingSubstrings('a'));        // ["a"]
console.log(leadingSubstrings('xyzzy'));    // ["x", "xy", "xyz", "xyzz", "xyzzy
```

## All Substrings
[Launch School Reference](https://launchschool.com/exercises/757204fb)

Write a function that returns a list of all substrings of a string. Order the returned list by where in the string the substring begins. This means that all substrings that start at index position 0 should come first, then all substrings that start at index position 1, and so on. Since multiple substrings will occur at each position, return the substrings at a given index from shortest to longest.

You may (and should) use the `leadingSubstrings` function you wrote in the previous exercise:

Example:

```
substrings('abcde');

// returns
[ "a", "ab", "abc", "abcd", "abcde",
  "b", "bc", "bcd", "bcde",
  "c", "cd", "cde",
  "d", "de",
  "e" ]
```

### Solution
```js
```

## Palindromic Substrings
[Launch School Reference](https://launchschool.com/exercises/b31e9777)

Write a function that returns a list of all substrings of a string that are palindromic. That is, each substring must consist of the same sequence of characters forwards as backwards. The substrings in the returned list should be sorted by their order of appearance in the input string. Duplicate substrings should be included multiple times.

You may (and should) use the `substrings` function you wrote in the previous exercise.

For the purpose of this exercise, you should consider all characters and pay attention to case; that is, `'AbcbA'` is a palindrome, but `'Abcba'` and `'Abc-bA'` are not. In addition, assume that single characters are not palindromes.

Examples:

```js
console.log(palindromes('abcd'));       // []
console.log(palindromes('madam'));      // [ "madam", "ada" ]

console.log(palindromes('hello-madam-did-madam-goodbye'));
// returns
// [ "ll", "-madam-", "-madam-did-madam-", "madam", "madam-did-madam", "ada",
//   "adam-did-mada", "dam-did-mad", "am-did-ma", "m-did-m", "-did-", "did",
//   "-madam-", "madam", "ada", "oo" ]

console.log(palindromes('knitting cassettes'));
// returns
// [ "nittin", "itti", "tt", "ss", "settes", "ette", "tt" ]
```

### Solution
```js
function palindromes(string) {
  return substrings(string).filter(isPalindrome);
}

function isPalindrome(word) {
  return word.length > 1 && word === word.split('').reverse().join('');
}
```

## Grocery List
[Launch School Reference](https://launchschool.com/exercises/17ebf9eb)

Write a function that takes a grocery list in a two-dimensional array and returns a one-dimensional array. Each element in the grocery list contains a fruit name and a number that represents the desired quantity of that fruit. The output array is such that each fruit name appears the number of times equal to its desired quantity.

In the example below, we want to buy 3 apples, 1 orange, and 2 bananas. Thus, we return an array that contains 3 apples, 1 orange, and 2 bananas.

Example:

```js
buyFruit([['apple', 3], ['orange', 1], ['banana', 2]]);
// returns ["apple", "apple", "apple", "orange", "banana", "banana"]
```
### Solution
```js
// iteration | forEach
// iteration | for loop
// LOOP over the given array
//   - LOOP the subarray's second element number of times
//     - append subarray's first element to the resulting array

function buyFruit(array) {
  let fruits = [];

  array.forEach(subArray => {
    for (let index = 0; index < subArray[1]; index += 1) {
      fruits.push(subArray[0]);
    }
  });

  return fruits;
}

console.log(buyFruit([['apple', 3], ['orange', 1], ['banana', 2]]));
// returns ["apple", "apple", "apple", "orange", "banana", "banana"]
```

### Recommended Solution
```js
function buyFruit(fruitsList) {
  return fruitsList.map(fruit => repeat(fruit))
                   .reduce((groceryList, fruit) => groceryList.concat(fruit));
}

function repeat(fruit) {
  const result = [];

  for (let i = 0; i < fruit[1]; i += 1) {
    result.push(fruit[0]);
  }

  return result;
}
```

## Inventory Item Transactions
[Launch School Reference](https://launchschool.com/exercises/ec4bc614)

Write a function that takes two arguments, an inventory item ID and a list of transactions, and returns an array containing only the transactions for the specified inventory item.

Example:

```js
const transactions = [ { id: 101, movement: 'in',  quantity:  5 },
                       { id: 105, movement: 'in',  quantity: 10 },
                       { id: 102, movement: 'out', quantity: 17 },
                       { id: 101, movement: 'in',  quantity: 12 },
                       { id: 103, movement: 'out', quantity: 15 },
                       { id: 102, movement: 'out', quantity: 15 },
                       { id: 105, movement: 'in',  quantity: 25 },
                       { id: 101, movement: 'out', quantity: 18 },
                       { id: 102, movement: 'in',  quantity: 22 },
                       { id: 103, movement: 'out', quantity: 15 }, ];

console.log(transactionsFor(101, transactions));
// returns
// [ { id: 101, movement: "in",  quantity:  5 },
//   { id: 101, movement: "in",  quantity: 12 },
//   { id: 101, movement: "out", quantity: 18 }, ]
```

### Solution
```js
function transactionsFor(inventoryItem, transactions) {
  return transactions.filter(({id}) => id === inventoryItem);
}
```

## Inventory Item Availability
[Launch School Reference](https://launchschool.com/exercises/cd69bc70)

Building on the previous exercise, write a function that returns `true` or `false` based on whether or not an inventory item is available. As before, the function takes two arguments: an inventory `item` and a list of `transactions`. The function should return `true` only if the sum of the `quantity` values of the `item`'s transactions is greater than zero. Notice that there is a `movement` property in each transaction object. A `movement` value of `'out'` will decrease the `item`'s `quantity`.

You may (and should) use the `transactionsFor` function from the previous exercise.

Examples:

```js
const transactions = [ { id: 101, movement: 'in',  quantity:  5 },
                       { id: 105, movement: 'in',  quantity: 10 },
                       { id: 102, movement: 'out', quantity: 17 },
                       { id: 101, movement: 'in',  quantity: 12 },
                       { id: 103, movement: 'out', quantity: 15 },
                       { id: 102, movement: 'out', quantity: 15 },
                       { id: 105, movement: 'in',  quantity: 25 },
                       { id: 101, movement: 'out', quantity: 18 },
                       { id: 102, movement: 'in',  quantity: 22 },
                       { id: 103, movement: 'out', quantity: 15 }, ];

isItemAvailable(101, transactions);     // false
isItemAvailable(105, transactions);     // true
```

### Solution
```js
function transactionsFor(inventoryItem, transactions) {
  return transactions.filter(({id}) => id === inventoryItem);
}

function isItemAvailable(item, transactions) {
  let totalInStock = transactionsFor(item, transactions).reduce((sum, {movement, quantity}) => {
    if (movement === 'in') {
      sum += quantity
    } else {
      sum -= quantity;
    }

    return sum;
  }, 0);

  return totalInStock > 0;
}

const transactions = [ { id: 101, movement: 'in',  quantity:  5 },
                       { id: 105, movement: 'in',  quantity: 10 },
                       { id: 102, movement: 'out', quantity: 17 },
                       { id: 101, movement: 'in',  quantity: 12 },
                       { id: 103, movement: 'out', quantity: 15 },
                       { id: 102, movement: 'out', quantity: 15 },
                       { id: 105, movement: 'in',  quantity: 25 },
                       { id: 101, movement: 'out', quantity: 18 },
                       { id: 102, movement: 'in',  quantity: 22 },
                       { id: 103, movement: 'out', quantity: 15 }, ];

console.dir(isItemAvailable(101, transactions));     // false
console.dir(isItemAvailable(105, transactions));     // true
```

# String and Text Processing
[Launch School Reference](https://launchschool.com/exercise_sets/2edda272)

## Uppercase Check
[Launch School Reference](https://launchschool.com/exercises/27444b15)

```js
/*
Write a function that takes a string argument and returns `true` if all of the alphabetic characters inside the string are uppercase; otherwise, return `false`. Ignore characters that are not alphabetic.
*/

console.log(isUppercase('t'));               // false
console.log(isUppercase('T'));               // true
console.log(isUppercase('Four Score'));      // false
console.log(isUppercase('FOUR SCORE'));      // true
console.log(isUppercase('4SCORE!'));         // true
console.log(isUppercase(''));                // true
```

### Solution
```js
/*
PROBLEM:
- INPUT: string
- OUTPUT: boolean, true if all characters in string are uppercase, false otherwise
- RULES:
  - ignore non-alphabetic characters

ABSTRACTION:
- interrogation (some/every)

ALGORITHM:
- split the given string into an array of characters
- call `every` on the array
  - check to see if each character is uppercase
*/

function isUppercase(string) {
  return string.split('').every(char => char === char.toUpperCase());
}

console.log(isUppercase('t'));               // false
console.log(isUppercase('T'));               // true
console.log(isUppercase('Four Score'));      // false
console.log(isUppercase('FOUR SCORE'));      // true
console.log(isUppercase('4SCORE!'));         // true
console.log(isUppercase(''));                // true
console.log(isUppercase('A!@#$%^&*,.?'));    // true
```

## Delete Vowels
[Launch School Reference](https://launchschool.com/exercises/64151dc4)

Write a function that takes an array of strings and returns an array of the same string values, but with all vowels (a, e, i, o, u) removed.

Examples:

```js
console.log(removeVowels(['abcdefghijklmnopqrstuvwxyz']));         // ["bcdfghjklmnpqrstvwxyz"]
console.log(removeVowels(['green', 'YELLOW', 'black', 'white']));  // ["grn", "YLLW", "blck", "wht"]
console.log(removeVowels(['ABC', 'AEIOU', 'XYZ']));                // ["BC", "", "XYZ"]
```

### Solution
```js
function removeVowels(array) {
  return array.map(string => string.replace(/[aeiou]/ig, ''));
}

console.log(removeVowels(['abcdefghijklmnopqrstuvwxyz']));         // ["bcdfghjklmnpqrstvwxyz"]
console.log(removeVowels(['green', 'YELLOW', 'black', 'white']));  // ["grn", "YLLW", "blck", "wht"]
console.log(removeVowels(['ABC', 'AEIOU', 'XYZ']));                // ["BC", "", "XYZ"]
```

## Lettercase Counter
[Launch School Reference](https://launchschool.com/exercises/ae099a61)

Write a function that takes a string and returns an object containing three properties: one representing the number of characters in the string that are lowercase letters, one representing the number of characters that are uppercase letters, and one representing the number of characters that are neither.

Examples:

```js
console.log(letterCaseCount('abCdef 123'));   // { lowercase: 5, uppercase: 1, neither: 4 }
console.log(letterCaseCount('AbCd +Ef'));     // { lowercase: 3, uppercase: 3, neither: 2 }
console.log(letterCaseCount('123'));          // { lowercase: 0, uppercase: 0, neither: 3 }
console.log(letterCaseCount(''));             // { lowercase: 0, uppercase: 0, neither: 0 }
```

### Solution
```js
function letterCaseCount(string) {
  let counts = {};
  counts.lowercase = (string.match(/[a-z]/g) || '').length;
  counts.uppercase = (string.match(/[A-Z]/g) || '').length;
  counts.neither = (string.match(/[^a-z]/ig) || '').length;
  return counts;
}

console.log(letterCaseCount('abCdef 123'));   // { lowercase: 5, uppercase: 1, neither: 4 }
console.log(letterCaseCount('AbCd +Ef'));     // { lowercase: 3, uppercase: 3, neither: 2 }
console.log(letterCaseCount('123'));          // { lowercase: 0, uppercase: 0, neither: 3 }
console.log(letterCaseCount(''));             // { lowercase: 0, uppercase: 0, neither: 0 }
```

## Capitalize Words
[Launch School Reference](https://launchschool.com/exercises/c963d077)

Write a function that takes a string as an argument and returns that string with the first character of every word capitalized and all subsequent characters in lowercase.

You may assume that a word is any sequence of non-whitespace characters.

Examples:

```js
console.log(wordCap('four score and seven'));       // "Four Score And Seven"
console.log(wordCap('the javaScript language'));    // "The Javascript Language"
console.log(wordCap('this is a "quoted" word'));    // 'This Is A "quoted" Word'
```

### Solution
```js
function capitalizeFirstLetter(string) {
  return string[0].toUpperCase() + string.slice(1).toLowerCase();
}

function wordCap(string) {
  return string.split(' ').map(capitalizeFirstLetter).join(' ');
}

console.log(wordCap('four score and seven'));       // "Four Score And Seven"
console.log(wordCap('the javaScript language'));    // "The Javascript Language"
console.log(wordCap('this is a "quoted" word'));    // 'This Is A "quoted" Word'
```

## Swap Case
[Launch School Reference](https://launchschool.com/exercises/3e2832f7)

Write a function that takes a string as an argument and returns that string with every lowercase letter changed to uppercase and every uppercase letter changed to lowercase. Leave all other characters unchanged.

Examples:

```js
swapCase('CamelCase');              // "cAMELcASE"
swapCase('Tonight on XYZ-TV');      // "tONIGHT ON xyz-tv"
```

### Solution
```js
function swapCharCase(char) {
  return char.toUpperCase() === char ? char.toLowerCase() : char.toUpperCase();
}

function swapCase(string) {
  // return string.split('').map(swapCharCase).join('');
  return string.replace(/\w/g, swapCharCase);
}

console.log(swapCase('CamelCase'));              // "cAMELcASE"
console.log(swapCase('Tonight on XYZ-TV'));      // "tONIGHT ON xyz-tv"
```

## Staggered Caps Part 1
[Launch School Reference](https://launchschool.com/exercises/a4032c98)

Write a function that takes a string as an argument and returns that string with a staggered capitalization scheme. Every other character, starting from the first, should be capitalized and should be followed by a lowercase or non-alphabetic character. Non-alphabetic characters should not be changed, but should be counted as characters for determining when to switch between upper and lower case.

Examples:

```js
console.log(staggeredCase('I Love Launch School!'));        // "I LoVe lAuNcH ScHoOl!"
console.log(staggeredCase('ALL_CAPS'));                     // "AlL_CaPs"
console.log(staggeredCase('ignore 77 the 4444 numbers'));   // "IgNoRe 77 ThE 4444 nUmBeRs"
```

### Solution
```js
/*
PROBLEM:
- INPUT: string
- OUTPUT: same string with every other character capitalized
- RULES:
  - don't change non-alphabetic characters
  - count non-alphabetic characters when determining which letters to capitalize
    - because of this, all characters with an even number index will be capitalized

DATA STRUCTURES:
- string

ABSTRACTION:
- iteration

ALGORITHM:
- declare result variable initialized to an empty string
- LOOP using `for`, start with 0, increment by 1 each iteration
  - IF index is odd
    - append lowercase character to result
  - ELSE
    - append uppercase character to result
- return result
*/

function staggeredCase(string) {
  let result = '';

  for (let index = 0; index < string.length; index += 1) {
    if (index % 2 === 1) {
      result += string[index].toLowerCase();
    } else {
      result += string[index].toUpperCase();
    }
  }

  return result;
}

console.log(staggeredCase('I Love Launch School!'));        // "I LoVe lAuNcH ScHoOl!"
console.log(staggeredCase('ALL_CAPS'));                     // "AlL_CaPs"
console.log(staggeredCase('ignore 77 the 4444 numbers'));   // "IgNoRe 77 ThE 4444 nUmBeRs"
```

## Staggered Caps Part 2
[Launch School Reference](https://launchschool.com/exercises/96764a36)

Modify the function from the previous exercise so that it ignores non-alphabetic characters when determining whether a letter should be upper or lower case. Non-alphabetic characters should still be included in the output string, but should not be counted when determining the appropriate case.

Examples:

```js
console.log(staggeredCase('I Love Launch School!'));        // "I lOvE lAuNcH sChOoL!"
console.log(staggeredCase('ALL CAPS'));                     // "AlL cApS"
console.log(staggeredCase('ignore 77 the 444 numbers'));    // "IgNoRe 77 ThE 444 nUmBeRs"
```

### Solution
```js
// PROBLEM:
// - INPUT: string
// - OUTPUT: same string with every other alphabetic character capitalized
// - RULES:
//   - don't change non-alphabetic characters
//   - don't count non-alphabetic characters when determining which letters to capitalize

// DATA STRUCTURES:
// - string

// ABSTRACTION:
// - iteration

// ALGORITHM:
// - declare result variable initialized to an empty string
// - declare isUpperCase variable initialized to the boolean 'true'
// - LOOP using `for`, start with 0, increment by 1 each iteration
//   - IF character is alphabetic
//     - IF isUppercase is true
//       - append uppercase character to result
//     - ELSE isUpperCase is false
//       - append lowercahse character to result
//     - reassign isUpperCase to the opposite boolean
//       - true if false, false if true
//   - ELSE
//     - append current character as-is to result
// - return result

function staggeredCase(string) {
  let result = '';
  let isUpperCase = true;

  for (let index = 0; index < string.length; index += 1) {
    let char = string[index];

    if (char.match(/[a-z]/i)) {
      if (isUpperCase) {
        result += char.toUpperCase();
      } else {
        result += char.toLowerCase();
      }

      isUpperCase = !isUpperCase
    } else {
      result += char;
    }
  }

  return result;
}

console.log(staggeredCase('I Love Launch School!'));        // "I lOvE lAuNcH sChOoL!"
console.log(staggeredCase('ALL CAPS'));                     // "AlL cApS"
console.log(staggeredCase('ignore 77 the 444 numbers'));    // "IgNoRe 77 ThE 444 nUmBeRs"
```

## How Long Are You
[Launch School Reference](https://launchschool.com/exercises/215da4d9)

Write a function that takes a string as an argument and returns an array that contains every word from the string, with each word followed by a space and the word's `length`. If the argument is an empty string or if no argument is passed, the function should return an empty array.

You may assume that every pair of words in the string will be separated by a single space.

Examples:

```js
wordLengths('cow sheep chicken');
// ["cow 3", "sheep 5", "chicken 7"]

wordLengths('baseball hot dogs and apple pie');
// ["baseball 8", "hot 3", "dogs 4", "and 3", "apple 5", "pie 3"]

wordLengths("It ain't easy, is it?");
// ["It 2", "ain't 5", "easy, 5", "is 2", "it? 3"]

wordLengths('Supercalifragilisticexpialidocious');
// ["Supercalifragilisticexpialidocious 34"]

wordLengths('');      // []
wordLengths();        // []
```

### Solution
```js
// PROBLEM:
// - INPUT: string
// - OUTPUT: array with elements in the form of 'word length'
// - RULES:
//   - if given string is empty or no string is passed in as an argument
//     - return an empty array
//
// DATA STRUCTURES:
// - string to array of words
//
// ABSTRACTION:
// - transformation: each word to 'word length' string
//
// ALGORITHM:
// - IF given string is empty, return empty array
// - split the string into an array of words
// - LOOP (using map)
//   - replace word with interpolated string 'word length'
// - return new array from map

function wordLengths(string = '') {
  if (string.length === 0) return [];

  return string.split(' ').map(word => `${word} ${word.length}`);
}

console.log(wordLengths('cow sheep chicken'));
// ["cow 3", "sheep 5", "chicken 7"]

console.log(wordLengths('baseball hot dogs and apple pie'));
// ["baseball 8", "hot 3", "dogs 4", "and 3", "apple 5", "pie 3"]

console.log(wordLengths("It ain't easy, is it?"));
// ["It 2", "ain't 5", "easy, 5", "is 2", "it? 3"]

console.log(wordLengths('Supercalifragilisticexpialidocious'));
// ["Supercalifragilisticexpialidocious 34"]

console.log(wordLengths(''));      // []
console.log(wordLengths());        // []
```

## Search Word Part 1
[Launch School Reference](https://launchschool.com/exercises/ef69fa72)

Write a function that takes two arguments, a word and a string of text, and returns an integer representing the number of times the word appears in the text.

You may assume that the `word` and `text` inputs will always be provided, and that all word breaks are spaces. Thus, some words will include punctuation such as periods and commas.

Example:

```js
const text = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?';

searchWord('sed', text);      // 3
```

### Solution
```js
// PROBLEM:
// - INPUT: a search term, text to search (both strings)
// - OUTPUT: integer representing how many times the search term appears in the text
// - RULES:
//   - both arguments will always be provided
//   - all word breaks are spaces
//
// DATA STRUCTURE:
// - string into array of words
// - return length of filtered array as an integer
//
// ABSTRACTION:
// - filter / selection
//
// ALGORITHM:
// - split the string using empty space as delimiter into an array of words
// - LOOP (using filter)
//   - keep only words, converted to lowercase, that match the search term, converted to lowercase
// - return the length of the filtered array

function searchWord(searchTerm, string) {
  return string
    .split(' ')
    .filter(word => word.toLowerCase() === searchTerm.toLowerCase())
    .length;
}

const text = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?';

console.log(searchWord('sed', text));      // 3
console.log(searchWord('qui', text));      // 4
console.log(searchWord('quiz', text));     // 0
```

### Alternative Solution using Regular Expression
```js
function searchWord(word, text) {
  let findWholeWord = new RegExp(`\\b${word}\\b`, 'ig');
  let matches = text.match(findWholeWord);

  return !matches ? 0 : matches.length;
}
```

## Search Word Part 2
The function from the previous exercise returns the number of occurrences of a word in some text. Although this is useful, there are also situations in which we just want to find the word in the context of the text.

For this exercise, write a function that takes a `word` and a string of `text` as arguments, and returns the `text` with every instance of the `word` highlighted. To highlight a word, enclose the word with two asterisks (`'**'`) on each side and change every letter of the word to uppercase (e.g., `'**HIGHLIGHTEDWORD**'`).

Example:

```js
const text = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?';

console.log(searchWord('sed', text));
// returns
// "**SED** ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, **SED** quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, **SED** quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"

console.log(searchWord('qui', text));
// "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos **QUI** ratione voluptatem sequi nesciunt. Neque porro quisquam est, **QUI** dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit **QUI** in ea voluptate velit esse quam nihil molestiae consequatur, vel illum **QUI** dolorem eum fugiat quo voluptas nulla pariatur?"
```

### Solution using split, map, join
```js
function searchWord(searchTerm, string) {
  return string
    .split(' ')
    .map(word => {
      if (word.toLowerCase() === searchTerm.toLowerCase()) {
        return `**${word.toUpperCase()}**`;
      } else {
        return word;
      }
    })
    .join(' ');
}
```

### Solution using regular expression
```js
function searchWord(word, text) {
  let findWholeWord = new RegExp(`\\b(${word})\\b`, 'ig');
  return text.replace(findWholeWord, `**${word.toUpperCase()}**`);
}
```

# Medium Problems 1
## Rotation (Part 1)
[Launch School Reference](https://launchschool.com/exercises/3d03ea77)

Write a function that rotates an array by moving the first element to the end of the array. Do not modify the original array.

  - If the input is not an array, return undefined.
  - If the input is an empty array, return an empty array.

Review the test cases below, then implement the solution accordingly.

```js
rotateArray([7, 3, 5, 2, 9, 1]);       // [3, 5, 2, 9, 1, 7]
rotateArray(['a', 'b', 'c']);          // ["b", "c", "a"]
rotateArray(['a']);                    // ["a"]
rotateArray([1, 'a', 3, 'c']);         // ["a", 3, "c", 1]
rotateArray([{ a: 2 }, [1, 2], 3]);    // [[1, 2], 3, { a: 2 }]
rotateArray([]);                       // []

// return `undefined` if the argument is not an array
rotateArray();                         // undefined
rotateArray(1);                        // undefined


// the input array is not mutated
const array = [1, 2, 3, 4];
rotateArray(array);                    // [2, 3, 4, 1]
array;                                 // [1, 2, 3, 4]
```

## Rotation (Part 2)
[Launch School Reference](https://launchschool.com/exercises/1aa322c0)

Write a function that rotates the last n digits of a number. For the rotation, rotate by one digit to the left, moving the first digit to the end.

Examples:

```
console.log(rotateRightmostDigits(735291, 1));      // 735291
console.log(rotateRightmostDigits(735291, 2));      // 735219
console.log(rotateRightmostDigits(735291, 3));      // 735912
console.log(rotateRightmostDigits(735291, 4));      // 732915
console.log(rotateRightmostDigits(735291, 5));      // 752913
console.log(rotateRightmostDigits(735291, 6));      // 352917
```

## Rotation (Part 3)
[Launch School Reference](https://launchschool.com/exercises/f6d52b17)

Take the number 735291 and rotate it by one digit to the left, getting 352917. Next, keep the first digit fixed in place and rotate the remaining digits to get 329175. Keep the first two digits fixed in place and rotate again to get 321759. Keep the first three digits fixed in place and rotate again to get 321597. Finally, keep the first four digits fixed in place and rotate the final two digits to get 321579. The resulting number is called the maximum rotation of the original number.

Write a function that takes an integer as an argument and returns the maximum rotation of that integer. You can (and probably should) use the rotateRightmostDigits function from the previous exercise.

```js
console.log(maxRotation(735291));          // 321579
console.log(maxRotation(3));               // 3
console.log(maxRotation(35));              // 53
console.log(maxRotation(105));             // 15 -- the leading zero gets dropped
console.log(maxRotation(8703529146));      // 7321609845
```

## Stack Machine Interpretation
[Launch School Reference](https://launchschool.com/exercises/6d0a9bf4)

A stack is a list of values that grows and shrinks dynamically. A stack may be implemented as an Array that uses two Array methods: Array.prototype.push and Array.prototype.pop.

A stack-and-register programming language is a language that uses a stack of values. Each operation in the language operates on a register, which can be thought of as the current value. The register is not part of the stack. An operation that requires two values pops the topmost item from the stack (i.e., the operation removes the most recently pushed value from the stack), operates on the popped value and the register value, and stores the result back in the register.

Consider a MULT operation in a stack-and-register language. It removes the value from the stack, multiplies the removed stack value with the register value, then stores the result back in the register. For example, if we start with a stack of [3, 6, 4] (where 4 is the topmost item in the stack) and a register value of 7, the MULT operation mutates the stack to [3, 6] (the 4 is removed), and the result of the multiplication, 28, is left in the register. If we do another MULT at this point, the stack is mutated to [3], and the register is left with the value 168.

Write a function that implements a miniature stack-and-register-based programming language that has the following commands (also called operations or tokens):

- n : Place a value, n, in the register. Do not modify the stack.
- PUSH : Push the register value onto the stack. Leave the value in the register.
- ADD : Pop a value from the stack and add it to the register value, storing the result in the register.
- SUB : Pop a value from the stack and subtract it from the register value, storing the result in the register.
- MULT : Pop a value from the stack and multiply it by the register value, storing the result in the register.
- DIV : Pop a value from the stack and divide the register value by the popped stack value, storing the integer result back in the register.
- REMAINDER : Pop a value from the stack and divide the register value by the popped stack value, storing the integer remainder of the division back in the register.
- POP : Remove the topmost item from the stack and place it in the register.
- PRINT : Print the register value.

All operations are integer operations (which is only important with DIV and REMAINDER).

Programs will be supplied to your language function via a string argument. Your function may assume that all arguments are valid programs — i.e., they will not do anything like trying to pop a non-existent value from the stack, and they will not contain any unknown tokens.

Initialize the stack and register to the values [] and 0, respectively.

Examples:

```js
minilang('PRINT');
// 0

minilang('5 PUSH 3 MULT PRINT');
// 15

minilang('5 PRINT PUSH 3 PRINT ADD PRINT');
// 5
// 3
// 8

minilang('5 PUSH POP PRINT');
// 5

minilang('3 PUSH 4 PUSH 5 PUSH PRINT ADD PRINT POP PRINT ADD PRINT');
// 5
// 10
// 4
// 7

minilang('3 PUSH PUSH 7 DIV MULT PRINT');
// 6

minilang('4 PUSH PUSH 7 REMAINDER MULT PRINT');
// 12

minilang('-3 PUSH 5 SUB PRINT');
// 8

minilang('6 PUSH');
// (nothing is printed because the `program` argument has no `PRINT` commands)
```

## Word to Digit
[Launch School Reference](https://launchschool.com/exercises/f9bfe9aa)

Write a function that takes a sentence string as an argument and returns that string with every occurrence of a "number word" — 'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine' — converted to its corresponding digit character.

Example:

```js
console.log(wordToDigit('Please call me at five five five one two three four. Thanks.'));
// "Please call me at 5 5 5 1 2 3 4. Thanks."
```

## Fibonacci Numbers (Recursion)
[Launch School Reference](https://launchschool.com/exercises/20bb7f80)

Write a recursive function that computes the nth Fibonacci number, where nth is an argument passed to the function.

```
F(1) = 1
F(2) = 1
F(n) = F(n - 1) + F(n - 2) where n > 2
```

Examples:

```js
console.log(fibonacci(1) === 1);
console.log(fibonacci(2) === 1);
console.log(fibonacci(3) === 2);
console.log(fibonacci(4) === 3);
console.log(fibonacci(5) === 5);
console.log(fibonacci(12) === 144);
console.log(fibonacci(20) === 6765);
```

## Fibonacci Numbers (Procedural)
[Launch School Reference](https://launchschool.com/exercises/e312c4cf)

Rewrite your recursive fibonacci function so that it computes its results without using recursion.

Note that JavaScript can accurately compute integers up to 16 digits long; this means that fibbonacci(78) is the largest Fibbonacci number that you can accurately compute with simple operations in JavaScript.

Examples:

```js
fibonacci(20);       // 6765
fibonacci(50);       // 12586269025
fibonacci(75);       // 2111485077978050
```

## Fibonacci Numbers (Memoization)
[Launch School Reference](https://launchschool.com/exercises/9b1769ca)

Our recursive fibonacci function from the previous exercise is not very efficient. It starts slowing down with an nth argument value as low as 35. One way to improve the performance of our recursive fibonacci function (and other recursive functions) is to use memoization.

Memoization is an approach that involves saving a computed answer for future reuse, instead of computing it from scratch every time it is needed. In the case of our recursive fibonacci function, using memoization saves calls to fibonacci(nth - 2) because the necessary values have already been computed by the recursive calls to fibonacci(nth - 1).

For this exercise, your objective is to refactor the recursive fibonacci function to use memoization.

# Medium Problems 2
[Launch School Reference](https://launchschool.com/exercise_sets/69a8c66a)

## Lettercase Percentage Ratio
[Launch School Reference](https://launchschool.com/exercises/f5d7eb65)

Write a function that takes a string and returns an object containing the following three properties:

- the percentage of characters in the string that are lowercase letters
- the percentage of characters that are uppercase letters
- the percentage of characters that are neither

You may assume that the string will always contain at least one character.

Examples:

```js
console.log(letterPercentages('abCdef 123'));
// { lowercase: "50.00", uppercase: "10.00", neither: "40.00" }

console.log(letterPercentages('AbCd +Ef'));
// { lowercase: "37.50", uppercase: "37.50", neither: "25.00" }

console.log(letterPercentages('123'));
// { lowercase: "0.00", uppercase: "0.00", neither: "100.00" }
```

## Triangle Sides
[Launch School Reference](https://launchschool.com/exercises/3b86afc7)

A triangle is classified as follows:

- Equilateral: All three sides are of equal length.
- Isosceles: Two sides are of equal length, while the third is different.
- Scalene: All three sides are of different lengths.

To be a valid triangle, the sum of the lengths of the two shortest sides must be greater than the length of the longest side, and every side must have a length greater than 0. If either of these conditions is not satisfied, the triangle is invalid.

Write a function that takes the lengths of the three sides of a triangle as arguments and returns one of the following four strings representing the triangle's classification: 'equilateral', 'isosceles', 'scalene', or 'invalid'.

Examples:

```js
console.log(triangle(3, 3, 3) === "equilateral");
console.log(triangle(3, 3, 1.5 === "isosceles");
console.log(triangle(3, 4, 5)  === "scalene");
console.log(triangle(0, 3, 3 === "invalid");
console.log(triangle(3, 1, 1 === "invalid");
```

## Tri-Angles
[Launch School Reference](https://launchschool.com/exercises/35f0d44e)

A triangle is classified as follows:

- Right: One angle is a right angle (exactly 90 degrees).
- Acute: All three angles are less than 90 degrees.
- Obtuse: One angle is greater than 90 degrees.

To be a valid triangle, the sum of the angles must be exactly 180 degrees, and every angle must be greater than 0. If either of these conditions is not satisfied, the triangle is invalid.

Write a function that takes the three angles of a triangle as arguments and returns one of the following four strings representing the triangle's classification: 'right', 'acute', 'obtuse', or 'invalid'.

You may assume that all angles have integer values, so you do not have to worry about floating point errors. You may also assume that the arguments are in degrees.

Examples:

```js
console.log(triangle(60, 70, 50) === "acute");
console.log(triangle(30, 90, 60) === "right");
console.log(triangle(120, 50, 10) === "obtuse");
console.log(triangle(0, 90, 90) === "invalid");
console.log(triangle(50, 50, 50) === "invalid");
```

## Unlucky Days
[Launch School Reference](https://launchschool.com/exercises/28e1b056)

Write a function that takes a year as an argument and returns the number of 'Friday the 13ths' in that year. You may assume that the year is greater than 1752 (when the modern Gregorian Calendar was adopted by the United Kingdom). You may also assume that the same calendar will remain in use for the foreseeable future.

Examples:

```js
console.log(fridayThe13ths(1986) === 1);
console.log(fridayThe13ths(2015) === 3);
console.log(fridayThe13ths(2017) === 2);
```

## Next Featured Number Higher than a Given Value
[Launch School Reference](https://launchschool.com/exercises/c1bfff07)

A featured number (something unique to this exercise) is an odd number that is a multiple of 7, with all of its digits occurring exactly once each. For example, 49 is a featured number, but 98 is not (it is not odd), 97 is not (it is not a multiple of 7), and 133 is not (the digit 3 appears twice).

Write a function that takes an integer as an argument and returns the next featured number greater than the integer. Issue an error message if there is no next featured number.

NOTE: The largest possible featured number is 9876543201.

Examples:

```js
console.log(featured(12) === 21);
console.log(featured(20) === 21);
console.log(featured(21) === 35);
console.log(featured(997) === 1029);
console.log(featured(1029) === 1043);
console.log(featured(999999) === 1023547);
console.log(featured(999999987) === 1023456987);
console.log(featured(9876543186)) === 9876543201);
console.log(featured(9876543200)) === 9876543201);
console.log(featured(9876543201)) === "There is no possible number that fulfills those requirements.");
```

## Sum Square - Square Sum
[Launch School Reference](https://launchschool.com/exercises/ac05fdea)

Write a function that computes the difference between the square of the sum of the first n positive integers and the sum of the squares of the first n positive integers.

Examples:

```js
console.log(sumSquareDifference(3) === 22);  // 22 --> (1 + 2 + 3)**2 - (1**2 + 2**2 + 3**2)
console.log(sumSquareDifference(10) === 2640);
console.log(sumSquareDifference(1) === 0);
console.log(sumSquareDifference(100) === 25164150)
```

## Bubble Sort
[Launch School Reference](https://launchschool.com/exercises/3a1e6ee4)

'Bubble Sort' is one of the simplest sorting algorithms available. Although it is not an efficient algorithm, it is an excellent exercise for student developers. In this exercise, you will write a function that sorts an array using the bubble sort algorithm.

A bubble sort works by making multiple passes (iterations) through an array. On each pass, the two values of each pair of consecutive elements are compared. If the first value is greater than the second, the two elements are swapped. This process is repeated until a complete pass is made without performing any swaps — at which point the array is completely sorted.

6 	2 	7 	1 	4 	Start: compare 6 > 2? Yes
2 	6 	7 	1 	4 	Swap
2 	6 	7 	1 	4 	6 > 7? No (no swap)
2 	6 	7 	1 	4 	7 > 1? Yes
2 	6 	1 	7 	4 	Swap
2 	6 	1 	7 	4 	7 > 4? Yes
2 	6 	1 	4 	7 	Swap

2 	6 	1 	4 	7 	2 > 6? No
2 	6 	1 	4 	7 	6 > 1? Yes
2 	1 	6 	4 	7 	Swap
2 	1 	6 	4 	7 	6 > 4? Yes
2 	1 	4 	6 	7 	Swap
2 	1 	4 	6 	7 	6 > 7? No

2 	1 	4 	6 	7 	2 > 1? Yes
1 	2 	4 	6 	7 	Swap
1 	2 	4 	6 	7 	2 > 4? No
1 	2 	4 	6 	7 	4 > 6? No
1 	2 	4 	6 	7 	6 > 7? No

1 	2 	4 	6 	7 	1 > 2? No
1 	2 	4 	6 	7 	2 > 4? No
1 	2 	4 	6 	7 	4 > 6? No
1 	2 	4 	6 	7 	6 > 7? No
1 	2 	4 	6 	7 	No swaps; all done; sorted

We can stop iterating the first time we make a pass through the array without making any swaps because this means that the entire array is sorted.

For further information — including pseudo-code that demonstrates the algorithm, as well as a minor optimization technique — see the Bubble Sort Wikipedia page.

Write a function that takes an array as an argument and sorts that array using the bubble sort algorithm described above. The sorting should be done "in-place" — that is, the function should mutate the array. You may assume that the array contains at least two elements.

Pseudocode from Wikipedia:
```
procedure bubbleSort(A : list of sortable items)
    n := length(A)
    repeat
        newn := 0
        for i := 1 to n - 1 inclusive do
            if A[i - 1] > A[i] then
                swap(A[i - 1], A[i])
                newn := i
            end if
        end for
        n := newn
    until n ≤ 1
end procedure
```

Examples:

```js
const array1 = [5, 3];
bubbleSort(array1);
console.log(array1);    // [3, 5]

const array2 = [6, 2, 7, 1, 4];
bubbleSort(array2);
console.log(array2);    // [1, 2, 4, 6, 7]

const array3 = ['Sue', 'Pete', 'Alice', 'Tyler', 'Rachel', 'Kim', 'Bonnie'];
bubbleSort(array3`);`
console.log(array3);    // ["Alice", "Bonnie", "Kim", "Pete", "Rachel", "Sue", "Tyler"]
```

# Advanced 1
[Launch School Reference](https://launchschool.com/exercise_sets/344e332e)

## Madlibs Revisited
[Launch School Reference](https://launchschool.com/exercises/0f47f2e8)

Let's build another program using madlibs. We made a similar program in the Easy exercises, but this time the requirements are a bit different.

Build a madlibs program that takes a text template as input, plugs in a selection of randomized nouns, verbs, adjectives, and adverbs into that text, and then returns it. You can build your lists of nouns, verbs, adjectives, and adverbs directly into your program. Your program should read this text and, for each line, place random words of the appropriate types into the text and return the result.

The challenge of this program isn't just about writing your solution—it's about choosing the structure of the text template. Choose the right way to structure your template and this problem becomes much easier. Consequently, this exercise is a bit more open-ended since the input is also something that you'll be defining.

Examples:

Note: The quotes in the example strings returned by the madlibs function are only shown for emphasis. These quotes are not present in the actual output strings. The words in quotes come from the list of texts and it is the madlibs function that puts them in.

```js
function madlibs(template) {
  // ...
}

// These examples use the following list of replacement texts:
adjectives: quick lazy sleepy noisy hungry
nouns: fox dog head leg tail cat
verbs: jumps lifts bites licks pats
adverbs: easily lazily noisily excitedly
------

madlibs(template1);
// The "sleepy" brown "cat" "noisily"
// "licks" the "sleepy" yellow
// "dog", who "lazily" "licks" his
// "tail" and looks around.

madlibs(template1);
// The "hungry" brown "cat" "lazily"
// "licks" the "noisy" yellow
// "dog", who "lazily" "licks" his
// "leg" and looks around.

madlibs(template2);      // The "fox" "bites" the "dog"'s "tail".

madlibs(template2);      // The "cat" "pats" the "cat"'s "head".
```

## Transpose 3 x 3
[Launch School Reference](https://launchschool.com/exercises/fb858a76)

Write a function that takes an array of arrays that represents a 3x3 matrix and returns the transpose of the matrix. You should implement the function on your own, without using any external libraries.

Take care not to modify the original matrix — your function must produce a new matrix and leave the input matrix array unchanged.

```
1  5  8
4  7  2
3  9  6
```

Is transposed to:

```
1  4  3
5  7  9
8  2  6
```

Examples:

```js
const matrix = [
  [1, 5, 8],
  [4, 7, 2],
  [3, 9, 6]
];

const newMatrix = transpose(matrix);

console.log(newMatrix);      // [[1, 4, 3], [5, 7, 9], [8, 2, 6]]
console.log(matrix);         // [[1, 5, 8], [4, 7, 2], [3, 9, 6]]
```

## Merge Sorted Lists
[Launch School Reference]()

Write a function that takes two sorted arrays as arguments and returns a new array that contains all the elements from both input arrays in sorted order.

You may not provide any solution that requires you to sort the result array. You must build the result array one element at a time in the proper order.

Your solution should not mutate the input arrays.

Examples:

```js
console.log(merge([1, 5, 9], [2, 6, 8]));      // [1, 2, 5, 6, 8, 9]
console.log(merge([1, 1, 3], [2, 2]));         // [1, 1, 2, 2, 3]
console.log(merge([], [1, 4, 5]));             // [1, 4, 5]
console.log(merge([1, 4, 5], []));             // [1, 4, 5]
```

## Merge Sort
[Launch School Reference](https://launchschool.com/exercise_sets/344e332e)

```js
/*
Merge sort is a recursive sorting algorithm that works by breaking down an array's elements into nested subarrays, then combining those nested subarrays back together in sorted order. It is best explained with an example. Given the array [9, 5, 7, 1], let's walk through the process of sorting it with merge sort. We'll start off by breaking the array down into nested subarrays:

[9, 5, 7, 1] -->
[[9, 5], [7, 1]] -->
[[[9], [5]], [[7], [1]]]

We then work our way back to a flat array by merging each pair of nested subarrays back together in the proper order:

[[[9], [5]], [[7], [1]]] -->
[[5, 9], [1, 7]] -->
[1, 5, 7, 9]

Write a function that takes an array argument and returns a new array that contains the values from the input array in sorted order. The function should sort the array using the merge sort algorithm as described above. You may assume that every element of the array will be of the same data type—either all numbers or all strings.

Feel free to use the merge function you wrote in the previous exercise.
*/

function merge(first, second) {
  let result = [];
  first = first.slice();

  second.forEach(secondNum => {
    let elementsRemoved = first.filter(firstNum => firstNum <= secondNum).length;
    result.push(...first.splice(0, elementsRemoved));
    result.push(secondNum);
  });

  if (first.length > 0) { result.push(...first) }

  return result;
}

console.log(mergeSort([9, 5, 7, 1]));            // [1, 5, 7, 9]
console.log(mergeSort([5, 3]));                  // [3, 5]
console.log(mergeSort([6, 2, 7, 1, 4]));         // [1, 2, 4, 6, 7]

console.log(mergeSort(['Sue', 'Pete', 'Alice', 'Tyler', 'Rachel', 'Kim', 'Bonnie']));
// ["Alice", "Bonnie", "Kim", "Pete", "Rachel", "Sue", "Tyler"]

console.log(mergeSort([7, 3, 9, 15, 23, 1, 6, 51, 22, 37, 54, 43, 5, 25, 35, 18, 46]));
// [1, 3, 5, 6, 7, 9, 15, 18, 22, 23, 25, 35, 37, 43, 46, 51, 54]
```

### Coderpad
/*
Write a function that takes an array argument and returns a new array that contains the values from the input array in sorted order. The function should sort the array using the merge sort algorithm as described below. You may assume that every element of the array will be of the same data type—either all numbers or all strings.

Start by breaking the array down into nested subarrays:

[9, 5, 7, 1] -->
[[9, 5], [7, 1]] -->
[[[9], [5]], [[7], [1]]]

We then work our way back to a flat array by merging each pair of nested subarrays back together in the proper order:

[[[9], [5]], [[7], [1]]] -->
[[5, 9], [1, 7]] -->
[1, 5,

Feel free to use the merge function you wrote in the previous exercise.
*/

function merge(first, second) {
  let result = [];
  first = first.slice();

  second.forEach(secondNum => {
    let elementsRemoved = first.filter(firstNum => firstNum <= secondNum).length;
    result.push(...first.splice(0, elementsRemoved));
    result.push(secondNum);
  });

  if (first.length > 0) { result.push(...first) }

  return result;
}

console.log(mergeSort([9, 5, 7, 1]));            // [1, 5, 7, 9]
console.log(mergeSort([5, 3]));                  // [3, 5]
console.log(mergeSort([6, 2, 7, 1, 4]));         // [1, 2, 4, 6, 7]

console.log(mergeSort(['Sue', 'Pete', 'Alice', 'Tyler', 'Rachel', 'Kim', 'Bonnie']));
// ["Alice", "Bonnie", "Kim", "Pete", "Rachel", "Sue", "Tyler"]

console.log(mergeSort([7, 3, 9, 15, 23, 1, 6, 51, 22, 37, 54, 43, 5, 25, 35, 18, 46]));
// [1, 3, 5, 6, 7, 9, 15, 18, 22, 23, 25, 35, 37, 43, 46, 51, 54]

# Interpretive Problem Solving
## 1000 Lights
[Launch School Reference](https://launchschool.com/exercises/e9aa7157)

```js
/*
You have a bank of switches before you, numbered from 1 to n. Every switch is connected to exactly one light that is initially off. You walk down the row of switches and toggle every one of them. You walk back to the beginning of the row and start another pass. On this second pass, you toggle switches 2, 4, 6, and so on. On the third pass, you go back to the beginning again, this time toggling switches 3, 6, 9, and so on. You continue to repeat this process until you have gone through n repetitions.

Write a program that takes one argument—the total number of switches—and returns an array of the lights that are on after n repetitions.
*/

function lightsOn(switches) {
  // ...
}

console.log(lightsOn(5));        // [1, 4]
// Detailed result of each round for `5` lights
// Round 1: all lights are on
// Round 2: lights 2 and 4 are now off;     1, 3, and 5 are on
// Round 3: lights 2, 3, and 4 are now off; 1 and 5 are on
// Round 4: lights 2 and 3 are now off;     1, 4, and 5 are on
// Round 5: lights 2, 3, and 5 are now off; 1 and 4 are on

console.log(lightsOn(100));      // [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
```

## Diamonds
[Launch School Reference](https://launchschool.com/exercises/c4ee64c7)

```js
/*
Write a function that displays a four-pointed diamond in an `nxn` grid, where `n` is an odd integer supplied as an argument to the function. You may assume that the argument will always be an odd integer.
*/

diamond(1);
// logs
// *

diamond(3);
// logs
//  *
// ***
//  *

diamond(9);
// logs
//     *
//    ***
//   *****
//  *******
// *********
//  *******
//   *****
//    ***
//     *
```

## Now I Know My ABCs
[Launch School Reference](https://launchschool.com/exercises/ff1cee69)

```js
/*
A collection of spelling blocks has two letters per block, as shown in this list:

B:O   X:K   D:Q   C:P   N:A
G:T   R:E   F:S   J:W   H:U
V:I   L:Y   Z:M

This limits the words you can spell with the blocks to only those words that do not use both letters from any given block. You can also only use each block once.

Write a function that takes a word string as an argument and returns `true` if the word can be spelled using the set of blocks, `false` otherwise. You can consider the letters to be case-insensitive when you apply the rules.
*/

console.log(isBlockWord('BATCH'));  // true
console.log(isBlockWord('BUTCH'));  // false
console.log(isBlockWord('jest'));   // true
```

## Caesar Cipher
[Launch School Reference](https://launchschool.com/exercises/1998e7d6)

Write a function that implements the Caesar Cipher. The Caesar Cipher is one of the earliest and simplest ways to encrypt plaintext so that a message can be transmitted securely. It is a substitution cipher in which each letter in a plaintext is substituted by the letter located a given number of positions away in the alphabet. For example, if the letter 'A' is right-shifted by 3 positions, it will be substituted with the letter 'D'. This shift value is often referred to as the key. The "encrypted plaintext" (ciphertext) can be decoded using this key value.

The Caesar Cipher only encrypts letters (including both lower and upper case). Any other character is left as is. The substituted letters are in the same letter case as the original letter. If the key value for shifting exceeds the length of the alphabet, it wraps around from the beginning.

Examples:

```js
// simple shift
caesarEncrypt('A', 0);       // "A"
caesarEncrypt('A', 3);       // "D"

// wrap around
caesarEncrypt('y', 5);       // "d"
caesarEncrypt('a', 47);      // "v"

// all letters
caesarEncrypt('ABCDEFGHIJKLMNOPQRSTUVWXYZ', 25);
// "ZABCDEFGHIJKLMNOPQRSTUVWXY"
caesarEncrypt('The quick brown fox jumps over the lazy dog!', 5);
// "Ymj vznhp gwtbs ktc ozrux tajw ymj qfed itl!"

// many non-letters
caesarEncrypt('There are, as you can see, many punctuations. Right?; Wrong?', 2);
// "Vjgtg ctg, cu aqw ecp ugg, ocpa rwpevwcvkqpu. Tkijv?; Ytqpi?"
```

## Vigenere Cipher
[Launch School Reference](https://launchschool.com/exercises/bc3c42c5)

The Vigenere Cipher encrypts alphabetic text using polyalphabetic substitution. It uses a series of Caesar Ciphers based on the letters of a keyword. Each letter of the keyword is treated as a shift value. For instance, the letter 'B' corresponds to a shift value of 1, and the letter 'd' corresponds to a shift value of 3. In other words, the shift value used for a letter is equal to its index value in the alphabet. This means that the letters 'a'-'z' are equivalent to the numbers 0-25. The uppercase letters 'A'-'Z' are also equivalent to 0-25.

Applying the Vigenere Cipher is done sequentially for each character by applying the current shift value to a Caesar Cipher for that particular character. To make this more concrete, let's look at the following example:

```
plaintext: Pineapples don't go on pizzas!
keyword: meat

Applying the Vigenere Cipher for each alphabetic character:
plaintext : Pine appl esdo ntgo onpi zzas
shift     : meat meat meat meat meat meat
ciphertext: Bmnx mtpe qwdh zxgh arpb ldal

result: Bmnxmtpeqw dhz'x gh ar pbldal!
```

Notice that in the example, the key isn't moved forward if the character isn't in the alphabet. Like the Caesar Cipher, the Vigenere Cipher only encrypts alphabetic characters.

Write a function that implements the Vigenere Cipher. The case of the keyword doesn't matter—in other words, the resulting encryption won't change depending on the case of the keyword's letters (e.g., 'MEat' === 'mEaT').

## Seeing Stars
[Launch School Reference](https://launchschool.com/exercises/519926e4)

```js
/*
Write a function that displays an 8-pointed star in an `nxn` grid, where n is an odd integer that is supplied as an argument to the function. The smallest such star you need to handle is a `7x7` grid (i.e., when n is 7).
*/

star(7);
// logs
// *  *  *
//  * * *
//   ***
// *******
//   ***
//  * * *
// *  *  *

star(9);
// logs
// *   *   *
//  *  *  *
//   * * *
//    ***
// *********
//    ***
//   * * *
//  *  *  *
// *   *   *
```

# Debugging
## Word Ladder
[Launch School Reference](https://launchschool.com/exercises/eb705274)

Gemma and some friends are working on a complex program to generate word ladders, transforming one word into another word one character at a time. The smallest of her tasks is to print the resulting ladder to the screen.

A "ladder" is simply an array of word strings; Gemma decides to transform this array into a single string where each word within the string is separated by a hyphen ('-'). For example, the array ['pig', 'pie', 'lie', 'lit', 'let'] should be printed as the string 'pig-pie-lie-lit-let'.

Upon first glance, Gemma's code below looks like it should work. But it throws a TypeError, saying: Cannot read property 'forEach' of undefined. Why is that?

```js
let ladder = ''

['head', 'heal', 'teal', 'tell', 'tall', 'tail'].forEach(word => {
  if (ladder !== '') {
    ladder += '-'
  }

  ladder += word
})

console.log(ladder)  // expect: head-heal-teal-tell-tall-tail
```

## Reserved Keywords
[Launch School Reference](https://launchschool.com/exercises/38b77046)

We have been asked to implement a function that determines whether or not a given word is a reserved keyword. We wrote the isReserved function below along with some test cases, but we aren't seeing the expected result. Why not? Fix the code so that it behaves as intended.

```js
const RESERVED_KEYWORDS = ['break', 'case', 'catch', 'class', 'const', 'continue',
  'debugger', 'default', 'delete', 'do', 'else', 'enum', 'export', 'extends', 'finally',
  'for', 'function', 'if', 'implements', 'import', 'in', 'instanceof', 'interface',
  'let', 'new', 'package', 'private', 'protected', 'public', 'return', 'static',
  'super', 'switch', 'this', 'throw', 'try', 'typeof', 'var', 'void', 'while',
  'with', 'yield'];

function isReserved(name) {
  RESERVED_KEYWORDS.forEach(reserved => {
    if (name === reserved) {
      return true;
    }
  });

  return false;
}

console.log(isReserved('monkey')); // false
console.log(isReserved('patch'));  // false
console.log(isReserved('switch')); // should be: true
```

## Random Recipe Generator
[Launch School Reference](https://launchschool.com/exercises/df2241fc)

One bored and hungry evening we decided to randomly generate recipes. We can't wait to see the first suggestions, but JavaScript raises a TypeError, telling us that dishName.join is not a function. What is wrong?

```js
// Picks n random elements from an array,
// and returns a new array with those elements.
function random(array, n) {
  if (n === undefined) {
    n = 1;
  }

  const elements = array.slice();
  const randomElements = [];

  while (n > 0 && elements.length > 0) {
    const randomIndex = Math.floor(Math.random() * elements.length);
    const randomElement = elements[randomIndex];

    randomElements.push(randomElement);
    elements.splice(randomIndex, 1);
    n--;
  }

  return randomElements;
}

// Ingredients

const ingredients = ['rice', 'green bell pepper', 'mushrooms', 'carrot', 'kebab',
  'spinach', 'soy bean sprouts', 'mashed potatoes', 'corn', 'cucumber', 'peas'];

const spices = ['peri peri', 'cinnamon', 'nutmeg', 'cardamom', 'ground ginger',
  'poppy seed', 'cumin'];

const extras = ['peanuts', 'sesame seeds', 'egg', 'wasabi', 'soy sauce'];

// Name

const adjective  = ['Delicious', 'Hot', 'Exotic', 'Creative', 'Festive', 'Dark'];
const firstNoun  = ['Power', 'After Work', 'Holiday', 'Disco', 'Late Night'];
const secondNoun = ['Mix', 'Delight', 'Bowl', 'Chunk', 'Surprise', 'Bliss'];

// Generate!

const dishName = random(adjective) + random(firstNoun) + random(secondNoun);
const dish = random(ingredients, 3) + random(spices, 2) + random(extras, 1);

console.log(`How about: ${dishName.join(' ')}`);
console.log(`You need: ${dish.join(', ')}`);
```

## Task List
[Launch School Reference](https://launchschool.com/exercises/f5ca6383)

We were asked to implement a task list and the following functionality:

- adding a new task
- completing a given number of existing tasks
- displaying the task list

We decided to keep things simple and model the tasks as strings. Completing a task for us simply means deleting the string from the array of tasks.

Experimenting with our code reveals that it doesn't work exactly as we expected. Find the problem and fix it.

```js
const todos = ['wash car', 'exercise', 'buy groceries', 'balance budget',
             'call plumber', 'feed fido', 'get gas',  'organize closet'];

function addTask(task) {
  if (todos.includes(task)) {
    console.log('That task is already on the list.');
  } else {
    todos.push(task);
  }
}

function completeTasks(n = 1) {
  let tasksComplete = 0;

  while (todos.length > 0 && tasksComplete < n) {
    console.log(`${todos[0]} complete!`);
    delete todos[0];
    tasksComplete++;
  }

  if (todos.length === 0) {
    console.log('All tasks complete!');
  } else {
    console.log(`${tasksComplete} tasks completed; ${todos.length} remaining.`);
  }
}

function displayTaskList() {
  console.log(`ToDo list (${todos.length} tasks):`)
  console.log('---------------------');

  for (let i = 0; i < todos.length; i++) {
    console.log(`-- ${todos[i]}`);
  }
}

// Utilizing our task manager

addTask('oil change');
addTask('dentist');
addTask('homework');

completeTasks(3);
displayTaskList();
```

## Range
[Launch School Reference](https://launchschool.com/exercises/778fc8e7)

We are assigned the task to implement a range function that returns an array of integers beginning and ending with specified start and end numbers. When only a single argument is provided, that argument should be used as the ending number and the starting number should be 0.

Check our code below. Why do the example invocations fail with an error saying Maximum call stack size exceeded? Can you fix the code, so it runs without error and satisfies the requirements?

```js
function range(start, end) {
  const range = [];

  for (let element = start; element <= end; element++) {
    range.push(element);
  }

  return range;
}

function range(end) {
  return range(0, end);
}

// Examples

console.log(range(10, 20));
console.log(range(5));
```

## Member Directory
[Launch School Reference](https://launchschool.com/exercises/bea207f7)

Caroline manages the member directory of her club and decided to implement a program she can use for doing that. Since the club is not very big, it's sufficient for her to keep the members' names and phone numbers in an object. Later she wants to add functionality that allows her to write this object to a file.

One requirement Caroline takes very seriously is input validation. She intended for her code to strictly require that only alphabetic letters be included in the members' first and last names, separated by a space. But upon making a typo when entering the information of the newest member, she realizes that isn't the case.

Figure out why not and fix the code so that it works as expected. You may also consider writing a few more test cases to insure that the input validation requirement is properly met.

```js
const memberDirectory = {
  'Jane Doe': '323-8293',
  'Margaret Asbury': '989-1111',
  'Callum Beech': '533-9090',
  'Juanita Eastman': '424-1919',
};

function isValidName(name) {
  return (/^\w+ \w+$/).test(name);
}

function isValidPhone(phone) {
  return (/^\d{3}-\d{4}$/).test(phone);
}

function validMemberInfo(name, phone) {
  return isValidName(name) && isValidPhone(phone);
}

function addMember(name, phone) {
  if (validMemberInfo(name, phone)) {
    memberDirectory[name] = phone;
  } else {
    console.log('Invalid member information.');
  }
}

addMember('Laura Carlisle', '444-2223');
addMember('Rachel Garcia', '232-1191');
addMember('Earl 5mith', '331-9191');

console.log(memberDirectory);
```

## Molecules
[Launch School Reference](https://launchschool.com/exercises/974b263e)

We decided to dip our toes into computational chemistry. Our first task is to write a function that computes the total number of [valence electrons](https://en.wikipedia.org/wiki/Valence_electron) in a molecule. Fortunately, this is relatively straightforward. For each element in the molecule, we need to know two things:

- The element's atomic number. We can get this from the periodic table and decided to simply hard-code it in a valence function for now.
- The number of atoms of that element in the molecule. We can read this directly from the string representation of the molecule.

For example, [Geosmin](https://en.wikipedia.org/wiki/Geosmin) has the string representation `C12H22O`, so it has 12 `C` atoms, 22 `H` atoms, and 1 `O` atom, summing up to `12*4 + 22*1 + 1*6 = 76` valence electrons.

We decided to write our function `valenceOfMolecule` such that it expects string representations of each element in the molecule as input, e.g. `valenceOfMolecule('C12', 'H22', 'O')`. It then returns the total number of all valence electrons in that molecule. Well, almost. Can you explain why it throws an exception and how to fix it?

```js
function valence(element) {
  switch (element) {
    case 'H': return 1;
    case 'C': return 4;
    case 'N': return 5;
    case 'O': return 6;
    // omitting the rest
  }
}

function valenceOfMolecule() {
  let sum = 0;

  arguments.forEach(atom => {
    const match   = /([a-zA-Z]+)([0-9]*)/.exec(atom);
    const element = match[1];
    const number  = parseInt(match[2]) || 1;

    sum += number * valence(element);
  });

  return sum;
}

// Example

console.log('Number of valence electrons');
console.log('---------------------------');
console.log(`Water:     ${String(valenceOfMolecule('H2', 'O'))}`);
console.log(`Propane:   ${String(valenceOfMolecule('C3', 'H8'))}`);
console.log(`Vitamin C: ${String(valenceOfMolecule('C6', 'H8', 'O6'))}`);
console.log(`Caffeine:  ${String(valenceOfMolecule('C8', 'H10', 'N4', 'O2'))}`);

// Expected output:
// Number of valence electrons
// ---------------------------
// Water:     8
// Propane:   20
// Vitamin C: 68
// Caffeine:  74
```

## Glory!
[Launch School Reference](https://launchschool.com/exercises/efacba6e)

We want to implement a role-playing game and started working on the dice roll functionality. First, study the game code. Then take a look at the example output and information provided below.

```js
// Standard role-playing dice, ranging from 4 faces to 20,
// specified in terms of minimum and maximum face value.
const d4  = {min: 1, max: 4};
const d6  = {min: 1, max: 6};
const d8  = {min: 1, max: 8};
const d10 = {min: 0, max: 9};
const d12 = {min: 1, max: 12};
const d20 = {min: 1, max: 20};

function roll({max, min}) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Toss one or more dice and sum up their face values.
function toss(...args) {
  const dice = Array.prototype.slice.call(args);

  return dice.map(roll).reduce((sum, value) => sum + value);
}

// Standard target roll tossing a 20-sided die,
// with optional bonus and penalty dice.
// Used to determine whether a character wanting to perform an action
// succeeds or fails, based on whether the sum of the dice is higher
// or lower than the relevant character trait.
// (See below for examples.)
function targetRoll(characterValue, bonus = {min: 0, max: 0}, penalty = {min: 0, max: 0}) {
  let result = toss(d20, bonus, penalty);
  // Normalize in case bonus or penalty push result out of the D20 range.
  result = Math.max(1, result);
  result = Math.min(20, result);

  console.log(`--> ${result}`);

  switch (result) {
    case 1:  automaticFail();
    case 20: automaticSuccess();
    default: result >= characterValue ? success() : fail();
  }
}

function success() {
  console.log('Your character succeeds.');
}

function fail() {
  console.log('Your character fails.');
}

function automaticSuccess() {
  console.log('Your character succeeds without effort. Glory!');
}

function automaticFail() {
  console.log('Meagre attempt. Your character fails miserably.');
}

// Example character.
const myCharacter = {
  name: 'Jenkins',
  strength: 4,
  constitution: 6,
  education: 11,
  luck: 3,
  sanity: 9,
};

// Example rolls:

// Jenkins wants to break in a door with brute force,
// so he has to roll against his strength value.
targetRoll(myCharacter.strength);

// Jenkins is challenged to a drinking contest.
// In order to determine how much he can take, he rolls against his
// constitution. Since he just ate a huge portion of pork roast, he
// gets a D4 bonus die.
targetRoll(myCharacter.constitution, {min: 0, max: 4});

// Jenkins found an ancient scroll and attempts to decipher it.
// He has to roll against his education, in order to determine
// whether he's able to read it.
targetRoll(myCharacter.education);
```

When playing around with the above program, our three test rolls result in three random values that produce the sample output below (because each dice roll produces a random value, your output may differ). The outcome of rolling 16 looks correct, but the output when we rolled values 1 and 20 doesn't make sense. For each roll, only one outcome should be displayed. What is wrong with the code?

```js
--> 1
Meagre attempt. Your character fails miserably.
Your character succeeds without effort. Glory!
Your character fails.
--> 20
Your character succeeds without effort. Glory!
Your character succeeds.
--> 16
Your character succeeds.
```

## Grade Analysis
[Launch School Reference](https://launchschool.com/exercises/3dbcdad2)

Professor Graham wrote some simple code to help him determine the average and [median](https://en.wikipedia.org/wiki/Median) scores on each of his quarterly exams, but some of the test cases are failing. Figure out why, and write the code necessary for the program to work as expected.

```js
function average(nums) {
  const sum = nums.reduce((total, num) => total + num);

  return sum / nums.length;
}

function median(nums) {
  nums.sort();

  let median;
  const length = nums.length;
  if (length % 2 === 0) {
    median = average([nums[(length / 2) - 1], nums[length / 2]]);
  } else {
    median = nums[Math.floor(length / 2)];
  }

  return median;
}

// Tests

const quarter1ExamScores = [89, 72, 100, 93, 64, 97, 82, 87, 90, 94];
const quarter2ExamScores = [76, 91, 89, 90, 91, 67, 99, 82, 91, 87];
const quarter3ExamScores = [99, 91, 88, 72, 76, 64, 94, 79, 86, 88];
const quarter4ExamScores = [100, 94, 73, 88, 82, 91, 97, 99, 80, 84];

// should all log 'true':
console.log(average(quarter1ExamScores) === 86.8);
console.log(average(quarter2ExamScores) === 86.3);
console.log(average(quarter3ExamScores) === 83.7);
console.log(average(quarter4ExamScores) === 88.8);

console.log(median(quarter1ExamScores) === 89.5);
console.log(median(quarter2ExamScores) === 89.5);
console.log(median(quarter3ExamScores) === 87);
console.log(median(quarter4ExamScores) === 89.5);
```

## Weekday Classes
[Launch School Reference](https://launchschool.com/exercises/4de6fd78)

There are a lot of exciting classes offered in our region. We wrote a small script that checks which ones are still upcoming and compatible with our calendar. We must be available to attend all sessions of a particular class in order to sign up for it. We can always arrange that on weekends, but for weekdays we have to check whether our calendar is free.

Although the code below runs, something is wrong with it. Why is everything except for the Back To The Future Movie Night in the list of compatible classes?

```js
const TODAY = toDate("2018-08-05");

function toDate(string) {
  return new Date(`${string}T00:00:00`);
}

function toString(date) {
  return `${date.getYear()}-${date.getMonth()}-${date.getDay()}`;
}

function isInThePast(date) {
  return date < TODAY;
}

function isWeekday(date) {
  return date.getDay() >= 1 &&
         date.getDay() <= 5;
}

const myCalendar = {
  "2018-08-13": ["JS debugging exercises"],
  "2018-08-14": ["Read 'Demystifying Rails'", "Settle health insurance"],
  "2018-08-15": ["Read 'Demystifying Rails'"],
  "2018-08-16": [],
  "2018-08-30": ["Drone video project plan"],
  "2018-09-10": ["Annual servicing of race bike"],
  "2018-09-12": ["Study"],
  "2018-11-02": ["Birthday Party"],
  "2018-11-03": ["Birthday Party"],
};

const offeredClasses = {
  "Back To The Future Movie Night": ["2018-07-30"],
  "Web Security Fundamentals": ["2018-09-10", "2018-09-11"],
  "Pranayama Yoga For Beginners": ["2018-08-30", "2018-08-31", "2018-09-01"],
  "Mike's Hikes": ["2018-08-16"],
  "Gordon Ramsay Master Class": ["2018-09-11", "2018-09-12"],
  "Powerboating 101": ["2018-09-15", "2018-09-16"],
  "Discover Parachuting": ["2018-11-02"],
};

function getCompatibleEvents(classes, calendar) {
  function isAvailable(date) {
    const dateStr = toString(date);
    return !calendar[dateStr] || calendar[dateStr].length === 0;
  };

  const compatibleClasses = [];

  Object.keys(classes).forEach(className => {
    const classDates = classes[className].map(toDate);

    if (classDates.some(isInThePast)) {
      return;
    }

    if (classDates.filter(isWeekday).every(isAvailable)) {
      compatibleClasses.push(className);
    }
  });

  return compatibleClasses;
}

console.log(getCompatibleEvents(offeredClasses, myCalendar));
// expected: ["Mike's Hikes", "Powerboating 101"]
```

## Pomodoro
[Launch School Reference](https://launchschool.com/exercises/d7c752a5)

The following code demonstrates the [Pomodoro technique](https://en.wikipedia.org/wiki/Pomodoro_Technique). Although it seems to work in principle, it never prints the minute count from line 11. What is wrong?

```js
var tasks = 10;
var checkmarks = 0;
var sessions = 0;
var minutes = 0;

function pomodoro() {
  console.log('Work.');

  while (minutes < 25) {
    minutes += 1;
    console.log('...' + minutes);
  }

  console.log('PLING!');

  sessions += 1;
  checkmarks += 1;

  if (checkmarks === tasks) {
    console.log('Done!');
    return;
  }

  var rest;
  if (sessions === 4) {
    sessions = 0;
    rest = 30;
  } else {
    rest = 5;
  }

  console.log('Rest for ' + rest + ' minutes.');

  var minutes = 0;
  pomodoro();
}

pomodoro();
```
