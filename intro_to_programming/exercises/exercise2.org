Exercises
https://launchschool.com/books/ruby/read/intro_exercises
Exercise 2

Same as above, but only print out values greater than 5.

#+begin_src ruby :results output
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].each { |num| puts num if num > 5 }
#+end_src

#+RESULTS:
: 6
: 7
: 8
: 9
: 10

