* greeting.rb
https://launchschool.com/books/ruby/read/more_stuff#readingstacktraces

#+begin_src ruby :results output :session
def greet(person)
  puts "Hello. " + person
end

greet("John")
greet(1)
#+end_src

#+RESULTS:
: Hello. John
: Traceback (most recent call last):
:         4: from /home/akeeba/.rbenv/versions/2.5.3/bin/irb:11:in `<main>'
:         3: from (irb):23
:         2: from (irb):19:in `greet'
:         1: from (irb):19:in `+'
: TypeError (no implicit conversion of Integer into String)

#+begin_src ruby :results output :session
def space_out_letters(person)
  return person.split("").join(" ")
end

def greet(person)
  return "H e l l o. " + space_out_letters(person)
end

def decorate_greeting(person)
  puts "" + greet(person) + ""
end

decorate_greeting("John")
decorate_greeting(1)
#+end_src

#+RESULTS:
: H e l l o. J o h n
: Traceback (most recent call last):
:         5: from /home/akeeba/.rbenv/versions/2.5.3/bin/irb:11:in `<main>'
:         4: from (irb):139
:         3: from (irb):135:in `decorate_greeting'
:         2: from (irb):131:in `greet'
:         1: from (irb):127:in `space_out_letters'
: NoMethodError (undefined method `split' for 1:Integer)
