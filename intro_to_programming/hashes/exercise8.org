Hashes
https://launchschool.com/books/ruby/read/hashes#exercises
Exercise 8

If you see this error, what do you suspect is the most likely problem?

#+begin_src bash
NoMethodError: undefined method `keys' for Array
#+end_src

A. We're missing keys in an array variable.

B. There is no method called ~keys~ for Array objects.

C. ~keys~ is an Array object, but it hasn't been defined yet.

D. There's an array of strings, and we're trying to get the string ~keys~ out of the array, but it doesn't exist.

This errors suggests that one is trying to use the ~Hash#keys~ method on an array. Arrays do not have keys, they have indexes. There is no ~Array#keys~ method. The answer is *B*.
