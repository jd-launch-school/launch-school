optional_parameters.rb
https://launchschool.com/books/ruby/read/hashes#hashesasoptionalparameters

#+begin_src ruby :results output
def greeting(name, options = {})
  if options.empty?
    puts "Hi, my name is #{name}."
  else
    puts "Hi, my name is #{name} and I'm #{options[:age]}" + 
         " years old and I live in #{options[:city]}."
  end
end

greeting("Bob")
greeting("Bob", {age: 62, city: "New York City"})

# Works without curly braces:
greeting("Sue", age: 43, city: "Burbank")
#+end_src

#+RESULTS:
: Hi, my name is Bob.
: Hi, my name is Bob and I'm 62 years old and I live in New York City.
: Hi, my name is Sue and I'm 43 years old and I live in Burbank.
