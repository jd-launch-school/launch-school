puts "Enter a number between 0 and 100:"
number = gets.chomp.to_i

def guess_number(number)
  until number > 0
    puts "#{number} is less than zero, try again!\n\n"
    puts "Enter a number between 0 and 100:"
    number = gets.chomp.to_i
  end

  puts "#{number} is between 0 and 50." if (number > 0) && (number <= 50)
  puts "#{number} is between 51 and 100." if (number > 50) && (number <= 100)
  puts "#{number} is over 100." if (number > 100)
end

guess_number(number)
