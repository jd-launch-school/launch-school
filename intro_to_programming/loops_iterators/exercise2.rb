loop do
  puts "What would you like to do? (\"STOP\" to quit):"
  answer = gets.chomp.upcase
  break if answer == "STOP"
  puts "Hi, try again!"
end
