conditional_loop.org
https://launchschool.com/books/ruby/read/loops_iterators#controllloop

#+begin_src ruby :results output
i = 0
loop do
  i += 2
  puts i
  if i == 10
    break
  end
end
#+end_src

#+RESULTS:
: 2
: 4
: 6
: 8
: 10
