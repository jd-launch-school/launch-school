fibonacci.rb
https://launchschool.com/books/ruby/read/loops_iterators#recursion

#+begin_src ruby :results output
def fibonacci(number)
  if number < 2
    number
  else
    fibonacci(number - 1) + fibonacci(number-2)
  end
end

puts fibonacci(1)
puts fibonacci(2)
puts fibonacci(3)
puts fibonacci(4)
puts fibonacci(5)
puts fibonacci(6)
puts fibonacci(7)
puts "..."
puts fibonacci(18)
puts fibonacci(19)
puts fibonacci(20)
#+end_src

#+RESULTS:
#+begin_example
1
1
2
3
5
8
13
...
2584
4181
6765
#+end_example

We give the ~fibonacci~ method the place in the fibonacci sequence we're looking for. For example, what number is at the ~6th~ place of the fibonacci sequence ~fibonacci(6)~ ... it's ~8~.
