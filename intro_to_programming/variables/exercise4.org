Variables
https://launchschool.com/books/ruby/read/variables#exercises
Exercise 4

Modify ~name.rb~ again so that it first asks the user for their first name, saves it into a variable, and then does the same for the last name. Then outputs their full name all at once.

#+begin_src ruby :results output :tangle name.rb
puts "Please enter your first name:"
first_name = gets.chomp

puts "Please enter your last name:"
last_name = gets.chomp

puts "Hello, #{first_name} #{last_name}!"
#+end_src
