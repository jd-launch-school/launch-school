puts "How old are you?:"
age = gets.chomp.to_i

4.times do |index|
  puts "In #{(index + 1) * 10} years you will be:"
  puts age + (index + 1) * 10
end
