* Sequence Count
  Small problems > Easy 9
  https://launchschool.com/exercises/02350925

** Exercise Description
Create a method that takes two integers as arguments. The first argument is a count, and the second is the first number of a sequence that your method will create. The method should return an Array that contains the same number of elements as the count argument, while the values of each element will be multiples of the starting number.

You may assume that the count argument will always have a value of 0 or greater, while the starting number can be any integer value. If the count is 0, an empty list should be returned.

Examples:

#+begin_example ruby
sequence(5, 1) == [1, 2, 3, 4, 5]
sequence(4, -7) == [-7, -14, -21, -28]
sequence(3, 0) == [0, 0, 0]
sequence(0, 1000000) == []
#+end_example

** My Solution
*** Understanding the Problem
**** Input
- two integers
  + count
  + first number (start) of sequence

**** Output
- array
  + contains same number of elements as the count
  + values of each element will be multiples of the starting number

**** Questions
- Should I validate for non-Integer inputs?

**** Rules
***** Explicit requirements
- count will be zero or greater
- starting number can be any integer, positive or negative
- if count is zero, return an empty array

***** Implicit requirements
- 

*** Examples / Test Cases
#+name: sequence_tests
#+begin_src ruby :results none :exports both :noweb yes :tangle no
def sequence_tests(tests)
  tests.each { |test| puts "#{sequence(test[0], test[1]) == test[2] ? "PASS" : "FAIL"}: sequence(#{test[0].inspect}, #{test[1].inspect}) == #{test[2].inspect}"}
end

sequence_tests([
  [5, 1, [1, 2, 3, 4, 5]], [4, -7, [-7, -14, -21, -28]],
  [3, 0, [0, 0, 0]], [0, 1000000, []]
])
#+end_src

*** Data Structure
**** Input
- Two Integers

**** Output
- Array

*** Algorithm
- My first thought is to use ~Array::new~ with a block:
  + ~Array.new(count) { |num| num * start }~ or something like that

Breaking the problem down a bit:

- First, create an array with the number of slots needed based on ~count~. Either do this with ~Array::new~ or use ~times~.
- Multiply each index + 1 in that new array by the starting number
  + For example:
    #+begin_src ruby :results output :exports both
    start = -7
    array = [1, 2, 3]
    new_array = array.map.with_index { |_, index| (index + 1) * start}
    p new_array
    #+end_src

    #+RESULTS:
    : [-7, -14, -21]

*** Code
#+name: sequence
#+begin_src ruby :results output :exports both :noweb yes :tangle yes
def sequence(count, start)
  Array.new(count) { |index| (index + 1) * start }
end

<<sequence_tests>>
#+end_src

#+RESULTS: sequence
: PASS: sequence(5, 1) == [1, 2, 3, 4, 5]
: PASS: sequence(4, -7) == [-7, -14, -21, -28]
: PASS: sequence(3, 0) == [0, 0, 0]
: PASS: sequence(0, 1000000) == []

Why does this work with zero? According to the [[https://docs.ruby-lang.org/en/2.6.0/Array.html#method-c-new][documentation on Array::new]]:

#+begin_quote
new(size=0, default=nil) 

Returns a new array.

In the first form, if no arguments are sent, the new array will be empty. 
#+end_quote

So, ~Array.new(0)~ is the default and that returns a new empty array.

** Recommended Solution
*** Solution 1
#+begin_src ruby :results none :exports both :session
def sequence(count, first)
  sequence = []
  number = first

  count.times do
    sequence << number
    number += first
  end

  sequence
end
#+end_src

*** Solution 2
#+begin_src ruby :results output :exports both
def sequence(count, first)
  (1..count).map { |value| value * first }
end
#+end_src

In the first solution, we begin by setting the scene. We initialize a ~sequence~ variable as an empty array and a ~number~ variable that we set to the value of ~first~, as it will be the first element of the sequence.

We then use the ~Integer#times~ method. It will execute the content of the block the number of times specified by the integer. In this case, on each iteration, we first append ~number~ to our ~sequence~ array and then increment ~number~ by the value of ~first~. When all iterations are completed, the ~sequence~ array contains all the elements needed and we just have to return it.

Our second solution is more concise, and maybe more difficult to parse. First we create a ~Range~ from 1 to ~count~, as we realize that the array our method needs to return will have that many elements in it. Note that ranges have access to ~Enumerable~ methods, such as ~map~. You don't need to convert it to an array before, it will be treated as one. Let's look at a simple example:

#+begin_example ruby
irb(main):008:0> (1..5).map { |num| num }
=> [1, 2, 3, 4, 5]
#+end_example

Within the block we then simply multiply the index (that is, each number of the range) by the sequential multiplier which gives us our value for each position in the array.

** Submitted Solutions
*** Uses ~step~ and ~take~
#+name: step_and_take
#+begin_src ruby :results output :exports both :noweb yes
def sequence(count, start)
    start.step(by: start).take(count)
end

<<sequence_tests>>
#+end_src

#+RESULTS: step_and_take
: PASS: sequence(5, 1) == [1, 2, 3, 4, 5]
: PASS: sequence(4, -7) == [-7, -14, -21, -28]
: PASS: sequence(3, 0) == [0, 0, 0]
: PASS: sequence(0, 1000000) == []

According to the [[https://docs.ruby-lang.org/en/2.6.0/Numeric.html#method-i-step][Numeric#step documentation]]:

#+begin_quote
step(by: step, to: limit) → an_enumerator 

Invokes the given block with the sequence of numbers starting at num, incremented by step (defaulted to 1) on each call.

The loop finishes when the value to be passed to the block is greater than limit (if step is positive) or less than limit (if step is negative), where limit is defaulted to infinity.
#+end_quote

#+begin_src ruby :results output :exports both
p -7.step(by: -7).take(4)
#+end_src

#+RESULTS:
: [-7, -14, -21, -28]

