* Grade Book
  Small problems > Easy 9
  https://launchschool.com/exercises/063fa805

** Exercise Description
Write a method that determines the mean (average) of the three scores passed to it, and returns the letter value associated with that grade.

| Numerical Score Letter | Grade |
|------------------------+-------|
| 90 <= score <= 100     | 'A'   |
| 80 <= score < 90       | 'B'   |
| 70 <= score < 80       | 'C'   |
| 60 <= score < 70       | 'D'   |
| 0 <= score < 60        | 'F'   |

Tested values are all between 0 and 100. There is no need to check for negative values or values greater than 100.

Example:

#+begin_example ruby
get_grade(95, 90, 93) == "A"
get_grade(50, 50, 95) == "D"
#+end_example

** My Solution
*** Understanding the Problem
**** Input
- three scores

**** Output
- letter grade associated with the average of the three scores

**** Questions
- Should I validate for non-Integer input?

**** Rules
***** Explicit requirements
- values must be between ~0~ and ~100~
- no need to validate for negative numbers or values greater than ~100~

***** Implicit requirements
- 

*** Examples / Test Cases
#+name: get_grade_tests
#+begin_src ruby :results none :exports both :noweb yes :tangle no
def get_grade_tests(tests)
  tests.each { |test| puts "#{get_grade(test[0], test[1], test[2]) == test[3] ? "PASS" : "FAIL"}: get_grade(#{test[0].inspect}, #{test[1].inspect}, #{test[2].inspect}) == #{test[3].inspect}"}
end

get_grade_tests([
  [95, 90, 93, "A"], [50, 50, 95, "D"],
  [81, 83, 85, "B"], [71, 73, 75, "C"],
  [33, 64, 32, "F"]
])
#+end_src

*** Data Structure
**** Input
- three Integers from 0-100

**** Output
- String

*** Algorithm
- Initially, I feel like I'll need a case statement to match the score average to letter table.
  - Ex:
  #+begin_src ruby :results output :exports both
  case
  when score >= 90 && score <= 100
    ...
  when score >= 80 && score < 90
    ...
  when score >= 70 && score < 80
    ...
  when score >= 60 && score < 70
    ...
  else
    ...
  end
  #+end_src

- Or, I could use a hash ... maybe.
#+begin_src ruby :results output :exports both
letters = {
  "A" => (90..100),
  "B" => (80...90),
  "C" => (70...80),
  "D" => (60...70),
  "E" => [*(0...60)],
}

p letters.values
#+end_src

Running out of time :)

#+RESULTS:
: [90..100, 80...90, 70...80, 60...70, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59]]

To calculate the average:
- get the sum of the three numbers and then divide by ~3.0~

To get the letter:
- if the average is between ~90~ and including ~100~, return ~'A'~
- if the average is between ~80~ and less than ~90~, return ~'B'~
- if the average is between ~70~ and less than ~80~, return ~'C'~
- if the average is between ~60~ and less than ~70~, return ~'D'~
- if the average is between ~0~ and less than ~60~, return ~'F'~

*** Code
#+name: get_grade
#+begin_src ruby :results output :exports both :noweb yes :tangle yes
def get_grade(*args)
  score = args.sum / 3

  case
  when score >= 90
    "A"
  when score >= 80 && score < 90
    "B"
  when score >= 70 && score < 80
    "C"
  when score >= 60 && score < 70
    "D"
  else
    "F"
  end
end

<<get_grade_tests>>
#+end_src

#+RESULTS: get_grade
: PASS: get_grade(95, 90, 93) == "A"
: PASS: get_grade(50, 50, 95) == "D"
: PASS: get_grade(81, 83, 85) == "B"
: PASS: get_grade(71, 73, 75) == "C"
: PASS: get_grade(33, 64, 32) == "F"

** Recommended Solution
#+begin_src ruby :results none :exports both :session
def get_grade(s1, s2, s3)
  result = (s1 + s2 + s3)/3

  case result
  when 90..100 then 'A'
  when 80..89  then 'B'
  when 70..79  then 'C'
  when 60..69  then 'D'
  else              'F'
  end
end
#+end_src

The ~result~ is just the average of the three scores. The case statement does a comparison on it. This takes advantage of the fact that the comparison used by the case statement returns ~true~ if the range includes the other object (essentially calling ~(range).include?(other_object)~.

You can therefore read it as

~when (90..100).include?(result)~

** Further Exploration
How would you handle this if there was a possibility of extra credit grades causing it to exceed 100 points?

*** My Answer
**** Tests
#+name: get_grade_tests_100
#+begin_src ruby :results none :exports both :noweb yes :tangle no
def get_grade_tests(tests)
  tests.each { |test| puts "#{get_grade(test[0], test[1], test[2]) == test[3] ? "PASS" : "FAIL"}: get_grade(#{test[0].inspect}, #{test[1].inspect}, #{test[2].inspect}) == #{test[3].inspect}"}
end

get_grade_tests([
  [95, 90, 93, "A"], [50, 50, 95, "D"],
  [81, 83, 85, "B"], [71, 73, 75, "C"],
  [33, 64, 32, "F"], [101, 102, 103, "A"],
  [0, 0, 0, 'F']
])
#+end_src

**** Code
#+name: get_grade_100
#+begin_src ruby :results output :exports both :noweb yes :tangle yes
def get_grade(*args)
  score = args.sum / 3

  case score
  when 90..    then 'A'
  when 80..89  then 'B'
  when 70..79  then 'C'
  when 60..69  then 'D'
  else              'F'
  end
end

<<get_grade_tests_100>>
#+end_src

#+RESULTS: get_grade_100
: PASS: get_grade(95, 90, 93) == "A"
: PASS: get_grade(50, 50, 95) == "D"
: PASS: get_grade(81, 83, 85) == "B"
: PASS: get_grade(71, 73, 75) == "C"
: PASS: get_grade(33, 64, 32) == "F"
: PASS: get_grade(101, 102, 103) == "A"
: PASS: get_grade(0, 0, 0) == "F"

Endless range, from the [[https://docs.ruby-lang.org/en/2.6.0/Range.html#class-Range-label-Endless+Ranges][Range documentation]]:

#+begin_quote
An “endless range” represents a semi-infinite range. Literal notation for an endless range is:

~(1..)~
# or similarly
~(1...)~

Which is equivalent to

~(1..nil)  # or similarly (1...nil)~
~Range.new(1, nil) # or Range.new(1, nil, true)~

- end of endless range is ~nil~;

#+end_quote

** Submitted Solutions
*** Uses ~ternary~
Clever, but relatively complicated to read/understand.
#+name: ternary
#+begin_src ruby :results output :exports both :noweb yes
def get_grade(*args)
  score = args.sum / 3
  score > 90 ? "A" : score > 80 ? "B" : score > 70 ? "C" : score > 60 ? "D" : "F"
end

<<get_grade_tests_100>>
#+end_src

#+RESULTS: ternary
: PASS: get_grade(95, 90, 93) == "A"
: PASS: get_grade(50, 50, 95) == "D"
: PASS: get_grade(81, 83, 85) == "B"
: PASS: get_grade(71, 73, 75) == "C"
: PASS: get_grade(33, 64, 32) == "F"
: PASS: get_grade(101, 102, 103) == "A"
: PASS: get_grade(0, 0, 0) == "F"

*** Uses ~hash~
#+name: hash
#+begin_src ruby :results output :exports both :noweb yes
GRADES_HASH = {
                 0...60  => "F",
                 60...70 => "D",
                 70...80 => "C",
                 80...90 => "B",
                 90..    => "A" # Infinite range
              }

def get_grade(*grades)
  average = grades.sum / grades.size

  GRADES_HASH.each do |range, grade|
    return grade if range.include?(average)
  end
end

<<get_grade_tests_100>>
#+end_src

#+RESULTS: hash
: PASS: get_grade(95, 90, 93) == "A"
: PASS: get_grade(50, 50, 95) == "D"
: PASS: get_grade(81, 83, 85) == "B"
: PASS: get_grade(71, 73, 75) == "C"
: PASS: get_grade(33, 64, 32) == "F"
: PASS: get_grade(101, 102, 103) == "A"
: PASS: get_grade(0, 0, 0) == "F"

