* How long are you?
  Small problems > Easy 9
  https://launchschool.com/exercises/15a07c61

** Exercise Description
Write a method that takes a string as an argument, and returns an Array that contains every word from the string, to which you have appended a space and the word length.

You may assume that words in the string are separated by exactly one space, and that any substring of non-space characters is a word.

Examples

#+begin_example ruby
word_lengths("cow sheep chicken") == ["cow 3", "sheep 5", "chicken 7"]

word_lengths("baseball hot dogs and apple pie") ==
  ["baseball 8", "hot 3", "dogs 4", "and 3", "apple 5", "pie 3"]

word_lengths("It ain't easy, is it?") == ["It 2", "ain't 5", "easy, 5", "is 2", "it? 3"]

word_lengths("Supercalifragilisticexpialidocious") ==
  ["Supercalifragilisticexpialidocious 34"]

word_lengths("") == []
#+end_example

** My Solution
*** Understanding the Problem
**** Input
- string

**** Output
- array that contains every word from the string, appended with a space and the length of the word

**** Questions
- Should I validate for non-String inputs? Probably no need since every substring of non-space characters is a word.

**** Rules
***** Explicit requirements
- input will contain a string of words separated by exactly one space
- output must have the word, one space, then the length of that word

***** Implicit requirements
- length of the word is the count of characters in the word
  + ex: ~'word'~ has four ~4~ characters

*** Examples / Test Cases
#+name: word_lengths_tests
#+begin_src ruby :results none :exports both :noweb yes :tangle no
def word_lengths_tests(tests)
  tests.each { |test| puts "#{word_lengths(test[0]) == test[1] ? "PASS" : "FAIL"}: word_lengths(#{test[0].inspect}) == #{test[1].inspect}"}
end

word_lengths_tests([
  ['cow sheep chicken', ["cow 3", "sheep 5", "chicken 7"]],
  ['baseball hot dogs and apple pie', ["baseball 8", "hot 3", "dogs 4", "and 3", "apple 5", "pie 3"]],
  ["It ain't easy, is it?", ["It 2", "ain't 5", "easy, 5", "is 2", "it? 3"]],
  ['Supercalifragilisticexpialidocious', ['Supercalifragilisticexpialidocious 34']]
])
#+end_src

*** Data Structure
**** Input
- String

**** Output
- Array

*** Algorithm
- split the string into an array of words
- iterate through the array using ~Array#map~
  + use string interpolation to construct the element for the new array
  + use ~String#length~ (~String#size~) to get the length of the string
    - Ex: ~"#{word} #{word.length}"~
- return the new array from ~map~

*** Code
#+name: word_lengths
#+begin_src ruby :results output :exports both :noweb yes :tangle yes
def word_lengths(string)
  string.split.map { |word| "#{word} #{word.length}" }
end

<<word_lengths_tests>>
#+end_src

#+RESULTS: word_lengths
: PASS: word_lengths("cow sheep chicken") == ["cow 3", "sheep 5", "chicken 7"]
: PASS: word_lengths("baseball hot dogs and apple pie") == ["baseball 8", "hot 3", "dogs 4", "and 3", "apple 5", "pie 3"]
: PASS: word_lengths("It ain't easy, is it?") == ["It 2", "ain't 5", "easy, 5", "is 2", "it? 3"]
: PASS: word_lengths("Supercalifragilisticexpialidocious") == ["Supercalifragilisticexpialidocious 34"]

** Recommended Solution
#+begin_src ruby :results none :exports both :session
def word_lengths(string)
  words = string.split

  words.map do |word|
    word + ' ' + word.length.to_s
  end
end
#+end_src

Let's take it step by step. First we want to split the separate words into an array:

#+begin_example ruby
irb(main):001:0> string = "cow sheep chicken"
=> "cow sheep chicken"
irb(main):002:0> string.split
=> ["cow", "sheep", "chicken"]
#+end_example

We store this word array in a ~words~ variable. Then we realize we need to modify each word separately, that is to /transform/ our ~words~ array. This is a good use case for ~Array#map~. Remember that map will return a new array, of the same size as the caller array, with each element replaced according to the return value of the block. Let's test this out with a simple example:

#+begin_example ruby
irb(main):003:0> ["cow", "sheep", "chicken"].map { |word| word + '!' }
=> ["cow!", "sheep!", "chicken!"]
#+end_example

This is close, but not exactly what we want. We can use string concatenation to combine the word with its length. Remember that the length will be an integer and needs to be converted to a string before you can concatenate them.

We could also use string interpolation and method chaining to solve this problem in one line:

#+begin_example ruby
def word_lengths(string)
  string.split.map { |word| "#{word} #{word.length}" }
end
#+end_example

** Submitted Solutions
*** Uses ~each_with_object~
Some people who submitted solutions are using ~Enumerable#each_with_object~. I'm not sure why they think that's preferable over ~Array#map~. I'll include an example of this below, but only for comparison sake, not because I think it's a good way to do this.

#+name: each_with_object
#+begin_src ruby :results output :exports both :noweb yes
def word_lengths(words)
  words.split(' ').each_with_object([]) {|word, array| array << "#{word} #{word.length}"}
end

<<word_lengths_tests>>
#+end_src

#+RESULTS: each_with_object
: PASS: word_lengths("cow sheep chicken") == ["cow 3", "sheep 5", "chicken 7"]
: PASS: word_lengths("baseball hot dogs and apple pie") == ["baseball 8", "hot 3", "dogs 4", "and 3", "apple 5", "pie 3"]
: PASS: word_lengths("It ain't easy, is it?") == ["It 2", "ain't 5", "easy, 5", "is 2", "it? 3"]
: PASS: word_lengths("Supercalifragilisticexpialidocious") == ["Supercalifragilisticexpialidocious 34"]

This is basically doing the same thing as ~Array#map~ but with more code.

*** Uses ~zip~
While we're on the topic of interesting solutions, this solution uses ~zip~ and two arrays to solve this. It seems like a lot of extra work, but it's interesting.

#+name: zip
#+begin_src ruby :results output :exports both :noweb yes
def word_lengths(str)
  arry1 = str.split
  arry2 = arry1.map(&:size)
  arry1.zip(arry2).map { |word| word.join(' ') }
end

<<word_lengths_tests>>
#+end_src

~arry1~ is the original string converted to an array of words
~arry2~ is the return value of ~map~ where each word is transformed into its size

Here's an example of that:

#+begin_src ruby :results output :exports both
array = ['a', 'bb', 'ccc']
p array.map(&:size)
#+end_src

#+RESULTS:
: [1, 2, 3]

The ~&:size~ here is shorthand for the block ~{ |word| word.size }~:

#+begin_src ruby :results output :exports both
array = ['a', 'bb', 'ccc']
p array.map { |word| word.size }
#+end_src

#+RESULTS:
: [1, 2, 3]

~arry1.zip(arry2)~ creates a new array with the indexes from each array as sub-arrays.

Example:

#+begin_src ruby :results output :exports both
array1 = ['a', 'b', 'c']
array2 = [1, 2, 3]
p array1.zip(array2)
#+end_src

#+RESULTS:
: [["a", 1], ["b", 2], ["c", 3]]

~.map { |word| word.join(' ') }~ then iterates through the new array created from ~zip~ and joins each sub-array, adding a space between each element of each sub-array. This ultimately returns the format needed to pass the tests.

It's interesting, but not as succinct as just using ~map~.

