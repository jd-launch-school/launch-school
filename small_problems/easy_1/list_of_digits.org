List of Digits
Small Problems > Easy 1
https://launchschool.com/exercises/30e147eb

Write a method that takes one argument, a positive integer, and returns a list of digits in the number.

Examples:

#+begin_src ruby :results output
puts digit_list(12345) == [1, 2, 3, 4, 5]     # => true
puts digit_list(7) == [7]                     # => true
puts digit_list(375290) == [3, 7, 5, 2, 9, 0] # => true
puts digit_list(444) == [4, 4, 4]             # => true
#+end_src

* My Solution
** Understanding the Problem
*** Input
- integer
  + positive
- how to handle negative?
- how to handle zero?
- how to handle non-Integer?
- how to handle floats?
- how to handle leading zeros?

*** Output
- list of digits in the number

** Examples / Test Cases
digit_list(12345)  => [1, 2, 3, 4, 5]
digit_list(7)      => [7]
digit_list(375290) => [3, 7, 5, 2, 9, 0]
digit_list(444)    => [4, 4, 4]
digit_list(0)      => [0]
digit_list(-1)     => "Provided argument must be positive."

** Data Structure
*** Input
- Integer

*** Output
- Array

** Algorithm
- Check if number is an integer.
- Check if number is positive.
- Use ~Integer#digits~ to break out each digit
- Use ~Array#reverse~ to reverse the array so it matches the original order

** Code
#+begin_src ruby :results output
def digit_list(num)
  return "Provided argument must be an integer." if !num.is_a? Integer
  return "Provided argument must be positive." if num.negative?
  num.digits.reverse
end

puts digit_list(12345) == [1, 2, 3, 4, 5]
puts digit_list(7) == [7]
puts digit_list(375290) == [3, 7, 5, 2, 9, 0]
puts digit_list(444) == [4, 4, 4]
puts digit_list(0) == [0]
puts digit_list(-1) == "Provided argument must be positive."
puts digit_list("test") == "Provided argument must be an integer."
puts digit_list(5.5) == "Provided argument must be an integer."
puts digit_list(007) == [7]
#+end_src

#+RESULTS:
: true
: true
: true
: true
: true
: true
: true
: true
: true

* Recommended Solution 1
#+begin_src ruby :results output
  def digit_list(number)
    return "Provided argument must be an integer." if !number.is_a? Integer
    return "Provided argument must be positive." if number.negative?
    digits = []
    loop do
      number, remainder = number.divmod(10)
      digits.unshift(remainder)
      break if number == 0
    end
    digits
  end

  puts digit_list(12345) == [1, 2, 3, 4, 5]
  puts digit_list(7) == [7]
  puts digit_list(375290) == [3, 7, 5, 2, 9, 0]
  puts digit_list(444) == [4, 4, 4]
  puts digit_list(0) == [0]
  puts digit_list(-1) == "Provided argument must be positive."
  puts digit_list("test") == "Provided argument must be an integer."
  puts digit_list(5.5) == "Provided argument must be an integer."
  puts digit_list(007) == [7]
#+end_src

#+RESULTS:
: true
: true
: true
: true
: true
: true
: true
: true
: true

* Recommended Solution 2
#+begin_src ruby :results output
def digit_list(number)
  return "Provided argument must be an integer." if !number.is_a? Integer
  return "Provided argument must be positive." if number.negative?
  number.to_s.chars.map(&:to_i)
end

  puts digit_list(12345) == [1, 2, 3, 4, 5]
  puts digit_list(7) == [7]
  puts digit_list(375290) == [3, 7, 5, 2, 9, 0]
  puts digit_list(444) == [4, 4, 4]
  puts digit_list(0) == [0]
  puts digit_list(-1) == "Provided argument must be positive."
  puts digit_list("test") == "Provided argument must be an integer."
  puts digit_list(5.5) == "Provided argument must be an integer."
  puts digit_list(007) == [7]
#+end_src

#+RESULTS:
: true
: true
: true
: true
: true
: true
: true
: true
: true

