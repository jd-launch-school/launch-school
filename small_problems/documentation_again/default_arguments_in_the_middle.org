Default Arguments in the Middle
Small Problems > Documentation Again
https://launchschool.com/exercises/c3d56d90

Consider the following method and a call to that method:

#+begin_src ruby :results output
def my_method(a, b = 2, c = 3, d)
  p [a, b, c, d]
end

my_method(4, 5, 6)
#+end_src

Use the ruby documentation to determine what this code will print.

* My Answer
([[https://docs.ruby-lang.org/en/2.6.0/syntax/calling_methods_rdoc.html#label-Default+Positional+Arguments][Reference]])

The ~b = 2~ and ~c = 3~ arguments are called **default positional arguments**. This will print ~[4, 5, 3, 6]~. First, ruby assigns ~4~ to ~a~ and ~6~ to ~d~. Then, ruby looks at the default positional arguments and assigns ~5~ to ~b~, leaving the default ~3~ assigned to ~c~.
