def valid_integer(input)
  (input.is_a? String) && /^\s*[0]*\d+[0]*\s*$/.match?(input)
  #(input.is_a? String) && input.to_i >= 0 && input.to_i.to_s == input
end

def retrieve_integer
  loop do
    input = gets.chomp
    return input.to_i if valid_integer(input)

    puts "Invalid input."
  end
end

def prompt(number_position, numbers)
  puts "Enter the #{number_position} number:"
  numbers.push(retrieve_integer)
end

numbers = []

prompt('1st', numbers)
prompt('2nd', numbers)
prompt('3rd', numbers)
prompt('4th', numbers)
prompt('5th', numbers)

puts "Enter the last number:"
last_number = retrieve_integer

if numbers.include?(last_number)
  puts "The number #{last_number} appears in #{numbers}."
else
  puts "The number #{last_number} does not appear in #{numbers}."
end
