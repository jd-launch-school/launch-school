def prompt(message)
  puts "==> #{message}"
end

def valid_integer(input)
  (input.is_a? String) && /^\s*[0]*\d+[0]*\s*$/.match?(input)
end

def add(first, second)
  first + second
end

def subtract(first, second)
  first - second
end

def multiply(first, second)
  first * second
end

def divide(first, second)
  return first / second unless second.zero?

  "Can't divide by zero."
end

def modulo(first, second)
  return first % second unless second.zero?

  "Can't divide by zero."
end

def exponent(first, second)
  return first ** second unless second > 100
  return 0 if first.zero?

  "Power is too high, must be less than or equal to 100."
end

def retrieve_number
  loop do
    input = gets.chomp
    return input.to_i if valid_integer(input)

    puts "That is not a valid positive Integer."
  end
end

prompt("Enter the first number:")
first_number = retrieve_number

prompt("Enter the second number:")
second_number = retrieve_number

prompt("#{first_number} + #{second_number} = #{add(first_number, second_number)}")
prompt("#{first_number} - #{second_number} = #{subtract(first_number, second_number)}")
prompt("#{first_number} * #{second_number} = #{multiply(first_number, second_number)}")
prompt("#{first_number} / #{second_number} = #{divide(first_number, second_number)}")
prompt("#{first_number} % #{second_number} = #{modulo(first_number, second_number)}")
prompt("#{first_number} ** #{second_number} = #{exponent(first_number, second_number)}")
