* Odd Lists
Small Problems > Easy 3
https://launchschool.com/exercises/7ced73ba

** Exercise Description
Write a method that returns an Array that contains every other element of an ~Array~ that is passed in as an argument. The values in the returned list should be those values that are in the 1st, 3rd, 5th, and so on elements of the argument Array.

Examples:

#+begin_example ruby
oddities([2, 3, 4, 5, 6]) == [2, 4, 6]
oddities([1, 2, 3, 4, 5, 6]) == [1, 3, 5]
oddities(['abc', 'def']) == ['abc']
oddities([123]) == [123]
oddities([]) == []
#+end_example

** My Solution
*** Understanding the Problem
**** Input
- an array
  + single element arrays are OK
  + an empty array returns an empty array
  + elements of the array can be of any type
  + should I validate to make sure an array is provided?

**** Output
- every other element of the given array

**** Rules
- start with the first element at index ~0~
- skip next element
- add element at index ~2~
- skip next
- and so on...

*** Examples / Test Cases
#+begin_src ruby :results output :exports both :session :tangle no
def oddities_tests(tests)
  tests.each { |test| puts "#{oddities(test[0]) == test[1] ? "PASS" : "FAIL"}: oddities(#{test[0].inspect}) == #{test[1].inspect}"}
end

oddities_tests([
  [[2, 3, 4, 5, 6], [2, 4, 6]], [[1, 2, 3, 4, 5, 6], [1, 3, 5]], [['abc', 'def'], ['abc']],
  [[123], [123]], [[], []], [[1, 1, 2, 2, 3, 4, 4], [1, 2, 3, 4]]
])
#+end_src

#+RESULTS:
: PASS: oddities([2, 3, 4, 5, 6]) == [2, 4, 6]
: PASS: oddities([1, 2, 3, 4, 5, 6]) == [1, 3, 5]
: PASS: oddities(["abc", "def"]) == ["abc"]
: PASS: oddities([123]) == [123]
: PASS: oddities([]) == []
: PASS: oddities([1, 1, 2, 2, 3, 4, 4]) == [1, 2, 3, 4]

*** Data Structure
**** Input
- Array

**** Output
- Array

*** Algorithm
- Iterate through the given array
- if the index of the element is odd, keep it
- else, don't add the element to the new array
  + use ~Array#filter~, ~Enumerator#with_index~ and ~%~ (modulo)
  + ex: array.filter.with_index( |item, index| index.even? )
- return new array

*** Code
#+begin_src ruby :results none :exports both :session :tangle yes
def oddities(array)
  array.filter.with_index { |_, index| index.even? }
end
#+end_src

** Recommended Solution
#+begin_src ruby :results output :exports both
def oddities(array)
  odd_elements = []
  index = 0
  while index < array.size
    odd_elements << array[index]
    index += 2
  end
  odd_elements
end
#+end_src

This problem can be slightly confusing because we want the 1st, 3rd, 5th, and so elements of the array, but these correspond to elements with indexes 0, 2, 4, etc. As long as you keep that in mind, there are many different ways to solve this problem correctly.

Our solution takes the most basic approach; rather than using any of a number of different Array methods, we use a simple ~while~ loop, incrementing our index by 2 with each iteration. For each iteration, we add the element value to our result Array, ~odd_elements~.

** Further Exploration
*** every other even
Write a companion method that returns the 2nd, 4th, 6th, and so on elements of an array.

**** Tests
#+begin_src ruby :results output :exports both :session :tangle no
def oddities_tests(tests)
  tests.each { |test| puts "#{oddities(test[0]) == test[1] ? "PASS" : "FAIL"}: oddities(#{test[0].inspect}) == #{test[1].inspect}"}
end

oddities_tests([
  [[2, 3, 4, 5, 6], [3, 5]], [[1, 2, 3, 4, 5, 6], [2, 4, 6]], [['abc', 'def'], ['def']],
  [[123], []], [[], []], [[1, 1, 2, 2, 3, 4, 4], [1, 2, 4]]
])
#+end_src

#+RESULTS:
: PASS: oddities([2, 3, 4, 5, 6]) == [3, 5]
: PASS: oddities([1, 2, 3, 4, 5, 6]) == [2, 4, 6]
: PASS: oddities(["abc", "def"]) == ["def"]
: PASS: oddities([123]) == []
: PASS: oddities([]) == []
: PASS: oddities([1, 1, 2, 2, 3, 4, 4]) == [1, 2, 4]

**** Code
#+begin_src ruby :results none :exports both :session
def oddities(array)
  even_elements = []
  counter = 0

  while counter < array.size do
    counter.odd? && (even_elements << array[counter])
    counter += 1
  end

  even_elements
end
#+end_src

*** two additional ways
Try to solve this exercise in at least 2 additional ways.

**** Using ~loop~
***** Tests
#+begin_src ruby :results output :exports both :session :tangle no
def oddities_tests(tests)
  tests.each { |test| puts "#{oddities(test[0]) == test[1] ? "PASS" : "FAIL"}: oddities(#{test[0].inspect}) == #{test[1].inspect}"}
end

oddities_tests([
  [[2, 3, 4, 5, 6], [2, 4, 6]], [[1, 2, 3, 4, 5, 6], [1, 3, 5]], [['abc', 'def'], ['abc']],
  [[123], [123]], [[], []], [[1, 1, 2, 2, 3, 4, 4], [1, 2, 3, 4]]
])
#+end_src

#+RESULTS:
: PASS: oddities([2, 3, 4, 5, 6]) == [2, 4, 6]
: PASS: oddities([1, 2, 3, 4, 5, 6]) == [1, 3, 5]
: PASS: oddities(["abc", "def"]) == ["abc"]
: PASS: oddities([123]) == [123]
: PASS: oddities([]) == []
: PASS: oddities([1, 1, 2, 2, 3, 4, 4]) == [1, 2, 3, 4]

***** Code
#+begin_src ruby :results none :exports both :session
def oddities(array)
  odd_items = []
  counter = 0

  loop do
    break if counter >= array.size

    counter.even? && odd_items << array[counter]
    counter += 1
  end

  odd_items
end
#+end_src

**** Using ~Enumerable#each_with_index~
***** Tests
#+begin_src ruby :results output :exports both :session :tangle no
def oddities_tests(tests)
  tests.each { |test| puts "#{oddities(test[0]) == test[1] ? "PASS" : "FAIL"}: oddities(#{test[0].inspect}) == #{test[1].inspect}"}
end

oddities_tests([
  [[2, 3, 4, 5, 6], [2, 4, 6]], [[1, 2, 3, 4, 5, 6], [1, 3, 5]], [['abc', 'def'], ['abc']],
  [[123], [123]], [[], []], [[1, 1, 2, 2, 3, 4, 4], [1, 2, 3, 4]]
])
#+end_src

#+RESULTS:
: PASS: oddities([2, 3, 4, 5, 6]) == [2, 4, 6]
: PASS: oddities([1, 2, 3, 4, 5, 6]) == [1, 3, 5]
: PASS: oddities(["abc", "def"]) == ["abc"]
: PASS: oddities([123]) == [123]
: PASS: oddities([]) == []
: PASS: oddities([1, 1, 2, 2, 3, 4, 4]) == [1, 2, 3, 4]

***** Code
#+begin_src ruby :results none :exports both :session
def oddities(array)
  odd_items = []
  array.each_with_index { |item, index| index.even? && odd_items << item }
  odd_items
end
#+end_src

** Submitted Solutions
*** Using ~Array#reject~
**** Tests
#+begin_src ruby :results output :exports both :session :tangle no
def oddities_tests(tests)
  tests.each { |test| puts "#{oddities(test[0]) == test[1] ? "PASS" : "FAIL"}: oddities(#{test[0].inspect}) == #{test[1].inspect}"}
end

oddities_tests([
  [[2, 3, 4, 5, 6], [2, 4, 6]], [[1, 2, 3, 4, 5, 6], [1, 3, 5]], [['abc', 'def'], ['abc']],
  [[123], [123]], [[], []], [[1, 1, 2, 2, 3, 4, 4], [1, 2, 3, 4]]
])
#+end_src

#+RESULTS:
: PASS: oddities([2, 3, 4, 5, 6]) == [2, 4, 6]
: PASS: oddities([1, 2, 3, 4, 5, 6]) == [1, 3, 5]
: PASS: oddities(["abc", "def"]) == ["abc"]
: PASS: oddities([123]) == [123]
: PASS: oddities([]) == []
: PASS: oddities([1, 1, 2, 2, 3, 4, 4]) == [1, 2, 3, 4]

**** Code
#+begin_src ruby :results none :exports both :session
def oddities(array)
  array.reject.with_index { |_, index| index.odd? }
end
#+end_src

