def valid_input?(input)
  input.match?(/.*[A-Za-z]+.*/)
end

def count_characters(input)
  input.gsub(/\s/, '').chars.length
end

def retrieve_input
  loop do
    input = gets.chomp
    return input if valid_input?(input)

    puts "Input is invalid. Please try again."
  end
end

def again?
  loop do
    input = gets.chomp
    return input if %w(y yes n no).include?(input.downcase)

    puts "Must be either (y)es or (n)o."
  end
end

loop do
  print "Please write word or multiple words: "
  input = retrieve_input

  puts "There are #{count_characters(input)} characters in \"#{input}\"."
  puts "Do you want to count another word or words?"

  break unless %w(y yes).include?(again?)
end

puts "Goodbye!"
