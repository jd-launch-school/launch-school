FEET_TO_INCH = 144
FEET_TO_CM = 929.0304

def area(length, width)
  area_square_feet = length.to_f * width.to_f
  area_square_inches = area_square_feet * FEET_TO_INCH
  area_square_centimeters = area_square_feet * FEET_TO_CM

  [area_square_feet, area_square_inches, area_square_centimeters]
end

puts "Enter the length of the room in feet:"
length = gets.chomp

puts "Enter the width of the room in feet:"
width = gets.chomp

square_feet, square_inches, square_centimeters = area(length, width)

puts "The area of the room is #{square_feet} square feet" \
     " (#{square_inches} square inches, #{square_centimeters} square centimeters)"
