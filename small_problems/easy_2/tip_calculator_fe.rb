bill = ''
loop do
  print "What is the bill? "
  bill = gets.chomp.to_f

  break unless bill <= 0
end

tip = ''
loop do
  print "What is the tip percentage? "
  tip = gets.chomp.to_f / 100

  break unless tip <= 0
end

calculate_tip = (bill * tip)
calculate_total = (bill + calculate_tip)

puts "\nThe tip is $#{format("%.2f", calculate_tip)}."
puts "The total is $#{format("%.2f", calculate_total)}."
