def area(length, width)
  area_meters = length.to_f * width.to_f
  area_square_feet = (area_meters * 10.7638).round(2)

  [area_meters, area_square_feet]
end

puts "Enter the length of the room in meters:"
length = gets.chomp

puts "Enter the width of the room in meters:"
width = gets.chomp

square_meters, square_feet = area(length, width)

puts "The area of the room is #{square_meters} square meters (#{square_feet} square feet)"
