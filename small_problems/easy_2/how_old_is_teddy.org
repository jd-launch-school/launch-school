How old is Teddy?
Small Problems > Easy 2
https://launchschool.com/exercises/84376930

Build a program that randomly generates a prints Teddy's age. To get the age, you should generate a random number between 20 and 200.

Example Output:
#+begin_example
Teddy is 69 years old!
#+end_example

* My Solution
** Understanding the Problem
*** Input
- no input

*** Output
- string: "Teddy is #{num} years old!"

*** Rules
- generate a random number between 20 and 200
- random numbers can be created using ~Kernel#rand~ and a range
- description says "between" ... does this mean including or excluding 20 and 200?
- return value or print?

** Examples / Test Cases
Not sure how to test this since there's no input.

** Data Structure
*** Input
- none

*** Output
- string

** Algorithm
- calculate random number between 20 and 200 inclusive
- print string

** Code
#+begin_src ruby :results output :session
def teddy_age
  "Teddy is #{rand(20..200)} years old!"
end

puts teddy_age
puts teddy_age
puts teddy_age
#+end_src

#+RESULTS:
: Teddy is 89 years old!
: Teddy is 169 years old!
: Teddy is 28 years old!

* Recommended Solution
#+begin_src ruby :results output
age = rand(20..200)
puts "Teddy is #{age} years old!"
#+end_src

#+RESULTS:
: Teddy is 28 years old!

* Further Exploration
#+begin_src ruby :results output :tangle how_old_is_teddy.rb
puts "What is your name?"
name = gets.chomp
age = rand(1..100)
puts "#{name.empty? ? "Teddy" : name} is #{age} years old!"
#+end_src


