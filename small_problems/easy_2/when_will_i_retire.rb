#require 'date'

print 'What is your age? '
age = gets.chomp.to_i

print 'At what age would you like to retire? '
age_at_retirement = gets.chomp.to_i

remaining_work_years = age_at_retirement - age
current_year = Date.today.year
year_of_retirement = current_year + remaining_work_years

puts "\nIt's #{current_year}. You will retire in #{year_of_retirement}."
puts "You have only #{remaining_work_years} years of work to go!"
