Tip Calculator
Small Problems > Easy 2
https://launchschool.com/exercises/90d719d7

Create a simple tip calculator. The program should prompt for a bill amount and a tip rate. The program must compute the tip and then display both the tip and the total amount of the bill.

Example:

#+begin_example
What is the bill? 200
What is the tip percentage? 15

The tip is $30.0
The total is $230.0
#+end_example

* My Solution
** Understanding the Problem
*** Input
- bill amount
  + handle zero?
  + handle negative?
  + handle non-numbers?

- tip rate
  + handle zero?
  + handle negative?
  + handle non-numbers?
  + handle decimals?

*** Output
- tip
- total amount

*** Rules
- convert tip to decimal:
  + tip / 100
- tip calculation:
  + total amount * tip_as_decimal
- total calculation:
  + total + tip
  + OR: total * (1 + tip_as_decimal)

** Examples / Test Cases
#+begin_src ruby :results output :session
def tests
  puts "#{calculate(200, 15) == [30, 230.0]}: calculate(200, 15) == [30, 230.0]"
  puts "#{}: "
  puts "#{}: "
end
#+end_src

** Data Structure
*** Input
- bill as Integer
- tip as Integer

*** Output
- Array of tip and total

** Algorithm
- get bill input
- validate bill input
- get tip input
- validate tip input
- convert tip to decimal
- calculate tip
- calculate total
- return array with tip and total

** Code
#+begin_src ruby :results output :session :tangle tip_calculator.rb
bill = ''
loop do
  print "What is the bill? "
  bill = gets.chomp.to_f

  break unless bill <= 0
end

tip = ''
loop do
  print "What is the tip percentage? "
  tip = gets.chomp.to_f / 100

  break unless tip <= 0
end

calculate_tip = (bill * tip).round(2)
calculate_total = (bill + calculate_tip).round(2)

puts "\nThe tip is $#{calculate_tip}."
puts "The total is $#{calculate_total}."
#+end_src

* Recommended Solution
#+begin_src ruby :results output
print 'What is the bill? '
bill = gets.chomp
bill = bill.to_f

print 'What is the tip percentage? '
percentage = gets.chomp
percentage = percentage.to_f

tip   = (bill * (percentage / 100)).round(2)
total = (bill + tip).round(2)

puts "The tip is $#{tip}"
puts "The total is $#{total}"
#+end_src

* Further Exploration
Our solution prints the results as ~$30.0~ and ~$230.0~ instead of the more usual ~$30.00~ and ~$230.00~. Modify your solution so it always prints the results with 2 decimal places.

Hint: You will likely need ~Kernel#format~ for this.

#+begin_src ruby :results output :tangle tip_calculator_fe.rb
bill = ''
loop do
  print "What is the bill? "
  bill = gets.chomp.to_f

  break unless bill <= 0
end

tip = ''
loop do
  print "What is the tip percentage? "
  tip = gets.chomp.to_f / 100

  break unless tip <= 0
end

calculate_tip = (bill * tip)
calculate_total = (bill + calculate_tip)

puts "\nThe tip is $#{format("%.2f", calculate_tip)}."
puts "The total is $#{format("%.2f", calculate_total)}."
#+end_src
