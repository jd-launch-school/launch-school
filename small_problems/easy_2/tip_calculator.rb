bill = ''
loop do
  print "What is the bill? "
  bill = gets.chomp.to_f

  break unless bill <= 0
end

tip = ''
loop do
  print "What is the tip percentage? "
  tip = gets.chomp.to_f / 100

  break unless tip <= 0
end

calculate_tip = (bill * tip).round(2)
calculate_total = (bill + calculate_tip).round(2)

puts "\nThe tip is $#{calculate_tip}."
puts "The totla is $#{calculate_total}."
