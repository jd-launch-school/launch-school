def valid_name?(name)
  /^[A-Za-z]+[!]?$/.match(name)
end

name = ''
loop do
  print "What is your name? "
  name = gets.chomp
  break if valid_name?(name)
  puts "Invalid Name. Please try again."
end

if name.include?("!")
  puts "HELLO #{name.chop.upcase}. WHY ARE WE SCREAMING?"
else
  puts "Hello #{name.capitalize}."
end
