def valid_integer(input)
  /^[1-9]+\d*$/.match?(input)
end

def valid_string(input)
  /[s, p]/i.match?(input)
end

# def sum(num)
#   (1..num).reduce(:+)
# end

def product(num)
  (1..num).reduce(:*)
end

def sum(num)
  (1..num).each(:+)
end

def prompt(text)
  puts ">> #{text}"
end

def retrieve_ending_integer
  loop do
    input = gets.chomp

    if valid_integer(input) 
      return input.to_i 
    else
      prompt('Input must be an Integer greater than zero')
    end
  end
end

def retrieve_operation
  loop do
    input = gets.chomp

    if valid_string(input) 
      return input.downcase
    else
      prompt('Operation can only be "s", "S", "p", or "P"')
    end
  end
end

prompt("Please enter an integer greater than 0:")
ending_integer = retrieve_ending_integer

prompt("Enter 's' to compute the sum, 'p' to compute the product.")
operation = retrieve_operation

result = if operation == 's'
           sum(ending_integer)
         else
           product(ending_integer)
         end

puts "The #{operation == 's' ? 'sum' : 'product'} of the integers between 1 and #{ending_integer} is #{result}."
