* 1000 Lights
  Small problems > Medium 1
  https://launchschool.com/exercises/61d687f4

** Exercise Description
You have a bank of switches before you, numbered from 1 to ~n~. Each switch is connected to exactly one light that is initially off. You walk down the row of switches and toggle every one of them. You go back to the beginning, and on this second pass, you toggle switches ~2~, ~4~, ~6~, and so on. On the third pass, you go back again to the beginning and toggle switches ~3~, ~6~, ~9~, and so on. You repeat this process and keep going until you have been through ~n~ repetitions.

Write a method that takes one argument, the total number of switches, and returns an Array that identifies which lights are on after ~n~ repetitions.

Example with ~n = 5~ lights:

    - round 1: every light is turned on
    - round 2: lights ~2~ and ~4~ are now off; ~1~, ~3~, ~5~ are on
    - round 3: lights ~2~, ~3~, and ~4~ are now off; ~1~ and ~5~ are on
    - round 4: lights ~2~ and ~3~ are now off; ~1~, ~4~, and ~5~ are on
    - round 5: lights ~2~, ~3~, and ~5~ are now off; ~1~ and ~4~ are on

The result is that ~2~ lights are left on, lights ~1~ and ~4~. The return value is ~[1, 4]~.

With ~10~ lights, ~3~ lights are left on: lights ~1~, ~4~, and ~9~. The return value is ~[1, 4, 9]~.

** My Solution
*** Understanding the Problem
**** Input
- total number of switches in the bank of lights ~n~

**** Output
- the lights that are on after the given number ~n~ repetitions

**** Questions
- Do I need to do any validation?
- How will zero be handled?
- What about negative numbers?

**** Rules
***** Explicit requirements
- all switches are initially off 
- first round toggles every switch
- next round toggles every second switch from off to on/on to off
- next round toggles every third switch from off to on/on to off
- next round toggles every fourth switch from off to on/on to off

***** Implicit requirements
- 

*** Examples / Test Cases
#+name: lights_on_tests
#+begin_src ruby :results none :exports both :noweb yes :tangle no
def lights_on_tests(tests)
  tests.each_with_index { |test, index| puts "Test #{'%2d' % (index + 1)} ((#{lights_on(test[0]) == test[1] ? "PASS" : "FAIL"})): lights_on(#{test[0].inspect}) == #{test[1].inspect}"}
end

lights_on_tests([
  [5, [1, 4]], [10, [1, 4, 9]], [20, [1, 4, 9, 16]], [25, [1, 4, 9, 16, 25]],
  #[1000, [1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400, 441, 484, 529, 576, 625, 676, 729, 784, 841, 900, 961]]
])
#+end_src

*** Data Structure
**** Input
- Integer

**** Output
- Array

*** Algorithm
- call ~each_with_object({})~ on the range from ~1~ up to and including ~n~, passing ~switch~ and ~hash~ to the block as a local variables
  - add each switch to the hash with the value of ~false~
  - assign return value of ~each_with_object~ to ~switch_states~

Outer Loop
- loop ~n~ times, passing ~index~ to the block as a local variable

Inner Loop
- loop through the ~switch_states~ hash using ~map~, passing ~switch~ and ~state~ to the block as local variables
  - if ~index == 0~, toggle ~switch * 1~ (1*1 = 1), (2*1 = 2), (3*1 = 3)
  - if ~index == 1~, toggle ~switch * 2~ (1*2 = 2), (2*2 = 4), (3*2 = 6)
  - if ~index == 2~, toggle ~switch * 3~ (1*3 = 3), (2*3 = 6), (3*3 = 9)
  - toggle ~switch * index~
    - Ex: ~switch_states[switch * index] = !state~

- return values in ~switches~ that are ~true~ using ~~
  - Ex: ~switch_states.filter { |_, value| value }.keys~

    #+begin_src ruby :results output :exports both
    hash = { a: true, b: false, c: true, d: false }
    p hash.filter { |_, value|  value }.keys
    #+end_src

    #+RESULTS:
    : [:a, :c]

*** Code
**** Longer way using a hash
#+name: lights_on_long
#+begin_src ruby :results output :exports both :noweb yes :tangle yes
def lights_on(num_switches)
  target_range = 1..num_switches

  switch_states = target_range.each_with_object({}) { |switch, hash| hash[switch] = false }

  target_range.each do |index|
    target_range.each do |index2|
      target_switch = index * index2
      switch_states[target_switch] = !switch_states[target_switch] if target_switch <= num_switches
    end
    #p switch_states
  end

  switch_states.filter { |_, value| value }.keys
end

<<lights_on_tests>>
#+end_src

#+RESULTS: lights_on_long
: Test  1 ((PASS)): lights_on(5) == [1, 4]
: Test  2 ((PASS)): lights_on(10) == [1, 4, 9]
: Test  3 ((PASS)): lights_on(20) == [1, 4, 9, 16]
: Test  4 ((PASS)): lights_on(25) == [1, 4, 9, 16, 25]
: Test  5 ((PASS)): lights_on(1000) == [1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400, 441, 484, 529, 576, 625, 676, 729, 784, 841, 900, 961]

**** Relatively quick way to do this using a ~range~ and ~each_with_object~
#+name: lights_on
#+begin_src ruby :results output :exports both :noweb yes :tangle yes
def lights_on(num_switches)
  (1..num_switches).each_with_object([]) do |index, switches_on|
    switch = index * index
    switches_on << switch if switch <= num_switches
  end
end

<<lights_on_tests>>
#+end_src

#+RESULTS: lights_on
: Test  1 ((PASS)): lights_on(5) == [1, 4]
: Test  2 ((PASS)): lights_on(10) == [1, 4, 9]
: Test  3 ((PASS)): lights_on(20) == [1, 4, 9, 16]
: Test  4 ((PASS)): lights_on(25) == [1, 4, 9, 16, 25]
: Test  5 ((PASS)): lights_on(1000) == [1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400, 441, 484, 529, 576, 625, 676, 729, 784, 841, 900, 961]

** Recommended Solution
#+begin_src ruby :results output :exports both :session
# initialize the lights hash
def initialize_lights(number_of_lights)
  lights = Hash.new
  1.upto(number_of_lights) { |number| lights[number] = "off" }
  lights
end

# return list of light numbers that are on
def on_lights(lights)
  lights.select { |_position, state| state == "on" }.keys
end

# toggle every nth light in lights hash
def toggle_every_nth_light(lights, nth)
  lights.each do |position, state|
    if position % nth == 0
      lights[position] = (state == "off") ? "on" : "off"
    end
  end
end

# Run entire program for number of lights
def toggle_lights(number_of_lights)
  lights = initialize_lights(number_of_lights)
  1.upto(lights.size) do |iteration_number|
    toggle_every_nth_light(lights, iteration_number)
  end

  on_lights(lights)
end

p toggle_lights(1000)
#+end_src

#+RESULTS:
: [1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400, 441, 484, 529, 576, 625, 676, 729, 784, 841, 900, 961]

In this exercise, we use a ~Hash~ to represent our lights. A single light is represented as a key-value pair within the hash. The key will be the position that light has out of the 1000 lights; the first light is position 1, the last is 1000. The value of each light is either "on" or "off" to represent the state of that light.

We start by calling ~toggle_lights~ with an argument (the number of lights) of ~1000~.

~toggle_lights~ immediately calls ~initialize_lights~, which creates and returns a ~Hash~ that represents all lights with a current state of ~off~. ~toggle_lights~ subsequently uses the ~1.upto(lights.size)~ method call to iterate over the lights as many times as needed (~1000~ based on our initial value). It uses ~toggl_every_nth_light~ to toggle first every light, then every other light, then every 3rd light, and so on, until we have iterated over all of the lights 1000 times.

Finally, we call ~on_lights~ to select the lights that are still on. We use ~Hash#select~, which returns a new ~Hash~ that contains only the on lights.

The definition of ~on_lights~ uses ~_position~ as a parameter name since we don't use the parameter, but want to show what it represents. Using an underscore at the beginning of a parameter name is a common convention to show that a parameter isn't used.

Note that we don't use the value ~1000~ internally in most of our methods. Instead, we use ~lights.size~. This lets us easily reuse the code for any number of lights.

Our final answer ends up being:

~p lights_on(lights)~

Output:

~[1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400, 441, 484, 529, 576, 625, 676, 729, 784, 841, 900, 961]~

** Further Exploration
There are a few interesting points about this exercise that we can explore:

*** 1. Do you notice the pattern in our answer? Every light that is on is a perfect square. Why is that?

|         | 1  | 2   | 3   | 4   | 5   |
|---------+----+-----+-----+-----+-----|
| round 1 | on | on  | on  | on  | on  |
| round 2 | on | off | on  | off | on  |
| round 3 | on | off | off | off | on  |
| round 4 | on | off | off | on  | on  |
| round 5 | on | off | off | on  | off |

*** 2. What are some alternatives for solving this exercise? What if we used an ~Array~ to represent our 1000 lights instead of a ~Hash~, how would that change our code?
If using an array, one can use the index of each value as the switch number. For example, ~[false, false, false]~ would be three switches ~0, 1, 2~, each turned off.

**** My solution using an ~Array~
#+name: lights_on_array
#+begin_src ruby :results output :exports both :noweb yes :tangle yes
def lights_on(num_switches)
  target_range = 0...num_switches

  switch_states = Array.new(num_switches) { |item| false }

  target_range.each do |index|
    target_range.each do |index2|
      target_switch = (index + 1) * (index2 + 1)
      switch_states[target_switch - 1] = !switch_states[target_switch - 1] if (target_switch - 1) < num_switches
    end
  end

  switch_states.each_with_index.map { |state, index| state ? index + 1 : nil  }.compact
end

<<lights_on_tests>>
#+end_src

#+RESULTS: lights_on_array
: Test  1 ((PASS)): lights_on(5) == [1, 4]
: Test  2 ((PASS)): lights_on(10) == [1, 4, 9]
: Test  3 ((PASS)): lights_on(20) == [1, 4, 9, 16]
: Test  4 ((PASS)): lights_on(25) == [1, 4, 9, 16, 25]

**** Recommended solution using an ~Array~
#+name: recommended_solution_array
#+begin_src ruby :results output :exports both :noweb yes
# initialize the lights hash
def initialize_lights(number_of_lights)
  lights = Array.new
  0.upto(number_of_lights - 1) { |number| lights[number] = "off" }
  lights
end

# return list of light numbers that are on
def on_lights(lights)
  lights.each_with_index.map { |state, index| state == "on" ? index + 1 : nil  }.compact
end

# toggle every nth light in lights hash
def toggle_every_nth_light(lights, nth)
  lights.each_with_index do |state, index|
    if (index + 1) % nth == 0
      lights[index] = (state == "off") ? "on" : "off"
    end
  end
end

# Run entire program for number of lights
def lights_on(number_of_lights)
  lights = initialize_lights(number_of_lights)
  1.upto(lights.size) do |iteration_number|
    toggle_every_nth_light(lights, iteration_number)
  end

  on_lights(lights)
end

<<lights_on_tests>>
#+end_src

#+RESULTS: recommended_solution_array
: Test  1 ((PASS)): lights_on(5) == [1, 4]
: Test  2 ((PASS)): lights_on(10) == [1, 4, 9]
: Test  3 ((PASS)): lights_on(20) == [1, 4, 9, 16]
: Test  4 ((PASS)): lights_on(25) == [1, 4, 9, 16, 25]

*** 3. We could have a method that replicates the output from the description of this problem (i.e. "lights 2, 3, and 5 are now off; 1 and 4 are on.") How would we go about writing that code?

**** My Answer
#+name: lights_on_print
#+begin_src ruby :results output :exports both :noweb yes :tangle yes
def lights_on(num_switches)
  target_range = 1..num_switches

  switch_states = target_range.each_with_object({}) { |switch, hash| hash[switch] = false }

  target_range.each do |index|
    target_range.each do |index2|
      target_switch = index * index2
      switch_states[target_switch] = !switch_states[target_switch] if target_switch <= num_switches
    end

    on, off = switch_states.partition(&:last)

    if off.empty?
      puts "Every light is turned on."
    else
      puts "Lights #{off.map(&:first).join(', ')} are now off; #{on.map(&:first).join(', ')} are on."
    end
  end

  switch_states.filter { |_, value| value }.keys
end

<<lights_on_tests>>
#+end_src

#+RESULTS: lights_on_print
#+begin_example
Every light is turned on.
Lights 2, 4 are now off; 1, 3, 5 are on.
Lights 2, 3, 4 are now off; 1, 5 are on.
Lights 2, 3 are now off; 1, 4, 5 are on.
Lights 2, 3, 5 are now off; 1, 4 are on.
Test  1 ((PASS)): lights_on(5) == [1, 4]
Every light is turned on.
Lights 2, 4, 6, 8, 10 are now off; 1, 3, 5, 7, 9 are on.
Lights 2, 3, 4, 8, 9, 10 are now off; 1, 5, 6, 7 are on.
Lights 2, 3, 9, 10 are now off; 1, 4, 5, 6, 7, 8 are on.
Lights 2, 3, 5, 9 are now off; 1, 4, 6, 7, 8, 10 are on.
Lights 2, 3, 5, 6, 9 are now off; 1, 4, 7, 8, 10 are on.
Lights 2, 3, 5, 6, 7, 9 are now off; 1, 4, 8, 10 are on.
Lights 2, 3, 5, 6, 7, 8, 9 are now off; 1, 4, 10 are on.
Lights 2, 3, 5, 6, 7, 8 are now off; 1, 4, 9, 10 are on.
Lights 2, 3, 5, 6, 7, 8, 10 are now off; 1, 4, 9 are on.
Test  2 ((PASS)): lights_on(10) == [1, 4, 9]
Every light is turned on.
Lights 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 are now off; 1, 3, 5, 7, 9, 11, 13, 15, 17, 19 are on.
Lights 2, 3, 4, 8, 9, 10, 14, 15, 16, 20 are now off; 1, 5, 6, 7, 11, 12, 13, 17, 18, 19 are on.
Lights 2, 3, 9, 10, 12, 14, 15 are now off; 1, 4, 5, 6, 7, 8, 11, 13, 16, 17, 18, 19, 20 are on.
Lights 2, 3, 5, 9, 12, 14, 20 are now off; 1, 4, 6, 7, 8, 10, 11, 13, 15, 16, 17, 18, 19 are on.
Lights 2, 3, 5, 6, 9, 14, 18, 20 are now off; 1, 4, 7, 8, 10, 11, 12, 13, 15, 16, 17, 19 are on.
Lights 2, 3, 5, 6, 7, 9, 18, 20 are now off; 1, 4, 8, 10, 11, 12, 13, 14, 15, 16, 17, 19 are on.
Lights 2, 3, 5, 6, 7, 8, 9, 16, 18, 20 are now off; 1, 4, 10, 11, 12, 13, 14, 15, 17, 19 are on.
Lights 2, 3, 5, 6, 7, 8, 16, 20 are now off; 1, 4, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 16 are now off; 1, 4, 9, 11, 12, 13, 14, 15, 17, 18, 19, 20 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 16 are now off; 1, 4, 9, 12, 13, 14, 15, 17, 18, 19, 20 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 16 are now off; 1, 4, 9, 13, 14, 15, 17, 18, 19, 20 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 16 are now off; 1, 4, 9, 14, 15, 17, 18, 19, 20 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 16 are now off; 1, 4, 9, 15, 17, 18, 19, 20 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16 are now off; 1, 4, 9, 17, 18, 19, 20 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15 are now off; 1, 4, 9, 16, 17, 18, 19, 20 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17 are now off; 1, 4, 9, 16, 18, 19, 20 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 18 are now off; 1, 4, 9, 16, 19, 20 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 18, 19 are now off; 1, 4, 9, 16, 20 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20 are now off; 1, 4, 9, 16 are on.
Test  3 ((PASS)): lights_on(20) == [1, 4, 9, 16]
Every light is turned on.
Lights 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24 are now off; 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25 are on.
Lights 2, 3, 4, 8, 9, 10, 14, 15, 16, 20, 21, 22 are now off; 1, 5, 6, 7, 11, 12, 13, 17, 18, 19, 23, 24, 25 are on.
Lights 2, 3, 9, 10, 12, 14, 15, 21, 22, 24 are now off; 1, 4, 5, 6, 7, 8, 11, 13, 16, 17, 18, 19, 20, 23, 25 are on.
Lights 2, 3, 5, 9, 12, 14, 20, 21, 22, 24, 25 are now off; 1, 4, 6, 7, 8, 10, 11, 13, 15, 16, 17, 18, 19, 23 are on.
Lights 2, 3, 5, 6, 9, 14, 18, 20, 21, 22, 25 are now off; 1, 4, 7, 8, 10, 11, 12, 13, 15, 16, 17, 19, 23, 24 are on.
Lights 2, 3, 5, 6, 7, 9, 18, 20, 22, 25 are now off; 1, 4, 8, 10, 11, 12, 13, 14, 15, 16, 17, 19, 21, 23, 24 are on.
Lights 2, 3, 5, 6, 7, 8, 9, 16, 18, 20, 22, 24, 25 are now off; 1, 4, 10, 11, 12, 13, 14, 15, 17, 19, 21, 23 are on.
Lights 2, 3, 5, 6, 7, 8, 16, 20, 22, 24, 25 are now off; 1, 4, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 21, 23 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 16, 22, 24, 25 are now off; 1, 4, 9, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 23 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 16, 24, 25 are now off; 1, 4, 9, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 16, 25 are now off; 1, 4, 9, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 16, 25 are now off; 1, 4, 9, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 16, 25 are now off; 1, 4, 9, 15, 17, 18, 19, 20, 21, 22, 23, 24 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 25 are now off; 1, 4, 9, 17, 18, 19, 20, 21, 22, 23, 24 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 25 are now off; 1, 4, 9, 16, 17, 18, 19, 20, 21, 22, 23, 24 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 25 are now off; 1, 4, 9, 16, 18, 19, 20, 21, 22, 23, 24 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 18, 25 are now off; 1, 4, 9, 16, 19, 20, 21, 22, 23, 24 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 18, 19, 25 are now off; 1, 4, 9, 16, 20, 21, 22, 23, 24 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 25 are now off; 1, 4, 9, 16, 21, 22, 23, 24 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 25 are now off; 1, 4, 9, 16, 22, 23, 24 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 25 are now off; 1, 4, 9, 16, 23, 24 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 25 are now off; 1, 4, 9, 16, 24 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25 are now off; 1, 4, 9, 16 are on.
Lights 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24 are now off; 1, 4, 9, 16, 25 are on.
Test  4 ((PASS)): lights_on(25) == [1, 4, 9, 16, 25]
#+end_example


