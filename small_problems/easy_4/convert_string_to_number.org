* Convert a String to a Number!
:LOGBOOK:
CLOCK: [2019-09-21 Sat 07:45]--[2019-09-21 Sat 08:09] =>  0:24
CLOCK: [2019-09-21 Sat 06:50]--[2019-09-21 Sat 07:24] =>  0:34
:END:
Small Problems > Easy 4
https://launchschool.com/exercises/192719a5

** Exercise Description
The ~String#to_i~ method converts a string of numeric characters (including an optional plus or minus sign) to an ~Integer~. ~String#to_i~ and the Integer constructor (~Integer()~) behave similarly. In this exercise, you will create a method that does the same thing.

Write a method that takes a String of digits, and returns the appropriate number as an integer. You may not use any of the methods mentioned above.

For now, do not worry about leading ~+~ or ~-~ signs, nor should you worry about invalid characters; assume all characters will be numeric.

You may not use any of the standard conversion methods available in Ruby, such as ~String#to_i~, ~Integer()~, etc. Your method should do this the old-fashioned way and calculate the result by analyzing the characters in the string.

Examples:

#+begin_example ruby
string_to_integer('4321') == 4321
string_to_integer('570') == 570
#+end_example

** My Solution
*** Understanding the Problem
**** Input
- string of digits
- all digits will be numeric
- no + or - sign
- no need to validate input

**** Output
- string of digits as an Integer

**** Rules
- cannot use ~String#to_i~, ~Integer()~, or any other existing conversion methods

*** Examples / Test Cases
#+begin_src ruby :results output :exports both :session :tangle no
def string_to_integer_tests(tests)
  tests.each { |test| puts "#{string_to_integer(test[0]) == test[1] ? "PASS" : "FAIL"}: string_to_integer(#{test[0].inspect}) == #{test[1].inspect}"}
end

string_to_integer_tests([
  ['4321', 4321], ['570', 570], ['1', 1], ['0', 0]
])
#+end_src

#+RESULTS:
: PASS: string_to_integer("4321") == 4321
: PASS: string_to_integer("570") == 570
: PASS: string_to_integer("1") == 1
: PASS: string_to_integer("0") == 0

*** Data Structure
**** Input
- String containing only positive numeric digits

**** Output
- Integer

*** Algorithm
- Create hash with string to integer equivalents
- Convert digits in the string to an array of integers using ~String#chars~ and ~Array#map~
- set the length to the length of the array minus 1 to get the number of zeros
- loop using ~reduce~, starting with zero
- determine the multiplier by using ~Kernel#eval~
  + ex: ~eval('1') == 1~
- decrement the length by one for the next iteration of the loop
- add the current ~sum~ to the result of the current number multiplied by the multiplier

*** Using ~eval~
#+begin_src ruby :results none :exports both :session :tangle yes
def string_to_integer(string)
  eval(string)
end
#+end_src

*** Code
#+begin_src ruby :results none :tangle yes :session
NUMBERS = {
  '0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4,
  '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9
}

def string_to_integer(string)
  array = string.chars.map { |char| NUMBERS[char] }
  length = array.length - 1

  array.reduce(0) do |sum, num|
    multiplier = eval('1' << '0' * length)
    length -= 1
    sum += num * multiplier
  end
end
#+end_src

** Recommended Solution
#+begin_src ruby :results output :exports both
DIGITS = {
  '0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4,
  '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9
}

def string_to_integer(string)
  digits = string.chars.map { |char| DIGITS[char] }

  value = 0
  digits.each { |digit| value = 10 * value + digit }
  value
end
#+end_src

As usual, this isn't the shortest or even the easiest solution to this problem, but it's straightforward. The big takeaway from this solution is our use of the DIGITS hash to convert string digits to their numeric values. This technique of using hashes to perform conversions is a common idiom that you can use in a wide variety of situations, often resulting in code that is easier to read, understand, and maintain.

The actual computation of the numeric value of ~string~ is mechanical. We take each digit and add it to 10 times the previous value, which yields the desired result in almost no time. For example, if we have 4, 3, and 1, we compute the result as:

#+begin_example
10 * 0 + 4 -> 4
10 * 4 + 3 -> 43
10 * 43 + 1 -> 431
#+end_example

** Further Exploration
Write a ~hexadecimal_to_integer~ method that converts a string representing a hexadecimal number to its integer value.

Example:

#+begin_example ruby
hexadecimal_to_integer('4D9f') == 19871
#+end_example

*** My Answer
**** Tests
#+begin_src ruby :results output :exports both :session :tangle no
def hexadecimal_to_integer_tests(tests)
  tests.each { |test| puts "#{hexadecimal_to_integer(test[0]) == test[1] ? "PASS" : "FAIL"}: hexadecimal_to_integer(#{test[0].inspect}) == #{test[1].inspect}"}
end

hexadecimal_to_integer_tests([
  ['4D9f', 19871], ['abcde', 703710], ['12495', 74901]
])
#+end_src

#+RESULTS:
: PASS: hexadecimal_to_integer("4D9f") == 19871
: PASS: hexadecimal_to_integer("abcde") == 703710
: PASS: hexadecimal_to_integer("12495") == 74901

**** Code
#+begin_src ruby :results none :exports both :session
NUMBERS = {
  '0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4,
  '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9,
  'a' => 10, 'b' => 11, 'c' => 12, 'd' => 13,
  'e' => 14, 'f' => 15
}

def hexadecimal_to_integer(string)
  digits = string.chars.map { |char| NUMBERS[char.downcase] }

  value = 0
  digits.each { |digit| value = 16 * value + digit }
  value
end
#+end_src

** Submitted Solutions
*** Using ~Array#inject~
**** Tests
#+begin_src ruby :results output :exports both :session :tangle no
def hexadecimal_to_integer_tests(tests)
  tests.each { |test| puts "#{hexadecimal_to_integer(test[0]) == test[1] ? "PASS" : "FAIL"}: hexadecimal_to_integer(#{test[0].inspect}) == #{test[1].inspect}"}
end

hexadecimal_to_integer_tests([
  ['4D9f', 19871], ['abcde', 703710], ['12495', 74901]
])
#+end_src

#+RESULTS:
: PASS: hexadecimal_to_integer("4D9f") == 19871
: PASS: hexadecimal_to_integer("abcde") == 703710
: PASS: hexadecimal_to_integer("12495") == 74901

**** Code
#+begin_src ruby :results output :exports both
INTEGER = {
  '0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6,
  '7' => 7, '8' => 8, '9' => 9, 'a' => 10, 'b' => 11, 'c' => 12,
  'd' => 13, 'e' => 14, 'f' => 15
}

def hexadecimal_to_integer(str)
  str.downcase.chars.inject(0){ |acc, char| acc * 16 + INTEGER[char] }
end
#+end_src

