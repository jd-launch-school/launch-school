* Lettercase Counter
  Small problems > Easy 7
  https://launchschool.com/exercises/96e0267a

** Exercise Description
Write a method that takes a string, and then returns a hash that contains 3 entries: one represents the number of characters in the string that are lowercase letters, one the number of characters that are uppercase letters, and one the number of characters that are neither.

Examples:

#+begin_example ruby
letter_case_count('abCdef 123') == { lowercase: 5, uppercase: 1, neither: 4 }
letter_case_count('AbCd +Ef') == { lowercase: 3, uppercase: 3, neither: 2 }
letter_case_count('123') == { lowercase: 0, uppercase: 0, neither: 3 }
letter_case_count('') == { lowercase: 0, uppercase: 0, neither: 0 }
#+end_example

** My Solution
:LOGBOOK:
CLOCK: [2019-10-11 Fri 09:25]--[2019-10-11 Fri 09:41] =>  0:16
CLOCK: [2019-10-11 Fri 09:04]--[2019-10-11 Fri 09:12] =>  0:08
:END:
*** Understanding the Problem
**** Input
- string

**** Output
- hash with 3 entries
  + number of characters in string that are lowercase
  + number of characters that are uppercase
  + number of characters that are neither

**** Questions
- Should I validate for non-string inputs?

**** Rules
***** Explicit requirements
- string may contain letters, numbers, spaces, and symbols
- string may be empty

***** Implicit requirements
- uppercase and lowercase only apply to letters

*** Examples / Test Cases
#+name: letter_case_count_tests
#+begin_src ruby :results none :exports both :noweb yes :tangle no
def letter_case_count_tests(tests)
  tests.each { |test| puts "#{letter_case_count(test[0]) == test[1] ? "PASS" : "FAIL"}: letter_case_count(#{test[0].inspect}) == #{test[1].inspect}"}
end

letter_case_count_tests([
  ['abCdef 123', { lowercase: 5, uppercase: 1, neither: 4 }],
  ['AbCd +Ef', { lowercase: 3, uppercase: 3, neither: 2 }],
  ['123', { lowercase: 0, uppercase: 0, neither: 3 }],
  ['', { lowercase: 0, uppercase: 0, neither: 0 }]
])
#+end_src

*** Data Structure
**** Input
- String

**** Output
- Hash with three keys

*** Algorithm
- create the ~results~ hash:
  + ~{ lowercase: 0, uppercase: 0, neither: 0 }~
- create ~counter~ variable set to ~0~
- create ~split_string~ and set it to the result of ~string.split('')~
- start the loop
  + break the loop if ~counter~ equals the length of ~split_string~
  + set ~current_element~ variable to the current element

  + if ~current_element~ is not a letter
    - use ~match?(/[A-Za-z]/)~ to look for letters
    - set ~results[neither] += 1~

  + if ~current_element~ is equal to ~current_element.upcase~
    - set ~results[uppercase] += 1~

  + if ~current_element~ is not equal to ~current_element.upcase~
    - set ~results[lowercase] += 1~

  + increment ~counter~ by ~1~

- return ~results~

*** Code
#+name: letter_case_count
#+begin_src ruby :results output :exports both :noweb yes :tangle yes
def letter_case_count(string)
  results = { lowercase: 0, uppercase: 0, neither: 0 }
  split_string = string.split('')

  split_string.each_with_object(results) do |element, results|
    if !element.match?(/[A-Za-z]/)
      results[:neither] += 1
    else
      element == element.upcase ? results[:uppercase] += 1 : results[:lowercase] += 1
    end
  end
end

<<letter_case_count_tests>>
#+end_src

#+RESULTS: letter_case_count
: {:lowercase=>5, :uppercase=>1, :neither=>4}
: PASS: letter_case_count("abCdef 123") == {:lowercase=>5, :uppercase=>1, :neither=>4}
: {:lowercase=>3, :uppercase=>3, :neither=>2}
: PASS: letter_case_count("AbCd +Ef") == {:lowercase=>3, :uppercase=>3, :neither=>2}
: {:lowercase=>0, :uppercase=>0, :neither=>3}
: PASS: letter_case_count("123") == {:lowercase=>0, :uppercase=>0, :neither=>3}
: {:lowercase=>0, :uppercase=>0, :neither=>0}
: PASS: letter_case_count("") == {:lowercase=>0, :uppercase=>0, :neither=>0}

** Recommended Solution
#+begin_src ruby :results none :exports both :session
UPPERCASE_CHARS = ('A'..'Z').to_a
LOWERCASE_CHARS = ('a'..'z').to_a

def letter_case_count(string)
  counts = { lowercase: 0, uppercase: 0, neither: 0 }

  string.chars.each do |char|
    if UPPERCASE_CHARS.include?(char)
      counts[:uppercase] += 1
    elsif LOWERCASE_CHARS.include?(char)
      counts[:lowercase] += 1
    else
      counts[:neither] += 1
    end
  end

  counts
end
#+end_src

#+begin_src ruby :results output :exports both
def letter_case_count(string)
  counts = {}
  characters = string.chars
  counts[:lowercase] = characters.count { |char| char =~ /[a-z]/ }
  counts[:uppercase] = characters.count { |char| char =~ /[A-Z]/ }
  counts[:neither] = characters.count { |char| char =~ /[^A-Za-z]/ }
  counts
end
#+end_src

This method is expected to return a hash of character counts.

In the first solution, we begin by initializing two constants containing the uppercase and lowercase letters of the alphabet. Then, after initializing a ~counts~ hash with the appropriate keys and default values of zero, we simply iterate over each character of the string passed in as an argument and update each count as needed.

We take a different approach in our second solution. We first initialize an empty ~counts~ hash and then use ~Array#count~ to find the number of occurrences of lowercase, uppercase, and all other characters. In the block following the ~count~ method invocation, we use the ~String#=~~ method to match each character of the string against a pattern specified by a regular expression. The block will return a truthy value if there is a match, ~nil~ otherwise.

Remember that a problem can always be solved in different ways. Don't worrry if you don't know regular expressions yet. Just use the tools you are familiar with.

** Submitted Solutions
*** Uses ~count~
#+name: count
#+begin_src ruby :results output :exports both :noweb yes
def letter_case_count(string)
  { lowercase: string.count('a-z'), uppercase: string.count('A-Z'), neither: string.count('^a-zA-Z') }
end

<<letter_case_count_tests>>
#+end_src

#+RESULTS: count
: PASS: letter_case_count("abCdef 123") == {:lowercase=>5, :uppercase=>1, :neither=>4}
: PASS: letter_case_count("AbCd +Ef") == {:lowercase=>3, :uppercase=>3, :neither=>2}
: PASS: letter_case_count("123") == {:lowercase=>0, :uppercase=>0, :neither=>3}
: PASS: letter_case_count("") == {:lowercase=>0, :uppercase=>0, :neither=>0}

