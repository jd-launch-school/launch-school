#+begin_export html
---
layout: post
title: Rock Paper Scissors Bonus Features
excerpt_separator: <!--more-->
tags: launch_school ruby project
---
#+end_export

I worked on fixing Rubocop warnings and refactoring various parts of my code based on code reviews of other projects.

#+begin_export html
<!--more-->
#+end_export

* RPS Bonus Features
** No More Global Variables
Initially, I used global variables to store the score so that these variables could be accessible in every method. I changed this to a hash that I pass around to methods as needed:

#+begin_src ruby :results output :exports both
score = { player: 0, computer: 0 }
#+end_src

This can then be passed used in the ~increment_score~ method like this:

#+begin_src ruby :results output :exports both
def increment_score(winner, score)
  score[winner] += 1
end
#+end_src

** Constant for winning score
I created a constant called ~WINNING_SCORE~ to use throughout the code as needed. For instance, in the ~grand_winner?~ method:

#+begin_src ruby :results output :exports both
def grand_winner?(score)
  score[:player] == WINNING_SCORE || score[:computer] == WINNING_SCORE
end
#+end_src

This way, if I need to update the winning score, I can update it in one place and it'll work wherever it's used.

** Display Grand Winner Method
I tweaked the ~display_grand_winner~ method a little, from:

#+begin_src ruby :results output :exports both
def display_grand_winner(message)
  display_stars(message)
  prompt(message)
  display_stars(message)
  prompt("")
end
#+end_src

to:

#+begin_src ruby :results output :exports both
def display_grand_winner(score)
  message = "#{score[:player] == WINNING_SCORE ? 'You are' : 'Computer is'}" \
            " the GRAND WINNER!"

  prompt("*" * message.length)
  prompt(message)
  prompt("*" * message.length)
  prompt("")
end
#+end_src

** Tests
I decided to add tests for the ~valid_choice~ method in order to make sure no input breaks the game. Here are the tests:

#+begin_src ruby :results none
def valid_choice(choice)
  choice_as_symbol = if choice.match?(/spock/i)
                       :k
                     else
                       choice[0]&.downcase&.to_sym
                     end

  choice_as_symbol if VALID_CHOICES.include?(choice)
end

p valid_choice('r') == :r
#+end_src

#+begin_src ruby :results none 
def valid_choice_tests(tests)
  tests.each { |test| puts "#{valid_choice(test[0]) == test[1] ? "PASS" : "FAIL"}: valid_choice(#{test[0] == nil ? 'nil' : test[0]}) == #{test[1].inspect}"}
end

valid_choice_tests([
  ['r', :r], ['rock', :r], ['R', :r], ['ROCK', :r], ['p', :p], ['paper', :p],
  ['P', :p], ['PAPER', :p], ['s', :s], ['scissors', :s], ['S', :s],
  ['SCISSORS', :s], ['l', :l], ['lizard', :l], ['L', :l], ['LIZARD', :l],
  ['k', :k], ['spock', :k], ['K', :k], ['SPOCK', :k], ['q', :q], ['quit', :q],
  ['Q', :q], ['QUIT', :q], ['sp', nil], ['asdfjoiajsdf', nil], ['123', nil],
  ['', nil], [' ', nil]
])
#+end_src

#+begin_example
PASS: valid_choice(r) == :r
PASS: valid_choice(rock) == :r
PASS: valid_choice(R) == :r
PASS: valid_choice(ROCK) == :r
PASS: valid_choice(p) == :p
PASS: valid_choice(paper) == :p
PASS: valid_choice(P) == :p
PASS: valid_choice(PAPER) == :p
PASS: valid_choice(s) == :s
PASS: valid_choice(scissors) == :s
PASS: valid_choice(S) == :s
PASS: valid_choice(SCISSORS) == :s
PASS: valid_choice(l) == :l
PASS: valid_choice(lizard) == :l
PASS: valid_choice(L) == :l
PASS: valid_choice(LIZARD) == :l
PASS: valid_choice(k) == :k
PASS: valid_choice(spock) == :k
PASS: valid_choice(K) == :k
PASS: valid_choice(SPOCK) == :k
PASS: valid_choice(q) == :q
PASS: valid_choice(quit) == :q
PASS: valid_choice(Q) == :q
PASS: valid_choice(QUIT) == :q
PASS: valid_choice(sp) == nil
PASS: valid_choice(asdfjoiajsdf) == nil
PASS: valid_choice(123) == nil
PASS: valid_choice() == nil
PASS: valid_choice( ) == nil
#+end_example

In doing so, I realized that the code didn't handle an empty string correctly, so I used the [[https://docs.ruby-lang.org/en/2.6.0/syntax/calling_methods_rdoc.html#label-Safe+navigation+operator][safe navigation operator]] ~&.~ to fix that and return ~nil~ when the ~downcase~ and ~to_sym~ methods are not available.

** From Array of Symbols to Array of Symbols Percent String
I decided to use ~%i~ percent string to create the computer choice array of symbols.

Changed from:
#+begin_src ruby :results output :exports both
computer_choice = [:r, :p, :s, :l, :k].sample
#+end_src

to:
#+begin_src ruby :results output :exports both
computer_choice = %i(r p s l k).sample
#+end_src

