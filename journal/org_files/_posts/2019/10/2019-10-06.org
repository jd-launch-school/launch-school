* Today I Learned ...
** Practice Problems: Methods and More Methods
This is a series of 10 practice problems related to various collection methods. Here's some highlights that I pulled out of the practice problems, either specific wording I liked or something that's new to me.

*** truthiness wording
I like the following way of describing a block's value when a method is looking for /truthiness/:

#+begin_quote
... the truthiness of the block's return value.
#+end_quote

*** each_with_object passing
This is a comprehensive way to describe how the object argument moves through the ~each_with_object~ iteration:

#+begin_quote
When we invoke ~each_with_object~, we pass in an object (~{}~ here) as an argument. That object is then passed into the block *and it's updated value is returned at the end of each iteration*. 
#+end_quote

I think it's helpful to think of the updated passed in object as being passed from iteration to iteration. 

*** any? stops evaluating
~Array#any?~ returns ~true~ if the block returns ~true~ for any element in the collection. However, it stops evaluating the block once the block evaluates to ~true~ the first time:

#+begin_quote
Since the ~Array#any?~ method returns ~true~ if the block ever returns a value other than ~false~ or ~nil~, and the block returns ~true~ on the first iteration, we know that ~any?~ will return ~true~. What is also interesting here is ~any?~ stops iterating after this point since there is no need to evaluate the remaining items in the array; therefore, ~puts num~ is only ever invoked for the first item in the array: ~1~.
#+end_quote

Here's the code:

#+begin_src ruby :results output :exports both
result = [1, 2, 3].any? do |num|
           puts num
           num.odd?
         end
p result
#+end_src

#+RESULTS:
: 1
: true

*** map returns arrays
~Enumerable#map~ will always return an array, even when the caller is a hash:

#+begin_src ruby :results output :exports both
result = { a: 1, b: 2, c: 3 }.map { |_, value| value }
p result
#+end_src

#+RESULTS:
: [1, 2, 3]

*** if returns nil
When an ~if~ statement is the last line to be executed and none of the branches of the ~if~ statement were executed, it returns ~nil~:

#+begin_src ruby :results output :exports both
result = if false
           'hi'
         end

p result
#+end_src

#+RESULTS:
: nil

** Practice Problems: Additional Practice
This is a series of 10 practice problems where we practice implementing various collection methods. I only finished the first one today.

I was able to create four solutions for the practice problem. I used ~loop~, ~each~, ~each_with_index~ and ~each_with_object~. I created the following table to highlight some of the differences between these methods:

| Solution         | Requires ~result~ variable? | Requires ~counter~ variable? | Requires ~Array#index~? | Number of lines |
|------------------+-----------------------------+------------------------------+-------------------------+-----------------|
| loop             | yes                         | yes                          | no                      |              13 |
| each             | yes                         | no                           | yes                     |               3 |
| each_with_index  | yes                         | no                           | no                      |               3 |
| each_with_object | no                          | no                           | yes                     |               1 |
