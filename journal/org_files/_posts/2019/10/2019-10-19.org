* Today I Learned ...
I streamed my drill session this morning. I couldn't talk, so I typed what I was thinking. This took longer, but not too much longer. I like streaming while talking more, but in a pinch, this will do as well. It's nice to do the drills (Emacs Org-Drill) while others watch so they can see the power of Org-Drill and flashcards and how this form of study may or may not be helpful to them.

** RB101 > Lesson 5 > Working with Blocks (first five examples)
I worked through the first five examples. My explanation for the first example was brisk and incomplete in comparison to the provided explanation. Even though I understand all of the details described, I don't have a lot of practice writing and explaining these details. So, I need lots more practice.

For future reference, here's my explanation in comparison to the provided explanation:

My explanation:
#+begin_quote
Hmm... well we have two arrays as elements of a parent array. We iterate over that parent array and set each element to ~arr~. Then we print the first element of each ~arr~ to the screen and return the original array.
#+end_quote

Provided explanation:
#+begin_quote
The ~Array#each~ method is being called on the multi-dimensional array ~[[1, 2], [3, 4]]~. Each inner array is passed to the block in turn and assigned to the local variable ~arr~. The ~Array#first~ method is called on ~arr~ and returns the object at index ~0~ of the current array - in this case the integers ~1~ and ~3~, respectively. The ~puts~ method then outputs a string representation of the integer. ~puts~ returns ~nil~ and, since this is the last evaluated statement within the block, the return value of the block is therefore ~nil~. ~each~ doesn't do anything with this returned value though, and since the return value of each is the calling object - in this case the nested array ~[[1, 2], [3, 4]]~ - this is what is ultimately returned.
#+end_quote

I share this here so hopefully my future self will be able to see how much I've improved. :)

In terms of the content, the suggested way of analyzing code is to answer these questions in regards to each part of the code:

- What is the type of action being performed (method call, block, conditional, etc...)?
- What is the object that action is being performed on?
- What is the side-effect of that action (e.g. output or destructive action)?
- What is the return value of that action?
- Is the return value used by whatever instigated the action?

It's possible to put these questions into a nice table so it's easier to see what's happening in the code:

| Line | Action | Object | Side Effect | Return Value | Is Return Value Used? |
|------+--------+--------+-------------+--------------+-----------------------|
|      |        |        |             |              |                       |

