* Today I Learned ...
** Small Problems > Easy 7 > Swap Case
*** Description of Exercise
Given a string, return that string with the case of every letter swapped.

Can't use ~String#swapcase~

Example:

#+begin_example ruby
swapcase('CamelCase') == 'cAMELcASE'
swapcase('Tonight on XYZ-TV') == 'tONIGHT ON xyz-tv'
#+end_example

*** My Solution
Time to Complete: 18 minutes

I completed this in two ways using ~gsub~ and ~map~. Really, it ended up being the same way but the ~map~ solution removes the unnecessary regular expression.

First, I converted the string into an array of characters. I then used ~map~ to iterate through the array. I check against ~char.upcase == char~ to determine if the character is uppercase. If it is, I call ~char.downcase~, if not, ~char.upcase~. I then use ~join~ to convert the array back into a string.

*** Other Observations
The recommended solution is similar, but it uses regular expressions to determine the case of the character, like ~char.match?(/[a-z]/)~ for lowercase and ~char.match?(/[A-Z]/)~ for uppercase.

** Small Problems > Easy 7 > Staggered Caps (Part 1)
*** Description of Exercise
Given a string, return a new string where every other letter is capitalized, starting with the first letter.

Example:

#+begin_example ruby
staggered_case('I Love Launch School!') == 'I LoVe lAuNcH ScHoOl!'
staggered_case('ALL_CAPS') == 'AlL_CaPs'
staggered_case('ignore 77 the 444 numbers') == 'IgNoRe 77 ThE 444 NuMbErS'
#+end_example

*** My Solution
Time to Complete: 9 minutes

This is similar to the Swap Case problem above, but I used ~map.with_index~ in order to keep track of the index and ~index.even?~ to determine whether or not the character should be capitalized.

*** Other Observations
The recommended solution did something different. It adds a ~need_upper~ variable set to ~true~ in order to determine if a character should be uppercase. At the end of the ~each~ loop, it changes ~need_upper~ to ~false~ so that the next character in the loop is lowercase.

*** Further Exploration
Modify this method so the caller can request that the first character be lowercase. I accomplished this using both my solution and the recommended solution.

I added a default keyword argument and had to use a nested ternary in my solution, making it more difficult to read:

#+begin_example ruby
need_upper ? index.even? : index.odd?
#+end_example

If ~need_upper~ is set to ~true~, then check if the index is even before making the character uppercase. Otherwise, check if the index is odd.

For the recommended solution, I only needed to make the ~need_upper~ variable a default keyword argument:

#+begin_example ruby
def staggered_case(string, need_upper = true)
#+end_example

*** Submitted Solutions
The submitted solutions had an interesting solution that used ~gsub~ to get two letters and only capitalize the first letter ~gsub(/..?/, &:capitalize)~:

#+begin_example ruby
string.gsub(/..?/, &:capitalize)
#+end_example

I converted this so it would work with the further exploration part:

#+begin_example ruby
need_upper ? string.gsub(/..?/, &:capitalize) : string.gsub(/(.)(.)?/) { $1.downcase + $2.to_s.upcase }
#+end_example

This is admittedly more difficult to understand, but it's all on one line.
