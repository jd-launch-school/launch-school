* Today I Learned ...
I spent most of my time working on my development environment. Specifically, getting myself used to using Pry with Emacs. I think I've found a workflow that'll work for what I need, but I won't know for sure until I start using it on more complicated problems.

This is after watching the video in RB101, Lesson 6, Debugging with Pry.

** Small Problems > Easy 8 > Convert number to a reversed array of digits
*** Description of Exercise
Given an integer, return a new integer with the digits reversed.

*** My Solution
I did this using ~to_s~, ~reverse~, and ~to_i~ initially:

#+begin_example ruby
integer.to_s.reverse.to_i
#+end_example

Then, after looking at the recommended solution where it mentioned possibly using a loop to do this, I solved this using a loop. I still had to convert the integer to a String and then back to an Integer, but I didn't need to use ~reverse~.

*** Recommended Solution
The recommended solution matched mine, but using variables instead of chaining.

*** Submitted Solutions
There were two submitted solutions I felt were worth noting:

**** Uses ~digits~
#+begin_example ruby
num.digits.join.to_i
#+end_example

**** Recursive
#+begin_src ruby :results output :exports both
def reversed_number(num, accumulator = 0)
  return accumulator if num.zero?
  reversed_number(num / 10, accumulator * 10 + num % 10)
end
#+end_src

The trick with this is that ~num / 10~ returns all of the digits except the last digit. ~num % 10~ returns the last digit.

***** First iteration
#+begin_src ruby :results output :exports both
num = 456
p 456 / 10
p 456 % 10
#+end_src

#+RESULTS:
: 45
: 6

The accumulator starts at ~0~, so the first iteration returns the last digit:
#+begin_src ruby :results output :exports both
p 0 * 10
p 0 * 10 + 456 % 10
#+end_src

#+RESULTS:
: 0
: 6

***** Second iteration
The method calls itself with the remaining digits (~45~).
#+begin_src ruby :results output :exports both
num = 45
p 45 / 10
p 45 % 10
#+end_src

#+RESULTS:
: 4
: 5

The accumulator is ~6~, so this iteration multiplies that by ~10~ and adds the last digit:
#+begin_src ruby :results output :exports both
p 6 * 10
p 6 * 10 + 45 % 10 # 60 + 5
#+end_src

#+RESULTS:
: 60
: 65

***** Third iteration
The method calls itself with the remaining digit (~4~).
#+begin_src ruby :results output :exports both
num = 4
p 4 / 10
p 4 % 10
#+end_src

#+RESULTS:
: 0
: 4

The accumulator is ~65~, so this iteration multiplies that by ~10~ and adds the last digit:
#+begin_src ruby :results output :exports both
p 65 * 10
p 65 * 10 + 4 % 10 # 650 + 4
#+end_src

#+RESULTS:
: 650
: 654

***** Fourth iteration
The method calls itself with the remaining digit (~0~).

This triggers the return statement and the accumulator is returned as it currently is ~654~.
