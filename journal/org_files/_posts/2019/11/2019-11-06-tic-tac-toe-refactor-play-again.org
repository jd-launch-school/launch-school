#+begin_export html
---
layout: post
title: Tic Tac Toe Refactor Play Again
excerpt_separator: <!--more-->
tags: launch_school ruby project tic_tac_toe
---
#+end_export

After refactoring the ~play_again?~ method in my Tic Tac Toe project, I added the first part of the computer defense logic.

#+begin_export html
<!--more-->
#+end_export

* Tic Tac Toe Project
** Play Again
Initially, I had the choices available to a player hard-coded. The player could choose either ~y~, ~yes~, ~n~, or ~no~. The code looked like this:

#+begin_src ruby
def play_again?
  loop do
    again = gets.chomp.downcase

    return true if %w(y yes).include?(again)
    return false if %w(n no).include?(again)

    prompt "Not a valid choice. Please choose 'y' or 'n'."
  end
end

prompt "Do you want to play another game? (y or n)"
break unless play_again?
#+end_src

Recognizing that there are more choices in my version of this program, I refactored this so that it uses an ~args~ parameter to determine which choices are available at a given time:

#+begin_src ruby
def play_again?(args)
  loop do
    player_choice = gets.chomp.downcase.to_sym
    return player_choice[0].to_sym if args.include?(player_choice)

    args_for_prompt = []
    args.each { |arg| args_for_prompt << "(#{arg[0]})#{arg[1..-1]}" if arg.size > 1 }

    prompt 'Not a valid choice.'
    prompt "Please choose [ #{args_for_prompt.join(' | ')} ]."
  end
end

#+end_src

I may decide to break this up into two or more methods at some point in the future, but it's working for now. Passing the arguments to this method looks like this:

#+begin_src ruby
prompt 'Do you want to play another game? [ (y)es | (n)o ]'
break unless play_again?(%i(yes y no n)) == :y
#+end_src

and ... 

#+begin_src ruby
prompt 'Do you want to quit, reset the score and play again,'
prompt 'or continue playing the current game?'
prompt '[ (q)uit | (r)eset | (c)ontinue ]'
answer = play_again?(%i(quit q continue c reset r))
#+end_src

While doing this, I improved my testing framework in order to show the test number for reference as needed:

#+begin_src ruby :results output :exports both
def play_again_tests(tests)
  tests.each_with_index do |test, index|
    puts "Test #{'%2d' % (index + 1)} ((#{play_again?(test[0], test[1]) == test[2] ? "PASS" : "FAIL"})) play_again?(#{test[0].inspect}, #{test[1].inspect}) == #{test[2].inspect}"
  end
end

def play_again?(args, player_choice)
  player_choice = player_choice.downcase.to_sym
  player_choice[0].to_sym if args.include?(player_choice)
end

play_again_tests([
  [%i(yes y no n), 'y', :y],
  [%i(yes y no n), 'yes', :y],
  [%i(yes y no n), 'yeserie', nil],
  [%i(yes y no n), 'Y', :y],
  [%i(yes y no n), 'YES', :y],
  [%i(yes y no n), '', nil],
  [%i(yes y no n), ' ', nil],
  [%i(yes y no n), 'y3s', nil],
  [%i(yes y no n), 'y e s', nil],
  [%i(yes y no n), '1', nil],
  [%i(quit q continue cont c reset r), 'q', :q],
  [%i(quit q continue cont c reset r), 'quit', :q],
  [%i(quit q continue cont c reset r), 'cont', :c],
  [%i(quit q continue cont c reset r), 'quitter', nil],
  [%i(quit q continue cont c reset r), ' cont', nil],
  [%i(quit q continue cont c reset r), '', nil],
  [%i(quit q continue cont c reset r), ' ', nil]
])
#+end_src

#+RESULTS:
#+begin_example
Test  1 ((PASS)) play_again?([:yes, :y, :no, :n], "y") == :y
Test  2 ((PASS)) play_again?([:yes, :y, :no, :n], "yes") == :y
Test  3 ((PASS)) play_again?([:yes, :y, :no, :n], "yeserie") == nil
Test  4 ((PASS)) play_again?([:yes, :y, :no, :n], "Y") == :y
Test  5 ((PASS)) play_again?([:yes, :y, :no, :n], "YES") == :y
Test  6 ((PASS)) play_again?([:yes, :y, :no, :n], "") == nil
Test  7 ((PASS)) play_again?([:yes, :y, :no, :n], " ") == nil
Test  8 ((PASS)) play_again?([:yes, :y, :no, :n], "y3s") == nil
Test  9 ((PASS)) play_again?([:yes, :y, :no, :n], "y e s") == nil
Test 10 ((PASS)) play_again?([:yes, :y, :no, :n], "1") == nil
Test 11 ((PASS)) play_again?([:quit, :q, :continue, :cont, :c, :reset, :r], "q") == :q
Test 12 ((PASS)) play_again?([:quit, :q, :continue, :cont, :c, :reset, :r], "quit") == :q
Test 13 ((PASS)) play_again?([:quit, :q, :continue, :cont, :c, :reset, :r], "cont") == :c
Test 14 ((PASS)) play_again?([:quit, :q, :continue, :cont, :c, :reset, :r], "quitter") == nil
Test 15 ((PASS)) play_again?([:quit, :q, :continue, :cont, :c, :reset, :r], " cont") == nil
Test 16 ((PASS)) play_again?([:quit, :q, :continue, :cont, :c, :reset, :r], "") == nil
Test 17 ((PASS)) play_again?([:quit, :q, :continue, :cont, :c, :reset, :r], " ") == nil
#+end_example

I'll be using this format for my testing going forward.

** Computer Defense
This method determines whether or not there's an immediate threat against the computer. If there is, it returns position. If not, returns ~nil~.

An immediate threat is considered to be a winning line where the player has already marked two of the three positions.

The logic for this is based on the following condition:

#+begin_src ruby
values.include?(INITIAL_MARKER) && values.count(PLAYER_MARKER) == 2
#+end_src

This checks to see if the three values of a winning line include at least one blank space (~INITIAL_MARKER~) and whether or not two of the spaces are marked by the ~PLAYER_MARKER~. If both conditions are ~true~, then there's an immediate threat of the player winning.

In that case, find the position of the blank space ~INITIAL_MARKER~ and then return the open position of the winning line:

#+begin_src ruby
threat_pos = values.find_index(INITIAL_MARKER)
return line[threat_pos]
#+end_src

Now, when I test the program, it appears the computer is reacting to the player's actions. It's not perfect, as it will still fail to choose a winning position if it detects an immediate threat, but I'll work on that more later.
