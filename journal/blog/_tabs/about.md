---
layout: page
title: About
permalink: /about/
icon: far fa-address-book
order: 1
---

>**It's simple. To be a learner, you've got to be willing to be a fool.** ([Leonard, 172](https://archive.org/details/masterykeystolon00leon_0/mode/2up))

This site is to surface my notes from my weekly (or so) journal so more people can find and read the notes as needed.

I'm currently studying at [Launch School](https://launchschool.com) where I'm focusing on software engineering using Ruby and JavaScript.

>**Remember to keep a “growth mindset”. There’s no such thing as “cut out for X”. You can always get better at X if you keep at it.** ([Launch School](https://launchschool.com))
