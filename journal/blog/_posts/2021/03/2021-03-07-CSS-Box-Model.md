---
layout: post
title: CSS Box Model
date: 2021-03-07 09:28:00 -0500
excerpt_separator: <!--more-->
tags: launch_school HTML CSS ls202
---

Even though I have a lot of experience with HTML and CSS, I tend to focus more on results than fundamental understanding. When building the sites I've built up to this point, I've focused on getting things to work. I don't always have the time or energy to learn how things work. This is especially true about the `box-sizing` and `display` CSS properties.

<!--more-->

# What is the CSS box model?
The **CSS box model** describes how the browser calculates the box size for any given element. Every element and character requires a box-shaped segment of the page, the dimensions of which are calculated from browser defaults and CSS. Every box element has a `width`, `height`, `border`, `padding` and `margin`. The `padding`, `margin` and `border` attributes have separate properties for the left, right, top, and bottom of each element.

# What are the top three `display` properties and how do they affect `width`, `height`, `padding`, `border`, and `margin`?
The visual display model is based on the `display` property. There are at least 24 different values for this property, with the most common being `inlnie`, `block`, and `inline-block`.

Other common visual display models include:
- `list-item`
- `table-cell`
- `flex`
- `grid`

## Block
These are elements that have a `block` value for the `display` property. `block` elements are sometimes referred to as containers. The main container that holds all other elements is the `body` element.

In terms of layout, `block` elements will take up all available horizontal space within their parent container. The vertical space is determined by the height of the element's contents. By default in most browsers, the `box-sizing: content-box;` property is used so the overall `width` and `height` of an element will include any additional `padding` and/or `border` values.

It's possible to convert any element into a `block` element by setting the `display` property to the `block` value: `display: block;`. It's most common to convert `a` and `img` elements to `block` elements.

Most elements are `block` elements, including:
  + `section`, `article`, `aside`, `header`, `footer`
  + `p`
  + `h1` through `h6`
  + `blockquote`
  + `ul`, `ol`, `dl`
  + `figure` and `figcaption`
  + `form` and `fieldset`

## Inline
These are elements that have an `inline` value for the `display` property. `inline` elements can be placed side-by-side within a parent container. Top and bottom margins and padding along with `width` and `height` values are ignored (except for `img` elements). Left and right margins and padding are respected. `border` will be fully visible and may overlap the parent container's content when combined with the element's padding.

It's not possible to nest `inline-block` or `block` elements within most `inline` elements, but the opposite is possible. The `a` tag is an exception because it **is** possible to nest `block` and `inline-block` elements within an `a` tag as long as there are no interactive elements like `input`, `button`, `select`, `textarea`, or another `a` tag within the nested block. Depending on the browser, improperly nested elements may be rendered correctly, but this is browser-dependant and may not be the same in different browsers.

It's possible to convert any element into `inline` by setting the `display` property to the `inline` value: `display: inline;`. This is mostly used to override an existing rule that switched the element to another `dispay` value.

Common examples:
  + `span`
  + `b`, `i`, `u`, `strong`, `em`
  + `a`
  + `sub` and `sup`
  + `img`

## Inline-Block
These are elements that act like `block` elements but do not take up all available horizontal space within their parent containers when the `width` property is less than the parent's width. They can be placed next to `block`, `inline`, or other `inline-block` elements. The `padding`, `border`, and `margin` all work the same as with `block` elements.

It's possible to convert any element into `inline-block` by setting the `display` property to the `inline-block` value: `display: inline;`. This is mostly used to arrange elements horizontally instead of vertically, like navigation menus.

# What are the two main `box-sizing` properties how do they affect an element's size?
`box-sizing` tells the browser how to calculate the size of a block. The default for modern browsers is `content-box`. Technically, there's a third value, `padding-box`, but it is rarely, if ever, used.

## content-box
This is the default setting for `box-sizing` for all elements in modern browsers. With this `box-sizing` value, the `width` and `height` specify only the size of the content, not including `padding` and `border`. One must add the `width` and/or `height` to the `border` and `padding` in order to get the size of the box.

## border-box
With this `box-sizing` value, the `width` and `height` represent the total size of the box, including the content, `border`, and `padding`.

# What's the difference between `padding` and `margin`? When should you use each?
`padding` is the space between the content and the `border` (the clickable/visible bounds of an element). `margin` is the space between the `border` and other elements. Both `padding` and `margin` have top, bottom, right, and left options. `margin` can have a value of `auto` and `padding` cannot. Top and bottom margins collapse into the larger of the two margins whereas `padding` does not collapse.

In general, it's a good idea to use margins everywhere unless padding is required. :)

More specifically, `padding` is helpful when one needs to:
- change the height and width of a border
- adjust how much background is visible around an element
- add more to or subtract from the clickable/visible area
- if margins are collapsing, use `padding` instead
- for `inline` elements, `padding` can be used to add horizontal space
- add space between the content and the left and right sides of the container

# What are the two broad types of dimensions? Give examples of each.
## Absolute Units
Absolute units are standard units across browsers that don't change based on page content. The only absolute unit in CSS is the `pixel` or `px`. The size of a pixel is not standard across devices, so there are two measurements CSS uses: the physical pixel and the **CSS Reference Pixel**.

The CSS Reference Pixel is a standard that is calculated based on the physical size of a pixel on a display that has 96 pixels per inch. CSS uses the reference pixel combined with the Typical Viewing Distance (TVD) to determine the size of elements for a given device.

In general, use absolute units for:
- root font size
- image width and heights
- top and bottom margins
- width or height of fixed-width/fixed-height containers
- border widths

## Relative Units
Relative units are not standard and can vary based on other elements like the screen size and other factors. Some examples of relative units used in CSS:

- `%`
- `em`
- `rem`
- `auto`

### Percentage
Percentage (`%`) is a fraction of a container's `width` or `height`. Given a container that's `300px` wide and `200px` high, a value of `50%` for the `width` will be equivalent to `150px` and `50%` for the `height` will be `100px`. If the container size is fixed, these calculated values will also be fixed.

If the container size is flexible, these calculated values will change as the `width` and/or `height` changes. This can be used for images and other elements where one wants them to change based on the size of their parent container.

### EM
`em` changes based on the calculated font size. This can be confusing since it's based on the parent container's calculated font size, so `1em` will not necessarily equal `1em` in all parts of the code. For example, given a root font size of `16px`, elements nested within a `div` set to a font size of `1.5em` will be `24px`.

```css
html {
  font-size: 16px;
}

.outer-div {
  font-size: 1.5em;
}
```

```html
<p>This is the root font size of 16px.</p>

<div class="outer-div">
  <p>This is `1.5em` and will be 24px.</p>
</div>
```

![Outer div Font Size](../../assets/img/outer-div-font-size.png)

If we then nest another `div` within the existing `div`, the font size of elements nested within this inner `div` also set to a font size of `1.5em` will now be `36px`:

```css
html {
  font-size: 16px;
}

.outer-div {
  font-size: 1.5em;
}

.inner-div {
  font-size: 1.5em;
}
```

```html
<p>This is the root font size of 16px.</p>

<div class="outer-div">
  <p>This is `1.5em` and will be 24px.</p>

  <div class="inner-div">
    <p>This is `1.5em` and will be 36px.</p>
  </div>
</div>
```

![Inner div Font Size](../../assets/img/inner-div-font-size.png)

This demonstrates that the compounding nature of `em` can make it difficult to determine the font size from looking at the CSS alone.

### REM
`rem`, on the other hand, is always based on the root font size. It's best to set the root font size in pixels. `1rem` will always be equal to the font size defined in the `html` element, regardless of where it is in the code.

Using the example above, `1.5rem` will always be based on the root font size regardless of a parent element's font-size:

```css
html {
  font-size: 16px;
}

.outer-div {
  font-size: 1.5rem;
}

.inner-div {
  font-size: 1.5rem;
}
```

```html
<p>This is the root font size of 16px.</p>

<div class="outer-div">
  <p>This is `1.5rem` and will be 24px.</p>

  <div class="inner-div">
    <p>This is `1.5rem` and will be 24px.</p>
  </div>
</div>
```

![Inner div Font Size rem](../../assets/img/inner-div-font-size-rem.png)

### Auto
`auto` is used to let the browser determine the size of the width or height. It's commonly used to center a block by setting both the left and right margins to `auto`. Given the `box-sizing: content-box` visual model, in comparison to setting `width: 100%`, `width: auto` will limit the width of the element to the width of the parent container. `width: 100%`, on the other hand, will set the width of the element to the actual size of the parent container's width without regard for whether or not the element fits within the parent container.

Here's an example:

```css
div {
  background-color: rgb(219, 219, 253);
  padding: 20px;
  margin: 20px;
  width: 500px;
}

.inner-div {
  background-color: rgb(227, 137, 137);
  padding: 20px;
  width: 100%;
}

.auto-width {
  width: auto;
}
```

```html
<div>
  <p>This is the outer div with 500px width.</p>
  <div class="inner-div">
    <p>This is the inner div with 500px width.</p>
  </div>
</div>

<div>
  <p>This is the outer div with 500px width.</p>
  <div class="inner-div auto-width">
    <p>This is the inner div with 420px width.</p>
  </div>
</div>
```

![auto vs 100% width](../../assets/img/auto-vs-100-percent-width.png)

In general, use `auto` for:
- font sizes
- left and right margins and/or padding
