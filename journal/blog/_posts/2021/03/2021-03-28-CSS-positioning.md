---
layout: post
title: CSS Positioning
date: 2021-03-28 08:22:00 -0500
excerpt_separator: <!--more-->
tags: launch_school HTML CSS ls202
---

I've been focusing on positioning including `static`, `relative`, `absolute`, and `fixed`. Here are some new or interesting things I learned.

<!--more-->

# position: static
This is the default positioning value. The most important points to remember about `position: static` are:

- the element is part of the page flow
- the offset properties `top`, `right`, `bottom`, `left` do not affect static elements

# position: relative
This positioning value moves the element to a new position that's relative to where it would normally be in the page flow. Therefore, the next element will be positioned as if the relative element was placed where it would normally be in the page flow, so either below or beside that space.

It's best to only use two of the four positioning properties, either `top` / `bottom` or `left` / `right`. If both `top` and `bottom` are used, then `bottom` is ignored. If both `left` and `right` is used, then depending on the flow of the text and whether it's left-to-right or right-to-left, one will be ignored.

Here it is in action:

![Relative Positioning](../../assets/img/relative_positioning.png)

The first column shows the elements in their static position. The arrows show the direction the element is moved with relative positioning and the positioning property or properties added.

As one can see from the image, the element moves the same with both `top` and `bottom` positioning properties added as it does when only the `top` positioning property is added. The same is true for `left` and `right`.

# position: absolute
This positioning value moves the element to a new position based on an ancestor (parent, grandparent, etc...) element that has relative, absolute, or sticky positioning. If no ancestor is positioned in one of those ways, the absolute position of the element is based on the page `body`.

The element is removed from the normal page flow so elements that come after this element are placed as if the absolute element doesn't exist.

All four positioning values are used to position the element. Here's an example:

![Absolute Positioning](../../assets/img/absolute_positioning.png)

Unlike the relative positioning example, when we apply absolute positioning to the elements, they're placed based on the immediate parent container. Using both `left` and `right` or `top` and `bottom` positioning properties skews the element to fit the desired space.

Finally, this example shows how the two other elements position themselves as if the second element didn't exist and that second element, being outside of the normal page flow, overlaps the other elements.

# position: fixed
This positioning value sets the element to a position based on the browser window. The element is removed from the flow of the page and remains in the same place when the page scrolls. Here is an example:

![Fixed Positioning](../../assets/img/fixed_positioning.gif)

This example shows how the second element stays in the same place in the window regardless of any other element. It also shows that the remaining two elements place themselves as if the second element didn't exist.
