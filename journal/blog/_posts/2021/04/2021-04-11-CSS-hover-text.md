---
layout: post
title: CSS Hover Text
date: 2021-04-11 08:25:00 -0500
excerpt_separator: <!--more-->
tags: launch_school HTML CSS ls202
---

While working on HTML/CSS projects, there are two interesting techniques I was introduced to that allow for actions on mouse hovering and clicking with pure CSS.

<!--more-->

Previously, I relied on copying/pasting code to do these things whenever I needed it. Sometimes that involved pure CSS, other times it involved JavaScript. Now, I'm more comfortable writing my own CSS to create these types of actions.

# Action on Click
For this, we want to enable some behavior when clicking on an item.

## HTML
This is the HTML that I started with:

```html
<div class="product-box">
  <label>
    <input type="checkbox">
    <figure>
      <img src="./spider.jpg" alt="Spider">
    </figure>
    <h3>Spider</h3>
    <p>$12.99</p>
  </label>
</div>
```

Then, to get the click action, I use this CSS:

```css
input[type="checkbox"]:checked + figure img {
  height: auto;
  left: 50%;
  margin: 0 0 0 -200px;
  position: absolute;
  top: 100px;
  width: 400px;
  z-index: 2;
}

input[type="checkbox"]:checked + figure::before {
  background-color: rgba(0, 0, 0, 0.3);
  bottom: 0;
  content: "";
  left: 0;
  position: fixed;
  top: 0;
  right: 0;
  z-index: 1;
}

input[type="checkbox"] {
  display: none;
}
```

The key is to use the CSS Adjacent Sibling Selector (`+`) to select the adjacent sibling element when the `input` is checked.

Technically, this is not valid HTML because the `figure`, `h3` and `p` elements are expected to be used as **flow content** but the `label` element is **phrasing content**. However, it seems to work in both Firefox and Chrome.

Here it is in action:

![Action on Click](../../assets/img/action_on_click.gif)

# Action on Hover
To get an action when the mouse is hovering over an item, I use this HTML:

```html
<article>
  <img src="./spider.jpg" alt="Spider">
  <h2><a href="#">Spider</a></h2>
  <p>This is a spider.</p>
  <div class="hover-text">
    <h2>Spider</h2>
    <p>
      This is a harmless house spider. It's actually very small, but the
      photo lets us see the detail and it appears to be much larger.
    </p>
  </div>
</article>
```

And this CSS:

```css
.hover-text {
  display: none;
}

article:hover .hover-text {
  background-color: rgba(0, 0, 0, 0.9);
  color: #fff;
  display: block;
  font-size: 13px;
  font-weight: 300;
  height: 378px;
  line-height: 15px;
  padding: 23px;
  position: absolute;
  top: 0;
  z-index: 2;
}
```

This code first hides the `hover-text`. Then, it adds the `hover-text` styles when the mouse is hovering over the `article` element. This is valid HTML and CSS.

Here's what it looks like:

![Action on Hover](../../assets/img/action_on_hover.gif)

I don't know if these are the best ways to achieve these actions, but they make sense to me and seem to work well in both Chrome and Firefox.
