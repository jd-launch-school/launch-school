---
layout: post
title: SQL Subquery Expressions
excerpt_separator: <!--more-->
tags: launch_school sql ls180
---

Another powerful way to query data from multiple tables, other than JOINs, is to use subqueries. I go through the subquery expressions and try to come up with reasonable examples for their use.

<!--more-->

# The Data
The following schema will be used for the examples in this post.

```sql
CREATE TABLE users (
    PRIMARY KEY (id),
    id         SERIAL,
    first_name TEXT      NOT NULL,
    last_name  TEXT,
    email      TEXT,
    created_at TIMESTAMP NOT NULL
                         DEFAULT(CURRENT_TIMESTAMP)
);

CREATE TABLE books (
    PRIMARY KEY (id),
    id             SERIAL,
    title          TEXT      UNIQUE NOT NULL,
    date_published DATE      DEFAULT('1970-01-01'),
    publisher_id   INTEGER   REFERENCES publishers(id)
                             ON DELETE CASCADE,
    created_at     TIMESTAMP NOT NULL
                             DEFAULT(CURRENT_TIMESTAMP)
);

CREATE TABLE checkouts (
    PRIMARY KEY (id),
    id         SERIAL,
    user_id    INTEGER   NOT NULL
                         REFERENCES users(id)
                         ON DELETE CASCADE,
    book_id    INTEGER   NOT NULL
                         REFERENCES books(id)
                         ON DELETE CASCADE,
    due        DATE      NOT NULL
                         DEFAULT(CURRENT_DATE + 14),
    created_at TIMESTAMP NOT NULL
                         DEFAULT(CURRENT_TIMESTAMP),
    UNIQUE(user_id, book_id)
);
```

## Users Table
```
 id | first_name | last_name |             email              |         created_at
----+------------+-----------+--------------------------------+----------------------------
  1 | Sherman    | Abbott    | Janae@edgar.ca                 | 2021-02-13 08:28:52.415286
  2 | Kaycee     | Homenick  | Brian_Lebsack@richmond.biz     | 2021-02-13 08:28:52.415286
  3 | Florencio  | Hermiston | Audreanne.Predovic@shaina.name | 2021-02-13 08:28:52.415286
  4 | Margret    | Rodriguez | Jimmie_Swaniawski@lisandro.me  | 2021-02-13 08:28:52.415286
  5 | Jovanny    | Kerluke   |                                | 2021-02-13 08:28:52.415286
  6 | Easter     | Gutkowski | Arjun@ova.org                  | 2021-02-13 08:28:52.415286
  7 | Manuel     | Corwin    | Neal_King@eda.ca               | 2021-02-13 08:28:52.415286
  8 | Clement    | Grant     |                                | 2021-02-13 08:28:52.415286
  9 | Madelynn   | Barrows   | Fernando@amina.com             | 2021-02-13 08:28:52.415286
 10 | Simone     | Trantow   | Ethyl@emmalee.ca               | 2021-02-13 08:28:52.415286
(10 rows)
```

## Books Table
```
id | title                               | date_published | publisher_id | created_at
---+-------------------------------------+----------------+--------------+---------------------------
1  | Femme Nikita, La (Nikita)           | 1975-01-24     | 9            | 2021-02-11 10:54:41.366764
2  | Kummeli Stories                     | 2002-12-25     | 8            | 2021-02-11 10:54:41.366764
3  | Chariots of Fire                    | 1970-11-06     | 5            | 2021-02-11 10:54:41.366764
4  | The Sinners of Hell                 | 1998-03-12     | 5            | 2021-02-11 10:54:41.366764
5  | Battery, The                        | 2004-03-14     | 2            | 2021-02-11 10:54:41.366764
6  | Otakus in Love                      | 1996-12-04     | 10           | 2021-02-11 10:54:41.366764
7  | Soap and Water                      | 1982-11-20     | 3            | 2021-02-11 10:54:41.366764
8  | Wrong Move, The (Falsche Bewegung)  | 2006-07-01     | 7            | 2021-02-11 10:54:41.366764
9  | Off and Running                     | 2004-09-07     |              | 2021-02-11 10:54:41.366764
10 | Not Reconciled                      | 1973-04-04     | 4            | 2021-02-11 10:54:41.366764
11 | Calculator                          | 1983-06-11     | 2            | 2021-02-11 10:54:41.366764
12 | Metro                               | 1987-06-20     | 6            | 2021-02-11 10:54:41.366764
13 | Yellow Earth (Huang tu di)          | 2016-09-09     | 7            | 2021-02-11 10:54:41.366764
14 | Magic Camp                          | 1987-01-10     | 11           | 2021-02-11 10:54:41.366764
15 | Star Trek III: The Search for Spock | 1981-10-25     | 9            | 2021-02-11 10:54:41.366764
16 | Figures: The Movie                  | 2013-06-13     |              | 2021-02-11 10:54:41.366764
17 | Castle of Cloads, The (Pilvilinna)  | 1983-08-28     | 7            | 2021-02-11 10:54:41.366764
18 | Aliens in Uniform                   | 1974-08-23     | 3            | 2021-02-11 10:54:41.366764
19 | Man Entertained                     | 1983-04-24     | 11           | 2021-02-11 10:54:41.366764
20 | Taste of Tea, The (Cha no aji)      | 1997-12-04     | 1            | 2021-02-11 10:54:41.366764
(20 rows)
```

## Checkouts Table
```
id | user_id | book_id | due        | created_at
---+---------+---------+------------+---------------------------
1  | 4       | 1       | 2021-02-27 | 2021-02-13 08:29:14.762501
2  | 4       | 3       | 2021-02-27 | 2021-02-13 08:29:14.762501
3  | 4       | 5       | 2021-03-10 | 2021-02-13 08:29:14.762501
4  | 4       | 6       | 2021-02-27 | 2021-02-13 08:29:14.762501
5  | 3       | 7       | 2021-02-27 | 2021-02-13 08:29:14.762501
6  | 7       | 8       | 2021-02-23 | 2021-02-13 08:29:14.762501
7  | 4       | 9       | 2021-02-08 | 2021-02-13 08:29:14.762501
8  | 10      | 10      | 2021-02-27 | 2021-02-13 08:29:14.762501
9  | 5       | 11      | 2021-02-15 | 2021-02-13 08:29:14.762501
10 | 2       | 12      | 2021-02-27 | 2021-02-13 08:29:14.762501
11 | 7       | 13      | 2021-02-18 | 2021-02-13 08:29:14.762501
12 | 8       | 14      | 2021-02-27 | 2021-02-13 08:29:14.762501
13 | 8       | 15      | 2021-02-27 | 2021-02-13 08:29:14.762501
14 | 2       | 16      | 2021-02-10 | 2021-02-13 08:29:14.762501
15 | 2       | 17      | 2021-02-27 | 2021-02-13 08:29:14.762501
16 | 10      | 19      | 2021-02-14 | 2021-02-13 08:29:14.762501
17 | 6       | 20      | 2021-02-27 | 2021-02-13 08:29:14.762501
(17 rows)
```

# Subqueries
A subquery is a query within a query. Subqueries can be used in `WHERE` clauses, as the virtual table of a `FROM` clause, and as a column being selected by the `SELECT` clause. In some cases, subqueries can be more efficient, more readable, and may make more logical sense than JOINs. For example, if we only want to return data from the left table but we want to use data in the right table, a subquery may be a good choice.

## WHERE Clause
Here, we're using a subquery in a `WHERE` clause. This is similar to using an `INNER JOIN`. We want to return the `title` of the book with id `14` that's been checked out.
```sql
SELECT title
  FROM books
 WHERE id = (SELECT id FROM checkouts
              WHERE book_id = 14);
```

```
 title
-------
 Metro
(1 row)
```

## FROM Clause
The results of a subquery in a `FROM` clause can be used as the virtual table we perform queries on. We want to return the total number of books user `4` has checked out.

```sql
SELECT COUNT(user_id)
  FROM (SELECT user_id FROM checkouts
         WHERE user_id = 4)
    AS user_4_checkouts;
```

```
 count
-------
     5
(1 row)
```

## Column Selection (Scalar Subqueries)
We can use the result of a subquery as the column of another query. The subquery must only return one row. Here, we can use a subquery to show each user along with the number of books they've checked out.

```sql
SELECT u.first_name || ' ' || u.last_name AS name,
       (SELECT COUNT(c.id)
          FROM checkouts AS c
         WHERE u.id = c.user_id) AS number_of_books_checked_out
  FROM users AS u
 ORDER BY number_of_books_checked_out DESC;
```

```
        name         | number_of_books_checked_out
---------------------+-----------------------------
 Margret Rodriguez   |                           5
 Kaycee Homenick     |                           3
 Simone Trantow      |                           2
 Manuel Corwin       |                           2
 Clement Grant       |                           2
 Easter Gutkowski    |                           1
 Florencio Hermiston |                           1
 Jovanny Kerluke     |                           1
 Madelynn Barrows    |                           0
 Sherman Abbott      |                           0
(10 rows)
```

# Subquery Expressions
Subquery expressions are SQL-compliant expressions that return Boolean results. The subquery expressions available in PostgreSQL are `EXISTS`, `IN`, `NOT IN`, `ANY/SOME`, and `ALL` ([PostgreSQL Subquery Expressions Docs](https://www.postgresql.org/docs/current/functions-subquery.html)).

In general, these expressions are most useful when we need a subquery to return more than one value and we need to compare values in the initial query to each of the values in the subquery.

## EXISTS
`EXISTS` returns `true` if **any** rows are returned from the subquery and `false` if no rows are returned.

In this example, we want to show the title of every book that is checked out without using a `JOIN`.

```sql
SELECT title
  FROM books
 WHERE EXISTS (SELECT 1 FROM checkouts
                WHERE book_id = books.id);
```

```
                title
-------------------------------------
 Femme Nikita, La (Nikita)
 Chariots of Fire
 Battery, The
 Otakus in Love
 Soap and Water
 Wrong Move, The (Falsche Bewegung)
 Off and Running
 Not Reconciled
 Calculator
 Metro
 Yellow Earth (Huang tu di)
 Magic Camp
 Star Trek III: The Search for Spock
 Figures: The Movie
 Castle of Cloads, The (Pilvilinna)
 Man Entertained
 Taste of Tea, The (Cha no aji)
(17 rows)
```

## IN
`IN` returns `true` if the column value is in the virtual table that's returned from the subquery. If the column value is not in the virtual table, then `IN` returns `false` and the row in the outer `SELECT` is not selected.

In the following example, we want to return the `id` and `title` of any books that are past due.

```sql
SELECT id, title AS "Past Due Books"
  FROM books
 WHERE books.id IN
       (SELECT book_id FROM checkouts
         WHERE due < CURRENT_DATE);
```
```
 id |   Past Due Books
----+--------------------
  9 | Off and Running
 16 | Figures: The Movie
(2 rows)
```

## NOT IN
`NOT IN` returns `true` if the column value is **NOT** in the virtual table returned from the subquery. If the column value is in the virtual table, then `NOT IN` returns `false` and the row in the outer `SELECT` is not selected.

In the following example, we want to return all books that are not checked out.

```sql
SELECT id, title AS "Available Books"
  FROM books
 WHERE books.id NOT IN
       (SELECT book_id FROM checkouts);
```
```
 id |   Available Books
----+---------------------
  2 | Kummeli Stories
  4 | The Sinners of Hell
 18 | Aliens in Uniform
(3 rows)
```

## ANY
`ANY` (or `SOME`) is used along with an operator (`=`, `<`, `>`, etc...) and returns `true` after the expression to the left of the operator is evaluated using the operator against each of the rows in the results of the subquery and **any** of the comparisons with the rows in the subquery return `true`.

In the following example, we want to return the `title` of any book where the length of the book's title is less than the length of any user's last name plus 3.
```sql
SELECT title
  FROM books
 WHERE length(title) < ANY
       (SELECT length(last_name) + 3
          FROM users);
```
```
   title
------------
 Calculator
 Metro
 Magic Camp
(3 rows)
```

## ALL
`ALL` is used along with an operator (`=`, `<`, `>`, etc...) and returns `true` after the expression to the left of the operator is evaluated using the operator against each of the rows in the results of the subquery and **all** of the comparisons with the rows in the subquery return `true`.

In the following example, we want to return the `title` of any book where the length of the title is greater than the length of all user's first names.

```sql
SELECT title
  FROM books
 WHERE LENGTH(title) < ALL
       (SELECT LENGTH(first_name) FROM users);
```
```
 title
-------
 Metro
(1 row)
```

# Summary
I found it difficult to come up with meaningful examples for `ANY` and `ALL` given the data that I'm working with here. However, other than that, SQL subqueries and subquery expressions can still be helpful.
