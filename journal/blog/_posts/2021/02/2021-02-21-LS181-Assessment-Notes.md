---
layout: post
title: LS181 Assessment Notes
date: 2021-02-21 16:32:00 -0500
excerpt_separator: <!--more-->
tags: launch_school sql ls180 assessments
---

The LS181 assessment was straightforward. This time, I tried to build a SQL database that enabled me to try out everything I'd learned.

<!--more-->

This time, since SQL was completely new to me, I decided to place my focus on the material as I was going through the course and focus less on what I may or may not need for the assessment. I built many databases and wrote many `CREATE`, `ALTER`, `UPDATE`, `INSERT`, and `SELECT` statements. I went over the exercises and practice problems multiple times until the SQL statements felt more natural to me.

I wasn't sure what the assessment would be like, so I wanted to be as prepared as possible. To do this with SQL databases, I reasoned that I'd need a test database to work with if I wanted to test things quickly. The last thing I wanted was to have to create a bunch of tables and insert data just to test an `ALTER` or `SELECT` statement.

So, I built a database with a schema and data that would allow me to test as much as I could on the fly.

For example, I purposely added `NULL` values so I would be able to test how `NULL` values may affect the various queries I may have been asked to explain.

I created tables with one-to-many and many-to-many relationships so I'd have that structure ready to go if I needed to work with `JOIN`s on the fly.

In the end, the database turned out to be relatively big in comparison to the practice databases I was used to up to this point. I ended up with 8 tables. This helped to ensure that I understood the entire process and pinpoint some of the areas I needed to focus more study on.

I used a site called [Mockaroo](https://mockaroo.com/) to help generate random but relevant data.

Here's a SQL dump of the database ([assessment_181.sql](/launch-school/assets/sql/assessment_181.sql)) for future reference.

Here are the `CREATE` statements to build the tables:

```sql
CREATE TABLE publishers (
    PRIMARY KEY (id),
    id           SERIAL,
    name         TEXT      UNIQUE NOT NULL,
    address      TEXT      NOT NULL,
    phone        TEXT      NOT NULL
                           CHECK(phone ~ '\d{10}'),
    email        TEXT,
    created_at   TIMESTAMP NOT NULL
                           DEFAULT(CURRENT_TIMESTAMP)
);

CREATE TABLE books (
    PRIMARY KEY (id),
    id             SERIAL,
    title          TEXT      UNIQUE NOT NULL,
    date_published DATE      DEFAULT('1970-01-01'),
    publisher_id   INTEGER   REFERENCES publishers(id)
                             ON DELETE CASCADE,
    created_at     TIMESTAMP NOT NULL
                             DEFAULT(CURRENT_TIMESTAMP)
);

CREATE TABLE genres (
    PRIMARY KEY (id),
    id   SERIAL,
    name TEXT    UNIQUE NOT NULL
);

CREATE TABLE books_genres (
    PRIMARY KEY (id),
    id       SERIAL,
    book_id  INTEGER NOT NULL
             REFERENCES books(id)
             ON DELETE CASCADE,
    genre_id INTEGER NOT NULL
             REFERENCES genres(id)
             ON DELETE CASCADE,
             UNIQUE(book_id, genre_id)
);

CREATE TABLE authors (
    PRIMARY KEY (id),
    id         SERIAL,
    first_name TEXT      NOT NULL,
    last_name  TEXT      NOT NULL,
    created_at TIMESTAMP NOT NULL
                         DEFAULT(CURRENT_TIMESTAMP)
);

CREATE TABLE authors_books (
    PRIMARY KEY (id),
    id        SERIAL,
    book_id   INTEGER NOT NULL
              REFERENCES books(id)
              ON DELETE CASCADE,
    author_id INTEGER NOT NULL
              REFERENCES authors(id)
              ON DELETE CASCADE,
              UNIQUE(book_id, author_id)
);

CREATE TABLE users (
    PRIMARY KEY (id),
    id         SERIAL,
    first_name TEXT      NOT NULL,
    last_name  TEXT,
    email      TEXT,
    admin      BOOLEAN,
    created_at TIMESTAMP NOT NULL
                         DEFAULT(CURRENT_TIMESTAMP)
);

CREATE TABLE checkouts (
    PRIMARY KEY (id),
    id         SERIAL,
    user_id    INTEGER   NOT NULL
                         REFERENCES users(id)
                         ON DELETE CASCADE,
    book_id    INTEGER   NOT NULL
                         REFERENCES books(id)
                         ON DELETE CASCADE,
    due        DATE      NOT NULL
                         DEFAULT(CURRENT_DATE + 14),
    created_at TIMESTAMP NOT NULL
                         DEFAULT(CURRENT_TIMESTAMP),
    UNIQUE(user_id, book_id)
);
```
