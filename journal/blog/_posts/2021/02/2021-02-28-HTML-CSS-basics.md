---
layout: post
title: HTML and CSS Basics
date: 2021-02-28 09:28:00 -0500
excerpt_separator: <!--more-->
tags: launch_school HTML CSS ls202
---

I'm now in the front end portion of the Launch School curriculum where we learn the fundamentals of HTML, CSS, and JavaScript. I have previous experience with front end work, so this is mostly review at this point.

<!--more-->

In the first lesson, we learn the basics of HTML and CSS. I already know most of what's presented in the lesson, so for this post I will highlight some of the things I either didn't know or feel I need to know better and remember.

# Using HTML Escape Codes
I've known about HTML escape codes, like `&amp;`, `&copy;`, and `&quot;` to replace `&`, `©`, and `"` respectively.

Here's a helpful reference to find HTML escape codes:

[HTML ESCAPE CODES](http://htmlandcssbook.com/extras/html-escape-codes/)

However, I learned that it's important to use escape codes in strings that get added to HTML externally. I knew this on some level from a security perspective, that it's always important to escape user input. I didn't think about it from a programmatic perspective where even if it's not user input, it's still helpful to use escape codes when adding HTML from code, specifically `&quot;`, to escape quotes since we never know how those quotes will affect the surrounding HTML.

# `<b>` and `<i>`
It's my understanding that these tags have more or less been deprecated in favor of `<strong>` and `<em>`. In terms of using these tags for styling, that's true. However, these tags still exist and they bring with them specific semantic meaning.

`<b>` can be used to "stylistically offset text". I'm not entirely sure what that means as it implies it could be used to style text that's different from surrounding text, similar to `<strong>`. However, maybe it also means style the text differently even if that text is not important.

`<i>` can be used to signify that some text is an alternate voice. The example given is: I said "Hello." She said "*Goodbye*."

# Consistently Use Trailing Slash `/`
For self-closing tags, I learned previously that the trailing slash is unnecessary. I'd internalized this so that I no longer use them. For example:

```html
<img src="...">
<br>
```

Instead of:

```html
<img src="..." />
<br />
```

Launch School suggests that using the trailing slash for self-closing tags is OK. Both styles are correct. However, if one chooses to use trailing slashes, they should use them consistently. The same with not using them ... consistently don't use them.

This isn't very important information, but it does make sense when trying to determine which style to use.

# Functional Groups for CSS Selectors
I tend to structure my CSS with tag selectors at the top of the file and class (and ID, but I don't use IDs for styling) selectors towards the bottom. Launch School suggests, alternatively, to section the CSS into functional groups like header-specific selectors in one section, article-specific selectors in another section, etc...

My typical CSS might look like:

```css
body {
  ...
}

header {
  ...
}

p {
  ...
}

.menu {
  ...
}

.some-list {
  ...
}

.another-list {
  ...
}
```

I might consider changing to something more like this:

```css
/* General Page Styles */
body {
  ...
}

p {
  ...
}

/* Header Styles */
header {
  ...
}

.menu {
  ...
}

/* List Styles */
.some-list {
  ...
}

.another-list {
  ...
}
```

I'll try this going forward and see if it helps to make CSS files more organized and more easy to navigate and read.

# Prefer Class Selectors
I am aware of half of this. I strongly prefer to use class selectors over ID selectors. However, Launch School also suggests preferring class selectors over tag selectors.

Classes can be used for multiple elements within an HTML file whereas IDs are supposed to be unique per HTML page. So, when we style an ID selector, that creates a selector with high specificity that may be difficult to change or override later.

It can be similarly cumbersome to change or override tag selectors. Styling all `<p>` elements on a page is fine, but then we have to add more specific styling for any `<p>` elements we want to look different.

So, when possible, it's best to style classes.

Here's an example:

```html
<p class="main-text" id="info">This is some text.</p>
<p class="main-text" id="more-info">This is some more text.</p>
<p class="main-text" id="less-info">This is even more text.</p>
```

With the HTML above, we can set the `font-size` for `<p>` elements like this:

```css
p {
  font-size: 2rem;
}
```

To change the color of the middle `<p>` element, we can do this:

```css
p {
  font-size: 2rem;
}

#more-info {
  color: red;
}
```

Later, we want to change the size for that middle `<p>` element:

```css
p {
  font-size: 2rem;
}

#more-info {
  color: red;
  font-size: 1rem;
}
```

Then, we want to add another `<p>` element with the same size as the middle `<p>` element:

```html
<p class="main-text" id="info">This is some text.</p>
<p class="main-text" id="more-info">This is some more text.</p>
<p class="main-text" id="less-info">This is even more text.</p>

<!-- other code -->

<p id="some-info">This is some info.</p>
```

```css
p {
  font-size: 2rem;
}

#more-info {
  color: red;
  font-size: 1rem;
}

#some-info {
  font-size: 1rem;
}
```

We're starting to see some duplication. If we use a class selector instead, we can remove the `main-text` class from the middle `<p>` element. Then, we can add an `important` class so that we can change the color of the middle `<p>` element and re-use that style for other `<p>` elements as needed.

```html
<p class="main-text" id="info">This is some text.</p>
<p class="important" id="more-info">This is some more text.</p>
<p class="main-text" id="less-info">This is even more text.</p>

<!-- other code -->

<p id="some-info">This is some info.</p>
```

```css
.main-text {
  font-size: 2rem
}

.important {
  color: red;
}
```

I don't know if this is the best example, but it sort of points to the idea that selecting classes will give us more flexibility to handle future changes without the specificity issues we might encounter with tag and ID selectors.
