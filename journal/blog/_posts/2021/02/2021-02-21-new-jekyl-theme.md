---
layout: post
title: New Jekyll Theme
date: 2021-02-21 18:04:00 -0500
excerpt_separator: <!--more-->
tags: jekyll theme
---

I took some time today to configure a new theme for the blog. My old theme served me well, but this theme has some helpful features and looks better for the kind of writing I want to do.

<!--more-->

I recently started a new project with Ruby 3.0.0. When I attempted to start the local Jekyll server to start writing a new post, I noticed an error stating that one of the Jekyll plugin dependencies was not compatible with Ruby 3.0.0. Normally, I'd set that project to use an older version of Ruby, but since I've been wanting to update my theme for a few months now, I used this as encouragement to both upgrade to Jekyll 4 and get a new theme.

I chose version `v3.2.1` of the [Chirpy](https://github.com/cotes2020/jekyll-theme-chirpy) theme. It seems to have a nice feature set and looks clean.

- I really like the [ARCHIVES tab](/launch-school/archives/).
- I also like the way the search field works and how individual tag pages show the dates of the posts with that tag.
  - Example: [assessments tag page](/launch-school/tags/assessments/)
- The reading time and word-count for each post are nice.
- The light and dark mode colors are pleasing and professional.

I only had to make a few adjustments in order to get things the way I like them.

# GitLab Instead of GitHub
One adjustment I had to make was to add GitLab to the social links instead of GitHub. There may have been a different way to do this via the `_config.yml` file, but I didn't try that. I deleted the GitHub and Twitter entries and replaced them with a GitLab entry. Here's the code for that:

**_data/contact.yml**
```yml
-
  type: gitlab
  icon: 'fab fa-gitlab'
  url: 'https://gitlab.com/jd-launch-school'
```

# Remove Categories Tab
I removed the `Categories` tab because I only use tags. To do this, I commented out all of the lines in the `_tabs/categories.md` file. Again, there's likely a better way to do this but this works for my needs for now.

**_tabs/categories.md**
```md
<!-- --- -->
<!-- layout: categories -->
<!-- title: Categories -->
<!-- icon: fas fa-stream -->
<!-- order: 1 -->
<!-- --- -->
```

# Offset Headers in Kramdown
In the `Kramdown` section of `_config.yml`, I added `header_offset: 1` because I feel there's never a case where I want to have an `h1` header in the content of my blog posts or pages.

**_config.yml**
```yml
kramdown:
  syntax_highlighter: rouge
  syntax_highlighter_opts:   # Rouge Options › https://github.com/jneen/rouge#full-options
    css_class: highlight
    # default_lang: console
    span:
      line_numbers: false
    block:
      line_numbers: true
      start_line: 1
  header_offset: 1
```

# Full Text RSS Feed
I added the `jekyll-feed` plugin because I couldn't get the full content of posts shown in the RSS feed with the default `feed.xml` that comes with the theme:

**_config.yml**
```yml
plugins:
  - jekyll-feed
```

I then renamed the default `feed.xml` to `feed.xml_backup` so as not to override the `jekyll-feed` plugin's `feed.xml`.

# Use `post.excerpt` for Home Page
By default, the home page only shows an excerpt from each post and it's not based on `post.excerpt`. Since I've been using `post.excerpt` in my posts using the old theme, my posts are set up to explicitly state what I want shown as the excerpt on the home page. I found an issue mentioning this: "[Post excerpts: Use post.excerpt syntax for home page content](https://github.com/cotes2020/jekyll-theme-chirpy/issues/42)". The theme author doesn't want this functionality, so I made the change to `home.html` and `home.scss` myself.

**_layouts/home.html**
{% raw %}
```html
    <div class="post-content">
      <p>
        {% include no-linenos.html content=post.content %}
        <!-- {{ content | markdownify | strip_html | truncate: 200 }} -->
        {{ post.excerpt }}
      </p>
    </div>
```
{% endraw %}

**_sass/layout/home.scss**
```scss
#post-list {
  // other code ...

  .post-preview {
    // other code ...
    }

    .post-content {
      margin-top: 0.6rem;
      margin-bottom: 0.6rem;
      color: var(--post-list-text-color);
      > p {
        /* Make preview shorter on the homepage */
        margin: 0;
        // overflow: hidden;
        // text-overflow: ellipsis;
        // display: -webkit-box;
        // -webkit-line-clamp: 2;
        // -webkit-box-orient: vertical;
      }
    }
```

# Remove Post Sharing Options
I don't like Twitter and Facebook, so I don't want to encourage people to share my posts on these platforms. Obviously, I can't control what people do manually, but I can remove the links that make it easier to share. I commented out all of the sharing options so that the only option under each post is to copy the link of the post.

# Summary
Those are the only changes I made beyond the typical customizations mentioned in the wiki and README. All-in-all, this is a really well-made theme!
