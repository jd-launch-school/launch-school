---
layout: post
title: Thinking About Inheritance
date: 2021-12-12 18:50:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js225 inheritance objects constructor_functions classes
---

I recently came across code in the wild that's similar to the code below. It seems like there might be some unnecessary duplication, so I applied some of the concepts I'm currently learning about, namely prototypal inheritance and classes.

<!--more-->

# Current Code
Here's the current code:

```js
var object1 = {
  property1: "string1",
  property2: "string2",
  property3: "string2",
  property4: "string3",
  property5: "string4",
  property6: "string5",
  property7: "string6",
  property8: "string7",
  property9: "string8",
  property10: "string9",
  property11: "string10",
  property12: "string11",
  property13: "string12",
  property14: "string13"
}

var object2 = {
  property1: "string1",
  property2: "string2",
  property3: "string2",
  property4: "string3",
  property5: "string4",
  property6: "string5",
  property7: "string6",
  property8: "string7",
  property9: "string8",
  property10: "string9",
  property11: "string10",
  property12: "string11",
  property13: "string12",
  property14: "string13"
}

var object3 = {
  property1: "string1",
  property2: "string2",
  property3: "string1",
  property4: "string1",
  property5: "string1",
  property6: "string1",
  property7: "",
  property8: "",
  property9: "",
  property10: "",
  property11: "",
  property12: "string1",
  property13: "",
}

var object4 = {
  property1: "string1",
  property2: "string2",
  property3: "string1",
  property4: "string1",
  property5: "string1",
  property6: "string1",
  property7: "string1",
  property8: "",
  property9: "",
  property10: "",
  property11: "",
  property12: "string1",
  property13: "",
}

var object5 = {
  property1: "",
  property2: "",
  property3: "",
  property4: "",
  property5: "",
  property6: "",
  property7: "",
  property8: "",
  property9: "",
  property10: "",
  property11: "",
  property12: "",
  property13: "",
  property14: ""
}

var object6 = {
  property1: "",
  property2: "",
  property3: "",
  property4: "",
  property5: "",
  property6: "",
  property7: "",
  property8: "",
  property9: "",
  property10: "",
  property11: "",
  property12: "",
  property13: "",
  property14: ""
}

var object7 = {
  property1: "",
  property2: "",
  property3: "",
  property4: "",
  property5: "",
  property6: "",
  property7: "",
  property8: "string28",
  property9: "string29",
  property10: "",
  property11: "",
  property12: "",
  property13: "",
}

var object8 = {
  property1: "",
  property2: "",
  property3: "",
  property4: "",
  property5: "",
  property6: "",
  property7: "",
  property8: "",
  property9: "",
  property10: "",
  property11: "string29",
  property12: "",
  property13: "string30",
}
```

# Reduce Duplication with the `Object.assign` Property
One way to reduce duplicated code is to use the `Object.assign` method. It's not inheritance, but it can be used as a quick way to simulate inheritance. This method returns a new object that contains the same properties as the given object. When we change a value in one object, the other object is not affected by that change. This is similar to changing values in child objects while the parent object is left unaffected.

```js
const object1 = {
  property1: '',
  property2: '',
  property3: '',
  property4: '',
  property5: '',
  property6: '',
  property7: '',
  property8: '',
  property9: '',
  property10: '',
  property11: '',
  property12: '',
  property13: '',
  property14: '',
};

const object2 = {
  property1: 'string1',
  property2: 'string2',
  property3: 'string3',
  property4: 'string4',
  property5: 'string5',
  property6: 'string6',
  property7: 'string7',
  property8: 'string8',
  property9: 'string9',
  property10: 'string10',
  property11: 'string11',
  property12: 'string12',
  property13: 'string13',
  property14: 'string14',
};

const object3 = Object.assign({}, object2);
const object4 = Object.assign({}, object3);
const object5 = Object.assign({}, object);
const object6 = Object.assign({}, object);
const object7 = Object.assign({}, object);
const object8 = Object.assign({}, object);

object3.property7 = '';
object3.property8 = '';
object3.property9 = '';
object3.property10 = '';
object3.property11 = '';
object3.property13 = '';
object4.property7 = 'string7';
object7.property8 = 'string28';
object7.property9 = 'string29';
object8.property11 = 'string29';
object8.property13 = 'string30';
```

This is the most straightforward of the options I list in this post, but it's not resilient. Future objects have to copy one of the already existing objects. That can get complicated with lots of objects, keeping track of who's the parent of what child.

# Constructor Functions
Another way to reduce duplicated code is to make use of inheritance with **constructor functions**:

```js
function Object1() {
  this.property1 = '';
  this.property2 = '';
  this.property3 = '';
  this.property4 = '';
  this.property5 = '';
  this.property6 = '';
  this.property7 = '';
  this.property8 = '';
  this.property9 = '';
  this.property10 = '';
  this.property11 = '';
  this.property12 = '';
  this.property13 = '';
  this.property14 = '';
}

function Object2() {
  this.property1 = 'string1';
  this.property2 = 'string2';
  this.property3 = 'string3';
  this.property4 = 'string4';
  this.property5 = 'string5';
  this.property6 = 'string6';
  this.property7 = 'string7';
  this.property8 = 'string8';
  this.property9 = 'string9';
  this.property10 = 'string10';
  this.property11 = 'string11';
  this.property12 = 'string12';
  this.property13 = 'string13';
  this.property14 = 'string14;';
}

function Object3() {
  // Need `call` to set up inheritance. `super` keyword does this behind the scenes.
  Object2.call(this);

  this.property8 = '';
  this.property9 = '';
  this.property10 = '';
  this.property11 = '';
  this.property13 = '';
}

// Need these to set up inheritance. `extends` keyword does this behind the scenes.
Object3.prototype = Object.create(Object2);
Object3.prototype.constructor = Object3;

// Create the objects
const object = new Object1();
const object2 = new Object2();
const object3 = new Object3();
const object4 = Object.assign({}, object3);
const object5 = new Object1();
const object6 = new Object1();
const object7 = new Object1();
const object8 = new Object1();

// Make the changes you need
object3.property7 = '';
object4.property7 = 'string7';
object7.property8 = 'string28';
object7.property9 = 'string29';
object8.property11 = 'string29';
object8.property13 = 'string30';
```

This code makes it easier to track parents and children. However, it's not needed because the objects do not have methods. Coding it this way could at least make it compatible with the rest of the codebase and potentially lead to further Object-Oriented changes.

# Classes
Instead of worrying about prototypes and constructors, it's possible to make use of inheritance using the `class` keyword to reduce duplicated code.

In comparison to the **constructor function** implementation above, using the `class` keyword makes it easier to write and read. They both do the same thing with this code.

```js
class Object1 {
  constructor() {
    this.property1 = '';
    this.property2 = '';
    this.property3 = '';
    this.property4 = '';
    this.property5 = '';
    this.property6 = '';
    this.property7 = '';
    this.property8 = '';
    this.property9 = '';
    this.property10 = '';
    this.property11 = '';
    this.property12 = '';
    this.property13 = '';
    this.property14 = '';
  }
}

class Object2 {
  constructor() {
    this.property1 = 'string1';
    this.property2 = 'string2';
    this.property3 = 'string3';
    this.property4 = 'string4';
    this.property5 = 'string5';
    this.property6 = 'string6';
    this.property7 = 'string7';
    this.property8 = 'string8';
    this.property9 = 'string9';
    this.property10 = 'string10';
    this.property11 = 'string11';
    this.property12 = 'string12';
    this.property13 = 'string13';
    this.property14 = 'string14';
  }
}

class Object3 extends Object2 {
  constructor() {
    super();
    this.property8 = '';
    this.property9 = '';
    this.property10 = '';
    this.property11 = '';
    this.property13 = '';
  }
}

// Create the objects
const object = new Object1();
const object2 = new Object2();
const object3 = new Object3();
const object4 = new Object3();
const object5 = new Object1();
const object6 = new Object1();
const object7 = new Object1();
const object8 = new Object1();

// Make the changes you need
object3.property7 = '';
object7.property8 = 'string7';
object7.property9 = 'string28';
object8.property11 = 'string29';
object8.property13 = 'string29';
```

I'd say this code is clean and easy to understand, however, like the previous example, it may not be needed since objects are not inheriting object methods.

The above are some different ways I can think of to reduce duplication for this particular code, when taken out of the codebase context. It's likely none of these would work with the codebase for various reasons. I imagine there are also other ways to reduce duplication in situations like this that I'm not yet aware of.