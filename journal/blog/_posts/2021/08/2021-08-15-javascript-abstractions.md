---
layout: post
title: Abstractions in JavaScript
date: 2021-08-15 13:41:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript ls215
---

The focus of this section of the curriculum is imperative and declarative styles and abstractions.

<!--more-->

# Imperative and Declarative Styles
Imperative style focuses on the steps or mechanics needed to solve the problem. Each line of code is one of the steps to solving the problem.

Contrast that with declarative style where the implementation details are less important in comparison to the function of the code. As we abstract more code away from the steps or mechanics needed to solve the problem, our code gets more declarative. This can help us focus on broader functionality rather than specific details.

For example, say we have the following code that capitalizes the first character of each element of a provided array:

```js
let names = ['bandile', 'aylin', 'inkar', 'karabo', 'adele', 'uri', 'ryota'];
let capitalizedElements = [];

for (let index = 0; index < names.length; index += 1) {
  capitalizedElements.push(names[index][0]
    .toUpperCase() + names[index].slice(1));
}

console.log(capitalizedElements);

// OUTPUT:
// [
//   'Bandile', 'Aylin',
//   'Inkar',   'Karabo',
//   'Adele',   'Uri',
//   'Ryota',   'Yuka'
// ]
```

In this code, we focus on how we do the iteration by using a `for` loop and keeping track of the `index`, incrementing it each iteration. We also explicitly convert the first character of each element to uppercase and use `slice` and concatenation to build the new string.

We do have some abstractions in this code, namely `toUpperCase`, `push`, and `slice`. These methods allow us to focus on their function so we don't have to implement and concern ourselves with the details of how the methods work.

We can start making more abstractions by creating a new function that handles the steps needed to capitalize the first character and return the new string:

```js
let names = ['bandile', 'aylin', 'inkar', 'karabo', 'adele', 'uri', 'ryota'];
let capitalizedElements = [];

function capitalizeFirstChar(string) {
  return string[0].toUpperCase() + string.slice(1)
}

for (let index = 0; index < names.length; index += 1) {
  capitalizedElements.push(capitalizeFirstChar(names[index]));
}

console.log(capitalizedElements);
```

Now, in our `for` loop, we tell the JavaScript engine that we want to use the `capitalizeFirstChar` function and push the return value into the `capitalizedElements` array. As far as the `for` loop is concerned, it doesn't know what the `capitalizeFirstChar` function is or how it works. It only knows that it takes one argument and to use the value that's returned.

As we continue to abstract more, we continue to move this code from imperative to declarative style. We can abstract away the details of the loop by using `forEach`:

```js
let names = ['bandile', 'aylin', 'inkar', 'karabo', 'adele', 'uri', 'ryota'];
let capitalizedElements = [];

function capitalizeFirstChar(string) {
  return string[0].toUpperCase() + string.slice(1)
}

names.forEach(name => capitalizedElements.push(capitalizeFirstChar(name)));

console.log(capitalizedElements);
```

The `forEach` method is now handling the details of the loop so we can focus on what we want to do with each element. We can abstract even more by using the `map` method to transform the provided array:

```js
let names = ['bandile', 'aylin', 'inkar', 'karabo', 'adele', 'uri', 'ryota'];

function capitalizeFirstChar(string) {
  return string[0].toUpperCase() + string.slice(1)
}

let capitalizedElements = names.map(capitalizeFirstChar);

console.log(capitalizedElements);
```

We've gotten the code to a point where it's now heavily declarative. `names.map(capitalizeFirstChar)` tells JavaScript to transform each element of the array so that the first character is capitalized. We don't tell it how to loop over the `names` array or how to create the `capiltalizedElements` array. Those details are abstracted away so we can focus on what we want the code to do.

# Abstractions
The main abstraction patterns we can use are: **Iteration**, **Filtering/Selection**, **Transformation**, **Reducing/Folding**, and **Interrogation**. These can help us when solving problems as we start to focus on and pay attention to the shape of code. Another important concept for abstractions is the **callback function** or **callback**. This is the function that's called on each element of the array.

## Iteration
For iteration, we loop over a list of items and perform some operation on each item. We call the callback function on each item of the list. The method that encompasses this abstraction in JavaScript is `forEach`. `forEach` returns `undefined` and the return value of the callback is not used.

### Example
```js
let names = ['bandile', 'aylin', 'inkar', 'karabo', 'adele', 'uri', 'ryota'];
names.forEach(name => console.log(name));

// OUTPUT:
// bandile
// aylin
// inkar
// karabo
// adele
// uri
// ryota
```

Here, we loop through the `names` array and log each name to the console.

## Filtering / Selection
For filtering, we loop over a list and select a subset of elements based on the return value of the callback. In JavaScript, the `filter` method is used for this. `filter` returns a new array with the subset of elements where the callback returns a truthy value. The return value of the callback is important for `filter` and is used to determine which elements are in the selected subset and, therefore, in the new array.

### Example
```js
let names = ['bandile', 'aylin', 'inkar', 'karabo', 'adele', 'uri', 'ryota'];
let filteredNames = names.filter(name => name.length > 5);
console.log(filteredNames);

// OUTPUT:
// [ 'bandile', 'karabo' ]
```

Here, we loop through the `names` array, check to see if the length of each name is greater than `5`, then return an array containing the names where the length check returns a truthy value, in this case when the length check returns `true`;

## Transformation
When we transform an array, we create a new array that contains the values that have been calculated using each of the elements in the given array. `map` is the method that's used in JavaScript. `map` returns a new array that contains one element for each element in the original array. The callback function is called on each element of the given array. The return value of the callback called on an element is the value that replaces that element.

### Example
```js
let names = ['bandile', 'aylin', 'inkar', 'karabo', 'adele', 'uri', 'ryota'];
let transformedNames = names.map(name => name.length);
console.log(transformedNames);

// OUTPUT:
// [
//   7, 5, 5, 6,
//   5, 3, 5
// ]
```

In this example, we transform each name in the array into the length of the name.

## Ordering
With ordering, we rearrange elements by sorting the array. We do this with the `sort` method in JavaScript. `sort` does not return a new value or array like the other methods. Instead, it sorts the given array in place and returns the sorted array. This mutates the calling array.

I think what's interesting here is that the value returned from the callback doesn't have to be `1` and `-1` or `0`. Rather, it can be any positive integer, any negative integer, and zero.

### Example:
```js
let names = ['bandile', 'aylin', 'inkar', 'karabo', 'adele', 'uri', 'ryota'];

function compareLengths(name1, name2) {
  return name1.length < name2.length ? -1 : 1;
}

console.log(names.sort());
// [
//   'adele',   'aylin',
//   'bandile', 'inkar',
//   'karabo',  'ryota',
//   'uri'
// ]

console.log(names);
// [
//   'adele',   'aylin',
//   'bandile', 'inkar',
//   'karabo',  'ryota',
//   'uri'
// ]

console.log(names.sort(compareLengths));
// [
//   'uri',     'adele',
//   'aylin',   'inkar',
//   'ryota',   'karabo',
//   'bandile'
// ]
```

In this example, we first call `sort` with no callback function. This defaults to a sort that's based on character codes in ascending order. So, the names are sorted alphabetically. In the next `console.log` statement, we can see that `sort` does mutate the caller. We can see this when we log the value of `names` to the console and it matches the sorted array. Finally, we call `sort` with a callback function that sorts the names by their lengths in ascending order.

## Reduction / Folding
The idea with reduction is to reduce the elements of an array into one value. In JavaScript, either the `reduce` or `reduceRight` methods can be used. `reduce` is different from the previous methods in that it passes four arguments (`accumulator`, `currentValue`, `currentIndex`, and the array being iterated over) to the callback and takes two arguments (`callback` and `initialValue`). The `accumulator` is passed from one callback invocation to the other.

`reduce` returns a new value that is the return value of the final callback function call.

### Example
```js
let names = ['bandile', 'aylin', 'inkar', 'karabo', 'adele', 'uri', 'ryota'];

function sumLengths(accumulator, currentValue) {
  return accumulator + currentValue.length;
}

let reduction = names.reduce(sumLengths, 0);
console.log(reduction);

// OUTPUT:
// 36
```

Here, we call `reduce` on the `names` array and pass in the callback `sumLengths` and an initial value of `0`. `reduce` keeps track of the value of the accumulator so that when it calls the `sumLengths` function on each element of the array, the accumulator is the value that was returned from the previous callback invocation. Essentially, this does the following:

| Iteration | Accumulator Value | Current Value | Callback Return Value |
|:---------:|:-----------------:|:-------------:|:---------------------:|
|     1     |         0         |       7       |           7           |
|     2     |         7         |       5       |          12           |
|     3     |        12         |       5       |          17           |
|     4     |        17         |       6       |          23           |
|     5     |        23         |       5       |          28           |
|     6     |        28         |       3       |          31           |
|     7     |        31         |       5       |          36           |

We've reduced or folded the array into a single value.

## Interrogation
This is the final abstraction. When we interrogate an array's elements, we check to see if the return value of the callback is truthy for some or every element. JavaScript has the `some` and `every` methods for this. They each work in slightly different ways.

### The `every` Method
`every` returns a boolean `true` or `false` value, depending on whether or not the callback returns a truthy value for every element in the array. As soon as the callback of the `every` method returns a falsey value, the `every` method returns `false`. If the callback method never returns a falsey value, the `every` method returns `true`.

#### Example
```js
let names = ['bandile', 'aylin', 'inkar', 'karabo', 'adele', 'uri', 'ryota'];
console.log(names.every(name => name.length > 0));  // true
console.log(names.every(name => name.length > 5));  // false
```

`every` returns `true` for the first `console.log` statement because every element in the array has a length that's greater than `0`. It returns `false` for the second `console.log` statement because only some of the names have a length that's greater than `5`.

### The `some` Method
`some` returns a boolean `true` or `false` value, depending on whether or not the callback returns a truthy value for at least one element in the array. As soon as the callback of the `some` method returns a truthy value, the `some` method returns `true`. If the callback method never returns a truthy value, the `some` method returns `false`.

#### Example
```js
let names = ['bandile', 'aylin', 'inkar', 'karabo', 'adele', 'uri', 'ryota'];
console.log(names.some(name => name.length > 0));  // true
console.log(names.some(name => name.length > 5));  // true
console.log(names.some(name => name.length > 7));  // false
```

`some` returns `true` for the first two `console.log` statements because in both cases, at least some one return value of the callback function returns a truthy value. The `some` in the third `console.log` statement returns `false` because no element in the array has a length that's greater than `7`.

# Problem Solving
Recognizing imperative versus declarative styles and using abstractions on their own and combined help us solve problems. The better we can recognize the correct abstraction and determine to what extent we need to be imperative versus declarative, the more efficiently we will solve problems presented to us.
