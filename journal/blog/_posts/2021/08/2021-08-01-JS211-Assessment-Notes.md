---
layout: post
title: JS211 Assessment Notes
date: 2021-08-01 08:22:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js210 assessments
---

I completed and passed the JS211 assessment. There was a lot of information in this course, so I ended up with bits of information in multiple places. This made studying more difficult and it ultimately didn't match up with the questions on the assessment. Here are some notes for future reference.

<!--more-->

When I started the test and looked through the first few questions, I began to panic a little because it appeared that I may need to spend more time than I planned on each question. However, as I went through my first reading of the questions, I saw that some of them were lighter and could be answered quickly, leaving more time for the more involved questions.

I think what bothered me was that the code in some of the questions was written in a way that I wasn't expecting. For example, if we're looking at the difference in the way that `for` loops work versus `forEach`, I would expect that the code in that question would be narrowed down to help the student focus on those concepts. Instead, I saw code that mixed a few concepts together while only asking about one aspect:

```js
// Describe what `forEach` is doing in the following code snippet:

let books = ['one book', 'this is a long title', 'book'];
let smallestTitle = books[0];

determineSmallest();

console.log(smallestTitle);       // book

function determineSmallest() {
  books.forEach(title => {
    if (smallestTitle.length > title.length) {
      smallestTitle = title;
    }
  })
}
```

The question is straightforward, yet the code snippet includes a demonstration of function hoisting. Why not move the `console.log` statement and `determineSmallest()` function call to the bottom? Maybe there's something that I don't yet understand about JavaScript, but I feel like this question would be better for someone at my level without the additional function hoisting:

```js
// Describe what `forEach` is doing in the following code snippet:

let books = ['one book', 'this is a long title', 'book'];
let smallestTitle = books[0];

function determineSmallest() {
  books.forEach(title => {
    if (smallestTitle.length > title.length) {
      smallestTitle = title;
    }
  })
}

determineSmallest();
console.log(smallestTitle);       // book
```

I feel like there were at least a few code snippets like this, where concepts were mixed together unnecessarily. I understand that we'll encounter code like this "in the wild," so I get that it's important for us to be able to read it. However, during a test, I'd like to not be distracted by wondering whether or not this code is recommended for some reason, you know, since it's on the test.

# Mock Test
I compiled my own mock test with a bunch of questions that I created and/or gathered from the course notes and exercises to help me write about each of the topics. I can't say that this was helpful, but I [share it here](https://gitlab.com/jd-launch-school/launch-school/-/blob/0c711bc98f84a5d1c7ab5d3207beb705dca7dc03/js210/mock_js211_test.md) in case I need to reference it at some point in the future.

# Study Guide
I feel that for this assessment, the study guide had some topics listed that were too broad. I'm not sure if there's a better way to do this given that many of these concepts blend together. With some of the topics, for example, I found myself having to think about things that didn't relate directly to the topic because they do relate to an important part of the topic. Again, aside from giving the test questions ahead of time, I'm not sure if there's anything that can be done about this.

For future me, just do your best and get through it.

# Future Recommendations
Working through the provided study guide is still helpful. Since there is so much content in these courses, it's important to know which content to focus on. The study guide helps with that.

Other than that, continue going through the courses with the intention of fully understanding each topic being presented. I find that this helps when I get to filling out the study guide.

I feel like I understand JavaScript better now than when I started, a lot better. However, I still feel like I have a lot of gaps, especially as it pertains to the JavaScript code that I see others around me using.
