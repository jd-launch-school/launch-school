---
layout: post
title: More About Problem Solving
date: 2021-08-29 09:41:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript ls215
---

Being able to solve problems by breaking big problems into multiple smaller problems is at the foundation of software engineering. It seems simple, but in practice, it's difficult to see the correct problem and break it into smaller parts.

<!--more-->

Solving small coding problems is an important part of the software engineering interview process. Interviewers like these kinds of problems because they help determine a candidate's level of programming language mastery. They also show how a candidate communicates and uses logic and reasoning. There's some pressure in the situation that tests how comfortable the candidate is with others watching them code and how the candidate handles critiques and feedback.

The process from the candidate's perspective can be daunting and stressful. However, this process also gives candidates a glimpse into how the interviewer communicates and the level of expertise they're expecting. Unlike a take home project, solving problems live with interviewers forces the candidate to be at their best in real time.

To prepare for this, Launch School offers multiple interview assessments. They simulate an interview situation. I look at it like they're interviewing me in order to determine if I'm ready to move on. One difference between a job interview and an interview assessment is that if I fail a job interview, I move on to the next one. If I fail a Launch School interview assessment, on the other hand, I risk being in a situation where I have to spend more time studying and spend more money in the program overall.

Interview assessments are what, I think, makes Launch School stand out when compared to other learning programs.

Here are some things I'd like to focus on as I'm preparing to take my LS216 interview assessment.

# Data Structures
I'd like to get better at focusing on data structures. Specifically, I'm interesting in how they move and transform throughout my program and how they impact the overall solution.

For example, when I have a problem that involves strings and I want to replace certain characters within a string, I want to remember that it might be a good idea to use regular expressions. If part of the problem requires me to do something to certain parts of a string, I may want to coerce the string into an array and use the array abstractions to accomplish my goal.

I used to skip this part of the PEDAC process since the problems I worked on back then were simpler and the goal was to solve the problem, not necessarily solve the problem while also taking into consideration extremes, edge cases, etc... Now, I want to get better at truly integrating this into my problem solving process.

# Examples and Test Cases
Previously, as mentioned above, I was only required to solve the problem using provided test cases. It had been mentioned that we might want to add or edit test cases to cover more situations, but for previous interview assessments, it wasn't tested. It now appears that the process of creating and evaluating test cases will be tested.

In the past couple of problems I've completed, thinking about the potential inputs and how to deal with invalid data has lead to extra test cases that I might have skipped over had I not thought about this more carefully.

For example, while solving a problem that had test cases with various integers, I realized that there were no test cases covering zero or decimals/floats even though the problem itself didn't specify that inputs would only be integers. This problem potentially involved converting numbers into strings and vice-versa, so the presence of extra zeros and floats can lead to unexpected results.

The following coercions might cause problems in this example if I didn't ensure the code avoids or accommodates them:

```js
parseInt('0.1');  // 0 -- lose precision
parseInt('3.14'); // 3 -- lose precision
String(008);      // 8 -- lose leading zeros
```

It's important to recognize that these may be issues when working with strings and numbers. The sooner I recognize this in the problem solving process, the more likely I'll avoid strange bugs involving these kinds of things later on.

Some other things to consider:

- What if the input is `NaN` or `Infinity`?
- What if the input is an empty string, array, or object?
- With arrays, does it matter if they're sparse or have object properties?
- When expecting a string, for example, how should we handle different inputs like numbers, arrays, objects, undefined, null, booleans, or functions?

# Practice, Practice, Practice
I'm now in the phase where I need to practice as often as I can with as many different problems as possible. I need to hone my skills at recognizing the problem, determining appropriate data structures, creating and evaluating test cases, and using the functions and methods that JavaScript makes available. I need to remember `Math.floor`, `Math.ceil`, and `Math.round`. I need to remember how to iterate over objects and get all of the keys and values as needed. I need to be clear about array abstractions so that I can use `forEach`, `map`, `filter`, `reduce`, `sort`, `every` and `some`.

Once I'm comfortable with all of this and I'm able to solve relatively complicated problems consistently within 40 minutes, I should be ready for the assessment.
