---
layout: post
title: SQL JOINs
excerpt_separator: <!--more-->
tags: launch_school sql ls180
---

SQL JOINs are powerful. In the following, I outline what I've learned about them so far.

<!--more-->

# The Starting Table
## Schema
```sql
CREATE TABLE users (
    id         serial                    PRIMARY KEY,
    name       varchar(100)              UNIQUE NOT NULL,
    created_at timestamp with time zone  NOT NULL
                                         DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE agendas (
    id          serial       PRIMARY KEY,
    name        varchar(100) UNIQUE NOT NULL,
    agenda_date date         NOT NULL
                             DEFAULT CURRENT_DATE,
    created_at  timestamp    NOT NULL
                             DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE agendas_users (
    id        serial       PRIMARY KEY,
    user_id   integer      NOT NULL
                           REFERENCES users(id)
                           ON DELETE CASCADE,
    agenda_id integer      NOT NULL
                           REFERENCES agendas(id)
                           ON DELETE CASCADE
);

CREATE TABLE items (
    id                 serial       PRIMARY KEY,
    name               varchar(250) NOT NULL,
    description        text,
    estimated_time_min integer      CHECK(estimated_time_min > 0),
    created_at         timestamp    NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE agendas_items (
    id        serial  PRIMARY KEY,
    agenda_id integer NOT NULL
                      REFERENCES agendas(id)
                      ON DELETE CASCADE,
    item_id   integer NOT NULL
                      REFERENCES items(id)
                      ON DELETE CASCADE
);
```
## Entity Relationship Diagram
![Entity Relationship Diagram for Agendas Database](/assets/img/agenda_database_erd.png)

<!-- ```plantuml -->
<!-- left to right direction -->

<!-- entity users { -->
<!--   **id** INTEGER (N)(D)(P) -->
<!--   -- -->
<!--   name CHARACTER VARYING(100) (U)(N) -->
<!--   -- -->
<!--   created_at TIMESTAMP (N)(D) -->
<!-- } -->

<!-- entity agendas { -->
<!--   **id** INTEGER (N)(D)(P) -->
<!--   -- -->
<!--   name CHARACTER VARYING(100) (U)(N) -->
<!--   -- -->
<!--   agenda_date DATE (N)(D) -->
<!--   -- -->
<!--   created_at CHARACTER VARYING(10) (N) -->
<!-- } -->

<!-- entity items { -->
<!--   **id** INTEGER (N)(D)(P) -->
<!--   -- -->
<!--   name CHARACTER VARYING(250) (N) -->
<!--   -- -->
<!--   description TEXT -->
<!--   -- -->
<!--   estimated_time_min INTEGER(>0) -->
<!--   -- -->
<!--   created_at CHARACTER VARYING(10) (N) -->
<!-- } -->

<!-- entity agendas_users { -->
<!--   **id** INTEGER (N)(D)(P) -->
<!--   -- -->
<!--   user_id INTEGER (N)(F) -->
<!--   -- -->
<!--   agenda_id INTEGER (N)(F) -->
<!-- } -->

<!-- entity agendas_items { -->
<!--   **id** INTEGER (N)(D)(P) -->
<!--   -- -->
<!--   item_id INTEGER (N)(F) -->
<!--   -- -->
<!--   agenda_id INTEGER (N)(F) -->
<!-- } -->

<!-- agendas::id ||---o{ agendas_users::agenda_id -->
<!-- users::id ||---o{ agendas_users::users_id -->
<!-- agendas::id ||---o{ agendas_items::agenda_id -->
<!-- items::id ||---o{ agendas_items::item_id -->
<!-- ``` -->

## Table Contents
```sql
INSERT INTO users (name)
VALUES ('Sio'),
       ('Moor'),
       ('Quin'),
       ('Solienad');

INSERT INTO agendas (name, agenda_date)
VALUES ('Political Group', '2021-01-01'),
       ('Work', '2020-05-01'),
       ('Club', '2019-08-03'),
       ('School Meeting', '2018-11-24'),
       ('Home', '2017-02-15'),
       ('Office', '2016-03-27');

INSERT INTO agendas_users (agenda_id, user_id)
VALUES (1, 1), (2, 1), (1, 3), (3, 4), (4, 3);

INSERT INTO items (name, description, estimated_time_min)
VALUES ('Introductions', 'Introduce everyone.', 5),
       ('Introductions', 'Introduce new members.', 10),
       ('Intros', 'Who''s here?', 5),
       ('New Water Pipe', 'The water pipe broke, we need a new one.', 30),
       ('Buy Computer', 'It''s time for a new computer.', 30),
       ('Remove Treasurer', 'Treasurer messed up, we need to remove them.', 30),
       ('Revise Statement', 'We need to revise our statement.', 20);

INSERT INTO agendas_items (agenda_id, item_id)
VALUES (1, 1), (1, 6),
       (2, 2), (2, 5),
       (3, 3), (3, 4),
       (4, 3), (4, 4);
```
```
SELECT * FROM users;
 id |   name   |          created_at
----+----------+-------------------------------
  1 | Sio      | 2021-01-31 09:08:48.831201-05
  2 | Moor     | 2021-01-31 09:08:48.831201-05
  3 | Quin     | 2021-01-31 09:08:48.831201-05
  4 | Solienad | 2021-01-31 09:08:48.831201-05
(4 rows)

SELECT * FROM agendas;
 id |      name       | agenda_date |         created_at
----+-----------------+-------------+----------------------------
  1 | Political Group | 2021-01-01  | 2021-01-31 13:15:22.989264
  2 | Work            | 2020-05-01  | 2021-01-31 13:15:22.989264
  3 | Club            | 2019-08-03  | 2021-01-31 13:15:22.989264
  4 | School Meeting  | 2018-11-24  | 2021-01-31 13:15:22.989264
  5 | Home            | 2017-02-15  | 2021-01-31 13:15:22.989264
  6 | Office          | 2016-03-27  | 2021-01-31 13:15:22.989264
(6 rows)

SELECT * FROM items;
 id |       name       |                 description                  | estimated_time_min |         created_at
----+------------------+----------------------------------------------+--------------------+----------------------------
  1 | Introductions    | Introduce everyone.                          |                  5 | 2021-01-31 13:16:40.408215
  2 | Introductions    | Introduce new members.                       |                 10 | 2021-01-31 13:16:40.408215
  3 | Intros           | Who's here?                                  |                  5 | 2021-01-31 13:16:40.408215
  4 | New Water Pipe   | The water pipe broke, we need a new one.     |                 30 | 2021-01-31 13:16:40.408215
  5 | Buy Computer     | It's time for a new computer.                |                 30 | 2021-01-31 13:16:40.408215
  6 | Remove Treasurer | Treasurer messed up, we need to remove them. |                 30 | 2021-01-31 13:16:40.408215
  7 | Revise Statement | We need to revise our statement.             |                 20 | 2021-01-31 13:16:40.408215
(7 rows)
```
# INNER JOIN
Now that we have the database setup with some test data, we can start querying the database using JOINs. The INNER JOIN is the most common JOIN and will return data only if it finds a matching row in both tables. Let's get the name of each agenda and the name and description of all items on each agenda:
```sql
SELECT a.name AS agenda_name, i.name AS item_name, i.description AS item_description
  FROM agendas AS a
      INNER JOIN agendas_items AS ai
      ON ai.agenda_id = a.id
      INNER JOIN items AS i
      ON i.id = ai.item_id;
```
```
   agenda_name   |    item_name     |               item_description
-----------------+------------------+----------------------------------------------
 Political Group | Introductions    | Introduce everyone.
 Political Group | Remove Treasurer | Treasurer messed up, we need to remove them.
 Work            | Introductions    | Introduce new members.
 Work            | Buy Computer     | It's time for a new computer.
 Club            | Intros           | Who's here?
 Club            | New Water Pipe   | The water pipe broke, we need a new one.
 School Meeting  | Intros           | Who's here?
 School Meeting  | New Water Pipe   | The water pipe broke, we need a new one.
(8 rows)
```
In this example, both the `agendas_items.agenda_id` and `agendas_items.item_id` need to match the `agenda.id` and `item.id` respectively. Return the agenda name, item name, and description for each row where there is a match for both.

# LEFT OUTER JOIN
This type of join will return every row from the left table and only matching rows from the right table. This will return `NULL` for columns that don't have a value. Let's get all of the agendas and their items, but also the agendas without items:
```sql
SELECT a.name AS agenda_name, i.name AS item_name, i.description AS item_description
  FROM agendas AS a
      LEFT OUTER JOIN agendas_items AS ai
      ON ai.agenda_id = a.id
      LEFT OUTER JOIN items AS i
      ON i.id = ai.item_id;
```
```
   agenda_name   |    item_name     |               item_description
-----------------+------------------+----------------------------------------------
 Political Group | Introductions    | Introduce everyone.
 Political Group | Remove Treasurer | Treasurer messed up, we need to remove them.
 Work            | Introductions    | Introduce new members.
 Work            | Buy Computer     | It's time for a new computer.
 Club            | Intros           | Who's here?
 Club            | New Water Pipe   | The water pipe broke, we need a new one.
 School Meeting  | Intros           | Who's here?
 School Meeting  | New Water Pipe   | The water pipe broke, we need a new one.
 Home            |                  |
 Office          |                  |
(10 rows)
```
Here we can see two addition agendas, `Home` and `Office` that do not have any items.

# RIGHT OUTER JOIN
RIGHT OUTER JOINs will return every row from the right table and only matching rows from the left table. This will return `NULL` for columns that don't have a value. Let's get all of the items and match the items that are on agendas to their agenda:
```sql
SELECT a.name AS agenda_name, i.name AS item_name, i.description AS item_description
  FROM agendas AS a
      RIGHT OUTER JOIN agendas_items AS ai
      ON ai.agenda_id = a.id
      RIGHT OUTER JOIN items AS i
      ON i.id = ai.item_id;
```
```
   agenda_name   |    item_name     |               item_description
-----------------+------------------+----------------------------------------------
 Political Group | Introductions    | Introduce everyone.
 Political Group | Remove Treasurer | Treasurer messed up, we need to remove them.
 Work            | Introductions    | Introduce new members.
 Work            | Buy Computer     | It's time for a new computer.
 Club            | Intros           | Who's here?
 Club            | New Water Pipe   | The water pipe broke, we need a new one.
 School Meeting  | Intros           | Who's here?
 School Meeting  | New Water Pipe   | The water pipe broke, we need a new one.
                 | Revise Statement | We need to revise our statement.
(9 rows)
```
The `Revise Statement` item is not in any agenda.

# FULL OUTER JOIN
This type of JOIN will return every row from both tables, returning `NULL` for columns without values.
```sql
SELECT a.name AS agenda_name, i.name AS item_name, i.description AS item_description
  FROM agendas AS a
      FULL OUTER JOIN agendas_items AS ai
      ON ai.agenda_id = a.id
      FULL OUTER JOIN items AS i
      ON i.id = ai.item_id;
```
```
   agenda_name   |    item_name     |               item_description
-----------------+------------------+----------------------------------------------
 Political Group | Introductions    | Introduce everyone.
 Political Group | Remove Treasurer | Treasurer messed up, we need to remove them.
 Work            | Introductions    | Introduce new members.
 Work            | Buy Computer     | It's time for a new computer.
 Club            | Intros           | Who's here?
 Club            | New Water Pipe   | The water pipe broke, we need a new one.
 School Meeting  | Intros           | Who's here?
 School Meeting  | New Water Pipe   | The water pipe broke, we need a new one.
 Home            |                  |
 Office          |                  |
                 | Revise Statement | We need to revise our statement.
(11 rows)
```
This shows that `Home` and `Office` agendas have not items and `Revise Statement` is an item that's not on any agenda.

# CROSS JOIN
I feel like a CROSS JOIN (the Cartesian Product) is the least common and least usable of the JOINs. It returns every combination of rows from both tables.
```sql
SELECT a.name AS agenda_name, i.name AS item_name, i.description AS item_description
  FROM agendas AS a
      CROSS JOIN items AS i;
```
```
   agenda_name   |    item_name     |               item_description
-----------------+------------------+----------------------------------------------
 Political Group | Introductions    | Introduce everyone.
 Political Group | Introductions    | Introduce new members.
 Political Group | Intros           | Who's here?
 Political Group | New Water Pipe   | The water pipe broke, we need a new one.
 Political Group | Buy Computer     | It's time for a new computer.
 Political Group | Remove Treasurer | Treasurer messed up, we need to remove them.
 Political Group | Revise Statement | We need to revise our statement.
 Work            | Introductions    | Introduce everyone.
 Work            | Introductions    | Introduce new members.
 Work            | Intros           | Who's here?
 Work            | New Water Pipe   | The water pipe broke, we need a new one.
 Work            | Buy Computer     | It's time for a new computer.
 Work            | Remove Treasurer | Treasurer messed up, we need to remove them.
 Work            | Revise Statement | We need to revise our statement.
 Club            | Introductions    | Introduce everyone.
 Club            | Introductions    | Introduce new members.
 Club            | Intros           | Who's here?
 Club            | New Water Pipe   | The water pipe broke, we need a new one.
 Club            | Buy Computer     | It's time for a new computer.
 Club            | Remove Treasurer | Treasurer messed up, we need to remove them.
 Club            | Revise Statement | We need to revise our statement.
 School Meeting  | Introductions    | Introduce everyone.
 School Meeting  | Introductions    | Introduce new members.
 School Meeting  | Intros           | Who's here?
 School Meeting  | New Water Pipe   | The water pipe broke, we need a new one.
 School Meeting  | Buy Computer     | It's time for a new computer.
 School Meeting  | Remove Treasurer | Treasurer messed up, we need to remove them.
 School Meeting  | Revise Statement | We need to revise our statement.
 Home            | Introductions    | Introduce everyone.
 Home            | Introductions    | Introduce new members.
 Home            | Intros           | Who's here?
 Home            | New Water Pipe   | The water pipe broke, we need a new one.
 Home            | Buy Computer     | It's time for a new computer.
 Home            | Remove Treasurer | Treasurer messed up, we need to remove them.
 Home            | Revise Statement | We need to revise our statement.
 Office          | Introductions    | Introduce everyone.
 Office          | Introductions    | Introduce new members.
 Office          | Intros           | Who's here?
 Office          | New Water Pipe   | The water pipe broke, we need a new one.
 Office          | Buy Computer     | It's time for a new computer.
 Office          | Remove Treasurer | Treasurer messed up, we need to remove them.
 Office          | Revise Statement | We need to revise our statement.
(42 rows)
```
It seems like this could be a good way to add every item to each agenda.
