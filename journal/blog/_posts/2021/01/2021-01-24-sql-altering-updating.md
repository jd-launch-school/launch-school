---
layout: post
title: Altering and Updating Tables, Columns, and Rows with SQL
excerpt_separator: <!--more-->
tags: launch_school sql ls180
---

Over the past month, I've been working with the Launch School SQL resources. This is all new to me, so I'm taking my time to make sure that I fully understand all of the concepts involved and the declarative SQL syntax I'm being exposed to. Here, I'm practicing altering and updating tables, columns, and rows.

<!--more-->

# The Starting Table
```sql
CREATE TABLE books_and_things (
  id serial,
  name_of_book varchar(10),
  author varchar(10),
  pages int
);
INSERT INTO books_and_things (name_of_book, author, pages)
VALUES ('TestBook1', 'Author1', 20),
       ('TestBook2', 'Author2', 50),
       ('TestBook3', 'Author3', 100);
```
```
                                      Table "public.books_and_things"
    Column    |         Type          | Collation | Nullable |                   Default
--------------+-----------------------+-----------+----------+----------------------------------------------
 id           | integer               |           | not null | nextval('books_and_things_id_seq'::regclass)
 name_of_book | character varying(10) |           |          |
 author       | character varying(10) |           |          |
 pages        | integer               |           |          |

 id | name_of_book | author  | pages
----+--------------+---------+-------
  1 | TestBook1    | Author1 |    20
  2 | TestBook2    | Author2 |    50
  3 | TestBook3    | Author3 |   100
(3 rows)
```
# Change the name of a table
The `books_and_things` table name isn't accurate. Currently, there are only books in the table. So, we should change this to reflect only books and if we need to add other things to our database, we can add additional tables as needed.
```sql
ALTER TABLE books_and_things RENAME TO books;
```
```
                                            Table "public.books"
    Column    |         Type          | Collation | Nullable |                   Default
--------------+-----------------------+-----------+----------+----------------------------------------------
 id           | integer               |           | not null | nextval('books_and_things_id_seq'::regclass)
 name_of_book | character varying(10) |           |          |
 author       | character varying(10) |           |          |
 pages        | integer               |           |          |
 ```
# Change the name of a column
The `name_of_book` column should probably be changed to `title`:
```sql
ALTER TABLE books RENAME COLUMN name_of_book TO title;
```
```
                                         Table "public.books"
 Column |         Type          | Collation | Nullable |                   Default
--------+-----------------------+-----------+----------+----------------------------------------------
 id     | integer               |           | not null | nextval('books_and_things_id_seq'::regclass)
 title  | character varying(10) |           |          |
 author | character varying(10) |           |          |
 pages  | integer               |           |          |
```
# Alter column type
The `title` and `author` columns allow up to 10 characters. This isn't enough for most titles and names. Let's change this in order to allow more characters:
```sql
ALTER TABLE books
ALTER COLUMN title TYPE varchar(50),
ALTER COLUMN author TYPE varchar(50);
```
```
                                         Table "public.books"
 Column |         Type          | Collation | Nullable |                   Default
--------+-----------------------+-----------+----------+----------------------------------------------
 id     | integer               |           | not null | nextval('books_and_things_id_seq'::regclass)
 title  | character varying(50) |           |          |
 author | character varying(50) |           |          |
 pages  | integer               |           |          |
 ```
# Add new column
Let's split the `author` column into two columns, `author_first_name` and `author_last_name`:
```sql
ALTER TABLE books
  ADD COLUMN author_last_name varchar(50);

 ALTER TABLE books
RENAME COLUMN author to author_first_name;
```
# Change column values
Once we do this, we'll need to change the old `author` column value and convert it into first and last names so the data makes more sense:
```sql
UPDATE books SET author_first_name = 'FirstName' || SUBSTRING(title from '\d');
UPDATE books SET author_last_name = 'LastName' || SUBSTRING(title from '\d');
```
```
                                              Table "public.books"
      Column       |         Type          | Collation | Nullable |                   Default
-------------------+-----------------------+-----------+----------+----------------------------------------------
 id                | integer               |           | not null | nextval('books_and_things_id_seq'::regclass)
 title             | character varying(50) |           |          |
 author_first_name | character varying(50) |           |          |
 pages             | integer               |           |          |
 author_last_name  | character varying(50) |           |          |

 id |   title   | author_first_name | pages | author_last_name
----+-----------+-------------------+-------+------------------
  1 | TestBook1 | FirstName1        |    20 | LastName1
  2 | TestBook2 | FirstName2        |    50 | LastName2
  3 | TestBook3 | FirstName3        |   100 | LastName3
(3 rows)
```
# Add column constraint
It probably doesn't make much sense to have a book without a title and at least an author's first name. Let's make sure there are no `NULL` values allowed for those columns:
```sql
ALTER TABLE books
ALTER COLUMN title SET NOT NULL,
ALTER COLUMN author_first_name SET NOT NULL;

-- Test to make sure the constraint works:
INSERT INTO books (title) VALUES ('TestBook4');
INSERT INTO books (author_first_name) VALUES ('FirstName4');
```
```
                                              Table "public.books"
      Column       |         Type          | Collation | Nullable |                   Default
-------------------+-----------------------+-----------+----------+----------------------------------------------
 id                | integer               |           | not null | nextval('books_and_things_id_seq'::regclass)
 title             | character varying(50) |           | not null |
 author_first_name | character varying(50) |           | not null |
 pages             | integer               |           |          |
 author_last_name  | character varying(50) |           |          |

INSERT INTO books (title) VALUES ('TestBook4');
ERROR:  null value in column "author_first_name" of relation "books" violates not-null constraint
DETAIL:  Failing row contains (5, TestBook4, null, null, null).

INSERT INTO books (author_first_name) VALUES ('FirstName4');
ERROR:  null value in column "title" of relation "books" violates not-null constraint
DETAIL:  Failing row contains (6, null, FirstName4, null, null).
```
# Add check constraint
Is it possible for a book to have zero pages? Not in this database. Let's fix this so books added must not be `NULL` and must have at least 1 page. We'll pretend that it's possible for a book to have one page:
```sql
ALTER TABLE books ADD CHECK (pages >= 1);
ALTER TABLE books ALTER COLUMN pages SET NOT NULL;

-- Test to make sure the constraint works:
INSERT INTO books (title, author_first_name, pages)
VALUES ('TestBook4', 'FirstName4', 0);
```
```
                                              Table "public.books"
      Column       |         Type          | Collation | Nullable |                   Default
-------------------+-----------------------+-----------+----------+----------------------------------------------
 id                | integer               |           | not null | nextval('books_and_things_id_seq'::regclass)
 title             | character varying(50) |           | not null |
 author_first_name | character varying(50) |           | not null |
 pages             | integer               |           | not null |
 author_last_name  | character varying(50) |           |          |
Check constraints:
    "books_pages_check" CHECK (pages >= 1)

INSERT INTO books (title, author_first_name, pages)
VALUES ('TestBook4', 'FirstName4', 0);
ERROR:  new row for relation "books" violates check constraint "books_pages_check"
DETAIL:  Failing row contains (4, TestBook4, FirstName4, 0, null).
```
# Add primary key
The `id` column should really be the primary key for this table:
```sql
ALTER TABLE books ADD PRIMARY KEY (id);
```
```
                                              Table "public.books"
      Column       |         Type          | Collation | Nullable |                   Default
-------------------+-----------------------+-----------+----------+----------------------------------------------
 id                | integer               |           | not null | nextval('books_and_things_id_seq'::regclass)
 title             | character varying(50) |           | not null |
 author_first_name | character varying(50) |           | not null |
 pages             | integer               |           |          |
 author_last_name  | character varying(50) |           |          |
Indexes:
    "books_pkey" PRIMARY KEY, btree (id)
Check constraints:
    "books_pages_check" CHECK (pages >= 1)
```
# Add foreign key
## Create the new table
Let's create another table `publishers`. There will be a one-to-many relationship between the `publishers` and `books`, i.e. a book can only have one publisher and a publisher can publish multiple books:
```sql
CREATE TABLE publishers (
  id serial PRIMARY KEY,
  name varchar(50) NOT NULL,
  address varchar(100),
  email varchar(100)
);
```
```
                                    Table "public.publishers"
 Column  |          Type          | Collation | Nullable |                Default
---------+------------------------+-----------+----------+----------------------------------------
 id      | integer                |           | not null | nextval('publishers_id_seq'::regclass)
 name    | character varying(50)  |           | not null |
 address | character varying(100) |           |          |
 email   | character varying(100) |           |          |
Indexes:
    "publishers_pkey" PRIMARY KEY, btree (id)
```
## Add `publisher_id` column to `books` table
We have to add a `publisher_id` column to the `books` table:
```sql
ALTER TABLE books ADD COLUMN publisher_id int REFERENCES publishers (id);
```
```
                                              Table "public.books"
      Column       |         Type          | Collation | Nullable |                   Default
-------------------+-----------------------+-----------+----------+----------------------------------------------
 id                | integer               |           | not null | nextval('books_and_things_id_seq'::regclass)
 title             | character varying(50) |           | not null |
 author_first_name | character varying(50) |           | not null |
 pages             | integer               |           |          |
 author_last_name  | character varying(50) |           |          |
 publisher_id      | integer               |           |          |
Indexes:
    "books_pkey" PRIMARY KEY, btree (id)
Check constraints:
    "books_pages_check" CHECK (pages >= 1)
Foreign-key constraints:
    "books_publisher_id_fkey" FOREIGN KEY (publisher_id) REFERENCES publishers(id)
```
## Insert rows into `publishers` table
We can now add publishers to the `publishers` table:
```sql
INSERT INTO publishers (name, address, email)
VALUES ('Publisher1', 'Address1', 'email1@example.com'),
       ('Publisher2', 'Address2', 'email2@example.com'),
       ('Publisher3', 'Address3', NULL),
       ('Publisher4', NULL, NULL);
```
```
 id |    name    | address  |       email
----+------------+----------+--------------------
  1 | Publisher1 | Address1 | email1@example.com
  2 | Publisher2 | Address2 | email2@example.com
  3 | Publisher3 | Address3 |
  4 | Publisher4 |          |
(4 rows)
```
## Add publisher IDs to `books` table
Now, we can add publisher IDs to the books in the `books` table:
```sql
UPDATE books SET publisher_id = 1 WHERE author_first_name LIKE '%1%';
UPDATE books SET publisher_id = 1 WHERE author_first_name LIKE '%2%';
UPDATE books SET publisher_id = 2 WHERE author_first_name LIKE '%3%';
```
```
 id |   title   | author_first_name | pages | author_last_name | publisher_id
----+-----------+-------------------+-------+------------------+--------------
  1 | TestBook1 | FirstName1        |    20 | LastName1        |            1
  2 | TestBook2 | FirstName2        |    50 | LastName2        |            1
  3 | TestBook3 | FirstName3        |   100 | LastName3        |            2
(3 rows)
```
## Don't allow `NULL` values for publisher IDs
And finally, we can set the `books.publisher_id` column so that it no longer accepts `NULL` values:
```sql
ALTER TABLE books ALTER COLUMN publisher_id SET NOT NULL;
```
```
                                              Table "public.books"
      Column       |         Type          | Collation | Nullable |                   Default
-------------------+-----------------------+-----------+----------+----------------------------------------------
 id                | integer               |           | not null | nextval('books_and_things_id_seq'::regclass)
 title             | character varying(50) |           | not null |
 author_first_name | character varying(50) |           | not null |
 pages             | integer               |           |          |
 author_last_name  | character varying(50) |           |          |
 publisher_id      | integer               |           | not null |
Indexes:
    "books_pkey" PRIMARY KEY, btree (id)
Check constraints:
    "books_pages_check" CHECK (pages >= 1)
Foreign-key constraints:
    "books_publisher_id_fkey" FOREIGN KEY (publisher_id) REFERENCES publishers(id)
```
# Join the tables
Finally, let's get a table that includes the title of the book, the author's first and last name, and the name of the publisher:
```sql
SELECT b.title, (b.author_first_name || ' ' || b.author_last_name) author, p.name publisher FROM books b
 INNER JOIN publishers p ON p.id = b.publisher_id;
```
```
   title   |        author        | publisher
-----------+----------------------+------------
 TestBook2 | FirstName2 LastName2 | Publisher1
 TestBook1 | FirstName1 LastName1 | Publisher1
 TestBook3 | FirstName3 LastName3 | Publisher2
(3 rows)
```
