---
layout: post
title: The LS216 Interview Assessment
date: 2021-10-08 07:04:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript ls215 assessments
---

I recently initiated the process to take the LS216 interview assessment. I thought it might be interesting to write a post detailing my thoughts before and after the assessment.

<!--more-->

# Before the Interview
Studying for the LS216 interview assessment is reminiscent of studying for the [RB109 Interview Assessment](https://jd-launch-school.gitlab.io/launch-school/posts/passed-interview-assessment/). There are some similarities, like solving lots of problems, live streaming and recording, and working with others. There are some differences though.

Since I haven't completed the interview, I don't know if any of these differences are helpful in terms of the content of the assessment. However, from what I can tell based on the study guide, doing these things should be helpful.

## Focus On The PEDA Process
This was stressed in the course and in the study guide. Part of what the interviewer will be looking for is whether or not we're comfortable breaking problems down into smaller problems and keeping track of this process through the PEDAC process. To practice this, it's been stressed that we should focus on the Problem, Examples, Data Structures, and Algorithm part of the PEDAC process. Once we have a handle on the these parts, the coding part can go quickly since we're following the algorithm we're relatively confident about.

So, when I'm solving problems, it's this part of the process that I've been focusing on most. If I can't get a good understanding of the problem within 30-35 minutes, I usually give up and don't bother to write the code. This way, I'm practicing the PEDA part of the process and not spending too much time hacking and slashing in the code.

## Write Tests for All Edge Cases and Requirements
This is tricky for a few reasons. The first reason is that given a few lines of the problem, it's not always clearly stated how we should handle missing arguments, empty strings, empty arrays, empty objects, null, undefined, negative numbers, or decimals. When I'm solving a problem that is explicit about the type of input, like "starting at 1 up to n" or "takes an integer," then I don't test for strings, arrays, objects, etc... In an interview, I would ask the interviewer which inputs will be possible and which edge cases I need to consider.

I want to make sure I have tests for upper and lower bounds when working with numbers.

I also usually want tests for:

- empty strings
- strings with numbers and symbols and
- strings with upper and lowercase characters.

If working with an array input, I need to have tests for:
- empty arrays and
- different possibilities for array elements given the context of the problem.

When working with objects, I need tests for:
- empty objects
- empty values
- different types of values depending on the context of the problem

These are the basics.

It's also important to test for invalid input. I will need to communicate with the interviewer to see how deep they want me to go. With nested arrays or array values in objects, it's possible to test pretty deep. For example, what if there's a nested array within an array within an array that requires certain values? So, I'm trusting the interviewer to guide me when it comes to testing for invalid input.

## Pure Functions, Side Effects, and Regular Expressions
The study guide states that we should study pure functions, side effects, and regular expressions. It's not yet clear to me how this will be tested during the interview. Will we have to verbally classify a function as a pure function and point out the side effects if it's not a pure function?

So far, while solving practice problems, I haven't found it useful to care about whether a function is a pure function or not, or worry about a function's side effects. The time limit often means I need to quickly understand the problem and create a solution, even if it's not the best code or it doesn't follow all of the best practices / idioms for the language. My brain is using all cylinders during the interview and I'm nervous and stressed, so I like to keep it as simple as possible.

In terms of regular expressions, I have some experience with them so I think I'll do fine. However, studying for this is also unclear. In some problems (all problems?), the use of regular expressions is optional. If I choose to use an algorithm that doesn't need regular expressions, how will my knowledge of regular expressions be tested?

I'm not sure how to study for this part of the assessment or how it will be tested. I think this is the part of Launch School study guides that's the most stressful and confusing.

## 20 Minutes Before the Assessment
I'm all set up. Waiting for my interview time. I've researched what I can research, studied what I can study, and practiced what I can practice. Now it's time for me to show the interview what I'm capable of. I flash back to some of the problem-solving I've seen other students do and I feel somewhat inferior. To me, my problem-solving is erratic and chaotic. At least, that's how it feels when I'm doing it. I've recorded many videos so I know most of the time it seems calculated and rational from the outside.

Will this be the first assessment that I fail? If so, I'll practice some more and try again. I'll try for as long as they let me or until I pass.

I'm not as nervous for this interview as I was for the RB109 problem-solving interview assessment. However, I've heard this one is more difficult. One of the differences between the RB109 interview and this one is my perspective on Launch School and my programming career in general. Back then, I was worried about failing because it would've hinted that I wasn't meant to be in this field. Even though that's not true, it's how I felt at the time. This time, I'm learning for my own edification, my own desire to learn JavaScript and software engineering fundamentals. If I fail, it means I need to practice more and try again. It's not a reflection on whether or not I'm fit for this field because I already know that I'm not fit for the field. I'm doing this for me and that's it.

I'm still not sure what I'll do with the knowledge and skills I'm developing. I might end up doing nothing. It may just be something I know and am proud to have experienced.

## 10 Minutes Before the Assessment
10 minutes remaining until the assessment.

As the time draws near, I feel like I can't wait to see what the problem or problems will be. I've been speculating and imagining all kinds of possibilities for weeks. I'm 10 minutes away from finally finding out. I'm almost certain the problem(s) will involve working with numbers, strings, arrays, objects, booleans, null, undefined, or functions. :) Unfortunately, that doesn't narrow it down too much.

How can I check if an input is an array? Use `Array.isArray()`.
How can I check for a `NaN` input? Use `Number.isNaN()`.

It's starting soon. I received a message from the tester. I'll write some notes when I'm finished with the assessment. Wish me luck!

# After the Interview
## Overview
It's about 45 minutes or so later and I've finished the interview. I was able to solve the problem and pass all of the test cases. It came down to the wire as I was trying to deal with a certain input (or lack thereof) and didn't see the solution at first. I struggled through it and eventually came up with a working solution.

I almost got stuck on the issue I mention above early on. So, I decided that since I couldn't see the solution easily, I'd continue working through the problem and maybe the solution would come to me. At one point I said something like, "I'm just going to make sure the first test case works."

Soon after saying this, I created an algorithm that would work sufficiently to solve the first few test cases. I wrote some code in order to determine if I had complete test cases and to get a sense of where the problems might be. Before running the code, I noticed another issue that I hadn't considered. I quickly wrote another test to verify the issue. At that point, my brain was free to start focusing on this new problem.

Turns out, I didn't have the issue I initially noticed. However, the new issue was similar, as evidenced by the very last test case. I think I had about 5 minutes left at this point, so I was nervous I might not finish. Interestingly, I remember thinking during the assessment that I had come across this issue in a few other coding problems I practiced while preparing for the assessment. Unfortunately, I couldn't remember how I'd previously solved it.

I relaxed myself and started thinking about different ways I might be able to solve it. I tested a few things in the terminal and as soon as I started to write a new algorithm for a helper function, it became clear what I needed to do. A couple of minutes later, I had a working solution.

I finished the entire process in a little over 40 minutes. Technically, I think I finished coding before 40 minutes was up, but it took me a while to add `console.log` statements to my tests.

I'm happy with the result. It's probably not the most efficient way to solve the problem, but I did my best. I successfully calmed myself multiple times when I became overwhelmed, redirecting my energy to other parts of the problem. I let the difficulties sit for a while in diffuse mode while I focused on the parts I knew how to solve.

I think this will be enough to pass and move on, but as with every Launch School assessment, I never know for sure until I receive the grade.

## Analysis of My Preparation Routine
Overall, I think I practiced and studied in the correct way. Focusing on the PEDA part of the PEDAC process, watching others code, coding in front of others, recording and live streaming, and attending the Launch School student-run Algorithm Clinic all seemed to prepare me well for the assessment.

# Results
## I Passed
It looks like I solved the problem and communicated sufficiently enough to pass the assessment. It always feels like a ton of weight is lifted off my shoulders once I pass an assessment. Now I can get back to learning more about JavaScript. I'm really looking forward to learning more about Object Oriented JavaScript!

## 3, 2, 1, Go...
In study sessions, we joked about being on the job and our boss would come to us saying something like, "It's an emergency! We need to duplicate every possible input in order to save the world! We only have 40 minutes left and then the world explodes. Save us!"

I think this type of situation is relatively rare for software engineers. However, being able to solve a problem and create code quickly may be helpful in some real world situations. I can imagine it being helpful in situations where an exploit is discovered, a bug is causing harm, or a client is demanding something immediately.

We joked about this, but in some sense, it's possible that as software becomes more and more ubiquitous in our lives, little problems like these may need quick solutions. I guess Launch School (and many job interviews) are preparing software engineers for the time when we're responsible for fixing something quickly.

## Plans for Next Time
My advice to my future self is to get the PEDA process down to 30 minutes or less. I find that it takes a while to create examples and reason through the problem to make sure I'm adding every edge case to my examples. So, if I can get that process to under 30 minutes, I'll have a lot more time to fix my mistakes before and during coding.

I also advise practicing ALL of the problems introduced throughout the lesson until I can consistently create all of the test cases and come up with a working algorithm within 30 minutes for each problem or as many problems as possible. If I find myself having trouble with a particular problem, move on! Do other problems. The chance that the problem I'm having trouble with is going to be like the one on the assessment is low.

Other than that, try to have fun.
