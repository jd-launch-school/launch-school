---
layout: post
title: JavaScript Factory Functions
date: 2021-10-24 14:18:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js225 objects this destructuring
---

Understanding the fundamentals of JavaScript objects is important to writing object-oriented code in JavaScript. With this understanding, we can create Factory Functions to help us create objects more efficiently.

<!--more-->

# A Better Way to Create Objects
The typical way of returning an object from a function might look like this:

```js
function createBook(title, author, publicationDate) {
  return {
    title: title,
    author: author,
    publicationDate: publicationDate,
  };
}
```

This gives us the ability to create multiple `book` objects with different properties:

```js
let book1 = createBook("My Book Title", "This Book's Author", "2019");
console.log(book1);  // { title: "My Book Title", author: "This Book's Author", publicationDate: "2019" }

let book2 = createBook("Another Book Title", "Another Book's Author", "2016");
console.log(book2);  // { title: "Another Book Title", author: "Another Book's Author", publicationDate: "2016" }

let book3 = createBook("Third Book's Title", "Third Book's Author", "2001");
console.log(book3);  // { title: "Third Book's Title", author: "Third Book's Author", publicationDate: "2001" }
```

Since our property names are the same as our argument names, we can write this in a more succinct way:

```js
function createBook(title, author, publicationDate) {
  return {
    title,
    author,
    publicationDate,
  };
}
```

```js
let book1 = createBook("My Book Title", "This Book's Author", "2019");
console.log(book1);  // { title: "My Book Title", author: "This Book's Author", publicationDate: "2019" }

let book2 = createBook("Another Book Title", "Another Book's Author", "2016");
console.log(book2);  // { title: "Another Book Title", author: "Another Book's Author", publicationDate: "2016" }

let book3 = createBook("Third Book's Title", "Third Book's Author", "2001");
console.log(book3);  // { title: "Third Book's Title", author: "Third Book's Author", publicationDate: "2001" }
```

# A Better Way to Write Object Methods
We may normally write our object methods like this:

```js
function createBook(title, author, publicationDate) {
  return {
    title,
    author,
    publicationDate,

    checkout: function() {
      console.log(`The book is checked out.`)
    },

    goToPage: function(page) {
      console.log(`You are now reading page ${page}.`);
    },
  };
}
```

We can call the methods like this:

```js
book1.checkout();     // The book is checked out.
book2.checkout();     // The book is checked out.
book3.checkout();     // The book is checked out.

book1.goToPage(33);   // You are now reading page 33.
book2.goToPage(45);   // You are now reading page 45.
book3.goToPage(119);  // You are now reading page 119.
```

If we want to save some characters, we can write the methods like this:

```js
function createBook(title, author, publicationDate) {
  return {
    title,
    author,
    publicationDate,

    checkout() {
      console.log(`The book is checked out.`)
    },

    goToPage(page) {
      console.log(`You are now reading page ${page}.`);
    },
  };
}
```

```js
book1.checkout();     // The book is checked out.
book2.checkout();     // The book is checked out.
book3.checkout();     // The book is checked out.

book1.goToPage(33);   // You are now reading page 33.
book2.goToPage(45);   // You are now reading page 45.
book3.goToPage(119);  // You are now reading page 119.
```

It doesn't save that much space, but it does make the code a little easier to read.

# Object Destructuring or Multiple Assignment
When we want to get values from an object, we can do this:

```js
let title = book1.title;
let author = book1.author;
let publicationDate = book1.publicationDate;

console.log(title);             // My Book Title
console.log(author);            // This Book's Author
console.log(publicationDate);   // 2019
```

With destructuring, we can make this a little cleaner with less code:

```js
let { title, author, publicationDate } = book1;

console.log(title);             // My Book Title
console.log(author);            // This Book's Author
console.log(publicationDate);   // 2019
```

We can rename the variables as needed:

```js
let { title: book1Title, author, publicationDate } = book1;

console.log(book1Title);        // My Book Title
console.log(author);            // This Book's Author
console.log(publicationDate);   // 2019
```

Finally, we can set variables for only the object properties we want:

```js
let { title, publicationDate } = book1;

console.log(title);             // My Book Title
console.log(publicationDate);   // 2019
```

# Destructuring Function Parameters
We can also use destructuring for setting the variables within a function:

```js
function readBookInfo({ title, author, publicationDate }) {
  console.log(title);           // My book Title
  console.log(author);          // This Book's Author
  console.log(publicationDate); // 2019
}

readBookInfo(book1);
```

# this
The magic to making object-oriented programming work in JavaScript is the `this` keyword. It refers to the object itself and gives us access to the object's properties and methods. Instead of logging `The book is checked out`, we can log the title of the book. We can also check the book out before we go to a page:

```js
function createBook(title, author, publicationDate) {
  return {
    title,
    author,
    publicationDate,

    checkout() {
      console.log(`${this.title} is checked out.`)
    },

    goToPage(page) {
      this.checkout();
      console.log(`You are now reading "${this.title}" on page ${page}.`);
    },
  };
}

book1.checkout();     // My Book Title is checked out.
book2.checkout();     // Another Book Title is checked out.
book3.checkout();     // Third Book's Title is checked out.

book1.goToPage(33);
// My Book Title is checked out.
// You are now reading "My Book Title" on page 33.

book2.goToPage(45);
// Another Book Title is checked out.
// You are now reading "Another Book Title" on page 45.

book3.goToPage(119);
// Third Book's Title is checked out.
// You are now reading "Third Book's Title" on page 119.
```

# We Now Have a Factory Function
The code above is known as a **factory function**. It's a function that returns an object based on the given arguments. Without a factory function, we would need to write a lot more code:

```js
let book1 = {
  title: "My Book Title",
  author: "This Book's Author",
  publicationDate: "2019",

  checkout() {
    console.log(`${this.title} is checked out.`)
  },

  goToPage(page) {
    this.checkout();
    console.log(`You are now reading "${this.title}" on page ${page}.`);
  },
}

let book2 = {
  title: "Another Book Title",
  author: "Another Book's Author",
  publicationDate: "2016",

  checkout() {
    console.log(`${this.title} is checked out.`)
  },

  goToPage(page) {
    this.checkout();
    console.log(`You are now reading "${this.title}" on page ${page}.`);
  },
}

let book3 = {
  title: "Third Book's Title",
  author: "Third Book's Author",
  publicationDate: "2016",

  checkout() {
    console.log(`${this.title} is checked out.`)
  },

  goToPage(page) {
    this.checkout();
    console.log(`You are now reading "${this.title}" on page ${page}.`);
  },
}

console.log(book1.title);  // My Book Title
console.log(book2.title);  // Another Book Title
console.log(book3.title);  // Third Book's Title
```

When we want to add another book, we'll have to create another `book` object, duplicating the methods in the process. If we want to add a new method, we have to add that new method to each of the objects. It's apparent that this will be too much duplication and hassle for more than a handful of objects.

The factory function and `this` help us solve these problems by placing our properties and methods into an object that's returned from a function. This makes it much easier for us to create and modify `book` objects as needed:

```js
function createBook(title, author, publicationDate) {
  return {
    title,
    author,
    publicationDate,

    checkout() {
      console.log(`${this.title} is checked out.`)
    },

    goToPage(page) {
      this.checkout();
      console.log(`You are now reading "${this.title}" on page ${page}.`);
    },
  };
}

let book1 = createBook("My Book Title", "This Book's Author", "2019");
let book2 = createBook("Another Book Title", "Another Book's Author", "2016");
let book3 = createBook("Third Book's Title", "Third Book's Author", "2001");
```

Now, we only have to add or change methods and properties within the object returned from the function and those changes will apply to as many `book` objects as we want or need to create.
