---
layout: post
title: Notes on JavaScript Execution Context
date: 2021-11-14 06:28:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js225 objects this functions
---

Another important concept to understand when working with JavaScript objects and specifically, `this`, is function execution context.

<!--more-->

Here are some notes about `this`, implicit and explicit function execution context, and the global object.

# The Global Object
The global object is created when JavaScript starts running.

In a browser, the global object is `window`.
In Node, the global object is `global`.
When using strict mode, the global object is `undefined`.

We can examine the global object by logging its value:

```js
console.log(window);

// or in Node:

console.log(global);
```

This global object is the **implicit function execution context**. Values such as `Infinity` and `NaN`, variables declared with `var` and global functions are properties of the global object. For example:

In the browser:

```js
num = 1;
console.log(window.num);   // 1
console.log(window);

// Window {
//   ...
//   num: 1
//   ...
// }
```

`num = 1` is the same as explicitly adding a property to the `window` object:

```js
window.num = 1
console.log(window.num);   // 1
console.log(window);

// Window {
//   ...
//   num: 1
//   ...
// }
```

`num = 1` will not work in strict mode because the global object is `undefined` and it's not possible to set properties on `undefined`:

```js
"use strict";

num = 1;       // Uncaught ReferenceError: `num` is not defined.
```

## In The Browser
Declaring variables with `var`, undeclared variables, and defining global functions sets them as properties on the global object. Declaring variables with `let` or `const` does not set them as properties on the global object:

```js
firstPerson = 'Moshi';
var secondPerson = 'Jing';
let thirdPerson = 'Qwia';
const fourthPerson = 'Pmau';

function giveGift(person) {
  console.log(`Gift given to ${person}`);
}

console.log(window.firstPerson);    // Moshi
console.log(window.secondPerson);   // Jing
console.log(window.thirdPerson);    // undefined --> Function scope, not property of 'window'
console.log(window.fourthPerson);   // undefined --> Function scope, not property of 'window'

console.log(window.giveGift);
// function giveGift() { console.log(...) }
```

Variables declared with `var` and functions cannot be deleted from the global object, whereas undeclared variables can be deleted from the global object:

```js
firstPerson = 'Moshi';
var secondPerson = 'Jing';

function giveGift(person) {
  console.log(`Gift given to ${person}`);
}

console.log(window.firstPerson);    // Moshi
console.log(window.secondPerson);   // Jing
console.log(window.giveGift);
// function giveGift() { console.log(...) }

delete window.firstPerson;
delete window.secondPerson;
delete window.giveGift;

console.log(window.firstPerson);
// undefined  <-- window.firstPerson was deleted

console.log(window.secondPerson);   // Jing

console.log(window.giveGift);
// function giveGift() { console.log(...) }
```

## In Node
Node wraps variables and functions in this special function:

```js
(function (exports, require, module, __filename, __dirname) {
   // ... more code here
});
```

Therefore, functions and variables declared with `var`, `let`, or `const` are not properties of the global object. They have function scope. Only undeclared variables become properties of the global object:

```js
firstPerson = 'Moshi';
var secondPerson = 'Jing';
let thirdPerson = 'Qwia';
const fourthPerson = 'Pmau';

function giveGift(person) {
  console.log(`Gift given to ${person}`);
}

console.log(global.firstPerson);    // Moshi
console.log(global.secondPerson);   // undefined --> Function scope, not a property of  'global'
console.log(global.thirdPerson);    // undefined --> Function scope, not a property of  'global'
console.log(global.fourthPerson);   // undefined --> Function scope, not a property of  'global'
console.log(global.giveGift);       // undefined --> Function scope, not a property of  'global'
```

# Execution Context
A function is declared with the keyword `function`, a function expression assigned to a variable, or an arrow function. A method is a property of an object that references a function.

All functions and methods that are invoked have access to an object that makes up its execution context. It could be the global object or another object. Execution context depends on how the function or method is **invoked**.

This small detail is important to keep in mind because it's different from the lexically determined scope. Scope is determined when writing the code and based on where the function or method **definition** (not the function or method invocation) exists in the code.

`this` resolves to a function or method's execution context.

## Implicit Function Execution Context
**Implicit binding for functions** is the function's context when no explicit context is provided at the time the function is invoked. Functions invoked without a provided explicit context are bound to the global object. We can log a function's execution context by logging the value of `this`:

```js
function buildHouse() {
  return `House built. The value of 'this' is: ${this}.`
}

// function invocation without explicit context

console.log(buildHouse());
// House built. The value of 'this' is: [object Window].
```

In strict mode, `this` is undefined:

```js
"use strict";

function buildHouse() {
  return `House built. The value of 'this' is: ${this}.`
}

// function invocation without explicit context
// in strict mode

console.log(buildHouse());
// House built. The value of 'this' is: undefined.
```

## Implicit Method Execution Context
The implicit execution context when a method is invoked without an explicit execution context is the calling object:

```js
let house = {
  address: '1234 Main St.',

  changeAddress(newAddress) {
    this.address = newAddress;
    return this;
  },
};

console.log(house.changeAddress('492 Tower Ave'));
// {address: '492 Tower Ave.', changeAddress: function }

let move = house.changeAddress;

console.log(move('492 Tower Ave'));
// Window { ... }

console.log(move('492 Tower Ave') === house);  // false
console.log(move('492 Tower Ave') === window); // true
```

This example demonstrates that when we call the `house.changeAddress()` method without an explicit execution context, the implicit execution context is the `house` object.

However, when we assign the function definition of `house.changeAddress` to the variable `move` and then call the `move` function without an explicit execution context, the implicit execution context is now set to the global object.

The same function definition called in two different ways sets the function's execution context to two different contexts.

We can see this by comparing the value of `this` to both the `house` object and the `window` object. The return value of the `move` function, i.e. the value of `this` for the `move` function invocation, is not the same as the `house` object. However, the return value of the `move` function is the same as the `window` global object.

So, even though we defined the `changeAddress` method within the scope of the `house` object, we take it out of the object and call the resulting function without an explicit execution context. Therefore, the `move` function's implicit execution context is the global object.

### In Node
The implicit execution context for a script is an empty object `{}`. We can see this by logging the value of `this` from within a script in Node (*not the Node REPL*):

```js
console.log(this); // {}
```

If we wanted to add a property to the script's implicit execution context, we would have to define the property explicitly:

```js
console.log(this); // {}

this.myVariable = 'My Variable';

this.myFunction = function myFunction () {
  console.log('My Function');
};

console.log(this);
// { myVariable: 'My Variable', myFunction: [Function: myFunction] }
```

## Explicit Function Execution Context
The `call` and `apply` methods can be used to assign an object to a function's explicit execution context. These bind a function's execution context to the object.

The basic example is binding a function that's not associated with an object to another object so the function executes within the context of that other object:

```js
let book = {
  title: 'The Title',
  author: 'Jili Min',
  turnPage() {
    console.log(`${this.title}: Turned a page.`);
  },
};
let book2 = {
  title: 'The Title Two',
  author: 'Bin Mal',
};

function describe() {
  console.log(`The book "${this.title}" was written by ${this.author}.`);
}
```

If we can call the `describe()` function on its own, the value of `this` resolves to the implicit function execution context of the global object. Since there are no `title` and `author` properties, `undefined` is returned when we attempt to access those properties from within the `global` object:

```js
describe();
// The book undefined was written by undefined.
```

In order to invoke the `describe` function with an explicit function execution context, we can use the `call` method. In each example below, the explicit execution context of the function is the object that's passed into the `call` method invocation:

```js
describe.call(book);
// The book "The Title" was written by Jili Min.

describe.call(book2);
// The book "The Title Two" was written by Bin Mal.
```

In the example below, we do not provide an explicit execution context to the `turnPage()` method call, so the implicit execution context is the object the method is called on, `book`:

```js
book.turnPage();
// The Title: Turned the page.
```

We can also invoke the `turnPage()` method with an explicit method execution context. This will make the `turnPage` method execution context the object `book2`:

```js
book.turnPage.call(book2);
// The Title Two: Turned the page.
```

For execution context, everything depends on how the function or method is called.

