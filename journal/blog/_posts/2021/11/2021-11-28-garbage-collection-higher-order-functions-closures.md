---
layout: post
title: Garbage Collection, Higher-Order Functions, and Closures
date: 2021-11-28 16:40:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js225 garbage_collection higher_order_functions first_class_functions closures
---

Garbage Collection, or GC, in JavaScript is important to understand so we can avoid memory problems with our programs. It's also important to understand closures and the difference between First-Class Functions and Higher-Order Functions.

<!--more-->

# Closures
The word **closures** can be intimidating. I know it was for me at one time. I think this is because the word itself doesn't provide context as to where or how closures are used in the language. In JavaScript, closures are created at the time of function definition.

Closures "close over" or "enclose" the surrounding context lexically, i.e. based on where they are defined in the code. Context includes any variables, functions, or classes that are defined and referred to by the function. These functions always have access to that context, even if they're invoked much later on in the program. This is also one way to create private data in JavaScript.

Here's a basic example:

```js
function library() {
  let books = ['My Title', 'Your Title'];

  return function () {
    console.log("\nBooks in Library");
    console.log('----------------');
    console.log(books.join("\n") + "\n");
  };
}

let myLibrary = library();
myLibrary();
// Output:
//
// Books in Library
// ----------------
// My Title
// Your Title
//
```

This demonstrates that the `books` variable is in the closure for the anonymous function returned from the `library` function. Since the `books` variable is referenced from within the anonymous function and it's declared in the outer function scope, it is available in the closure for the anonymous function.

We cannot access the `books` variable from outside of the `library` function because once the `library` function is finished executing, `books` will no longer be in scope. If we try to access the `books` variable from outside of the `library` function, a `ReferenceError` exception will be thrown:

```js
console.log(books);        // ReferenceError: books is not defined
console.log(library.books) // undefined
```

This is working as expected since we most likely do not want the `books` variable to be accessible from outside of the function.

If we want to have reasonable methods for use in an outer scope, then we need to return an object instead of a function:

```js
function library() {
  let books = [];

  return {
    add(title) {
      books.push(title);
      console.log(`"${title}" added.`);
    },

    report() {
      console.log("\nBooks in Library");
      console.log('----------------');
      console.log(books.join("\n") + "\n");
    },
  };
}

let myLibrary = library();
myLibrary.add('My Title');    // "My Title" added.
myLibrary.add('Your Title');  // "Your Title" added.

myLibrary.report();
// Output:
//
// Books in Library
// ----------------
// My Title
// Your Title
//
```

Doing this creates another problem. How can we add a `remove` function in order to remove a book from `books`?

If `books` was a property of our `myLibrary` object, we would be able to create a new method like this:

```js
myLibrary.remove = function remove(title) {
  this.books.splice(this.books.indexOf(title), 1);
  console.log(`"${title}" removed.`);
};

myLibrary.remove('My Title');  // "My Title" removed.
```

Since `books` is **not** a property of `myLibrary` and is only accessible from within the `library` function, we **cannot** access the `books` variable from outside of `myLibrary`:

```js
myLibrary.remove = function remove(title) {
  this.books.splice(this.books.indexOf(title), 1);
  console.log(`"${title}" removed.`);
};

myLibrary.remove('My Title');  // ReferenceError: books is not defined
```

The only way to add a method to the object returned from the `library` function is to modify the returned object definition:

```js
function library() {
  let books = [];

  return {
    add(title) {
      books.push(title);
      console.log(`"${title}" added.`);
    },

    report() {
      console.log("\nBooks in Library");
      console.log('----------------');
      console.log(books.join("\n") + "\n");
    },

    remove(title) {
      books.splice(books.indexOf(title), 1);  // ReferenceError: books is not defined
      console.log(`"${title}" removed.`);
    },
  };
}

let myLibrary = library();

myLibrary.add('My Title');    // "My Title" added.
myLibrary.add('Your Title');  // "Your Title" added.
myLibrary.remove('My Title'); // "My Title" removed.

myLibrary.report();
// Output:
// Books in Library
// ----------------
// Your Title
//
```

# Higher-Order Functions VS. First-Class Functions
The difference between higher order functions and first class functions is subtle, but important.

**Higher-order functions** are functions that can accept a function as an argument, return a function when invoked, or both. A function is a higher order function if either the input or output value is a function. Essentially, higher order functions are functions that work with other functions.

**First-class functions** are functions that can be returned and passed to other functions, assigned to variables, and generally used like any other value.

First class functions make it possible in JavaScript to have higher-order functions, since higher order functions in JavaScript need functions that can be passed in as arguments and/or returned as values. However, higher-order functions can and do exist in languages where functions are not first-class functions. There are different ways to enable higher-order functions. First-class functions is one of them.

# Garbage Collection
There are four general steps to garbage collection:

1. **Allocate** Memory for Data from the System
2. **Test** for Correct Allocation
3. **Use** Data in Memory as Needed
4. **Release** Memory Back to System

Some languages require the programmer to create and keep track of garbage collection to efficiently manage memory for their program. In this case, one has to calculate the amount of memory needed for the variable and then assign a value to it. It's not possible to do both in one step.

Other languages, like JavaScript, have automatic garbage collection where JavaScript handles those four steps so the programmer doesn't have to think them. JavaScript automatically allocates memory when it creates new objects and primitives and then automatically releases that memory when no code in the program references those objects or primitive values. If objects and primitives are accessible and referenced in the code, even if they're part of a function's closure, those objects and primitives cannot be garbage collected.

Since this process is automatic, it might seem like the programmer does not need to think about memory management. That's not always the case.

Consider the following code:

```js
function describe() {
  let title = 'Book Title';

  console.log(title);
  return title;
}

let description = describe();
```

In the above code, JavaScript automatically allocates memory for two variables, `describe` and `description`. Each variable references a separate `Book Title` string.

Since strings are immutable primitives, when we assign the return value of the `describe` function (`Book Title`) to the variable `description`, this is a different `Book Title` string from the `Book Title` string assigned to the `title` variable. Each variable points to a different immutable string primitive with a value of `Book Title`.

When JavaScript's garbage collection process looks for values that can be garbage collected, it sees that the `Book Title` value that the `title` variable references is only used or referred to during the execution of the `describe` function. So, when the `describe` function is finished executing, that value is eligible for garbage collection.

The other string value is actively being referenced by the `description` variable. Therefore, this `Book Title` string value is not eligible for garbage collection.

One last thing to note is that not every value in JavaScript is eligible for garbage collection.


