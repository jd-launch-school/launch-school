---
layout: post
title: JavaScript Objects
date: 2021-06-20 12:20:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js210 objects
---

Here are some interesting things about objects in JavaScript.

<!--more-->

# What are Objects?
A JavaScript object stores key/value pairs, also known as properties. An object's keys are strings while the values associated with these keys can be any data type. It's possible to create, read, update, and delete object properties. For example:

## Create an Object
```
// Object literal notation

let myObject = {
  title: 'Title',
  author_id: 2,
  publishers: [ 'publisher1', 'publisher2' ],
  add(first, second) { return first + second },
};
```

## Add a Property
```js
myObject.pages = 300;
console.log(myObject);

// output:
// {
//   title: 'Title',
//   author_id: 2,
//   publishers: [ 'publisher1', 'publisher2' ],
//   add: [Function: add],
//   pages: 300
// }
```

## Retrieve the Value of a Property
```js
// bracket notation
console.log(myObject['author_id']);  // 2

// dot notation
console.log(myObject.author_id);     // 2
```

## Use a Property Method
```js
console.log(myObject.add(1, 2));     // 3
```

## Update a Property
```js
myObject.publishers.push('publisher3');
console.log(myObject);

// output:
// {
//   title: 'Title',
//   author_id: 2,
//   publishers: [ 'publisher1', 'publisher2', 'publisher3' ],
//   add: [Function: add],
//   pages: 300
// }
```

## Delete a Property
```js
delete myObject.pages;
console.log(myObject);

// output:
// {
//   title: 'Title',
//   author_id: 2,
//   publishers: [ 'publisher1', 'publisher2', 'publisher3' ]
//   add: [Function: add]
// }
```

# Primitives vs. Objects
A string primitive is not the same as a String object. Same for number primitives and Number objects. Fortunately, JavaScript temporarily coerces primitives into their object counterpart. Here's an example:

```js
let primitive = 'this is a string primitive';
console.log(typeof primitive);                       // string
console.log(primitive.toUpperCase());                // THIS IS A STRING PRIMITIVE

let object = new String('this is a String object');
console.log(typeof object);                          // object
console.log(object.toUpperCase());                   // THIS IS A STRING OBJECT
```

In the example above, we can see that the string primitive `this is a string primitive` is a string and not an object. However, we're still able to call String object method `toUpperCase()` on the string primitive. This demonstrates that the string primitive is temporarily coerced into a String object. This works for Number and Boolean primitives as well.

# Object Prototype
Objects in JavaScript can inherit from other objects. The object that an object inherits from is known as the object's **prototype**. We can see this inheritance with the following example using `Object.create`:

```js
let myObject = { id: 1, title: 'Title' };
let otherObject = Object.create(myObject);
otherObject.description = "Description";

console.log(otherObject.id);                 // 1
console.log(otherObject.description);        // Description
console.log(myObject.description);           // undefined
```

In the above example, we create two objects, `myObject` and `otherObject`. `otherObject` inherits from `myObject` and uses `myObject` as its prototype, so it has access to all of `myObject`'s properties. `myObject`, on the other hand, does not have access to the `description` property that was added to `otherObject`. `myObject` is the parent and `otherObject` is the child.

# Retrieving Object Values
We can retrieve all of an object's values by using the `Object.values` method:

```js
console.log(Object.values(myObject));     // [ 1, 'Title' ]
```

The order of the returned values may not be consistent. This method does not retrieve inherited values:

```js
console.log(Object.values(otherObject));  // [ 'Description' ]
```

Or, we can use a `for/in` loop to iterate over each property and log each value:

```js
for (let key in otherObject) {
  console.log(otherObject[key]);
}

// output:
//
// Description
// 1
// Title
```

This example shows that using a `for/in` loop iterates through both the object's key/value pairs and the object prototype's key/value pairs.

If we don't want this behavior and only want the object's properties, we can use the `hasOwnProperty` method:

```js
for (let key in otherObject) {
  if (otherObject.hasOwnProperty(key)) console.log(otherObject[key]);
}

// output:
//
// Description
```

# Retrieving an Object's Keys
We can use `Object.keys` to return an array containing an object's keys:

```js
console.log(Object.keys(myObject));     // [ 'id', 'title' ]
console.log(Object.keys(otherObject));  // [ 'description' ]
```

We can see that only the keys for the object's properties are returned, not the keys of the object's prototype.

# Getting All Properties
We can get an array of nested arrays representing the key/value pairs of an object using `Object.entries`:

```js
console.log(Object.entries(myObject));

// output:
// [ [ 'id', 1 ], [ 'title', 'Title' ] ]
```

# Combining Two or More Objects
We can combine two or more objects into a single object by using `Object.assign`. This permanently mutates the first object, so if we want to avoid this, we can set the first object to be an empty object:

```js
let thirdObject = Object.assign({}, myObject, otherObject);
console.log(thirdObject);  // { id: 1, title: 'Title', description: 'Description' }
console.log(myObject);     // { id: 1, title: 'Title' }
console.log(otherObject);  // { description: 'Description' }
```
