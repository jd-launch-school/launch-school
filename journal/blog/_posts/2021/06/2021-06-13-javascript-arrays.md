---
layout: post
title: JavaScript Arrays
date: 2021-06-13 09:30:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js210 arrays
---

Here are some interesting things about arrays in JavaScript.

<!--more-->

# Arrays are Objects
In JavaScript, arrays are objects.

```js
typeof [1, 2, 3];  // 'object'
```

Technically, `[1, 2, 3]` is an array literal. When we use the `typeof` operator on the array literal, JavaScript coerces it into an Array object. Another way to create an array is by using the Array object constructor:

```js
let testAarray = new Array(1, 2, 3);
```

If arrays are objects, how do we determine if something is an array? For example, the following will not work:

```js
typeof '';               // 'string'
typeof [];               // 'object'
typeof '' === 'string';  // true
typeof '' !== 'array';   // true
typeof [] === 'array';   // false
```

For determining whether an object is an array, we can use the `Array.isArray` function:

```js
Array.isArray('string');   // false
Array.isArray([1, 2, 3]);  // true
```

# Array Properties and Elements
Like Ruby, it's possible to retrieve elements from an array by index using bracket notation with a non-negative integer. Unlike Ruby, each index is actually a key in the Array object:

```js
let array = [1, 2, 3];
array[0];               // 1
Object.keys(array);     // [ '0', '1', '2' ]
```

It's possible to add and retrieve negative integer or non-integer keys as well:

```js
array[-1] = 4;          // -1 is implicitly coerced into the string `-1`
array['string'] = 5;
array;                  // [1, 2, 3, '-1': 4, string: 5]
array[-1];              // 4 (-1 is implicitly coerced into the string `-1`)
array['-1'];            // 4
array['string'];        // 5
```

If we try to retrieve the value of an index or key that doesn't exist within the array, `undefined` will be returned. Unlike Ruby, no error message will be raised:

```js
array[5];                     // undefined
array['test'];                // undefined
array[5] === undefined;       // true
array['test'] === undefined;  // true
```

The `length` property is also present, but not shown. We can see the `length` property by calling the `Object.getOwnPropertyDescriptors` function, passing in the `array` as an argument:

```js
Object.getOwnPropertyDescriptors(array);
```

Output:
```sh
{
  '0': { value: 1, writable: true, enumerable: true, configurable: true },
  '1': { value: 2, writable: true, enumerable: true, configurable: true },
  '2': { value: 3, writable: true, enumerable: true, configurable: true },
  length: { value: 3, writable: true, enumerable: false, configurable: false },
  '-1': { value: 4, writable: true, enumerable: true, configurable: true },
  string: { value: 5, writable: true, enumerable: true, configurable: true }
}
```

In the above, you can see the `enumerable` flag of the `length` property is set to `false`, so that is why the `length` property isn't visible when iterating over array elements or keys.

# Sparse Arrays
It's possible to change the value of the `length` property of an array without adding or removing elements. When we change the value to less than the current value, the extra elements are removed from the array:

```js
array.length = 3;
array;             // [1, 2, 3]
```

When we change the `length` value to more than the current value, `<empty items>` are added to the array:

```js
array.length = 10;
array;               // [ 1, 2, 3, <7 empty items> ]
array.length;        // 10
```

These empty items are included in the length (`10`), but are not actually elements of the array:

```js
Object.keys(array);                       // [ '0', '1', '2' ]
Object.getOwnPropertyDescriptors(array);
```

Output:
```sh
{
  '0': { value: 1, writable: true, enumerable: true, configurable: true },
  '1': { value: 2, writable: true, enumerable: true, configurable: true },
  '2': { value: 3, writable: true, enumerable: true, configurable: true },
  length: { value: 10, writable: true, enumerable: false, configurable: false }
}
```

When we access one of these empty items, we will see that it is `undefined`;

```js
array[5];                 // undefined
array[5] === undefined;   // true
```

Interestingly, this makes it more difficult to determine the size of arrays or if they're empty:

```js
array.length;                    // 10
Object.keys(array).length        // 3
```

What is the size of `array`? Is it `10` or `3`? That depends on whether or not we want to include empty items and all array properties (other than `length`).

```js
let emptyArray = [];
emptyArray['string'] = 1;
emptyArray.length;               // 0
Object.keys(emptyArray).length   // 1

let emptyArray2 = [];
emptyArray2.length = 3;
emptyArray2;                     // [ <3 empty items> ]
emptyArray2.length;              // 3
Object.keys(emptyArray2).length  // 0
```

If we only care about an array's element and its empty items, then we can use the `length` property to determine if an array is empty. `emptyArray` would be empty while `emptyArray2` is not empty.

However, if we want to include other array properties (not including `length`) and we don't care abut empty items, then we can use `Object.keys(array).length` to determine if an array is empty. `emptyArray` would not be empty while `emptyArray2` is empty.

# Comparing Arrays
It's not straightforward to compare arrays in JavaScript. In order for the strict equality operator (`===`) to return `true`, each operand must reference the same array object in memory. It's not enough for each array object to have the same values:

```js
[] === []                                  // false
[1, 2, 3] === [1, 2, 3];                   // false
new Array() === new Array()                // false
new Array(1, 2, 3) === new Array(1, 2, 3)  // false
```

This is because each operand in the examples above reference a different array object in memory. Here is an example comparing two variables that both reference the same array object in memory:

```js
let array = [1, 2, 3];
let anotherArray = array;
array === anotherArray;    // true
```

In order to compare two arrays to see if they contain the same values, we need to compare each value at each index:

```js
function arraysEqual(arr1, arr2) {
  if (arr1.length !== arr2.length) return false;

  for (let i = 0; i < arr1.length; i++) {
    if (arr1[i] !== arr2[i]) return false;
  }

  return true;
}
```

These are only some examples of the way arrays work in JavaScript. Learning arrays deeply like this will help to troubleshoot errors and bugs one might encounter in one's code that may seem strange otherwise. When my code returns `undefined` when I expect a value or the length of an array and/or the number of keys in the array doesn't match up to what I'm expecting, knowing these things about arrays should help me figure out the problem.
