---
layout: post
title: Studying for JavaScript Test (JS211)
date: 2021-07-11 13:00:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js210 assessments
---

For all of the Launch School assessments, I go through the study guide and compile everything I can find about each topic into one place. For the RB109 assessment, I had to work on finding and using the correct precise language. The same is true as I study for the JS211 assessment.

<!--more-->

Fortunately, I wrote about [my experience with the RB109 assessment](https://jd-launch-school.gitlab.io/launch-school/posts/passed-written-assessment/). Also, fortunately, I compiled a [mock assessment](https://gitlab.com/jd-launch-school/launch-school/blob/e60eeb1b1b386315860dc4c4bc3bf4ed3e7f86b9/mock_assessments.org) with various problems I'd been exposed to during the study sessions. I'm thinking that both of these resources will help me as I study for the JS211 assessment.

I'm going to improve the mock assessment by adding in some questions about specific concepts and tailoring it more for JavaScript. Below, I share some example questions.

# Example JavaScript Question 1
Examine the code below. What structure does `{...}` define? Explain with reference to this structure why line 9, `console.log(a)`, outputs `5` but line 10, `console.log(b)`, throws a `ReferenceError` exception. What underlying principle about the structure does this demonstrate?

```js
let a = 10

{
  a = 5;
  let b = 15;
}

console.log(a);  // 5
console.log(b);  // Uncaught ReferenceError: b is not defined
```

## Solution
`{...}` defines a block with local block scope.

The `console.log` statement on line 9 logs `5` to the console because variables declared in an outer scope are accessible and can be reassigned from within inner block and function scopes. Since the variable `b` was declared within the inner scope of the `{...}` block, it is not accessible outside of the block in the outer global scope. Therefore, when we try to log the value of the variable `b` in the outer global scope on line 10, JavaScript throws a `ReferenceError` because the variable `b` has not been defined in the outer scope.

The underlying principal at work here is variable scoping in JavaScript where variables declared in outer scopes are accessible to be mutated and/or reassigned from within inner scopes but not vice-versa.

# Example JavaScript Question 2
Explain why the following code outputs `Xyzzy` with precise language. What is the underlying concept?

```js
let a = 'Xyzzy';

function myValue(b) {
  b[2] = '-';
}

myValue(a);
console.log(a);
```

## Solution
This outputs `Xyzzy` because the `console.log` statement on line 8 logs the value of the variable `a` which is declared on line 1. The value of `a` does not change within the `myValue` function because string values in JavaScript are primitive values that cannot be mutated. Even though the return value of the statement `b[2] = '-'` will be `-`, suggesting a mutation has been made, string values cannot be mutated so no mutation actually takes place.

The underlying concept is that primitive values are immutable and characters within an immutable string cannot be changed. In order to change a character within a string, we need to create a new string with the changed character.

# Example JavaScript Question 3
Explain why the line `console.log(sentence)` outputs `hello world` rather than `HELLO WORLD`. What does this demonstrate about what happens on line 2?

```js
function shout(string) {
  return string.toUpperCase();
}

let sentence = 'hello world';
shout(sentence);
console.log(sentence);         // hello world
```

## Solution
Line 7 logs `hello world` to the console instead of `HELLO WORLD` because the variable `sentence` declared on line 5 with the string literal `hello world` assigned to it is not mutated within the `shout` function. The only `sentence` variable JavaScript knows about accessible to the outer global scope is the variable declared on line 5.

When we call the `shout` method and pass in the variable `sentence` as an argument, the parameter `string` is declared within the `shout` function scope as a local variable and assigned to the value of the `sentence` argument, `hello world`. Then, on line 2, we call the non-mutating `toUpperCase()` method on the `string` local variable. This returns the string `HELLO WORLD` while leaving the initial value of `string` intact. `HELLO WORLD` is then returned from the `shout` function.

Since we do not store the return value of the `shout` function, the return value is garbage collected after JavaScript finishes executing line 9. Finally, on line 10, we log the value of the variable `sentence` to the console. JavaScript finds the `sentence` variable declared and initialized on line 5 and logs its value to the console, printing `hello world` to the screen.

I'll be working on creating more questions like these over the next couple of weeks so that I can attempt to flesh out and be precise about everything I've learned in JS210.
