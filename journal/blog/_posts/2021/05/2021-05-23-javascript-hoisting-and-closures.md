---
layout: post
title: JavaScript Hoisting and Closures
date: 2021-05-23 10:00:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js210 hoisting closures
---

Hoisting and closures are important in JavaScript. I outline what they are and provide some examples.

<!--more-->

# Hoisting

Hoisting doesn't really exist, technically. It's a mental model one can use to think about the creation and execution phases of JavaScript.

When JavaScript runs, it first goes through a **creation phase** where it finds all of the functions and variables that have been declared. It loads these into memory for use in the next phase, the **execution phase**. During the execution phase, JavaScript executes the code, using the functions and variables it found in the creation phase.

## Function Hoisting
It can seem like JavaScript is moving function and variable declarations to the top of the current scope. For example, the following code demonstrating function hoisting works even though the function is declared after the `console.log` statement:

```js
console.log(getNumber());  // 2

function getNumber() {
  return 2;
}
```

JavaScript first goes through the code during the creation phase and detects the `getNumber` function. Then, in the execution phase, JavaScript executes the `console.log` statement, passing in the return value of the `getNumber` function as an argument. JavaScript uses the `getNumber` function definition it found in the creation phase and returns `2`.

After hoisting, this is the equivalent code:

```js
function getNumber() {
  return 2;
}

console.log(getNumber());  // 2
```

In fact, the two code snippets above are equivalent and have the same output.

If we were to write equivalent code in Ruby, where we invoke a function before it's defined, would raise `NameError` exception:

```ruby
p get_number     # undefined local variable or method `get_number` for main:Object (NameError)

def get_number()
  2
end
```

## Variable Hoisting
Here's an example of variable hoisting:

```js
console.log(Var);          // undefined
// console.log(Let);       // ReferenceError: Cannot access 'Let' before initialization
// console.log(CONST);     // ReferenceError: Cannot access 'CONST' before initialization
// console.log(globalVar); // ReferenceError: globalVar is not defined

var Var = 1;
let Let = 2;
const CONST = 3;
globalVar = 4;

console.log(Var);        // 1
console.log(Let);        // 2
console.log(CONST);      // 3
console.log(globalVar);  // 4
```

After hoisting, this code is equivalent to:

```js
var Var;
let Let;
// const CONST;            // this doesn't work since const requires an initializer, but
                           // this is essentially what happens

console.log(Var);          // undefined
// console.log(Let);       // ReferenceError: Cannot access 'Let' before initialization
// console.log(CONST);     // ReferenceError: Cannot access 'CONST' before initialization
// console.log(globalVar); // ReferenceError: globalVar is not defined

Var = 1;
Let = 2;
// CONST = 3;            // this doesn't work since it's not possible to reassign a constant, but
                         // this is esentially what happens
globalVar = 4;

console.log(Var);        // 1
console.log(Let);        // 2
console.log(CONST);      // 3
console.log(globalVar);  // 4
```

### Hoisting with var
When variables declared with `var` are hoisted, it's as if the variable declaration is moved to the top, but the initialization is not. Instead, the variable is declared an initialized to the default, `undefined`. This is why the `console.log(Var)` statement above returns `undefined`.

### Hoisting with let and const
When variables declared with `let` or `const` are hoisted, it's similar to hoisting with `var`, in that it's as if the variable declaration is moved to the top, but the initialization is not. However, it's different in that these variables are not initialized to any default. They exist in what's known as the Temporal Dead Zone (TDZ). JavaScript knows about them, but they have not yet been declared and initialized. This is why, in the example above, logging the values of `Let` and `CONST` to the console raises a `ReferenceError`.

### Undeclared variables
In the case of `glovalVar` in the above example, this is not hoisted. I think this is because during the creation phase, JavaScript doesn't see a variable declaration for `globalVar`. Instead, it sees this as assignment and moves on. So, when we try to log the value of `globalVar` to the console before it's been assigned, JavaScript raises a `ReferenceError` exception. Note that this exception is different from the exception for `let` and `const`. `glovalVar` doesn't exist, even in the TDZ.

# Closures
A **closure** is a function combined with any variables that the function needs form its lexical scope, that is, the scope at the time of the function's definition. A closure is created when a function or method is defined. The closure essentially closes over its environment. Both the function definition and the scope become a closure. Variables that are declared prior to and used within the function definition become part of the function's closure.

## Example Closure
Here's an example:

```js
function getName() {
  let name = "Sinji";

  return function () {
    console.log(name);
  };
}

let printName = getName();
printName();                 // Sinji
```

In this example, we assign the return value of the `getName` function to the `printName` variable on line 9. The return value of the `getName` function is a function definition which logs the value of the `name` variable to the console. The `printName` variable points to the function definition itself, not an invocation of the function. So, when we invoke `printName()` as a function, the function defined as the return value for the `getName` function is invoked.

When this return function is defined, it creates a closure that includes the `name` variable declared and initialized on line 2. So, when the return function is invoked, JavaScript first looks for a `name` variable within the function scope. It doesn't find one. Then, JavaScript looks in the outer scope, which is the closure, and finds the `name` variable with value `Sinji`. This is what gets logged to the console.

Even though the variable `name` is not currently in scope and does not exist in the outer global scope, the closure makes it accessible to the return function assigned to the `printName` variable.
