---
layout: post
title: JavaScript Functions
date: 2021-05-18 06:00:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js210 functions
---

This is a quick note to keep in mind some interesting things about functions in JavaScript.

<!--more-->

Functions are first-class functions in JavaScript, that is, they are objects that can be treated like any other value. It's possible to assign a function to a variable, pass it as an argument to another function, and return it as the return value of a function.

# Defining Functions

There are three ways to define a function.

**Function Declaration**
```js
function myFunction(name, num) {
  console.log(name + num);
}
```

**Function Expression**
```js
let myFunction = function (name, num) {
  console.log(name + num);
};
```

Another version of the function expression is the named function expression:

```js
let myFunction = function concatNameAndNumber(name, num) {
  console.log(name + num);
};
```

The [Airbnb Style Guide](https://github.com/airbnb/javascript#functions--declarations) suggests that it's best to use named function expressions instead of function declarations or anonymous function expressions.

**Arrow Function**
```js
let myFunction = (name, num) => console.log(name + num);
```

# Function Names as Variables
When we declare a function with a function declaration, JavaScript also initializes a variable with the same name. That variable can be reassigned like any other variable:

```js
function myFunction(name, num) {
  console.log(name + num);
}

myFunction('myName', 1);  // myName1
myFunction = 'test';
console.log(myFunction);  // test
```

# Function Composition
When we pass a function as an argument to another function, this is known as **function composition**. Here's an example:

```js
let myFunction = function concatNameAndNumber(name, num) {
  console.log(name + num);
};

console.log(myFunction('myName', 1));
```

Every time we use `console.log` to log the return value of a function, we're using function composition. This can get much more complicated when there are multiple arguments, each function returning a value for the argument of the outer function.

# Function Calls and Method Invocation
From what I understand at this point, there's a difference between declared functions and methods. I don't yet know all of the differences, however, I suspect it has something to do with where the function is defined. For now, the main difference is in the way each is invoked.

Functions defined in the above ways are invoked using function calls like `console.log('test')` or `myFunction('myName', 1)`. Method invocations, on the other hand, are called on an object like `'string'.toUpperCase()`.

In comparison to Ruby, it appears that method invocations are similar to instance methods that are defined within a class.

# Function Scope
Functions have access to variables defined in their own local scope and the surrounding scope. They can be nested within one another, each function creating a new local function scope.

```js
let myFunction = function concatNameAndNumber(name, num) {
  let anotherFunction = function addThreeNumbers(first, second) {
    let third = '3';
    return first + second + third;
  };

  console.log(name + anotherFunction(num, 2));
  // console.log(third);   // Uncaught ReferenceError: third is not defined
};

myFunction('myName', 1)
// anotherFunction(1, 2);  // Uncaught ReferenceError: anotherFunction is not defined
```

In this case, the variable `third` is locally scoped to the `addThreeNumbers` function and is not accessible from the body of the `concatNameAndNumber` function. Similarly, the variable `anotherFunction` is declared within the scope of the `concatNameAndNumber` function and is not available in the outer scope surrounding `myFunction`.
