---
layout: post
title: Messy Methods and Exclusive OR
excerpt_separator: <!--more-->
tags: launch_school ruby lesson_3 practice_problems medium_1
---

The following are some highlights from this section.

<!--more-->

# Lesson 3's Medium 2 practice problems
The first two were about tracking how ruby passes objects around to blocks and methods. References get passed around and if the reference points to an object with mutating methods, the object is changed as opposed to creating a copy of that object. If the reference points to an object that doesn't have mutating methods, then it will not be possible to mutate the original object.

We explored this more by passing strings and arrays to a method and watching the difference. I like in the recommended solution for the problem that asks us to refactor this problem so it makes the results easier to predict.

The problem is that the current method either mutates an argument or not, depending on what's in the method. It's not easy to see from the outside (without looking at the code in the method) how the method will behave. So, to fix this, the recommended solution avoids mutation and returns new values instead.

Here's an example of a method that's not clear:

```ruby
def messy_method(string, array)
  string += "something"
  array << "something"
end

my_string = 'test'
my_array = ['one thing']

messy_method(my_string, my_array)

p my_string
p my_array
```

```
: "test"
: ["one thing", "something"]
```

So, the array object will be mutated while the string object will not. To make this more clear, do something like this:

```ruby
def better_method(string, array)
  string += "something"
  array += ["something"]

  return string, array
end

my_string = 'test'
my_array = ['one thing']

my_string, my_array = better_method(my_string, my_array)

p my_string
p my_array
```

```
: "testsomething"
: ["one thing", "something"]
```

This way, it's clear that the method returns new objects that can then be assigned to variables as needed.

# Small Problems > Easy 3 > Exclusive OR
This one seems easy on the surface but is actually more complex if one wants to get it right.

We're supposed to make an `XOR` (exclusive or) method that's similar to `||`, but returns false when both given arguments evaluate to `true`. Here's the truth table:

| item one | item two | result |
|----------+----------+--------|
| false    | true     | true   |
| true     | false    | true   |
| true     | true     | false  |
| false    | false    | false  |

The solution I came up with was the following:

```ruby
def xor?(first, second)
  (!first && second) || (first && !second)
end

p xor?("hi", false)
p xor?(false, "hi")
p xor?(5, 6)
```

```
: true
: "hi"
: false
```

This seems relatively straightforward. Even though it does technically pass the provided test cases, it does so in a way that's not consistent. Sometimes, it'll return a boolean, other times it'll return the value of the reference object. Since everything except `nil` and `false` are truthy in Ruby, an object's value is likely to be truthy.

To fix this, it's possible to use `!!` to convert the value to its boolean value. It's also possible to do this in a way similar to the recommended solution:

```ruby
def xor?(value1, value2)
  return true if value1 && !value2
  return true if value2 && !value1
  false
end

p xor?("hi", false)
p xor?(false, "hi")
p xor?(5, 6)
```

```
: true
: true
: false
```

Or:

```ruby
def xor?(first, second)
  (!first && !!second) || (first && !second)
end

p xor?("hi", false)
p xor?(false, "hi")
p xor?(5, 6)
```

```
: true
: true
: false
```

There were a bunch of other solutions in the submitted solution section. Some of them were comprehensive, others not. Most needed to convert to boolean values and then they'd work comprehensively.

I think this exercise highlights the potential pitfalls if one is not paying close attention to the return value. Truthy values may cause a certain and desired result if one is expecting a truthy value. However, one may not realize that the value is not a boolean and is instead an object that's truthy.
