---
layout: post
title: Palindromic Strings (Part 1), Palindromic Numbers, Short Long Short
excerpt_separator: <!--more-->
tags: launch_school ruby practice_problems easy_3
---

The following are some highlights from this section.

<!--more-->

# Small Problems > Easy 3 > Palindromic Strings (Part 2)
## Description of the Exercise
Write a method that returns `true` if the input is a palindrome, but it can ignore all non-alphanumeric characters.

## My Solution
I solved this with my previous `palindrome?` method and `String#gsub`, like so:

```ruby
def palindrome?(input)
  return false if input.empty?

  input.reverse == input
end
```

```ruby
def broader_palindrome?(input)
  input = input.is_a?(Array) ? input.join : input

  palindrome?(input.downcase.gsub(/['"?!., ]/, ""))
end
```

## Other Observations
My solution works, but in looking at the recommended and submitted solutions, I see better solutions.

One easy way to simplify my solution is to substitute characters that are not alphanumeric:

```ruby
palindrome?(input.downcase.gsub(/\W+/, '')
```

The recommended solution uses `String#delete` which has a regex-like syntax:

```ruby
string = string.downcase.delete('^a-z0-9')
```

# Small Problems > Easy 3 > Palindromic Numbers
## Description of Exercise
This is similar to the other palindrome exercises, but takes an Integer as input instead of a String.

## My Solution
I converted the input into a String, reversed it, converted it back into an Integer, and then compared that to the original input. If they were the same, then the given Integer is a palindrome:

```ruby
def palindrome?(input)
  input.to_s.reverse.to_i == input
end
```

## Other Observations
The recommended solution did the same thing, but it converted the input to a String and then used it as an argument for the `palindrome?` method we made previously.

The further exploration asked us what would happen if the number has leading zeros and whether or not the method will continue to work. When an Integer in Ruby is preceded by a zero, Ruby converts it to an octal number. For instance:

```ruby
p 0121
p 0122
p 01111
```

```
: 81
: 82
: 585
```

Interestingly, it's possible to get the numbers by using `Integer#to_s` like this:

```ruby
p 0121.to_s(8)
p 0122.to_s(8)
p 01111.to_s(8)
```

```
: "121"
: "122"
: "1111"
```

When I tried to implement this into the `palindromic_number?` method, I couldn't figure out a way to detect if the input has leading zeros before it was converted to its decimal equivalent. By the time the method gets access to the input, it's already been converted. It is possible to create a method that determines whether or not numbers with leading zeros are palindromes:

```ruby
def palindrome?(input)
  input = input.to_s(8)
  input.reverse == input
end

p palindrome?(0121)
p palindrome?(023)
p palindrome?(0404)
p palindrome?(00121)
p palindrome?(00233)
```

```
: true
: false
: true
: true
: false
```

# Small Problems > Easy 4 > Short Long Short
## Description of Exercise
We need to write a method that takes two strings, one longer than the other, and returns a concatenated string that looks like: short + long + short.

## My Solution
I used a ternary operator and the `String.size` method:

```ruby
def short_long_short(first, second)
  first.size < second.size ? first + second + first : second + first + second
end
```

## Other Observations
The recommended solution uses an `if/else` statement and suggests that to be the most readable out of the possible options. I think the ternary is similarly readable. Here's the `if/then` for comparison:

```ruby
def short_long_short(string1, string2)
  if string1.length > string2.length
    string2 + string1 + string2
  else
    string1 + string2 + string1
  end
end
```

The `if/then` becomes more readable, to me, if `string1` and `string2` are changed to `first` and `second`:

```ruby
def short_long_short(first, second)
  if first.length > second.length
    second + first + second
  else
    first + second + first
  end
end
```
