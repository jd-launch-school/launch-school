---
layout: post
title: Small Problems > Easy 5 > Letter Counter (Part 1)
excerpt_separator: <!--more-->
tags: ruby launch_school small_problems easy_5
---

This post includes:  letter counter, alphabetical numbers

<!--more-->

# Small Problems > Easy 5 > Letter Counter (Part 1)
## Description of Exercise
When given a string with one or more space separated words, return a hash with the number of words of each length. Symbols and punctuation are included in the string length.

## My Solution
This took me about 24 minutes.

I was almost able to do this in one line. First, I created an empty hash. Then, I split the string into an array of words and iterated over these words. The keys of the hash are the lengths of the words. If the hash has a length as a key, increment its value by one. If the hash doesn't have a length as a key, add the length as a key with a value of one. Return the hash.

## Other Observations
The recommended solution pointed out the use of `Hash::new` and a default value. This replaces the need to check for a key in the hash. Whenever a value is incremented, it'll add one. If the key already exists, it increments the existing value. If the key doesn't exist, the default value is zero and this gets incremented to one.

Looking at submitted solutions, I liked this one-liner that uses `Enumerable#each_with_object`:

```ruby
def word_sizes(string)
  string.split.each_with_object(Hash.new(0)) { |word, hash| hash[word.length] += 1 }
end
```

# Small Problems > Easy 5 > Letter Counter (Part 2)
## Description of Exercise
For part 2, we use the method from the previous exercise but don't include punctuation and/or symbols in the calculation of the length.

## My Solution
This took me 6 minutes.

I added `gsub([^a-z]/i, '')` to the previous method before calculating the length:

```ruby
def word_sizes(string)
  string.split.each_with_object(Hash.new(0)) { |word, hash| hash[word.gsub(/[^a-z]/i, '').length] += 1 }
end
```

## Other Observations
Oops, I didn't catch the pattern for `String#delete`!

Even though this was used a few exercises ago, I still didn't catch it. This simplifies my solution to this:

```ruby
def word_sizes(string)
  string.split.each_with_object(Hash.new(0)) { |word, hash| hash[word.delete('^A-Za-z').length] += 1 }
end
```

Interestingly, this can also be done with `String#count`:

```ruby
def word_sizes(string)
  string.split.each_with_object(Hash.new(0)) { |word, hash| hash[word.count('A-Za-z')] += 1 }
end
```

Hopefully, I'll remember this pattern in the future!

# Small Problems > Easy 5 > Alphabetical Numbers
## Description of Exercise
Given an array of integers from 0-19, return an array with the integers sorted by their English names.

## My Solution
This took me about 24 minutes to complete.

I did find a one-line solution for this. First, I created a hash `NUMBERS_TO_ENGLISH` where each number is matched up with its English name. I then use `Array#map` to iterate through the given array and return a new array. Each number is matched to its English name. The new array contains English names for the integers instead of the integers themselves.

I then use `Array#sort` to sort the English names alphabetically. Then, I iterate through the array and match each English name back to its number using `Hash#invert`:

```ruby
NUMBER_TO_ENGLISH = {
  0 => 'zero', 1 => 'one', 2 => 'two', 3 => 'three', 4 => 'four',
  5 => 'five', 6 => 'six', 7 => 'seven', 8 => 'eight', 9 => 'nine',
  10 => 'ten', 11 => 'eleven', 12 => 'twelve', 13 => 'thirteen',
  14 => 'fourteen', 15 => 'fifteen', 16 => 'sixteen',
  17 => 'seventeen', 18 => 'eighteen', 19 => 'nineteen'
}

def alphabetic_number_sort(array)
  array.map { |num| NUMBER_TO_ENGLISH[num] }.sort.map { |word| NUMBER_TO_ENGLISH.invert[word] }
end
```

## Other Observations
The recommended solution used an array instead of a hash. The indexes of the array enable one to match integers to their English names. It also uses `Enumerable#sort_by` for a nice, one-line solution:

```ruby
NUMBER_WORDS = %w(zero one two three four
                  five six seven eight nine
                  ten eleven twelve thirteen fourteen
                  fifteen sixteen seventeen eighteen nineteen)

def alphabetic_number_sort(numbers)
  numbers.sort_by { |number| NUMBER_WORDS[number] }
end
```

`Enumerable#sort_by` returns a new array with the elements of the given array sorted by the results of the given block.

The further exploration asked us to use `Array#sort`. One solution, seen in the submitted solutions does this:

```ruby
NUMBER_TO_ENGLISH = %w(zero one two three four five six seven eight nine
                       ten eleven twelve thirteen fourteen
                       fifteen sixteen seventeen eighteen nineteen)

def alphabetic_number_sort(array)
  array.sort { |a, b| NUMBER_TO_ENGLISH[a] <=> NUMBER_TO_ENGLISH[b] }
end
```

When a block is given to `Array#sort`, it /"must implement a comparison between `a` and `b` and return an integer less than 0 when `b` follows `a`, `0` when `a` and `b` are equivalent, or an integer greater than 0 when `a` follows `b`."/ 

Comparison (`<=>`) returns `-1` if the left is less than the right, `0` if they're equal, and `1` if the left is greater than the right.
