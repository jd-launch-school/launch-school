---
layout: post
title: Easy 4 and Easy 5 Small Problems Exercises
excerpt_separator: <!--more-->
tags: ruby launch_school small_problems easy_4 easy_5
---

This post includes:  easy 4 and easy 5 small problems exercises, multiples of 3 and 5, running totals, convert a string to a number!, convert a string to a signed number!, convert a number to a string!, convert a signed number to a string!, ascii string value, after midnight

<!--more-->

# Easy 4 and Easy 5 Small Problems Exercises
Over the past few days, I've been working through the Easy 4 and Easy 5 Small Problems exercises. They've been challenging, especially in terms of figuring out a solution that closely matches the recommended solution.

My solution works, but it's not as concise nor does it use the methods the recommended solutions use. I'm worried that I'm missing something in terms of how to think about these problems.

I worked on the following problems over this time period. I'm going to show my solution in comparison to the recommended solution. Maybe I'll notice some patterns by seeing them side-by-side. Then, I'll add any submitted solutions or refactorings that I think are helpful.

## Small Problems > Easy 4 > Multiples of 3 and 5
### My Solution
```ruby
def multisum(max_result)
  result_array = []

  [3, 5].each do |num|
    counter = 1
    result = 0

    loop do
      result = counter * num
      break if result > max_result

      result_array.push(result)

      counter += 1
      break if counter * num > max_result
    end
  end

  result_array.uniq.reduce(:+)
end
```

### Recommended Solution
```ruby
def multiple?(number, divisor)
  number % divisor == 0
end

def multisum(max_value)
  sum = 0

  1.upto(max_value) do |number|
    if multiple?(number, 3) || multiple?(number, 5)
      sum += number
    end
  end

  sum
end
```

### Analysis
I have nested loops wheres the recommended solution is able to do this with only one loop. I used `Array#uniq` and `Array#reduce` whereas the recommended solution used a more brute-force method and looked at all of the values between `1` and the `max_value` separately. I also have two `break` statements, which seems awkward.

## Small Problems > Easy 4 > Running Totals
### My Solution
```ruby
def running_total(array)
  running_total = []
  result = 0

  array.each do |num|
    result = num + result
    running_total.push(result)
  end

  running_total
end
```

### Recommended Solution
```ruby
def running_total(array)
  sum = 0
  array.map { |value| sum += value }
end
```

### Analysis
I sensed that this could be done with `Enumerable#reduce`, but it didn't seem clear to me when writing my solution. However, after looking at the recommended solution, it became more clear and I wrote this:

```ruby
def running_total(array)
  sum = 0
  array.reduce([]) { |result, num| result << sum += num }
end
```

I think the difficulty was using `Enumerable#reduce` along with a variable set in the outer scope `sum`.

Also, my variable name `result` isn't as clear as the recommended solution's variable `sum`.

## Small Problems > Easy 4 > Convert a String to a Number!
### My Solution
```ruby
NUMBERS = {
  '0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4,
  '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9
}

def string_to_integer(string)
  array = string.chars.map { |char| NUMBERS[char] }
  length = array.length - 1

  array.reduce(0) do |sum, num|
    multiplier = eval('1' << '0' * length)
    length -= 1
    sum += num * multiplier
  end
end
```

### Recommended Solution
```ruby
DIGITS = {
  '0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4,
  '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9
}

def string_to_integer(string)
  digits = string.chars.map { |char| DIGITS[char] }

  value = 0
  digits.each { |digit| value = 10 * value + digit }
  value
end
```

### Analysis
I correctly created a `NUMBERS` hash which is similar to the `DIGITS` hash in the recommended solution. I also correctly created the array of digits based on the hash. The difference is in the math conversion process.

In my solution, I created a multiplier that helps determine the place value of the character. So, for a character in the thousands place, the multiplier would be `1000`, a character in the hundreds place would have a multiplier of `100` and so on. Then, I sum those up, so it might be `1000 + 100` and the resulting output would be `1100`. This is a relatively inefficient and complicated way to convert a string into a number.

The recommended solution, on the other hand, is much cleaner. It creates the `digits` array like I did. Then, it sets `value` to zero and loops through each digit. Each iteration of the loop sets `value` to the current `value` multiplied by 10 and then adds the `digit`. Finally, it returns `value`. Here's an example of that flow:

```Input '4321'
['4', '3', '2', '1']
value = 0
START LOOP
  value = 10 * 0 + 4
  value = 10 * 4 + 3
  value = 10 * 43 + 2
  value = 10 * 432 + 1
END
RETURNS 4321
```

## Small Problems > Easy 4 > Convert a String to a Signed Number!
### My Solution
```ruby
def string_to_signed_integer(str)
  case
  when str.start_with?("-") then string_to_integer(str[1..-1]) * -1
  when str.start_with?("+") then string_to_integer(str[1..-1])
  else                           string_to_integer(str)
  end
end
```

### Recommended Solution
```ruby
def string_to_signed_integer(string)
  case string[0]
  when '-' then -string_to_integer(string[1..-1])
  when '+' then string_to_integer(string[1..-1])
  else          string_to_integer(string)
  end
end
```

### Analysis
I got pretty close with this one. The only difference in the solutions is how I make the signed integer negative. I multiply the return value of `string_to_integer` by `-1` whereas the recommended solution adds a dash to the return value of `string_to_integer`.

## Small Problems > Easy 4 > Convert a Number to a String!
### My Solution
```ruby
INTEGERS = {
  0 => '0', 1 => '1', 2 => '2', 3 => '3', 4 => '4',
  5 => '5', 6 => '6', 7 => '7', 8 => '8', 9 => '9'
}

def integer_to_string(integer)
  return "0" if integer.zero?

  array = []

  loop do
    break if integer.zero?

    array << integer % 10
    integer /= 10
  end

  string = ''
  array.reverse.map { |num| string << INTEGERS[num] }
  string
end
```

### Recommended Solution
```ruby
DIGITS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

def integer_to_string(number)
  result = ""

  loop do
    number, remainder = number.divmod(10)
    result.prepend(DIGITS[remainder])
    break if number == 0
  end

  result
end
```

### Analysis
I didn't catch the trick of using Integer values as indexes, so I created an `INTEGERS` hash. The recommended solution created a `DIGITS` array. I appended to a temporary array whereas the recommended solution prepended. The recommended solution used `Numeric#divmod` whereas I stuck with division and modulus. My solution needed an extra stop of `Array#reverse` and `Array#map` which the recommended solution doesn't need.

Basically, I didn't figure out the relationship between the remainder and the `DIGITS` array. Also, I didn't think to reassign `number` with the results of `number.divmod(10)`.

## Small Problems > Easy 4 > Convert a Signed Number to a String!
### My Solution
```ruby
DIGITS = %w(0 1 2 3 4 5 6 7 8 9)

def signed_integer_to_string(input)
  input_as_string = integer_to_string(input.abs)
  return "0" if input.zero?

  input.negative? ? "-" << input_as_string : "+" << input_as_string
end
```

### Recommended Solution
```ruby
def signed_integer_to_string(number)
  case number <=> 0
  when -1 then "-#{integer_to_string(-number)}"
  when +1 then "+#{integer_to_string(number)}"
  else         integer_to_string(number)
  end
end
```

### Analysis
I like my solution better. the only major difference is the use of a `case` statement versus a ternary. This submitted solution is neat as it does everything in one line that's relatively readable:

```ruby
def signed_integer_to_string(number)
  number.zero? ? '0' : (number < 0 ? '-' : '+') << integer_to_string(number.abs)
end
```

## Small Problems > Easy 5 > ASCII String Value
### My Solution
```ruby
def ascii_value(string)
  string.chars.reduce(0) { |sum, chr| sum + chr.ord }
end
```

### Recommended Solution
```ruby
def ascii_value(string)
  sum = 0
  string.each_char { |char| sum += char.ord }
  sum
end
```

### Analysis
My solution was pretty close to the recommended solution in this case. I used `String#chars` to create an array of characters and then `Enumerable#reduce` with a starting value of `0` and the `String#ord` method as mentioned in the exercise description. The recommended solution used `String#each_char` and `String#ord`, but also needed an additional `sum` variable in the outer scope.

Interestingly, there are a couple of other ways to do this that are more clear, in my opinion:

```ruby
string.sum
```

```ruby
str.chars.map(&:ord).reduce(0, :+)
```

```ruby
str.codepoints.sum
```

## Small Problems > Easy 5 > After Midnight (Part 1)
### My Solution
```ruby
MINUTES_PER_HOUR = 60
MINUTES_PER_DAY = 1440
LEADING_ZEROS = "%02d"

def time_of_day(integer)
  hours, minutes = if integer.abs <= MINUTES_PER_DAY
                     if integer < 0
                       (MINUTES_PER_DAY - integer.abs).divmod(MINUTES_PER_HOUR)
                     else
                       integer.divmod(MINUTES_PER_HOUR)
                     end
                   else
                     (integer % MINUTES_PER_DAY).divmod(MINUTES_PER_HOUR)
                   end

  format('%02d:%02d', hours, minutes)
end
```

### Recommended Solution
```ruby
MINUTES_PER_HOUR = 60
HOURS_PER_DAY = 24
MINUTES_PER_DAY = HOURS_PER_DAY * MINUTES_PER_HOUR

def time_of_day(delta_minutes)
  delta_minutes =  delta_minutes % MINUTES_PER_DAY
  hours, minutes = delta_minutes.divmod(MINUTES_PER_HOUR)
  format('%02d:%02d', hours, minutes)
end
```

### Analysis
Back to being drastically different from the recommended solution. I feel like I was overthinking this one a little.

My solution basically has the same variables as the recommended solution.

The logic behind my solution involves checking to see if the absolute value of the given integer is less than or equal to the number of minutes in a day (1440). If it is, then it checks to see if the given integer is less than zero. 

If it's less than zero, it subtracts the number of minutes in a day from the absolute value of the given integer and then calls `Numeric#divmod` with the number of minutes in an hour.

If it's greater than zero, it uses the given integer as-is and calls `Numeric#divmod` with it and the number of minutes in an hour.

If the absolute value of the given integer is greater than the number of minutes in a day (1440), it takes the modulus of integer and the number of minutes in a day and then calls `divmod` on that result with the number of minutes in an hour.

The resulting array is then assigned to the `hours` and `minutes` variables and formatted as needed.

This works, but it's cumbersome and not entirely clear what's happening.

The recommended solution, on the other hand, is a little easier to understand and follow, although it still takes some thought. Here's the logic:

```delta_minutes is the given integer, so say it's 35.
set delta_minutes to the modulus of delta_minutes and the number of minutes in a day (1440), which is 35 in this case.
set hours and minutes to the array returned by delta_minutes.divmod(minutes per hour), which is [0, 35], so hours = 0 and minutes = 35.
```

Another example, with only numbers:
```delta_minutes = -1437
delta_minutes = -1437 % 60 = 57
hours, minutes = 57.divmod(60) = [0, 57]
```
