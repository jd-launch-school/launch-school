---
layout: post
title: Rock Paper Scissors Bonus Features
excerpt_separator: <!--more-->
tags: launch_school ruby project
---

I worked on the Rock, Paper, Scissors Bonus Features assignment.

<!--more-->


# RPS Bonus Features

Here are some of the features I'm working on:


## Hash to store the winning combinations

Instead of the complicated comparison statement with lots of `||`, I moved the logic of deciding whether or not a win condition is present into a hash that looks like this:

```ruby
WINNING_COMBINATIONS = {
  r: [:s, :l],
  p: [:r, :k],
  s: [:p, :l],
  l: [:p, :k],
  k: [:r, :s]
}
```

My problem with this at the moment is that it's not totally clear what each of the letters represent. Currently, directly above this constant is another constant called `CONVERSIONS` that shows what each letter represents:

```ruby
CONVERSIONS = { r: 'rock', p: 'paper', s: 'scissors', l: 'lizard', k: 'spock' }
```

I use this constant to add the English version of the choice to the choice output statement. So, in a way at the moment, this also serves as a key to the meanings of the letters in the hash. I don't think this is the best way to handle this, but it works for now.

My `win?` method now looks like this:

```ruby
def win?(first, second)
  WINNING_COMBINATIONS[first].include?(second)
end
```

Oh, yeah, I also added `lizard` and `spock` as additional choices.


## From `display_results` to `results`

I converted my `display_results` method into a method that returns meaningful values instead of only displaying them:

```ruby
def results(player, computer)
  if win?(player, computer)
    ['You Win!', :player]
  elsif win?(computer, player)
    ['Computer Wins!', :computer]
  else
    ["It's a Tie!"]
  end
end
```

This now returns the string for the winning message and which player won. I decompose the array that's returned by assigning its elements into two variables:

```ruby
win_message, winner = results(choice, computer_choice)
```


## Score

I'm now keeping track of the score in terms of wins. To do this, I set two global variables, `$player_score` and `$computer_score`. This works for now, but I'd ultimately like to think of a better way to do this. Also, while writing this, I'm thinking it might more accurate to name these `$player_wins` and `$computer_wins`, although that may be confusing for other reasons.

I created a `score` method to increment the score as needed:

```ruby
def score(winner)
  $player_score += 1 if winner == :player
  $computer_score += 1 if winner == :computer
end
```

Then, I display the score with the `display_score` method:

```ruby
def display_score
  prompt("")
  prompt("Score: Player [#{$player_score}], Computer [#{$computer_score}]")
  prompt("")
end
```

I'm not happy with the repetition and will have to figure out a way around this.

There's also a `reset_score` method that sets the number of wins back to zero after someone gets 5 wins:

```ruby
def reset_score
  $player_score = 0
  $computer_score = 0
end
```


## Grand Winner

With the introduction of scoring, it's possible to determine when the player or computer reaches 5 wins. When they do so, they become the Grand Winner an the `display_grand_winner` is called:

```ruby
def display_grand_winner
  prompt(WINNING_STARS)
  prompt("You are the GRAND WINNER!") if $player_score == 5
  prompt("Computer is the GRAND WINNER!") if $computer_score == 5
  prompt(WINNING_STARS)
  prompt("")
end
```

Again, I don't like the repetition here.
