---
layout: post
title: RPS Bonus Features and Arithmetic Integer
excerpt_separator: <!--more-->
tags: launch_school ruby small_problems easy_3 project
---

I mostly worked on the `grand_winner` method and logic. I also completed the Arithmetic Integer exercise.

<!--more-->


# RPS Bonus Features


## Grand Winner method

I started with the following code:

```ruby
def display_grand_winner
  display_winning_stars
  prompt("You are the GRAND WINNER!") if $player_score == 5
  prompt("Computer is the GRAND WINNER!") if $computer_score == 5
  display_winning_stars
  prompt("")
end
```

This was called with:

```ruby
if $player_score == 5 || $computer_score == 5
  display_grand_winner
  reset_score
end
```

I didn't like that I was not tracking the grand winner state and instead checking it twice. So, I refactored into these methods:

```ruby
def grand_winner?
  $player_score == 5 || $computer_score == 5
end
```

```ruby
def display_grand_winner(message)
  display_stars(message)
  prompt(message)
  display_stars(message)
  prompt("")
end
```

The `display_grand_winner` method is now called with:

```ruby
if grand_winner?
  display_grand_winner("#{$player_score == 5 ? 'You are' : 'Computer is'}" \
                       " the GRAND WINNER!")
  reset_score
end
```

In looking at the code side-by-side like this, I think I may like the way I did it earlier better than the refactor. It seems simpler even though there is some duplicate checking against the scores. Maybe keeping the `grand_winner?` method but using the first implementation will be ideal.

However, that won't be possible given the new way of calculating the stars.


## Winning Stars

I changed the following way of printing stars to the screen:

```ruby
def display_winning_stars
  prompt("*****************************")
end
```

This only printed that string of stars regardless of the message. So, I changed it to:

```ruby
def display_stars(win_message)
  prompt("*" * win_message.length)
end
```

This new way allows for more flexibility and correctly matches each message.


# Small Problems > Easy 3 > Arithmetic Integer

My solution pretty much matched the recommended solution, but I added tests, methods, and error messages for impossible calculations, like dividing by zero or raising something to a high power. I refined my testing logic a little and figured out a way to separate the tests from the input logic so I can run tests as needed and as the code changes.

Some of the recommended solutions were interesting, although most of them do not account for division by zero and large powers. Here are some methods they used:


### `Kernel#eval`

I'm not completely sure how this works, but at minimum, it evaluates its string arguments. For example:

```ruby
p eval "1 + 1"
p "1 + 1"
```

    2
    "1 + 1"

Normally, as the second statement shows, `"1 + 1"` is a string that isn't evaluated. Yet, with `Kernel#eval`, it treats it as an expression that's integer addition.


### `Object#method`

This is another one that I don't completely understand, but it works. Basically, it treats the given string as the name of a method and executes that method. Here's an example:

```ruby
p 1.method("+").(1)
```

    2

This comes in handy when iterating through `%w(+ - * / % **)` and evaluating those string operators with Integers later.


### `Enumberable#reduce` and the `%i` (array of symbols percent string)

This is my favorite. This is especially useful when combined with operations as symbols, i.e. `:+` and `:*` etc&#x2026; Here's an example:

```ruby
%i(+ - *).each { |operator| p [1, 2].reduce(operator) }
```

    3
    -1
    2

Nice.
