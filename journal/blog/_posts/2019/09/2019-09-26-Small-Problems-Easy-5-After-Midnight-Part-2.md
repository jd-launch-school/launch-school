---
layout: post
title: Small Problems > Easy 5 > After Midnight (Part 2)
excerpt_separator: <!--more-->
tags: ruby launch_school small_problems easy_5
---

This post includes:  after midnight, letter swap, clean up the words

<!--more-->

# Small Problems > Easy 5 > After Midnight (Part 2)
## Description of Exercise
Write two methods that return the number of minutes for a given time. One method is before midnight, the other method is after midnight. This is the opposite of the previous exercise.

## My Solution
My solution took me about 39 minutes to figure out. I realized early that the `before_midnight` method could use the `after_midnight` method. Even with the knowledge that I'd basically have to create only one method, it still took me a while to get it. 

Getting the hours and minutes for the given time:

```ruby
array = string.split(':').map(&:to_i)
```

This matched the recommended solution, except that in the recommended solution, they used multiple assignment to set `hours` and `minutes` variables:

```ruby
hours, minutes = time_str.split(':').map(&:to_i)
```

The formula to get the number of minutes was `hours * 60 + minutes`. I wrote it like the following:
```ruby
(array[0] * MINUTES_IN_HOUR) + array[1]
```

From there, I needed to handle the `24` edge case. I did that by returning `0` in a guard clause:

```ruby
return 0 if string == '24:00'
```

The formula for finding the minutes for a time before midnight is `1440 - after_midnight_return_value`:

```ruby
minutes == 0 ? 0 : MINUTES_IN_DAY - minutes
```

I checked for zero first.

## Other Observations
One big difference between my solution and the recommended solution was the use of modulus operator:

```ruby
(hours * MINUTES_PER_HOUR + minutes) % MINUTES_PER_DAY
```

Using modulus in this way successfully handles the `24` edge case because both `24` and `0` will return `0`:

```ruby
p (24 * 60 + 0) % 1440
p (0 * 60 + 0) % 1440
```

```
: 0
: 0
```

The further exploration asked about using `Date` and `Time` methods. This is how I did that:

```ruby
def after_midnight(string)
  require 'time'
  time = Time.parse(string)
  time.hour * 60 + time.min
end
```

The `before_midnight` method remained the same.

# Small Problems > Easy 5 > Letter Swap
## Description of Exercise
Reverse the first and last letter of each word in a given string.

## My Solution
This only took me 13 minutes to complete.

I used `String#split`, `Array#map`, Element Reference (`array[]`), a ternary operator, and `Array#join`:

```ruby
def swap(string)
  string.split.map { |word| word.length > 1 ? word[-1] + word[1..-2] + word[0] : word }.join(' ')
end
```

It's a little wordy, with the ternary operator. This returns each word's last letter concatenated with the middle letters concatenated with the first letter, essentially reversing the first and last letters of each word.

## Other Observations
The recommended solution uses multiple assignment in an idiomatic way like this:

```ruby
word[0], word[-1] = word[-1], word[0]
```

This reassigns the first and last letters in the word to one another.

A more succinct way to refactor my solution might be:

```ruby
def swap(string)
  words.split.each { |word| word[0], word[-1] = word[-1], word[0] }.join(' ')
end
```

Using `Array#each` here is important because it returns the original word whereas `Array#map` will only return the first and last letters.

# Small Problems > Easy 5 > Clean up the words
## Description of Exercise
Return a string that has all non-alphabetic characters replaced by spaces with no consecutive spaces.

## My Solution
This took me about 40 minutes.

I spent some time looking for a regular expression solution to this problem. I didn't find one, but in looking at the submitted solutions, I see what I missed.

I created a `result` variable to hold the result. Then, I used `String#chars` to create an array of characters. I called `Array#each` to iterate through the characters. I then checked to see if the character was non-alphabetic. If it was, I concatenated a space to `result` if the last character of `result` was not a space. If it isn't non-alphabetic, i.e. it's alphabetic, concatenate it to `result`.

## Other Observations
I didn't know about the `String#squeeze` method. Knowing this would've helped me figure out a working regular expression solution, similar to the recommended solution:

```ruby
def cleanup(text)
  text.gsub(/[^a-z]/i, ' ').squeeze(' ')
end
```

`String#squeeze` returns a new string where consecutive same characters are replaced by a single character.

The submitted solutions showed me that `String#squeeze` wasn't even necessary and this can be done solely with regular expressions:

```ruby
def cleanup(str)
  str.gsub(/\W+/, " ")
end
```

