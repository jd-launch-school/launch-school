---
layout: post
title: More on Pass by Reference Value
excerpt_separator: <!--more-->
tags: small_problems easy_3 ruby launch_school
---

The idea of pass by reference value and pass by value of the reference is an idea that I'll have to continually improve upon as I continue to use Ruby. It's around this time that I begin working on and implementing an efficient testing strategy for the exercises.

<!--more-->


# Pass by Reference vs Pass by Value vs Pass by Reference Value

I've been working on understanding this for the past three days whenever I get a chance. I think I'm at a point where I need to move on and refine my understanding of this as I get more experience.

Ruby's object passing strategy is `pass by reference value`, which means, a copy of the reference is passed to methods, not the reference itself. Basically, this means it acts mostly like `pass by reference` except where assignment comes in when it acts more like `pass by value`. Technically, it's neither of these as it's passing a copy of the reference to an object, not the reference itself.

So, in a way, this is like saying Ruby is `pass by value of the reference`, as it passes copies, but not copies of the objects, it passes copies of references.

I understand this, but will need to refine my thinking on this a bit to solidify it. I get that Ruby acts like `pass by reference` with mutable objects and `pass by value` with immutable objects. I also get that it's actually `pass by reference` in all cases except where immutable values are concerned. It's a little more difficult to understand what the reference, and therefore the copy of the reference actually is. Is it the variable, the object<sub>id</sub>, or something else?


# Small Problems > Easy 3 > Searching 101

I used `Array#push` and `Array#include?` for this problem.

I validated the input even though that was not necessary. In doing so, I refined my testing strategy so there's less repetition in my tests. My test code looks like this:

```ruby
def valid_integer_tests(tests)
  tests.each { |test| puts "#{valid_integer(test[0]) == test[1]}: valid_integer(#{test[0] == nil ? 'nil' : test[0]}) == #{test[1]}"}
end

valid_integer_tests([
  ['0', true], ['1', true], ['10', true], ['11', true], ['10101010000', true],
  ['1100110110011', true], [' 01 ', true], ['01', true], [' 010', true], [nil, false],
  ['', false], ['a', false], ['1a', false], [['a', 'b'], false], [' ', false],
  ['0.', false], ['0.01', false], ['1.0', false], ['1.1', false], [' 1.1 ', false]
])
```

It's a little dense, but it's better than the repetition I had previously. It's much easier to add tests. In doing this, I found a difference between the regex I'm using and the code others use to validate Integer inputs.

Others recommend and use this code:

```ruby
input.to_i > 0 && input.to_i.to_s == input
```

This fails if the input is an array `['1', '2', '3']`. It also doesn't recognize somewhat valid inputs like `(' 01 ')`, `('01')`, and `(' 010')`. The regex I'm using, on the other hand, works on all of the possible inputs:

```ruby
(input.is_a? String) && /^\s*[0]*\d+[0]*\s*$/.match?(input)
```

The recommended solution took a more straightforward approach and used `Array#<<` and `Array#include?`. The logic is basically the same as my solution though.
