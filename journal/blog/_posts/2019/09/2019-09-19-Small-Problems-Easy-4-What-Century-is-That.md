---
layout: post
title: Small Problems > Easy 4 > What Century is That?
excerpt_separator: <!--more-->
tags: ruby launch_school small_problems easy_4
---

This post includes:  what century is that?, leap years

<!--more-->

# Small Problems > Easy 4 > What Century is That?
## Description of Exercise
This method takes a year as a positive Integer and returns the century.

## My Solution
There's a bunch of information one needs to understand to complete this that isn't spelled out in the exercise description. One needs to understand how to determine the century from a year and then which years go with which endings (suffixes).

For determining years, I used `Numeric#divmod` and multiple assignment.

```ruby
century, remainder = year.divmod(100)
```

I then increment the century by `1` if the remainder is zero:

```ruby
century += 1 if !remainder.zero?
```

For the century ending (suffix), I created a method to return the appropriate ending (suffix) using a `case` statement. Special consideration needs to be made for numbers ending with 11, 12, and 13.

## Other Observations
My solution was close to the recommended solution, but it didn't match exactly. 

One of the submitted solutions pointed out the Rails `ordinal` method which is also similar to my solution and the recommended solution. This method uses `Range#include?` to account for 11, 12, and 13:

```ruby
if (11..13).include?(abs_number % 100)
  "th"
else
  case abs_number % 10
    when 1; "st"
    when 2; "nd"
    when 3; "rd"
    else    "th"
  end
end
```

The absolute number is to account for negative numbers, which our exercise doesn't need to account for.

# Small Problems > Easy 4 > Leap Years (Part 1)
## Description of Exercise
Create a method that determines if a given year is a leap year.

## My Solution
I created three solutions, each one leading to the next. I took the description literally and worked my way from years that are divisible by `4` to years divisible by `400`. Doing this lead to this result:

```ruby
def leap_year?(year)
  divisible_by_4 = year % 4 == 0
  divisible_by_100 = year % 100 == 0
  divisible_by_400 = year % 400 == 0

  divisible_by_all = divisible_by_4 && divisible_by_100 && divisible_by_400
  not_divisible_by_100 = divisible_by_4 && !divisible_by_100

  divisible_by_all || not_divisible_by_100 ? true : false
end
```

## Other Observations
The recommended solution simplified this process considerably by starting from `400` and working down to `4`:

```ruby
def leap_year?(year)
  (year % 400 == 0) || (year % 4 == 0 && year % 100 != 0)
end
```

So, if the year is divisible by `400`, it's also divisible by `100` and `4`, so it's a leap year. If it's not divisible by `400`, then it checks to see if it's divisible by `4` and not divisible by `100`.

The further exploration asks us to re-order the conditions in the first recommended solution and determine whether it still works. It doesn't work because it'll check for years divisible by `100` first and return false if they're not. That will stop the check 

There were two notable submitted solutions that I liked. One used a ternary operator and tested whether the year is divisible by `100` first:

```ruby
year % 100 == 0 ? year % 400 == 0 : year % 4 == 0
```

The other problem used guard statements checking years divisible by `400` first:

```ruby
def leap_year?(year)
  return true if (year % 400).zero?
  return false if (year % 100).zero?
  (year % 4).zero?
end
```

# Small Problems > Easy 4 > Leap Years (Part 2)
## Description of Exercise
Modify the previous leap year method to determine if a given year is a leap year before and after 1752.

## My Solution
I chose to use the guard clause solution and added another guard clause:

```ruby
def leap_year?(year)
  return (year % 4).zero? if year <= 1752
  return true if (year % 400).zero?
  return false if (year % 100).zero?

  (year % 4).zero?
end
```

## Other Observations
The better way to do this is to disable the `false` guard clause if the year is less than 1752:

```ruby
return false if (year % 100).zero? && year > 1752
```

