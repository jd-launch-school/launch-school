---
layout: post
title: Lesson 2 Quiz
excerpt_separator: <!--more-->
tags: launch_school ruby quiz
---

**SPOILER ALERT**: Don't continue reading if you haven't taken this quiz!

I scored a 17/19 (89.47%) on the quiz. I was unsure about one of the questions, so I wasn't surprised. However, I just messed up on the other question.

<!--more-->

Here are the concepts that I need to focus more on and pay closer attention to, at least, based on the results of this quiz:


# Scope of Conditional Statements

Does each branch in a conditional statement define its own scope? I didn't miss this on the quiz, but I had to think about it a little.

It's possible to create a variable within an `if/then` branch. That variable can then be accessed in the outer scope of that conditional. However, what happens if the variable is assigned but not used, i.e. the branch doesn't get executed?

Here's an example:

```ruby
if true
  a = 'true branch'
else
  b = 'false branch'
end

puts a
```

    true branch

In this case, `true branch` will always be printed to the screen since the condition will never be `false`. That said, what happens to the `b` variable assignment?

In thinking about this, it seems like it sort of acts like each branch of the condition has a local scope since both branches will never execute at the same time. Yet, I know that's not true since variables defined within `if/then` conditionals can be accessed in the outer scope. So, what happens to the `b` variable?

```ruby
if true
  a = 'true branch'
else
  b = 'false branch'
end

p b
p c
```

    nil
    [1mTraceback[m (most recent call last):
            4: from /home/akeeba/.rbenv/versions/2.6.3/bin/irb:23:in `<main>'
            3: from /home/akeeba/.rbenv/versions/2.6.3/bin/irb:23:in `load'
            2: from /home/akeeba/.rbenv/versions/2.6.3/lib/ruby/gems/2.6.0/gems/irb-1.0.0/exe/irb:11:in `<top (required)>'
            1: from (irb):120
    [1mNameError ([4mundefined local variable or method `c' for main:Object[0;1m)[m
    [1mDid you mean?  cb[m

It exists and returns `nil`! Contrast that with a variable that hasn't been defined and doesn't exist `c` where it throws and `NameError` exception.


# `{}` and `do/end` blocks

I forgot that code enclosed in this way doesn't necessarily create a new scope. It only creates a new scope when it immediately follows a method invocation.

Here's an example of a `do/end` that doesn't create a new scope:

```ruby
while true do
  a = 'it is true'
  break
end

puts a
```

    it is true

If `do/end` created a new scope, the `a` variable assigned within it wouldn't be available in the outer scope, yet it is.

Here's an example of a `do/end` that does create a new scope, since it follows a method invocation:

```ruby
'test'.chars.each do |char|
  char << '--'
end

puts char
```

    : NameError (undefined local variable or method `char' for main:Object)


# Local Variables vs. Methods

In this question, we're asked why a local variable isn't used over a method call. I recognized the nuance and was not sure what level of detail I was being asked to focus on. In hindsight, it's clear that I should've focused on a greater level of detail. I will default to that in the future.

In this case, there's a difference between:

`can't access it from within the method`

and

`it does not exist within the method`

To me, these are very close because if a variable is not available from within a method, it can be said to not exist within the method. Yet, I think the difference here is about whether or not the method knows about the variable.

In the following example, the `say_hello` method does not know about the variable and therefore, doesn't try to access it:

```ruby
def name
  "Mia"
end

name = "Singh"

def say_hello
  puts "Hello #{name}!"
end

say_hello
```

    Hello Mia!

There's no error here because other methods can be accessed from within the `say_hello` method.

That said, in general, I think Ruby first looks for a local variable, if it can't find it, it looks for a method of the same name in the current class, it then looks for a method of the same name in the parent class and so on until it either finds the method or throws an exception. So, in a sense, the fact that the variable doesn't exist within the method does mean that the method can't access it.

The question, again after looking at it with the answer in mind, hints that we want to know why the variable provided is not used, not whether or not Ruby will check for local variables first. In that context, it's not used because it doesn't exist within the method.
