---
layout: post
title: More Coding Tips, Rock Paper Scissors Walk-through
excerpt_separator: <!--more-->
tags: project ruby launch_school coding_tips
---

I started work on the Rock Paper Scissors project by watching and working through the walk-through. I also reviewed some more coding tips.

<!--more-->


# Walk-through: Rock Paper Scissors

While watching the video for the walk-through, I realized that this can evolve into a more complicated problem than it first appears to be.

For instance, initially there are only three options: rock, paper, and scissors. Handling three options isn't too complicated, however, if more options are added, the way one handles these options may not be sufficient or may lead to messy code.

I added to the code in the video by creating `retrieve_choice`, `play_again`, and `clear` methods, since I'll likely need these for the bonus features assignment coming up.

The "Things to consider" section involved learning more about the order of methods and how they affect one another. Basically, in order to call a method, its method definition must first be loaded into memory.

One of the questions also asked about returning strings instead of displaying the results from the `display_results` method.

The last thing to consider are the warnings from Rubocop about the complexity of the `display_results` method.

The original method from the video looked something like:

```ruby
def prompt(message)
  puts("=> #{message}")
end

def display_results(player, computer)
  if (player == :r && computer == :s) ||
     (player == :p && computer == :r) ||
     (player == :s && computer == :p)
    prompt('You Win!')
  elsif player == computer
    prompt("It's a Tie!")
  else
    prompt("Computer Wins!")
  end
end

display_results(:r, :s)
display_results(:s, :r)
display_results(:s, :s)
```

    => You Win!
    => Computer Wins!
    => It's a Tie!

This is what I came up with before the solution that was in the video. Doing it this way caused some problems for refactoring because I didn't realize the connection between the player and computer winning.

To deal with the Rubocop warnings, I refactored into this:

```ruby
def prompt(message)
  puts("=> #{message}")
end

def win?(player, computer)
  player == :r && computer == :s ||
    player == :p && computer == :r ||
    player == :s && computer == :p
end

def tie?(player, computer)
  player == computer
end

def display_results(player, computer)
  if win?(player, computer)
    prompt('You Win!')
  elsif tie?(player, computer)
    prompt("It's a Tie!")
  else
    prompt('Computer Wins!')
  end
end

display_results(:r, :s)
display_results(:s, :r)
display_results(:s, :s)
```

    => You Win!
    => Computer Wins!
    => It's a Tie!

Even though this works, it's not as nice as the solution in the next video.


# Rubocop

This video showed a nice way to deal with the complexity in `display_results`. Here's the way it was refactored in the video:

```ruby
def prompt(message)
  puts("=> #{message}")
end

def win?(first, second)
  first == :r && second == :s ||
    first == :p && second == :r ||
    first == :s && second == :p
end

def display_results(player, computer)
  if win?(player, computer)
    prompt('You Win!')
  elsif win?(computer, player)
    prompt('Computer Wins!')
  else
    prompt("It's a Tie!")
  end
end

display_results(:r, :s)
display_results(:s, :r)
display_results(:s, :s)
```

    => You Win!
    => Computer Wins!
    => It's a Tie!

My refactoring required two methods whereas this refactoring uses only one method. It works by swapping the computer and the player as the first player and then checking results from the first player's perspective.


# Coding Tips 2

The coding tips presented here were helpful. Some of them seem to be repeats of previous tips, but it's still helpful to be reminded of them.


## Using new lines to organize code

Knowing when and where to place new lines in code is something one learns with experience. For instance, the following is with no new lines:

```ruby
name = ''
loop do
  puts "What's your name?"
  name = gets.chomp
end
puts "Hello #{name}."
```

The following is with new lines:

```ruby
name = ''

loop do
  puts "What's your name?"
  name = gets.chomp
end

puts "Hello #{name}."
```


## Should a method return or display?

A method should do one thing, either return a meaningful result or display something to the screen. It shouldn't do both. Displaying something is considered a side-effect in that it affects something other than the return value of the method.

An example of a method with a side-effect:

```ruby
def display_name(name)
  puts name
end
```

That method will return `nil` because `puts` returns `nil` and that's the last expression to be evaluated in the method.

Here's an example that returns something meaningful:

```ruby
def capitalize_name(name)
  name.capitalize
end

capitalize_name('test')
```

It's important to understand whether a method returns a value or has some other side-effect.


## Name methods appropriately

Use names that match what the method does:

-   `total` will return a total amount
-   `display_total` will print total to the screen
-   `total!` likely has side-effects that mutate the total in some way


## Don't mutate the caller during iteration

Mutating the caller during iteration may lead to unexpected results. It's OK to mutate the elements of the caller, but don't mutate the caller itself. The example given attempted to delete every item from the calling array but two elements remained because as elements were deleted, the index increased, so some elements were skipped and left in the array.


## Variable shadowing

Using a name in an inner scope that's the same name of a variable in the outer scope is variable shadowing.

Example:

```ruby
name = 'Smith'
['Jim', 'Sue', 'Sally'].each { |name| puts "#{name} #{name}" }
```

    Jim Jim
    Sue Sue
    Sally Sally

`name` is set both in the outer scope and in the inner scope. Contrast that with the following:

```ruby
name = 'Smith'
['Jim', 'Sue', 'Sally'].each { |first_name| puts "#{first_name} #{name}" }
```

    Jim Smith
    Sue Smith
    Sally Smith


## Don't use assignment in conditional

This is confusing to others because it's not clear whether one means assignment or comparison:

```ruby
if name = get_name
  puts name
...
```

Rubocop will warn about this. Apparently, there are reasons to do this, however it's better to avoid whenever possible. Instead of the above, do the following:

```ruby
name = retrieve_name
if name
  puts name
...
```


## Use underscore for unused parameters

If a parameter is not used, use an underscore instead of a variable name that will never get used:

```ruby
books = ['test', 'test2', 'test3']
books.each { |_| puts 'Yes, there is a name present.'}
```

    Yes, there is a name present.
    Yes, there is a name present.
    Yes, there is a name present.


## Gain experience through struggling

Spend time programming:

> We can't say this enough: spend the time programming. Learn to debug through problems, struggle with it, search for the right terms, play around with the code, and you'll be able to transform into a professional developer. Because that's exactly what professional developers do on a daily basis.
