---
layout: post
title: Practice Problems and Multiplying Two Numbers
excerpt_separator: <!--more-->
tags: launch_school ruby small_problems easy_3 practice_problems lesson_3
---

Nine practice problems and the Multiplying Two Numbers small problems exercise.

<!--more-->


# RB101 > Lesson 3 > Practice Problems > Easy 1

There were nine problems in this exercise set. Here are the main points I got from doing these:


## Array#delete vs. `Array#delete_at`

`Array#delete` removes the element that matches the given argument from the array. It is a mutating method, i.e. it mutates the caller, even though the method name does not end with an exclamation point.

Example:

```ruby
array = ["a", "b", "c", "d", "e"]

array.delete("a")
p array
```

    ["b", "c", "d", "e"]

`Array#delete_at` removes the element at the index of the provided index from the array. This is also a mutating method that mutates the caller. Like `Array#delete`, there's no exclamation point to indicate its destructiveness.

Example:

```ruby
array = ["a", "b", "c", "d", "e"]

array.delete_at(0)
p array
```

    ["b", "c", "d", "e"]


## Range#cover?

`Range#cover?` returns `true` if the provided argument is within the calling range.

Example:

```ruby
p (0..1).cover?(0.3)
p (0..1).cover?(2)
p ("a".."e").cover?("d")
p ("a".."e").cover?("f")
```

    true
    false
    true
    false


## Prepending to a String

There are at least 4 ways to do this:


### Using `String#prepend` (mutating method)

```ruby
string = "is a string."
string.prepend("This ")
puts string
```

    This is a string.


### Using `String#insert` (mutating method)

```ruby
string = "is a string."
string.insert(0, "This ")
puts string
```

    This is a string.


### Using concatenation with `+` or `concat`

```ruby
string = "is a string."
p "This " + string
```

    "This is a string."


### Using `<<` (mutating method)

```ruby
string = "is a string."
this = "This "
this << string
p this
```

    "This is a string."


## Kernel#eval

It's possible to evaluate a string as if it were a statement. The question created a string that looked like:

```ruby
"add_eight(add_eight(add_eight(add_eight(add_eight(number)))))"
```

Then evaluated it.

Example:

```ruby
string = "1 + 1"
p eval(string)
```

    2


## Hash#assoc

This returns the key and value of the given key as an array.

Example:

```ruby
hash = { person1: "Ozaria", person2: "Ramona", person3: "Felipe", person4: "Rodenda" }
p hash.assoc(:person1)
```

    [:person1, "Ozaria"]


# Small Problems > Easy 3 > Multiplying Two Numbers

Even though this was simple, it required some additional thought, especially in terms of validation and what types of input to accept, even though that wasn't in the main exercise.

The exercise asked us to create a method that takes two arguments, multiplies them, and returns the result. It's purpose is to focus on the difference between returning a value and printing a value to the screen.

For example, this solution prints a value to the screen but returns the value of `nil`:

```ruby
def multiply(first, second)
  puts first * second
end

multiply(1, 2)
p multiply(1, 2) == nil
```

    2
    2
    true

The following example actually returns the result of the multiplication:

```ruby
def multiply(first, second)
  first * second
end

p multiply(1, 2)
p multiply(1, 2) == nil
```

    2
    false

It's a subtle difference, but an important one.

The other issue with this exercise is using `*` when the inputs aren't validated.

For example:

```ruby
def multiply(first, second)
  first * second
end

p multiply("a", 2)
p multiply(["a", "b"], 2)
```

    "aa"
    ["a", "b", "a", "b"]

Those results may or may not be desired, depending on where this method is going to be used.
