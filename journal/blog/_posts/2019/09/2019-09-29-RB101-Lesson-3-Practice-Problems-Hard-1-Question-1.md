---
layout: post
title: RB101 > Lesson 3 > Practice Problems > Hard 1 > Question 1
excerpt_separator: <!--more-->
tags: ruby launch_school rb101 lesson_3 practice_problems hard_1
---

This post includes:  question 1, question 2, question 3, question 4

<!--more-->

# RB101 > Lesson 3 > Practice Problems > Hard 1 > Question 1
## Description of Exercise
Determine the output of the following code:

```ruby
if false
  greeting = “hello world”
end

greeting
```

## My Solution
I correctly noted that `greeting` would return `nil` since variables created within `if` statements are available outside the `if` statement even if the `if` block isn't executed.

## Other Observations
Here's another example:

```ruby
if false
  test1 = 'a'
else
  test2 = 'b'
end

puts 'Only the false branch is executed:'
puts "test1: #{test1.inspect}"
puts "test2: #{test2.inspect}"

if true
  test3 = 'a'
else
  test4 = 'b'
end

puts "\nOnly the true branch is executed:"
puts "test3: #{test3.inspect}"
puts "test4: #{test4.inspect}"
```

```
: Only the false branch is executed:
: test1: nil
: test2: "b"
: 
: Only the true branch is executed:
: test3: "a"
: test4: nil
```

# RB101 > Lesson 3 > Practice Problems > Hard 1 > Question 2
## Description of Exercise
What is the result of the last line in the code below?

```ruby
greetings = { a: 'hi' }
informal_greeting = greetings[:a]
informal_greeting << ' there'

puts informal_greeting  #  => "hi there"
puts greetings
```

## My Solution
I correctly stated that the element with key `:a` was mutated by the shovel operator (`String#<<`) so the result will be `{ a: 'hi there' }`.

## Other Observations
The recommended solution mentioned a couple of ways to not mutate the element. Here they are in code:

### First, using `Object#clone`
```ruby
greetings = { a: 'hi' }
informal_greeting = greetings[:a].clone
informal_greeting << ' there'

puts informal_greeting  #  => "hi there"
puts greetings
```

```
: hi there
: {:a=>"hi"}
```

### Second, using reassignment and concatenation
```ruby
greetings = { a: 'hi' }
informal_greeting = greetings[:a]
informal_greeting += ' there'

puts informal_greeting  #  => "hi there"
puts greetings
```

```
: hi there
: {:a=>"hi"}
```

# RB101 > Lesson 3 > Practice Problems > Hard 1 > Question 3
## Description of Exercise
Given three code snippets (A, B, C), determine what will be printed. This question is testing our knowledge and awareness of how reassignment affects variables.

## My Solution
Reassignment never mutates, it always points to a new object. So, in the first two code snippets, the variables within the methods are being reassigned and do not have an impact on the variables in the outer scope. In the third snippet, on the other hand, `String#gsub!` is a mutating method that does affect the variables in the outer scope.

## Other Observations
Here's another example:

### Non-mutating (reassignment)
```ruby
def method_scope(a, b, c)
  a += b
  b += a
  c += c
end

a = 'this'
b = 'that'
c = 'other'

method_scope(a, b, c)

puts "a: #{a}" # 'this'
puts "b: #{b}" # 'that'
puts "c: #{c}" # 'other'
```

```
: a: this
: b: that
: c: other
```

### Mutating using `String#<<`
```ruby
def method_scope(a, b, c)
  a << b
  b << a
  c << c
end

a = 'this'
b = 'that'
c = 'other'

method_scope(a, b, c)

puts "a: #{a}" # 'thisthat'
puts "b: #{b}" # 'thatthisthat'
puts "c: #{c}" # 'otherother'
```

```
: a: thisthat
: b: thatthisthat
: c: otherother
```

# RB101 > Lesson 3 > Practice Problems > Hard 1 > Question 4
## Description of Exercise
Debug some code that didn't have a `false` condition and didn't fully validate the input.

The code is supposed to check for a valid IP address.

## My Solution
I created the `is_an_ip_number?` method so that I could run tests:

```ruby
def is_an_ip_number?(string)
  string.to_i.between?(0, 255)
end
```

My tests looked like this:
```ruby
def dot_separated_ip_address_tests(tests)
  tests.each { |test| puts "#{dot_separated_ip_address?(test[0]) == test[1] ? "PASS" : "FAIL"}: dot_separated_ip_address?(#{test[0].inspect}) == #{test[1].inspect}"}
end

dot_separated_ip_address_tests([
  ['1.2.3.4', true], ['1.2.3.4.5', false], ['1.2.3', false], ['255.255.255.255', true],
  ['256.256.256.256', false]
])
```

I solved this by adding a `counter` variable and only incrementing this variable when the given number is between `0` and `255`. If, at the end of the loop, `counter` is equal to `4`, then it's a valid IP address.

## Other Observations
The recommended solution is better in that it doesn't require a separate `counter` variable:

```ruby
def dot_separated_ip_address?(input_string)
  dot_separated_words = input_string.split(".")
  return false unless dot_separated_words.size == 4

  while dot_separated_words.size > 0 do
    word = dot_separated_words.pop
    return false unless is_an_ip_number?(word)
  end

  true
end
```

