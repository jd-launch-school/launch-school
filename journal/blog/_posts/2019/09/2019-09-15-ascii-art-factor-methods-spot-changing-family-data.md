---
layout: post
title: ASCII Art, Factor Methods, Spot Changing Family Data
excerpt_separator: <!--more-->
tags: launch_school ruby lesson_3 practice_problems medium_1
---

The following are some highlights from this section.

<!--more-->

# ASCII Art

This one had to do with ASCII Art, in a sense. We were asked to add empty spaces before a string, adding one extra space each time. I initially did this with `Integer#times` and `String.rjust`. It worked, but I had to add a magic number to make the string with spaces longer than the given string so spaces would be added to the left. My solution looked like this:

```ruby
10.times { |index| puts "The Flintstones Rock!".rjust(index + 21) }
```

```
The Flintstones Rock!
 The Flintstones Rock!
  The Flintstones Rock!
   The Flintstones Rock!
    The Flintstones Rock!
     The Flintstones Rock!
      The Flintstones Rock!
       The Flintstones Rock!
        The Flintstones Rock!
         The Flintstones Rock!
```

The recommended solution used `*` to repeat the spaces and then `+` concatenation to add the repeated spaces to the given string. This has the benefit of working with any string size and no magic numbers.

I modified the recommended solution and used string interpolation, like this:

```ruby
10.times { |index| puts "#{" " * index}The Flintstones Rock!" }
```

# Factor Methods
In this one, we're given a `factors` method that works, but doesn't account for zero or negative numbers. I removed `begin/end/until` and added `loop/do/end` instead. The recommended solution used a `while` loop. They both seems similar:

## loop

```ruby
loop do
  break if divisor <= 0

  factors << number / divisor if number % divisor == 0
  divisor -= 1
end
```

## while

```ruby
while divisor > 0 do
  factors << number / divisor if number % divisor == 0
  divisor -= 1
end
```

# Spot Changing Family Data
This answers the question of whether or not changes to a hash's elements within a method change the hash outside of the method. It does, unless changes are placed into a different hash. I created a working solution that does this:

```ruby
munsters = {
  "Herman" => { "age" => 32, "gender" => "male" },
  "Lily" => { "age" => 30, "gender" => "female" },
  "Grandpa" => { "age" => 402, "gender" => "male" },
  "Eddie" => { "age" => 10, "gender" => "male" },
  "Marilyn" => { "age" => 23, "gender" => "female"}
}

def mess_with_demographics(demo_hash)
  temp_hash = {}

  demo_hash.keys.each do |family_member|
    temp_hash[family_member] = {
      "age" => demo_hash[family_member]["age"] + 42,
      "gender" => 'other'
    }
  end

  temp_hash
end

p mess_with_demographics(munsters)
p munsters
```

```
: {"Herman"=>{"age"=>74, "gender"=>"other"}, "Lily"=>{"age"=>72, "gender"=>"other"}, "Grandpa"=>{"age"=>444, "gender"=>"other"}, "Eddie"=>{"age"=>52, "gender"=>"other"}, "Marilyn"=>{"age"=>65, "gender"=>"other"}}
: {"Herman"=>{"age"=>32, "gender"=>"male"}, "Lily"=>{"age"=>30, "gender"=>"female"}, "Grandpa"=>{"age"=>402, "gender"=>"male"}, "Eddie"=>{"age"=>10, "gender"=>"male"}, "Marilyn"=>{"age"=>23, "gender"=>"female"}}
```
