---
layout: post
title: Mutating and Non-Mutating Methods, Sum of Product
excerpt_separator: <!--more-->
tags: small_problems easy_2 ruby launch_school
---

I learned some more about mutating and non-mutating methods. Some operations that look like assignment may actually be mutating. Also, some mutating methods don't use the convetion of placing an exclamation mark `!` at the end of their name.

<!--more-->


# Blog Post on Mutating and Non-Mutating Methods in Ruby

The main takeaways for me from this article are that assignment is non-mutating and some operations that look like assignment, i.e. they're in the form of `variable = value` are mutating because they're not assignment.

> While `=` is not an actual method in Ruby, it acts like a non-mutating method, and should be treated as such.

Some examples:

```ruby
def fix(value)
  value.upcase!
  value.concat('!')
  value
end

s = 'hello'
t = fix(s)

p s
p t
p s.object_id
p t.object_id
```

    "HELLO!"
    "HELLO!"
    47294866682000
    47294866682000

Here, `String#upcase!` and `String#concat` are mutating methods, so `s` and `t` reference the same String object throughout the operation.

versus:

```ruby
def fix(value)
  value = value.upcase
  value.concat('!')
end

s = 'hello'
t = fix(s)

p s
p t
p s.object_id
p t.object_id
```

    "hello"
    "HELLO!"
    47208536166780
    47208536166760

Here, `value` is reassigned to the result of the `String#upcase` method, which is a non-mutating method. Therefore, it creates a copy of `value` and references a new String object `'HELLO'`.

Then `String#concat`, a mutating method, mutates (changes) the new String object referenced by `value` to `'HELLO!'`.

In the end, `s` continues to reference the original String object `'hello'` while `t` references the new String object `'HELLO!'`.

The blog post goes through a few more examples like this, showing how assignment interacts with mutating methods.


# Small Problems > Easy 2 > Sum or Product of Consecutive Integers

I decided to do this with `Enumberable#reduce`, so I didn't need to do the further exploration. I added some more robust testing to test the inputs and the calculations. I need to figure out a way to deal with user input within org-mode src blocks so that I can integrate testing better.

`Enumerable#reduce` is an alias for `Enumerable#inject`, so I'm not sure if it matters which one someone uses. I chose `Enumerable#reduce` since I'm used to it from JavaScript. Here's an example of `Enumerable#reduce` in action:

```ruby
p (1..5).reduce { |sum, num| sum + num }
```

    15

Another way to write the above is:

```ruby
p (1..5).reduce(:+)
```

    15

In terms of validating user input, some of the recommended solutions had this:

```ruby
def valid_integer(input)
  input.to_i > 0 && input.to_i.to_s == input
end

def integer_tests
  puts "#{valid_integer('1') == true}: valid_integer('1') == true"
  puts "#{valid_integer('10') == true}: valid_integer('10') == true"
  puts "#{valid_integer('a') == false}: valid_integer('a') == false"
  puts "#{valid_integer('0') == false}: valid_integer('0') == false"
  puts "#{valid_integer('01') == false}: valid_integer('01') == false"
  puts "#{valid_integer('1a') == false}: valid_integer('1a') == false"
  puts "#{valid_integer('a1') == false}: valid_integer('a1') == false"
  puts "#{valid_integer('') == false}: valid_integer('') == false"
  puts "#{valid_integer(nil) == false}: valid_integer(nil) == false"
  puts "#{valid_integer('-1') == false}: valid_integer('-1') == false"
end

integer_tests
```

    true: valid_integer('1') == true
    true: valid_integer('10') == true
    true: valid_integer('a') == false
    true: valid_integer('0') == false
    true: valid_integer('01') == false
    true: valid_integer('1a') == false
    true: valid_integer('a1') == false
    true: valid_integer('') == false
    true: valid_integer(nil) == false
    true: valid_integer('-1') == false

That seems to work as well as my regex solution:

```ruby
def valid_integer(input)
  /^[1-9]+\d*$/.match?(input)
end

def integer_tests
  puts "#{valid_integer('1') == true}: valid_integer('1') == true"
  puts "#{valid_integer('10') == true}: valid_integer('10') == true"
  puts "#{valid_integer('a') == false}: valid_integer('a') == false"
  puts "#{valid_integer('0') == false}: valid_integer('0') == false"
  puts "#{valid_integer('01') == false}: valid_integer('01') == false"
  puts "#{valid_integer('1a') == false}: valid_integer('1a') == false"
  puts "#{valid_integer('a1') == false}: valid_integer('a1') == false"
  puts "#{valid_integer('') == false}: valid_integer('') == false"
  puts "#{valid_integer(nil) == false}: valid_integer(nil) == false"
  puts "#{valid_integer('-1') == false}: valid_integer('-1') == false"
end

integer_tests
```

    true: valid_integer('1') == true
    true: valid_integer('10') == true
    true: valid_integer('a') == false
    true: valid_integer('0') == false
    true: valid_integer('01') == false
    true: valid_integer('1a') == false
    true: valid_integer('a1') == false
    true: valid_integer('') == false
    true: valid_integer(nil) == false
    true: valid_integer('-1') == false

I'm torn on which is easier to read. Right now, I prefer the regex.

I also had this validation for the operation:

```ruby
def valid_string(input)
  /[s, p]/i.match?(input)
end
```
