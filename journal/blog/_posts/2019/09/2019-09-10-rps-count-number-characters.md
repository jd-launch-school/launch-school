---
layout: post
title: RPS Bonus Features and Counting Characters
excerpt_separator: <!--more-->
tags: launch_school ruby project small_problems easy_3
---

Continued work on the Rock Paper Scissors assignment. Completed the Counting the Number of Characters small problem exercise.

<!--more-->


# RPS Bonus Features


## Display Header Prompt

I moved the prompts about the available choices along with the score to a separate `display_header_prompt` method. This gets displayed every time the user is asked to input a choice:

```ruby
def display_header_prompt(score)
  display_score(score)
  prompt("")
  prompt("Choose one: (r)ock, (p)aper, (s)cissors, (l)izard, spoc(k)")
  prompt("You can also choose (q)uit.")
end
```

Some of this was previously in the `main` loop and some was in the `retrieve_choice` method.


## Input to Symbol Method

I moved the logic for converting an input into a symbol to its own method. This was originally in the `retrieve_choice` method.

```ruby
def input_to_symbol(input)
  input.match?(/spock/i) ? :k : input[0].downcase.to_sym
end
```

In this version, I'm no longer using the safe navigation operator `&.` because I changed the logic in the `retrieve_choice` method so that it first checks to see if the input is valid and then converts it to a symbol:

```ruby
return input_to_symbol(input) if VALID_PLAYER_CHOICES.include?(input)
```


## Reek

I added explanations to the documentation for the code smells that Reek warns about. Most of the warnings are `UtilityFunction` warnings. The other warnings are `FeatureEnvy`, `DuplicateMethodCall`, and `TooManyStatements`.


# Counting the Number of Characters

This exercise involves prompting the user for input and then returning the count of characters in that input, not including spaces. I chose to use `String#gsub`, `String#chars`, and `Array#length` like this:

```ruby
def retrieve_input(input)
  input.gsub(/\s/, '').chars.length
end

p retrieve_input('test')
p retrieve_input('testing a 25 character string')
```

    4
    25

The recommended solution used `String#delete` and `String#size` like this:

```ruby
def retrieve_input(input)
  input.delete(' ').size
end

p retrieve_input('test')
p retrieve_input('testing a 25 character string')
```

    4
    25

I also did some validation on the input, although it's not perfect as it still lets non-words through:

```ruby
def valid_input?(input)
  input.match?(/.*[A-Za-z]+.*/)
end

p valid_input?('')
p valid_input?(' ')
p valid_input?('test')
p valid_input?('testing a 25 character string')
```

    false
    false
    true
    true
