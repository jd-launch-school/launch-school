---
layout: post
title: RB101 > Lesson 4 > Ruby Collections > Collection Basics
excerpt_separator: <!--more-->
tags: ruby launch_school rb101 lesson_4
---

This post includes:  ruby collections, collection basics, strings, arrays, hashes, looping

<!--more-->

# RB101 > Lesson 4 > Ruby Collections > Collection Basics
I reviewed element reference, conversion, and element assignment for strings, arrays, and hashes.

Here's an example of each:

## Strings
### Element Reference
```ruby
string = 'this is a string'
p string[0]
p string[0, 4]
p string.slice(0, 4)
```

```
: "t"
: "this"
: "this"
```

### Conversion
```ruby
string = 'this is a string'
p string.split

string = '1'
p string.to_i
```

```
: ["this", "is", "a", "string"]
: 1
```

### Assignment
```ruby
string = 'this is a string'
string[0] = 'T'
string[10] = 'S'
p string
```

```
: "This is a String"
```

## Arrays
### Element Reference
```ruby
array = [1, 2, 3, 4, 5]
p array[2]
p array[2, 3]
```

```
: 3
: [3, 4, 5]
```

### Conversion
```ruby
array = [1, 2, 3, 4, 5]
p array.join
p array.join(' ')

array2 = [[:a, '1'], [:b, '2'], [:c, '3']]
p array2.to_h
```

```
: "12345"
: "1 2 3 4 5"
: {:a=>"1", :b=>"2", :c=>"3"}
```

### Assignment
```ruby
array = [1, 2, 3, 4, 5]
array[0] = 1000
p array
```

```
: [1000, 2, 3, 4, 5]
```

## Hashes
### Element Reference
```ruby
hash = {a: 1, b: 2, c: 3}
p hash[:a]
p hash = {a: 1, b: 2, c: 3, a: 4}
```

```
: 1
: (irb):43: warning: key :a is duplicated and overwritten on line 43
: {:a=>4, :b=>2, :c=>3}
```

### Conversion
```ruby
hash = {a: 1, b: 2, c: 3}
p hash.to_a
```

```
: [[:a, 1], [:b, 2], [:c, 3]]
```

### Assignment
```ruby
hash = {a: 1, b: 2, c: 3}
hash[:a] = 1000
p hash
```

```
: {:a=>1000, :b=>2, :c=>3}
```

# RB101 > Lesson 4 > Ruby Collections > Looping
I was able to skim through this section since I understand the first example. Here's an explanation of that example:

```ruby
arr = [1, 2, 3, 4, 5]
counter = 0

loop do
  arr[counter] += 1
  counter += 1
  break if counter == arr.size
end

arr # => [2, 3, 4, 5, 6]
```

- set the `arr` variable to reference the `[1, 2, 3, 4, 5]` array object.
- set the `counter` variable to `0`
- start the loop
  + increment the element of the array at the `counter` position by one
  + increment the `counter` by reassigning it to one plus its current value
  + if `counter` is equal to the number of elements in the array (`arr.size`), break out of the loop
- end loop
- return `arr`

Nevertheless, I picked up some important information in the lesson.

Even though I've used `if` modifiers, I'm not sure I knew what they were called. An `if` modifier is when an `if` condition is appended to a statement. For example:

```ruby
puts "print this" if true
puts "don't print this" if false
```

```
: print this
```

The four basic elements of a loop are:

1. loop
2. counter
3. way to retrieve current value
4. way to exit loop

I need more practice iterating over hashes using basic loops. Here's an example:

```ruby
hash = { name: 'Someone', age: 33, profession: 'software engineer' }
counter = 0
keys = hash.keys

loop do
  break if counter >= hash.length
  puts "#{keys[counter]}: #{hash[keys[counter]]}"
  counter += 1
end
```

```
: name: Someone
: age: 33
: profession: software engineer
```

