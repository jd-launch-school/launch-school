---
layout: post
title: Lesson 3 Practice Problems
excerpt_separator: <!--more-->
tags: launch_school ruby lesson_3 practice_problems
---

Worked through the Easy 2 set of the Lesson 3 practice problems.

<!--more-->

It felt like I went through these problems faster. I think this is because most of the solutions were one-liners or involved methods I'm already familiar with. That said, I did pick up some new methods along the way.


# Hash key matching

It seems that the following Hash methods are synonymous:

-   `has_key?`
-   `key?`
-   `include?`
-   `member?`

The source code for each of these is exactly the same, so they are aliases.

I guess one reason to use `Hash#key?` instead of `Hash#has_key?` is that it recommends the former over the latter in the [style guide](https://github.com/rubocop-hq/ruby-style-guide#hash-key).

At this point, my first instinct is to use `Hash#include?` since that's what I often use for arrays and strings.

This [LS forum post](https://launchschool.com/posts/a8c45b7e) about the use of these four methods may be helpful.


# String#swapcase

I hadn't used this before, but it's straightforward:

```ruby
string = "Hello Everyone!"
puts string.swapcase
```

    hELLO eVERYONE!


# Hash#merge

Merging two hashes into one flattened hash is also straightforward:

```ruby
hash1 = {test: 1, test2: 2, test3: 3}
hash2 = {test4: 4, test5: 5}

p hash1.merge(hash2)
```

    {:test=>1, :test2=>2, :test3=>3, :test4=>4, :test5=>5}


# Adding multiple items to an Array

I found four ways to do this:


## Using `Array#concat`

```ruby
my_array = %w(1, 2, 3, 4, 5)
my_array.concat(%w(6, 7, 8, 9))
p my_array
```

    ["1,", "2,", "3,", "4,", "5", "6,", "7,", "8,", "9"]


## Using `Array#push`

```ruby
my_array = %w(1, 2, 3, 4, 5)
my_array.push('6', '7', '8', '9')
p my_array
```

    ["1,", "2,", "3,", "4,", "5", "6", "7", "8", "9"]


## Using `Array#append`

```ruby
my_array = %w(1, 2, 3, 4, 5)
my_array.append('6', '7', '8', '9')
p my_array
```

    ["1,", "2,", "3,", "4,", "5", "6", "7", "8", "9"]


## Using `Array#prepend`

```ruby
my_array = %w(1, 2, 3, 4, 5)
my_array.prepend('6', '7', '8', '9')
p my_array
```

    ["6", "7", "8", "9", "1,", "2,", "3,", "4,", "5"]


# Using `String#index` to get length

This is a neat trick. In this case, we needed to slice a string at a certain word. I did this by using a regular expression. It works, but it's not resilient and difficult to modify if needed. The recommended solution, on the other hand, used `String#index` which makes it much more resilient to change:

```ruby
string = "This is a long string that needs to be cut in half at the word 'needs'."
p string.slice!(0, string.index('needs'))
p string
```

    "This is a long string that "
    "needs to be cut in half at the word 'needs'."

If I wanted to cut off the sentence at the word 'cut', it's a matter of changing one word:

```ruby
string = "This is a long string that needs to be cut in half at the word 'needs'."
p string.slice!(0, string.index('cut'))
p string
```

    "This is a long string that needs to be "
    "cut in half at the word 'needs'."


# String#count

I don't use this much, but it's nice to know it's available as needed.

```ruby
string = "How many e's are there in this sentence?"
p string.count('e')
```

    7

I feel like this is a precursor to being able to display a count of all of the letters in a string, something like:

    a: 2
    b: 0
    c: 1
    d: 0
    e: 7
    ... and so on ...


# String#center

Another straightforward method that I don't use much but am happy to know it exists.

```ruby
string = "This is a string."
p string.center(40)
```

    "           This is a string.            "
