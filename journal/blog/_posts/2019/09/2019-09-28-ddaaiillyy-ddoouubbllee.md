---
layout: post
title: ddaaiillyy ddoouubbllee
excerpt_separator: <!--more-->
tags: ruby launch_school
---

This post includes:  ddaaiillyy ddoouubbllee, bannerizer, spin me around in circles

<!--more-->

# ddaaiillyy ddoouubbllee
## Description of Exercise
Given a method with duplicate consecutive characters, return a string that removes the duplicate consecutive characters.

For example:

`ddaaiillyy ddoouubbllee` would return `daily double`

## My Solution
This took me 8 minutes to get a working solution. It took me much longer to mess with the regex.

I based my solution on the way that I did the [Clean Up Words](file:`/Documents/ruby/launch_school/small_problems/easy_5/clean_up_words.org) exercise. First, it creates an empty `results` variable. Then, it creates an array of characters from the given string. If the character is not equal to the last character in `result`, it appends the character to `result`. If it is equal, it moves to the next iteration. When the loop is done, it returns `result`.

## Other Observations
I like my solution better than the recommended solution, at least as far as the way the code looks. The recommended solution loops based on the length of the given string and then compares the current character to the next character.

That said, one part of the further exploration mentioned that it was intentional to not use `String#each_char` and `String#char`. I'm not sure why one way is better/worse than the other.

The third part of the further exploration asked us to try using Regex to do this. I didn't figure it out. I need more practice with groups and variables in regex. This solution seems succinct and effective:

```ruby
str.gsub(/(.)\1+/, '\1')
```

Breaking this down, the `(.)` matches one character. `\1` references that first match (character). The `+` says one or more of the same character. So, it says that if two or more of the same characters. `str.gsub` means replace the left argument with the right, so in this case, two or more of the same characters is replaced with the character `\1`.

I understand this conceptually, but couldn't figure out how to do this with Ruby.

# Bannerizer
## Description of Exercise
Write a method that will take a short line of text, and print it within a box.

## My Solution
It took me 19 minutes to get a working solution, mostly messing with the tests.

Since I'm running my code through my test suite, I needed to have a String as output as opposed to using `puts` like the recommended solution.

First, I find out what the length of the dashes and spaces is going to be. I call this `filler_length`. It's the length of the string + 2. Then, I create two variables, `border` and `blank_line`. `border` represents the top and bottom of the box. `blank_line` represents the blank lines between the borders and the string. Finally, I put it all together as one string, adding newlines as needed:

```ruby
border + "\n" + blank_line + "\n| " + string + " |\n" + blank_line + "\n" + border
```

## Other Observations
The recommended solution did basically the same thing, except it used string interpolation instead of string concatenation and used `puts` instead of returning a string.

One of the submitted solutions that stuck out to me really maintained the feel of the box by using triple quotes:

```ruby
def print_in_box(string)
spaces = string.chars.map { |element| ' ' }.join
dashes = string.chars.map { |element| '-' }.join
puts"""
+-#{dashes}-+
| #{spaces} |
| #{string} |
| #{spaces} |
+-#{dashes}-+
"""
end
```

Further exploration asks us to truncate the string if it's longer than 80 characters. I did this by slicing the given string when the length of the string was greater than 77 character.

```ruby
length = string[0..76].length

...

string = length == 77 ? "#{string[0..73]}..." : string
```

The reason I have `length == 77` is because sometimes the length is going to be less than `77` and I don't want to truncate in those cases.

# Spin Me Around In Circles
## Description of Exercise
Given a method, we're asked to state whether the result will be a new object or the original object.

## My Solution
The answer I gave was that it will be a new object because we're splitting the string and that creates an new array object. Here's an example:

```ruby
p string = 'this is a string'
p string.object_id

p split_string = string.split
p split_string.object_id
```

```
: "this is a string"
: 47401138573800
: ["this", "is", "a", "string"]
: 47401138573600
```

The rest of the method changes that array, leaving the original string untouched.
