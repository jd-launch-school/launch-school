---
layout: post
title: Odd Lists and Palindromic Strings (Part 1)
excerpt_separator: <!--more-->
tags: launch_school ruby practice_problems easy_3
---

The following are some highlights from this section.

<!--more-->

# Small Problems > Easy 3 > Odd Lists
## Description of the Exercise
An array is given as an argument. Return every other element of the array starting with the first element in the array. This means return the first, third, fifth, ..., n elements.

## My Solution
I chose to use `Array#filter`, `Enumerable#with_index`, and `Integer#even?`. I return a filtered array that only includes even **indexes** (0, 2, 4, 6, etc...) which match up to the first, third, fifth, ..., n elements.

Here's my code:

```ruby
def oddities(array)
  array.filter.with_index { |_, index| index.even? }
end
```

## Other Observations
The recommended solution used a `while` loop, appending to and returning a new array.

For the further exploration, we're asked to return every other element starting with the second element in the array, so the second, fourth, sixth, ..., n elements. Using my solution, this was a matter of changing `index.even?` to `index.odd?`. However, I chose to try to write a `while` loop and did it similar to the recommended solution.

We're also asked to complete this in two additional ways. I solved it with `loop` and `Enumerable#each_with_index`. Both of these require creating and returning a new array. The `loop` solution is similar to the `while` solution, but uses `break` instead:

### Tests

```ruby
def oddities_tests(tests)
  tests.each { |test| puts "#{oddities(test[0]) == test[1] ? "PASS" : "FAIL"}: oddities(#{test[0].inspect}) == #{test[1].inspect}"}
end

oddities_tests([
  [[2, 3, 4, 5, 6], [2, 4, 6]], [[1, 2, 3, 4, 5, 6], [1, 3, 5]], [['abc', 'def'], ['abc']],
  [[123], [123]], [[], []], [[1, 1, 2, 2, 3, 4, 4], [1, 2, 3, 4]]
])
```

```
: PASS: oddities([2, 3, 4, 5, 6]) == [2, 4, 6]
: PASS: oddities([1, 2, 3, 4, 5, 6]) == [1, 3, 5]
: PASS: oddities(["abc", "def"]) == ["abc"]
: PASS: oddities([123]) == [123]
: PASS: oddities([]) == []
: PASS: oddities([1, 1, 2, 2, 3, 4, 4]) == [1, 2, 3, 4]
```

### Code

```ruby
def oddities(array)
  odd_items = []
  counter = 0

  loop do
    break if counter >= array.size

    counter.even? && odd_items << array[counter]
    counter += 1
  end

  odd_items
end
```

# Small Problems > Easy 3 > Palindromic Strings (Part 1)
## Description of the Exercise
Create a method that returns `true` if the given string is a palindrome and `false` if it's not. Case, punctuation, and spaces are important.

## My Solution
I solved this with `String#reverse`, like so:

```ruby
def palindrome?(input)
  return false if input.empty?

  input.reverse == input
end
```

I added a check for empty inputs because I don't think empty strings should be considered palindromes. Since they're empty, there's nothing to compare so it's not possible to state that the empty string reads the same forward and backward ... there is no forward and backward.

## Other Observations
The recommended solution was the same, except without the check for empty strings.

For further exploration, we're asked to make this method work with arrays and then with both arrays and strings. This is a trick question, in a sense, because the method doesn't care whether the input is an array or a string. There's are `Array#reverse` and `String#reverse` methods.

A couple of the submitted solutions were interesting, both doing a recursive approach. Here they are for future reference:

### Recursion
I added the check for empty strings. This checks if the size of the input is greater than `2`. If it is, checks to see if the first character matches the last character. If they don't match, it's not a palindrome and it returns `false`. If they do match, it calls itself with the string minus its first and last character.

It continues until the string size is less than or equal to `2`, then it checks one last time. If the starting letter and ending letter are equal, it returns `true`.

```ruby
def palindrome?(str)
  return false if str.empty?
  return str[0] == str[-1] && palindrome_rec?(str[1..-2]) if str.size > 2

  str[0] == str[-1]
end
```

### Loop (like recursion)
I added the check for empty strings. This breaks if the string size is less than or equal to `1`. So, when it's down to one character or zero characters, it must be a palindrome, so it returns `true`.

It also breaks and returns `false` if the first and last characters don't match at any point during the loop. Then, at the end of each iteration, it reassigns the inner `string` variable to the same string but with the first and last character removed.

```ruby
def palindrome?(string)
  return false if string.empty?

  loop do
    break true if string.size <= 1
    break false if string[0] != string[-1]

    string = string[1..-2]
  end
end
```
