---
layout: post
title: RB101 Lesson 4 Practic Problems 7-10
excerpt_separator: <!--more-->
tags: launch_school ruby rb101 lesson_4
---

Working on RB101 Lesson 4.

<!--more-->


# Additional Practice > Practice Problem 7


## Description of Problem

Create a hash that expresses the frequency with which each letter occurs in a string.


## My Solution

I solved this using `loop`, `each`, and `each_with_object`. None of the solutions are particularly elegant, but they all work. The `each` and `each_with_object` solutions required that I delete spaces, split the string into characters, and then sort alphabetically.


## Other Observations

The recommended solution created an empty `result` hash and an array of each upper and lower case letter of the alphabet:

```ruby
letters = ('A'..'Z').to_a + ('a'..'z').to_a
```

It then uses `each` to iterate through the array of letters. It creates a `letter_frequency` variable that holds the result of `scan` to check if the current letter is in the original string and then calling count on that to return the number of times that letter is in the string.

Then, if `letter_frequency` is greater than zero, it adds the letter as the key and the `letter_frequency` as the value to the `result` hash.

I feel like this is probably more efficient than my `each` and `each_with_object` solutions since it only iterates through the `letters` array once whereas my solutions chain multiple iterations together.


# Additional Practice > Practice Problem 8


## Description of Problem

What happens when we modify an array while we are iterating over it?


## My Solution

The array changes after each iteration so the code will skip indexes:

If the first element is removed from `[1, 2, 3, 4]`, we're left with `[2, 3, 4]`. The second iteration will look for the element at index `1` and will see `3` instead of `2`. `2` will be skipped. Then, the `3` will be removed and the array will be `[2, 4]`. The next iteration will not execute because it thinks it has already iterated through every element of the array since there's no element at the `2` index.


## Other Observations

> In both cases we see that iterators DO NOT work on a copy of the original array or from stale meta-data (length) about the array. They operate on the original array in real time.


# Additional Practice > Practice Problem 9


## Description of Problem

Create a `titlize` method


## My Solution

I did this using `loop`, `each`, `each_with_object`, and `map`. `each_with_object` and `map` are both one-line solutions, with `map` being more succinct and easier to read.


## Other Observations

The recommended solution matches the `map` solution that I came up with.


# Additional Practice > Practice Problem 10


## Description of Problem

Given a hash that has keys and the values are other hashes, modify the hash to add another key/value pair to the value hashes.


## My Solution

I solved this using `loop`, `each`, and `each_value`.


## Other Observations

The recommended solution uses `each` and a `case` statement like I used in my `loop` solution. I used a `ternary` operator in my `each`, which admittedly makes it long and relatively difficult line to read.


# Exercises > Small Problems > Easy 6 > Combining Arrays


## Description of Exercise

Given two arrays, return an array that contains all of the elements of both arrays without duplicates.


## My Solution

I solved this in 11 minutes by using `concat` and `uniq`:

```ruby
first.concat(second).uniq
```


## Other Observations

The recommended solution used Set union, which makes the solution even shorter:

```ruby
array_1 | array_2
```

I wasn't aware of that method.

Some of the other submitted solutions were nice:


### Uses `+`

```ruby
(first + second).uniq
```

This is similar to my solution, but a little more succinct.


### Uses `flatten`

```ruby
[first, second].flatten(1).uniq
```

Or handling more than two arrays:

```ruby
def merge(*arrays)
  arrays.flatten(1).uniq
end
```
