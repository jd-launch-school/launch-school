---
layout: post
title: Tic Tac Toe Computer Offense and Rotation 2
excerpt_separator: <!--more-->
tags: launch_school ruby project small_problems medium_1 tic_tac_toe
---

For the Tic Tac Toe Bonus Features project, I added the computer's ability to choose a winning position instead of only blocking the player from winning. I also added the ability for the computer to choose the 5th position (center) position if that position is open instead of choosing a random position on their turn. I added a rudimentary way to set whether or not the player, computer, or the player chooses who goes first. It's at the point where I have to think about it in order to win and sometimes I lose because I'm not paying attention.

I streamed and recorded the Rotation (Part 2) small problem in the Medium 1 set. I was able to do the PEDAC process and come up with working code within 20 minutes.

<!--more-->


# Tic Tac Toe Bonus Features Project


## Computer Offense

At first, I duplicated the defense code entirely and instead of checking to see if `PLAYER_MARKER` occupied two spaces, I checked for `COMPUTER_MARKER`. This worked, but resulted in a lot of duplicated code. I then combined the two methods using a `marker` parameter to determine which player marker we're looking at:

```ruby
def computer_move(board, marker)
  WINNING_LINES.each do |line|
    values = line.map { |pos| board[pos] }
    if values.include?(INITIAL_MARKER) && values.count(marker) == 2
      next_pos = values.find_index(INITIAL_MARKER)
      return line[next_pos]
    end
  end

  nil
end
```

When I refactor this code, I'll likely break this method up into smaller methods. I'll probably have to do that with most of the code. I like to get the code working first, then I refactor and remove duplication as needed.

When the code gets to the point where it decides where to place the computer piece, it checks to see if the center position is empty. If it is, it chooses that space:

```ruby
def computer_places_piece!(board)
  win = computer_move(board, COMPUTER_MARKER)
  threat = computer_move(board, PLAYER_MARKER)

  if win
    board[win] = COMPUTER_MARKER
  elsif threat
    board[threat] = COMPUTER_MARKER
  elsif board[5] == INITIAL_MARKER
    board[5] = COMPUTER_MARKER
  else
    square = empty_squares(board).sample
    board[square] = COMPUTER_MARKER
  end
end
```


## Choose who goes first

This ended up adding a lot more code than I'm comfortable with. However, for now, it's OK. Per the instructions, I added a new constant named `FIRST_MOVE`. Then, in the first loop, before the board is created, I check that constant. If the constant is `:player`, then the player will choose first. If the constant is `:computer`, then the computer will go first. If the constant is `choose`, then the player is asked to choose who goes first. At the moment, this choice lasts for an entire round until either the player or computer gets 5 wins or the player quits and/or resets:

```ruby
if first_player.empty?
  case FIRST_MOVE
  when :player
    first_player = :player
  when :computer
    first_player = :computer
  when :choose
    system('clear')
    prompt 'Who goes first?'
    prompt '[ (c)omputer | (p)layer ]'

    if play_again?(%i(c computer p player)) == :c
      first_player = :computer
    else
      first_player = :player
    end
  end
end
```

This sets `first_player` and is used to determine the order of the moves:

```ruby
loop do
  display_board(board, score)

  if first_player == :player
    player_places_piece!(board)
    break if someone_won?(board) || board_full?(board)

    computer_places_piece!(board)
    break if someone_won?(board) || board_full?(board)
    else
    computer_places_piece!(board)
    break if someone_won?(board) || board_full?(board)

    display_board(board, score)

    player_places_piece!(board)
    break if someone_won?(board) || board_full?(board)
  end
end
```

More duplication, I know&#x2026;


## Renamed play<sub>again</sub>? method

I wanted to re-use the logic in the `play_again?` method, but for other choices, not just whether the player wants to play again. So, I renamed it to `player_choice?`. This lets me re-use this method in more places.


# Small Problems > Medium 1 > Rotation (Part 2)


## Description of Exercise

Write a method that can rotate the last `n` digits of a number. For example:

    rotate_rightmost_digits(735291, 1) == 735291
    rotate_rightmost_digits(735291, 2) == 735219
    rotate_rightmost_digits(735291, 3) == 735912
    rotate_rightmost_digits(735291, 4) == 732915
    rotate_rightmost_digits(735291, 5) == 752913
    rotate_rightmost_digits(735291, 6) == 352917


## My Solution

I correctly figured out that if given an integer that represents the number of digits to remove from a number, we can use the negative of that digit to get a slice of the number with the digits removed. For example:

```ruby
number = 12345
digits_to_remove = 3

p number.to_s[-digits_to_remove..-1]
```

    "345"

Using this, I was able to create a new array with the first part concatenated with the second part rotated, like this:

```ruby
number = 12345
digits_to_remove = 3
number_as_string = number.to_s

left = number.to_s[0...-digits_to_remove]
right = number.to_s[-digits_to_remove..-1]
rotated = right[1..-1] + right[0]

p (left + rotated).to_i
```

    12453


## Recommended Solution

The recommended solution did basically the same thing as what I did above here. In my solution, I used `Integer.digits` and worked with arrays instead of strings. The main part to understand is how to remove an `n` number of digits from a number, like I did above.


## Submitted Solutions

There were a couple of interesting solutions.


### Using Array#pop

```ruby
def rotate_rightmost_digits(integer, count)
  chars = integer.to_s.chars
  chars << rotate_array(chars.pop(count))
  chars.join.to_i
end
```

This solution converts to an array of characters as strings, pops the `count` number of digits off the string, then passes them to `rotate_array`, appends the rotated array back to the array of characters as strings, joins and converts back to an integer.


### Using String#slice!

```ruby
def rotate_rightmost_digits(number, n)
  number = number.to_s
  number << number.slice!(number[-n])
  number.to_i
end
```

This solution converts to a string, appends the sliced (destructive) digit at the negative `n` index to the end of the string and then converts it back into an integer.
