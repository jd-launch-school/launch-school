---
layout: post
title: Tic Tac Toe Scores and Rotation
excerpt_separator: <!--more-->
tags: launch_school small_problems medium_1 ruby project tic_tac_toe
---

I spent some time today fleshing out what's needed in order to create a scoring system for the Tic Tac Toe project. I also finished the first Medium problem in the small problems exercises.

<!--more-->


# Tic Tac Toe Scores

I outlined the problem and attempted to figure out when and where in the code I would need to have a `score` hash.

I borrowed some of my code from a previous project to build out the scoring system, including all of the methods needed to determine a grand winner, increment the score, and reset the score. Then, I updated the main loop to initialize a score hash with zero values for both the player and computer. I added functionality for the user to be able to quit the game early as well as decide whether or not to reset the score.

I'll go over this again when I'm reviewing the project for submission.


# Small Problems > Medium 1 > Rotation (Part 1)


## Description of Exercise

Write a method that rotates an array by moving the first element to the end of the array. The original array should not be modified.


## My Solution

This turned out to be relatively straightforward. Slice from the index `1` to the end `-1` and append the item at index `0` to that new array:

    array[1..-1] << array[0]


## Recommended Solution

The recommended solution was similar, but used concatenation instead:

    array[1..-1] + [array[0]]


## Further Exploration

We were tasked with rotating strings and integers. I rewrote the logic in each case, however, others used the `rotate_array` method we'd created above to do the rotating while massaging the string/integer into an array of characters/digits.


## Submitted Solutions

One submitted solution used `Array#drop` and `Array#take`:

    array.drop(1) + array.take(1)

This is short and sweet, but it's not entirely clear what `drop(1)` is doing unless one knows that `drop` creates a new array with the number of items given dropped from the new array. For example:

```ruby
p [1, 2, 3, 4, 5].drop(1)
p [1, 2, 3, 4, 5].drop(2)
```

    [2, 3, 4, 5]
    [3, 4, 5]

`take` does the opposite and creates a new array with the number of items given. For example:

```ruby
p [1, 2, 3, 4, 5].take(1)
p [1, 2, 3, 4, 5].take(2)
```

    [1]
    [1, 2]
