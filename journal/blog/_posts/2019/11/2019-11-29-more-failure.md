---
layout: post
title: More Failure, As A Process
excerpt_separator: <!--more-->
tags: launch_school assessments
---

Out of three study sessions with other Launch School students, I failed to successfully complete two verbal assessment questions, rooting me deeply into the process of failure towards mastery. My live coding experience, as little experience as it is, didn't adequately prepare me.

<!--more-->

Launch School encourages students to practice and study for the written and verbal assessments with fellow students. We do this via [Zoom](https://zoom.us) and [Repl.it](https://repl.it). So far, I've been in two types of study sessions. One, the other student and I practiced describing a piece of code in order to practice for the written assessment. This goes along with the code explanations I've been practicing, so I did pretty well with this.

The second type of study session is one in which we practice solving a coding challenge in a job interview-like setting. In this type of session, one student gives the other student a problem to solve and then helps them through the problem and/or offers suggestions to make aspects of the problem solving method better.

I attended two study sessions of the second type and I failed to solve the problem I was given both times.

In the book **"Mastery: The Keys to Success and Long-Term Fulfillment"** by George Leonard, he notes:

> Even without comparing ourselves to the world’s greatest, we set such high standards for ourselves that neither we nor anyone else could ever meet them—and nothing is more destructive to creativity than this. We fail to realize that mastery is not about perfection. It’s about a process, a journey. The master is the one who stays on the path day after day, year after year. The master is the one who is willing to try, and fail, and try again, for as long as he or she lives. (pg. 140)

Well, if failing and trying again is part of the path to mastery, then I can safely say that I'm starting to master the art of verbal coding. I've so far failed 100% of the verbal assessment practices that I've undertaken. I'm not sure if it's possible to fail more deeply than that!

I should say that I consider these attempts failures. However, with help, I was able to solve both problems, it just took longer than it probably should have. So, maybe they're failures in the sense that I didn't solve the problems on my own in a reasonable amount of time. I did solve them though.

The first problem involved selecting odd numbers from an array that are not also prime. I got hung up on figuring out which numbers were prime. For some reason, I couldn't figure out that calculating prime is a matter of iterating through all of the numbers from 1 up to the number in question and determining whether or not any of those numbers divided into the number in question evenly. So, I stumbled around that problem for a while, probably wasting the other students' time.

The second problem involved summing consecutive numbers of equal value. So, given the array `[1, 4, 4, 4, 2]`, the code would return another array with the sums, `[1, 12, 2]`. I got stuck on the looping process for this one and was fixated on checking the current number with the previous number. As soon as the Zoom call ended, I thought about it more and realized I could also check the current number with the next number, not just the previous number. That small realization lead me to the solution.

In both cases, the problems themselves were not too difficult. However, I wasn't able to figure out the solutions on my own while on the calls. This is an area that I need to practice more.

The other thing I need to practice is using web-based text/coding editors. For two of the practice sessions, I used [Coderpad](https://coderpad.io). The third session we used [Repl.it](https://repl.it). Since I'm so familiar and comfortable with Emacs, using these other editors felt clunky and slow to me. Each time I'd reach for an Emacs key chord to do something, I'd be reminded that I was out of my Emacs comfort zone. This tells me that I might be too reliant on Emacs to the point where it's becoming a crutch. Therefore, I also need to focus my practice on these web-based editors so that I can efficiently use the keyboard shortcuts available to me or, at the least, become efficient using only the basic shortcuts in my operating system and/or browser.

With the knowledge of my failures at hand, I continue onward. I continue despite the nagging sensation that failure is unacceptable. I continue even though I know I have many more failures ahead.

In the wise words of one of the students I studied with:

> &#x2026;you are on the right track. Success is a process, not an event.
