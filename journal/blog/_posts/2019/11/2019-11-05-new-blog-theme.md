---
layout: post
title: New Theme for Blog
excerpt_separator: <!--more-->
tags: jekyll yaml liquid
---

I came across another Jekyll blog using the default Minima theme today. It's a blog from someone who seemed to be passively-aggressively supporting sexism/racism/non-inclusive processes for the Emacs community. I don't want to have a blog that looks anything like theirs, so I spent some time finding a new theme.

<!--more-->

I'm not sure yet how to switch themes efficiently. Basically, I had to clone from GitHub and then start over, moving my markdown files from the old blog theme's `_posts` to the new theme's `_posts`.

The theme I'm using is called [Dash by @bitbrain](https://github.com/bitbrain). I had to make some tweaks in order for it to work with my content.

The first tweak I made is the social icons. This was the original code in `/_includes/header.html`:

```html
<span class="social_links">
    {% for link in site.dash.social_links %}<a class="color-{{ link.color }}-hover" href="{{ link.url }}"><i class="fab fa-{{ link.icon }}"></i></a>{% endfor %}
</span>
```

If you're looking closely, and it's difficult to do unless you're in my position with the need to add an RSS icon, all FontAwesome icons are prefixed with `fab`, for brands. This worked fine for my GitLab icon, but not the RSS icon.

So, I modified that code to this:

```html
<span class="social_links">
    {% for link in site.dash.social_links %}<a class="color-{{ link.color }}-hover" href="{{ link.url }}"><i class="{{ link.type }} fa-{{ link.icon }}"></i></a>{% endfor %}
</span>
```

I then added a new `type` attribute to the `social_links` section of the `_config.yml` file so it now looks like this:

```yaml
# generate social links in footer
# supported colors: green, red, orange, blue, cyan, pink, teal, yellow, indigo, purple
social_links:
  - url: https://jd-launch-school.gitlab.io/feed
    icon: rss
    color: red
    type: fas
  - url: https://gitlab.com/jd-launch-school
    icon: gitlab
    color: purple
    type: fab
```

Now, the user can determine which type of icon, whether a normal FontAwesome icon `fas` or a branded one `fab`.

I also had to add `{{ post.excerpt }}` to the `home.html` layout so it would show my post excerpts.

In doing that, I realized two things. One, the color of the date was too dark and barely readable in dark mode. Two, quotes were yellow and difficult to read in light mode. So, using the provided `colors.scss` and `themes.scss`, I changed the colors so they're more readable.

I'll continue to tweak the theme as needed and as I come across instances where tweaking is needed.
