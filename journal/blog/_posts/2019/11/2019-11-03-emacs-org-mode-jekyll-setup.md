---
layout: post
title: My Emacs Org-Mode to Jekyll GitLab Setup
excerpt_separator: <!--more-->
tags: jekyll emacs org-mode markdown gitlab
---

I'm successfully, albeit slowly, moving my [Emacs](https://www.gnu.org/software/emacs/) [org-mode](https://orgmode.org/) journal files over to a [Jekyll](https://jekyllrb.com/) blog so I can host it for free on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) and my notes can be found and read a little easier by more people. To do this, I needed to refresh my understanding of GitLab Pages and learn how Jekyll works in terms of converting org-mode files to markdown and learning how to move files in the GitLab CI to display the final result.

<!--more-->


# Part 1 - Set Up Ruby Environment Locally

I use [rbenv](https://github.com/rbenv/rbenv) for my Ruby version control system. I think I chose this because there was some problem on my system when I attempted to get newer versions of Ruby through [RVM](https://rvm.io/).

[rbenv-gemset](https://github.com/jf/rbenv-gemset) lets me create a project-specific `.gems` directory, sort of like [npm](https://www.npmjs.com/) projects have project specific `node_modules` directories. I already had this installed, so I only needed to set it up for my project:

```bash
cd [project dir]
rbenv gemset init
echo '.gems' > .rbenv-gemsets
```

I like to make sure it works, so I use the following to see where gems for this project will be installed:

```bash
gem env home
```

Once I'm sure rbenv-gemset is working correctly, I installed Jekyll:

```bash
gem install jekyll bundler
```

Then I created a new Jekyll project.

```bash
jekyll new [blog_directory]
```

To make sure all of that worked, I build the site for local development and launch a development server:

```bash
cd [blog_directory]
bundle exec jekyll serve
```

At this point, my Jekyll blog was up and running. I can view it at `http://localhost:4000`. This was the relatively easy part over the entire process.


# Part 2 - Convert Org-Mode to Markdown

I then started work on my process for converting org-mode files to markdown. To do this, I added the following code to my `init.org` file. I should note that I'm using `Emacs 26.3` in these instructions:

```elisp
(setq org-publish-project-alist
      '(

  ("org-todayilearned"
          ;; Path to your org files.
          :base-directory "~/Documents/ruby/launch_school/journal/org_files/"
          :base-extension "org"

          ;; Path to your Jekyll project.
          :publishing-directory "~/Documents/ruby/launch_school/journal/blog"
          :recursive t
          :publishing-function org-md-publish-to-md
          :headline-levels 4
          :html-extension "md"
          :body-only t ;; Only export section between <body> </body>
    )


    ("org-static-til"
          :base-directory "~/Documents/ruby/launch_school/journal/org_files/"
          :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|php"
          :publishing-directory "~/Documents/ruby/launch_school/journal/"
          :recursive t
          :publishing-function org-publish-attachment)

    ("til" :components ("org-todayilearned" "org-static-til"))

))
```

Here's a breakdown of the parts of the code that are unique to my setup:

`org-todayilearned`: this is a unique name for the function. I used the name of my blog, but it could be anything.

`:base-directory`: this path is where my org-mode files are located.

`:publishing-directory`: this path is where my markdown files will go.

`:publishing-function`: this is the function that converts from org-mode to markdown. Other functions can be used here, converting to HTML instead, for example.

`org-static-til`: this is a unique name for the function. I think it has something to do with static resources like images, CSS, JS, etc&#x2026;

`til`: stands for `Today I Learned`, so it's unique as well and can be anything.

Unfortunately, I haven't had time to master Emacs Lisp, so I don't know exactly what's happening with this code, but I know it's working for me as needed.

Once this is running within Emacs, I add the following header (front matter) to an org-mode file that I want to convert:

    #+begin_export html
    ---
    layout: post
    title: Post Title
    excerpt_separator: <!--more-->
    ---
    #+end_export
    
    Summary
    
    #+begin_export html
    <!--more-->
    #+end_export

`#+begin_export html` tells the converter to add the block as-is. In this case, it's the front matter that's needed for Jekyll processing.

The second `#+begin_export html` block is used to mark the part of the content that marks the end of the post excerpt.

From that org-mode file, I call `M-x` `org-publish-current-file`. This converts the org-mode to markdown and moves the markdown file into the `_posts` directory within the Jekyll file hierarchy. I started out using `M-x` `org-publish` and selecting `til`, but that converted every journal entry which not only took a long time to process every file, but didn't really work because most of my org-mode files haven't been prepared properly with the correct front matter for the conversion.

The org-mode file name has to be in the form `yyyy-mm-dd-post_name.org` in order for it to be converted properly and for Jekyll to recognize it.

To see if Jekyll is getting the markdown file it needs, I start the server (or refresh my browser page if it's already running) and see the post I just converted on the page.

It took me some fiddling around with the code in order to get this working. At first, the Emacs Lisp code snippet I borrowed for the org-mode to markdown conversion wasn't clear about which directories were which. Also, it used a different `:publishing-function`, one that I'm guessing has been phased out or is a part of another package I don't have. So, I had to figure out what the function was for my system. That code was using `HTML`, but I wanted markdown, so I needed to change the code so it works with markdown instead. Eventually, as I tried one thing, then another, I pieced the parts together in order to get a working process for writing in org-mode and publishing in markdown with Jekyll.


# Part 3 - Deploy to GitLab

Now for the fun part, that is, if pushing a bunch of commits, waiting, pushing more commits, waiting, and so on is fun for you.

My `.gitalb-ci.yml` file now looks like this:

```yaml
image: ruby:2.6

variables:
  JEKYLL_ENV: production
  LC_ALL: C.UTF-8

cache:
  paths:
  - vendor/

before_script:
  - cd ./journal/blog/
  - gem install bundler:2.0.2
  - bundle install --path ../../vendor

pages:
  stage: deploy
  script:
    - bundle exec jekyll build -d public
    - mv ./public ../../public
  artifacts:
    paths:
    - public
  only:
  - master

```

I'm not going to go too much into the details of this, except to note that I needed to tell the container to install `bundler` before I ran `bundle install` for some reason. I didn't see that in any of the documentation anywhere. The error message was clear, but I'm not sure why it was necessary.

Again, I'm not 100% sure what every bit of this YAML config file does. After running it 20 or so times, I have a good idea of what it's telling the container to do, but I don't know the details of how the container works and how it interacts with this file.

The only other thing I needed to change was my Jekyll `_config.yml` file. It currently looks like this:

```yaml
title: Today I Learned ...
email: jdenzin@eyedarts.net
description: >- # this means to ignore newlines until "baseurl:"
  This is a site that makes my Launch School blog notes more accessible.
baseurl: "/launch-school" # the subpath of your site, e.g. /blog
url: "https://jd-launch-school.gitlab.io" # the base hostname & protocol for your site, e.g. http://example.com
twitter_username: 
github_username: jd-launch-school
rss: rss

# Build settings
theme: minima
plugins:
  - jekyll-feed
show_excerpts: true
exclude:
  - vendor
```

For the deployment process, I think the tricky part for me was understanding where the files were at each stage of the deployment. My Jekyll blog files are nested relatively deep within my repository `/journal/blog`, rather than at the root of the project, so it took me some time to figure out where each part of the deployment was looking for the files it needed. Ultimately, it's not too complicated and it follows a similar path to the way I'd do it manually, but not having the ability to stop the process in the middle and look around made it take longer.

Deployment also took longer until I figured out how to cache the build files so they didn't need to be re-installed each time.


# Post Creation and Publishing Process

Now that everything is set up, my process is as follows:

1.  Write a post in org-mode with a front matter block at the top and file name in the `yyyy-mm-dd-post_name.org` format.
2.  Call `M-x` `org-publish-current-file` on that file.
3.  Turn on Jekyll server.
4.  Check to make sure the post is formatted correctly and fix other errors.
5.  Edit post in org-mode and re-publish if needed.
6.  Add, commit, and push to GitLab, using [Magit](https://magit.vc) of course. ;)

I'm sure some of that can be automated in some way and I'll continue to improve the process as I use it. However, for now, this works well and I'm happy with the result.


# Resources

Here are some resources that helped me figure this out:

-   [Using org to Blog with Jekyll](https://orgmode.org/worg/org-tutorials/org-jekyll.html)
-   [Publishing Org-mode files to HTML](https://orgmode.org/worg/org-tutorials/org-publish-html-tutorial.html)
-   [Creating and Tweaking GitLab CI/CD for GitLab Pages](https://gitlab.com/help/user/project/pages/getting_started_part_four.md)
-   [Example Jekyll site using GitLab Pages](https://gitlab.com/pages/jekyll)
