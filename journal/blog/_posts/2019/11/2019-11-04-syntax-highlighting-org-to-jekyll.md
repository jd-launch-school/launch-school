---
layout: post
title: Org-Mode Conversion to Markdown - Add Syntax Highilghting
excerpt_separator: <!--more-->
tags: jekyll emacs org-mode
---

After setting up my blog to write in org-mode, export to markdown, and publish as a Jekyll blog, I realized that I didn't have syntax highlighting. So, I did some more searching online and found a nice solution.

<!--more-->

If you haven't seen the [first part of the setup](https://jd-launch-school.gitlab.io/launch-school/2019/11/03/emacs-org-mode-jekyll-setup.html), you'll want to look there before reading further.

I started searching to see if there's a quick tweak I could make to the org-to-markdown conversion process that would enable syntax highlighting. The first page I looked at was an [Emacs Stack Exchange post](https://emacs.stackexchange.com/questions/42471/how-to-export-markdown-from-org-mode-with-syntax) that lead me to [ox-gfm](https://github.com/larstvei/ox-gfm), a GitHub flavored markdown exporter for org-mode.

I'm using [use-package](https://jwiegley.github.io/use-package/) in my Emacs `init.org` file, so I first installed ox-gfm like this:

```elisp
(use-package ox-gfm
  :after (org)
 )
```

Using `M-x org-gfm-export-to-markdown`, I converted an org-mode file to markdown to test, and it worked well, adding the `ruby` at the start of the code block to be highlighted. From there, I thought it would be a matter of changing the publishing function in the `org-publish-project-alist` code I wrote earlier, like so:

```elisp
(setq org-publish-project-alist
      '(

  ("org-todayilearned"
          ...
          :publishing-function org-md-publish-to-md
          ;; Change this to org-gfm-export-to-markdown
          ...
    )

  ...

))

```

That didn't work. :) I realized after a few moments of trial-and-error that I needed a publish function.

With a little more searching, I found a nice publish function from [Blogging with Emacs, Org-Mode, and Hugo](https://vurt.co.uk/post/blogging-with-emacs-and-hugo/), where Giles Paterson helpfully includes a publish function that can be used with ox-gfm:

```elisp
(defun gp-org-gfm-publish-to-md (plist filename pub-dir)
  "Publish an org file to Github Flavoured Markdown.

FILENAME is the filename of the Org file to be published.  PLIST
is the property list for the given project.  PUB-DIR is the
publishing directory.

Return output file name."
  (org-publish-org-to 'gfm filename ".md" plist pub-dir))
```

It's pretty simple, but it's nice to not have to think it through myself since I really want to focus on Launch School and Ruby.

With that in place, I changed the publishing function above to `gp-org-gfm-publish-to-md` and everything worked nicely.
