---
layout: post
title: Win or Block Method Rewrite
excerpt_separator: <!--more-->
tags: launch_school ruby project
---

I took some time to rewrite the `win_or_block` method into smaller methods that each do one thing.

<!--more-->

As I learned from writing a bunch of code explanations, there's value in methods that do only one thing as the explanation is shorter and simpler. So, seeing that I had a large explanation for the `win_or_block` code, and coming across the [methods should do one thing](https://github.com/uohzxela/clean-code-ruby#methods-should-do-one-thing) section of the [Clean Code Ruby GitHub repository](https://github.com/uohzxela/clean-code-ruby), I decided to rewrite the method and break it into smaller methods if possible.

> This is by far the most important rule in software engineering. When methods do more than one thing, they are harder to compose, test, and reason about. When you can isolate a method to just one action, they can be refactored easily and your code will read much cleaner. If you take nothing else away from this guide other than this, you'll be ahead of many developers.

My `win_or_block` method originally looked like this:

```ruby
# :reek:FeatureEnvy
def win_or_block(board, marker)
  WINNING_LINES.each do |line|
    values = position_values(board, line)

    if values.include?(INITIAL_MARKER) && values.count(marker) == 2
      empty_square_index = values.find_index(INITIAL_MARKER)
      return line[empty_square_index]
    end
  end

  nil
end
```

Even though that's not too many lines, it's doing a bunch of things all in one method. Also, it is pretty close to the [example in the Clean Code Ruby Repo](https://github.com/uohzxela/clean-code-ruby#methods-should-do-one-thing).

I rewrote the logic as the following four methods:

```ruby
def two_markers_in_line?(line_values, marker)
  line_values.include?(INITIAL_MARKER) && line_values.count(marker) == 2
end
```

```ruby
def empty_square_index(line_values)
  line_values.find_index(INITIAL_MARKER)
end
```

```ruby
def potential_winning_line(board, marker)
  WINNING_LINES.find do |line|
    two_markers_in_line?(position_values(board, line), marker)
  end
end
```

```ruby
def best_move(board, marker)
  potential_win = potential_winning_line(board, marker)

  if potential_win
    line_values = position_values(board, potential_win)
    potential_win[empty_square_index(line_values)]
  end
end
```

These four methods bring out into the open what was previously happening within the method. Not only that, they also potentially reduce future repeated code as some of the four methods may be able to be used again by other methods.

It took a while to get to this point. I first had to isolate the different parts of the `win_or_block` method, then think about names for each part, then refactor the methods so they're more efficient and concise. Having tests helped so that I could quickly see if changes to something affected the return values.
