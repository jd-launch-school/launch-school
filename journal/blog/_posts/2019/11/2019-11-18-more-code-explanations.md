---
layout: post
title: More Code Explanations
excerpt_separator: <!--more-->
tags: launch_school ruby code_explanation project
---

Today, I worked mostly on practicing code explanations. This time, along with practicing code explanations, I started to include explanations for the code in my Tic Tac Toe project.

<!--more-->

It can take me a while to write out the explanations for what seems like relatively simple code. I'm still getting used to using the language correctly. Here's an example of one of the methods I explained today:

```ruby
def halvsies(array)
  middle = (array.size / 2.0).ceil
  first_half = array[0..middle - 1]
  second_half = array[middle..-1]
  [first_half, second_half]
end
```

> On `line 1`, we define the method `halvsies` with a parameter `array`.
> 
> On `line 2`, the local variable `array` calls `Array#size`. This returns the number of elements in `array`. We then divide that return value by the float `2.0`. The float that's returned from that division then calls `ceil`. This returns the smallest number greater than or equal to the float that's returned. This will return an integer. Finally, we initialize the local variable `middle` and assign to it the return value of the `ceil` method call.
> 
> On `line 3`, the local variable `array` is sliced from the first element up to and including one integer less than the integer value referenced by `middle`. This creates a new array containing only the elements between `0` and `middle - 1`. We initialize the local variable `first_half` and assign to it the new array created by the slice.
> 
> On `line 4`, the local variable `array` is sliced from the value referenced by the local variable `middle` up to and including the last element in `array`. This creates a new array containing only the elements between `middle` and `-1` (the last element in the array). We initialize the local variable `second_half` and assign to it the new array created by the slice.
> 
> On `line 5`, we create a new array with the local variable `first_half` as the first element and the local variable `second_half` as the second element. Since this is the last line evaluated within the method, the return value of the method is this new array, which contains two sub-arrays `[array[0..middle -1], array[middle..-1]]`.

In terms of the Tic Tac Toe project, I added explanations for the first four sections of the code. Here's an example of the `clear_screen` method's explanation:

```ruby
def clear_screen
  system('clear') || system('cls')
end
```

> On `line 1`, we define a method named `clear_screen` with no parameters.
> 
> On `line 2`, we determine if `Kernel#system` evaluates to `true` when passed the argument `'clear'`.
> 
> If `system` returns `true` and therefore the return value evaluates to `true`, the OR `||` comparison operator is short-circuited and the `clear` command is executed in the shell. The screen is cleared.
> 
> If `system` returns `false` or `nil` and therefore evaluates to `false`, the other side of the OR `||` comparison operator is evaluated, checking to see if `system` returns `true` when passed `'cls'` as an argument.
> 
> If either one of the `system` calls evaluates to `true`, the screen is cleared and the method will return `true`. If either one evaluates to `false`, the screen is not cleared and the method will return `false`.

The more of these I write, the better I get at writing them. Unfortunately, I can't get feedback from Launch School on the accuracy of these explanations because they explicitly state that they can't help with that.
