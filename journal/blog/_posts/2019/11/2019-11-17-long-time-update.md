---
layout: post
title: Long Time Update, Unbeatable Tic Tac Toe Algorithm No More
excerpt_separator: <!--more-->
tags: launch_school ruby project tic_tac_toe
---

It's been a while since I've written here. Far longer than I prefer. However, I ran into some trouble with my Tic Tac Toe minimax algorithm and that set me back a few days. I then focused on some exercises and test prep.

<!--more-->

Well, there's not much to say except that I failed in grasping the recursive code required to implement the minimax algorithm. In my previous post, I noted that I'd come up with a hybrid algorithm that seemed to do the trick. Yes, it plays better than the computer choosing random squares, however, it didn't play perfectly.

I realized that I needed to dig deeper into the minimax algorithm proper in order to figure it out.

I think I understand the minimax algorithm on paper, that is, theoretically. I got hung up on how to implement it in code. The minimax part of the code seemed to work, but I couldn't figure out how to get it to return the next best move for the computer. I was able to see the `10`, `-10`, and `0` values as the code moved along, but I couldn't translate that into choosing the next move. So, sadly, I felt that I'd spent enough time working it out, failed, and decided I needed to move on and come back to this later when I've worked more with recursion.

After that, I reverted changes and cleaned up the Tic Tac Toe code. I'm currently in the process of comparing the code to the reviews of other students in order to catch things I may have left out or overlooked. I will be submitting it for review shortly.

I'm disappointed about how long it took me to complete this assignment, but I'm moving on in hopes that more practice will push me in the right direction.

Today, I spent most of my time writing out the evaluation of various code snippets in as detailed language as I can. For example:

```ruby
a = 1
```

Instead of writing something like:

> "`a` is equal to `1`."

I practiced writing the following:

> "We initialize the local variable `a` and assign to it an integer with value `1`."

Writing out the code like this gets more complicated and detailed as the code gets more complicated. I'm finding this to be challenging but also rewarding as I can see that I'll be able to discuss code better with others as a result of practicing this more.

I used to talk vaguely to others, like this method does this and that, etc&#x2026; I remember thinking that I didn't need to know the details while I was talking to someone because I could look it up in the docs or Stack Overflow if I needed to truly understand it. However, I didn't realize that I may have been judged by those I was talking with as not deeply understanding the code in question.

Ultimately, I'm appreciating the challenge to come up with descriptive and meaningful words for the Ruby code I and others write. Challenging and tedious as it may be, I have a feeling that I'll come out of the experience and practice in a better place.
