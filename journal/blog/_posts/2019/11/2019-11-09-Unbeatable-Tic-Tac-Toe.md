---
layout: post
title: Unbeatable Tic Tac Toe AI
excerpt_separator: <!--more-->
tags: launch_school ruby project tic_tac_toe
---

Over the past two days, I worked on deeply understanding Tic Tac Toe in order to create an unbeatable AI. I think I've accomplished this, although I can't be sure as I don't know how to test it comprehensively. I'm also not sure how my solution will hold up to larger boards and more players (Hint: I don't think it will).

> "Why not break the problem down into tiny parts and build up from there?"

<!--more-->

It all started with an `if/else` statement that tried to determine whether there's a threat, win, or whether the middle space (position 5) is open:

```ruby
if win
  board[win] = COMPUTER_MARKER
elsif threat
  board[threat] = COMPUTER_MARKER
elsif board[5] == INITIAL_MARKER
  board[5] = COMPUTER_MARKER
else
  square = empty_squares(board).sample
  board[square] = COMPUTER_MARKER
end
```

That worked for many cases, but it didn't have any offense other than to win when the computer had two in a row on the computer's turn. At this point, the assignment mentioned using the [minimax algorithm](https://en.wikipedia.org/wiki/Minimax). Looking at that Wikipedia page wasn't too helpful, so I did a little more searching on Wikipedia about Tic Tac Toe in general and found this about [Tic Tac Toe strategy](https://en.wikipedia.org/wiki/Tic-tac-toe#Strategy):

> 1.  **Win**: If the player has two in a row, they can place a third to get three in a row.
> 
> 2.  **Block**: If the opponent has two in a row, the player must play the third themselves to block the opponent.
> 
> 3.  **Fork**: Create an opportunity where the player has two ways to win (two non-blocked lines of 2).
> 
> 4.  **Blocking an opponent's fork**: If there is only one possible fork for the opponent, the player should block it. Otherwise, the player should block all forks in any way that simultaneously allows them to create two in a row. Otherwise, the player should create a two in a row to force the opponent into defending, as long as it doesn't result in them creating a fork. For example, if "X" has two opposite corners and "O" has the center, "O" must not play a corner in order to win. (Playing a corner in this scenario creates a fork for "X" to win.)
> 
> 5.  **Center**: A player marks the center. (If it is the first move of the game, playing on a corner gives the second player more opportunities to make a mistake and may therefore be the better choice; however, it makes no difference between perfect players.)
> 
> 6.  **Opposite corner**: If the opponent is in the corner, the player plays the opposite corner.
> 
> 7.  **Empty corner**: The player plays in a corner square.
> 
> 8.  **Empty side**: The player plays in a middle square on any of the 4 sides.

Hmm, that seems like a list of conditionals to me. So, adding what I didn't already have to my current `if` statement, I eventually got to this:

```ruby
if win
  board[win] = COMPUTER_MARKER
elsif threat
  board[threat] = COMPUTER_MARKER
# elsif create_fork
  # create two ways to win (two non-blocked lines of 2)
elsif board[5] == INITIAL_MARKER
  board[5] = COMPUTER_MARKER
elsif opposite
  board[opposite] = COMPUTER_MARKER
elsif corner
  board[corner] = COMPUTER_MARKER
else
  square = empty_squares(board).sample
  board[square] = COMPUTER_MARKER
end
```

The `opposite` variable mentioned there refers to the return value of an `opposite_corner` method that checks the diagonal winning lines for player markers in any of the corners of the board. The `corner` variable refers to the return value of an `empty_corner` method that checks for an empty corner and returns that space if there is one.

This covered many cases, however, there was still one main, important part of the computer's offense that I didn't solve, forking.

I tried to solve this by checking for some situations and placing the computer marker in certain places to create a win situation on its next turn.

```ruby
def create_fork(board)
  return 4 if (board[2] == PLAYER_MARKER) &&
              (board[5] == COMPUTER_MARKER) && (board[4] == INITIAL_MARKER && board[6] == INITIAL_MARKER)
  return 2 if (board[4] == PLAYER_MARKER) &&
              (board[5] == COMPUTER_MARKER) && (board[2] == INITIAL_MARKER && board[8] == INITIAL_MARKER)
  return 6 if (board[8] == PLAYER_MARKER) &&
              (board[5] == COMPUTER_MARKER) && (board[4] == INITIAL_MARKER && board[6] == INITIAL_MARKER)
  return 8 if (board[6] == PLAYER_MARKER) &&
              (board[5] == COMPUTER_MARKER) && (board[2] == INITIAL_MARKER && board[8] == INITIAL_MARKER)
  nil
end
```

However, I eventually ran into problems because I realized that there may be many situations that I'm not accounting for and therefore, the computer will not play perfectly. Also, this code was getting unwieldy and I became increasingly dissatisfied with it.

So, back to the drawing board, as they say, and in my case, back to the [Emacs scratch buffer](https://www.gnu.org/software/emacs/manual/html_node/emacs/Buffers.html). I read a couple of articles (not looking at the code) to understand the minimax algorithm a little more. It helped a little. I began to understand that the algorithm tracks whether a win condition was met and for whom. It assigns values to this win (and loss) condition and then uses those values to determine the best possible move. Yet, I didn't understand how I was going to use this knowledge in code.

It seemed insurmountable at the time. I said, *"Should I focus on this and potentially take up a month's worth of time or move on without knowing and understanding how to make an unbeatable Tic Tac Toe AI?"* I mean, that's the pinnacle of every programmer's learning, right? :)

I decided to sleep on it, applying some of what I learned from Barbara Oakley. At some point throughout the night, I woke up and thought to myself, *"Why not break the problem down into tiny parts and build up from there?"* It turned out to be a good thought.

The next morning, I got to work on truly understanding what I needed to do to create code that could simulate moves so that the computer can pick the best move. I have some [notes showing my process here](https://gitlab.com/jd-launch-school/launch-school/blob/0bba9914d209307fe2efb4e2804be79046e11f2d/rb101_programming_foundations/lesson_6/tictactoe_bonus_features_unbeatable_notes.org).

I started with testing things manually on an almost complete board in a way that I could fully understand. It looked something like this:

```ruby
INITIAL_MARKER   = ' '
PLAYER_MARKER    = 'X'
COMPUTER_MARKER  = 'O'
WINNING_LINES    = [[1, 2, 3], [4, 5, 6], [7, 8, 9],
                   [1, 4, 7], [2, 5, 8], [3, 6, 9],
                   [1, 5, 9], [3, 5, 7]]

board = { 1 => "X", 2 => "O", 3 => "X", 4 => " ", 5 => "O", 6 => " ", 7 => " ", 8 => "X", 9 => "O" }
result = []
empty_squares =  board.keys.select { |num| board[num] == INITIAL_MARKER }

def detect_winner(board)
  WINNING_LINES.each do |line|
    values = line.map { |pos| board[pos] }
    return 'Player' if values.all?(PLAYER_MARKER)
    return 'Computer' if values.all?(COMPUTER_MARKER)
  end

  nil
end

board[empty_squares[0]] = COMPUTER_MARKER
result << detect_winner(board)

board[empty_squares[1]] = PLAYER_MARKER
result << detect_winner(board)

board[empty_squares[2]] = COMPUTER_MARKER
result << detect_winner(board)

p result
```

    [nil, nil, nil]

This first test showed me that there was not a win at any point in the sequence of moves `computer: 4, player: 6, computer: 7`. I had a lot of duplicated code at first, re-initializing the board to the state I wanted it in, placing the markers, etc&#x2026; Eventually, I refactored into something a little better that uses a `simulate` method:

```ruby
INITIAL_MARKER   = ' '
PLAYER_MARKER    = 'X'
COMPUTER_MARKER  = 'O'
WINNING_LINES    = [[1, 2, 3], [4, 5, 6], [7, 8, 9],
                   [1, 4, 7], [2, 5, 8], [3, 6, 9],
                   [1, 5, 9], [3, 5, 7]]

def detect_winner(board)
  WINNING_LINES.each do |line|
    values = line.map { |pos| board[pos] }
    return -1 if values.all?(PLAYER_MARKER)
    return 1 if values.all?(COMPUTER_MARKER)
  end

  0
end

grand_result = []

def simulate(board, array)
  empty_squares =  board.keys.select { |num| board[num] == INITIAL_MARKER }
  result = []
  option_result = []

  board[empty_squares[array[0]]] = COMPUTER_MARKER
  result << detect_winner(board)

  board[empty_squares[array[1]]] = PLAYER_MARKER
  result << detect_winner(board)

  board[empty_squares[array[2]]] = COMPUTER_MARKER
  result << detect_winner(board)

  result.sum
end

# Possible outcomes for computer choosing position 4
# First possibility (Player chooses 6)
board = { 1 => "X", 2 => "O", 3 => "X", 4 => " ", 5 => "O", 6 => " ", 7 => " ", 8 => "X", 9 => "O" }
option_result = []
option_result << simulate(board, [0, 1, 2])

# Second possibility (Player chooses 7)
board = { 1 => "X", 2 => "O", 3 => "X", 4 => " ", 5 => "O", 6 => " ", 7 => " ", 8 => "X", 9 => "O" }
option_result << simulate(board, [0, 2, 1])
p option_result
grand_result << option_result.sum

# Possible outcomes for computer choosing position 6
# First possibility (Player chooses 4)
board = { 1 => "X", 2 => "O", 3 => "X", 4 => " ", 5 => "O", 6 => " ", 7 => " ", 8 => "X", 9 => "O" }
option_result = []
option_result << simulate(board, [1, 0, 2])

# Second possibility (Player chooses 7)
board = { 1 => "X", 2 => "O", 3 => "X", 4 => " ", 5 => "O", 6 => " ", 7 => " ", 8 => "X", 9 => "O" }
option_result << simulate(board, [1, 2, 0])
p option_result
grand_result << option_result.sum

# Possible outcomes for computer choosing position 7
# First possibility (Player chooses 4)
board = { 1 => "X", 2 => "O", 3 => "X", 4 => " ", 5 => "O", 6 => " ", 7 => " ", 8 => "X", 9 => "O" }
option_result = []
option_result << simulate(board, [2, 0, 1])

# Second possibility (Player chooses 6)
board = { 1 => "X", 2 => "O", 3 => "X", 4 => " ", 5 => "O", 6 => " ", 7 => " ", 8 => "X", 9 => "O" }
option_result << simulate(board, [2, 1, 0])
grand_result << option_result.sum
p option_result

p grand_result
```

    [0, 1]
    [0, 1]
    [0, 0]
    [1, 1, 0]

This still has a lot of repetition, but it's helpful because I can see what's happening at each stage of the process. The result is an array with the sum of wins/losses/ties for each empty space on the board. In this case, the computer should choose either position `4` or `6` as there's a slightly higher chance of winning with those two positions.

In terms of the `simulate` method, I realized that I could use a parameter to determine how many moves to simulate and the `empty_spaces` parameter could be any combination/permutation of empty spaces.

Also, looking at the repetition, I realized that I need to test every combination/permutation of empty spaces. So, I used `each` and `permutation` to come up with the following:

```ruby
INITIAL_MARKER   = ' '
PLAYER_MARKER    = 'X'
COMPUTER_MARKER  = 'O'
WINNING_LINES    = [[1, 2, 3], [4, 5, 6], [7, 8, 9],
                   [1, 4, 7], [2, 5, 8], [3, 6, 9],
                   [1, 5, 9], [3, 5, 7]]

def detect_winner(board)
  WINNING_LINES.each do |line|
    values = line.map { |pos| board[pos] }
    return -1 if values.all?(PLAYER_MARKER)
    return 1 if values.all?(COMPUTER_MARKER)
  end

  0
end

def simulate(board, empty_squares, array)
  result = []
  option_result = []
  current_player = COMPUTER_MARKER

  array.each do |square|
    board[empty_squares[square]] = current_player
    result << detect_winner(board)
    current_player = current_player == COMPUTER_MARKER ? PLAYER_MARKER : COMPUTER_MARKER
  end

  result.sum
end

board = { 1 => "X", 2 => " ", 3 => "X", 4 => " ", 5 => "O", 6 => " ", 7 => " ", 8 => " ", 9 => "O" }
empty_squares =  board.keys.select { |num| board[num] == INITIAL_MARKER }
option_result = {}

empty_squares.permutation.to_a.each do |permutation|
    board = { 1 => "X", 2 => " ", 3 => "X", 4 => " ", 5 => "O", 6 => " ", 7 => " ", 8 => " ", 9 => "O" }
    simulation = simulate(board, permutation, 0...permutation.size)

    if option_result[permutation[0]]
      option_result[permutation[0]] += simulation
    else
      option_result[permutation[0]] = simulation
    end
end

option_result
choice = option_result.key(option_result.values.max)

p option_result
puts "Computer should choose position #{choice}."
```

    {2=>14, 4=>-22, 6=>-34, 7=>-26, 8=>-20}
    Computer should choose position 2.

This sets a test `board` with five empty spaces. It creates an array of permutations of those five empty spaces:

```ruby
[[2, 4, 6, 7, 8], [2, 4, 6, 8, 7], [2, 4, 7, 6, 8], [2, 4, 7, 8, 6], [2, 4, 8, 6, 7], [2, 4, 8, 7, 6], [2, 6, 4, 7, 8], [2, 6, 4, 8, 7], [2, 6, 7, 4, 8], [2, 6, 7, 8, 4], [2, 6, 8, 4, 7], [2, 6, 8, 7, 4], [2, 7, 4, 6, 8], [2, 7, 4, 8, 6], [2, 7, 6, 4, 8], [2, 7, 6, 8, 4], [2, 7, 8, 4, 6], ...]
```

It then calls `each` on that array so as to evaluate the block of code for each permutation. In the block, for each iteration, the board is initialized to its original state. `simulation` is called using the `board`, the `permutation`, and a range from zero up to but not including the size of the permutation, in this case `0` to `4`. This will run through the next five moves for each permutation.

The `if` statement within the block groups the results by the first number of the permutation. It adds the current result to the previous result, if there is one. The end result is a rough total of the number of wins for each empty space:

```ruby
: {2=>14, 4=>-22, 6=>-34, 7=>-26, 8=>-20}
```

This tells the computer that since 4, 6, 7, 8 are negative and 2 is positive, it's more likely to win if it chooses position 2.

I tested this with other board states, states that I recognized the best move for, and the algorithm seemed to pick the best choice in all of those cases.

I don't think this is the minimax algorithm proper, as it returns duplicate results leading the computer to choose the first max result instead of the most strategic result in those cases. Also, it checks each permutation and doesn't stop when it hits a win, so there's a lot of inefficiency, I think. However, minimax or not, I understand fully what's happening and this combined with the following method makes the AI unbeatable as far as I can tell at the moment:

```ruby
def computer_places_piece!(board)
  threat = win_or_threat(board, PLAYER_MARKER)
  win = win_or_threat(board, COMPUTER_MARKER)

  return 5 if board[5] == INITIAL_MARKER
  return win if win
  return threat if threat
  return 1 if board[5] == PLAYER_MARKER
  return simulate_all_moves(board)
end
```

There's probably a lot of explanation that I'm leaving out here. However, this is the gist of what I went through to figure it out. It seems to work. I can't beat it. Others that play it can't beat it. Is it the most elegant and efficient solution? No, definitely not. It's a solution I created and understand so that makes it valuable to me.
