---
layout: post
title: Realization About Method Length
excerpt_separator: <!--more-->
tags: launch_school ruby
---

While writing some more code explanations for my Tic Tac Toe assignment, I noticed something interesting about the length of various methods.

<!--more-->

When I came across a method that was short, maybe one-line, I felt a sense of ease and relief. On the other hand, when I came across a method that was longer, say five to seven lines, I felt a sense of dread and dismay.

I think the reason for this is that it's much easier to explain the shorter methods. There's less code, less complication, and generally less to explain.

With the longer methods, on the other hand, it often appeared that there's much more going on within the method and it felt as though the explanation is going to take much longer to write. It also felt like I'll have to take more time to wrap my head around the method and fully understand what's happening.

To be fair, some longer methods in my version of the Tic Tac Toe assignment are made up of `puts` method calls, so those parts aren't too complicated.

However, I think I'm beginning to better understand why people prefer to write small, concise methods that do only one thing. Even if one isn't writing out the explanation, they need to be holding the explanation in their mind as they evaluate the method. This is much easier to do when a method is small and only doing one thing.

Interestingly, the sense of dread often happens when I see that I've told reek to ignore a `TooManyStatements` warning, which looks for methods that are longer than `6` lines. I'm still working out how best to fix methods that have this warning.
