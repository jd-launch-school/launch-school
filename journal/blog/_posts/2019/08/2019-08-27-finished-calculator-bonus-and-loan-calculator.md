---
layout: post
title: Finished Calculator and Loan Calculator Bonus Features
excerpt_separator: <!--more-->
tags: launch_school project ruby
---

After a week or so of work, I finished and submitted my Calculator Bonus Features and Loan Calculator assignment for review. What at first seemed like a relatively straightforward task turned out to be something much more. I used past reviews of other projects to guide me towards aspects of the program reviewers might be concerned with.

<!--more-->


# Finished Calculator Bonus Features and Loan Calculator

These two programs took some time for me to get right. I wanted to create as comprehensive code as possible for this stage of my understanding. I noticed that each time I thought I was finished, I'd look at other code reviews and realize something I hadn't considered. Sometimes, implementing that new thing took some time and refactoring.

For the Calculator Bonus Features, I tried not to have Utility Functions since Reek complained about them. I let that go for the Loan Calculator. I think this particular code smell applies more to an object-oriented approach as opposed to the procedural/functional approach I'm applying at the moment. I'm not sure about that though.

I enjoyed working with org-mode on these programs and like the testing and PEDAC process that I'm learning.


# Small Problems > Easy 2 > How Big is the Room?

I decided to use an `area` function. The recommended solution didn't use a separate function, but did use a constant variable for the square meters to square feet conversion number. I like that and implemented that into the further exploration part where I converted square feet into square inches and square centimeters.


# Small Problems > Easy 2 > Tip Calculator

I didn't use a separate function for this one, but I did add looping for minor validation checking. The validation checking is nowhere near as comprehensive as the Calculator Bonus Features and Loan Calculator. If I do these exercises again, maybe I'll implement more robust validation.

The only difference between my solution and the recommended solution was the placement of the tip conversion from an integer into a decimal (`tip / 100`). I chose to do this chained to the `gets.chomp` statement. The recommended solution did this during the tip calculation. I imagine doing this later in the process allows for more flexibility early on as needed.

I had to use `Kernel#format` for the further exploration. This can be done like this to ensure there are always two decimal places present:

```ruby
puts format('%.2f', 20)
```

    20.00
