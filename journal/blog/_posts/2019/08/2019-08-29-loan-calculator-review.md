---
layout: post
title: Loan Calculator Review Refactor
excerpt_separator: <!--more-->
tags: launch_school project ruby
---

I worked on implementing the feedback for my Loan Calculator assignment. So far, out of the two assignments I've had reviewed, I find the code reviews from Launch School to be very high quality in comparison to other learning Internet-based resources I've used in the past.

<!--more-->


# Refactor `retrieve_loan_duration`

One of the issues mentioned was the complexity of my `retrieve_loan_duration` method. It used to look like this:

```ruby
def retrieve_loan_duration
  loop do
    loan_duration = gets.chomp

    if valid_duration?(loan_duration)
      duration, label = parse_duration(loan_duration)
      duration_as_integer = duration.to_i
      years = label == "y"

      if years && duration_as_integer > 30
        prompt('invalid_loan_duration_years')
        prompt('loan_duration')
      else
        return duration_as_integer * 12 if years

        return duration_as_integer
      end
    else
      prompt('invalid_loan_duration')
    end
  end
end
```

I refactored that method into the following:

```ruby
def duration_as_months(duration)
  duration.to_i * YR_TO_MONTH
end
```

```ruby
def retrieve_loan_duration
  loop do
    loan_duration = gets.chomp

    if valid_duration?(loan_duration)
      duration, label = parse_duration(loan_duration)

      return duration.to_i if label == 'm'

      return duration_as_months(duration) if valid_year?(duration)

      prompt('invalid_loan_duration_years')
      prompt('loan_duration')

    else
      prompt('invalid_loan_duration')
    end
  end
end
```

I think the refactored code is better, but it can still be improved. I'm not yet sure how to do this. Something feels off with the way I'm doing this because I'm having trouble moving code from this method into smaller methods. I was at least I able to remove the nested `if/else/end`, so that's good.


# Instance Variable or Global Variable

Another issue brought to my attention is the use of an instance variable when not using an explicit object-oriented approach.

I decided to use an instance variable here because I didn't want to have to add the language to each `prompt` method call. I didn't want to use a global variable because Rubocop gives me a `Style/GlobalVars: Do not introduce global variables.` offense.

If I'm understanding correctly, instance variables are scoped to an instance of a class. In this case, what I might consider to be global scope for my program (i.e. the parent scope to all of my methods) is actually the scope of an instance of the `Object` class.

I think in this program, I could use either the instance variable `@language` or global variable `$language` since there's only one class.

I could be wrong about this and I'm sure there's more nuance here that I'm leaving out.


# Take Time Off

While waiting for the review, I worked on other code. When I returned to this code two days later, I was able to solve some of the problems that I initially thought might take me too much time and energy. From this I learned that I need to get the code to a point where I can submit it, take a day or two off from looking at it, and revisit the code with fresh eyes. I'll try that next time and see if it helps.
