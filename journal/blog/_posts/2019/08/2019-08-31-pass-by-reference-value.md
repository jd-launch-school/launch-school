---
layout: post
title: Pass By Value, Pass By Reference, Pass By Reference Value
excerpt_separator: <!--more-->
tags: launch_school ruby
---

Is Ruby pass by value? Is it pass by reference? It appears to be both in some cases. It’s more accurate to describe Ruby as pass by reference value.

> When an operation within the method mutates the caller, it will affect the original object.

<!--more-->


# Pass by Value

Pass by value object passing strategy is when a copy of the object is passed to the method. In Ruby, at least as far as I know at this point, this happens with immutable objects:

```ruby
def add_one(n)
  p "n: #{n}"
  p "n.object_id #{n.object_id}"

  n += 1

  p "n: #{n}"
  p "n.object_id #{n.object_id}"
end

a = 3
p "a: #{a}"
p "a.object_id #{a.object_id}"

add_one(a)
p "a: #{a}"
p "a.object_id #{a.object_id}"
```

    "a: 3"
    "a.object_id 7"
    "n: 3"
    "n.object_id 7"
    "n: 4"
    "n.object_id 9"
    "a: 3"
    "a.object_id 7"

This doesn't mutate the caller because the caller `a` is an Integer `3` which cannot be mutated. Instead, this reassigns `n` to a new Integer `4`.

Technically, it looks like it's initially passing `a` by reference since `a` and `n` have the same object (are pointing to the same space in memory) before the reassignment:

```ruby
def return_a(n)
  p "n: #{n}"
  p "n.object_id #{n.object_id}"

  n
end

a = 3
p "a: #{a}"
p "a.object_id #{a.object_id}"

return_a(a)
p "a: #{a}"
p "a.object_id #{a.object_id}"
```

    "a: 3"
    "a.object_id 7"
    "n: 3"
    "n.object_id 7"
    "a: 3"
    "a.object_id 7"

It's only when there's an attempt to mutate `n`, the reassignment, where they start to diverge and a copy is made.


# Pass by Reference

Pass by reference object passing strategy is when a reference to the object is passed to the method. In Ruby, at least as far as I know at this point, this happens with mutable objects:

```ruby
def modify(n)
  puts "n: #{n}"
  p "n.object_id #{n.object_id}"

  puts "n[1]: #{n[1]}"
  p "n[1].object_id #{n[1].object_id}"

  n[1] = 'x'

  puts "n: #{n}"
  p "n.object_id #{n.object_id}"

  puts "n[1]: #{n[1]}"
  p "n[1].object_id #{n[1].object_id}"
end

a = %w(a b c)
puts "a: #{a}"
p "a.object_id #{a.object_id}"

modify(a)
puts "a: #{a}"
p "a.object_id #{a.object_id}"
```

    a: ["a", "b", "c"]
    "a.object_id 46937654840760"
    n: ["a", "b", "c"]
    "n.object_id 46937654840760"
    n[1]: b
    "n[1].object_id 46937654840800"
    n: ["a", "x", "c"]
    "n.object_id 46937654840760"
    n[1]: x
    "n[1].object_id 46937654840060"
    a: ["a", "x", "c"]
    "a.object_id 46937654840760"

`a`, an Array in this case, is changed and continues to point to the same Array object even though `a[1]` is reassigned to `'x'`. `a[1]` points to a new object `'x'`.


# Variables as Pointers

Variables are pointers to a physical space in memory, or to an object. So, in the following, `a` points to a String object with a value of `"Hello"`:

```ruby
a = "Hello"
```

Assigning `b` to `a` points `b` to that same String object with the value of `"Hello"`:

```ruby
a = "Hello"
b = a
p b.object_id
p a.object_id
```

    47100296476040
    47100296476040

Changing `a` will reassign `a` to a new point in memory, a new String object with the value of `"hi"`:

```ruby
a = "Hello"
b = a

p b
p b.object_id

p a
p a.object_id

a = "hi"

p a
p a.object_id

p b
p b.object_id
```

    "Hello"
    47077106005140
    "Hello"
    47077106005140
    "hi"
    47077106004820
    "Hello"
    47077106005140

When using a destructive (mutating) method, both `a` and `b` will continue to point to the same String object:

```ruby
a = "hi there"
p a
p a.object_id

b = a
p b
p b.object_id

a << ", bob!"
p a
p a.object_id
p b
p b.object_id
```

    "hi there"
    47340092174640
    "hi there"
    47340092174640
    "hi there, bob!"
    47340092174640
    "hi there, bob!"
    47340092174640


# Variable References and Mutability of Ruby Objects

I read through the [Variable References and Mutability of Ruby Objects](https://launchschool.com/blog/references-and-mutability-in-ruby) blog post. I learned some more terminology.

```ruby
greeting = 'Hello'
```

Here, `greeting` is said to *reference* the String object with a value of `'Hello'`. It can also be said to be *bound* to that String object.

In Ruby, numbers and booleans are immutable. That is, they do not have destructive (mutable) methods. When a mutation is about to occur, the variable is set to reference a new number/boolean object instead of mutating the original object:

```ruby
a = 1
puts "a: #{a}"
puts "a.object_id #{a.object_id}"

b = 2
puts "b: #{b}"
puts "b.object_id #{b.object_id}"

a = b
puts "a: #{a}"
puts "a.object_id #{a.object_id}"
puts "b: #{b}"
puts "b.object_id #{b.object_id}"

b += 1
puts "a: #{a}"
puts "a.object_id #{a.object_id}"
puts "b: #{b}"
puts "b.object_id #{b.object_id}"
```

    a: 1
    a.object_id 3
    b: 2
    b.object_id 5
    a: 2
    a.object_id 5
    b: 2
    b.object_id 5
    a: 2
    a.object_id 5
    b: 3
    b.object_id 7

I found this quote helpful:

> Immutable objects aren’t limited to numbers and booleans. Objects of some complex classes, such as `nil` (the only member of the NilClass class) and Range objects (e.g., `1..10`) are immutable. Any class can establish itself as immutable by simply not providing any methods that alter its state.
