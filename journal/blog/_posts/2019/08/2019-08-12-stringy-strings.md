---
layout: post
title: Stringy Strings, Array Average, Sum of Digits, What's My Bonus?
excerpt_separator: <!--more-->
tags: launch_school ruby small_problems easy_1
---

More work on the basic exercises.

<!--more-->


# Stringy Strings

This exercise asks us to write a method that takes a positive integer and returns a string the length of the given integer made up of alternating 1s and 0s. I decided to do this in a looping way by using `Array#each` and appending to a temporary `string`. Using a range `(0..num-1)`, it checks to see if the last character of the temporary string is `"1"` . If it is, it appends a `"0"` . If it's not, it appends a `"1"`.

The recommended solution in this case used a temporary `array`, `Integer#times`, `Integer.even?`, and `Array#join` instead. It uses the `index` to determine if the `index` is odd or even. If the string starts with a `1`, then all odd indexes are ones and all even indexes are zeros.

For further exploration, we're asked to modify the method so it takes an optional argument that defaults to `1` in order to determine how to start the string, with a `1` or a `0`. I did this with my solution and the recommended solution. In doing so, I noticed how much easier it was to add this functionality to the recommended solution. For my solution, I had to create another conditional which modified the given integer as needed. For the recommended solution, I created another conditional but didn't need to change the given integer. I took it a step further and created a method to determine whether or not to use `Integer#odd?` or `Integer#even?`.

There was a few interesting submitted solutions:


## Recursion

The base case checks to see if size (length) is less than or equal to zero. If it's not, it determines what the first character should be (a one or zero). If the first character should be a zero, it returns `"0"` concatenated with another call to itself with the `size - 1` and `1` as arguments. If the first character should be a one, it return `"1"` concatenated with another call to itself with the `size - 1` and `0` as arguments. Eventually, size will be equal to or less than zero and the full concatenated string is returned.


## String.ljust

This is a neat trick that uses the way the `String#ljust` method works instead of any logic. Basically, the `String#ljust` method takes an integer for size and a string. When the length of the string that calls it is less than the integer, it adds the difference to the provided string.

For example:

```ruby
p "test".ljust(7, "-")
```

    "test---"

When an empty string is the caller, it adds the provided string to the empty string:

```ruby
p "".ljust(3, "10")
p "".ljust(7, "10")
```

    "101"
    "1010101"


## Integer#divmod

Another interesting use of `Integer#divmod`. This solution also uses `String#*` (Copy). First, it sets the results of `Integer_divmod / 2` to two variables, the `result` and `remainder`. If the first character should be a `1`, it returns the string `"10"` copied `result` times (`"10" * result`) plus the string `"1"` copied `remainder` times (`"1" * remainder`). If the first character should be a `0`, it returns the string `"01"` copied `result` times (`"01" * result`) concatenated with the string `"0"` copied `remainder` times (`"0" * remainder`). The `remainder` changes depending on whether the given integer is odd or even. Basically, `remainder` will either add zero or one `"1"` or `"0"` to the end of the resulting string.

```ruby
def stringy(size, start = 1)
  result, remainder = size.divmod(2)

  if start == 1
    "10" * result + "1" * remainder
  else
    "01" * result + "0" * remainder
  end
end

p stringy(3)
p stringy(6)
```

    "101"
    "101010"


# Array Average

Take an array of positive integers and return the average of the integers in the array. I used `Enumerable#reduce` to get the sum of the integers in the array and divided that by the length of the array using `Array#length`. I prefer the shorthand (`:+`), but am not yet used to seeing and understanding it:

```ruby
def average(numbers)
  numbers.reduce(:+) / numbers.length
end

p average([1, 2, 3, 4, 5])
```

    3

The recommended solution also used `Enumerable#reduce` but used `Array#count` instead, which is basically the same as `Array#length`. The overall logic matched my solution.

The further exploration asked us to try to accommodate floats. This can be done with `Integer#to_f` and `Float#round` as needed.

There were a few submitted solutions of interest here:


## Array#sum

This eliminates the need for `Array#reduce` as the `Array#sum` method sums up the elements of an array:

```ruby
def average(array)
  array.sum / array.length
end

p average([1, 2, 3, 4, 5])
```

    3


## Array#sum with argument

Using an argument with `Array#sum` makes it possible to account for floats with little effort:

```ruby
def average(array)
  (array.sum(0.0) / array.length).round(2)
end

p average([1, 2, 3, 4])
```

    2.5


## Integer#fdiv

This method divides two integers but returns a float:

```ruby
def average(array)
  array.sum.fdiv(array.size).round(2)
end

p average([1, 2, 3, 4])
```

    2.5


# Sum of Digits

Take a positive integer and return the sum of its digits. I found this one to be fun because there are a few quick and simple ways to accomplish it. My solution chained `Integer#to_s`, `String#split`, `Array#map`, and `Array#sum` together:

```ruby
number.to_s.split('').map(&:to_i).sum
```

The recommended solution seemed a longer and more complex to me using `String#chars` and `Array#each`.

Turns out, this can be done with three methods chained together succinctly, as shown in the submitted solutions:


## Integer#digits

Since we don't care about the order of the digits in the array, we can use `Integer#digits` to break up the integer into an array of digits, reversed. Then we can use `Array#sum` to add them together and return the result:

```ruby
num.digits.sum
```


## Integer#sum

It's also possible to provide a block to `Array#sum` in order to do something to each integer in the array before summing them:

```ruby
number.to_s.split('').sum { |n| n.to_i }
```


# What's my Bonus?

Given a salary and boolean of whether or not to give a bonus, return the bonus which is half the salary. I chose to do this with a ternary:

```ruby
bonus ? salary / 2 : 0
```

I found the tests to be more complicated because I don't yet know the best way to test for the presence of a boolean. Here are the two ways I found:

```ruby
((bonus.class == TrueClass) or (bonus.class == FalseClass))
```

```ruby
[true, false].include?(bonus)
```

This solution matched the recommended solution and there were no interesting submitted solutions.
