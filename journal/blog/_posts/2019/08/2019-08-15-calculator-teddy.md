---
layout: post
title: Calculator and How Old is Teddy?
excerpt_separator: <!--more-->
tags: launch_school ruby project small_problems easy_2
---

I went through the first few lessons about creating a Ruby calculator. I followed along with the videos, practicing concepts that I hadn’t previously been exposed to and solidifying concepts I know.

<!--more-->


# Walk-through: Calculator

Watched a video about refactoring the sample `calculator.rb` program. Moved `if` statements to `case` statements and added methods for the prompt, validation, and to convert operations to friendlier wording. Added looping for the main loop as well as for the validations.

One of the additional questions prompted me to do some more research. The question about why it's possible in Ruby to use `puts` alone without the `Kernel` module name before it like `Kernel.puts()`. I guessed that it involved some type of global scoping. Turns out, the answer involves a default `main` object that's used when not working within a class. Since this `main` object is an object and inherits from the `Object` class, it also inherits the modules mixed-into the `Object` class, in this case, the `Kernel` module.

Basically, the methods in the `Kernel` module are available everywhere in Ruby.


# Rubocop Calculator

Watched a video showing the use of Rubocop. As I mentioned previously, this is built-in to my editor but not working in org-mode src blocks. To follow along with the video, I opened the ruby file directly and could see the error messages.


# Debugging

The majority of work done by a programmer is debugging, figuring out ways to solve a problem, experimenting, and coming up with solutions. Temperament is important. That is, one needs to be able to patiently work through a difficult problem instead of panicking and worrying. Cultivating a systematic, patient temperament when faced with a problem is important and necessary to be a successful programmer. We all get frustrated, so it's important to learn how to deal with that frustration.

Learning to read error messages is also important. Once the error has been found, one can learn to efficiently utilize online resources (search engines, Stack Overflow, Ruby Documentation).

The main steps to debugging are:

1.  Reproduce the Error
2.  Determine the Boundaries of the Error
3.  Trace the Code
4.  Understand the Problem Well
5.  Implement the Fix
6.  Test the Fix

Some of the different techniques:

-   Line by Line
-   Rubber Duck
-   Walking Away (Diffuse mode)
-   Using Pry
-   Using a Debugger to step through code.

I sometimes find myself panicking when I encounter a problem that's not simple to solve. However, I find that if I break the problem down into smaller, testable steps, I can usually make headway and often solve the problem. In some sense, I think using GNU/Linux as my main operating system helps because I'm used to looking things up, isolating problems, etc&#x2026; in order to get things working as I want them to.


# Small Problems > Easy 2 > How old is Teddy?

This asks us to randomly generate and print an age for Teddy. The age needs to be between 20 and 200. While doing the PEDAC process for this, I noticed that there's no input. I also noticed that I don't know a good way to create tests for things that involve random numbers.

I decided to use `Kernel#rand`. My solution was similar to the recommended solution, except that I made it more complicated by creating and calling a method. The recommended solution didn't use a method.

For the further exploration, we're tasked with asking for name and using Teddy as the default name if no name is given. I did this with `Kernel#puts`, `Kernel#gets`, `String.empty?`, and a ternary operator within the final string.
