---
layout: post
title: The Day Before, Yesterday, and Today
excerpt_separator: <!--more-->
tags: launch_school ruby
---

I started working with the [Launch School](https://launchschool.com) curriculum about three months ago with the prep course. This journal entry marks the beginning of my journey into the paid core course.

<!--more-->


# August, 6, 2019


## RB101 Programming Foundations Lesson 1: Preparations

In this lesson, I connected a GitHub account (that basically forwards to my LS GitLab account) so I could have an avatar show up in LS forums and comments. There's advice about using markdown, the best system to use, becoming aware of potential differences between Ruby versions, suggested ways to use Git and GitHub (GitLab), suggestions on how to focus when watching videos, the differences between good and bad questions, and how to navigate the lesson discussion forums.

The [Exercises: Small Problems](https://launchschool.com/lessons/c82cd406/assignments/df5b7d1e) part was a little more involved. For this, I read [The Two-Layer Problem](https://medium.com/launch-school/the-two-layer-problem-915b7587654c) which suggests there are two layers to solving coding problems: identifying logical steps and representing these steps with code. LS teaches the PEDAC process, which stands for Understand the Problem, Examples and Test Cases, Data Structure, Algorithm, and Code. The article links to [Solving Coding Proglems with PEDAC](https://medium.com/launch-school/solving-coding-problems-with-pedac-29141331f93f) for more info about the PEDAC process.

Back in the exercise, it's recommended to ****do 20 exercises (or 2 sets) after each lesson (no need to complete the Advanced exercises set)****.

There's a small quiz at the end.


## Problem Solving Videos - Part I

Before starting on the small problems, LS suggests reading through [the primer](https://launchschool.com/gists/2a3a3d72). This is a detailed document that gives an overview of the Small Problem set and provides suggestions for how to solve the problems. To learn more about solving problems, LS recommends watching the [Problem Solving videos](https://launchschool.com/gists/c013accd). There are two videos.

These videos show the PEDAC process, well the PEDA part, in detail with example problems. I took time to take notes on the videos, copy the examples, and attempt to use the PEDAC process on the example problems on my own. I watched the first video.


# August, 7, 2019


## Problem Solving Videos - Part II

I watched the second video. Interestingly, it's in this second video where we learn about the Algorithm part of the PEDAC process. By the time the algorithm is known, the last part of the process (Coding) turns out to be much easier. Again, I took notes on the video and copied the examples.


## Class and Instance Methods

<https://launchschool.com/exercises/e215608d>

In this exercise, we're tasked with recognizing how to read the documentation and distinguish between `class` and `instance` methods. We also learn how these two types of methods are called. Instance methods are called on an object (instance) of a class `f.path` whereas class methods are called on the class itself `File.path`. We're told to ****Pay attention when reading the documentation; make sure you are using a class method when you need a class method, and an instance method when you need an instance method.****


## Optional Arguments Redux

<https://launchschool.com/exercises/4debd143>

This one involved the `Date` class in the Standard Library API documentation. I noticed a discrepancy between the exercise solution and the most recent documentation for Ruby 2.6.3. In the solution, it shows that the `Date::new` class method points to a signature that looks like `new([year=-4712[, month=1[, mday=1[, start=Date::ITALY]]]]) → date`. However, in the Ruby 2.6.3 documentation, the `Date::new` class method points to this signature: `new(p1 = v1, p2 = v2, p3 = v3, p4 = v4)`. In any case, the lesson learned here is to learn to look in the Standard Library API for documentation as needed.


## Default Arguments in Middle

<https://launchschool.com/exercises/c3d56d90>

It's possible to find information about arguments in the `syntax/Calling Methods` section of the documentation. We're asked to determine what the given code will print, but in order to do so, we must first find the documentation. Default positional arguments can be placed in the middle of the arguments list, however all default positional arguments must be next to one another.


## Mandatory Blocks

<https://launchschool.com/exercises/4ebc422b>

We're given an array and asked how we would search the array to find the first element that's greater than `8`. Ruby has a binary search method (`Array#bsearch`) and we're told to use that method because it can search more quickly than the `Array#find` and `Array#select` methods. The documentation for `Array#bsearch` shows that it takes a `block` that returns `true` or `false` as an argument. It will then return the first element in the array that matches the `true` condition and `nil` if the condition doesn't match any element in the array.

Example:

```ruby
a = [1, 4, 8, 11, 15, 19]

true_case = a.bsearch { |num| num > 8 }
false_case = a.bsearch { |num| num > 20 }

p true_case
p false_case
```

What's important to note in this case is that the block is mandatory and not optional.


# August, 8, 2019


## Multiple Signatures

<https://launchschool.com/exercises/d45f350a>

We focus on the way the documentation might provide us with multiple signatures for a method. In this case, we're looking at the `Array#fetch` method. It's interesting how this method is different from the `Array[]` method. The `Array#fetch` method will throw an `IndexError` exception if the given index doesn't exist within the array. The `Array[]` on the other hand, will return `nil` when the given index doesn't exist within the array. `Array#fetch` is helpful when one needs to know what's in an array.

The point of this exercise is to learn to read the multiple signatures. `Array#fetch` has three of them.

****Note in particular that having multiple lines in the signature is yet another way in which the ruby documentation indicates that an argument is optional.****


## Keyword Arguments

<https://launchschool.com/exercises/7761d12c>

We're given `5.step(to: 10, by: 3) { |value| puts value }` and asked to determine what this code will print. To do this without running the code, one needs to find the `#step` method. I started to look for this method in the `Integer` class. It wasn't there. Then I looked in the `Numeric` class and it was there. The `Numeric` class is the superclass for both the `Integer` and `Float` classes.

There are four different types of number classes in Ruby: Integer, Float, Numeric, and Math. When looking for methods, it might be necessary to look through the parent classes in order to find the method in question.


## Parent Class

<https://launchschool.com/exercises/3e9ad7d3>

Again, this one requires one to find the method by looking in the parent class. We're given a `String` and asked to modify the following code in order to only include methods that are defined or overridden by the `String` class: `puts s.public_methods.inspect`. I started with the `String` class and noticed that there is no `#public_methods` method. I then looked in the `String` class's parent class, `Object`. The `#public_methods` method was there. In the documentation, it states that the `Object#public_methods` method has a default positional argument set to `true`. When this is set to `true`, as it is by default, the `#public_methods` method will return all of the methods associated with the `String` class and it's mixed-in methods and parent classes. When set to `false`, it only returns the methods set or overridden in the `String` class.

One of the comments said that `String.instance_methods(false).inspect` also works. I was hung up on this for a bit because it was more re-writing than modifying the given code. However, in re-reading the exercise text, I can see that this solution is technically correct: ****How would you modify this code to print just the public methods that are defined or overridden by the `String` class?****


## Included Modules

<https://launchschool.com/exercises/bbb9b595>

This exercise relied on older documentation, but made the point that sometimes the method in question is found in a mixed-in method as opposed to a parent class. Ruby does not allow Multiple Inheritance (MI), i.e. inheriting from more than one parent class. Instead, Ruby has `mix-in modules` instead. For this exercise, we look at one of the most used mix-ins, the `Enumerable` module.

So, sometimes one looks in parent classes and other times in mixed-in modules.
