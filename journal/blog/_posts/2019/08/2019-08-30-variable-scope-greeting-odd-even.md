---
layout: post
title: Variable Scope, Greeting a User, Odd and Even Numbers
excerpt_separator: <!--more-->
tags: launch_school ruby small_problems easy_2
---

Variable scope can be tricky, especially when working with collections and methods, which is very common. It's important to understand what is being returned and how objects are changing throughout the code.

<!--more-->


# Variable Scope

The main point of this section is to note that ****Inner scope can access variables initialized in an outer scope, but not vice versa.**** A basic example of this:

```ruby
a = 1

loop do
  a += 1
  b = 1
  break
end

puts a
```

    2

If I try to print `b` to the screen, I'll get the following error:

    : NameError (undefined local variable or method `b' for main:Object)

Except for constants, which act more like global variables:

```ruby
loop do
  CONSTANT = 1
  break
end

puts CONSTANT
```

    1

Method definitions don't have access to local variables in the outer scope:

```ruby
a = 1

def some_method
  puts a
end
```

If I try to call `some_method`, `puts a` will throw the following error:

    : NameError (undefined local variable or method `a' for main:Object)


# More Variable Scope

Learned about more specific definitions for ****method definition**** and ****method invocation****.

Method definition is when one defines a method using the `def` keyword. Method invocation calls that method.

When a method is called with a block, it's like the block is an argument to that method.

All method can take blocks as arguments, but they may or may not be setup to use them.

The key points for review are:

-   `def/end` is a method definition
-   referencing a method, either existing or after method definition, is method invocation
-   method invocation that's followed by curly braces `{..}` or `do/end` defines a block and is part of method invocation
-   method definition sets the scope and interaction with blocks
-   method invocation uses that scope


# Small Problems > Easy 2 > Greeting a user

I chose to use `String#upcase`, `String#chop`, `String#chomp`, and `String#capitalize` in my solution. I chose to use `String.include?` in order to check for the presence of the exclamation mark. I also validated the name by only allowing letters from a-z with zero or one exclamation point at the end of the word.

The recommended solution used `name[-1] == '!'` to check for the exclamation mark. At first, when I looked at this, I thought it was done this way so it didn't pick up an exclamation mark in the middle of the word, or something like that. Since the recommended solution doesn't validate the input, exclamation marks, numbers, etc&#x2026; will get through.

I noticed that some of the submitted solutions don't match the expected output because they failed to remove the exclamation mark with `String#chop` or something else.


# Small Problems > Easy 2 > Odd Numbers

I chose to use `Range#each` and `Integer#odd?` for my solution:

```ruby
(1..99).each { |num| puts num if num.odd? }
```

    1
    3
    5
    7
    9
    11
    13
    15
    17
    19
    21
    23
    25
    27
    29
    31
    33
    35
    37
    39
    41
    43
    45
    47
    49
    51
    53
    55
    57
    59
    61
    63
    65
    67
    69
    71
    73
    75
    77
    79
    81
    83
    85
    87
    89
    91
    93
    95
    97
    99

This does the job. It may not be the most efficient, I don't know.

The recommended solution used a `whlie` loop, iterating the counter by two each time.

For further exploration, we were asked to try different methods. I completed this using `Integer#upto`, `Array#select`, `Integer#times`, `Numeric#step`, `while` incremented by one, `while` incremented by two, and `loop/do` incremented by two.


# Small Problems > Easy 2 > Even Numbers

I chose to use `Range#each` and `Integer#even?` for my solution. The recommended solution used a while loop and `Integer#even?` incremented by 1.
