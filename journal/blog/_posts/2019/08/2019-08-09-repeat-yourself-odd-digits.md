---
layout: post
title: Repeat Yourself, Odd, List of Digits
excerpt_separator: <!--more-->
tags: launch_school ruby small_problems easy_1
---

Today, I worked on some of the course work as well as some exercises. Launch School's curriculum is a combination of instruction, exercises, and projects. There are hundreds of exercises, so today I start working on the beginning exercises and I'll continue to work on these exercises, a little each day, until I'm finished.

<!--more-->


# Small Problems > Documentation Again > Down the Rabbit Hole

This documentation exercise reminded us that sometimes the documentation can be sparse and it may take more time and energy to find what one is looking for. The example given involved the `YAML::load_file` method. When looking at the `YAML` documentation page, it's not clear where the methods are until one reads the text on the page stating that the `YAML` library is an alias for the `Psych` library. Looking in the `Psych` library documentation shows the methods, including `Psych#load_file`.

Apparently, in past documentation, this connection between the `YAML` library and the `Psych` library was not as obvious. The Ruby 2.6.0 documentation shows this connection clearly.


# Small Problems > Easy 1 > Repeat Yourself

This is the first problem that I used the PEDAC process on. I found PEDAC to be most helpful in terms of thinking about necessary test cases. I created a bunch of test cases based on assumptions of how to handle different inputs. In an interview or work situation, I'd ask the interviewer/my team how we want to handle the varying inputs.

Basically, this created a method that takes a string and integer as arguments. It then prints the string the integer number of times.

The recommended solution in this case described the value of solving problems in a step-by-step manner, starting with `puts string` in the body of the method and then making sure that works. Then, loop around that statement in order to call `puts string` multiple times as needed.


# Small Problems > Easy 1 > Odd

Using the PEDAC process again, I realized that there's implicit instructions for this exercise in terms of determining odd numbers. Also, the instructions said do not use the `#odd?` and `#even?` methods. The instructions also noted that the input would be a valid integer value, so there's no need to validate the input any further.

I chose to do this with a one-line piece of code using `Integer#abs` and the `%` (modulo) operator.

The recommended solution noted that in Ruby, the `%` is modulo and not remainder, like it is in JavaScript noting:

****Remainder operators return negative results if the number on the left is negative, while modulus always returns a non-negative result if the number on the right is positive.****

It then prompted a further exploration condition of re-writing the solution using `Integer#remainder` instead of `%`. Initially, I solved this using `Integer#abs` like `num.abs.remainder(2)==1`. Looking at the comments from other students, I noticed that comparing the result of `Integer#remainder` with `0` instead of `1` lets me use `num.remainder(2)!=0` and not worry about negative integers.


# Small Problems > Easy 1 > List of Digits

I found that this one needed a little more input validation. The challenge was to *write a method that takes one argument, a positive integer, and returns a list of digits in the number.* I accomplished this with a couple of input validations, `Integer#digits`, and `Array.reverse`. Once the method knows it's dealing with a positive integer, it converts the integer into an array of digits. `Integer#digits` reverses the digits when it stores them in the array. So, `Array.reverse` is needed in order to reverse the digits within the array back into their original order.

There were two recommended solutions.

The first one is what's known as a *brute force* solution. This solution relies on a loop and the `Integer#divmod` method, which divides the two numbers and returns the result of the division without the remainder and the remainder itself. So, with this method, it's possible to get the result and remainder in separate variables. Dividing the integer by `10` will essentially chop off the last part of the integer and place that part in the remainder variable. Then, it adds that remainder to the beginning of a `digits` array created outside the loop using `unshift`. Finally, it breaks out of the loop when the result of the `Integer#divmod` is `0` and returns the `digits` array. I added the same validation I used for my solutions and this passed all of the tests.

The second recommended solution is considered *idiomatic*. It uses the `Integer#to_s`, `String#chars`, and `Array#map` methods along with `&:to_i`, which is the shorthand for `{|num| num.to_i}`. I added the validations I used for my solution and this passed all of the tests.

I learned two major things here:

1.  There are many ways to solve a problem. I knew this already, but this is yet another example.
2.  `Integer#divmod` is an interesting method to use when dealing with remainders.
