---
layout: post
title: Array#first, Array#last, and Array#delete
excerpt_separator: <!--more-->
tags: launch_school ruby
---

Where I answer a question in Slack about Array#first, Array#last, and Array#delete in as detailed a manner as I know at the time.

<!--more-->


# Someone in LS Slack asked the following regarding `Array#first`, `Array#last`, and `Array#delete`:

> I've been trying to make sense of this exercise in the prep course for quite some time now but can't seem to understand why the third line has to be the way it is in order to delete the element 1 in `["b", 1]`. Why doesn't `arr.delete(arr.first.last)` or `arr.delete(first.last)` or `arr.first.delete(last)` work?

```ruby
arr = ["b", "a"]
arr = arr.product(Array(1..3))
arr.first.delete(arr.first.last)
```

The following is my understanding given my knowledge so far.

The solution works because each time `arr.first` is executed, it points to the same object.

To test this, I did the following:

```ruby
irb> arr
=> [["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]]

irb> arr[0].object_id
=> 47337457424260

irb> arr1 = arr.first
=> ["b", 1]

irb> arr1.object_id
=> 47337457424260

# `arr1` is the caller instead of `arr` which seems like it
# will delete only from `arr1`
irb> arr1.delete(arr.first.last)
=> 1

irb> arr1
=> ["b"]

irb> arr
=> [["b"], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]]
```

Since `arr1` and `arr[0]` are pointing to the same object, both are mutated when that object is mutated. `arr` is also mutated since it contains `arr[0]` which points to the same object.

So, `arr.first.delete(arr.first.last)` can be thought of like:

```ruby
[["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]].first.delete([["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]].first.last)
```

After evaluating `[["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]].first` both times:

```ruby
["b", 1].delete(["b", 1].last)
```

After evaluating `["b", 1].last`:

```ruby
["b", 1].delete(1)
```

This understanding helped me figure out why the code in the question doesn't work.


## `arr.delete(arr.first.last)`

This can also be written like:

```ruby
[["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]]`.delete([["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]].first.last)
```

After evaluating `[["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]].first`:

```ruby
[["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]]`.delete(["b", 1].last)
```

After evaluating `["b", 1].last`:

```ruby
[["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]]`.delete(1)`
```

There's no element in `[["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]]` that matches `1`, so this returns `nil`.


## `arr.delete(first.last)` and `arr.first.delete(last)`

These will not work because `Array#first` and `Array#last` are instance methods that need to be called by an instance of the Array class. They can't be used as arguments alone.


## `arr.first.delete(arr.last)`

This is possible. It can be thought of like:

```ruby
[["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]].first.delete([["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]].last)
```

After evaluating `[["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]].last`:

```ruby
[["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]].first.delete(["a", 3])
```

After evaluating `["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]].first`:

```ruby
["b", 1].delete(["a", 3])
```

There's no `["a", 3]` in `["b", 1]`, so this returns `nil`.
