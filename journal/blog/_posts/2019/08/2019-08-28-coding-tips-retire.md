---
layout: post
title: Coding Tips, When will I Retire?
excerpt_separator: <!--more-->
tags: launch_school small_problems easy_2 coding_tips ruby
---

The coding tips lesson offered a variety of helpful tips on how to structure, name, and think about one’s code. I took some time to solidify my understanding of the term side-effects.

<!--more-->


# Coding Tips

I spent a lot of time today on this section. There's a lot of information and I didn't want to continue until I had at least a basic understanding of the concepts therein.

The part about retaining knowledge makes sense and I understand this rationally. I can't say that I understand it completely from experience, although I have spent lots of time on something only to realize I didn't need to spend time on that. Did I repeat that later? I don't remember.

I find naming things to be difficult. The key point here is that ****Variables are named not for how they are set, but for what they actually store.**** The example given was between `yes_or_no = gets.chomp` and `play_again = gets.chomp`. The latter allows for the possibility of responses beyond `yes` and `no`. It was also noted that naming variables in small blocks of code can be different, since those variables don't live past the block. An example might be `numbers.map { |n| n**2 }`. It doesn't necessarily matter what `n` is since it isn't used outside of the block.

Ruby's preferred naming conventions:

variables: snake<sub>case</sub> classes: CamelCase constants: UPPERCASE

It's important to never mutate constants.

The method guidelines part took me a little time to get through. Each method should only do one thing. It's important to not display something and return a meaningful value.

I've heard the term `side effects` but wanted a better understanding, so I did some more research on this. Basically, a side effect is a change to something. I found an analogy to medicine that helped. The medicine may do one thing, like reducing pain, but it may also have a side effect like causing nose-bleeds. So, in code, the method should only do the one thing and not have any additional things making other changes.

In terms of naming methods, a method name like `total` implies that the return value of that method will be a number representing the total of something. It's redundant to use `return_total` for that method.

This part stood out for me:

****If you find yourself always looking at the method implementation, it's a sign that the method is not named appropriately, or that it's doing more than one simple thing.****

I feel like I've been here many times while writing my programs.

I'm still not totally clear in regards to the abstraction level of methods. In researching this a little, I think it's referring to not mixing logic with calls to other methods. I'm not totally sure about this and look forward to digging into this deeper in the future.

If a method mutates (changes) something, the method name should reflect that. The example given was `total` vs `update_total`. With the former name, one expects a total return value. With the latter, one expects it to update the total but not return it. The two can be used like this:

```ruby
update_total
puts "The total is: #{total}."
```

When a method displays output, prefix it with something like `display_`, `say_`, `print_`. This denotes that the method only displays something and doesn't return a meaningful value.

The miscellaneous tips were self-explanatory, one exit point, indent two spaces, name things with the idea that they'll be used later, differences between the `do/while` and `while` loops.

As far as Truthiness goes, ****everything in Ruby is truthy except `nil` and `false`.**** Instead of `if user_input == true` we can just do `if user_input`.

That's a lot of information and I'll likely be returning to this section often to refresh my memory and check programs before submission.


# Small Problems > Easy 2 > When will I Retire?

I did this one simply with no validations. I think the overall point of this was to be exposed to the `Time` class and the `Time#year` method. It's good that this was mentioned, because my first thought was to use the `Date` class and `Date#year` method from the standard library. Now I know there's a `Time` class that I can use instead.
