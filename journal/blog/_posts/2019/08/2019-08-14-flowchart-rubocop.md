---
layout: post
title: Flowcharts and Rubocop
excerpt_separator: <!--more-->
tags: launch_school ruby
---

I learned more about flowcharting software available on GNU/Linux, attempted a few solutions within Emacs, and ended up settling for LibreOffice Draw. My Rubocop in Emacs Org-mode source blocks stopped working. :(

<!--more-->


# Flowchart

I spent a little time on this, probably more than I should have. Since I'm hoping to learn how to plan and organize software projects, I figured I should be able to make flowcharts as needed. They're more difficult for me than I anticipated.

I started looking for a good tool. It's possible to make flowcharts in Emacs, but I don't yet like the way it's done using [Ditaa](http://plantuml.com/ditaa) or [Graphviz](https://www.tonyballantyne.com/graphs.html). I considered using [Gimp](https://www.gimp.org/), but in looking it up further, I found a couple of recommendations for using [LibreOffice Draw](https://www.libreoffice.org/discover/draw/).

I like LibreOffice Draw so far. It's straightforward and has a lot of options. I'm probably not doing things efficiently at this point, but I'll pick up what I need as I use it more. I duplicated the flowcharts in the lesson as much as I could from memory.


# Rubocop

I already have this installed and have been using it. I added the recommended configuration. Recently, I stopped seeing some of the [Rubocop](https://www.rubocop.org/) messages while editing source blocks in [Emacs org-mode](https://orgmode.org/). I had it working before, but something changed on my system in the last week or so that broke it. It works when I'm in a buffer with ruby-mode, so I'll be fine overall. Just wish it worked in org-mode so I could work on more literate programming styles and have Rubocop helping me along the way.


# Imperative vs. Declarative

I learned a little about the differences between ****Imperative**** and ****Declarative**** problem solving. Imperative is when one maps out each logic step. Declarative uses encapsulation to hide the details. Imperative is used for more fine-grained understanding. Declarative is used for a more global or broad-level understanding of the problem.
