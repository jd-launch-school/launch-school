---
layout: post
title: Study Sessions Continue
excerpt_separator: <!--more-->
tags: launch_school ruby 
---

I've now in been in about five additional study sessions and I've managed to solve all of the problems provided to me in a relatively reasonable amount of time. Yet, I still fail in various ways either during the problem solving process, the description of my process, and in other ways.

<!--more-->

On some level, I feel as though the months of studying I've done up to this point was an unproductive use of my time. Sure, I have a much deeper understanding of Ruby as a result of that studying and learning. However, while I'm working through a problem, I still find myself using really basic Ruby statements and logic to solve a problem.

When I first read through a problem, I get a sense of the problem in my mind's eye. I can picture code snippets and methods I might want to use. I literally see them as if I typed them already, but they're just in my mind. I feel like this hurts me because instead of really understanding the problem deeply, I'm skipping ahead to the code.

Here's an example of my reasoning while working through a problem.

The problem:

```ruby
# Given a string, return a new string that's the reverse of the original string,
# keeping spacing intact. DO NOT USE the `String#reverse` method.

# Examples:
# 'hello'            => 'olleh'
# 'this is a string' => 'gnirts a si siht'
```

This is a really basic problem.

I start by pointing out the inputs, outputs, and rules:

```ruby
# INPUT:
#  - string with one or more words
# OUTPUT:
#  - string that's the reverse of the original string
# RULES:
#  - if the string contains more than one word, reverse each word and then also
#    reverse the order of the words
```

Then, I list out some examples. Usually, they're the same examples given in the problem description, only formatted to work in Ruby:

```ruby
# EXAMPLES:
# p rev(string) == 'olleh'
# p rev(string) == 'gnirts a si siht'
```

I then make sure I focus on the data structure of the input and output:

```ruby
# DATA STRUCTURE
# INPUT: String
# OUTPUT: String
```

Doing this helps to remind me which methods might be most appropriate. Finally, it's time to think about the algorithm I want to try:

```ruby
# ALGORITHM
# - initialize a local variable `result` and assign to it an empty string
# - use the `chars` method to split the string into an array of characters
# - use the `each` method on a range from `0` to the size of the array of
#   characters to create a loop that's as long as the given string. Pass 
#   a `do..end` block as an argument with `index` as a parameter.
# - For each iteration, append the element at 
```

That's about as far as I get most times and then I run into problems. I begin to see the full complexity of the problem and start to panic because it feels more complex than my algorithm is prepared for.

At this point, I'll usually jump into the code and tell the other person that I'll come back to the algorithm (which I rarely do). Doing this is frustrating to me because I'd like to be able to solve these problems without writing the code. It feels as though I have a mental block that prevents me from solving the problems in English. I feel like I need the iterative process of struggling with the code in order to solve the problem and feel inadequate without it.

Here's how I might solve that problem without a pre-written algorithm.

First, get the string into an array of characters:

```ruby
p 'this is a string'.chars
```

    ["t", "h", "i", "s", " ", "i", "s", " ", "a", " ", "s", "t", "r", "i", "n", "g"]

Then, call the `map` method:

```ruby
p 'this is a string'.chars.map { |char| char }
```

    ["t", "h", "i", "s", " ", "i", "s", " ", "a", " ", "s", "t", "r", "i", "n", "g"]

Then, prepend that \`char\` to a new array object. At this point, I realize that I need another array object to store results in. So, I can either use `map.with_object` or `each_with_object`. Since this is code, it feels natural to change it as needed. However, when I got to the same part in the algorithm above, it felt unnatural to erase and re-write the algorithm.

```ruby
p 'this is a string'.chars.map.with_object([]) { |char, array| array.prepend(char) }
```

    ["g", "n", "i", "r", "t", "s", " ", "a", " ", "s", "i", " ", "s", "i", "h", "t"]

Then, join the result:

```ruby
p 'this is a string'.chars.map.with_object([]) { |char, array| array.prepend(char) }.join
```

    "gnirts a si siht"

Done.

Why can I solve it so quickly and naturally with code and not in English? What prevents me from seeing the English like I see the code, as malleable and easily changed?

I think one problem is that I don't get immediate feedback from my English sentences like I do with code. With code, I can try something, see the result, and try something else until I get what I want. While writing out the algorithms in English, I don't know if what I'm writing is going to work at any stage so it seems like if I make one mistake, the entire algorithm may be wrong.

I need to really focus more on the algorithm section of the PEDAC process and really nail it down. I've written it out many times, but it still hasn't clicked.

So, even though I'm solving the problems, I'm not solving them in a way that shows that I truly understand the problems. I'll keep trying&#x2026;
