---
layout: post
title: Two Medium Problems
excerpt_separator: <!--more-->
tags: launch_school ruby small_problems medium_1
---

I finished the RB101 course and am now in the process of preparing for the assessment in RB109. To do this, I'm working through the rest of the Medium level Small Problems, continuing to attend as many study groups as possible, and working through the Easy level Small Problems again.

<!--more-->

Over the past few days, I've been getting back into the problem-solving mode. This time, I'm focusing on getting a good understanding of the problem quickly and working my way to a solution in under 20 minutes.

For the Diamonds problem, I wasn't able to do this. It took me a couple of days and a few attempts to get to a point where I felt comfortable enough with the problem to solve it and truly understand it. There are a lot of moving parts with this problem. I solved it in a strange (to me) way that uses two counters, one counting up and the other counting down. I recognized that the absolute value of `counter1 - counter2` returns the total number of spaces in the row. From that, I subtracted this result from the given grid size in order to get the number of stars. From there, I only needed to divide the total number of spaces in half to get the number of spaces at the beginning and end of the row.

In looking at other people's solutions, I notice that they also used some sort of relatively arcane math to help them accomplish their goal. Many solutions divide by two or subtract two. In reading the solutions, it's not clear at first why one needs to do this.

My favorite part of this exercise was the further exploration where we were tasked with printing a hollow diamond. I got to showcase my regular expression skills when I noticed that one of the solutions took the complete diamond and removed the stars in the middle using regular expressions. This was fun. :)

The other problem I worked on was the Stack Machine Interpretation exercise. I found this one to be relatively straightforward for a Medium level exercise and fun to implement. It basically required a `case` statement after separating the given program into separate commands. Commands look like this:

```ruby
'5 PUSH 3 MULT PRINT'
```

This particular command tells the program to add `5` to the register, push `5` to the stack, add `3` to the register, take the `5` off of the stack and multiply it by `3` storing the result in the register, then print the register. The result should return `15`.

Other than that, I've been doing better in my study groups. I'm mostly able to get to a solution in 20-25 minutes. I'm not perfect. Sometimes I fail to get the solution in time. In that case, I usually continue to work on the problem after the study session and share my solution when I get it.

I still need more practice with problems that require sub-iterations, that is, iterating over a collection and then iterating over that same collection or a different collection within the iteration of the first collection. I find these types of problems especially challenging when working through the algorithm section.

Also, I need to practice more math-based problems. These problems tend to trip me up when I'm not certain about the math involved, so I need to practice what I'll do in those situations so I can be ready if one of these problems is asked on the assessment.
