---
layout: post
title: Behavior Delegation (Prototype Pattern)
date: 2022-01-10 14:00:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js225 inheritance objects constructor_functions prototype
---

I've been continuing to focus on the JavaScript prototype OOP model. This is an overview of the prototype pattern. The prototype pattern is a pattern that defines shared behaviors on a constructor's function prototype.

<!--more-->

I show both object inheritance diagrams and code with results in table form to show the connections between object prototypes and constructors. For the diagrams, it's best to read from the `__proto__` or `constructor` properties to the objects they point to.

# Example of inefficient method duplication
The following is an example demonstrating that even though a method's behavior defined in the `Book` function constructor is the same for all Book child instances, the actual behavior objects themselves are unique. During runtime, JavaScript creates a new copy of the method each time we instantiate a new `Book` instance.

With lots of Book objects, this may be problematic as there will be multiple objects with the same code in memory.

This is OK in terms of code maintenance. When we need to add or change a property or method, we can add to or change the `Book` constructor's properties and methods or add properties and methods to the constructor's function prototype as needed.

```js
const Book = function (title) {
  this.title = title;

  this.readPage = function () {
    console.log(`Reading next page of ${this.title}.`);
  };
};

const book1 = new Book('First');
const book2 = new Book('Second');

/*
 *                          +----------+
 *                          | Book f() | <-----+
 *                          +----------+       |
 * +-------+        +-----> prototype: {       |
 * | book1 |        |       ^ [[Prototype]]: --|--+          +------------+
 * +-------+        |      /  constructor: ----+  |          | Object f() | <-------+
 * [[Prototype]]: --+     / },                    |          +------------+         |
 * title: 'First',       /  [[Prototype]]: --+    +--------> prototype { <----------|--+
 * readPage f(),        /                    |                 [[Prototype]]: null, |  |
 *                     /                     |                 constructor: --------+  |
 *                    |                      |               },                        |
 * +-------+          |                      |               [[Prototype]]: --+        |
 * | book2 |          |                      |                                |        |
 * +-------+          |                      |                                |        |
 * [[Prototype]]: ----+                      |    +--------------+            |        |
 * title: 'Second'                           |    | Function f() |            |        |
 * readPage f(),                             |    +--------------+            |        |
 *                                           +--> prototype f(): { <----------+        |
 *                                                  [[Prototype]]: Object.prototype,   |
 *                                                  constructor: Function f()          |
 *                                                },                                   |
 *                                                [[Prototype]]: ----------------------+
 */

book1.book1Method = (string) => {
  console.log(string);
};

Book.bookObjectMethod = (string) => {
  console.log(string);
};

Book.prototype.bookProtoMethod = (string) => {
  console.log(string);
};
```

```sh
-------- Method Comparison --------
┌─────────────────────────────────────────────┬──────────┬──────────────────┬────────┐
│                  operand1                   │ operator │     operand2     │ result │
├─────────────────────────────────────────────┼──────────┼──────────────────┼────────┤
│     'book1.hasOwnProperty("readPage")'      │    ''    │        ''        │  true  │
│     'book2.hasOwnProperty("readPage")'      │    ''    │        ''        │  true  │
│     'book1.hasOwnProperty("title")'         │    ''    │        ''        │  true  │
│     'book2.hasOwnProperty("title")'         │    ''    │        ''        │  true  │
│      'Book.hasOwnProperty("readPage")'      │    ''    │        ''        │ false  │
│ 'Book.prototype.hasOwnProperty("readPage")' │    ''    │        ''        │ false  │
│              'book1.readPage'               │  '==='   │ 'book2.readPage' │ false  │
└─────────────────────────────────────────────┴──────────┴──────────────────┴────────┘

-------- book1 Prototype Chain --------
┌──────────────────────────────┬──────────┬────────────────────┬────────┐
│           operand1           │ operator │      operand2      │ result │
├──────────────────────────────┼──────────┼────────────────────┼────────┤
│      'book1.__proto__'       │  '==='   │  'Book.prototype'  │  true  │
│  'Book.prototype.__proto__'  │  '==='   │ 'Object.prototype' │  true  │
│ 'Object.prototype.__proto__' │  '==='   │       'null'       │  true  │
└──────────────────────────────┴──────────┴────────────────────┴────────┘

-------- book2 Prototype Chain --------
┌──────────────────────────────┬──────────┬────────────────────┬────────┐
│           operand1           │ operator │      operand2      │ result │
├──────────────────────────────┼──────────┼────────────────────┼────────┤
│      'book2.__proto__'       │  '==='   │  'Book.prototype'  │  true  │
│  'Book.prototype.__proto__'  │  '==='   │ 'Object.prototype' │  true  │
│ 'Object.prototype.__proto__' │  '==='   │       'null'       │  true  │
└──────────────────────────────┴──────────┴────────────────────┴────────┘

-------- Book Prototype Chain --------
┌────────────────────────────────┬──────────┬──────────────────────┬────────┐
│           operand1             │ operator │       operand2       │ result │
├────────────────────────────────┼──────────┼──────────────────────┼────────┤
│       'Book.__proto__'         │  '==='   │ 'Function.prototype' │  true  │
│ 'Function.prototype.__proto__' │  '==='   │  'Object.prototype'  │  true  │
│  'Book.prototype.__proto__'    │  '==='   │  'Object.prototype'  │  true  │
│ 'Object.prototype.__proto__'   │  '==='   │        'null'        │  true  │
└────────────────────────────────┴──────────┴──────────────────────┴────────┘

-------- Constructor Chain --------
┌──────────────────────────────┬──────────┬────────────┬────────┐
│           operand1           │ operator │  operand2  │ result │
├──────────────────────────────┼──────────┼────────────┼────────┤
│     'book1.constructor'      │  '==='   │   'Book'   │  true  │
│     'book2.constructor'      │  '==='   │   'Book'   │  true  │
│ 'Book.prototype.constructor' │  '==='   │   'Book'   │  true  │
│      'Book.constructor'      │  '==='   │ 'Function' │  true  │
└──────────────────────────────┴──────────┴────────────┴────────┘

-------- Output --------
┌────────────────────────────────────────────────────────────┬────────────────────────────────────────────────────────┐
│          code                                              │                     output                             │
├────────────────────────────────────────────────────────────┼────────────────────────────────────────────────────────┤
│                     'book1.readPage()'                     │              'Reading next page of First.'             │
│                     'book2.readPage()'                     │              'Reading next page of Second.'            │
│            'book1.book1Method("book1 method")'             │                     'book1 method'                     │
│ 'book1.bookObjectMethod("book1 --> book object method")'   │ 'TypeError: book1.bookObjectMethod is not a function'  │
| 'book1.bookProtoMethod("book1 --> Book.prototype method")' |            'book1 --> Book.prototype method'           |
|    'book2.book1Method("book2 --> book1 object method")'    | 'TypeError: book2.book1ObjectMethod is not a function' | 
| 'book2.bookProtoMethod("book2 --> Book prototype method")' |            'book2 --> Book.prototype method'           |
└────────────────────────────────────────────────────────────┴────────────────────────────────────────────────────────┘
```

# Example method delegation to prototype
This example demonstrates explicitly setting the prototype for instances in a constructor function. We tell instances of the `Book` constructor function to use `BookPrototype` as their prototype. Instances of the `Book` constructor function delegate `readPage` to the `BookPrototype` object. Therefore, for any properties that are not found in a Book instance, JavaScript looks up the prototype chain to the `BookPrototype` object before looking in the `Object.prototype` object.

With this approach, there is less duplication as methods inherited from the parent are the same for every child instance. The `BookPrototype` object defines the only `readPage` method, creating only one object that's shared by all Book instances. All `Book` instances delegate `readPage` to the `BookPrototype` object.

This approach is better for memory management because it only requires one copy of a method instead of many copies.

```js
const BookPrototype = {
  readPage() {
    console.log(`Reading next page of ${this.title}.`);
  }
};

function Book (title) {
  Object.setPrototypeOf(this, BookPrototype);
  this.title = title;
}

const book1 = new Book('First');
const book2 = new Book('Second');

/*
 *                            +----------+
 *                            | Book f() | <-----+
 *                            +----------+       |  +-------------------------------+
 * +-------+                  prototype: {       |  |                               |
 * | book1 |                    [[Prototype]]: --|--+     +------------+            |
 * +-------+                    constructor: ----+        | Object f() | <-------+  |
 * [[Prototype]]: --+         },                          +------------+         |  |
 * title: 'First',  |         [[Prototype]]: --+  +-----> prototype {  <---------|--+
 *                  |                          |  |         [[Prototype]]: null, |
 *                  |                          |  |         constructor: --------+
 * +-------+        +---------------+          |  |       },
 * | book2 |                        |          |  |        [[Prototype]]: ---------+
 * +-------+                        |          |  |                                |
 * [[Prototype]]: --+               V          |  |                                |
 * title: 'Second', |      +---------------+   |  |                                |
 *                  +----> | BookPrototype |   |  |      +--------------+          |
 *                         +---------------+   |  |      | Function f() |          |
 *                         [[Prototype]]:------|--+      +--------------+          |
 *                         readPage f(),       +-------> prototype f(): { <--------+
 *                                                         [[Prototype]]: Object.prototype,
 *                                                         constructor: Function f(),
 *                                                       },
 *                                                       [[Prototype]]: Object.prototype,
 */
```

```sh
-------- Method Comparison --------
┌─────────────────────────────────────────────┬──────────┬──────────────────┬────────┐
│                  operand1                   │ operator │     operand2     │ result │
├─────────────────────────────────────────────┼──────────┼──────────────────┼────────┤
│     'book1.hasOwnProperty("readPage")'      │    ''    │        ''        │ false  │
│     'book2.hasOwnProperty("readPage")'      │    ''    │        ''        │ false  │
│     'book1.hasOwnProperty("title")'         │    ''    │        ''        │  true  │
│     'book2.hasOwnProperty("title")'         │    ''    │        ''        │  true  │
│      'Book.hasOwnProperty("readPage")'      │    ''    │        ''        │ false  │
│ 'Book.prototype.hasOwnProperty("readPage")' │    ''    │        ''        │ false  │
│              'book1.readPage'               │  '==='   │ 'book2.readPage' │  true  │
│ 'BookPrototype.hasOwnProperty("readPage")'  │    ''    │        ''        │  true  │
└─────────────────────────────────────────────┴──────────┴──────────────────┴────────┘

-------- book1 Prototype Chain --------
┌──────────────────────────────┬──────────┬────────────────────┬────────┐
│           operand1           │ operator │      operand2      │ result │
├──────────────────────────────┼──────────┼────────────────────┼────────┤
│      'book1.__proto__'       │  '==='   │  'Book.prototype'  │ false  │
│      'book1.__proto__'       │  '==='   │   'BookPrototype'  │  true  │
│   'BookPrototype.__proto__'  │  '==='   │ 'Object.prototype' │  true  │
│ 'Object.prototype.__proto__' │  '==='   │       'null'       │  true  │
└──────────────────────────────┴──────────┴────────────────────┴────────┘

-------- book2 Prototype Chain --------
┌──────────────────────────────┬──────────┬────────────────────┬────────┐
│           operand1           │ operator │      operand2      │ result │
├──────────────────────────────┼──────────┼────────────────────┼────────┤
│      'book2.__proto__'       │  '==='   │  'Book.prototype'  │ false  │
│      'book2.__proto__'       │  '==='   │   'BookPrototype'  │  true  │
│   'BookPrototype.__proto__'  │  '==='   │ 'Object.prototype' │  true  │
│ 'Object.prototype.__proto__' │  '==='   │       'null'       │  true  │
└──────────────────────────────┴──────────┴────────────────────┴────────┘

-------- Book Prototype Chain --------
┌────────────────────────────────┬──────────┬──────────────────────┬────────┐
│           operand1             │ operator │       operand2       │ result │
├────────────────────────────────┼──────────┼──────────────────────┼────────┤
│       'Book.__proto__'         │  '==='   │ 'Function.prototype' │  true  │
│ 'Function.prototype.__proto__' │  '==='   │  'Object.prototype'  │  true  │
│  'Book.prototype.__proto__'    │  '==='   │  'Object.prototype'  │  true  │
│ 'Object.prototype.__proto__'   │  '==='   │        'null'        │  true  │
└────────────────────────────────┴──────────┴──────────────────────┴────────┘

-------- Constructor Chain --------
┌──────────────────────────────┬──────────┬────────────┬────────┐
│           operand1           │ operator │  operand2  │ result │
├──────────────────────────────┼──────────┼────────────┼────────┤
│     'book1.constructor'      │  '==='   │   'Book'   │ false  │
│     'book2.constructor'      │  '==='   │   'Book'   │ false  │
│ 'Book.prototype.constructor' │  '==='   │   'Book'   │  true  │
│      'Book.constructor'      │  '==='   │ 'Function' │  true  │
│ 'BookPrototype.constructor'  │  '==='   │  'Object'  │  true  │
└──────────────────────────────┴──────────┴────────────┴────────┘

-------- Output --------
┌─────────────────────┬────────────────────────────────┐
│        code         │             output             │
├─────────────────────┼────────────────────────────────┤
│ 'book1.readPage();' │ 'Reading next page of First.'  │
│ 'book2.readPage()'  │ 'Reading next page of Second.' │
└─────────────────────┴────────────────────────────────┘
```

# Example of more explicit prototype delegation
This example shows that we can add properties directly to JavaScript functions because JavaScript functions are objects. Once we add the `myPrototype` property to the `Book` constructor function, we can set the prototype of Book instances to point to the object referenced by the `myPrototype` property of `Book`.

We're closer to our first example in terms of delegation, but this code is much more succinct and logical. We can clearly see that the `title` property and `readPage()` method are part of Book objects.

The lines with `=` and `#` are lines that have been changed in relation to the example above.

```js
function Book (title) {
  Object.setPrototypeOf(this, Book.myPrototype);
  this.title = title;
}

Book.myPrototype = {
  readPage() {
    console.log(`Reading next page of ${this.title}.`);
  }
};

const book1 = new Book('First');
const book2 = new Book('Second');

/*
 *                         +----------+                               
 *                         | Book f() | <-------+
 *                         +----------+         |  +--------------------------------+
 * +-------+               prototype: {         |  |                                |
 * | book1 |                 [[Prototype]]: ----|--+      +------------+            |
 * +-------+                 constructor: ------+         | Object f() | <-------+  |
 * [[Prototype]]: ====+    },                             +------------+         |  |
 * title: 'First',    #    [[Prototype]]: --------+  +--> prototype {  <---------|--+
 *                    +==> myPrototype { <===+    |  |      [[Prototype]]: null, |
 *                           [[Prototype]]:--#----|--+      constructor: --------+
 *                           readPage f(),   #    |       },
 * +-------+               },                #    |       [[Prototype]]: -----------+
 * | book2 |                                 #    |                                 |
 * +-------+                                 #    |                                 |
 * [[Prototype]]: ===========================+    |       +--------------+          |
 * title: 'Second',                               |       | Function f() |          |
 *                                                |       +--------------+          |
 *                                                +-----> prototype f(): { <--------+
 *                                                          [[Prototype]]: Object.prototype,
 *                                                          constructor: Function f(),
 *                                                        },
 *                                                        [[Prototype]]: Object.prototype,
 */
```

```sh
-------- Method Comparison --------
┌───────────────────────────────────────────────┬──────────┬──────────────────────────────┬────────┐
│                   operand1                    │ operator │           operand2           │ result │
├───────────────────────────────────────────────┼──────────┼──────────────────────────────┼────────┤
│      'book1.hasOwnProperty("readPage")'       │    ''    │              ''              │ false  │
│      'book2.hasOwnProperty("readPage")'       │    ''    │              ''              │ false  │
│        'book1.hasOwnProperty("title")'        │    ''    │              ''              │  true  │
│        'book2.hasOwnProperty("title")'        │    ''    │              ''              │  true  │
│       'Book.hasOwnProperty("readPage")'       │    ''    │              ''              │ false  │
│  'Book.prototype.hasOwnProperty("readPage")'  │    ''    │              ''              │ false  │
│               'book1.readPage'                │  '==='   │ 'Book.prototype.myPrototype' │  true  │
│               'book2.readPage'                │  '==='   │ 'Book.prototype.myPrototype' │  true  │
│               'book1.readPage'                │  '==='   │       'book2.readPage'       │  true  │
│     'Book.hasOwnProperty("myPrototype")'      │    ''    │              ''              │  true  │
│ 'Book.myPrototype.hasOwnProperty("readPage")' │    ''    │              ''              │  true  │
└───────────────────────────────────────────────┴──────────┴──────────────────────────────┴────────┘

-------- book1 Prototype Chain --------
┌──────────────────────────────┬──────────┬────────────────────┬────────┐
│           operand1           │ operator │      operand2      │ result │
├──────────────────────────────┼──────────┼────────────────────┼────────┤
│       'book1.__proto'        │  '==='   │  'Book.prototype'  │ false  │
│       'book1.__proto'        │  '==='   │ 'Book.myPrototype' │  true  │
│ 'Book.myPrototype.__proto__' │  '==='   │ 'Object.prototype' │  true  │
│ 'Object.prototype.__proto__' │  '==='   │       'null'       │  true  │
└──────────────────────────────┴──────────┴────────────────────┴────────┘

-------- book2 Prototype Chain --------
┌──────────────────────────────┬──────────┬────────────────────┬────────┐
│           operand1           │ operator │      operand2      │ result │
├──────────────────────────────┼──────────┼────────────────────┼────────┤
│       'book2.__proto'        │  '==='   │  'Book.prototype'  │ false  │
│       'book2.__proto'        │  '==='   │ 'Book.myPrototype' │  true  │
│ 'Book.myPrototype.__proto__' │  '==='   │ 'Object.prototype' │  true  │
│ 'Object.prototype.__proto__' │  '==='   │       'null'       │  true  │
└──────────────────────────────┴──────────┴────────────────────┴────────┘

-------- Book Prototype Chain --------
┌────────────────────────────────┬──────────┬──────────────────────┬────────┐
│           operand1             │ operator │       operand2       │ result │
├────────────────────────────────┼──────────┼──────────────────────┼────────┤
│       'Book.__proto__'         │  '==='   │ 'Function.prototype' │  true  │
│ 'Function.prototype.__proto__' │  '==='   │  'Object.prototype'  │  true  │
│  'Book.prototype.__proto__'    │  '==='   │  'Object.prototype'  │  true  │
│ 'Object.prototype.__proto__'   │  '==='   │        'null'        │  true  │
└────────────────────────────────┴──────────┴──────────────────────┴────────┘

-------- Constructor Chain --------
┌────────────────────────────────┬──────────┬────────────┬────────┐
│            operand1            │ operator │  operand2  │ result │
├────────────────────────────────┼──────────┼────────────┼────────┤
│      'book1.constructor'       │  '==='   │  'Object'  │  true  │
│      'book2.constructor'       │  '==='   │  'Object'  │  true  │
│  'Book.prototype.constructor'  │  '==='   │   'Book'   │  true  │
│       'Book.constructor'       │  '==='   │ 'Function' │  true  │
│ 'Book.myPrototype.constructor' │  '==='   │  'Object'  │  true  │
└────────────────────────────────┴──────────┴────────────┴────────┘

-------- Output --------
┌─────────────────────┬────────────────────────────────┐
│        code         │             output             │
├─────────────────────┼────────────────────────────────┤
│ 'book1.readPage();' │ 'Reading next page of First.'  │
│ 'book2.readPage()'  │ 'Reading next page of Second.' │
└─────────────────────┴────────────────────────────────┘
```

# Delegation using the Function Prototype
We can make the connection between properties and `Book` objects even more explicit by taking advantage of the fact that all functions in JavaScript have a `prototype` property. We can make use of the **function prototype** which emulates the way we set the prototype ourselves in the above code. This allows us to write more readable code with fewer lines because we no longer have to explicitly set the function prototype.

Whenever we call a function with the `new` keyword, JavaScript assigns the object referenced by the constructor's function prototype to the returned object's `[[prototype]]` or `__proto__` property. In other words, the constructor creates an object that delegates to the constructor function's `prototype` property. Or, another way to say it, the constructor creates an object with a `prototype` property that references the same object as the object referenced by the constructor function's `prototype` property.

```js
function Book (title) {
  this.title = title;
}

Book.prototype.readPage = function () {
  console.log(`Reading next page of ${this.title}.`);
};

const book1 = new Book('First');
const book2 = new Book('Second');

/*
 *                          +----------+
 *                          | Book f() | <------+
 *                          +----------+        |
 * +-------+        +=====> prototype: {        |
 * | book1 |        #       ^ [[Prototype]]: ---|----+     +------------+
 * +-------+        #      #  readPage f(),     |    |     | Object f() | <-------+
 * [[Prototype]]: ==+     #   constructor: -----+    |     +------------+         |
 * title: 'First',       #  },                       +---> prototype { <----------|---+
 *                      #   [[Prototype]]: ---+              [[Prototype]]: null, |   |
 *                     #                      |              constructor: --------+   |
 * +-------+           #                      |            },                         |
 * | book2 |           #                      |            [[Prototype]]: ---+        |
 * +-------+           #                      |                              |        |
 * [[Prototype]]: =====+                      |    +--------------+          |        |
 * title: 'Second',                           |    | Function f() |          |        |
 *                                            |    +--------------+          |        |
 *                                            +--> prototype f(): { <--------+        |
 *                                                   [[Prototype]]: Object.prototype, |
 *                                                   constructor: Function f(),       |
 *                                                 },                                 |
 *                                                 [[Prototype]]: --------------------+
 *                                             
 */
```

```sh
-------- Method Comparison --------
┌─────────────────────────────────────────────┬──────────┬───────────────────────────┬────────┐
│                  operand1                   │ operator │         operand2          │ result │
├─────────────────────────────────────────────┼──────────┼───────────────────────────┼────────┤
│     'book1.hasOwnProperty("readPage")'      │    ''    │            ''             │ false  │
│     'book2.hasOwnProperty("readPage")'      │    ''    │            ''             │ false  │
│       'book1.hasOwnProperty("title")'       │    ''    │            ''             │  true  │
│       'book2.hasOwnProperty("title")'       │    ''    │            ''             │  true  │
│      'Book.hasOwnProperty("readPage")'      │    ''    │            ''             │ false  │
│     'Book.hasOwnProperty("prototype")'      │    ''    │            ''             │  true  │
│ 'Book.prototype.hasOwnProperty("readPage")' │    ''    │            ''             │  true  │
│              'book1.readPage'               │  '==='   │ 'Book.prototype.readPage' │  true  │
│              'book2.readPage'               │  '==='   │ 'Book.prototype.readPage' │  true  │
│              'book1.readPage'               │  '==='   │     'book2.readPage'      │  true  │
└─────────────────────────────────────────────┴──────────┴───────────────────────────┴────────┘

-------- book1 Prototype Chain --------
┌──────────────────────────────┬──────────┬────────────────────┬────────┐
│           operand1           │ operator │      operand2      │ result │
├──────────────────────────────┼──────────┼────────────────────┼────────┤
│      'book1.__proto__'       │  '==='   │  'Book.prototype'  │  true  │
│  'Book.prototype.__proto__'  │  '==='   │ 'Object.prototype' │  true  │
│ 'Object.prototype.__proto__' │  '==='   │       'null'       │  true  │
└──────────────────────────────┴──────────┴────────────────────┴────────┘

-------- book2 Prototype Chain --------
┌──────────────────────────────┬──────────┬────────────────────┬────────┐
│           operand1           │ operator │      operand2      │ result │
├──────────────────────────────┼──────────┼────────────────────┼────────┤
│      'book2.__proto__'       │  '==='   │  'Book.prototype'  │  true  │
│  'Book.prototype.__proto__'  │  '==='   │ 'Object.prototype' │  true  │
│ 'Object.prototype.__proto__' │  '==='   │       'null'       │  true  │
└──────────────────────────────┴──────────┴────────────────────┴────────┘

-------- Book Prototype Chain --------
┌────────────────────────────────┬──────────┬──────────────────────┬────────┐
│            operand1            │ operator │       operand2       │ result │
├────────────────────────────────┼──────────┼──────────────────────┼────────┤
│        'Book.__proto__'        │  '==='   │ 'Function.prototype' │  true  │
│ 'Function.prototype.__proto__' │  '==='   │  'Object.prototype'  │  true  │
│   'Book.prototype.__proto__'   │  '==='   │  'Object.prototype'  │  true  │
│  'Object.prototype.__proto__'  │  '==='   │        'null'        │  true  │
└────────────────────────────────┴──────────┴──────────────────────┴────────┘

-------- Constructor Chain --------
┌──────────────────────────────┬──────────┬────────────┬────────┐
│           operand1           │ operator │  operand2  │ result │
├──────────────────────────────┼──────────┼────────────┼────────┤
│     'book1.constructor'      │  '==='   │  'Object'  │ false  │
│     'book2.constructor'      │  '==='   │  'Object'  │ false  │
│ 'Book.prototype.constructor' │  '==='   │   'Book'   │  true  │
│      'Book.constructor'      │  '==='   │ 'Function' │  true  │
│ 'Book.prototype.constructor' │  '==='   │  'Object'  │ false  │
└──────────────────────────────┴──────────┴────────────┴────────┘

-------- Output --------
┌─────────────────────┬────────────────────────────────┐
│        code         │             output             │
├─────────────────────┼────────────────────────────────┤
│ 'book1.readPage();' │ 'Reading next page of First.'  │
│ 'book2.readPage()'  │ 'Reading next page of Second.' │
└─────────────────────┴────────────────────────────────┘
```