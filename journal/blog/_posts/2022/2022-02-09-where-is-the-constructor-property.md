---
layout: post
title: Where is the constructor property?
date: 2022-02-09 14:00:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js225 inheritance objects constructor_functions prototype
---

When we reference the `constructor` property of an object, JavaScript uses the object prototype chain. 

<!--more-->

Let's start with the most basic object we can think of:

```js
const obj = {};
```

Here, we have an object `obj` that, like all other objects, delegates to the object referenced by the `Object.prototype` property.

Looking at the `constructor` property more deeply, we can see that the `constructor` property doesn't exist by default in objects:

```js
obj.hasOwnProperty('constructor'); // false
```

When we attempt to reference the `constructor` property of an object, JavaScript looks up the prototype chain to the object's prototype (`[[Prototype]]`). In this case, the object referenced by `Object.prototype` is the `[[Prototype]]` of `obj`:

```js
Object.prototype.isPrototypeOf(obj); // true
```

JavaScript will now return the object that is referenced by the `constructor` property within the object referenced by `Object.prototype`: 

```js
Object.getPrototypeOf(obj) === Object.prototype; // true
Object.prototype.hasOwnProperty('constructor');  // true
```

A diagram of this delegation might look like this:

```js
/*
 * +-----+             +------------+
 * | obj |             | Object f() | <--+
 * +-----+             +------------+    |
 * [[Prototype]] ----> prototype: {      |
 *                       constructor: ---+
 *                       [[Prototype]] --------+
 *                     },                      |
 *                                             |
 *                     +--------------+        |
 *                     |  Function f() | <--+  |
 *                     +--------------+     |  |
 *                     prototype: f() { <---|--+
 *                       constructor: ------+
 *                     },
 */
```

From this, we can gather that the `constructor` property is a default property of the object referenced by the `prototype` property, which itself is a default property of all functions. The `constructor` property references the function itself by default, in most cases. The `prototype` property of functions is not always useful, but it's there if we need it.

Thinking about objects, we know they don't have a default `constructor` property, as we saw above.

This is important when one is working with object delegation and inheritance:

```js
const obj = {
  name: 'myName',
};

const newObj = Object.create(obj);

newObj.hasOwnProperty('name');        // false
newObj.name;                          // myName
newObj.hasOwnProperty('constructor'); // false
newObj.constructor;                   // [Function: Object]
```

The object that's returned and assigned to `newObj` is a new object with its `[[Prototype]]` set to `obj`:

```js
Object.getPrototypeOf(newObj) === obj; // true
```

In the background, JavaScript looks for a `constructor` property within `newObj`. It doesn't find one, so it delegates up the prototype chain to `obj`. It doesn't find a `constructor` property there, so it delegates up the prototype chain to `Object.prototype`. At this point, JavaScript finds the `constructor` property and returns the object that it references.

```js
newObj.constructor === Object.prototype.constructor; // true
```

This is OK, but what we probably want in this case is for the `constructor` property of `newObj` to reference the `obj` object, since that's technically the object that `newObj` was created from. Having a knowledge of how JavaScript finds the `constructor` property and the prototype chain, we can explicitly assign a `constructor` property to `obj`, referencing `obj` itself. Then, when we reference the `constructor` property of object's created from `obj`, we'll get `obj`:

```js
const obj = {
  name: 'myName',
};

obj.constructor = obj;
Object.getOwnPropertyNames(obj);   // [ 'name', 'constructor' ]

const newObj = Object.create(obj);

newObj.name;                       // myName
newObj.constructor === obj;        // true
```

In summary, `obj.constructor` doesn't exist by default. If we create an object from `obj` and reference the `constructor` property without explicitly declaring the `constructor` property of `obj`, JavaScript delegates up the prototype chain until it finds a `constructor` property and returns the object it references.