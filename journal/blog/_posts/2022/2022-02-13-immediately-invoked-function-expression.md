---
layout: post
title: JavaScript's Immediately Invoked Function Expression (IIFE)
date: 2022-02-14 18:50:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js225 IIFE objects functions
---

In the dark days of JavaScript, there was only function scope. This was a difficult time. Data was exposed. Code was more difficult to follow. Life was hard.

<!--more-->

Software engineers needed to find a way to encapsulate data so it wouldn't pollute the global environment while still allowing methods to access the data, like languages that have block scope. In order to simulate block scope and protect the global environment, they had to use a programming idiom known as an immediately invoked function expression or IIFE (many say "iffy") for short.

```js
// ... other code

function test () {
  console.log(1);  // 3
};

test();

// ... other code

function test () {
  console.log(2);  // 3
};

test();

// ... other code

function test () {
  console.log(3);  // 3
};

test();
```

In the above code, we've accidentally defined the same function three times in different parts of our code. Since our code is long and unwieldy, we may not notice we've done this. Due to hoisting, the logged value will be `3` for the rest of the program, thereby breaking previous code. The second `test` function definition overrides the first. Then, the third overrides the second. I can imagine something like this being difficult to debug in large projects.

That's where IIFEs come in. An IIFE is a function that is defined and then immediately invoked.

```js
// ... other code

function test () {
  console.log(1);
};

test();             // 1

// ... other code

(function test () {
  console.log(2);   // 2
})();

// ... other code

(function test () {
  console.log(3);   // 3
})();

test();             // 1
```

In the above code, we define the last two functions as function expressions instead of a function declarations. They are immediately invoked after each is defined. Since they're immediately invoked in a private scope, the original `test` function is not overridden.

IIFEs can also help us create private data that is not accessible from outside of the IIFE.

```js
const addBook = (function () {
  const books = [];
  let id = 0;

  return function (title, author) {
    books.push({
      title,
      author,
    });

    id += 1;

    console.log(`Added: { title: ${title}, author: ${author}, bookId: ${id} }`);
  };
})();

console.log(addBook); // [Function (anonymous)]

addBook('myTitle', 'myAuthor');
addBook('mySecondTitle', 'mySecondAuthor');
addBook('myThirdTitle', 'myThirdAuthor');

// Output:
// Added: { title: myTitle, author: myAuthor, bookId: 1 }
// Added: { title: mySecondTitle, author: mySecondAuthor, bookId: 2 }
// Added: { title: myThirdTitle, author: myThirdAuthor, bookId: 3 }
```

The IIFE defines an anonymous function with the `books` and `id` variables declared and initialized to an empty array and `0` respectively. The function returns another anonymous function (lines 5-12) that creates a closure, i.e. it closes over all of the current variables in scope. This gives the inner anonymous function access to the `book` and `id` values even after the IIFE is finished executing. We immediately invoke the outer anonymous function so the variable `addBook` now references the returned inner anonymous function.

We can now invoke the anonymous function referenced by `addBook` (lines 17-19). This pushes a new book to the private `books` array and increments the private `id` by `1`. It then logs this information to the console.

As we can see below, we cannot access the `books` and `id` variables directly from outside of the IIFE:

```js
console.log(books);
// ReferenceError: counter is not defined

console.log(id);
// ReferenceError: id is not defined
```

The above is how we can use an IIFE to create private data. In this case, the `books` and `id` variables are private and only accessible from within the IIFE.

We can also use an IIFE to return an **object** that maintains access to private variables:

```js
const library = (function () {
  const books = [];
  let id = 0;

  return {
    add(title) {
      id += 1;

      books.push({
        id,
        title,
      });
      console.log(`Added: (ID: ${id}) ${title}`);
    },

    displayAll() {
      console.log('\nID\tTitle\n--\t-----');
      books.forEach((book) => {
        console.log(`${book.id}\t\t${book.title}`);
      });
    },
  };
})();

library.add('Gone Now');        // Added: (ID: 1) Gone Now
library.add('Back Again');      // Added: (ID: 2) Back Again
library.add('Splitting Heros'); // Added: (ID: 3) Splitting Heros

library.displayAll();
//
// ID      TITLE
// --      -----
// 1       Gone Now
// 2       Back Again
// 3       Splitting Heros
```

The above code is similar to the previous example, but instead of returning an anonymous function, we're returning an object that has private data. The `books` and `id` variables are private:

```js
console.log(library);       // { add: [Function: add], displayAll: [Function: displayAll] }
console.log(library.id);    // undefined
console.log(library.books); // undefined

// console.log(books);
// ReferenceError: books is not defined

// console.log(id);
// ReferenceError: id is not defined
```

This allows us to have greater control over the data we want to make available via methods.

For example, if we wanted to be able to access a book by its `id` property, we can add the following `displayBook` method to the object returned from the IIFE:

```js
const library = (function () {
  const books = [];
  let id = 0;

  return {
    add (title) {
      id += 1;

      books.push({
        id,
        title,
      });
      console.log(`Added: (ID: ${id}) ${title}`);
    },

    displayAllBooks () {
      console.log('\nID\tTitle\n--\t-----');
      books.forEach((book) => {
        console.log(`${book.id}\t\t${book.title}`);
      });
    },

    displayBook (id) {
      const book = books.filter(book => book.id === id)[0];
      Object.keys(book).forEach(property => {
        console.log(`${property}: ${book[property]}`);
      });
    },
  };
})();

library.add('Gone Now');        // Added: (ID: 1) Gone Now
library.add('Back Again');      // Added: (ID: 2) Back Again
library.add('Splitting Heros'); // Added: (ID: 3) Splitting Heros

library.displayBook(2);
// id: 2
// title: Back Again

console.log(library);
// {
//   add: [Function: add],
//   displayAllBooks: [Function: displayAllBooks],
//   displayBook: [Function: displayBook]
// }
```

Those are some of the uses for IIFEs in JavaScript.