---
layout: post
title: Partial Function Application
date: 2022-02-23 15:00:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js225 functions
---

Partial function application is a general software engineering concept that refers to a process involving three functions. The first function creates the second function and the second function calls the third function.

<!--more-->

## The Basics

The first function takes one or more of the third function's arguments and creates the second function. The second function takes the rest of the third function's arguments and calls the third function. We pass in the arguments from the first and second functions to the third function. This essentially reduces the number of arguments needed to call the third function.

In order for something to be said to use partial function application, there must be a *reduction* in the number of arguments needed when a function is called.

```js
// this function cannot be changed
function addCategory (category, title) {
  return `${category}: ${title}`;
}

function categoryAdder (category) {
  return function (title) {
    return addCategory(category, title);
  };
}

const addFictionCategory = categoryAdder('Fiction');
const addNonfictionCategory = categoryAdder('Nonfiction');

console.log(addFictionCategory('My Title'));                        // Fiction: My Title
console.log(addFictionCategory('Fiction Title'));                   // Fiction: Fiction Title
console.log(addNonfictionCategory('About Me'));                     // Nonfiction: About Me
console.log(addNonfictionCategory('Nonfiction Title'));             // Nonfiction: Nonfiction Title
```

In the above, the `categoryAdder` function is the first function, the anonymous function returned from `categoryAdder` is the second function, and the `addCategory` function is the third function.

We can see that if we were to only use the `addCategory` function to create a category, we'd have to pass in two arguments each time, `category` and `title`. With partial function application, we can pass in one argument, `category`, to the first function, `categoryAdder`. Then, when we call the anonymous function returned from `categoryAdder`, we can pass in the second argument `title`. At this point, we call the third function `addCategory` and pass in the values of the `category` and `title` variables.

The first argument is passed in when the first function `categoryAdder` is called. The second function, the anonymous function returned from the `categoryAdder` function, uses a closure to enclose or close over the variables currently in scope. Here, the `category` variable is currently in scope. That closure enables us to call the function returned from the `categoryAdder` function, now assigned to `addFictionCategory` and `addNonfictionCategory` respectively, at a later time. We can pass in one argument for the `title` and the returned string contains the values of both `category` and `title`.

Now, we only need to pass one argument to the `addFictionCategory` and `addNonFictionCategory` functions and the `addCategory` function will have the two arguments it needs.

This is a contrived example since there are better ways to do this, but it demonstrates partial function application nicely.

Here's an example that looks like partial function application, but isn't:

```js
const books = [];

// this function cannot be changed
function addBook (book) {
  books.push(book);
}

function categoryAdder (category) {
  return function (title) {
    return addBook(`${category}: ${title}`);
  };
}

const addFictionCategory = categoryAdder('Fiction');
const addNonfictionCategory = categoryAdder('Nonfiction');

addFictionCategory('My Title');
addFictionCategory('Fiction Title');
addNonfictionCategory('About Me');
addNonfictionCategory('Nonfiction Title');

console.log(books.join('\n'));

/* Output:
 *
 * Fiction: My Title
 * Fiction: Fiction Title
 * Nonfiction: About Me
 * Nonfiction: Nonfiction Title
 *
 */
```

The above is **not** partial function application because both the `categoryAdder` function and the `addBook` function take one argument. There's no reduction in the number of arguments needed. Even though we can see that the `categoryAdder` function takes one argument and the function returned from `categoryAdder` takes a second argument, the `addBook` function only takes one argument. Ultimately, we don't need to write this code in this way.

We can rewrite the above code in a way that doesn't require the `categoryAdder` function since we're supplying both strings when we call the function. We can call `addBook` directly with no problems:

```js
const books = [];

// this function cannot be changed
function addBook (book) {
  books.push(book);
}

addBook('Fiction: My Title');
addBook('Fiction: Fiction Title');
addBook('Nonfiction: About Me');
addBook('Nonfiction: Nonfiction Title');

console.log(books.join('\n'));

/* Output:
 *
 * Fiction: My Title
 * Fiction: Fiction Title
 * Nonfiction: About Me
 * Nonfiction: Nonfiction Title
 *
 */
```

## General Purpose Functions
Even though partial function application sets things up so they're easier to call and allows us to work with functions that cannot be changed, using partial function application in this way isn't efficient. We need to create new functions for each thing we want to add.

To make our function more reusable, we can write them in a way that takes a function for an argument:

```js
const catalogue = [];

// this function cannot be changed
function addBook (category, title) {
  catalogue.push({ type: 'book', category, title });
}

function addVideo (category, title, rating) {
  catalogue.push({ type: 'video', category, title, rating });
}

function adder (func, arg1) {
  return function (...args) {
    return func(arg1, ...args);
  };
}

const addFictionCategory = adder(addBook, 'Fiction');
const addNonfictionCategory = adder(addBook, 'Nonfiction');

addFictionCategory('My Title');
addFictionCategory('Fiction Title');
addNonfictionCategory('About Me');
addNonfictionCategory('Nonfiction Title');

const addHorrorVideo = adder(addVideo, 'Horror');
addHorrorVideo('My Horror', 'R');
addHorrorVideo('My Horror 2', 'R');

console.log(catalogue);

/* Output:
 *
 * [
 *   { type: book, category: "Fiction", title: "My Title" }
 *   { type: book, category: "Fiction", title: "Fiction Title" }
 *   { type: book, category: "Nonfiction", title: "About Me" }
 *   { type: book, category: "Nonfiction", title: "Nonfiction Title" }
 *   { type: video, category: "Horror", title: "My Horror", rating: 'R' }
 *   { type: video, category: "Horror", title: "My Horror2", rating: 'R' }
 * ]
 *
 */
```

Using a more general function for the partial function application, we can reuse that same function as needed. We can make it even more general by changing the name of the function to something like `partial`:

```js
function partial (func, arg1) {
  return function (...args) {
    return func(arg1, ...args);
  };
}
```

## Bind for Partial Function Application
It's also possible to do something similar to partial function application using `Function.prototype.bind` and get the same output:

```js
const catalogue = [];

// this function cannot be changed
function addBook (category, title) {
  catalogue.push({ type: 'book', category, title });
}

function addVideo (category, title, rating) {
  catalogue.push({ type: 'video', category, title, rating });
}

const addFictionCategory = addBook.bind(null, 'Fiction');
const addNonfictionCategory = addBook.bind(null, 'Nonfiction');

addFictionCategory('My Title');
addFictionCategory('Fiction Title');
addNonfictionCategory('About Me');
addNonfictionCategory('Nonfiction Title');

const addHorrorVideo = addVideo.bind(null, 'Horror');
addHorrorVideo('My Horror', 'R');
addHorrorVideo('My Horror 2', 'R');
```

This method is less lines of code, but it may also be a little more opaque since one may not know the ins and outs of `Function.prototype.bind`.