---
layout: post
title: JavaScript Bind Method
date: 2022-04-10 17:00:00 -0500
excerpt_separator: <!--more-->
tags: launch_school JavaScript js225 methods
---

I've been studying for the JS129 written assessment and I decided to solidify my understanding of the `Function.prototype.bind` method. The `bind` method is used to permanently bind a given execution context to a function invocation.

<!--more-->

This post continues where my [Notes on JavaScript Execution Context](../notes-on-javascript-execution-context/) left off.

The `bind` method is helpful because it allows us to always know the execution context of a specific function invocation. This can be helpful when we want a function to work with different contexts and we want to clearly know which context we're working with when we invoke that function.

In order to permanently bind a function to a specific execution context, we can use `bind`. Unlike `call` and `apply`, `bind` does not execute the function. It creates and returns a new function that binds the calling function to the execution context of the object that's passed in as an argument.

# Understanding `bind`
The following is a hypothetical implementation of the `Function.prototype.bind` method:

```js
Function.prototype.myBind = function (context, ...args) {
  const self = this;
  return function () {
    return self.apply(context, [...args]);
  };
}

function myFunc () {
  console.log(this.name);
}

const obj = {
  name: 'My Name',
 }

const boundFunc = myFunc.myBind(obj);
boundFunc();  // My Name
```

Like `Function.prototype.bind`, the `Function.prototype.myBind` method takes an execution context as well as arguments to pass into the calling function.

Starting on line 16:

```JS
const boundFunc = myFunc.myBind(obj);
```

The variable `boundFunc` is declared and initialized to the return value of the `myFunc.myBind(obj)` method call. The `myFunc` function invokes the `myBind` method and passes in the `obj` object as an argument. The `myFunc` function object is now the implicit method execution context for the `myBind` method call. In general, the object the method is called on is the implicit execution context for the method invocation.

```js
const self = this;
```

Inside the `myBind` method definition on line 2, the variable `self` is declared and initialized to the value of `this`, which is a reference to the `myFunc` function object. 

```js
return function () {
  return self.apply(context, [...args]);
};
```

The `myBind` method then returns an anonymous function with a closure containing the given `context` object (`obj`), a reference to the array of passed-in arguments (`args`), and the value of the `self` variable, which is a reference to the `myFunc` function object.

```js
function () {
  return self.apply(context, [...args]);
};
```

The returned anonymous function is then assigned to the `boundFunc` variable back on line 16. The `boundFunc` function now has access to the values of the `self`, `context`, and `args` variables, thanks to the anonymous function's closure. 

```js
boundFunc();  // My Name
```

When we invoke the `boundFunc()` function on line 17, the code on lines 3-5 is executed, returning the result of the `self.apply(context, [...args])` expression.

```js
self.apply(context, [...args]);
```

The `boundFunc()` invocation returns the return value of the expression on line 4. We invoke the `apply` method on the `myFunc` function object via the value of `self`, set the execution context to `obj` via the value of `context`, and pass in an array of arguments (or an empty array in this case) via the result of the `[...args]` expression. 

```js
function myFunc () {
  console.log(this.name);
}
```

Since the `myFunc` function invocation is provided with an explicit function execution context `obj`, `this.name` uses the `name` property of `obj` and logs the string `My Name` to the console.

Going further, we can see that the `boundFunc` execution context is permanently bound to the `obj` object by attempting to invoke the `boundFunc` function with the `Function.prototype.call` method:

```js
const obj2 = { name: 'Another Name', };
boundFunc.call(obj2); // My Name
```

The result is still the value of `obj.name` instead of `obj2.name`. We cannot change the `boundFunc` execution context.

# Using `bind`

```js
const album1 = {
  title: 'myFirstAlbum',

  displayTitle (before = '=', after = '=') {
    if (this.title) {
      console.log(`${before} ${this.title} ${after}`);
    } else {
      console.log('Error');
    }
  },
};

const album2 = {
  title: 'mySecondAlbum',
};

const album1DisplayTitle = album1.displayTitle.bind(album1);

album1DisplayTitle('***', '***');                 // *** myFirstAlbum ***

album1DisplayTitle.call(album2, '<<<', '>>>');    // <<< myFirstAlbum >>>
// can't change execution context with 'call'

album1DisplayTitle.apply(album2, ['<<<', '>>>']); // <<< myFirstAlbum >>>
// can't change execution context with 'apply'
```

In the above code, the execution object `album1` is bound to the `displayTitle` method call and assigned to `album1DisplayTitle`. It's no longer possible to change the execution context for the function referenced by `albumDisplayTitle` by using `call` or `apply`.