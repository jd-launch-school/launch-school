---
layout: post
title: SQL and RDBMS Notes
excerpt_separator: <!--more-->
tags: launch_school sql ls180
---

Learning about SQL (Structured Query Language) and RDBMSes (Relational Database Management Systems) is actually more fun and interesting than I originally thought it would be. There's something magical about being able to store, retrieve, view, and remove structured data so easily. I'm about half of the way through the [Launch School SQL Book](https://launchschool.com/books/sql/read/introduction) so I thought I'd write about what I learned so far.

<!--more-->

After installing PostgreSQL, our RDBMS of choice, we start learning how to interact with it on the command line. Interacting with the RDBMS is similar to interacting with an HTTP server in that the client issues a request to the RDBMS and then the RDBMS replies with a response.

We use the `psql` interactive console, which is basically a REPL for PostgreSQL. We learn there are two types of commands in a `psql` console: **Meta-commands** and **SQL Statements**.

Meta-commands start with a backslash `\` followed by the command and optional arguments, like `\q` to exit `psql`. SQL Statements are a combination of upper and lowercase words that end with a semicolon, something like `SELECT * FROM orders;`.

We then go deeper into SQL and learn about the three sub-languages that make up SQL:

- **DDL** (Data Definition Language)
  + used to define the structure of a database and the tables/columns within it (structure control)
- **DML** (Data Manipulation Language)
  + used to retrieve or modify data that's stored in a database (data control)
- **DCL** (Data Control Language)
  + used to determine what users are allowed to do what when interacting with a database (access control)

We focus on DDL at first to learn about creating and viewing databases, creating and viewing tables and altering tables and columns. You can watch as I go through the exercises in the book ([link to video](https://archive.org/details/postgresql-database-practice)):

<iframe src="https://archive.org/embed/postgresql-database-practice" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

A quick session for creating a database, adding a table, and modifying columns in that table might look like this:

# Create database in the terminal:
```bash
$ createdb test_database
$ psql -d test_database
test_database=#
```

# Create the `test_table`:
```sql
test_database=# CREATE TABLE test_table (
test_database=#     id serial,
test_database=#     name varchar(50) NOT NULL,
test_database=#     description varchar(200),
test_database=#     last_accessed timestamp NOT NULL DEFAULT NOW(),
test_database=#     amount decimal(5, 2) DEFAULT 0
test_database=# );
```

`\d test_table`:
```bash
                              Table "public.test_table"
    Column     |            Type             | Collation | Nullable |                Default
---------------+-----------------------------+-----------+----------+----------------------------------------
 id            | integer                     |           | not null | nextval('test_table_id_seq'::regclass)
 name          | character varying(50)       |           | not null |
 description   | character varying(200)      |           |          |
 last_accessed | timestamp without time zone |           | not null | now()
 amount        | numeric(5,2)                |           |          | 0
```

# Add `first_name` column:
```sql
test_database=# ALTER TABLE test_table
test_database=#   ADD COLUMN first_name varchar(50) NOT NULL;
```
`\d test_table`:

```bash
                                          Table "public.test_table"
    Column     |            Type             | Collation | Nullable |                Default
---------------+-----------------------------+-----------+----------+----------------------------------------
 id            | integer                     |           | not null | nextval('test_table_id_seq'::regclass)
 name          | character varying(50)       |           | not null |
 description   | character varying(200)      |           |          |
 last_accessed | timestamp without time zone |           | not null | now()
 amount        | numeric(5,2)                |           |          | 0
 first_name    | character varying(50)       |           | not null |

```

# Rename `name` column to `last_name`:
```sql
test_database=#  ALTER TABLE test_table
test_database=# RENAME COLUMN name TO last_name;
```
`\d test_table`:
```bash
                                          Table "public.test_table"
    Column     |            Type             | Collation | Nullable |                Default
---------------+-----------------------------+-----------+----------+----------------------------------------
 id            | integer                     |           | not null | nextval('test_table_id_seq'::regclass)
 last_name     | character varying(50)       |           | not null |
 description   | character varying(200)      |           |          |
 last_accessed | timestamp without time zone |           | not null | now()
 amount        | numeric(5,2)                |           |          | 0
 first_name    | character varying(50)       |           | not null |
```

# Modify `last_name` to be able to be `NULL`:
```sql
test_database=# ALTER TABLE test_table
test_database=# ALTER COLUMN last_name DROP NOT NULL;
```

`\d test_table`:
```bash
                                          Table "public.test_table"
    Column     |            Type             | Collation | Nullable |                Default
---------------+-----------------------------+-----------+----------+----------------------------------------
 id            | integer                     |           | not null | nextval('test_table_id_seq'::regclass)
 last_name     | character varying(50)       |           |          |
 description   | character varying(200)      |           |          |
 last_accessed | timestamp without time zone |           | not null | now()
 amount        | numeric(5,2)                |           |          | 0
 first_name    | character varying(50)       |           | not null |
```
