---
layout: post
title: Getting Ready for RB129 OOP Written Assessment
excerpt_separator: <!--more-->
tags: launch_school ruby rb120 oop assessments
---

After finishing the RB120 course proper, I'm now in the phase of studying and preparing for the RB129 written assessment.

<!--more-->

On some level, I feel that studying and preparing for this assessment is more complicated and time-consuming than the preparation needed for the RB109 written assessment. The requirements to pass the RB109 written assessment seemed to be mostly to understand Ruby syntax and be able to use precise language to explain various code snippets and programs.

For the RB129 written assessment, at least as far as I can tell at this point, I feel like the goal is to fully integrate more general object oriented programming concepts into one's understanding of programming in Ruby in such a way that one will be able to clearly and comprehensively explain ALL of the details related to a particular concept.


# RB109 Written Assessment Question Example

In the RB109 written assessment, for example, one might be asked to describe why the following code does not mutate the local variable `string`:

```ruby
def my_method(string)
  string += "else"
end

string = "Something"
my_method(string)

p string
```

    "Something"

It's sufficient for one to point out that `string += "else"` is equivalent to `string = string + "else"` and is therefore reassigning the local variable `string` within the `my_method` method to the String object with value `Something else`. Therefore, the local variable `string` within the `my_method` method now references a different String object than the local variable `string` initialized outside of the `my_method`.


# RB129 Written Assessment Question Example

For the RB129 written assessment, on the other hand, it doesn't seem sufficient to just point out how the code works. We're also required to explain how the code demonstrates a particular object oriented programming concept. The following question is a question multiple people have asked in study sessions so far:

**What is Object Oriented Programming?**

My answer:

Object oriented programming is a programming paradigm that uses classes and objects to take advantage of inheritance, encapsulation, and polymorphism in order to divide a program into different parts that interact with one another, making our code more flexible and reusable.

Starting with classes and objects, classes are like blueprints that objects are based on. We define state and behavior within our classes and then instantiate objects from those classes.

For example, in the following code, we have an `Animal` class that has a name for its state and can walk for its behavior:

```ruby
class Animal
  def initialize(name)
    @name = name
  end

  def walk
    puts "#{name} walks."
  end
end
```

This acts like a blueprint for objects that we can then create or instantiate as needed.

We create objects by defining a class and instantiating the objects by calling the `new` method on the class to create a new instance, also known as an object. In this case, the `new` method calls the `Animal#initialize` method, initializing the `@name` instance variable as part of the new `cat` object's state:

```ruby
cat = Animal.new('Sasha')
```

Once we have instantiated an object from the class, we can access the object's public interface and call public methods on that object:

```ruby
cat.walk   # Sasha walks.
```

By creating classes and objects like this, we can structure our program in a way that often makes it easier to debug, maintain, and think about.

When we're ready to create more objects, we can reuse the `Animal` class like a blueprint without having to duplicate the code within the `Animal` class:

```ruby
dog = Animal.new('Simba')
dog.walk   # Simba walks.
```

The `cat` and `dog` objects are two different objects. Both objects are `Animal` objects. They have different states, i.e. they have different names. Both share the same behavior, i.e. they can both walk.

Object oriented programming is a powerful programming method that allows us to manage complex code in more efficient and readable ways.


# Moving On

My answer to **"What is object oriented programming?"** seems comprehensive, but I wouldn't be surprised to realize after publishing this that some key element of object oriented programming was left out. I doubt this particular question, as open-ended as it is, will be on the RB129 written assessment, but it clearly demonstrates the requirement that one fully understand this general programming concept when articulating one's answer.

By my count, there are aspects of about 20 different concepts we're expected to master in this way for this assessment. That's overwhelming, to say the least.

On the one hand, I feel that I have a strong understanding of most of these concepts. It's now just a matter of writing concise and comprehensive notes and code snippets to help me during the written exam. I feel like I can pass if I do this alone.

On the other hand, I'm paying money to Launch School to have access to a curriculum that will guide me on the path to mastering this material. So, even though it may be sufficient for me to have notes and snippets on these concepts in order to fill in gaps in my memory or remind me of important things I need to write in my answers, doing that alone doesn't necessarily help me internalize these concepts on a level that makes them useful while programming.

Also, future Launch School courses will require that I fully understand and that I can adequately articulate these concepts. Moving through this process faster than I'm comfortable with will likely only lead to future difficulties that I'll eventually need to take time for.


# So Much More to Learn, Yet I've Learned

Prior to taking the RB120 course, my understanding of these concepts was nominal at best. I attempted to write object oriented programming code and even though I vaguely understood that objects are instantiated from classes, I didn't fully understand the usefulness of this in comparison to procedural programming's local or global variables.

Now, even prior to taking the RB129 assessments, I feel I have a much deeper understanding of these concepts. I now have a much stronger base of understanding from which to move forward from.
