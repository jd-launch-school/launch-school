---
layout: post
title: Thoughts About Object State
excerpt_separator: <!--more-->
tags: launch_school ruby oop rb120
---

Understanding an object's state is trickier than it first appears. On the surface, instance variables initialized within a class keep track of an object's state. However, how is an object's state used? What determines which part of an object's state is part of an object's public interface?

<!--more-->

This blog post comes from a question that was given to me recently, "Why are instance Variables Private?" Is this question asking, "Why did Ruby creators make instance variables private?" Or is it asking something like, "What is it about instance variables that makes them necessarily private?"

Also, what does private mean in this context?


# What is an Instance Variable?

An instance variable is a variable that is scoped to an object instantiated from a class. Instance variables are denoted by prefixing an `@` symbol in front of the variable name:

```ruby
class Animal
  def initialize(name)
    @name = name
  end
end
```

In this example, the instance variable `@name` is initialized within the `initialize` method and we assign to it the value of the local variable `name`. We can see that `Animal` objects have this `@name` instance variable set by inspecting them:

```ruby
cat = Animal.new('Rolo')
dog = Animal.new('Fil')

p cat  #<Animal:0x0000556fb6ffcbb8 @name="Rolo">
p dog  #<Animal:0x0000556fb6ffcb68 @name="Fil">
```

Another way to see the instance variable is to call the `Object#instance_variables` method on the `cat` and `dog` objects:

```ruby
p cat.instance_variables  # [:@name]
p dog.instance_variables  # [:@name]
```

This method is available to our objects because all objects inherit from the `Object` class where the `instance_variables` instance method is mixed in via the `kernel` module.


# Instance Variables Keep Track of State

Instance variables keep track of the state of an object. The variables themselves can be inherited by subclasses if the method they're defined within is invoked because they exist at the class level.

The values, i.e. the collaborator objects the instance variables refer to, cannot be inherited. The relationship between the collaborator object and the instance variable takes place at the object level and is unique to a given object.

In our example, `Animal` objects have a name as part of their state. We instantiate two animal objects by initializing local variables and assigning to them new `Animal` objects, passing in String objects with values `Rolo` and `Fil` respectively. These two collaborator objects become the attributes of the object and each object's instance variable `@name` tracks the object's name.


# Why are Instance Variables Private?

I struggled with this question for quite some time. Ultimately, the answer is that instance variables are private because there's no Ruby syntax to refer directly to instance variables. Since instance variables keep track of state and instance methods expose that state as an object's public interface, instance variables themselves remain hidden from the outside. There's no option in Ruby to make instance variables public, so Ruby forces programmers to make use of encapsulation and be conscious of the parts of the an object's state they want in an object's public interface.

Encapsulation is basically hiding or protecting data from being read or modified from the outside.

An object's public interface is the set of public methods that can be called on the object from the outside.


# How Do We Expose Instance Methods to the Outside?

The only way to do this in Ruby is to define instance methods within our classes that make use of instance variables in some way. For example, if we want to be able to change the name of our `cat` or `dog` objects, we need to define a getter method:

```ruby
class Animal
  def initialize(name)
    @name = name
  end

  def name
    @name
  end
end
```

Defining this getter method within the `Animal` class allows us to call the `name` getter method on the `cat` or `dog` objects:

```ruby
cat = Animal.new('Rolo')
dog = Animal.new('Fil')

puts cat.name  # Rolo
puts dog.name  # Fil
```

We can also define setter methods to allow us to modify the value of the `@name` instance variable:

```ruby
class Animal
  def initialize(name)
    @name = name
  end

  def name
    @name
  end

  def name=(new_name)
    @name = new_name
  end
end

cat = Animal.new('Rolo')
dog = Animal.new('Fil')

puts cat.name      # Rolo
puts dog.name      # Fil

cat.name = 'Rolph'
dog.name = 'Lifo'

puts cat.name      # Rolph
puts dog.name      # Lifo
```

We can also use `attr_reader`, `attr_writer`, or `attr_accessor` to take advantage of built-in getter and setter accessor methods:

```ruby
class Animal
  attr_accessor :name

  def initialize(name)
    @name = name
  end
end

cat = Animal.new('Rolo')
dog = Animal.new('Fil')

puts cat.name      # Rolo
puts dog.name      # Fil

cat.name = 'Rolph'
dog.name = 'Lifo'

puts cat.name      # Rolph
puts dog.name      # Lifo
```


# Summary

Instance variables are private because they can only be accessed from within instance methods. There's no Ruby syntax that allows us to access an object's state directly. This is because instance variables are meant to keep track of state while instance methods are meant to expose behaviors.

Therefore, an object's state is private in the sense that it's not accessible to the outside without defining instance methods to create the object's public interface.

In Ruby, it's not possible to get the value of an instance variable without calling an instance method on an object. Whether the instance method is a default getter method defined from \`attr<sub>\*</sub>\` methods, a custom getter/setter method, or one of the methods in the method lookup path, an instance method is required in order to make getting or setting the value of an instance variable part of an object's public interface.

This forces Ruby programmers to make use of encapsulation, hiding or protecting data from being retrieved or modified from the outside. In Ruby, programmers must make conscious decisions about an object's state and which parts of that state should be accessible in an object's public interface.
