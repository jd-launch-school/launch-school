---
layout: post
title: OOP Debugging and Videos
excerpt_separator: <!--more-->
tags: launch_school ruby oop rb120
---

I've been working on OOP debugging exercises as well as continuing to post videos of me solving coding problems and going through these exercises.

<!--more-->


# Videos

Here are the videos I've posted recently. I switched my [PeerTube instance](https://peertube.social/accounts/bodi) in hopes that this new PeerTube instance will be adequately moderated.

-   [Simple Simple Simple String Expansion](https://peertube.social/videos/watch/396fd15c-6dbe-4a9f-b0ba-526d5543ea3f)
-   [Are they the 'same'?](https://peertube.social/videos/watch/e8051036-1e70-4968-b398-175fb7abcd66)
-   [Beeramid](https://peertube.social/videos/watch/f0b88f0c-9370-48e9-acef-fee2a8942f4e)
-   [Encrypt This!](https://peertube.social/videos/watch/56985050-6b2c-4391-a5c2-0109c3a25bf1)
-   [Triangle Sides](https://peertube.social/videos/watch/4d5cb26f-bd2b-4ba5-bef6-8e9868fbe583)
-   [Alphabetized](https://peertube.social/videos/watch/e08b6976-f48b-43d8-bbcf-1d8cf863d6b5)
-   [OOP Debugging: Animal Kingdom](https://peertube.social/videos/watch/822d78ac-07cf-433d-8b29-fbb1ee74d3e5)

You’ll be able to find more as I upload them on my [Learn Programming Channel](https://peertube.social/video-channels/learn_programming/videos).


# Debugging OOP

In doing these exercises, I've realized some of my blind spots in terms of understanding how some of this code works. Here are some of those blind spots for future reference:


## The `super` Keyword

As you can see in the [OOP Debugging: Animal Kingdom](https://peertube.social/videos/watch/822d78ac-07cf-433d-8b29-fbb1ee74d3e5) video, I hesitated a little when it came to determining if using `super` without arguments would raise an exception. Here's some example code to show what I mean:

```ruby
class MyClass
  def initialize(name, number)
    @name = name
    @number = number
  end
end

class MyOtherSubclass < MyClass
  def initialize(name, number)
    super
  end
end

class MySubclass < MyClass
  def initialize(name, number, text)
    super
    @text = text
  end
end
```

When I first looked at code like this, I couldn't determine definitively whether or not the `MyOtherSubclass` use of `super` would work. I recognized that the `MySubclass` use of `super` would not work, but I wasn't sure if it wouldn't work because no arguments were passed to `super` or if because all three of the arguments would be passed to `super`. Also, since we're not doing anything different in `MyOtherSubclass#initialize`, is it necessary?

I now know that `super` works without explicit arguments. `MyOtherSubclass` will work and the `@name` and `@number` instance variables will be initialized. I also know that it's not necessary as simply inheriting from `MyClass` ensures that the `MyClass#initialize` constructor is called when a new `MyOtherSubclass` object is instantiated:

```ruby
class MyClass
  def initialize(name, number)
    @name = name
    @number = number
  end
end

class MyOtherSubclass < MyClass; end

test = MyOtherSubclass.new('my_name', 'my_number')
p test
```

    #<MyOtherSubclass:0x00005567fba3c8f8 @name="my_name", @number="my_number">

I also now know, but wasn't sure when I recorded the video, that since `super` doesn't need explicit arguments and if there are no explicit arguments, it will pass in all of the parameters as arguments, the `MySubClass#initialize` method will raise an `ArgumentError` exception since three arguments are being passed in while the `MyClass#initialize` constructor only takes two arguments.

```ruby
class MyClass
  def initialize(name, number)
    @name = name
    @number = number
  end
end

class MySubclass < MyClass
  def initialize(name, number, text)
    super
    rescue ArgumentError => e
      puts e.message

    @text = text
  end
end

test = MySubclass.new('my_name', 'my_number', 'my_text')
p test
```

    wrong number of arguments (given 3, expected 2)
    #<MySubclass:0x000055f5bfb7ef48 @text="my_text">

To solve this, we must pass in the two arguments to `super`:

```ruby
class MyClass
  def initialize(name, number)
    @name = name
    @number = number
  end
end

class MySubclass < MyClass
  def initialize(name, number, text)
    super(name, number)
    @text = text
  end
end

test = MySubclass.new('my_name', 'my_number', 'my_text')
p test
```

    #<MySubclass:0x00005558542aaad8 @name="my_name", @number="my_number", @text="my_text">


## `protected` vs. `private` Methods

Prior to coming across this in the debugging exercise [Employee Management](https://launchschool.com/exercises/2908a93d), I didn't fully understand the difference between `protected` and `private` instance methods. While working through the problem, this quote from the lesson helped:

> `Private` methods are methods that don't need to be available to the rest of the program.
> 
> `Protected` methods are methods that are accessible from within the class just like `public` methods but not accessible from outside of the class similar to `private` methods.

In practice, this means that `private` methods are not accessible to other class instance methods:

```ruby
class Person
  def initialize(name)
    @name = name
  end

  def compare_names(other)
    name == other.name

    rescue NoMethodError => e
      puts e.message
  end

  private

  attr_reader :name
end

person1 = Person.new("Person1")
person2 = Person.new("Person2")
p person1.compare_names(person2)
```

    private method `name' called for #<Person:0x000055c3eed9af28 @name="Person2">
    nil

`Public` and `protected` methods are accessible to class instance methods:

```ruby
class Person
  def initialize(name)
    @name = name
  end

  def compare_names(other)
    name == other.name

    rescue NoMethodError => e
      puts e.message
  end

  protected

  attr_reader :name
end

person1 = Person.new("Person1")
person2 = Person.new("Person2")
p person1.compare_names(person2)
```

    false

Both `private` and `protected` methods are **not** accessible from a calling object:

```ruby
class Person
  def initialize(name)
    @name = name
  end

  def compare_names(other)
    name == other.name

    rescue NoMethodError => e
      puts e.message
  end

  protected

  attr_reader :name
end

person1 = Person.new("Person1")
person2 = Person.new("Person2")
p person1.compare_names(person2)

begin
  p person1.name
rescue NoMethodError => e
  puts e.message
end
```

    false
    protected method `name' called for #<Person:0x0000561704e2dff8 @name="Person1">

So, using `protected` is handy in cases like this where we want other class instance methods to have access to `protected` methods but we don't want those `protected` methods to be a part of the interface that other parts of the code can use to interact with the class.

That said, the [Ruby documentation for the protected method](https://docs.ruby-lang.org/en/2.7.0/Module.html#method-i-protected) suggests that:

> Usually private should be used.


## Spaceship Operator `<=>` in Relation to the `Comparable` Module

For the [Sorting Distances](https://launchschool.com/exercises/01e60ed0) debugging exercise, even though the changes I initially made were correct, they didn't show a complete understanding of how the spaceship operator `<=>` relates to the `Comparable` module.

For example, consider the following code:

```ruby
class MyClass
  attr_reader :value

  def initialize(value)
    @value = value
  end

  def ==(other)
    value == other.value
  end

  def >(other)
    value > other.value
  end
end

my_object = MyClass.new(10)
my_object2 = MyClass.new(20)
p my_object == my_object2
p my_object > my_object2
p my_object2 == my_object
p my_object2 > my_object
```

    false
    false
    false
    true

The `==` and `>` are not necessary if we instead `include Comparable` and define `<=>`

```ruby
class MyClass
  include Comparable

  attr_reader :value

  def initialize(value)
    @value = value
  end

  def <=>(other)
    value <=> other.value
  end
end

my_object = MyClass.new(10)
my_object2 = MyClass.new(20)
p my_object == my_object2
p my_object > my_object2
p my_object2 == my_object
p my_object2 > my_object
```

    false
    false
    false
    true

So, even though the code in both cases works and neither is better than the other, per se, it's still important to know how `<=>` and `Comparable` work together. The following is from the [Comparable documentation](https://docs.ruby-lang.org/en/2.7.0/Comparable.html) for reference:

> The `Comparable` mixin is used by classes whose objects may be ordered. The class must define the `<=>` operator, which compares the receiver against another object, returning a value less than 0, returning 0, or returning a value greater than 0, depending on whether the receiver is less than, equal to, or greater than the other object. If the other object is not comparable then the `<=>` operator should return `nil`. **`Comparable` uses `<=>` to implement the conventional comparison operators (`<`, `<=`, `==`, `>=`, and `>`) and the method `between?`.**
