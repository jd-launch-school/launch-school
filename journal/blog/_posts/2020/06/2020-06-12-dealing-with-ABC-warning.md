---
layout: post
title: Dealing with ABC Warning
excerpt_separator: <!--more-->
tags: launch_school ruby project rb120 oop twenty-one
---

For future reference, I show how I refactored a larger method into multiple smaller methods, where each smaller method does its part to get the overall job done.

<!--more-->

This is the code that I initially had to display the cards in a player's hand:

```ruby
def display_hand
  border = []
  suit = []
  blank = []
  face = []

  hand.each do |card|
    border << card.border_line
    suit << card.suit_line
    blank << card.blank_line
    face << card.face_line
  end

  puts "\n#{self.class}'s hand: #{score}"

  lines = [border.join, suit.join, blank.join, face.join]
  puts lines[0, 3], lines.last, lines.reverse[1..-1]
end
```

Unfortunately, it raised the following Rubocop errors:

```shell
Assignment Branch Condition size for display_hand is too high. [<6, 23, 0> 23.77/18] (http://c2.com/cgi/wiki?AbcMetric, https://en.wikipedia.org/wiki/ABC_Software_Metric)
```

```shell
Method has too many lines. [13/10] (https://github.com/bbatsov/ruby-style-guide#short-methods) (ruby-rubocop)
```

So, instead of taking the easy way out and disabling those warnings, I decided to break this code into multiple methods that don't throw warnings.

In looking at this code, the first thing I noticed was that the top part of it, before the first `puts` method call, could be moved into it's own method, like this:

```ruby
def build_hand
  border = []
  suit = []
  blank = []
  face = []

  hand.each do |card|
    border << card.border_line
    suit << card.suit_line
    blank << card.blank_line
    face << card.face_line
  end

  [border.join, suit.join, blank.join, face.join]
end
```

```ruby
def display_hand
  puts "\n#{self.class}'s hand: #{score}"

  lines = build_hand
  puts lines[0, 3], lines.last, lines.reverse[1..-1]
end
```

This solves the problem in terms of the ABC size metric, but the `build_hand` method is still flagged for being too long.

From here, I can see that maybe there's a better way to initialize the four variables at the top of the method: `border`, `suit`, `blank`, and `face`. In looking at the last line of the `build_hand` method, I see that we eventually build a new array using those local variables. So, I tried that by making these changes:

```ruby
def build_hand
  array = [[], [], [], []]

  hand.each do |card|
    array[0] << card.border_line
    array[1] << card.suit_line
    array[2] << card.blank_line
    array[3] << card.face_line
  end

  [array[0].join, array[1].join, array[2].join, array[3].join]
end
```

Hmmm, this does solve the problem of the method having too many lines and but it now raises an ABC size warning with the assignment part of the calculation going down to `1` from `6`, but there are still too many branches:

```shell
Assignment Branch Condition size for build_hand is too high. [<1, 22, 0> 22.02/18] (http://c2.com/cgi/wiki?AbcMetric, https://en.wikipedia.org/wiki/ABC_Software_Metric)
```

Looking at that last line again, it does seem to be doing a lot, with all of those joins and it's really doing the same thing with each of the elements of `array`. Maybe I can use `map` instead. `map` will do something to each of the elements (transformation) of the given array and the return a new array:

```ruby
def build_hand
  array = [[], [], [], []]

  hand.each do |card|
    array[0] << card.border_line
    array[1] << card.suit_line
    array[2] << card.blank_line
    array[3] << card.face_line
  end

  array.map(&:join)
end
```

Cool. That successfully solves the ABC issue by bringing the ABC size to `<1, 16, 0>` or `16.03`, which is under the max size limit of `18`. That first line where I'm initializing the `array` local variable to an array of arrays still bothers me. So, I changed it to this:

```ruby
def build_hand
  array = Array.new(4) { [] }

  hand.each do |card|
    array[0] << card.border_line
    array[1] << card.suit_line
    array[2] << card.blank_line
    array[3] << card.face_line
  end

  array.map(&:join)
end
```

`Array.new(4) { [] }` is equivalent to `[[], [], [], []]`. When passing a block to `Array::new` like this, it creates an array with `4` elements, each array element refers to a unique array object:

```ruby
array2 = Array.new(4) { [] }
array2[1] << "array element 2"
p array2[0].object_id
p array2[1].object_id
p array2[2].object_id
p array2[3].object_id
p array2
```

    60
    80
    100
    120
    [[], ["array element 2"], [], []]

Interestingly, without the block and passing an empty array `[]` as the second argument of `Array::new`, `4` array elements are still created within the array, but each element refers the same array object:

```ruby
array = Array.new(4, [])
p array
array[1] << "array element 2"
p array[0].object_id
p array[1].object_id
p array[2].object_id
p array[3].object_id
p array
```

    [[], [], [], []]
    60
    60
    60
    60
    [["array element 2"], ["array element 2"], ["array element 2"], ["array element 2"]]

With this refactoring complete, the code no longer displays those warnings. It still has some Ruby Reek warnings, but I'm not ready to deal with those. However, as long as the `build_hand` method returns an array of elements corresponding to the different parts of a card, the `display_hand` method will be able to display the cards in the hand and other methods will be able to use the cards in the hand as needed. This seems like a good example of how encapsulating code like this into methods and classes makes it easier to refactor code like this without worrying too much about causing issues in other parts of the program.
