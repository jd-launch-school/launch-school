---
layout: post
title: The Lone Card Problem
excerpt_separator: <!--more-->
tags: launch_school ruby project rb120 twenty-one oop
---

You find a single card on the side of a street. What does this mean for object-oriented programming design decisions related to cards and decks?

<!--more-->

I imagine a lonely card that's separated from its deck, lost on a street in some forgotten part of a city. When one who has never seen such cards before finds this card, do they know that it's actually part of a deck of cards with certain faces and suits? Should one be able to pick up that card and also know all of the other potential cards in the deck it comes from? They would be able to do research and determine what type of card it is. They may hold the card up to a passerby and ask if the other person knows. They may consult a book on different types of cards. They may search the Internet.

Interestingly, for those of us who are familiar with these cards, we can know just by looking at the card how many and which types of cards are in its deck.

In the Launch School reference implementation, they put the `SUITS` and `FACES` constants within the `Card` class.

```ruby
class Card
  SUITS = ['H', 'D', 'S', 'C']
  FACES = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']

  # ... other code
end
```

Then, to create the deck, they reference these constants. The reference implementation only uses the `SUITS` and `FACES` constants within the `Deck` class. They're not used anywhere else:

```ruby
class Deck
  # ... other code

  def initialize
    @cards = []
    Card::SUITS.each do |suit|
      Card::FACES.each do |face|
        @cards << Card.new(suit, face)
    end
  end

  # ... other code
end
```

From my perspective, this means that each individual card can know about all of the available suits and faces in the deck. This code tells us that the cards we're adding have `SUITS` and `FACES` from the `Card` class.

So, if we found a lone card on the street, we could look in a book of cards, for example, to learn more about the card we found. That's equivalent to looking through our code to find the `Card` class.

In contrast, I put those constants in the `Deck` class. Then, I create the deck using those constants but I do it in the `Deck#reset` method:

```ruby
class Deck
  # ... other code

  FACES = %w(2 3 4 5 6 7 8 9 10 j q k a)
  SUITS = %w(◆ ♣ ♥ ♠)

  # ... other code

  def reset
    @cards = SUITS.product(FACES).map do |card|
      Card.new(card.first, card.last)
    end
  end

  # ... other code
```

With this implementation, I'm communicating that the deck knows about all of the available suits and faces and will use that knowledge to construct a deck of cards as needed. The card, on the other hand, only knows about the suit and the face it's given.

The deck can determine which types of cards it contains. In this case, it's creating cards that take both a suit and a face. However, it could also create cards that take two suits or two faces or some other type of card altogether. Those cards would need to have blueprints in the form of a `class`, but the point is that the deck can determine which types of cards it contains.

Plus, the `FACES` and `SUITS` constants are only used in the `Deck` class, so there's no reason to call out to another class like the `Card` class to access and use them.

This is all well and good and makes some sense, but what if we wanted to include multiple types of cards in one deck?

In the reference implementation, one would need to create a new `OtherCard` class. They can encapsulate all of the knowledge about this type of card within this class. This type of card may not have suits and faces and may instead, have pictures or directions or whatever. Then, back in the `Deck` class, they can reference the constants of both the `Card` and `OtherCard` classes to add these cards to the deck as needed. It might look like this:

```ruby
class Deck
  attr_accessor :cards

  def initialize
    @cards = []
    Card::SUITS.each do |suit|
      Card::FACES.each do |face|
        @cards << Card.new(suit, face)
    end

    OtherCard::PICTURES.each do |picture|
      OtherCard::DIRECTIONS.each do |direction|
        OtherCard::SOMETHING_ELSE.each do |something_else|
          @cards << Card.new(picture, direction, something_else)
    end
  end

  # ... other code
end
```

In my implementation, on the other hand, I would need to add the new constants for the new cards to the `Deck` class. I would also need to create a new `OtherCard` class:

```ruby
class Deck
  # ... other code

  FACES = %w(2 3 4 5 6 7 8 9 10 j q k a)
  SUITS = %w(◆ ♣ ♥ ♠)
  DIRECTIONS = %w(n e s w u d)
  PICTURES = %w(cow dog cat mouse)
  SOMETHING_ELSE = %w(...)

  # ... other code

  def reset
    @cards = SUITS.product(FACES).map do |card|
      Card.new(card.first, card.last)
    end

    @cards = PICTURES.product(DIRECTIONS, SOMETHING_ELSE).map do |card|
      OtherCard.new(card.first, card[1], card.last)
    end    
  end

  # ... other code
```

The reference implementation can grow relatively simply by adding new `each` iterations for as many types of cards as needed.

In my implementation, adding more types of cards to the deck means adding more constants. When there are constants for more than one type of card present, it's not clear from looking at the constants in the `Deck` class which types of cards need which constants. Also, as the list of constants grows, the code becomes longer and more difficult to read.

I initially thought that since the `Deck` class was the only class to use the constants, the constants should be placed there. However, in thinking about this further and using the reference implementation as a guide, I now see that encapsulating these constants into the `Card` class will make the code more readable overall.
