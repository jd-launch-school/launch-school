---
layout: post
title: Passed RB101-RB109 Interview Assessment
excerpt_separator: <!--more-->
tags: launch_school ruby assessments rb101
---

This morning, after more than three months of practicing, live-coding with other Launch School students, live-coding on Twitch, recording myself, working on recognizing patterns and calming myself when I got stuck in the middle of solving a problem, I found some courage and took the Launch School RB109 interview assessment. I passed. Here's some information about how I prepared and some things I'd like to remember for future Launch School assessments.

<!--more-->

> I'm shaking. I'm nervous and excited to finally get this chance. I'm worried that I'll be the only one of the people I've practiced with to get a "not yet". I mean, I guess it fits since I've struggled with this part of coding for a while. I guess, if I do get a "not yet", I'll struggle some more until I'm ready. If I'm never ready, then at least I'll know. I don't think that'll happen though. I think I am ready. I know enough about Ruby to pass this. I've worked on my communication skills. I know that I need to be very explicit about what I'm thinking and try not to jump logic without explaining myself.

That's something I wrote minutes before the assessment started.

I happened to pick up the book ["A Mind for Numbers" by Barbara Oakley Ph.D.](https://barbaraoakley.com/books/a-mind-for-numbers/) a couple of hours before the assessment and stumbled upon this part on page 250, leading me to write what I wrote above:

> High-stakes learning and performance situations can put you under a lot of stress. However, there is a growing body of research showing that fairly simple psychological interventions can lower your anxiety about tests and boost what you learn in the classroom. These interventions don't teach academic content; they target your attitudes.
> 
> My research team has found that if you write about your thoughts and feelings about an upcoming test immediately before you take the test, it can lessen the negative impact of pressure on performance. We think that writing helps to release negative thoughts from the mind, making them less likely to pup up and distract you in the heat of the moment.

Desperate to do something to relax, I tried this. I think it helped. Once my thoughts were written out, I let them go and focused on doing what needed to be done to solve the problems and pass the assessment. I don't remember feeling that what I wrote was interesting until I read it after the test. Here's the rest of what I wrote:

> Look, I'll do my best. That's all I can do. I think I'm worried about the problems I'll get. If I get problems that I recognize, in terms of seeing the logic straight away, then I'll be OK. If, on the other hand, I get more complicated problems that I don't immediately see the logic for, I may or may not be OK. Of course, failure is good in this case.
> 
> One reason it might be good for me to fail is to show my children what happens when one fails something like this so they can see that I'm not perfect and I won't quit. It also won't be the end of my life or career or something like that. I fail all the time. I've failed many times in this before and if I fail this time, it'll be no different from those other times. Fail and move on, keep practicing, keep learning.
> 
> So, whether I fail or succeed, there's good to come about from that.
> 
> The question is &#x2026; do I feel ready to move on? I do.

My interviewer agreed. :)

After what seemed like an eternity of focusing on various Ruby methods and communication techniques, I can now move on.

Before I move on, I want to record some of the things I did to prepare.


# Twitch Live Streaming

I wanted to give myself some pressure and practice solving problems out loud, so I streamed some problem-solving on Twitch with a 20-minute time limit and recorded those streams. Overall, this was fun and the more problems I solved while talking out loud, the better/faster I got. I wasn't able to solve all of the problems I attempted and some I solved in inefficient ways, but overall I noticed myself improving.

I tried streaming in the morning, afternoon, and evening because I wanted to practice as much as possible. I started to notice that my best streams were in the morning when my mind was fresh. I did OK in the afternoons, sometimes succeeding and sometimes rage quitting. In the evenings, however, I almost always quit from the frustration of not being able to solve a problem. So, when it came time to schedule my assessment, I knew that I'd do my best in the mornings.

Live streaming on Twitch also has the benefit of not having to coordinate with other students. Instead of having to plan ahead, I was able to jump onto the computer, set up for live streaming, and start coding at any time.


# Solving LOTS of Problems

I solved a lot of problems throughout this time period. I re-solved every RB101-109 Small Problem (Easy and Medium) that Launch School offers. I solved some of them multiple times. I solved many Codewars problems. I solved every problem the Launch School TAs gave me during the study sessions. I solved every problem I saw in the videos listed in the study guide.

A couple of days before the assessment, I attempted to solve random Codewars problems to test my skills on exercises I've never seen before. I did relatively well, being able to solve a few using a PEDAC-like process. At one point, I came across a problem that seemed more difficult. While attempting to understand the problem, I realized it was a 4 kyu level, a level that I wouldn't have attempted consciously. Yet, there I was, breaking down the problem like any other problem. I didn't solve it because once I realized its difficulty, I knew it was beyond what I needed for the assessment, so I stopped trying.

I decided to schedule my assessment once I was able to complete the following twenty problems, each within 20 minutes and with as comprehensive a PEDAC process as possible:

<details>
<summary>Find Even Index</summary>
<pre><code># You are going to be given an array of integers. Your job is to
# take that array and find an index `N` where the sum of the
# integers to the left of `N` is equal to the sum of the integers
# to the right of `N`. If there is no index that would make this
# happen, return -1.

# For example:

# Let's say you are given the array [1,2,3,4,3,2,1]:
# Your method will return the index 3, because at the 3rd position
# of the array, the sum of left side of the index [1,2,3] and the
# sum of the right side of the index [3,2,1] both equal 6.

# find_even_index([1,2,3,4,3,2,1]) == 3
# find_even_index([1,100,50,-51,1,1]) == 1
# find_even_index([1,2,3,4,5,6]) == -1
# find_even_index([20,10,30,10,10,15,35]) == 3
# find_even_index([20,10,-80,10,10,15,35]) == 0
# find_even_index([10,-80,10,10,15,35,20]) == 6
# find_even_index([-1,-2,-3,-4,-3,-2,-1]) == 3
</code></pre>
</details>

<details>
<summary>Repeated Strings</summary>
<pre><code># Given the string of chars a..z A..Z do as in the sample cases

# accum("abcd")    # "A-Bb-Ccc-Dddd"
# accum("RqaEzty") # "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
# accum("cwAt")    # "C-Ww-Aaa-Tttt"
</code></pre>
</details>

<details>
<summary>Find Largest Palindrome</summary>
<p>(<a href="https://launchschool.com/blog/watch-others-code-recording-part-2">Watch Others Code Part 2</a>)</p>
<pre><code># Write a method that finds the largest substring that
# is a palindrome within a string.
#
# largest_palindrome("ppop")    #== "pop"
# largest_palindrome("stat")    #== "tat"
# largest_palindrome("hannah")  #== "hannah"
</code></pre>
</details>

<details>
<summary>Consecutive Strings</summary>
<p>(<a href="https://www.codewars.com/kata/consecutive-strings/ruby">Codewars</a>)</p>
<pre><code># You are given an array `strarr` of strings and an integer k.
# Your task is to return the first longest string consisting of
# k consecutive strings taken in the array.
#
# n being the length of the string array, if n = 0 or k > n or k <= 0 return "".
#
# Examples:
# longest_consec(["zone", "abigail", "theta", "form", "libe", "zas"], 2) == "abigailtheta"
# longest_consec(["ejjjjmmtthh", "zxxuueeg", "aanlljrrrxx", "dqqqaaabbb", "oocccffuucccjjjkkkjyyyeehh"], 1) == "oocccffuucccjjjkkkjyyyeehh"
# longest_consec([], 3) == ""
# longest_consec(["itvayloxrp","wkppqsztdkmvcuwvereiupccauycnjutlv","vweqilsfytihvrzlaodfixoyxvyuyvgpck"], 2) == "wkppqsztdkmvcuwvereiupccauycnjutlvvweqilsfytihvrzlaodfixoyxvyuyvgpck"
# longest_consec(["wlwsasphmxx","owiaxujylentrklctozmymu","wpgozvxxiu"], 2) == "wlwsasphmxxowiaxujylentrklctozmymu"
# longest_consec(["zone", "abigail", "theta", "form", "libe", "zas"], -2) == ""
# longest_consec(["it","wkppv","ixoyx", "3452", "zzzzzzzzzzzz"], 3) == "ixoyx3452zzzzzzzzzzzz"
# longest_consec(["it","wkppv","ixoyx", "3452", "zzzzzzzzzzzz"], 15) == ""
# longest_consec(["it","wkppv","ixoyx", "3452", "zzzzzzzzzzzz"], 0) == ""
</code></pre>
</details>

<details>
<summary>Valid Parenthesis</summary>
<p>(<a href="https://launchschool.com/exercises/f3d7f26e">Medium 2</a>)</p>
<pre><code># Write a function called `valid_parentheses` that takes a
# string of parentheses, and determines if the order of
# the parentheses is valid. The function should return true
# if the string is valid, and false if it's invalid.
#
# Tests
#
# p valid_parentheses( "()" ) == true
# p valid_parentheses( "())" ) == false
# p valid_parentheses( ")(()))" ) #== false
# p valid_parentheses( "(" ) == false
# p valid_parentheses( "(())((()())())" ) == true
# p valid_parentheses( "(()))(" ) == false
# p valid_parentheses( "(((((()))))))((())" ) == false
# p valid_parentheses( "((())())" ) == true
</code></pre>
</details>

<details>
<summary>Scramble</summary>
<p>(<a href="https://www.codewars.com/kata/55c04b4cc56a697bb0000048">Codewars</a>)</p>
<pre><code># Write method scramble(str1,str2) that returns true if a portion of str1
# characters can be rearranged to match str2, otherwise returns false.
#
# For example:
# str1 is 'rkqodlw' and str2 is 'world' the output should return true.
# str1 is 'cedewaraaossoqqyt' and str2 is 'codewars' should return true.
# str1 is 'katas' and str2 is 'steak' should return false.
#
# Only lower case letters will be used (a-z). No punctuation or digits will be included.
#
# Examples:
# p scramble('javaass', 'jjss') == false
# p scramble('rkqodlw','world') == true
# p scramble('cedewaraaossoqqyt','codewars') == true
# p scramble('katas','steak') == false
# p scramble('scriptjava','javascript') == true
# p scramble('scriptingjava','javascript') == true
</code></pre>
</details>

<details>
<summary>Highest Scoring Word</summary>
<p>(<a href="https://www.codewars.com/kata/57eb8fcdf670e99d9b000272">Codewars</a>)</p>
<pre><code># Given a string of words, you need to find the highest
# scoring word.
# 
# Each letter of a word scores points according to it's
# position in the alphabet:
# 
# a = 1, b = 2, c = 3 etc.
# 
# You need to return the highest scoring word as a string.
#
# If two words score the same, return the word that appears
# earliest in the original string.
#
# All letters will be lowercase and all inputs will be valid.
#
# Examples:
# alphabet_score('man i need a taxi up to ubud') == 'taxi'
# alphabet_score('what time are we climbing up the volcano') == 'volcano'
# alphabet_score('take me to semynak') == 'semynak'
# alphabet_score('aa b') == 'aa'
</code></pre>
</details>

<details>
<summary>Prime Numbers Between Two Numbers</summary>
<p>(<a href="https://www.codewars.com/kata/5927db23fb1f934238000015">Codewars</a>)</p>
<pre><code># Print all the primes between 2 numbers without using the prime class.
#
# You will always be given two positive integers. One or both of the integers can be zero.
#
# Exmaples:
# p primes_between(1, 5) == [2, 3, 5]
# p primes_between(3, 10) == [3, 5, 7]
# p primes_between(3, 7)  == [3, 5, 7]
# p primes_between(8, 9)  == []
# p primes_between(0, 1) == []
</code></pre>
</details>

<details>
<summary>Sum Consecutive Numbers</summary>
<p>(<a href="https://www.codewars.com/kata/55eeddff3f64c954c2000059">Codewars</a>)</p>
<pre><code># You are given an array which contains only integers (positive
# and negative). Your job is to sum only the numbers that are
# the same and consecutive. The result should be one array.
# 
# You can assume there is never an empty array and there will
# always be an integer.
#  
# Examples:
# ---------
# sum_consecutives([1,4,4,4,0,4,3,3,1]) == [1,12,0,4,6,1]
# sum_consecutives([1,1,7,7,3]) == [2,14,3]
# sum_consecutives([-5,-5,7,7,12,0]) ==  [-10,14,12,0]
# sum_consecutives([1]) == [1]
# sum_consecutives([-1]) == [-1]
</code></pre>
</details>

<details>
<summary>Reverse an Array</summary>
<pre><code># Reverse an array without using the built-in `reverse` method.
</code></pre>
</details>

<details>
<summary>Fibonacci Number Index</summary>
<pre><code># Given an array, select all elements whose index is a fibonacci number.
</code></pre>
</details>

<details>
<summary>Palindrome Without Reverse</summary>
<pre><code># Given a word as a string, write a method to determine if a word is
# a palindrome, without using the `reverse` method.
</code></pre>
</details>

<details>
<summary>Number of Primes</summary>
<p>(<a href="https://launchschool.com/blog/watch-others-code-recording-part-1">Watch Others Code Part 1</a>)</p>
<pre><code># Write a method that will take an array of numbers,
# and return the number of primes in the array.
</code></pre>
</details>

<details>
<summary>Integer Sum or Product</summary>
<p>(<a href="https://launchschool.com/blog/watch-others-code-recording-part-1">Watch Others Code Part 1</a>)</p>
<pre><code># Write a program that asks the user to enter an integer greater
# than 0, then asks if the user wants to determine the sum or
# product of all numbers between 1 and the entered integer.
#
# Examples:
#
# >> Please enter an integer greater than 0:
# 5
# >> Enter 's' to compute the sum, 'p' to compute the product:
# s
# >> The sum of the integers between 1 and 5 is 15.
#
# >> Please enter an integer greater than 0:
# 6
# >> Enter 's' to compute the sum, 'p' to compute the product:
# p
# >> The product of the integers between 1 and 6 is 120.
</code></pre>
</details>

<details>
<summary>Combine Two Arrays</summary>
<p>(<a href="https://launchschool.com/blog/watch-others-code-recording-part-1">Watch Others Code Part 1</a>)</p>
<pre><code># Write a method that combines two Arrays passed in as arguments,
# and returns a new Array that contains all elements from both Array
# arguments, with the elements taken in alternation.
#
# You may assume that both input Arrays are non-empty, and that they have
# the same number of elements.
#
# Example:
#
# interleave([1, 2, 3], ['a', 'b', 'c']) == [1, 'a', 2, 'b', 3, 'c']
</code></pre>
</details>

<details>
<summary>Titleize (Capitalize)</summary>
<p>(<a href="https://launchschool.com/blog/watch-others-code-recording-part-4">Watch Others Code Part 4</a>)</p>
<pre><code># Write a method that takes a single String argument and returns
# a new string that contains the original value of the argument
# with the first character of every word capitalized and all other
# letters lowercase.
#
# You may assume that words are any sequence of non-blank characters.
#
# Examples:
#
# word_cap('four score and seven') == 'Four Score And Seven'
# word_cap('the javaScript language') == 'The Javascript Language'
# word_cap('this is a "quoted" word') == 'This Is A "quoted" Word'
</code></pre>
</details>

<details>
<summary>Friday the 13ths (Unlucky Days)</summary>
<p>(<a href="https://launchschool.com/exercises/a7fce257">Medium 2</a>)</p>
<pre><code># Write a method that returns the number of Friday the 13ths in the year
# passed in as an argument. You may assume that the year is greater than
# 1752 (when the modern Gregorian Calendar was adopted by the United
# Kingdom), and you may assume that the same calender will remain in use
# for the foreseeable future.
#
# Examples:
# friday_13th(2015) # => 3
# friday_13th(1986) # => 1
#
# Hint: Ruby's `Date` class could be helpful here.
#  - look at how to initialize a Date object
#  - look at the `friday?` method 
</code></pre>
</details>

<details>
<summary>Computer Returns English</summary>
<p>(<a href="https://launchschool.com/blog/watch-others-code-recording-part-4">Watch Others Code Part 4</a>)</p>
<pre><code># Write a method that will generate random English math
# problems. The method should take a length, then return
# a math phrase with that many operations.
#
# Examples:
# mathphrase(1)  # => "five minus two"
# mathphrase(2)  # => "two plus three times eight"
# mathphrase(3)  # => "one divided by three plus five times zero"
#
# Given:

NUMBERS = %w(zero one two three four five six seven eight nine ten)
OPERATORS = %w(plus minus times divided)
#+end_src
</code></pre>
</details>

<details>
<summary>Letter Swap</summary>
<p>(<a href="https://launchschool.com/exercises/56e92849">Easy 5</a>)</p>
<pre><code># Given a string of words separated by spaces, write a method
# that takes this string of words and returns a string in which
# the first and last letters of every word are swapped.

# You may assume that every word contains at least one letter,
# and that the string will always contain at least one word.
# You may also assume that each string contains nothing but words
# and spaces.

# Examples:

# swap('Oh what a wonderful day it is') == 'hO thaw a londerfuw yad ti si'
# swap('Abcde') == 'ebcdA'
# swap('a') == 'a'
</code></pre>
</details>

<details>
<summary>Word to Digit</summary>
<p>(<a href="https://launchschool.com/exercises/753d4461">Medium 1</a>)</p>
<pre><code># Write a method that takes a sentence string as input,
# and returns the same string with any sequence of the
# words 'zero', 'one', 'two', 'three', 'four', 'five',
# 'six', 'seven', 'eight', 'nine' converted to a string
# of digits.
#
# Example:
# 
# word_to_digit('Please call me at five five five one two three four. Thanks.') == 'Please call me at 5 5 5 1 2 3 4. Thanks.'
</code></pre>
</details>


# Outline Lots of Problem Solutions Without Solving Them

On page 241 of ["A Mind for Numbers" by Barbara Oakley Ph.D.](https://barbaraoakley.com/books/a-mind-for-numbers/) in the test prep checklist from Professor Richard Felder, it asks:

> Did you attempt to outline lots of problem solutions quickly, without spending time on the algebra and calculations?

Even though I solved lots of problems, I hadn't done this for too many problems. This is something that I think I'd like to practice more next time. I'd like to practice writing an abbreviated PEDAC process for many problems so that I get used to quickly analyzing a problem.


# Watching Others Code

Unfortunately, I found the resources for this to be lacking. Most of the Launch School videos show students solving problems with minimal to no PEDAC process. That's helpful on some level, but I really wanted to see students solving problems using some sort of PEDAC process. Since I couldn't find many videos showing students solving problems from start to finish while also using a PEDAC process, I created my own.

-   [Unlucky Days (Friday the 13ths)](https://devtube.dev-wiki.de/videos/watch/054f7987-bf6d-4f91-82f6-3b1b1e2f3384)
-   [Matching Parenthesis?](https://devtube.dev-wiki.de/videos/watch/6e6353bd-16f5-4c8d-ba12-dcc2474aa554)
-   [Repeated Strings](https://devtube.dev-wiki.de/videos/watch/67611db7-2075-4bec-859e-50d437af0f92)
-   [Sum Consecutive Numbers](https://devtube.dev-wiki.de/videos/watch/054f7987-bf6d-4f91-82f6-3b1b1e2f3384)

I may add more to that PeerTube account as I record more or as I watch older recordings.


# Study Session With Other Students

I studied with about 10 different Launch School students. All of them were helpful in various ways. Hopefully, I'll continue to work with these and other students going forward.

Some weeks, I had study sessions each day, sometimes multiple per day. Other weeks, I had only one or two sessions. I found these sessions to be helpful, not just for interview practice, but also for learning more about the people in Launch School.

One thing I may change going forward is to expect the sessions to last longer than one hour. I typically tried to fit them into one-hour time slots, but they often ran longer. This is a good thing. So, in the future, I'll expect at least an hour and a half to two hour long sessions.

I'm grateful for everyone's time and energy!
