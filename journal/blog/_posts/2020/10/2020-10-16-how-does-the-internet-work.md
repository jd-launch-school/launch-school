---
layout: post
title: How Does the Internet Work?
excerpt_separator: <!--more-->
tags: launch_school networking ls170
---

This is an overview of the infrastructure and protocols that make up the Internet.

<!--more-->

The Internet is a network of networks made up of physical networking infrastructure and protocols. Protocols are systems of rules that standardize network communications.

Different types of protocols are concerned with different aspects of network communication. They act like different layers. These protocols use a method of encapsulating data payloads to transmit data without the need for parsing and executing the data that's meant for other protocols. This is implemented by way of Protocol Data Units (PDUs).

We can visualize these protocols in their various layers using two of the most popular models, the Open Systems Interconnection (OSI) Model and the Internet Protocol Suite (a.k.a TCP/IP) model.

```
   OSI Model               TCP/IP Model
   ---------               ------------
 ______________            _____________
| Application  |          |             |
|______________|          |             |
 ______________           | Application |
| Presentation |          |             |
|______________|          |             |
 ______________           |_____________|
|    Session   |           _____________
|______________|          |             |
 ______________           |  Transport  |
|  Transport   |          |             |
|______________|          |_____________|
 ______________            _____________
|   Network    |          |  Internet   |
|______________|          |_____________|
 ______________            _____________
|  Data Link   |          |             |
|______________|          |    Link     |
 ______________           |             |
|   Physical   |          |_____________|
|______________|
```

These models give us a high-level overview of how the Internet works. Let's dig deeper into each of the layers of the TCP/IP Model by following a browser request from start to finish.


# The Application Layer

The application layer is the layer that most software engineers focus on. It's made up of a set of protocols that provide various communication services to applications. Some of the protocols involved at this layer include FTP, SSH, Telnet, POP, IMAP, and HTTP. There a many more protocols used at this layer. They can be seen in the [Application Layer Protocols page](https://en.wikipedia.org/wiki/Category:Application_layer_protocols) on Wikipedia.

When we focus on "the web," we are really referring to what is known as the World Wide Web which is closely tied to the Hypertext Transfer Protocol (HTTP). HTTP is a request-response protocol. This means a client, often a browser, will send a request to a server. Then, the server replies with a response to that request.


## The HTTP Request

In many cases, it all starts with an idea or a question: *"What time does a certain store close?"* or *"Which X should I buy?"*. One of the first websites someone accesses when they have a question like this is a search engine website. In modern browsers, one can type the search terms directly into the URL bar of the browser and the browser will send the term to the default search engine set for that browser.

Behind the scenes, the browser is actually sending an HTTP Request to a URL that looks something like `http://duckduckgo.com/?q=test`. The browser creates the URL by combining the essential parts needed for a URL: the scheme (`http`), the host (`duckduckgo.com`), and the path which, in this case, includes a query parameter (`/?q=test`). It's possible to manually enter the URL above into the browser's URL bar, but with modern browsers, we don't need to.

The corresponding HTTP Request looks something like the following:

```
GET /?q=test HTTP/1.1
Host: duckduckgo.com

```


### URIs and URLs

A Uniform Resource Identifier (URI) is an identifier for a specific resource on a network. A Uniform Resource Locator (URL) is a subset of a URI. Basically, URIs identify a specific resource while URLs point to the location of the resource. URLs contain a scheme, host, port, path, and query string.

The query string in our example above `?q=test` is an example of passing additional data to the server during an HTTP Request. The question mark (`?`) denotes the start of a query string and the ampersand (`&`) separates multiple query strings.

Not all characters are allowed in URLs. URL encoding is used to add characters that do not exist within the ASCII character set, characters that are unsafe, or characters that are reserved, like `?`, `=`, and `&`.


### Domain Name System (DNS) Lookup

So, at this point, the browser knows that it wants to send a request to a specific URL, but it doesn't yet know where that URL exists in terms of the broader Internet. This is when the browser sends the host (`duckduckgo.com`) to a Domain Name System (DNS) resolver. Sometimes this can be a program running on one's operating system, other times it's a direct connection between the browser and the DNS resolver running on a remote server.

The important thing to keep in mind at this point is that the browser needs to determine the Internet Protocol Address (IP Address) of the server that hosts the `duckduckgo.com` web server. So, it asks the DNS resolver to return the IP Address for `duckduckgo.com`. Within seconds, the DNS resolver returns an IP address that's something like `40.89.244.237`.


# The Transport Layer

The browser has everything it needs to send its HTTP Request to the next layer. That next layer is known as the Transport layer. In most cases, the browser's HTTP Request relies on another protocol known as the Transmission Control Protocol (TCP). TCP is a connection-oriented protocol. This means that a connection must be established before data can be transmitted.

Another common protocol at this layer is the User Datagram Protocol (UDP). UDP is much simpler than TCP, but that also means some of TCP's helpful features are not available by default. UDP is a connectionless protocol, so a establishing a connection is not necessary before data can be sent and received.

Both TCP and UDP enable multiplexing. Multiplexing is the transmission of multiple signals over a single channel. In this case, the network interface card is the single channel and there may be multiple applications on one device that are sending and listening for data from servers on the Internet all at the same time.

This ability to send multiple streams of data through the network interface card (multiplexing) and receive data from that single channel and have that data directed to the correct application (demultiplexing) is possible thanks to the TCP and UDP protocols.

Multiplexing is implemented by using ports and sockets. Ports are identifiers for specific processes running on a device. A port, combined with an IP Address, creates a communications end-point, also known as a socket.


## The Socket

With the IP Address in hand, the browser creates an Internet Socket. The important point to remember is that port numbers enable application-to-application network connections.

For our example, the browser opens a TCP connection from the local socket using the local area network (LAN) IP Address `192.168.1.246` and port `55254` to the application running on the server at `40.89.244.237` that is listening on port `80`.

The `55254` port number is a random port number chosen by the browser to represent this specific HTTP Request process. The browser will choose different port numbers for each website we visit.

The `80` port number is a default port number for an HTTP server. When the HTTP Request gets to the device that hosts the HTTP server, the network interface card of that device will route the HTTP Request to the HTTP server listening on port `80`. If there is no HTTP server listening on port `80`, the HTTP Request will be dropped.

The following is sample output using the `netcat` program from the browser side to help visualize the open connection between the local socket and the HTTP server:

```bash
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 192.168.1.50:58600     23.54.215.151:80        ESTABLISHED 14340/thunderbird
tcp        0      0 192.168.1.50:55254     40.89.244.237:80        ESTABLISHED 13197/firefox
```

Here's what the `netcat` output might look like from the HTTP server side:

```bash
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 40.89.244.237:80        64.56.211.10:55254      ESTABLISHED 8015/apache
udp        0      0 192.168.1.51:68         192.168.1.51:67         ESTABLISHED 1145/NetworkManager
```

The `64.56.211.10` IP Address is the public IP Address for the LAN router. All devices connected to the LAN router share that public IP Address. The router assigns its own pool of IP Addresses to each device on the network, in our example, these are IP Addresses that start with `192.168.1`.

This clearly demonstrates multiplexing. Generally, there are many open connections listed in the `netcat` output that need to travel back and forth over one network communications channel.

Any communication that contains a source of `192.168.1.50:55254` and a destination of `40.89.244.237:80` will be routed to the `duckduckgo.com` HTTP server. Any communication that has a source of `40.89.244.237:80` and a destination of `64.56.211.10:55254` will be routed by the LAN router to the device at `192.168.1.50:55254` and back to the browser.


## Three-Way Handshake

TCP initiates the request using a three-way handshake.

1.  The browser sends a `SYN` (synchronize) TCP Segment to the server.

2.  Once the server receives this `SYN` segment, it responds with a `SYN ACK` (synchronize acknowledge) segment.

3.  Once the browser receives this `SYN ACK` segment, it sends an `ACK` (acknowledge) segment to the server. At this point, the connection is established and the browser can send more segments as needed.

This three-way handshake adds latency to the connection. Software engineers need to take this into account when working with TCP connections.


## TCP Segment

With the connection established, the browser streams the HTTP Request through the open TCP connection.

TCP breaks up the HTTP Request into as many TCP Segments as are needed. A TCP Segment has a header with the HTTP Request as the encapsulated data payload. Both the source and destination ports are added to the TCP Header, along with a sequence number, window size, checksum and flags indicating the state of the TCP connection.

This segmenting process, along with the data in the header, make TCP a reliable transmission protocol that has the following features required for reliable network communications:

-   **In order delivery**: data is received in the order it was sent
-   **Error detection**: corrupt data is identified using a checksum
-   **Handling data loss**: missing data is retransmitted based on acknowledgements and timeouts
-   **Handling duplication**: duplicate data is eliminated through the use of sequence numbers

TCP, in particular, also provides flow control and congestion avoidance, both necessary so the receiving device is not overwhelmed with requests and data is not slowed down by network congestion.


# The Internet / Network Layer

Stepping back a bit, we need to describe how data in text form gets transmitted from device to device across a physical network. Since the TCP Segment only contains the source and destination ports, TCP Segments are encapsulated within Internet Protocol (IP) Packets that contain the source and destination IP Addresses.

The Internet Protocol (IP) is the main protocol used at the Network layer. This protocol facilitates and standardizes communication between computers on different networks. There are two versions of this protocol, IPv4 and IPv6.

This helps our data get from one device to another on different networks. We now have an IP Packet that contains headers including the source and destination IP Addresses and has, encapsulated within its data payload, a TCP Segment with a header that contains the source and destination ports.

The IP Packet travels from the device the HTTP Request is created on, through the network interface card, to the LAN router. The LAN router then transmits this data to a router outside of the local area network. That router transmits the data to another router and so on until the data reaches the device with an IP Address that matches the destination IP Address in the IP Packet header.


# The Link Layer

The main role of this layer is to identify which device on a local network to send specific data to. When two devices are directly connected, all transmitted data goes from one device to another. However, when more devices are on the network, it's inefficient to send all data to all potential receiving devices. The protocols at this layer help to direct and move this data to specific devices across the same network.

The most-used protocol at this layer is the Ethernet protocol, IEEE 802.3. It's made up of a header, data payload (encapsulated IP Packet), and a footer.

It uses Media Access Control (MAC) addresses to route data from one device to another. A network interface card has only one MAC address that is assigned to it during production. This is sometimes referred to as the physical address of the device or network interface card.

It's not practical to store a list of every known MAC Address within each router. Therefore, any one router only knows about the devices connected to that router by storing this information in a routing table. The routing table matches each MAC Address with its associated LAN IP Address.

Since the source and destination MAC Addresses are stored within the header of the Ethernet frame, a router can look at the destination MAC Address and either pass the Ethernet Frame on to the next router, if that MAC Address is not in the table, or pass the IP Packet on to the device with the matching MAC Address.

Here's an image that might help to visualize IP Packets, TCP Segments, and HTTP Requests:

[![IP_packet_encapsulation](/assets/img/IP_packet_encapsulation.png)](https://www.oreilly.com/library/view/http-the-definitive/1565925092/ch04s01.html)

(Source: [HTTP: The Definitive Guide](https://www.oreilly.com/library/view/http-the-definitive/1565925092/ch04s01.html))


# The Physical Layer

Once we have an Ethernet Frame, each bit of the frame (a one or a zero) is transmitted over the infrastructure that makes up the Internet, from router to router. Signals can be transmitted through copper wires, radio waves, and/or light waves in order to move the bits to their destination devices.

Each router decapsulates the data payload in the Ethernet Frame, which is the IP Packet, to inspect the destination IP Address and then re-encapsulates the data payload and sends the Ethernet Frame to the next router that will get the communication closer to its destination.

Bandwidth and Latency are some of the issues encountered at this level. Bandwidth is the amount of data that can be sent over a specific time, like 100 megabits per second (i.e. 100 million bits per second). Latency is the time it takes for a signal to get from the sender to the receiver. Both of these can cause delays and/or dropped or lost packets.


# The HTTP Response

Back to the TCP connection, once the bits have been sent across the network to the HTTP Server, the HTTP server responds to the original HTTP Request with an HTTP Response that looks like this:

```
HTTP/1.1 200 OK
Content-Length: 700
Content-Type:text/html; charset=utf-8
server: server_name

<<body of response, usually HTML>>
<!DOCTYPE html><html lang="en-US"><head><script>
var __SUPPORTS_TIMING_API = typeof performance === 'object' && !!performance.mark && !! performance.measure && !!performance.getEntriesByType;
function __perfMark(name) { __SUPPORTS_TIMING_API && performance.mark(name); };
var __firstPostLoaded = false;
function __markFirstPostVisible() {
  if (__firstPostLoaded) { return; }
...
</html>
```

This is then encapsulated within a TCP Segment, encapsulated again within an IP Packet, then finally encapsulated another time within an Ethernet Frame. The Ethernet Frame is then sent back across the network to the browser.

When the browser receives the HTTP Response, it parses the response and displays the `body` of the response to the user, which will hopefully contain links that may help the user answer their initial question.


# HTTP Request and Response Cycle

The above describes the process from a URL in the browser to a response from the server displayed as a web page in the browser.

The HTTP Request and Response cycle is now finished. A new cycle is started when a user clicks on a hyperlink or enters a new URL into the browser URL bar.

This process is sufficient for many types of data. However, as we interact with various servers and send more personalized and private data, we eventually come across the need for more efficiency and security.

The following covers some of the techniques used to make HTTP requests and responses more efficient and secure. These techniques help software engineers achieve the stateful and secure experience we expect while using the Internet.


# Security and State

So far, all of the communications are happening in plain text. The data can be inspected by anyone who has access to it at any point throughout the Ethernet Frame's journey because HTTP is inherently insecure.

Also, HTTP by itself is stateless, i.e. it does not keep track of the state of an application. For instance, HTTP does not know if someone is signed into an account.

In order to make our communications between applications more secure and maintain state, we can implement Secure HTTP (HTTPS) via Transport Layer Security (TLS), browser cookies, sessions, and AJAX.


## Secure HTTPS

Transport Layer Security (TLS) can be used to encrypt messages over a TCP connection. It uses a combination of Symmetric Key Encryption and Asymmetric Key Encryption.

Basically, after the TCP three-way handshake and before sending data over a TCP connection, a client (in our example, the browser) can initiate a TLS Handshake in order to both exchange encryption keys and determine the Cipher Suite the connection will use.

This adds important security features to the connection, enabling encryption, authentication, and data integrity features.

Certificate Authorities play an important role in the process of both encryption and authentication. A Certificate Authority signs a certificate stating the identity has been verified in association with a specific domain name. The certificate is also used to encrypt and decrypt the messages between the sender and the receiver.

A Message Authentication Code (MAC) is used to check whether a message has been tampered with or corrupted during transmission.


## Adding State to HTTP Requests

As noted above, HTTP is stateless. Each HTTP Request is separate from all other HTTP Requests. Yet, in many applications we use in the browser, it appears that the server remembers that we're signed in and other details that seem to suggest the various HTTP Requests know about one another.

Small files stored in the browser that hold information about a session (cookies) are sent during each HTTP Request, letting the server know the state of the client application. A session ID is usually included in the cookie, helping to make the stateless HTTP Request and Response process seem stateful.

Asynchronous JavaScript and XML (AJAX) utilize logic that's passed on to some function to be executed when a certain event takes place (callback) to issue requests and process responses without refreshing the page. The callback function is usually running in the browser as client-side JavaScript, so even though a normal HTTP Request and Response cycle is taking place in the background, the user only sees the data updating as needed without the page refreshing.


## More Security

HTTP can be secured even more with the use of the same-origin policy along with techniques to prevent session hijacking and cross-site scripting (XSS).


### Same-Origin Policy

The same-origin policy allows for unrestricted interaction between resources originating from the same origin while restricting resources originating from different origins. An origin is determined by a URL's scheme, host, and port.

For example, given the URLs `http://example.com/doc1` and `http://example.com/doc2`, Application Programming Interface (API) requests to and from either resource will be allowed. They both share the same origin, i.e. the same scheme `http`, host `example.com`, and port `80`.

Given the URLs `http://example.com/doc1` and `https://example.com/doc2`, API requests would not be allowed since the schemes are different, `http` versus `https`.

The same-origin policy protects against session hijacking and is very important to web application security.


### Session Hijacking Mitigation

Session hijacking is when an attacker intercepts a cookie stored in a browser and copies the session id. The attacker can then use that session id to impersonate (or hijack) a user's session. Without anti-session hijacking measures in place, the attacker will be able to access a user's personal details and completely control a user's account.

Forcing users to reset their session, setting short session expiration times, and using HTTPS can help prevent cookies from being intercepted and prevent their use if they are intercepted.


### Cross-Site Scripting (XSS) Mitigation

Whenever a user is able to input HTML or JavaScript into a form that is then later displayed on a site, the potential for an XSS vulnerability exists. The ability for users to make comments on a post is one example of a form used to then display that content on the page later.

If the form input is not properly sanitized, an attacker can enter JavaScript into the form that can execute code to do almost anything. This type of attack is difficult to detect.

To prevent this, it's important to always sanitize user input. Either don't accept `<script>` tags and other HTML or use another markup language, like Markdown, instead.

Also, one can escape user input when displaying it using HTML entities. This way, the browser will only display the offending code as plain text. The offending code will not be run when the page is accessed.


# The End

With the help of our networking models, protocols, infrastructure, and techniques to simulate statefulness and improve security, we have everything we need to create the Internet. It's all text going back and forth using various rules, converted to bits to travel the world as electrical signals, radio waves, and/or light waves, passing through many routers and servers.

This is how we search for information, listen to streaming music, watch streaming videos, participate in video calls and online conferences, participate in text and voice chat, and do so much more 24 hours a day, 7 days a week, 365 days a year with people all over the world.
