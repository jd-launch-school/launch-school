---
layout: post
title: LS171 Assessment Experience
excerpt_separator: <!--more-->
tags: launch_school ls170 networking assessments
---

I took and passed the LS171 assessment recently. Here are some notes for future reference.

<!--more-->

I found this assessment to be relatively difficult in relation to the RB139 assessment.

I think this is partly because there is a lot of information in LS170 about a lot of aspects of networking as it applies to the web. When preparing for the assessment and following the study guide, I wasn't sure how much detail I was responsible for knowing.

The lessons mention paying close attention to the summaries to help determine the level of detail needed. However, I found that some of the entries in the summaries required one to understand concepts that were not in the summaries.

In essence, it felt like I needed to have a handle on everything.

Obviously, there were some details that we didn't need to know, like the details of every header in each protocol data unit, the nitty gritty details of DNS, and the specific ways that data is transferred through wires and across radio and light waves.

That said, I persisted and did my best.


# Blog Post

This time, the [blog post I wrote](../../../2020/10/16/how-does-the-internet-work.html) about how the Internet works helped me the most, in terms of preparation for the assessment.

My first draft was a loose outline of what happens when one enters a URL into a browser and roughly how that process works. I asked a fellow student for feedback on this first draft to determine if I was thinking about things correctly and what important info I may have left out.

From there, I fleshed out the various parts of the process, adding more detail as needed, until I landed at a more solid draft. I then asked others to read it and implemented their feedback.

On some level, this peer-review process for the blog post was similar to an assessment process, only the people reviewing didn't necessarily know the material well enough to grade me like a Launch School TA.


# Study Guide

Also, this time, I focused a lot on the study guide throughout the lessons. What I mean is that I tried to imagine the important points that might be on the study guide and I created my own study guide as I moved through the lessons. I used the summaries at the end of lessons to help determine what I needed to add to my study guide.

When I got to the LS study guide, I merged theirs with mine and then added the concepts and other things I left out. This time, it seemed reasonable to turn the LS study guide into questions, sort of like a practice test. I then answered those questions with as much precision and detail as I could.

When I had a detailed answer to each of the "questions" on the study guide, I took the assessment.


# Overall Thoughts

Overall, thanks to this process, I feel much more confident about my knowledge of networking in general and the protocols underlying what we know as the Internet.
