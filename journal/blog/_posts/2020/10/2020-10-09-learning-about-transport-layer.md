---
layout: post
title: Learning About the Transport Layer
excerpt_separator: <!--more-->
tags: launch_school networking ls170
---

Continuing the discussion about the various layers and protocols involved with networking and the Internet in general.

<!--more-->

While the Internet Protocol provides a connection between host devices, the transport layer and its protocols provide reliable network communication between the applications running on those hosts.

Once we have a connection between hosts, we have to have some way of splitting up the data so that some data goes to some applications and other data goes to other applications. In essence, we have to be able to transmit and receive multiple data inputs or signals over a single IP channel.

In general, the process of sending multiple data inputs over a single IP channel is known as **multiplexing** while the process of receiving multiple data inputs over a single IP channel is known as **demultiplexing**. More specifically, at the transport layer, multiplexing and demultiplexing take place through the use of ports and sockets.

In order for us to have reliable data transfer, our protocol must have:

-   In order delivery
-   Error detection
-   Data loss handling
-   Duplication handling


# Transmission Control Protocol (TCP)

Enter TCP. One of the cornerstones of the Internet, this **connection-oriented** protocol enables reliable data transfer by implementing all of the features above. In addition to the features above, TCP also can keep track of flow control and congestion.

The Protocol Data Unit (PDU) for TCP is the **TCP Segment**. Like other PDUs, this PDU has a header that contains various fields. These fields include `CHECKSUM`, `SEQUENCE NUMBER`, `ACKNOWLEDGEMENT NUMBER`, `WINDOW SIZE`, and flags `SYN`, `ACK`, `FIN`, and `RST`.

One important part of TCP is that it must first establish a connection before it can transmit data. This connection process is known as a **three-way handshake**. This creates some amount of **round-trip latency**.

![TCP Three-Way Handshake](/assets/img/tcp-three-way-handshake.png)

Once the sender receives the `SYN ACK` segment, it can send the `ACK` segment and start sending data.

TCP provides **flow control** through the `WINDOW` header field which can be set depending upon the amount of data a receiver is willing to accept during the connection.

It also provides **congestion avoidance** by using the amount of data loss as feedback and adjusting the size of transmissions accordingly.


# User Datagram Protocol (UDP)

Another main protocol that's used at the transport layer is the **User Datagram Protocol (UDP)**. This is a **connectionless** protocol, so there is no round-trip latency. The Protocol Data Unit (PDU) for UDP is the **datagram**.

UDP DOES NOT provide:

-   message delivery guarantee
-   In order delivery
-   congestion avoidance
-   flow control
-   connection state

Instead, UDP offers speed and flexibility, leaving it up to the software engineer to determine which connection reliability features they need, depending on the needs of the specific application.

Voice and/or video calling applications and online games tend to use UDP since they often need to prioritize speed and lag reduction over the quality of the data being transferred. A few pixels missing from a video or some small in-game glitches might be preferable over long lag spikes.
