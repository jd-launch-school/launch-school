---
layout: post
title: Learning About the Physical Network, Link, and Internet Layers
excerpt_separator: <!--more-->
tags: launch_school ls170 networking
---

Here are some notes about the Internet and networking in general.

<!--more-->

We start at the question, "What is the Internet?" This seems like a simple question to answer, given that it's so ubiquitous in our lives. However, pinning it down specifically can be tricky.


# The Internet

Basically, the Internet is a network of networks that's made up of a combination of network infrastructure (devices, routers, cables, etc&#x2026;) and protocols.

To help visualize this, we can start at the local network level, the **Local Area Network (LAN)**. The local network is the network in one's home, for example. There are many potential setups for this, but most of us probably have a modem and router (either two separate devices or one combined device) that routes signals from the outside Internet to the various devices connected to it within the network, and vice-versa. Oftentimes, this is in the form of a **Wireless Local Area Network (WLAN)**.

Businesses and Internet service providers use switches, hubs, routers, and other devices to route signals where they need to go. There are long fiber optic cables going from state to state and country to country, connecting routers to one another.

This network of networks is the Internet.


# Protocols and Layers

In order for signals to travel from network to network, through different devices, routers, cultures, countries, regulations, etc&#x2026;, we need to have standardized rules for communication. These rules are known as **protocols**. There are many different protocols, some deal with different aspects of communication while others deal with similar aspects of communication in different ways. Some examples include: `IP`, `SMTP`, `TCP`, `UDP`, `HTTP`, `Ethernet`, `FTP`, `DNS`, and `TLS`.

To solve the problem of communication between protocols, engineers developed a layered system where one layer provides what another layer needs through data payload encapsulation. There are two models for understanding the various layers involved, the [OSI model](https://en.wikipedia.org/wiki/OSI_model) and the [Internet Protocol Suite](https://en.wikipedia.org/wiki/Internet_protocol_suite) or TCP/IP model.

[![Data Payload Layer Encapsulation](/assets/img/data_payload_encapsulation.png)](https://en.wikipedia.org/wiki/File:UDP_encapsulation.svg)

From the above image, we can see that the data in the Application layer becomes the data payload for the Transport layer. The Transport layer adds a header and then the combined Transport layer header and data payload become the data payload for the Internet layer.

This enables one layer to transfer the data from another layer without having to understand the details of the previous layer.


# More Details About the Layers

Now that we have a simple understanding of the various layers involved, we can look more closely at each layer.


## Physcial Network

The bottom layer, if you will, is the physical network that's made up of devices, cables, and radio and light waves. At this layer, we're only concerned with **bits**. A bit is binary data that can either be a `1` or a `0`. Bits are converted into their electrical counterpart (`on` or `off`) and transferred across devices as needed.

For future reference:

```bash
8 bits        = 1 byte
32 bits       = 4 bytes
64 bits       = 8 bytes
1 kilobit     = 125 bytes
1 megabit     = 125 kilobytes
100 megabits  = 12.5 megabytes
1 gigabit     = 125 megabytes
```

There are two characteristics of physical networks that need to be taken into consideration when thinking about performance: **latency** and **bandwidth**. Latency is the time it takes for data to get from one point to another and bandwidth is the amount of data that can be sent in a specific unit of time, usually seconds.


## The Link / Data Link Layer

This is the interface between the physical devices and logical or digital code. It's main purpose is to route data from one device to another. This is the layer where **Protocol Data Units** (PDUs) start to be used.

The **Ethernet** protocol is the most commonly used protocol at this layer. It's a standard that enables communication between devices. The PDU for the Ethernet protocol is known as an **Ethernet Frame**.

The key point to remember about the Ethernet protocol is that it uses **MAC (Media Access Control) addresses** to keep track of both the source and destination MAC address within the header of the Ethernet Frame. Every network-enabled device has a unique MAC address, sometimes referred to as its physical address. This gets sent along with the data payload and tells routers where the Frame is coming from and where it needs to go.


## The Internet / Network Layer

This layer is responsible for facilitating the communication between computers on different networks. An example is when one computer on one network wants to communication with another specific computer on a different network. The message will be routed through the Link/Data Link layer from router to router, but once the message gets to the destination router, the Internet/Network layer kicks in and directs the message to the specific computer.

The most-used protocol at this layer is the **Internet Protocol (IP)**. There are two versions, **IPv4** and **IPv6**. The PDU at this layer is known as a **Packet**.

Unlike a MAC Address, an **IP address** is logical and can be assigned to devices as they join a network. So, each device's networking card gets an IP address assigned to it once it connects to a network. This allows the LAN router to direct signals to that device as needed.

[![Data Payload Layer Encapsulation](/assets/img/ipv4_address.png)](https://en.wikipedia.org/wiki/IP_address#/media/File:Ipv4_address.svg)

Some differences between IPv4 and IPv6:

-   IPv4 is a 32 bit address, IPv6 is a 128 bit address
-   IPv4 can accommodate ~4.3 billion addresses whereas IPv6 can accommodate ~340 undecillion addresses
-   IPv4 and IPv6 have different header structures
-   IPv4 handles errors whereas IPv6 relies on other layers to handle errors


# Networked Applications

These layers do well to get signals from one computer on one network to another computer on another network. However, they do not help when it comes to routing to specific applications running on various networking devices. This is the work of higher layers.
