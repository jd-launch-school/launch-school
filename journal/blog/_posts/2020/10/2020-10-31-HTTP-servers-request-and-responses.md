---
layout: post
title: HTTP Servers, Requests, and Responses
excerpt_separator: <!--more-->
tags: launch_school ruby networking rb175 HTML HTTP
---

This post serves as a summary of the things I've been learning over the past week. We start with an HTTP server using the `TCPServer` class in Ruby. Then, we move to Sinatra. Finally, we deploy a Sinatra application to Heroku.

<!--more-->


# The HTTP Server

Using the information we learn from LS170 and the LS171 written assessment, we now move on to learning how to implement those concepts within Ruby.

HTTP is just an exchange of text between a client and a server. We're encouraged to remember this while working with frameworks like Sinatra that tend to hide the HTTP request and response details.

We took a slight detour to brush up on some very basic HTML and CSS, and then jumped into our HTTP server.

Our first step is to implement our own HTTP server that can receive HTTP requests from a client and send HTTP responses back to the client. That may sound complicated, but it's just text being passed back and forth, leaving both the client and the server to do the heavy lifting of managing that text.

We use the `Socket` class, well a child of the `Socket` class called the `TCPServer` class, from the Ruby Standard Library to help us create the server. The [Ruby TCPServer class documentation](https://docs.ruby-lang.org/en/2.7.0/TCPServer.html) has a good example of something similar to what we created:

```ruby
require 'socket'

server = TCPServer.new 2000 # Server bind to port 2000
loop do
  client = server.accept    # Wait for a client to connect
  client.puts "Hello !"
  client.puts "Time is #{Time.now}"
  client.close
end
```

When this file is executed, it creates a socket object that's listening at `localhost:2000`. We then use a client to connect. The HTTP server above will not necessarily work with browsers. However, the Ruby docs show a simple client that one can use to connect to this server in order to see the output. With some slight modifications, we can get this to work in both Firefox and Chrome browsers:

```ruby
require 'socket'

server = TCPServer.new("localhost", 2000)
loop do
  client = server.accept
  request_line = client.gets.split
  client.puts "HTTP/1.1 200 OK\r\n\r\n" # valid response for Chrome
  client.puts "Hello !"
  client.puts "Time is #{Time.now}"
  client.close
end
```

We then create a small dice rolling application that can take parameters to determine both the number of sides and the number of dice to roll. Basically, this involves adding some code to our HTTP server.

This is to parse the request line and get the passed in parameters into a Hash that can be used later:

```ruby
def parse_request(request_line)
  http_method, path, http_version = request_line.split
  params = path.delete("?/").split('&').to_h { |param| param.split('=') }

  [http_method, path, http_version, params]
end
```

This rolls the dice and displays the results:

```ruby
params["rolls"].to_i.times do |index|
  client.puts "Dice roll #{index + 1}: #{rand(params["sides"].to_i) + 1}"
end
```

Up to this point, the HTTP response from the server wouldn't necessarily be acceptable as a complete HTTP response to clients that are expecting well-formed HTTP responses. To fix this, we respond with the response line `HTTP/1.1 200 OK` and an HTTP response header for the content type `Content-Type: text/html`. We add two new lines after the last response header to indicate the start of the body of the response, which now uses `<html>`, `<head>`, and `<body>` HTML elements.

What if we wanted to simulate an application state? Well, we do this using parameters and we used a counter app as an example.

```ruby
number = params["number"].to_i

client.puts """
         <!-- other code here -->

         <h2>Counter App</h2>
         <p>Current Number: #{number}</p>
         <p><a href='?number=#{number + 1}'>Increment (+1)</a></p>
         <p><a href='?number=#{number - 1}'>Decrement (-1)</a></p>

         <h2>Commands</h2>
         <p><a href='?number=0'>Reset counter to zero</a></p>
         <p><a href='?quit=true'>Stop server</a></p>
"""
```

So, for each request, we assign the value of the `number` parameter to the `number` local variable and then modify the Increment and Decrement links to add or subtract 1 from the current value of the `number` local variable. This produces the effect of the app maintaining the state of the current number and operating on that number, even though each HTTP request and response is separate from all previous and future requests and responses. Here is my version in action:

![Simulate State HTTP Server](/assets/img/counter_simulate_state_http_server.gif)

# Rack

Before we learn about Sinatra, we have to learn about Rack. Rack is a protocol that makes it easier for applications to work with HTTP requests and responses. A Rack-based framework is a framework that conforms to the Rack standard when sending and receiving HTTP requests and responses. Sinatra is an example of a Rack-based framework.

A Rack-based framework must implement a `call` method that accepts an `env` parameter and returns an Array that includes the status code as the first element, a Hash with HTTP headers, and an Array (or other object that can respond to an `each` method) that contains the body of the HTTP response.

Here's an example of a `call` method that would be using the Rack protocol:

```ruby
class App
  def call(env)
    [
      200,
      { 'Content-Length' => 25, 'Content-Type' => 'text/html' },
      [ "<html>", "...", "</html>" ]
    ]
  end
end
```

We also need a Rackup file `config.ru`, the `ru` stands for `rack up`:

```ruby
require_relative 'hello_world'

run HelloWorld.new
```

We then take some time to build out a small app using this raw Rack syntax. We implement Embedded Ruby (ERB) templates and break code up into multiple files. Ultimately, we go from that basic app to creating our own framework. We used the [Growing Your Own Web Framework with Rack](https://launchschool.com/blog/growing-your-own-web-framework-with-rack-part-1) blog post series as a guide.


# Sinatra

Having some experience with the underlying Rack specification, we can now move on to Sinatra, which handles all of this behind the scenes. Sinatra is a web development framework that automates many of the common tasks involved with using Rack and HTTP servers.

We walk through the creation of a book reader app that includes search functionality, directing search results to specific paragraphs containing the search term. We also make an app to traverse a directory and display the contents of a file as well as an app to use a YAML file containing users and their interests to display the users and interests.

The following is the app we created in the Growing Your Own Web Framework with Rack series:

```ruby
require_relative 'advice'

class App
  def call(env)
    case env['REQUEST_PATH']
    when '/'
      [
        '200',
        { 'Content-Type' => 'text/html' },
        [ erb(:index) ]
      ]
    when '/advice'
      piece_of_advice = Advice.new.generate
      [
        '200',
        { 'Content-Type' => 'text/html' },
        [ erb(:advice, message: piece_of_advice) ]
      ]
    when '/info'
      [
        '200',
        { 'Content-Type' => 'text/html' },
        [ erb(:env, message: env) ]
      ]
    else
      [
        '404',
        { 'Content-Type' => 'text/html', 'Content-Length' => '48' },
        [ erb(:not_found) ]
      ]
    end
  end

  private

  def erb(filename, local = {})
    b = binding
    message = local[:message]
    content = File.read("views/#{filename}.erb")
    ERB.new(content).result(b)
  end
end
```

With Sinatra, this can be condensed down into:

```ruby
require 'sinatra'
require 'sinatra/reloader'
require_relative 'advice'

get '/' do
  erb :index
end

get '/info' do
  erb :env, :locals => { message: env }
end

get '/advice' do
  piece_of_advice = Advice.new.generate
  erb :advice, :locals => { message: piece_of_advice }
end
```

I think this comparison demonstrates the benefits of using a framework like Sinatra for our web applications.


# Quiz (Spoiler)

I found the quiz for this section to be relatively complicated and time-consuming. It's one of the longer quizzes I've had up to this point. Some of the questions were straightforward, like which file out of the list of files presented is used for the startup code. Other questions, seemed trickier.

One question in particular really surprised me. The question provided some code of an ERB file with one part of the code missing. It asked us which of the following code snippets would work to print each element of an array as a separate list element.

This required paying close attention to the specific syntax used. For instance, in ERB templates, it's possible to have both `<% some ruby code %>` and `<%= some ruby code %>` syntax. The only difference between the two is the `=` sign in the second code snippet, which displays the code. The `<% some ruby code %>` version runs the code without displaying anything. That small difference, combined with the other slight differences between the code snippets made this question more challenging.

One of the snippets was `<%= @words.map { |word| "<li>#{word}</li>\n" }.join %>`. That `\n` threw me off, which I'm sure in hindsight was the intention. When I first looked at this, I thought the `\n` would be printed by the browser so it would not yield the results we wanted. However, after getting this wrong and testing this, it turns out that the when the HTML is created by ERB, it interprets those line breaks `\n` and adds them to the HTML string. The browser ignores the extra line breaks and only focuses on the `<li>` tags, which include line breaks in the display.

So, that snippet is the same as `<%= @words.map { |word| "<li>#{word}</li>" }.join %>`, which is valid and would return a string with each word as an item in the list.

The other part of this question is to recognize that embedding a string that ultimately looks like `<li>word1</li>\n<li>word2</li>` is the same as looping through using `<% @words.map do |word| %>` and then enclosing each `word` within `<li>` tags like this: `<li>word</li>`.


# Deploying to Heroku

Finally we learn about the things we need to include in our app in order to make it compliant with Heroku's needs. We learn how to use Heroku. We also learn why we should use a production-ready web server like `Puma` instead of the web server `WEBrick` that's in the Ruby Standard Library.

Once I deployed my book reader app, I wanted to include the search functionality in a more prominent place. So, I added it to the page header so it is always available. In doing this, I noticed that I hadn't sanitized the input form.

I looked up some ways to do this and found the `Sanitize` gem. In my tests with my limited knowledge of sanitizing HTML input, using this gem now makes the app seem to be more resistant to tampering. This is something that comes up in a couple more lessons, so I'll wait until then to go deeper. But for now, here's a link to the app for future reference:

<https://ls-rb175-book-viewer-jd.herokuapp.com/>

![Book Viewer Search](/assets/img/book_viewer_search.gif)
