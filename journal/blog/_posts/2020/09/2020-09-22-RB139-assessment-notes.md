---
layout: post
title: RB139 Assessment Notes
excerpt_separator: <!--more-->
tags: launch_school ruby rb130 assessments
---

I took and passed the RB139 written assessment yesterday. I prepared in much the same way that I prepared for the [RB129 written assessment](https://jd-launch-school.gitlab.io/launch-school/2020/08/01/how-to-take-written-assessment-for-launch-school.html). Here are some more thoughts on the process and experience.

<!--more-->

Overall, I felt like this was a long and interesting assessment. I used almost the full 4 hours, although I finished all of the questions in about 3 hours. I spent the last hour making sure my answers were complete.

At one point during the assessment, after my nerves calmed a bit, I remember thinking how enjoyable it was to be challenged with these types of questions.

To prepare for the assessment, I finished all of the Easy challenge exercises and four of the Medium challenge exercises. I watched all of the video walkthroughs and tried to focus on the practice of problem solving rather than the code used to solve a specific problem. I think I was well-prepared for the assessment, as far as solving coding challenges goes.

In terms of the topics, I compiled information about each of the topics in the study guide into one place so it was easily accessible. Even though I had good notes, I still found the assessment challenging in that I had to think beyond the notes to answer many of the questions.

I did two things differently this time.

1.  **I didn't attend any study groups.** I found this to be refreshing on some level as sometimes, for past assessments, I'd feel more confused after a study session. This time, I did my best to look through the forums for nuances and follow Slack discussions to determine the level of detail I'd need for the assessment.

2.  **I took the assessment earlier than I did for previous assessments.** In the past, I think in part because of study sessions, I waited to take the assessment until I was absolutely sure I was ready. This time, I woke up one morning (early) and decided to take the assessment. I had been preparing for the previous two weeks, so I knew I was ready, but I jumped into the assessment even though I didn't necessarily feel like I knew for sure that I was ready. It's a subtle difference, but important nonetheless.

All in all, I felt the entire experience in RB130 was enjoyable and very helpful in terms of understanding closures, binding, blocks, procs, lambdas, testing, and packaging.
