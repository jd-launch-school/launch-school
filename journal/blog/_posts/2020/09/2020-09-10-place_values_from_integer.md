---
layout: post
title: Getting the Place Values from an Integer
excerpt_separator: <!--more-->
tags: ruby
---

This is an interesting trick for getting the place values of an Integer that I want to remember for future reference.

<!--more-->

So, say we have some problem where we need to convert each place value of an Integer into something else. This may come in handy in cases where we're determining how many pennies, nickels, dimes, and quarters to use to make up a given amount. Or, maybe when to use `I` versus `X` in Roman Numerals.

In Ruby, we can use multiple assignment and the `Integer#digits` method relatively easily like:

```ruby
given_integer = 4321

ones, tens, hundreds, thousands = given_integer.digits

puts "Ones: #{ones}"
puts "Tens: #{tens}"
puts "Hundreds: #{hundreds}"
puts "Thousands: #{thousands}"
```

    Ones: 1
    Tens: 2
    Hundreds: 3
    Thousands: 4

This works because the `Integer#digits` method converts an Integer into an array of Integers and reverses the order of the digits.

```ruby
given_integer = 4321

p given_integer.digits
```

    [1, 2, 3, 4]

Then, with multiple assignment, we can assign each element of the array to a separate variable. Here's a simpler example where we assign the Integer object with value `1` to the local variable `a`, the Integer object with value `2` to the local variable `b`, and the Integer object with value `3` to the local variable `c`:

```ruby
a, b, c = [1, 2, 3]

p a, b, c
```

    1
    2
    3

What happens for smaller or larger numbers?

```ruby
given_integer = 321

ones, tens, hundreds, thousands = given_integer.digits

puts "Smaller Number"
puts "--------------"
p ones, tens, hundreds, thousands

given_integer = 54321

ones, tens, hundreds, thousands = given_integer.digits

puts "\nLarger Number"
puts "-------------"
p ones, tens, hundreds, thousands
```

    Smaller Number
    --------------
    1
    2
    3
    nil
    
    Larger Number
    -------------
    1
    2
    3
    4

When there are less elements in the array than variables, Ruby assigns `nil` to the extra variables. In the above 'Smaller Number' example, `thousands` references `nil`.

When there are more elements in the array than variables, the extra elements are not used. In the above 'Larger Number' example, only the digits in the thousands, hundreds, tens, and ones places are assigned to variables.

It's probably important to note that this works best with relatively smaller numbers.

```ruby
given_integer = 1_234_567

ones, tens, hundreds, thousands, ten_thousands, hundred_thousands, millions = given_integer.digits

p ones, tens, hundreds, thousands, ten_thousands, hundred_thousands, millions
```

    7
    6
    5
    4
    3
    2
    1

Any higher than that will make the code more difficult to read.

That's it. Short and simple. Remembering this may have saved me some time in a recent problem I was working on. Hopefully, by taking some time to write about this, I'll be more likely to remember it when I need it in the future.
