---
layout: post
title: Sinatra and Sanitizing HTML
excerpt_separator: <!--more-->
tags: launch_school ruby networking rb175 sinatra
---

More about Sinatra and sanitizing (escaping) HTML in Sinatra.

<!--more-->


# Sinatra

This week, I focused on Sinatra and built two apps. The first was a Todo Tracker app that was basically following along with a walk-thru provided by Launch School. The second was an Agenda app that I created to help solidify the things I learned during the walk-thru.

The walk-thru consisted of watching videos, following along and writing the code, and then solving various problems related to the code. The problems were mostly about adding various features to the app using what we learned up to the point the problem was introduced.


# Quick Note About the Videos

I don't learn very well via video, so the learning from the Todo Tracker project was challenging for me. It's extremely easy for me to copy code from a video and feel like I understand it. I constantly have to pause the video, rewind, pause again, and so on. I don't always notice the nuances and tiny details involved in what the video is showing even though I play around with the code on my own.

I think part of the problem with videos, for me, is that they often don't show natural practice and learning. They're usually produced in such a way that it appears like the person in the video is moving from one step to the next with very little effort. They hide the many hours of effort that it likely took for the person in the video to be at the point where they feel comfortable making a video.

This tricks my brain into thinking that I, too, will be able to do that. Then, when reality strikes and I'm struggling on something that the person in the video breezed by, I feel incapable and inferior.

This part of the curriculum seems equivalent to many of the video courses around the Internet. I don't see the same Launch School mastery philosophy here. Granted, we're not expected to master this material, so I guess this is to be expected.

Unfortunately, it's taking me just as long to get through this material as it does any other Launch School material.


# Back to Sinatra

The apps this week were far more complicated than the Book Viewer app last week. There are a lot of moving parts and the main Ruby file seems to be getting longer and longer.

We focused on routes and views. When should we make a `post /edit` versus `get /edit` request. Basically, the difference between `POST` and `GET` HTTP requests is that the `POST` request sends data in the body of the request whereas the `GET` request puts that data into the URL as query parameters in a query string.

Sinatra automatically serves files it finds in the `/public` directory. So, when linking to these files in HTML, it's important to remember this and not include `/public` in the URL.

We practiced more with redirection and when to use `redirect` versus `erb`. When we want data to persist, we should re-render the template using `erb` so parameters will remain intact. `redirect` reloads the browser showing the new template and refreshes parameters. So, if a form is edited incorrectly and a message needs to be shown to the user, then redirected back to the form, it's best to use `erb` so the form can show the parameters the user entered.

We learned and practiced a lot more about Sinatra. Most of it involved working with HTML, ERB, and Ruby.


# Sanitizing HTML

We learned about two ways to sanitize or escape HTML.


## Modify every part of all templates where user-provided input is displayed

In routes file (.rb):

```ruby
helpers do
  def h(content)
    Rack::Utils.escape_html(content)
  end
end
```

In every template that displays user-provided input:

```html
<h3><%= h todo[:name] %></h3>
```


## Escape all HTML and only allow HTML that's trusted

In routes file (.rb)

```ruby
configure do
  set :erb, :escape_html => true
end
```

Disable with `<%==` instead of `<%=`.

Disable escaping where needed in templates, probably mostly in (layout.rb):

```html
<!-- other code -->
<%== yield content :header_links %>

<!-- other code -->
<%== yield %>
```

This seems to be a cleaner way of handling HTML than the Sanitize gem that I used for my Book Viewer app.
