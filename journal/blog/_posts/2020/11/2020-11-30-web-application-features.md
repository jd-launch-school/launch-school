---
layout: post
title: Web Application Features
excerpt_separator: <!--more-->
tags: launch_school ruby rb175
---

By the time one gets to the end of RB175, one has been exposed to many features that are common in typical web applications. Here's a list of the features I've learned along with some notes and implementation details using Ruby and Sinatra.

<!--more-->


# View Markdown File

We can display Markdown using the `Redcarpet` library. First, we add the library to our `Gemfile`:

```ruby
gem 'redcarpet'
```

Then, we can convert Markdown to HTML with the following:

```ruby
require 'redcarpet'

markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML)
markdown.render("# This is a headline in markdown")
```

`Redcarpet::Markdown#render` returns a String. We can display that String in an embedded Ruby file (`.erb`) to show the HTML representation of the Markdown text in the browser.


# Edit Contents of a File

In the GET route to edit a file, we determine the details needed to pass on to the view:

```ruby
get '/:file/edit' do
  @file = params[:file]
  @title = "Editing '#{@file}'"
  file_path = File.join(data_path, @file)
  @file_contents = File.read(file_path)
  erb :edit_file
end
```

`data_path` is determined by checking the current Rack environment and it returns a different path depending on whether or not the environment is the `test` environment:

```ruby
def data_path
  if ENV["RACK_ENV"] == "test"
    File.expand_path("../test/data", __FILE__)
  else
    File.expand_path("../data", __FILE__)
  end
end
```

We create a view to allow a user to edit the contents of a file, something like this:

```html
<form action="/<%= @file %>/edit" method="post">
  <label for="file_content">Edit content of <%= @file %>:<br>
    <textarea name="file_contents" rows="25" cols="100"><%= @file_contents %></textarea>
  </label>

  <p><button type="submit">Save Changes</button></p>
</form>
```

We also create a new route `/:file/edit` to handle the action for the HTML form and actually write the edited contents to the file:

```ruby
post '/:file/edit' do
  @file = params[:file]
  @file_path = File.join(data_path, @file)

  file_contents = params[:file_contents]
  File.write(@file_path, file_contents)
  session[:success] = "Successfully edited #{@file}."
  redirect '/'
end
```

We write the `file_contents` to a file by using `File::write`. We could also write the `file_contents` to a file by using `File::open`, as needed:

```ruby
File.open(file_path, 'w') do |file|
  file.write(@file_contents)
end
```


# Show Files in a Directory

To display a list of files in a directory. We use the `Dir::glob` method to get an Array of file names for each file in the given directory:

```ruby
@files = Dir.glob('*', base: data_path)
```

We can then display this list of files to the user in a view:

```html
<ul>
  <% @files.each do |file| %>
    <li>
      <a href="<%= file %>"><%= file %></a>

      <form class="inline" action="<%= file %>/edit" method="get">
        <button type="submit">Edit</button>
      </form>
    </li>
  <% end %>
</ul>
```

[Here's a video of me going through this process.](https://archive.org/details/ruby-sinatra-dynamic-directory-challenge)


# User Login

We store our users in a YAML file `users.yml`:

```yaml
---
admin: password1
user1: password2
user2: password3
```

We need a GET route so the user can request the login form:

```ruby
get '/login' do
  @title = "Sign In"
  erb :login
end
```

We create a `login` view for the user to enter their username and password:

```html
<form action="/login" method="post">
  <label for="username">Username:
    <input type="text" name="username" value="<%= params[:username] %>">
  </label><br>

  <label for="password">Password:
    <input type="password" name="password">
  </label><br>

  <button type="submit">Sign In</button>
</form>
```

We create the route `/login` to handle the action for the login form and determine if the username and password combination is correct:

```ruby
post '/login' do
  @username = params[:username]
  users = YAML.load_file(File.join(yaml_path, 'users.yml'))

  if users[@username] && users[@username] == params[:password]
    session[:username] = @username
    session[:success] = 'Welcome!'
    redirect '/'
  else
    session[:error] = 'Invalid Credentials'
    status 422
    @title = "Sign In"
    erb :login
  end
end
```

`yaml_path` is determined by checking the current Rack environment and it returns a different path depending on whether or not the environment is the `test` environment, very similar to the `data_path` method above:

```ruby
def yaml_path
  if ENV["RACK_ENV"] == "test"
    File.expand_path("../test", __FILE__)
  else
    File.expand_path("../", __FILE__)
  end
end
```

At this point, we're storing passwords in plain text. This is a **BAD** idea.


# Hash Passwords

We can use the `BCrypt` module to hash passwords. First, we add the gem to our `Gemfile`:

```ruby
gem "bcrypt"
```

Then, we require the `bcrypt` module:

```ruby
require 'bcrypt'
```

We hash the existing passwords and store the hashed passwords in the `users.yml` instead of the plain text passwords:

```ruby
BCrypt::Password.create(password)
```

This returns something that looks like this:

```
$2a$12$qShvxMRDAErT7p8UtaKcPuOhnHxa9wyAJ.yMDoQ.ZoEPPSVCgaac2
```

When we want to check a given password to see if it matches the password hash, we can replace this from the `/login` POST route above:

```ruby
if users[@username] && users[@username] == params[:password]
```

with this:

```ruby
if users[@username] && BCrypt::Password.new(users[@username]) == params[:password]
```


# User Registration

We'll need a GET route `/register` to direct users to a user registration page:

```ruby
get '/register' do
  @title = "Register New User"
  erb :register
end
```

And here's the corresponding view, identical to the `login.erb` view, but with the `/register` action instead of `/login`:

```html
<form action="/register" method="post">
  <label for="username">Username:
    <input type="text" name="username" value="<%= params[:username] %>">
  </label><br>

  <label for="password">Password:
    <input type="password" name="password">
  </label><br>

  <button type="submit">Register</button>
</form>
```

A basic version of this feature can be added with the following `/register` POST route:

```ruby
post '/register' do
  @username = params[:username]
  password = params[:password]
  file_path = File.join(yaml_path, 'users.yml')

  File.open(file_path, 'a') do |file|
    file.write("#{@username}: #{BCrypt::Password.create(password)}\n")
  end

  session[:success] = 'New user successfully created'
  redirect '/'
end
```

However, this allows anyone to create a user and then gain access to any protected views and actions.

First, we can check the username and password for various errors:

```ruby
def error_for_username(username, users)
  return "Username cannot be empty." if username.empty?

  if username.match?(/[^a-z0-9]/i)
    return "Username must contain only letters and numbers."
  end

  if users.keys.any? { |value| value == username }
    "Username already exists."
  end
end
```

```ruby
def error_for_password(password)
  return "Password cannot be empty." if password.empty?
end
```

Then, we can add these into our `/register` POST route along with storing the submitted information in a separate file to be reviewed by site administrators:

```ruby
post '/register' do
  @username = params[:username]
  password = params[:password]
  file_path = File.join(yaml_path, 'users.yml')

  users = YAML.load_file(file_path)
  error = error_for_username(@username, users) || error_for_password(password)

  if error
    session[:error] = error
    status 422
    @title = 'Register New User'
    erb :register
  else
    File.open(File.join(yaml_path, 'new_users.yml'), 'a') do |file|
      file.write("#{@username}: #{write_pw(password)}\n")
    end

    session[:success] = 'New user successfully created. An administrator will review your registration soon.'
    redirect '/'
  end
end
```

This way, users need to be approved before gaining access to user-only content.


# Show List of Users

Here's a basic setup that can be used to display a list of users.

Using Sinatra's `before` method, we can have the list of users available before every route:

```ruby
before do
  @users = YAML.load_file('users.yml')
end
```

Here's a `/users` GET route:

```ruby
get '/users' do
  @title = "Users"
  @names_of_users = @users.keys.map { |user| user.to_s }
  erb :home
end
```

We can access the instance variable from within a view like this in order to display the users:

```ruby
<ul>
  <% @names_of_users.each do |user| %>
    <li><a href="/users/<%= user %>"><%= user.capitalize %></a></li>
  <% end %>
</ul>
```

[Here's a video of me going through this process.](https://archive.org/details/ruby-sinatra-users-interests-challenge)


# Search

This is in the context of a book with chapters and paragraphs, however it can be applied to the content of files or other content as needed.

The `/search` GET route is somewhat complicated:

```ruby
get "/search" do
  @results = @toc.each_with_index.reduce({}) do |hash, (title, index)|
    contents = File.read "data/chp#{index + 1}.txt"
    title.strip!
    hash[title] = [index + 1, contents] if contents.match(params[:query])
    hash
  end

  erb :search
end
```

Here, we use the table of contents and specific file names to read the contents of a file and create a Hash with the title of the chapter as a key and the paragraph number `index + 1` and the contents of the paragraph as the values.

The search view is also relatively complicated:

```ruby
<h2 class="content-subhead">Search</h2>

<form action="/search" method="get">
  <input name="query" value="<%= params[:query] %>">
  <button type="submit">Search</button>
</form>

<% unless params.empty? %>
  <h3 class="content-subhead">Results for '<%= params[:query] %>'</h3>
  <% if @results.empty? || params[:query] == '' %>
    <p><%= "Sorry, no matches were found" %></p>
  <% else %>
    <ul>
      <% @results.each do |title, (num, content)| %>
        <li>
          <a href="/chapters/<%= num %>""><%= title %></a>
        </li>
      <% end %>
    </ul>
  <% end %>
<% end %>
```


# Summary

There are more features we were exposed to, like making duplicate files and storing versions of edited files. Also, this is more like a proof-of-concept than usable production code. While we learning, it's important to understand how things work first and then we can work on refactoring for security and best practices.
