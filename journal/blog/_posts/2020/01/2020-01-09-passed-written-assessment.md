---
layout: post
title: Passed RB101-RB109 Written Assessment
excerpt_separator: <!--more-->
tags: launch_school ruby assessments rb101
---

I took the RB101-RB109 written assessment yesterday morning and passed! Here are some observations about the assessment for future reference.

<!--more-->

The assessment uses the same system that the lesson quizzes uses, so that was familiar. I had 21 questions and I took the entire 3 hours. I had about 15-20 minutes for review when I finished a first pass at all of the questions.


# Level of Precision

It seemed daunting and somewhat exhausting to be tasked with answering 21 questions with a relatively unknown level of precision. Well, I knew I had to be precise, but as I was studying, I often came across examples of more or less precise language and it wasn't clear whether the less precise language in certain cases would be sufficient.

So, for example, will it always be necessary to describe precisely what's happening with the `p` method in code like the following:

```ruby
def hello
  p 'hello world'
end

hello
```

    "hello world"

Will one need to describe `p` like this:

> We call the `p` method and pass in the string object with value `hello world`. This outputs the string representation of the return value from passing the string object as an argument to the `inspect` method, printing `"hello world"` to the screen.

Or, will something less specific like the following be OK?

> We call the `p` method and pass in the string object with value `hello world`, printing `hello world` to the screen.

It turns out that it mostly depends on the question that's being asked. If the question asks for as much precision as possible, then the first response is better. If, however, the question asks to describe the concept at work here, in this case `method definition` and `method_invocation`, then the precision about the `p` method may not be necessary.


# Question Length

The questions were much longer than I'd anticipated, sometimes multiple sentences with multiple parts. I found that it took some time to read and fully understand some of the questions.

I often read a question multiple times, just to be sure that I'd answered each part. I'd heard from the study sessions that not answering each part of the question was one of the reasons people received lower scores.

Also, many of the outputs for the code snippets in question were given to us in the question itself, like:

> Explain why the following code outputs `Goodbye` with precise language. What is the underlying concept?

```ruby
def greetings(str)
  puts "Goodbye"
end

word = "Hello"
greetings(word)
```

    Goodbye

This contrasts with the way I studied in that, when I studied, I needed to determine the output of the code snippets AND explain why. In the assessment, on the other hand, I only needed to explain why.

Overall, I feel the question wording helped a lot in terms of knowing what level of precision to use.

When I practice for assessments in the future, I'm going to practice by timing myself with longer questions.


# Practice and Preparations Leading Up To The Assessment

I studied for the assessment for about a month, starting in the last week of November 2019 through the first week of January 2020. I'm glad I took this time to study. As I neared the end, I started to feel like I wasn't gaining anything new from my study sessions and I was starting to see things being repeated. I guess this is the point where I knew I was ready for the assessment. In my case, it's not easy for me to find an uninterrupted 3-hour block of time, so I had to wait a little longer for that.

I practiced writing out precise explanations in markdown format for as many code snippets as I could. The more explanations I wrote, the easier the language came to me. I think this helped me finish the assessment on time as the words and phrases are now seared into my memory, so typing them out on the fly didn't take long once I knew what I wanted to type.

During the first group study session, I realized that my hands were used to writing in org-mode and writing in markdown felt foreign. I'd read from others that it would be helpful to practice writing in markdown, so I stopped using `org-mode` and `yasnippets` and started writing in plain text with markdown.

I'm sure Emacs has some neat features for working with markdown, but I wanted to mimic the test environment as close as possible, so that meant writing markdown in plain text. After practicing like this, I'm finding the markdown to be more familiar than org-mode, making this blog post more difficult to write. :)

This basically meant writing:

```
On line 3, we call the `map` method on the array `[1, 2, 3]`...
```

instead of:

```
On line 3, we call the ~map~ method on the array ~[1, 2, 3]~...
```

As small a difference as it is, practicing by writing in plain text markdown made this one less thing I had to think about during the assessment.

I attended 5 study sessions with Launch School Teaching Assistants (TAs). It got to the point where I'd seen all of the examples one of the TAs gave out to students. That was also an indication that it was time to move on and take the assessment.

I went through the study guide, read articles from other students, watched videos to tweak the precision of my language, took copious notes to try and remember everything, drilled myself on a few code snippets each day, thought about the precise language throughout my day, dreamed about it, exercised regularly, ate as well as I could, and slept as much as possible so that I might be in the best possible condition while working through the assessment.

I created a [mock assessment](https://gitlab.com/jd-launch-school/launch-school/blob/e60eeb1b1b386315860dc4c4bc3bf4ed3e7f86b9/mock_assessments.org) using the problems I'd collected in the study sessions. It turns out that it's not that close to the real assessment as some of the examples in the mock assessment are more complex, there are some concepts missing from the mock assessment, and there are no specific questions about various concepts in the mock assessment.

It's a start and it was helpful for me to get a sense of the timing it would take me to be as precise as possible.

I also created notes with specific language describing various methods and concepts. The notes that helped me the most were the wording for the `each`, `select`, and `map` methods. I was often able to copy and paste from my notes directly into the assessment for these methods, which helped me have confidence that I'd covered the necessary parts of each method. Here they are for future reference:

**Map**

> The `map` method transforms each element in the calling array, returning a new array containing elements based on the return value of the block.

For example:

> On line 3, we call the `map` method on the array `[1, 2, 3]` and pass in a `{..}` block as an argument with `num` as a parameter. The `map` method transforms each element in the calling array, returning a new array containing elements based on the return value of the block.
> 
> The return value of the block for each iteration is either based on an explicit `return` statement or an implicit return, i.e. the last line of the block to be evaluated. In this case, the last line of the `map` method to be evaluated is the value of the local variable `a`, the string object `hello` &#x2026; this is the return value of the block for each iteration. This is possible because the inner scope of a block has access to variables initialized in an outer scope.
> 
> Since `map` cares about the return value of the block and adds that value to a new array, it adds the value of the string object `a` to the new array for each iteration, ultimately returning an array with three string objects, each with a value of `hello`.

**Select**

> The `select` method returns a new array based on the truthiness of the block's return value. If the return value evaluates to `true`, then the element is selected and added to the new array. Otherwise, the element is skipped and not added to the new array.

**Each**

> The `each` method returns the caller. The `each` method evaluates the given block for each element in the caller.

I also followed this advice [from Melissa](https://melissamanousos.com/ls-rb109-assessment-tips/):

> It’s easy to be too verbose during this exam for fear of leaving something out. After attending the study group a few times, I adopted Srdjan’s suggestion. Start by covering what the output and return values of the code will be, then do a first-pass explanation of why. Don’t spend time outlining what happens on every line of the provided code. Instead simply identify the concept being demonstrated (reassignment, variable shadowing, etc.). You can always come back to flesh out your explanation if you have time.

I think this saved me a lot of time since prior to reading this (and hearing this from Srdjan), I would've started by outlining what happens on every line and that would've taken me much longer to do for each of the 21 questions I had. In fact, after doing the above, I realized that sometimes I didn't need to flesh out my answer any further since explaining why something was happening answered the question(s) completely.

I wouldn't say one thing or another helped me the most. I think it was all of this combined. So, for my future assessments, I know I'll need to do something similar.


# My Assessment of the Assessment

The questions were very easy. I often knew the answer within seconds of looking at the question. There was one question that seemed a little tricky if one weren't paying attention, involving the return value of the `each` method. All of the others were straightforward and easy to see the correct answer.

This could be due to me fully understanding the material. I think it is partly. However, I also think it's due to the assessment questions being relatively simple and easy. The focus of the assessment is more on testing one's ability to describe what's happening than on the complexity of the problems.

This is the time-consuming part. Where do I start? What precision do I write to? What are the parts of the question I need to answer? How much time should I spend on one question when I have so many more to do?!?

I definitely felt the stress. I imagined myself spending so much time on one question getting the precision just right only to find out that I didn't have enough time to answer all of the other questions. Then, I imagined not getting the precision correct for one or more of the questions, finishing all of the questions, and then ending up with errors in multiple questions because I didn't have time to review.

Ultimately, I pushed these thoughts out of my head, kept track of the time as I answered the questions, and did the best I could.


# In Hindsight

I received my grades relatively quickly after the assessment, so I didn't have to wait too long.

There are two grades, one for the technical aspects and the other for the non-technical aspects, basically using markdown correctly and breaking answers into easier-to-read paragraphs.

That said, as soon as I submitted my answers and finished the assessment, I felt like I'd fully understood the questions and concepts I was tested on. To me, that felt better than receiving my grades.

I'm enrolled in Launch School because I want to learn the basics of programming so that I have a strong foundation to build on. So, the fact that I felt good about my assessment submission was an indication that I'm on the right track regardless of the specific grades I received.


# What's next?

I'm now focusing on more practice for the live coding interview assessment. This means solving as many code challenges as I can. I'll share details about this in another blog post after I take the live coding interview assessment. I haven't yet figured out my path to success with live coding yet, but I'll share it here when I do.

Learning how to solve problems is one of the reasons I enrolled in Launch School, so I'm truly looking forward to this concentrated practice.
