---
layout: post
title: OOP Design Decisions for Tic Tac Toe Computer Move Logic
excerpt_separator: <!--more-->
tags: launch_school ruby oop project tic_tac_toe rb120
---

I started working on the OOP Tic Tac Toe bonus features project this past week. Most of it is straightforward, until it comes to the computer player move logic. I tried implementing the computer move logic in three different ways. Here are some notes on each of the different ways.

<!--more-->

My first idea was to put it all in a separate module. The second idea was to put it in the `Board` class alongside other logic that checks the status of various squares on the board. The third idea was to put the logic in a `Computer` subclass of the `Player` class.


# Separate AI Module

This was my initial idea. I placed the computer move code from the [procedural Tic Tac Toe program](https://gitlab.com/jd-launch-school/launch-school/-/blob/master/rb101_programming_foundations/lesson_6/tictactoe_bonus_features.rb) I made for RB101 Lesson 6 into a [module called `AI`](https://gitlab.com/jd-launch-school/launch-school/-/blob/9e3d2040e9b5a54334f7343a0488212e3a4f1acf/rb129_oop/ttt/ttt.rb#L22-57) and then included that module [within the `TTTGame` class](https://gitlab.com/jd-launch-school/launch-school/-/blob/9e3d2040e9b5a54334f7343a0488212e3a4f1acf/rb129_oop/ttt/ttt.rb#L181). Including it in the `TTTGame` class in this way makes all of the methods within the `AI` module available within the `TTTGame` class.

I had to tweak the code that initially calls the module, [the `computer_moves` method](https://gitlab.com/jd-launch-school/launch-school/-/blob/9e3d2040e9b5a54334f7343a0488212e3a4f1acf/rb129_oop/ttt/ttt.rb#L271-273) and add [a `computer_next_move` method](https://gitlab.com/jd-launch-school/launch-school/-/blob/9e3d2040e9b5a54334f7343a0488212e3a4f1acf/rb129_oop/ttt/ttt.rb#L263-269). As can been seen in the code below, I had to add some arguments to the method calls so the module would have access to the board, winning lines, and the markers. Ultimately, this felt very procedural and having this code as a separate module didn't feel right since only one class will ever need to use it.

```ruby
def computer_next_move
  open_square(board, Square::INITIAL_MARKER, 5) ||
    best_move(board, Board::WINNING_LINES, Square::INITIAL_MARKER, COMPUTER_MARKER) ||
    best_move(board, Board::WINNING_LINES, Square::INITIAL_MARKER, HUMAN_MARKER) ||
    open_square(board, Square::INITIAL_MARKER, 1) ||
    board.empty_squares.sample
end
```

This shows that I'm repeating a bunch of arguments. It works, in that the computer player now chooses to block and take advantage of potential win conditions, but is it object-oriented?

Also, I should note that I didn't spend any time this go around on trying to figure out the minmax algorithm, like I did with my first Tic Tac Toe program. Here's where I wrote about my previous attempts: ["Unbeatable Tic Tac Toe AI"](https://jd-launch-school.gitlab.io/launch-school/2019/11/09/Unbeatable-Tic-Tac-Toe.html).


# Board Class

If the code doesn't look right in its own module, where should I put it? Which class would have an AI-like behavior? In looking at my current classes to find a better place for this code, I noticed the `three_identical_markers?` method in the `Board` class.

It doesn't make a ton of sense to have the computer move logic within the `Board` class since the board doesn't really need to know about the logic to use the board. However, I wouldn't need to pass the board in as an argument and the `WINNING_LINES` constant would be available. Also, if this is successful, I might not need to create subclasses for the `Player` class.

So, I tried it just to get a sense of how it would look and feel.

I first moved the [`computer_next_move`](https://gitlab.com/jd-launch-school/launch-school/-/blob/85353e63c6a46305892328755e633f84ae360939/rb129_oop/ttt/ttt.rb#L87-93) method out of the `TTTGame` class and into the `Board` class. I then called this method from within the [`TTTGame#computer_moves`](https://gitlab.com/jd-launch-school/launch-school/-/blob/85353e63c6a46305892328755e633f84ae360939/rb129_oop/ttt/ttt.rb#L263-265) method:

```ruby
def computer_moves
  board[board.computer_next_move(human, computer)] = computer.marker
end
```

Then, I moved all of the methods out of the `AI` method and into [the `Board` class](https://gitlab.com/jd-launch-school/launch-school/-/blob/85353e63c6a46305892328755e633f84ae360939/rb129_oop/ttt/ttt.rb#L22-134).

Eventually, I tweaked the code so everything had the data it needed and all of the tests were working. The obvious downside to doing this is that the `Board` class now needed to know about both of the players, so I had to pass them in as arguments.

Even though this was working, something didn't feel right. Why does the `Board` class need this logic? A board is a board that displays itself, lets others place, remove, and move pieces or markers on or within it, and reports on the status of itself like whether it's full or whether a player's pieces or markers are in a winning position. Does it need to know a player's name? Or, what about the logic a player uses to determine its next move?


# Computer Class

From there, after more thinking and looking at the way others implemented this feature, I thought that maybe all of this logic related to the computer player's next move should go into a `Computer` subclass of the `Player` method.

Up until this point, I didn't have any subclasses of the `Player` class. For some reason, I was sort of hoping to keep the players as generic as possible. However, it's now clear that the players will have different behavior when determining their next move.

The human player will use their brain, eyes, and knowledge of the Tic Tac Toe game to determine their next move. The computer player relies on an algorithm.

I created a `Human` class that inherits from the `Player` superclass. I moved the `TTTGame#retrieve_choice` method into this class since this is specific to the human player. The `Human` class doesn't need any logic relating to the next move since the actual human player themselves will be supplying that logic:

```ruby
class Human < Player
  include Utilities

  attr_reader :choice

  def initialize(name, marker)
    super(name, marker)
  end

  def retrieve_choice(empty_squares)
    puts "Choose a square (#{joinor(empty_squares)}): "

    loop do
      @choice = gets.chomp.to_i
      break if empty_squares.include?(choice)

      puts "Sorry, that's not a valid choice."
    end
  end
end
```

Them, I created the `Computer` class that inherits from the `Player` superclass. I moved all of the logic that was once in the `AI` module into this class:

```ruby
class Computer < Player
  INITIAL_MARKER = Square::INITIAL_MARKER
  WINNING_LINES = Board::WINNING_LINES

  def initialize(marker)
    super("Computer", marker)
  end

  def next_move(board, human_marker)
    open_square(board, 5) ||
      best_move(board, marker) ||
      best_move(board, human_marker) ||
      open_square(board, 1) ||
      board.empty_squares.sample
  end

  def best_move(board, marker)
    potential_win = potential_winning_line(board, marker)

    return unless potential_win

    line_values = position_values(board, potential_win)
    potential_win[empty_square_index(line_values)]
  end

  def potential_winning_line(board, marker)
    WINNING_LINES.find do |line|
      two_markers_in_line?(position_values(board, line), marker)
    end
  end

  def open_square(board, square)
    square if board.empty_squares.include?(square)
  end

  def position_values(board, line)
    line.map { |pos| board[pos] }
  end

  def two_markers_in_line?(line, marker)
    line.include?(INITIAL_MARKER) && line.count(marker) == 2
  end

  def empty_square_index(line)
    line.find_index(INITIAL_MARKER)
  end
end
```

The `TTTGame#computer_moves` method looked like this:

```ruby
def computer_moves
  board[computer.next_move(board, human.marker)] = computer.marker
end
```

I needed to pass in both the board and human player's marker so that the computer player would have what it needs in order to analyze and make the best choice.

This feels much better to me as it makes a lot more sense that the computer player will be considering its next move and scanning the board for possible win conditions. In a sense, this is what the actual human player is doing when deciding their next move.


# The Solution?

While writing this, it occurred to me that all of the `AI` logic doesn't need to be in one class or even one module. I could split it into multiple classes or modules. I was getting caught up in the idea that this was `AI` stuff that belonged together, but when looking at it in more detail, I realized that some of the methods fit better within the `Board` class and others fit better in the `Computer` class.

I moved some of the `AI` code into the `Board` class and some of it into the `Computer` class.


## Board Class

```ruby
class Board
  # ... other code

  def potential_winning_line(marker)
    WINNING_LINES.find do |line|
      two_markers_in_line?(position_values(line), marker)
    end
  end

  def open_square(square)
    square if empty_squares.include?(square)
  end

  def position_values(line)
    line.map { |pos| @squares[pos].marker }
  end

  def empty_square_index(line)
    line.find_index(Square::INITIAL_MARKER)
  end

  # ... other code

  private

  def two_markers_in_line?(line, marker)
    line.include?(Square::INITIAL_MARKER) && line.count(marker) == 2
  end

  # ... other code
end
```


## Computer Class

```ruby
class Computer < Player
  def initialize(marker)
    super("Computer", marker)
  end

  def next_move(board, human_marker)
    board.open_square(5) ||
      best_move(board, marker) ||
      best_move(board, human_marker) ||
      board.open_square(1) ||
      board.empty_squares.sample
  end

  private

  def best_move(board, marker)
    potential_win = board.potential_winning_line(marker)

    return unless potential_win

    line_values = board.position_values(potential_win)
    potential_win[board.empty_square_index(line_values)]
  end
end
```

This seems to be the best solution I can come up with at the moment. The board can report whether there is a potential win, whether a given space is empty, and what the values of a given line are. Meanwhile, the computer player can use these methods to determine what move it should make next.
