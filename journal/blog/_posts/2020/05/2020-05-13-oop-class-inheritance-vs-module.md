---
layout: post
title: Class Inheritance Vs. Modules
excerpt_separator: <!--more-->
tags: launch_school ruby oop rb120
---

I worked on an exercise today that confused me a little in terms of what it was asking and why, Question 2 of the Practice Problems: Hard 1 section of Lesson 4.

<!--more-->


# The Problem

Ben and Alyssa are working on a vehicle management system. So far, they have created classes called `Auto` and `Motorcycle` to represent automobiles and motorcycles. After having noticed common information and calculations they were performing for each type of vehicle, they decided to break out the commonality into a separate class called `WheeledVehicle`. This is what their code looks like so far:

```ruby
class WheeledVehicle
  attr_accessor :speed, :heading

  def initialize(tire_array, km_traveled_per_liter, liters_of_fuel_capacity)
    @tires = tire_array
    @fuel_efficiency = km_traveled_per_liter
    @fuel_capacity = liters_of_fuel_capacity
  end

  def tire_pressure(tire_index)
    @tires[tire_index]
  end

  def inflate_tire(tire_index, pressure)
    @tires[tire_index] = pressure
  end

  def range
    @fuel_capacity * @fuel_efficiency
  end
end

class Auto < WheeledVehicle
  def initialize
    # 4 tires are various tire pressures
    super([30,30,32,32], 50, 25.0)
  end
end

class Motorcycle < WheeledVehicle
  def initialize
    # 2 tires are various tire pressures
    super([20,20], 80, 8.0)
  end
end
```

Now Alan has asked them to incorporate a new type of vehicle into their system - a `Catamaran` defined as follows:

```ruby
class Catamaran
  attr_reader :propeller_count, :hull_count
  attr_accessor :speed, :heading

  def initialize(num_propellers, num_hulls, km_traveled_per_liter, liters_of_fuel_capacity)
    # ... code omitted ...
  end
end
```

This new class does not fit well with the object hierarchy defined so far. Catamarans don't have tires. But we still want common code to track fuel efficiency and range. Modify the class definitions and move code into a Module, as necessary, to share code among the `Catamaran` and the wheeled vehicles.


# My Answer

So, I feel there are a few ways to interpret what I'm being asked to do in this problem. The key is this sentence:

> Modify the class definitions and move code into a Module, as necessary, to share code among the `Catamaran` and the wheeled vehicles.

When I first read this, I understood that "as necessary" instruction to suggest that it might not be necessary to move ANY code into a method. When I looked at the problem, it does in fact seem like this can be done through class inheritance by creating a broader `Vehicle` class that both `WheeledVehicle` and `Catamaran` inherit from. Something like this:

```ruby
class Vehicle
  attr_accessor :speed, :heading

  def range
    @fuel_capacity * @fuel_efficiency
  end
end

class WheeledVehicle < Vehicle
  def initialize(tire_array, km_traveled_per_liter, liters_of_fuel_capacity)
    @tires = tire_array
    @fuel_efficiency = km_traveled_per_liter
    @fuel_capacity = liters_of_fuel_capacity
  end

  def tire_pressure(tire_index)
    @tires[tire_index]
  end

  def inflate_tire(tire_index, pressure)
    @tires[tire_index] = pressure
  end
end

class Auto < WheeledVehicle
  def initialize
    # 4 tires are various tire pressures
    super([30,30,32,32], 50, 25.0)
  end
end

class Motorcycle < WheeledVehicle
  def initialize
    # 2 tires are various tire pressures
    super([20,20], 80, 8.0)
  end
end

class Catamaran < Vehicle
  attr_reader :propeller_count, :hull_count

  def initialize(num_propellers, num_hulls, km_traveled_per_liter, liters_of_fuel_capacity)
    @num_propellers = num_propellers
    @num_hulls = num_hulls
    @fuel_efficiency = km_traveled_per_liter
    @fuel_capacity = liters_of_fuel_capacity
  end
end

auto = Auto.new
moto = Motorcycle.new
cata = Catamaran.new(2, 2, 50, 25.0)

p auto.range
p moto.range
p cata.range
```

    1250.0
    640.0
    1250.0


# Recommended Solution

Apparently, the intention here was to test our knowledge of implementing modules, as evidenced by this teacher assistant's response to another student with a similar perception:

> The question is about using modules, not how to choose modules over classes or vice versa or even whether its better to choose one or the other. It's certainly possible to use a regular class hierarchy for the contrived application, but that's not the point of the problem - the point is to get practice using modules.

Hmm&#x2026; I see that it states "move code into a Module" in the problem description, however directly after that is the dreaded "as necessary." It's not clear from the problem description how this question is about modules until after I've seen the recommended solution:

```ruby
module Moveable
  attr_accessor :speed, :heading
  attr_writer :fuel_capacity, :fuel_efficiency

  def range
    @fuel_capacity * @fuel_efficiency
  end
end

class WheeledVehicle
  include Moveable

  def initialize(tire_array, km_traveled_per_liter, liters_of_fuel_capacity)
    @tires = tire_array
    self.fuel_efficiency = km_traveled_per_liter
    self.fuel_capacity = liters_of_fuel_capacity
  end

  def tire_pressure(tire_index)
    @tires[tire_index]
  end

  def inflate_tire(tire_index, pressure)
    @tires[tire_index] = pressure
  end
end

class Catamaran
  include Moveable

  attr_reader :propeller_count, :hull_count

  def initialize(num_propellers, num_hulls, km_traveled_per_liter, liters_of_fuel_capacity)
    self.fuel_efficiency = km_traveled_per_liter
    self.fuel_capacity = liters_of_fuel_capacity

    # ... other code to track catamaran-specific data omitted ...
  end
end
```


# Analysis

I'm not wanting to point out a problem with potentially ambiguous description text and leave it at that. I've learned through my experience at Launch School that there's likely a lot I don't know about right now that makes this seem like an obvious candidate for modules instead of class inheritance. I'm likely not seeing it at the moment. Maybe one day, I can come back to this problem with fresh eyes and more experience and I'll laugh at my cute naivete.

I imagine that this case may be similar to some cases one finds when working on other people's code. Maybe there's a reason in some other part of the code where it makes sense to use a module over class inheritance for this section. Maybe adding a new class messes something up. Maybe the module needs to be used in other unrelated classes. Maybe the project lead hates adding new classes but doesn't know about modules, so engineers sneak modules in where they can. There are probably many reasons I can't even imagine.

Another thing I'd like to remember is that when the problem description mentions a design suggestion or suggested data structure, I should follow it. It's unlikely that the description will mention something I won't need.
