---
layout: post
title: Tic Tac Toe Overview of Classes
excerpt_separator: <!--more-->
tags: launch_school ruby oop project tic_tac_toe rb120
---

I've been tweaking my Tic Tac Toe (TTT) program a lot over the past week. I've made many changes, mostly involving the location of various methods.

<!--more-->

The two questions I've been considering are:

-   Does this method fit in this class?
-   What should this class be able to do and what should it know about?


# Overview of Classes

Zoomed out to only see the method names, the program so far looks like this:

```text
Utilities Module
----------------
- clear_screen
- joinor

Display Module
--------------
- display_welcome_message
- display_play_again_message
- display_goodbye_message

Board Class
-----------
- empty_squares
- full?
- someone_won?
- winning_lines
- winning_marker
- potential_winning_lines
- reset
- draw
- random_square

Square Class
------------
- mark
- marked?
- unmarked?

Line Class
----------
- all_markers
- player_markers
- empty_square
- num_markers?
- identical_markers?
- potential_win?
- win?
- winning_marker

Player Class
------------
- increment_score
- reset_score

Human Class (Inherits from Player Class)
----------------------------------------
- next_move
- retrieve_name
- valid_name?
- retrieve_marker
- valid_marker?
- retrieve_choice

Computer Class (Inherits from Player Class)
-------------------------------------------
- next_move
- best_move
- retrieve_name

TTTGame Class (Orchestration Engine)
------------------------------------
- play
- display_board
- display_grand_winner
- display_result
- display_score
- clear_screen_and_display_board
- play_rounds
- current_player_moves
- human_turn?
- winner
- grand_winner?
- play_again?
- reset_scores
- reset
```


# Board Class

```text
Board Class
-----------
- empty_squares
- full?
- someone_won?
- winning_lines
- winning_marker
- potential_winning_lines
- reset
- draw
- random_square
```

The `Board` class contains a `WINNING_LINES` constant that can theoretically be changed as needed in order to accommodate more or less winning line combinations. It's only state consists of a `Hash` of the board positions and their values assigned to a `@squares` instance method.

The `Board` class can do the following:

-   return an `Array` of `Square` objects, representing the board's empty squares
-   determine if the board is full
-   determine if someone won
-   convert the `WINNING_LINES` constant into an array of `Line` objects
-   return the marker that won
-   return the lines that have winning potential
-   reset the board
-   draw the board
-   return a random square on the board


# Square Class

```text
Square Class
------------
- mark
- marked?
- unmarked?
```

The `Square` class contains an `INITIAL_MARKER` constant that's set to an empty space `' '`. It's only state consists of a `String` representing a marker that's in the square.

The `Square` class can do the following:

-   mark a square
-   determine if a square is marked
-   determine if a square is unmarked


# Line Class

```text
Line Class
----------
- all_markers
- player_markers
- empty_square
- num_markers?
- identical_markers?
- potential_win?
- win?
- winning_marker
```

The `Line` class state consists of an `Array` of `Square` objects representing one line on the board assigned to an `@values` instance method.

The `Line` class can do the following:

-   return an `Array` of the markers in the line
-   return an `Array` of only the player markers in the line
-   return a `Square` object representing an empty square in the line
-   determine if there is a given number of markers in a line
-   determine if all of the markers in the line are identical
-   determine if the markers in the line potentially lead to a winning line
-   determine if the markers in the line are a winning line
-   return the marker that's present in a winning line

I created this class because I noticed that I was referring to lines in the `Board` class a lot and it seemed like it might be helpful to have a separate `Line` class. So far, I think it has been helpful because instead of code like this:

```ruby
def winning_marker
  WINNING_LINES.each do |line|
    markers = markers_in_line(line)
    if num_markers_in_line?(3, markers) && identical_markers?(markers)
      return markers.first
    end
  end

  nil
end
```

I can have code like this:

```ruby
def winning_marker
  winning_lines.find(&:win?)&.winning_marker
end
```

There are other benefits to having a `Line` class. Currently, it's mostly used by the `Board` class.


# Player Class

```text
Player Class
------------
- increment_score
- reset_score
```

The `Player` class state consists of a `@name` and `@score`. The `@score` instance variable is initialized to the `Integer` object with value `0`. The `@name` instance variable is initialized to the return value of the `retrieve_name` method call.

The `Player` class can do the following:

-   increment the player's score by `1`
-   reset the player's score back to `0`

Since those are the only two score behaviors in the game, I chose to keep the score as a state of the `Player` class as opposed to creating a separate `Score` class.


# Human Class

```text
Human Class (Inherits from Player Class)
----------------------------------------
- next_move
- retrieve_name
- valid_name?
- retrieve_marker
- valid_marker?
- retrieve_choice
```

The `Human` class state consists of the inherited state from the `Player` class along with a `@marker`. The `@marker` instance variable is initialized to the return value of the `retrieve_marker` method call.

The `Human` class can do the following:

-   make the Human player's next move
-   retrieve the Human player's name
-   validate the input entered to be the Human player's name
-   retrieve the Human player's marker
-   validate the input entered to be the Human player's marker
-   retrieve the Human player's choice for the next move


# Computer Class

```text
Computer Class (Inherits from Player Class)
-------------------------------------------
- next_move
- best_move
- retrieve_name
```

The `Computer` class state consists of the inherited state from the `Player` class along with a `@marker`. The `@marker` instance variable is initialized to the `String` object with value `O`.

The `Computer` class can do the following:

-   determine and make the computer player's next move
-   determine and make the computer player's best move
-   retrieve the computer player's name


# TTTGame Class

```text
TTTGame Class (Orchestration Engine)
------------------------------------
- play
- display_board
- display_grand_winner
- display_result
- display_score
- clear_screen_and_display_board
- play_rounds
- current_player_moves
- human_turn?
- winner
- grand_winner?
- play_again?
- reset_scores
- reset
```

This is the main class of the game that orchestrates and brings all of the other classes together in order to make the game playable.

The `TTTGame` class contains a `MAX_WINS` constant that can be changed as needed to determine the maximum number of wins a player needs to achieve before becoming the grand winner.

The `TTTGame` state consists of `@board`, `@human`, `@computer`, and `@current_marker`. The `@board`, `@human`, and `@computer` instance variables hold newly instantiated `Board`, `Human`, and `Computer` objects. The `@current_marker` instance variable is initialized to the Human player's marker.

The call to the `TTTGame#play` method at the end of the `TTTGame#initialize` method starts the gameplay portion of the game.

The `TTTGame` class can do the following:

-   play the game
-   display the board
-   display the grand winner
-   display the result of a game
-   display the current score
-   clear the screen and display the board
-   play the rounds that make up a game
-   tell each player when it is their turn to move
-   determine if it's currently the Human player's turn
-   determine the winner, if there is one
-   determine if there is a grand winner
-   ask the player whether they want to play again
-   reset the player scores
-   reset the game back to its initial state


# Summary

For the most part, each class has state and behaviors that make sense for objects of that type. For instance, when playing tic tac toe, a player looks over the current state of the board in order to determine their next move. This is modeled in the code by passing in a `Board` object to a `Computer` object's `next_move` instance method.

The code has evolved a lot since I started. I didn't know how helpful a `Line` class would be, for example, until I started refactoring. I expect that the code may change more over time as I continue to learn more about Object Oriented Programming and design patterns.

Also, I found [Ruby Reek](https://github.com/troessner/reek) very helpful in terms of pointing out when a method in one of my classes may have behavior that fits better in a different class instead.
