---
layout: post
title: "OOP: Equivalence and Variable Scope"
excerpt_separator: <!--more-->
tags: launch_school ruby oop rb120
---

For the past week or so, I've been studying the Launch School RB120 curriculum, Lesson 3: More OO Ruby. I'm focusing on fully understanding the concepts and code as well as practicing how I describe the code and how I write about the concepts.

<!--more-->

When creating our own custom classes, we may want to compare objects of these custom classes in various ways. Is this object's name equal to another object's name? Is this object's age greater than another object's age? We can override certain methods so that we can make these comparisons between objects in more succinct/efficient ways.


# Equivalence

In this section, we learn about some ways to determine if one object is equivalent or equal to another object. There are two general questions to think about when thinking about equivalence:

1.  Are the values within the two objects the same?
2.  Are the two objects the same?


## The == Method

This is the most important method in this section. It's originally defined in the `BasicObject` class that every other class inherits from. So, technically, every object in Ruby has access to an `==` method. Each class then overrides this method in order to perform the type of equivalence it needs and make it more meaningful for the class.

In some cases, like Strings and Arrays, the `==` method is implemented in such a way that it compares the **values** of the objects, like this:

```ruby
str1 = "word"
str2 = "word"
p str1 == str2

arr1 = [1, 2, 3]
arr2 = [1, 2, 3]
p arr1 == arr1
```

    true
    true

In other cases, like Symbols and Integers, the `==` method is implemented in such a way that it compares the **objects**, since Symbols and Integers are immutable:

```ruby
sym1 = :test
sym2 = :test
p sym1 == sym2

int1 = 6
int2 = 6
p int1 == int2
```

    true
    true

However, if we were to try this with a custom class and not override the `BasicObject#==` instance method, this would compare the **objects**, not their values:

```ruby
class Book
  attr_reader :name

  def initialize(name)
    @name = name
  end
end

book1 = Book.new("book")
book2 = Book.new("book")

p book1.object_id
p book2.object_id
p book1 == book2
```

    47360862310380
    47360862310340
    false

Overriding the `==` method would allow us to control what we want to compare. In this example, we want to compare the values assigned to the object's `@name` instance variables:

```ruby
class Book
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def ==(other)
    name == other.name
  end
end

book1 = Book.new("book")
book2 = Book.new("book")

p book1.object_id
p book2.object_id
p book1 == book2
```

    47297965421780
    47297965421740
    true


# Variable Scope

We looked at instance, class, and constant variable scope as it relates to classes and modules.


## Instance Variable Scope

Instance variables, variables starting with `@`, have **object level scope**. That is, they are available to an object's instance methods even if they're initialized in another instance method. For example, the `@type` instance variable below is initialized in the `first` method but accessible from the `second` method:

```ruby
class Desk
  def initialize
    first
  end

  def first
    @type = "desk_type"
  end

  def second
    @type
  end
end

my_desk = Desk.new
p my_desk.second
```

    "desk_type"


## Class Variable Scope

Class variables start with `@@` and have **class level scope**. Like instance variables, they are accessible from an object's instance methods. They are also accessible from the class itself via class methods.

```ruby
class Folder
  @@folders = 0

  def self.total_folders
    "From class method: #{@@folders}"
  end

  def initialize
    @@folders += 1
  end

  def total_folders
    "From instance method: #{@@folders}"
  end
end

p Folder.total_folders

red_folder = Folder.new
blue_folder = Folder.new

p Folder.total_folders
p red_folder.total_folders
p blue_folder.total_folders
```

    "From class method: 0"
    "From class method: 2"
    "From instance method: 2"
    "From instance method: 2"

Each `Folder` object uses the same `@@folder` class variable, so when that variable is reassigned, the change is reflected in both the `self.total_foldes` class method and the `total_folders` instance method.


## Constant Variable Scope

Constants begin with a capital letter (they are usually all uppercase) and they have **lexical scope**, that is, their availability depends on where they are used within the code. For the most part, constants behave as expected within classes as they are available to class and instance methods and can be called from the class.

```ruby
class Animals
  TYPES = ['cat', 'dog', 'bird', 'fish']

  attr_reader :message

  def self.types
    TYPES.join(', ')
  end

  def initialize
    @message = "You got a #{TYPES.sample}!"
  end
end

p Animals::TYPES
p Animals.types
animal = Animals.new
p animal.message
```

    ["cat", "dog", "bird", "fish"]
    "cat, dog, bird, fish"
    "You got a bird!"
