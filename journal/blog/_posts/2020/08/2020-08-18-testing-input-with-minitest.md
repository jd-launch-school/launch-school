---
layout: post
title: Testing Input with Minitest
excerpt_separator: <!--more-->
tags: launch_school ruby testing rb130
---

In learning about testing and trying to apply what I learned to the TwentyOne program I made for RB120, I encountered a problem of testing input that I hadn't learned yet. Here's a solution, maybe not the best, but it works.

<!--more-->

When I was in RB120 and working on the TwentyOne problem, I developed my own test framework. It worked for that problem but I was only really able to get it working for some of the more simple methods.

One example is the `player_choice` method in the `Utilities` module. When I finished, it looked like this:

```ruby
def player_choice(choices)
  loop do
    player_choice = gets.chomp.downcase.to_sym
    return player_choice[0].to_sym if choices.include?(player_choice)

    prompt "\nNot a valid choice.\nPlease choose" \
             " [ #{retrieve_options(choices)} ]."
  end
end
```

Using my ad-hoc framework, I had to rewrite the method for the test so it would not use `gets` since I didn't know how to test user-input. My test looked like this:

```ruby
def player_choice_tests(tests)
  tests.map.with_index do |test, index|
    "Test #{'%2d' % (index + 1)} ((#{player_choice(test[0], test[1]) == test[2] ? "PASS" : "FAIL"})) player_choice(#{test[0].inspect}, #{test[1].inspect}) == #{test[2].inspect}"
  end
end

def player_choice(args, player_choice)
  player_choice = player_choice.downcase.to_sym
  player_choice[0].to_sym if args.include?(player_choice)
end

puts player_choice_tests([
  [%i(yes y no n), 'y', :y],
  [%i(yes y no n), 'yes', :y],
  [%i(yes y no n), 'yeserie', nil],
  [%i(yes y no n), 'Y', :y],
  [%i(yes y no n), 'YES', :y],
  [%i(yes y no n), '', nil],
  [%i(yes y no n), ' ', nil],
  [%i(yes y no n), 'y3s', nil],
  [%i(yes y no n), 'y e s', nil],
  [%i(yes y no n), '1', nil],
  [%i(quit q continue cont c reset r), 'q', :q],
  [%i(quit q continue cont c reset r), 'quit', :q],
  [%i(quit q continue cont c reset r), 'cont', :c],
  [%i(quit q continue cont c reset r), 'quitter', nil],
  [%i(quit q continue cont c reset r), ' cont', nil],
  [%i(quit q continue cont c reset r), '', nil],
  [%i(quit q continue cont c reset r), ' ', nil],
  [%i(cash_out c play_again p), 'c', :c],
  [%i(cash_out c play_again p), ' ', nil]
])
```

    Test  1 ((PASS)) player_choice([:yes, :y, :no, :n], "y") == :y
    Test  2 ((PASS)) player_choice([:yes, :y, :no, :n], "yes") == :y
    Test  3 ((PASS)) player_choice([:yes, :y, :no, :n], "yeserie") == nil
    Test  4 ((PASS)) player_choice([:yes, :y, :no, :n], "Y") == :y
    Test  5 ((PASS)) player_choice([:yes, :y, :no, :n], "YES") == :y
    Test  6 ((PASS)) player_choice([:yes, :y, :no, :n], "") == nil
    Test  7 ((PASS)) player_choice([:yes, :y, :no, :n], " ") == nil
    Test  8 ((PASS)) player_choice([:yes, :y, :no, :n], "y3s") == nil
    Test  9 ((PASS)) player_choice([:yes, :y, :no, :n], "y e s") == nil
    Test 10 ((PASS)) player_choice([:yes, :y, :no, :n], "1") == nil
    Test 11 ((PASS)) player_choice([:quit, :q, :continue, :cont, :c, :reset, :r], "q") == :q
    Test 12 ((PASS)) player_choice([:quit, :q, :continue, :cont, :c, :reset, :r], "quit") == :q
    Test 13 ((PASS)) player_choice([:quit, :q, :continue, :cont, :c, :reset, :r], "cont") == :c
    Test 14 ((PASS)) player_choice([:quit, :q, :continue, :cont, :c, :reset, :r], "quitter") == nil
    Test 15 ((PASS)) player_choice([:quit, :q, :continue, :cont, :c, :reset, :r], " cont") == nil
    Test 16 ((PASS)) player_choice([:quit, :q, :continue, :cont, :c, :reset, :r], "") == nil
    Test 17 ((PASS)) player_choice([:quit, :q, :continue, :cont, :c, :reset, :r], " ") == nil
    Test 18 ((PASS)) player_choice([:cash_out, :c, :play_again, :p], "c") == :c
    Test 19 ((PASS)) player_choice([:cash_out, :c, :play_again, :p], " ") == nil

I wanted to attempt to test this method using Minitest. After some messing around, I realized that I needed two things:

1.  some way to simulate user-input
2.  separate the code with `gets` from the error message

In terms of separating the code, I did this:

```ruby
def get_player_choice(choices)
  player_choice = gets.chomp.downcase.to_sym
  return player_choice[0].to_sym if choices.include?(player_choice)
end

def player_choice(choices)
  loop do
    player_choice = get_player_choice(choices)
    return player_choice unless player_choice.nil?

    prompt "\nNot a valid choice.\nPlease choose" \
             " [ #{retrieve_options(choices)} ]."
  end
end
```

I'm not sure if this is the best way to handle it, but it does remind me of the idea that a method should only do one thing, either return something or have a side-effect, not both.

Now that the code is separated, I only needed a way to simulate user-input. I started by modifying the code again to take an optional argument that would determine whether or not the user-input would be set by the calling code as an argument or from the user.

I didn't like that, so I looked on the Internet for some other ideas. I found [this article on understanding and testing IO in Ruby](<https://tommaso.pavese.me/2016/05/08/understanding-and-testing-io-in-ruby/>). Based on the code in that article, I was able to come up with the following `simulate_stdin` method:

```ruby
def simulate_stdin(input)
  io = StringIO.new
  io.puts(input)
  io.rewind

  actual_stdin = $stdin
  $stdin = io

  yield
ensure
  $stdin = actual_stdin
end
```

We define a `simulate_stdin` method with `input` as a parameter. Within the method, we create a new `StringIO` object that is used to simulate `stdin`, in this case. We call the `IO#puts` method on the `io` object and pass in the local variable `input`. Then, we call the `StringIO#rewind` method on the `io` object which sets the input stream back to the beginning.

We then initialize the `actual_stdin` local variable in order to save the value of the `$stdin`, before we reassign `$stdin` to the `io` object.

We yield to the block which gives whatever code is in the block access to the `io` object via `$stdin`. In our case, we call our `get_player_choice` method which expects some type of input from the user.

Once the `get_player_choice` method returns with a return value based on our `io` object, execution moves back to the `simulate_stdin` method and executes the `ensure` section, which sets `$stdin` to the value of the `actual_stdin` local variable for use as needed later.

With this new method and the code separated as needed, I'm now able to test user-input using Minitest. Here's my test suite:

```ruby
class PlayerChoiceTest < MiniTest::Test
  include Utilities
  include Display

  def test_with_single_lower_case_valid_letter
    result = simulate_stdin('y') { get_player_choice(%i(yes y no n)) }
    assert_equal(:y, result)
  end

  def test_with_lower_case_valid_word
    result = simulate_stdin('yes') { get_player_choice(%i(yes y no n)) }
    assert_equal(:y, result)
  end

  def test_with_single_upper_case_valid_letter
    result = simulate_stdin('Y') { get_player_choice(%i(yes y no n)) }
    assert_equal(:y, result)
  end

  # ... other test cases
end
```

I think I'll likely use this in the future until I learn a better way to capture and test user-input.
