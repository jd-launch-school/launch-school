---
layout: post
title: More on Testing Input
excerpt_separator: <!--more-->
tags: launch_school ruby testing rb130
---

I learned more about standard input in Ruby and use this to streamline the `simulate_stdin` method and standard input testing I created last week.

<!--more-->

The topic of standard input, `$stdin`, `STDIN`, and `StringIO` came up in the Launch School curriculum recently. This gave me the opportunity to think about testing standard input more.

Previously, I'd been using the following `simulate_stdin` method:

```ruby
def simulate_stdin(input)
  io = StringIO.new
  io.puts(input)
  io.rewind

  actual_stdin = $stdin
  $stdin = io

  yield
ensure
  $stdin = actual_stdin
end
```

There's nothing wrong with this method, as-is. Ultimately, it works. What I didn't understand fully was what the value of `input` should be in certain situations.

Previously, I changed my code in order to accommodate this lack of understanding. I discuss this in detail in my post, [Testing Input with Minitest](../../08/18/testing-input-with-minitest.html). I now understand that I don't need to modify the code I'm testing and can now test the following code as-is:

```ruby
def player_choice(choices)
  loop do
    player_choice = gets.chomp.downcase.to_sym
    return player_choice[0].to_sym if choices.include?(player_choice)

    prompt "\nNot a valid choice.\nPlease choose" \
             " [ #{retrieve_options(choices)} ]."
  end
end
```

First, I modified the `simulate_stdin` method so it's more succinct and straightforward:

```ruby
def simulate_stdin(*input)
  io = StringIO.new
  io.puts(input)
  io.rewind

  $stdin = io
  yield
ensure
  $stdin = STDIN
end
```

The two main differences here are not needing to initialize an `actual_stdin` local variable to hold the value of the current `$stdin` and accepting a variable number of arguments using the splat operator. Apparently, `$stdin` refers to the `STDIN` constant, so if we reassign `$stdin`, we can reassign it back to the value of `STDIN` later as needed.

Looking more closely at the [IO#puts method](https://docs.ruby-lang.org/en/2.7.0/IO.html#method-i-puts), I see that it takes multiple arguments and can even take an array of arguments. It will write a newline after any arguments that don't already have a newline. This can help simulate multiple keyboard inputs.

So, for instance, given the following test, the initial input results in a message to the user that the input is not valid and then loops to ask the user for another input:

```ruby
def test_with_space_underscore_choices
  result = simulate_stdin(' ') { player_choice(%i(cash_out c play_again p)) }
  assert_nil(result)
end
```

If, like in this case, only one argument is passed to `simulate_stdin` (an empty space, `' '`), the test will raise a `NoMethodError` exception since there is no second `$stdin` value and so it passes in `nil` as the second input:

```shell
PlayerChoiceTest#test_with_space_underscore_choices:
NoMethodError: undefined method `chomp' for nil:NilClass
```

To fix this, we need to pass in a second input value to `simulate_stdin` so that the test has a non-nil input value to work with:

```ruby
def test_with_space_underscore_choices
  result = simulate_stdin(' ', 'c') { player_choice(%i(cash_out c play_again p)) }
  assert_nil(result)
end
```

At this point, the test works, but it will fail because the output is no longer `nil` and will instead, be `:c` in this case. Changing the assertion to the following will make the test pass:

```ruby
def test_with_space_underscore_choices
  result = simulate_stdin(' ', 'c') { player_choice(%i(cash_out c play_again p)) }
  assert_equal :c, result
end
```

That works great! However, now we see some output that clutters the test results:

```shell
Please choose [ (q)uit | (c)ontinue | (c)ont | (r)eset ].
>> .....
Not a valid choice.
Please choose [ (y)es | (n)o ].
>> .
Not a valid choice.
Please choose [ (q)uit | (c)ontinue | (c)ont | (r)eset ].
>> .
Not a valid choice.
Please choose [ (y)es | (n)o ].
>> ..
```

There are a few ways to solve this, as far as I can tell. I'm not sure which is best or what the trade-offs are for using one way or the other. I settled on using `capture_io` which captures both `$stdout` and `$stderr` into strings. I capture them, but only use `$stdout`. I also have to change my assertion from `assert_equal` to `assert_match` in order to match the output message. The example test now looks like this:

```ruby
def test_with_space_underscore_choices
  result = capture_io do
    simulate_stdin(' ', 'c') { player_choice(%i(cash_out c play_again p)) }
  end

  assert_match /.*Not a valid choice.*/, result.first
end
```

Ultimately, I feel this is pretty close to what I want to test. I want to enter an invalid input and test whether or not there is output to `stdout` that is a specific message to the user.

Now I'm able to test the original working code for both valid and invalid inputs in a way that's relatively clear and understandable.

The final change I made was to move the repeated logic for the valid and invalid tests into their own methods. So, instead of the following:

```ruby
def test_with_lower_case_valid_word
  result = simulate_stdin('yes') { player_choice(%i(yes y no n)) }
  assert_equal :y, result
end

def test_with_single_upper_case_valid_letter
  result = simulate_stdin('Y') { player_choice(%i(yes y no n)) }
  assert_equal :y, result
end
```

I will have:

```ruby
def valid_choice_test(*args, expected, choices)
  result = simulate_stdin(args) { player_choice(choices) }
  assert_equal expected, result
end

def test_with_lower_case_valid_word
  valid_choice_test 'yes', :y, %i(yes y no n)
end

def test_with_single_upper_case_valid_letter
  valid_choice_test 'Y', :y, %i(yes y no n)
end
```

There's likely a better way to do this, so hopefully I'll come across that way as I continue through Launch School.

Running my tests now shows:

```shell
Run options: --seed 51369

# Running:

..................................................................

Finished in 0.003614s, 18260.2821 runs/s, 32647.1710 assertions/s.

66 runs, 118 assertions, 0 failures, 0 errors, 0 skips
```
