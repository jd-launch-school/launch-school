---
layout: post
title: Thoughts on the RB129 Interview Assessment
excerpt_separator: <!--more-->
tags: launch_school ruby oop rb120 assessments
---

After a year in RB101 and RB120, I'm now ready to move on to RB130 where I can finally learn the basics of testing in Ruby.

<!--more-->

Before I started Launch School, I was learning on my own. I thought I knew enough to get a job, so I applied to a few. I received two take home projects and each project had a focus on the overall process and testing. It seemed overwhelming even though I thought I had a good understanding of what I'd learned up to this point.

Fast forward to Launch School and I realized that I needed to pass two written assessments, two interview assessments, complete six projects, and practice many hundreds of coding problems before I could even begin to think about testing. So, for the past year, I've been working hard to get myself to a point where learning about testing makes sense in the context of Launch School's curriculum.

I'm finally there. I passed the RB129 Interview Assessment this past Saturday and now I can move on to RB130, which introduces Ruby's Minitest. It's been a long time coming and a lot of work.


# RB129 Interview Assessment Experience

As far as the RB129 interview assessment itself, it was easier than I thought it would be. That said, it still required that I concentrate strongly on the code presented and the questions asked. There were a few moments where if I wasn't paying attention, I might not have provided the correct understanding. Also, one of the questions almost stumped me until I realized what was happening.

All in all, I appreciate having gone through the process and am grateful for having the interview practice.


# How I Prepared

I started over with a new list and gathered all of the information about each topic into one place. This took a few days to complete. I also practiced the Launch School **Debugging** exercises as these are sometimes tricky and helped me to examine the code closer, paying attention to smaller details.

I found Megan Turley's Medium post, [Preparing for the 129 Interview Assessment](https://medium.com/launch-school/preparing-for-the-129-interview-assessment-4e6c208ce2e4), to be really helpful, especially the part about talking through the problems. This helped me develop a similar pattern of analyzing the problems that helped me catch incorrect code sooner in the process than I would have found them using my "old process," which was basically winging it.

I recorded myself solving the **Debugging** and **Easy 2** exercises. After I completed an exercise, I'd listen to the recording and pay attention to the language I used and make note of information I said incorrectly or forgot to mention.


# The RB120 Projects

I took my time through RB120 in order to make sure the OOP concepts presented were familiar and almost second nature to me.

To get to this point, I spent a lot of time on the projects, reasoning through different ways of implementing the functionality required.

-   I focused on learning how and why the different modules, classes, and objects interacted with one another.

-   I compared both the procedural version and the OOP version of the projects to understand how OOP was improving the code, if it was.

-   I looked through other student's projects to learn how they achieved something and how it differed from the way I chose to do it.

-   I read the TA's comments to learn about things that may not have been presented in the curriculum, but may be helpful in understanding some aspect of the project.

This took extra time, but I feel it was really helpful. Unlike the projects in RB101, I feel the projects in RB120 are directly connected to understanding OOP and therefore, directly connected to performing well on the assessments.
