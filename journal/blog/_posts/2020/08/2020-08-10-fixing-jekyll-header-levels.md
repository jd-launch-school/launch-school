---
layout: post
title: Fixing Jekyll Header Levels
excerpt_separator: <!--more-->
tags: jekyll yaml markdown
---

It's bothered me for a while that when Jekyll renders the markdown files I provide, it makes all of the header levels `h1` instead of starting at `h2`. Here's how I fixed that.

<!--more-->

Initially, and I guess by default with my particular setup, the only way that I was able to get `h2` header levels was to create a container header that would be set as `h1`. This wasn't too bad, but it still meant that I had multiple `h1` header levels on my site, one for the container header and one for the title of the blog post, which isn't the best practice.

At first, I thought the problem was with the way I create the markdown files Jekyll uses for posts. In looking at it closer, however, I learned that the markdown files were coming out just fine and that it was something with Jekyll itself.

Jekyll uses [kramdown](https://jekyllrb.com/docs/configuration/markdown/) to convert from markdown to HTML. To change the values for these options, it's possible to add a `kramdown` section in the Jekyll `_config.yml` file, like this:

```yaml
kramdown:
  html_to_native: true
```

In looking at the various options available to modify how kramdown does this conversion, I noticed one called [header<sub>offset</sub>](https://kramdown.gettalong.org/options.html#option-header-offset).

From that documentation:

> Sets the output offset for headers
> 
> If this option is c (may also be negative) then a header with level n will be output as a header with level c+n. If c+n is lower than 1, level 1 will be used. If c+n is greater than 6, level 6 will be used.
> 
> Default: 0

So, I changed the default for that option to `1` with the following code:

```yaml
kramdown:
  header_offset: 1
```

Now, my section headers are `h2` header level and the only `h1` on the page is the title of the blog post.

<hr>

You can read more about my Emacs Org-Mode Jekyll setup here:

-   [My Emacs Org-Mode to Jekyll GitLab Setup](../../../2019/11/03/emacs-org-mode-jekyll-setup.html)
-   [Org-Mode Conversion to Markdown - Add Syntax Highlighting](../../../2019/11/04/syntax-highlighting-org-to-jekyll.html)

<hr>
