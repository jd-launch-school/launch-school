---
layout: post
title: How To Take a Written Assessment for Launch School
excerpt_separator: <!--more-->
tags: launch_school ruby rb129 assessments oop
---

I recently finished the RB129 written assessment. Here are some of my thoughts in hindsight about the study process leading up to the assessment that I hope will help me on future assessments.

<!--more-->


# Gather all information about a specific topic listed on the study guide together into one place

So far, there's been a study guide provided for each written assessment. The study guide contains all of the topics that may be covered on the assessment. Usually, this is a long list of 15-20 or so topics and whenever I see it, I usually glance at it and think, "I know this, I know that, etc&#x2026;"

Instead of thinking that, the next time I see topics listed like this on the study guide, I want to just start gathering the information about each topic into a new document or section.

I use Emacs and org-mode, so for me it's usually a matter of creating a new top-level heading labeled "Study Guide" and then creating subheadings for each of the topics listed. Then, within those subheadings, I can have different headings depending on what I need.

For example:

![Example Org-Mode for Study Guide](/assets/img/org-mode-example-for-study-guide.png)

As you can see in this example, the description and code (with results) is all in one place so it's easy to find during the assessment. It's also possible to use Markdown so the description can more easily be used on the assessment as-is.


# Write an answer to the general question of "What is [topic]?" as if I was submitting that answer for the assessment

This process is by far the most time-consuming part of the process. Figuring out how to word an answer so that it's both comprehensive and concise can be challenging, especially for new topics.

I think it might be helpful to start with a bullet-point list of important points about the topic. Then, combine those points into a paragraph.

In some cases, adding the code examples into the Markdown can be helpful:

![Example Org-Mode with Markdown Code Snippet](/assets/img/org-mode-markdown-code-snippet.png)

Here you can see how the code example is in the Markdown so it can more easily be copy/pasted into the Launch School assessment as needed and it's possible to evaluate and modify the code if I need to make changes.


# Create examples that demonstrate each topic

The next step in the process is to create examples for each of the topics. By this time in the curriculum, I've likely written a lot of code already, so coming up with examples doesn't take too long. It's also possible to use some of the examples given in the curriculum, especially if they demonstrate a topic well.


# Share these with others and/or look on forums to make sure all aspects of the topic are included

This is an important step to make sure I'm being comprehensive and not stuck in my own thoughts. It's easy to think I know everything about a topic because at the time, I know what I know but don't know what I don't know. If I knew what I didn't know, then I'd already know it, so it wouldn't be something that I didn't know.

Sharing both the descriptions and code snippets with others, by literally sharing the text and/or speaking the content verbally, will help to ensure that I've covered all of a topic's important points. Sometimes, there are nuances about a topic that I miss or that I don't explicitly state clear enough.

An example of this from the RB129 written assessment is how classes and objects relate to state. My preferred way of explaining this is to say that:

> A class defines an object's state.

Even though this is true if one fully understands the relationship between classes and objects, the sentence by itself doesn't necessarily demonstrate that understanding. It's more clear to say:

> A class defines the instance methods that keep track of an object's state. Each object has a unique state and multiple objects instantiated from the same class will have different states.

This is something that another student or TA will point out in a study session and it generally helps me refine my understanding of the topic.
