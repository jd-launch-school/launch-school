---
layout: post
title: RB120 OOP Update
excerpt_separator: <!--more-->
tags: launch_school ruby rb120 oop
---

A brief update on my status at Launch School and some things I've been learning about Object Oriented Programming (OOP).

<!--more-->


# More Videos

First off, I've been continuing to add to the videos on my [PeerTube account](https://devtube.dev-wiki.de/video-channels/learn_programming/videos). Here are the videos I've added since February:

-   [Debugging HEY YOU!](https://devtube.dev-wiki.de/videos/watch/bfb23b4a-597f-46b1-abd7-3499e2193b99)
-   [Debugging Valid Series?](https://devtube.dev-wiki.de/videos/watch/37fc2f8e-3dc0-483d-82ba-395f3674af0b)
-   [Debugging Reverse](https://devtube.dev-wiki.de/videos/watch/2c4446f5-70c9-4425-a256-2acae7c70c93)
-   [Accessor Methods (Part 1)](https://devtube.dev-wiki.de/videos/watch/7289fd10-21f4-4e1b-8052-644bc3a57636)
-   [Accessor Methods (Part 2)](https://devtube.dev-wiki.de/videos/watch/eafc2517-8a7b-4a31-a8e9-57c03c4f2239)
-   [Accessor Methods (Part 3)](https://devtube.dev-wiki.de/videos/watch/84c62f41-47f9-48ba-a9c2-5c314a112776)
-   [Accessor Methods (Part 4)](https://devtube.dev-wiki.de/videos/watch/9cd18e93-46a3-4817-ab8f-80c76e5e5102)
-   [Solving Banner Class](https://devtube.dev-wiki.de/videos/watch/03b17315-d9d4-4841-83c0-d3858860f7ae)
-   [Solving What's the Output?](https://devtube.dev-wiki.de/videos/watch/9902bd51-b8a1-409b-a886-71a08eb687ac)
-   [Fix the Program - Books](https://devtube.dev-wiki.de/videos/watch/f4e776a8-b0aa-419f-a086-7ec6f3a0a7da)

You'll be able to find more as I upload them on my [Learn Programming Channel](https://devtube.dev-wiki.de/video-channels/learn_programming/videos).


# Status Update

As you may be able to notice from the videos, I've been plugging away at the Launch School RB120 OOP course little by little. With all that's been happening with COVID-19 and the pandemic, it's been difficult some days to find the time to get my mind into the code. However, slowly, I'm getting through it. I'm just now finishing up my first draft of the Rock, Paper, Scissors program for Lesson 2. Over the next week or so, I'll review other submissions and rewrite/refactor my code to fit more of the reviewers' expectations.


# Object Oriented Programming (OOP)

1.  [Class](#orge648966)
2.  [Object](#org4483293)
    1.  [Constructor](#org20f4773)
    2.  [Instance Variable](#org8b4f927)
    3.  [Instance Method](#org4c13a01)
3.  [Inheritance](#orgd844871)
    1.  [Superclass](#org901c30a)
    2.  [Subclass](#org45a7a99)
    3.  [Example of Superclass and Subclass](#org10bea84)
4.  [Encapsulation](#org467ab30)
5.  [Polymorphism](#org0b521b7)
6.  [Getter, Setter, and Accessor Methods](#org9804b45)
    1.  [Getter Methods](#orgf3d439d)
    2.  [Setter Methods](#org9f7973f)
    3.  [Accessor Methods](#org2994701)
7.  [Collaborator Objects](#orgdf6a3ae)
8.  [Modules](#orgda83b35)

Over the past couple of months, I've been learning about different aspects of object oriented programming, including ideas about classes, objects, inheritance, encapsulation, polymorphism, accessor methods, collaborator objects and modules. Here is a brief description of each of these concepts, for future reference:


<a id="orge648966"></a>

## Class

A class is generally a container for data. The two main things to focus on when creating classes are states and behaviors. Instance variables keep track of state while instance methods define the class behaviors.


<a id="org4483293"></a>

## Object

An object is an instance of a class that is instantiated using the `new` method from a class. For example:

```ruby
class MyClass
end

my_object = MyClass.new
```

In the above example, `my_object` is instantiated. It is an object/instance from the `MyClass` class.


<a id="org20f4773"></a>

### Constructor

When an object is instantiated from a class using the `new` method, the constructor is called. The constructor is an instance method named `initialize`, like this:

```ruby
class MyClass
  def initialize
    puts "A MyClass object was initialized."
  end
end

my_object = MyClass.new
```

    A MyClass object was initialized.


<a id="org8b4f927"></a>

### Instance Variable

This is how we keep track of the state of an object. Each object has its own unique state. Instance variables, variables starting with an `@` symbol, are the way we keep track of this state. For example:

```ruby
class MyClass
  def initialize
    @state = "This is the object's default state."
  end

  def state
    puts @state
  end
end

my_object = MyClass.new
my_object.state
```

    This is the object's default state.


<a id="org4c13a01"></a>

### Instance Method

Unlike the state of objects where each object has its own state, an object's behavior is shared with other objects of the same class. So, two objects of the same class share behaviors but have different states. For example:

```ruby
class MyClass
  def initialize(name)
    @name = name
  end

  def name
    @name
  end

  def greeting
    puts "Hello #{name}!"
  end
end

my_object1 = MyClass.new("Becky")
my_object2 = MyClass.new("Mandy")

my_object1.greeting
my_object2.greeting
```

    Hello Becky!
    Hello Mandy!


<a id="orgd844871"></a>

## Inheritance

Inheritance is where one class inherits states and/or behaviors from another class. One or more subclasses (child classes) may inherit from a superclass (parent class).


<a id="org901c30a"></a>

### Superclass

The class that another class inherits states and/or behaviors from is known as the superclass or parent class.


<a id="org45a7a99"></a>

### Subclass

The class that inherits states and/or behaviors from another class is known as the subclass or child class.


<a id="org10bea84"></a>

### Example of Superclass and Subclass

```ruby
class Parent
  def method
    puts "This is from the superclass."
  end
end

class Child < Parent
end

class Child2 < Parent
end

my_obj = Child.new
my_obj2 = Child.new
my_obj.method
my_obj2.method
```

    This is from the superclass.
    This is from the superclass.

In this example, `Child` and `Child2` are subclasses that inherit the `method` method (behavior) from the `Parent` superclass.


<a id="org467ab30"></a>

## Encapsulation

Encapsulation is hiding pieces of functionality so that methods and variables are inaccessible to the rest of the code. To do this in Ruby, one creates classes and only exposes certain methods so that when one instantiates an object from the class, only the exposed methods are available to be called on the object leaving the rest of the functionality inaccessible.

This can be done by not making getters, setters, and other instance methods publicly available by using the `private` method. For example:

```ruby
class MyClass
  def some_method
    puts "This method can be called on an object/instance."
  end

  private

  def private_method
    puts "This method cannot be called on an object/instance."
  end
end

my_obj = MyClass.new
my_obj.some_method
# my_obj.private_method
# => NoMethodError (private method `private_method' called for #<MyClass:0x000055a0898cae90>)
```

    This method can be called on an object/instance.


<a id="org0b521b7"></a>

## Polymorphism

This is a little tricky for me to describe at this time. Polymorphism comes from the root words **poly** and **morph** which means **many forms**. In general, this means that data can be represented as different types. The way I understand this at this point is that method overriding creates polymorphism because in a parent class or superclass, a method may do one thing while in the child class or subclass, that same method call does something else. Here's an example, I think:

```ruby
class MyClass
  def name
    puts "This is my name."
  end
end

class OtherClass < MyClass
  def name
    puts "This is NOT my name."
  end
end

my_obj = MyClass.new
other_obj = OtherClass.new

my_obj.name
other_obj.name
```

    This is my name.
    This is NOT my name.

So, even though the `name` method is called on each object and `OtherClass` inherits from `MyClass`, the output (behavior) of the `name` method for each class is different.


<a id="org9804b45"></a>

## Getter, Setter, and Accessor Methods

This is something that I struggled with prior to digging deeper in Launch School. It's something I've seen in Rails projects but hadn't fully understood. Now, it's much more clear what's happening so when I see these methods, I know more of what to expect.

Basically, accessor methods are Ruby's built-in way to create getter and setter methods.


<a id="orgf3d439d"></a>

### Getter Methods

Getter methods are instance methods that output the value of instance variables. Without a getter method, it's not possible to get the value of an instance variable from an object. For example:

```ruby
class MyClass
  def initialize
    @name = "Tony"
  end
end

class OtherClass
  def initialize
    @name = "Molly"
  end

  def name
    @name
  end
end

my_obj = MyClass.new
# puts my_obj.name
# => NoMethodError (undefined method `name' for #<MyClass:0x0000564faa23a370 @name="Tony">)

other_obj = OtherClass.new
puts other_obj.name
```

    Molly

In this case, the `name` instance method in `OtherClass` is the getter method.


<a id="org9f7973f"></a>

### Setter Methods

Setter methods are instance methods that enable objects to change the state or change the value of an instance variable. Without a setter method, one cannot change the value of an instance variable from an object. For example:

```ruby
class MyClass
  def initialize
    @name = "Tony"
  end

  def name
    @name
  end
end

class OtherClass
  def initialize
    @name = "Molly"
  end

  def name
    @name
  end

  def name=(new_name)
    @name = new_name
  end
end

my_obj = MyClass.new
puts "my_obj"
puts "------"
puts my_obj.name
# my_obj.name = "Lissy"
# => NoMethodError (undefined method `name' for #<MyClass:0x0000564faa23a370 @name="Tony">)
puts my_obj.name

puts
puts "other_obj"
puts "---------"

other_obj = OtherClass.new
puts other_obj.name
other_obj.name = "Callie"
puts other_obj.name
```

    my_obj
    ------
    Tony
    Tony
    
    other_obj
    ---------
    Molly
    Callie

In this case, the `name=` method in `OtherClass` is the setter method. `MyClass` does not have a setter method, so trying to change the name raises a `NoMethodError` exception.


<a id="org2994701"></a>

### Accessor Methods

This is Ruby's shorthand for creating built-in getter and setter methods. Ruby has three accessor methods:


#### `attr_reader`

This method only creates the getter methods. In some cases, we want to be able to access an instance variable but not change it. For example:

```ruby
class MyClass
  attr_reader :name

  def initialize
    @name = "Tony"
  end
end

my_obj = MyClass.new
puts my_obj.name
# my_obj.name = "Missy"
# => NoMethodError (undefined method `name' for #<MyClass:0x0000564faa23a370 @name="Tony">)
```

    Tony


#### `attr_writer`

This method only creates the setter methods. In some cases, we want to be able to change an instance variable but not access it. For example:

```ruby
class MyClass
  attr_writer :name

  def initialize
    @name = "Tony"
  end
end

my_obj = MyClass.new
p my_obj
# puts my_obj.name
# => NoMethodError (undefined method `name' for #<MyClass:0x0000564faa23a370 @name="Tony">)
my_obj.name = "Missy"
p my_obj
```

    #<MyClass:0x000055df6ed1a558 @name="Tony">
    #<MyClass:0x000055df6ed1a558 @name="Missy">


#### `attr_accessor`

This method creates both getter and setter methods for those times when we want to be able to access and change instance variables. For example:

```ruby
class MyClass
  attr_accessor :name

  def initialize
    @name = "Tony"
  end
end

my_obj = MyClass.new
puts my_obj.name
my_obj.name = "Missy"
puts my_obj.name
```

    Tony
    Missy


<a id="orgdf6a3ae"></a>

## Collaborator Objects

Objects that are stored as state within another object are known as collaborator objects. Up to this point, the String objects stored in the instance variables of the examples have been collaborator objects.

Objects that have relationships to other objects are collaborator objects. For instance, in the examples above, `MyClass` objects **have a** name which is a String object.

I still need to focus more on this to get a better understanding of its importance to object oriented design.


<a id="orgda83b35"></a>

## Modules

A module is a collection of behaviors that is usable in other classes via mixins. A module is said to be "mixed in" to a class using the `include` method invocation.

A module allows us to group reusable code into one place. We use modules in our classes by using the `include` method invocation, followed by the module name.

This is a way to have multiple inheritance in Ruby. Generally, class inheritance in Ruby means that a class can only inherit from one other superclass. With modules, the class can inherit behavior from any number of modules as needed. It's as if the methods in a module are copy/pasted into the class. For example:

```ruby
module MyModule
  def calculate
    puts "Performing some calculation."
  end

  # ... other similar methods as needed
end

module OtherModule
  def calculate_something_else
    puts "Performing another calculation."
  end

  # ... other similar methods as needed
end

class MyClass
  include MyModule
  include OtherModule
end

class OtherClass
  include MyModule
end

my_obj = MyClass.new
my_obj.calculate
my_obj.calculate_something_else

other_obj = OtherClass.new
other_obj.calculate
```

    Performing some calculation.
    Performing another calculation.
    Performing some calculation.
