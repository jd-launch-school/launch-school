---
layout: post
title: Refactor Case Statement To Use Polymorphism
excerpt_separator: <!--more-->
tags: launch_school ruby refactor code_smell oop project rb120
---

While reviewing my Rock, Paper, Scissors project, I noticed a very long method with a relatively long `case` statement. This hinted to me that I might be able to refactor in order to reduce the complexity of the method and make better use of polymorphism.

<!--more-->

1.  [The Starting Code](#org8451123)
2.  [Refactoring](#org61d940f)
    1.  [Initial Class Skeleton](#orgf7d7e3a)
    2.  [Adding State `@names`](#org4b904d2)
    3.  [Add Prompt for an Invalid Choice](#orgc4407b4)
    4.  [Exiting the Loop and Returning True/False](#orgf0ed375)
    5.  [Choice Actions](#org8a2d7b5)
        1.  [Show Rules](#org70ad437)
        2.  [Show History](#org7f7c26f)
        3.  [Choose New Opponent](#org7dd4108)
3.  [The Final Code](#orgb70be27)
    1.  [Utilities Module](#orgdc0686f)
    2.  [Choice Classes](#org882db3b)
        1.  [Choice Superclass](#org755ebf7)
        2.  [HistoryChoice Subclass](#org8de3f99)
        3.  [ContinueChoice Subclass](#orge0aab83)
        4.  [NewChoice Subclass](#orge018ad3)
        5.  [RulesChoice Subclass](#org5de495e)
        6.  [QuitChoice Subclass](#org6cd0273)
    3.  [RPSGame Class](#org474a5d6)
4.  [Summary](#org8982b0a)


<a id="org8451123"></a>

# The Starting Code

Here's what the code I was working on looked like when I started ([source](https://gitlab.com/jd-launch-school/launch-school/-/blob/568ebefc984710795843e83c19db5b4bf82ea730/rb129_oop/rps.rb)):

```ruby
def validate_choice(choice)
  ['c', 'cont', 'continue',
   'n', 'new',
   'h', 'hist', 'history',
   'r', 'rules',
   'q', 'quit'].include?(choice)
end

def play_again?
  loop do
    puts
    prompt "What would you like to do?"
    prompt "Options: (c)ontinue | (n)ew opponent | (h)istory | (r)ules | (q)uit"
    answer = gets.chomp.downcase

    if validate_choice(answer)
      case answer[0]
      when 'h'
        clear_screen
        next display_history
      when 'c'
        return true
      when 'n'
        self.computer = choose_computer_player
        human.score.reset
        @history << "Changed to new opponent (#{computer.upcase})."
        return true
      when 'r'
        clear_screen
        display_rules
      else
        return false
      end
    else
      prompt "That's not a valid choice."
      prompt "Please choose 'c', 'n', 'h', 'r', or 'q'.\n"
    end
  end
end

```

Looking at this code in isolation helped me see that the `validate_choice` method and the `if` statement within the `play_again?` method were somewhat redundant since I could just put the choices directly into the case statement. So, the code now looks like this:

```ruby
def play_again?
  loop do
    puts
    prompt "What would you like to do?"
    prompt "Options: (c)ontinue | (n)ew opponent | (h)istory | (r)ules | (q)uit"
    answer = gets.chomp.downcase

    case answer
    when 'h', 'hist', 'history' then display_history
    when 'c', 'cont', 'continue' then return true
    when 'n', 'new'
      @computer = choose_computer_player
      human.score.reset
      @history << "Changed to new opponent (#{computer.upcase})."
      return true
    when 'r', 'rules'
      clear_screen
      display_rules
    when 'q', 'quit' then return false
    else
      prompt "That's not a valid choice."
      prompt "Please choose 'c', 'n', 'h', 'r', or 'q'.\n"
    end
  end
end
```

That works. I also recognized that I could move the output for the prompt into its own method:

```ruby
def play_again_prompt
  puts
  prompt "What would you like to do?"
  prompt "Options: (c)ontinue | (n)ew opponent | (h)istory | (r)ules | (q)uit"
end

def play_again?
  loop do
    play_again_prompt
    answer = gets.chomp.downcase

    case answer
    when 'h', 'hist', 'history' then display_history
    when 'c', 'cont', 'continue' then return true
    when 'n', 'new'
      @computer = choose_computer_player
      human.score.reset
      @history << "Changed to new opponent (#{computer.upcase})."
      return true
    when 'r', 'rules'
      clear_screen
      display_rules
    when 'q', 'quit' then return false
    else
      prompt "That's not a valid choice."
      prompt "Please choose 'c', 'n', 'h', 'r', or 'q'.\n"
    end
  end
end
```

It's less complex, but it's still a relatively long `case` statement. If I ever want to add a choice, I'll have to add another `when` branch to the `case` statement and depending upon what actions need to take place, this could make the `case` statement more complex and therefore more difficult to read.


<a id="org61d940f"></a>

# Refactoring

How do I refactor a `case` statement to make better use of polymorphism? In reading around the Internet, I found that the basic idea is to move code into classes that can inherit and set different state and behavior as needed.

So, instead of saying **if the choice is X, then do X**, we say something like **the object says to do X, so let's do X**. I think that's the basic idea.


<a id="orgf7d7e3a"></a>

## Initial Class Skeleton

The first thing that I did was create a skeleton set of classes that might be used in place of the `case` statement.

```ruby
class Choice
end

class HistoryChoice < Choice
end

class ContinueChoice < Choice
end

class NewChoice < Choice
end

class RulesChoice < Choice
end

class QuitChoice < Choice
end
```

This helps me see the overall structure of the code before it becomes populated with more code and before I might get distracted by other details.

Looking at this, I can imagine there being some defaults that all choices start with in the `Choice` superclass and then variations on those defaults in the various subclasses.

For instance, each choice is going to have a `@names` instance variable that contains an array of all of the possible variations for each choice, like `@names = ['r', 'rules']` for example. Also, each choice may have different behavior (methods).


<a id="org4b904d2"></a>

## Adding State `@names`

I started with the instance variable that's most obvious:

```ruby
class Choice
  attr_reader :names
end

class HistoryChoice < Choice
  def initialize
    @names = ['h', 'hist', 'history']
  end
end

class ContinueChoice < Choice
  def initialize
    @names = ['c', 'cont', 'continue']
  end
end

class NewChoice < Choice
  def initialize
    @names = ['n', 'new']
  end
end

class RulesChoice < Choice
  def initialize
    @names = ['r', 'rules']
  end
end

class QuitChoice < Choice
  def initialize
    @names = ['q', 'quit']
  end
end
```

Cool. Now what? How will I use that state? Not wanting to go too far before making sure this will work, I came up with this:

```ruby
def play_again?
  loop do
    play_gain_prompt
    answer = gets.chomp.downcase

    choices = [HistoryChoice.new, ContinueChoice.new,
               NewChoice.new, RulesChoice.new,
               QuitChoice.new]

    choice = choices.find { |choice| choice.names.include?(answer) }
  end
end
```

This works and successfully finds the user's correct choice. At this point, there are many issues that need to be addressed in order to get this to work fully.

Here are the questions I can think of:

-   What happens if the `find` method returns `nil`?
-   How will the code exit the loop and return `true` or `false` from the `play_again?` method?
-   How will the computer player be reassigned when the `new` option is chosen?
-   How will the history be shown?
-   How will the rules be shown?


<a id="orgc4407b4"></a>

## Add Prompt for an Invalid Choice

If the user enters an invalid choice, the current `case` statement code prompts the user that their choice is invalid and prompts with the available choices. To do this with the new class-based code, we need to stay within the loop but only prompt the user when the choice is invalid and not for valid choices.

After some attempts at various solutions, the one I found that worked best uses `next` and `unless`. I moved the prompt into its own method for brevity:

```ruby
def invalid_choice_prompt
  prompt "That's not a valid choice."
  prompt "Please choose 'c', 'n', 'h', 'r', or 'q'.\n"
end

def play_again?
  # ... code omitted for brevity

  next invalid_choice_prompt unless choice
end
```

If `choice` is anything other than `nil`, the `next` line will be skipped. If `choice` is `nil`, then the `invalid_choice_prompt` is output and the loop starts over.


<a id="orgf0ed375"></a>

## Exiting the Loop and Returning True/False

I had a tough time with this. I started on it and then went to sleep. I woke up half-way through the night and had the following idea on how I might accomplish this:

```ruby
loop do
  break if choice.exit_loop
end

return choice.play_again
```

This pointed me in the direction of creating two instance variables `@exit_loop` and `@play_again` and then maintaining different state for each choice, like this:

```ruby
class Choice
  attr_reader :names

  def initialize
    @exit_loop = false
    @play_again = true
  end
end

class HistoryChoice < Choice
  def initialize
    super
    @names = ['h', 'hist', 'history']
  end
end

class ContinueChoice < Choice
  def initialize
    super
    @names = ['c', 'cont', 'continue']
    @exit_loop = true
  end
end

class NewChoice < Choice
  def initialize
    super
    @names = ['n', 'new']
    @exit_loop = true
  end
end

class RulesChoice < Choice
  def initialize
    super
    @names = ['r', 'rules']
  end
end

class QuitChoice < Choice
  def initialize
    super
    @names = ['q', 'quit']
    @exit_loop = true
    @play_again = false
  end
end
```

The default choice does not exit the loop and returns `true` to play again. `HistoryChoice` and `RulesChoice` objects don't exit the loop, so they stay with the default. `ContinueChoice` and `NewChoice` objects exit the loop and then also cause the `play_again?` method to return `true`, so we override the `@exit_loop` initialization setting it to `true`, leaving the default for `@play_again`. `QuitChoice` objects exit the loop and cause the `play_again?` method to return `false`, so we override both `@exit_loop` and `@play_again` instance variables.

The above combined with the following fits everything together nicely:

```ruby
def play_again?
  play_gain_prompt
  answer = gets.chomp.downcase

  choices = [HistoryChoice.new, ContinueChoice.new,
             NewChoice.new, RulesChoice.new,
             QuitChoice.new]

  choice = choices.find { |choice| choice.names.include?(answer) }

  next invalid_choice_prompt unless choice

  return choice.play_again if choice.exit_loop
end
```


<a id="org8a2d7b5"></a>

## Choice Actions

I then needed to figure out how to choose a new opponent, show the history, and show the rules.

I think this is a good example of polymorphism at work. In order to call a choice's action, all I need to do is call `choice.action` and then whatever code is present in the `action` instance method of each choice will be evaluated. That code can be anything, but all the code calling the method needs to know is that it exists and does something.

Previously, the calling code needed to determine which code to evaluate. Now, that code has been encapsulated within each choice subclass.


<a id="org70ad437"></a>

### Show Rules

Before the `RulesChoice` subclass, I was showing the rules both at the start of the game and when the user typed `r` or `rules`. Both used the `display_rules` method:

```ruby
def display_rules
  prompt "Here are the rules:"
  puts
  prompt "Choose a move: Rock, Paper, Scissors, Lizard, or Spock."
  prompt "Your opponent will also choose a move."
  puts
  prompt "The winner is determined by the following:"
  prompt "Scissors cuts Paper"
  prompt "Scissors decapitates Lizard"
  prompt "Paper covers Rock"
  prompt "Paper disproves Spock"
  prompt "Rock crushes Scissors"
  prompt "Rock crushes Lizard"
  prompt "Lizard poisons Spock"
  prompt "Lizard eats Paper"
  prompt "Spock smashes Scissors"
  prompt "Spock vaporizes Rock"
end
```

This needs to be accessible from within the `RPSGame` class and the `RulesChoice` class. One way to do this and the way I settled on is to move the `display_rules` method into the `Utilities` module.


<a id="org7f7c26f"></a>

### Show History

Initially, I had a method within the `RPSGame` class called `display_history` that looked like this:

```ruby
def display_history
  prompt format(line_format, "RND", "SCORE", "HUM", "H. MOVE",
                             "COM", "C. MOVE", "WINNER")
  prompt format(line_format, "---", "-----", "---", "-------",
                             "---", "-------", "------")
  history.each { |round| prompt round }
end
```

I only called that once the user chose `h`, `hist`, or `history` in the case statement. So, I moved the code in that method to the `HistoryChoice#action` method:

```ruby
def action
  clear_screen
  prompt format(line_format, "RND", "SCORE", "HUM", "H. MOVE",
                "COM", "C. MOVE", "WINNER")
  prompt format(line_format, "---", "-----", "---", "-------",
                "---", "-------", "------")
  history.each { |round| prompt round }
end
```

In doing so, two things became apparent. One, I need access to the `line_format` method in the `Utilities` module. That's easy enough, I just included the `Utilities` module in the `Choice` superclass and its methods are now available in all of the subclasses.

Two, I need access to the `@history` instance variable in the `RPSGame` class. This was a little more difficult to reason through and is related to decisions I made to solve the problem of choosing a new opponent.


<a id="org7dd4108"></a>

### Choose New Opponent

This took me a while to figure out. First, and probably not the best idea, but I had to pass in the current `RPSGame` object as an argument to the choice subclasses at instantiation.

```ruby
def choices
  [HistoryChoice.new(self), ContinueChoice.new(self),
   NewChoice.new(self), RulesChoice.new(self),
   QuitChoice.new(self)]
end
```

This has the negative effect of passing a lot of data around, especially as the history grows with more rounds. That said, it works.

Then, I need to accept the parameter in the `Choice#initialize` method and setup the necessary instance variables:

```ruby
class Choice
  include Utilities
  attr_reader :names, :exit_loop, :play_again, :human, :rps_game

  def initialize(rps_game)
    @human = rps_game.human
    @rps_game = rps_game
    @exit_loop = false
    @play_again = true
  end

  def action; end
end
```

The local variable `rps_game` is assigned to the `@rps_game` instance variable for use in actions as needed. Since every choice can have an action, I have a default empty `action` instance method in case any of the subclasses don't need to override it.

With this change, each choice object now has access to the \`RPSGame\` state and a default `action` instance method.

The next step is to move the `choose_computer_player` method into the `Utilities` module and modify it slightly to accept a `computer` object as a parameter:

```ruby
def choose_computer_player(computer = nil)
  players = [R2D2.new, Hal.new, Chappie.new, Sonny.new, Number5.new]
  players.delete_if { |player| player.name == computer.name } if computer
  players.sample
end
```

From here, I have what I need to implement the actions for each choice. `ContinueChoice` and `QuitChoice` objects don't have specific actions so they rely on the `Choice#action`, which does nothing at the moment. The action for `RulesChoice` was already completed. This leaves `NewChoice` and `HistoryChoice`.

For `NewChoice`, the action looks like this:

```ruby
def action
  rps_game.computer = choose_computer_player(rps_game.computer)
  human.score.reset
  rps_game.history << "Changed to new opponent (#{rps_game.computer.upcase})."
end
```

We're reassigning the `RPSGame@computer` instance variable to a new computer player object. We reset the human's score using the `@human` instance method which references the `RPSGame@human` instance variable. Then, we add a new line to the history by appending a String object to the `RPSGame@history` instance method.

Finally, to get the `HistoryChoice` action working, I needed to change:

```ruby
history.each { |round| prompt round }
```

to:

```ruby
rps_game.history.each { |round| prompt round }
```


<a id="orgb70be27"></a>

# The Final Code

([Source](https://gitlab.com/jd-launch-school/launch-school/-/blob/b8263e164d21be65d877e1b3bfde59ec4e34748d/rb129_oop/rps.rb))


<a id="orgdc0686f"></a>

## Utilities Module

```ruby
module Utilities
  # ... code omitted for brevity

  def display_rules
    prompt <<~RULES
      Here are the rules:

      => Choose a move: Rock, Paper, Scissors, Lizard, or Spock.
      => Your opponent will also choose a move.

      => The winner is determined by the following:

      => Scissors cuts Paper
      => Scissors decapitates Lizard
      => Paper covers Rock
      => Paper disproves Spock
      => Rock crushes Scissors
      => Rock crushes Lizard
      => Lizard poisons Spock
      => Lizard eats Paper
      => Spock smashes Scissors
      => Spock vaporizes Rock
    RULES
  end

  def choose_computer_player(computer = nil)
    players = [R2D2.new, Hal.new, Chappie.new, Sonny.new, Number5.new]
    players.delete_if { |player| player.name == computer.name } if computer
    players.sample
  end
end
```


<a id="org882db3b"></a>

## Choice Classes


<a id="org755ebf7"></a>

### Choice Superclass

```ruby
class Choice
  include Utilities
  attr_reader :names, :exit_loop, :play_again, :human, :rps_game

  def initialize(rps_game)
    @human = rps_game.human
    @rps_game = rps_game
    @exit_loop = false
    @play_again = true
  end

  def action; end
end
```


<a id="org8de3f99"></a>

### HistoryChoice Subclass

```ruby
class HistoryChoice < Choice
  def initialize(rps_game)
    super(rps_game)
    @names = ['h', 'hist', 'history']
  end

  def action
    clear_screen
    prompt format(line_format, "RND", "SCORE", "HUM", "H. MOVE",
                  "COM", "C. MOVE", "WINNER")
    prompt format(line_format, "---", "-----", "---", "-------",
                  "---", "-------", "------")
    rps_game.history.each { |round| prompt round }
  end
end
```


<a id="orge0aab83"></a>

### ContinueChoice Subclass

```ruby
class ContinueChoice < Choice
  def initialize(rps_game)
    super(rps_game)
    @names = ['c', 'cont', 'continue']
    @exit_loop = true
  end
end
```


<a id="orge018ad3"></a>

### NewChoice Subclass

```ruby
class NewChoice < Choice
  def initialize(rps_game)
    super(rps_game)
    @names = ['n', 'new']
    @exit_loop = true
  end

  def action
    rps_game.computer = choose_computer_player(rps_game.computer)
    human.score.reset
    rps_game.history << "Changed to new opponent (#{rps_game.computer.upcase})."
  end
end
```


<a id="org5de495e"></a>

### RulesChoice Subclass

```ruby
class RulesChoice < Choice
  def initialize(rps_game)
    super(rps_game)
    @names = ['r', 'rules']
  end

  def action
    clear_screen
    display_rules
  end
end
```


<a id="org6cd0273"></a>

### QuitChoice Subclass

```ruby
class QuitChoice < Choice
  def initialize(rps_game)
    super(rps_game)
    @names = ['q', 'quit']
    @exit_loop = true
    @play_again = false
  end
end
```


<a id="org474a5d6"></a>

## RPSGame Class

```ruby
class RPSGame
  # ... code omitted for brevity

  def play_again_prompt
    puts
    prompt "What would you like to do?"
    prompt "Options: (c)ontinue | (n)ew opponent | (h)istory | (r)ules | (q)uit"
  end

  def choices
    [HistoryChoice.new(self), ContinueChoice.new(self),
     NewChoice.new(self), RulesChoice.new(self),
     QuitChoice.new(self)]
  end

  def invalid_choice_prompt
    prompt "That's not a valid choice."
    prompt "Please choose 'c', 'n', 'h', 'r', or 'q'.\n"
  end

  def play_again?
    loop do
      play_again_prompt
      answer = gets.chomp.downcase

      choice = choices.find { |option| option.names.include?(answer) }

      next invalid_choice_prompt unless choice

      choice.action
      return choice.play_again if choice.exit_loop
    end
  end
end
```


<a id="org8982b0a"></a>

# Summary

While working on this refactor, I realized how intertwined and interconnected some parts of the code is with other parts of the code. Moving code to one place often meant changing code in another place. However, since I used an object-oriented approach for the players, moves, scores, and rounds, I didn't need to change anything in that code other than maybe the way I referred to it.

So, basically, I was able to add these new classes and refactor away from the `case` statement without modifying much of the rest of the code.

I also noticed that this refactoring or rewriting added a lot of code in comparison to the code that I replaced. In this example, I probably didn't need to move away from the `case` statement as I am very unlikely to ever add more choices to this game. However, since the `play_again?` method had too many lines and I'm interested in learning how `case` statements can be converted into their OOP counterpart that uses polymorphism, I felt the effort was justified.

I think there's still a lot of room for improvement with this code. I feel like there are parts that are too dependent on one another and that dependency isn't always immediately visible. However, for now, I think what I have here will be sufficient.
