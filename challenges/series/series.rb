class Series
  def initialize(string)
    @numbers = string.chars.map(&:to_i)
  end

  def slices(length)
    raise ArgumentError, 'Slice length is too long!' if length > @numbers.size
    @numbers.each_cons(length).with_object([]) { |slice, result| result << slice }
  end
end
