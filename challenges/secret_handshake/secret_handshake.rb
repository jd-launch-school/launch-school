=begin
PROBLEM:
- INPUT: Integer or String
  - if input cannot be converted to binary, binary value is 0
- OUTPUT: Array of Strings representing handshake commands
  - output starts from right to left (unless reversed)
  - return empty array for binary value 0

Lookup table:
1 = wink
10 = double blink
100 = close your eyes
1000 = jump
10000 = reverse output

EXAMPLES:
2 = ['double blink']
4 = ['close your eyes']
8 = ['jump']
'piggies' = []
'1' = ['wink']

DATA STRUCTURES:
- Array to store lookup table using index starting at 0

- Input is Integer or String
  - convert to Integer and then to String (base 2)

- Binary is String

- Split Binary String into an array of characters and reverse

- Result will be an Array

ALGORITHM:
- set `result` array to empty array
- convert input to integer and then to string (base 2)
- split the string into an array of characters
- reverse the array of characters
- iterate over the array of characters with 'num' and 'index' as parameters
  - return reversed array if `num` is equal to `1` and `index` is equal to `4`
  - lookup index in lookup table if `num` is equal to `1`
  - append return value of lookup to `result`
=end

class SecretHandshake
  COMMANDS = [ 'wink', 'double blink', 'close your eyes', 'jump' ]

  def initialize(input)
    @binary = input.to_i.to_s(2).chars.reverse
  end

  def commands
    result = []

    @binary.each_with_index do |num, index|
      return result.reverse if num == '1' && index == 4

      result << COMMANDS[index] if num == '1'
    end

    result
  end
end
