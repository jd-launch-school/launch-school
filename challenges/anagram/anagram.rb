class Anagram
  def initialize(word)
    @word = word
  end

  def match(words)
    words.select { |word| anagram?(word, @word) }
  end

  private

  def anagram?(word1, word2)
    return false if word1.downcase == word2.downcase || word1.size != word2.size

    counter = 0

    loop do
      break if counter > word1.size

      current_letter = word1[counter]
      return false unless word2.downcase.chars.include?(current_letter)

      word1_chars = word1.chars
      word1_chars.delete_at(word1.index(current_letter))
      word1 = word1_chars.join

      counter += 1
    end

    word1.size.zero?
  end
end
