require 'date'

class Meetup
  DAYS_IN_MONTH = { 1 => 31,  2 => 28,  3 => 31,  4 => 30,
                    5 => 31,  6 => 30,  7 => 31,  8 => 31,
                    9 => 30, 10 => 31, 11 => 30, 12 => 31}

  WEEKDAY_NUMBERS = { sunday: 0, monday: 1, tuesday: 2, wednesday: 3,
                      thursday: 4, friday: 5, saturday: 6 }

  def initialize(month, year)
    @date = Date.new(year, month)
    @days_in_month = DAYS_IN_MONTH[month]
  end

  def day(weekday, repeated_day)
    result = []
    weekday_number = WEEKDAY_NUMBERS[weekday]
    
    (1..@days_in_month).each do
      result << @date if weekday_number == @date.wday
      @date = @date.next_day
    end

    case repeated_day
    when :teenth
      result.select { |date| date.day.between?(13, 19) }.first
    when :first
      result.first
    when :second
      result[1]
    when :third
      result[2]
    when :fourth
      result[3]
    when :last
      result.last
    end
  end
end
