=begin
PROBLEM:
 - name of student
 - roster of multiple students
 - grade
=end

class School
  def initialize
    @roster = {}
  end

  def add(student, grade)
    @roster[grade] = [] unless @roster[grade]
    @roster[grade].push(student).sort!
  end

  def grade(integer)
    @roster[integer] || []
  end

  def to_h
    @roster.sort.to_h
  end
end
