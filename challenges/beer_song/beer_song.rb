class DefaultVerse
  def verse(number)
    "#{number} bottles of beer on the wall, #{number} bottles of beer.\n" \
    "Take one down and pass it around, #{number - 1} bottles of beer on the wall.\n"
  end
end

class ZeroVerse
  def verse(_)
    "No more bottles of beer on the wall, no more bottles of beer.\n" \
    "Go to the store and buy some more, 99 bottles of beer on the wall.\n"
  end
end

class OneVerse
  def verse(_)
    "1 bottle of beer on the wall, 1 bottle of beer.\n" \
    "Take it down and pass it around, no more bottles of beer on the wall.\n"
  end
end

class TwoVerse
  def verse(_)
    "2 bottles of beer on the wall, 2 bottles of beer.\n" \
    "Take one down and pass it around, 1 bottle of beer on the wall.\n"
  end
end

class BeerSong
  def initialize
    @verse_lookup = {
      0 => ZeroVerse,
      1 => OneVerse,
      2 => TwoVerse,
      3..99 => DefaultVerse
    }
  end

  def verse(number)
    @verse_lookup.select { |verse| verse === number }.values.first.new.verse(number)
  end

  def verses(first, last)
    first.downto(last).map { |num| verse(num) }.join("\n")
  end

  def lyrics
    verses(99, 0)
  end
end
