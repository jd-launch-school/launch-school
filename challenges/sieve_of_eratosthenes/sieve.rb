=begin
Wikipedia algorithm:

algorithm Sieve of Eratosthenes is
    input: an integer n > 1.
    output: all prime numbers from 2 through n.

    let A be an array of Boolean values, indexed by integers 2 to n,
    initially all set to true.
    
    for i = 2, 3, 4, ..., not exceeding √n do
        if A[i] is true
            for j = i2, i2+i, i2+2i, i2+3i, ..., not exceeding n do
                A[j] := false

    return all i such that A[i] is true.
=end

class Sieve
  def initialize(limit)
    @limit = limit
    @primes = Array.new(@limit + 1, true)
  end

  def primes
    (2..@limit).each do |num|
      if num
        counter = 0

        loop do
          not_prime = (num ** 2) + (counter * num)
          break if not_prime > @limit

          @primes[not_prime] = false
          counter += 1
        end
      end
    end

    @primes.map.with_index { |prime, index| prime = index if prime }[2..-1].compact
  end
end
