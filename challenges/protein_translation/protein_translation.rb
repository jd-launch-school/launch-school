=begin
DATA STRUCTURES:
- `of_codon` method: given String representing a codon
- `of_rna` method: given String representing an RNA strand
- Hash to store codon - amino acid lookup table

ALGORITHM:
- `of_codon` class method:
  - return value of hash element reference of given String in `codon-to-amino-acid` lookup table
  - raise `InvalidCodonError` if given String is not found in lookup table

- `of_rna` class method:
  - split the given string into an array of segments conataining 3 characters each
    - use `scan(/.{3}/)
  - iterate over the array using `each_with_object`
    - break early if the amino acid equals "STOP"
    - append amino acid to result
  - return joined (by commas) result
=end

class InvalidCodonError < StandardError; end

class Translation
  CODON_TO_AMINO_ACID = { "Methionine" => ["AUG"],
                          "Phenylalanine" => ["UUU", "UUC"],
                          "Leucine" => ["UUA", "UUG"],
                          "Serine" => ["UCU", "UCC", "UCA", "UCG"],
                          "Tyrosine" => ["UAU", "UAC"],
                          "Cysteine" => ["UGU", "UGC"],
                          "Tryptophan" => ["UGG"],
                          "STOP" => ["UAA", "UAG", "UGA"] }

  def self.of_codon(codon)
    result = CODON_TO_AMINO_ACID.select { |_, codons| codons.include? codon }.keys.first
    raise InvalidCodonError if result.nil?
    result
  end

  def self.of_rna(strand)
    strand.scan(/.{3}/).each_with_object([]) do |codon, result|
      return result if of_codon(codon) == "STOP"

      result << of_codon(codon)
    end
  end
end
