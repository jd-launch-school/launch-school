class PerfectNumber
  def self.classify(number)
    raise StandardError if number.negative?

    sum = (1...number).select { |num| (number % num).zero? }.sum

    return 'deficient' if sum < number
    return 'perfect' if sum == number

    'abundant'
  end
end
