=begin
PROBLEM:
- INPUT: String
- OUTPUT: String with given String translated to Pig Latin
- RULES:
  - if word begins with vowel sound, add "ay" sound to the end of the word
  - if word begins with consonant sound, move consonant souned to the end of the word
    and then add "ay" sound to the end of the word

DATA STRUCTURE
- given String

ALGORITHM:
- IF word starts with a vowel, `y` (with consonant following), or `x` (with consonant following) (use `match` and regex)
  - add 'ay' to the end

- IF word starts with a consonant, `ch`, `qu`, `squ`, `th`, `thr`, `sch` (use `match` and regex)
  - move matched consonants to end plus 'ay'
=end

class PigLatin
  def self.translate(words)
    result = words.split.map do |word|
      if begins_with_vowel_sound?(word)
        word + 'ay' 
      else
        match = consonant_sound(word)
        word[match.size..-1] + match + 'ay'
      end
    end

    result.join(' ')
  end

  def self.begins_with_vowel_sound?(word)
    word.match(/^[aeiou]|^[yx][^aeiou]/)
  end

  def self.consonant_sound(word)
    word.match(/^ch|^qu|^squ|^thr|^th|^sch|^[^aeiou]/)[0]
  end
end

p PigLatin.translate('chrysalis')
