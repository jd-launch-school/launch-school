class Phrase
  def initialize(phrase)
    @words = phrase.gsub(/\\n/, ' ')
               .gsub(/[^a-z0-9 ,']*/i, '')
               .gsub(/\s'(.*)'/) { " " + $1 }
               .gsub(/,/, ' ').split
  end

  def word_count
    @words.each_with_object({}) do |word, counts|
      next counts[word.downcase] += 1 if counts[word.downcase]
      counts[word.downcase] = 1
    end
  end
end
