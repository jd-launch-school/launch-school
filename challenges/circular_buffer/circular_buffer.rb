=begin
PROBLEM:
- INPUT for `initialize` method: Integer
- INPUT for `write` method: String
- OUTPUT: String

DATA STRUCTURE:
- buffer is an Array of Strings

ALGORITHM:
- `initialize` method:
  - initialize `length` instance variable to given length
  - initialize `buffer` instance variable to empty array
  - initialize `oldest_index` to 0
  - initialize `next_available_index` to 0

- `read` method
  - raise exception if buffer is empty
  - remove and return item at oldest_index
  - increment oldest_index by 1
  - reassign oldest_index to 0 if buffer is emtpy

- `write` method
  - return exception if buffer is full
  - use element assignment to set item at next_available_index to given String
  - increment next_available_index by 1
  - reassign next_available_index to 0 if next_available_index == length

- `write!` method
  - call `read`
  - call `write` and pass in given String

- `clear` method
  - reassign buffer to an empty array
=end

class CircularBuffer
  class BufferEmptyException < StandardError; end
  class BufferFullException < StandardError; end

  def initialize(length)
    @length = length
    @buffer = Array.new(@length)
    @oldest_index = 0
    @next_available_index = 0
  end

  def read
    raise BufferEmptyException if @buffer.all?(&:nil?)

    result = @buffer[@oldest_index]
    @buffer[@oldest_index] = nil
    @oldest_index += 1
    @oldest_index = 0 if @oldest_index == @length

    if @buffer.all?(&:nil?)
      @oldest_index = 0
      @next_available_index = 0
    end

    result
  end

  def write(char)
    raise BufferFullException if @buffer.all?
    unless char.nil?
      @buffer[@next_available_index] = char
      @next_available_index += 1
      @next_available_index = 0 if @next_available_index == @length
    end
  end

  def write!(char)
    unless char.nil?
      read if @buffer.all?
      write(char)
    end
  end

  def clear
    @buffer = Array.new(@length)
  end
end
