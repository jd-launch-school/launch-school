class Integer
  def to_roman
    number = self

    ones_numerals = %w(I II III IV V VI VII VIII IX)
    tens_numerals = %w(X XX XXX XL L LX LXX LXXX XC)
    hundreds_numerals = %w(C CC CCC CD D DC DCC DCCC CM)
    thousands_numerals = %w(M MM MMM)

    ones, tens, hundreds, thousands = self.digits

    roman_numeral = ''

    roman_numeral += thousands_numerals[thousands - 1] unless thousands.nil? || thousands.zero?
    roman_numeral += hundreds_numerals[hundreds - 1] unless hundreds.nil? || hundreds.zero?
    roman_numeral += tens_numerals[tens - 1] unless tens.nil? || tens.zero?
    roman_numeral += ones_numerals[ones - 1] unless ones.nil? || ones.zero?

    roman_numeral
  end
end
