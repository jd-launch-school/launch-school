class Trinary
  def initialize(string)
    @digits = string.to_i.digits
  end

  def to_decimal
    @digits.each_with_index.reduce(0) do |decimal, (digit, index)|
      decimal + (digit * 3 ** index)
    end
  end
end
