=begin
PROBLEM:
- INPUT: Integer representing upper limit of range to look through
         Zero or more Integers representing the multiples to check for
          - defaults to `3` and `5`
- OUTPUT: Sum of the multiples
- WHAT WILL I DO WITH THE INPUT?
  - use the upper limit given to create a range from `1` up to but not including the upper limit
  - iterate over the range
    - iterate over the number set, either given or default
      - IF the current number in the range divided by the current number from the number set
        has no remainder
        - store the current number
        - break out of the loop and move on to the next number in the range
  - return the sum of the multiples in the resulting list of multiples

- need both a class and instance method named `to`
  - the `to` instance method can call the `to` class method

- DATA STRUCTURES
  - use a Range (1...upper_limit) to create the possible numbers to check
  - use Array for resulting list of multiples
  - store multiples in an Array, ex: [3, 5]
=end

class SumOfMultiples
  def initialize(*number_set)
    @number_set = number_set
  end

  def self.to(upper_limit, number_set = [3, 5])
    multiples = []

    (1...upper_limit).each do |potential_multiple|
      number_set.each do |number|
        if (potential_multiple % number).zero?
          break multiples << potential_multiple
        end
      end
    end

    multiples.sum
  end

  def to(upper_limit)
    self.class.to(upper_limit, @number_set)
  end
end
