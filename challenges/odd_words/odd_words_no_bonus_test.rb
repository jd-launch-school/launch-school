require 'minitest/autorun'
require_relative 'odd_words'

class OddWordsTest < Minitest::Test
  def test_empty_string
    odd_words = OddWords.new('')
    assert_raises(StandardError, 'String must have at least one letter and a period.') { odd_words.output }
  end

  def test_word_longer_than_20_characters
    odd_words = OddWords.new('some verylongwordthatistoolong.')
    assert_raises(StandardError, 'Words in string must not be more than 20 characters.') { odd_words.output }
  end

  def test_one_word
    odd_words = OddWords.new('test.')
    assert_equal('test.', odd_words.output)
  end

  def test_only_period
    odd_words = OddWords.new('.')
    assert_equal('.', odd_words.output)
  end

  def test_two_words
    odd_words = OddWords.new('a test.')
    assert_equal('a tset.', odd_words.output)
  end

  def test_three_words
    odd_words = OddWords.new('is another test.')
    assert_equal('is rehtona test.', odd_words.output)
  end

  def test_more_words
    odd_words = OddWords.new('whats the matter with kansas.')
    assert_equal('whats eht matter htiw kansas.', odd_words.output)
  end

  def test_three_words_spaces_before_period
    odd_words = OddWords.new('is another test  .')
    assert_equal('is rehtona test.', odd_words.output)
  end

  def test_more_spaces_between_words
    odd_words = OddWords.new('whats the  matter with kansas.')
    assert_equal('whats eht matter htiw kansas.', odd_words.output)
  end

  def test_more_spaces_before_period
    odd_words = OddWords.new('whats the matter with kansas        .')
    assert_equal('whats eht matter htiw kansas.', odd_words.output)
  end

  def test_extra_spaces
    odd_words = OddWords.new('whats    the matter   with kansas        .')
    assert_equal('whats eht matter htiw kansas.', odd_words.output)
  end

  def test_more_spaces_before_period_reveresed_last_word
    odd_words = OddWords.new('whats the matter with kansas word         .')
    assert_equal('whats eht matter htiw kansas drow.', odd_words.output)
  end
end
