class OddWords
  def initialize(string)
    @string = string
  end

  def output
    raise StandardError, 'String must have at least one letter and a period.' if @string == ''

    if @string.split(' ').any? { |word| word.size > 20 }
      raise StandardError, 'Words in string must not be more than 20 characters.'
    end

    result = @string.split(/[ .]/).reject { |word| word.empty? }.map.with_index do |word, index|
      index.odd? ? word.reverse : word
    end

    result.join(' ') + '.'
  end
end
