require 'minitest/autorun'
require_relative 'odd_words'

class OddWordsTest < Minitest::Test
  def test_empty_string
    odd_words = OddWords.new('')
    assert_raises(StandardError, 'String must have at least one letter and a period.') { odd_words.output }
  end

  def test_word_longer_than_20_characters
    odd_words = OddWords.new('some verylongwordthatistoolong.')
    assert_raises(StandardError, 'Words in string must not be more than 20 characters.') { odd_words.output }
  end

  def test_one_word
    odd_words = OddWords.new('test.')
    assert_output('test.') { odd_words.output }
  end

  def test_only_period
    odd_words = OddWords.new('.')
    assert_output('.') { odd_words.output }
  end

  def test_two_words
    odd_words = OddWords.new('a test.')
    assert_output('a tset.') { odd_words.output }
  end

  def test_three_words
    odd_words = OddWords.new('is another test.')
    assert_output('is rehtona test.') { odd_words.output }
  end

  def test_more_words
    odd_words = OddWords.new('whats the matter with kansas.')
    assert_output('whats eht matter htiw kansas.') { odd_words.output }
  end

  def test_three_words_spaces_before_period
    odd_words = OddWords.new('is another test  .')
    assert_output('is rehtona test.') { odd_words.output }
  end

  def test_more_spaces_between_words
    odd_words = OddWords.new('whats the  matter with kansas.')
    assert_output('whats eht matter htiw kansas.') { odd_words.output }
  end

  def test_more_spaces_before_period
    odd_words = OddWords.new('whats the matter with kansas        .')
    assert_output('whats eht matter htiw kansas.') { odd_words.output }
  end

  def test_extra_spaces
    odd_words = OddWords.new('whats    the matter   with kansas        .')
    assert_output('whats eht matter htiw kansas.') { odd_words.output }
  end

  def test_more_spaces_before_period_reveresed_last_word
    odd_words = OddWords.new('whats the matter with kansas word         .')
    assert_output('whats eht matter htiw kansas drow.') { odd_words.output }
  end
end
