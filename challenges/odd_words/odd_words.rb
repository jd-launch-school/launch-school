=begin
PROBLEM:
- INPUT: String ending with a period, words separated by spaces
  - each word cannot be longer than 20 characters
- OUTPUT: String ending with a period, words separated by spaces,
          odd words reversed
  - odd is defined based on 0 starting index
    - ex: "this is a test" --> 'is' and 'test' have odd indexes
- WHAT WILL I DO WITH THE INPUT?
  - convert the given string into an array of characters
  - iterate over the array of characters
    - append each character to a result string up to and including the first space
    - prepend each character to a temporary string to hold the reversed word
    - append that temporary string to the result string
    - keep track of the number of spaces
      - so, the first space means the next word is reversed
      - the second space doesn't mean anything
      - the third space means the next word is reversed
      - etc...

ALGORITHM:
- split given string into array of characters using `chars`
- iterate over array of characters using `each`
  - initialize `reversed` local variable to an empty string
  - initialize `space_number` local variable to 0
  - IF the current character is a space
    - IF `space_number` is even
      - `print` current character (space)
      - increment `space_number` by 1
    - ELSE
      - iterate over `reversed` and `print` each character
      - `print` current character (space)
      - increment `space_number` by 1
  - ELSE IF the current character is a period
    - IF `space_numer` is even
      - `print` current character (period)
    - ELSE
      - iterate over `reversed` and `print` each character
      - `print` current character (period)
  - ELSE
    - IF `space_number` is even
      - `print` current character
    - ELSE
      - prepend current character to `reversed`
- return `result`
=end

class OddWords
  def initialize(string)
    @string = string
    @space_number = 0
    @reversed = []
  end

  def output
    raise StandardError, 'String must have at least one letter and a period.' if @string == ''

    if @string.split(' ').any? { |word| word.size > 20 }
      raise StandardError, 'Words in string must not be more than 20 characters.'
    end

    @string.chars.each.with_index do |char, index|
      if @space_number.even?
        unless char == ' ' && @string[index - 1] == ' '
          print char
          @space_number += 1 if char == ' '
        end
      else
        if char == ' ' || char == '.'
          @reversed.each { |reversed_char| print reversed_char }
          @reversed = []
          print char
          @space_number += 1
        else
          @reversed.prepend(char)
        end
      end
    end
  end
end
