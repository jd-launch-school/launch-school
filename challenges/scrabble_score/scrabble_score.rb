class Scrabble
  SCORES = { 
    ['a', 'e', 'i', 'o', 'u', 'l', 'n', 'r', 's', 't'] => 1,
    ['d', 'g'] => 2,
    ['b', 'c', 'm', 'p'] => 3,
    ['f', 'h', 'v', 'w', 'y'] => 4,
    ['k'] => 5,
    ['j', 'x'] => 8,
    ['q', 'z'] => 10
  }

  def initialize(word)
    @word = word
  end

  def self.score(word)
    new(word).score
  end

  def score
    return 0 if @word.nil? || !@word.match?(/[[:alpha:]]/)

    @word.chars.reduce(0) do |score, letter|
      #      p score_letter(letter)
      score + score_letter(letter.downcase)
    end
  end

  private

  def score_letter(letter)
    SCORES[SCORES.keys.find { |key| key.include?(letter) }]
  end
end
