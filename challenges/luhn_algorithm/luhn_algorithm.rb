=begin
PROBLEM:
- INPUT: Integer
- OUTPUT: 
  - addends method: Array
  - checksum method: Integer
  - valid? method: Boolean
  - create method: Integer

EXAMPLE:

  Given:   2323 2005 7766 355
  Addends: 2626 2001 7563 315   (Checksum: 49)
  Try:     2323 2005 7766 3550
  Addends: 4343 4005 5736 6510  (Checksum: 56)
                             ^
                             Can change this number as needed.

  Next highest valid checksum: 60
  Valid Number: 2323 2005 7766 3554  (change to 4: 60 - 56)

DATA STRUCTURE:
- convert given Integer into an Array of digits

ALGORITHM:
- initilalize:
  - convert to an array of digits using `digits` assigned to `digits` local variable

- addends method:
  - iterate over array of digits with index using `map` with `digit` and `index` as parameters
    - IF `index` is odd, double the digit
      - IF the total after doubling is 10 or more, subtract 9

- checksum method
  - call `addends` method and sum the returned array

- valid? method
  - call `checksum` method and return `true` if result % 10 is equal to zero

- create method
  - iterate over the range (0..9) with `num` as parameter
    - append `num` to `digits`
    - call `checksum` method on new `digits` array
    - return `digits` if `checksum` ends in `0` (checksum % 10)
=end

class Luhn
  def initialize(number)
    @digits = number.digits
  end

  def addends
    addends = @digits.map.with_index do |digit, index|
      doubled = digit * 2

      if index.odd?
        doubled >= 10 ? doubled - 9 : doubled
      else
        digit
      end
    end

    addends.reverse
  end

  def checksum
    addends.sum
  end

  def valid?
    (checksum % 10).zero?
  end

  def self.create(input)
    (0..9).each do |num|
      number = (input.to_s + num.to_s).to_i
      return number if new(number).valid?
    end
  end
end
