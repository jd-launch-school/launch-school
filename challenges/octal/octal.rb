class Octal
  def initialize(input)
    @digits = input.match?(/^[0-7]*$/) ? input.to_i.digits : [0]
  end

  def to_decimal
    @digits.each_with_index.reduce(0) { |decimal, (digit, index)| decimal + digit * (8 ** index) }
  end
end
