* Say Hello
  RB101 > Lesson 4 > Practice Looping > Loops 1
  https://launchschool.com/exercises/f55289ff

** Exercise Description
Modify the code below so "Hello!" is printed 5 times.

#+begin_example ruby
say_hello = true

while say_hello
  puts 'Hello!'
  say_hello = false
end
#+end_example

** My Solution
#+begin_src ruby :results output :exports both
say_hello = true
counter = 1

while say_hello
  puts 'Hello!'
  say_hello = false if counter == 5
  counter += 1
end
#+end_src

#+RESULTS:
: Hello!
: Hello!
: Hello!
: Hello!
: Hello!

** Recommended Solution
#+begin_src ruby :results output :exports both
say_hello = true
count = 0

while say_hello
  puts 'Hello!'
  count += 1
  say_hello = false if count == 5
end
#+end_src

