MAX_NUMBERS = 5
numbers = []

loop do
  puts "Enter number #{numbers.size + 1} of #{MAX_NUMBERS}:"
  input = gets.chomp.to_i
  numbers << input
  break if numbers.size == 5
end
puts numbers
