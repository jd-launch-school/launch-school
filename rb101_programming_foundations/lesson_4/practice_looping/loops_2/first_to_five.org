* First to Five
RB101 > Lesson 4 > Practice Looping > Loops 2
https://launchschool.com/exercises/41f4b2a2

** Exercise Description
The following code increments ~number_a~ and ~number_b~ by either 0 or 1. ~loop~ is used so that the variables can be incremented more than once, however, ~break~ stops the loop after the first iteration. Use ~next~ to modify the code so that the loop iterates until either ~number_a~ or ~number_b~ equals ~5~. Print ~"5 was reached!"~ before breaking out of the loop.

#+begin_example ruby
number_a = 0
number_b = 0

loop do
  number_a += rand(2)
  number_b += rand(2)

  break
end
#+end_example

** My Solution
#+begin_src ruby :results output :exports both
number_a = 0
number_b = 0

loop do
  number_a += rand(2)
  number_b += rand(2)

  next if number_a != 5 && number_b != 5
  puts "5 was reached!"
  break 
end
#+end_src

#+RESULTS:
: 5 was reached!

** Recommended Solution
#+begin_src ruby :results output :exports both
number_a = 0
number_b = 0

loop do
  number_a += rand(2)
  number_b += rand(2)
  next unless number_a == 5 || number_b == 5

  puts '5 was reached!'
  break
end
#+end_src

