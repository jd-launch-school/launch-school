* Practice Problem 8
RB101 > Lesson 4 > Practice Problems: Additional Practice
https://launchschool.com/lessons/85376b6d/assignments/a76c28ac

** Exercise Description
What happens when we modify an array while we are iterating over it? What would be output by this code?

#+begin_example ruby
numbers = [1, 2, 3, 4]
numbers.each do |number|
  p number
  numbers.shift(1)
end
#+end_example

What would be output by this code?

#+begin_example ruby
numbers = [1, 2, 3, 4]
numbers.each do |number|
  p number
  numbers.pop(1)
end
#+end_example

** My Answer
*** What happens when we modify an array while we are iterating over it?
The array changes after each iteration so the code will skip indexes.

Example:

If the array has four elements at indexes ~0~, ~1~, ~2~, and ~3~ and the first iteration removes the first element (at index ~0~) from the array, the array now only has three elements at indexes ~0~, ~1~, ~2~, moving each remaining element in the array back one index. The next iteration will be looking for the element at index ~1~ and will skip the element at ~0~. The resulting array will include the original element at index ~1~ (now at index ~0~) even though that element was supposed to be removed or evaluated in some other way.

*** What would be output by this code? (shift)
#+begin_example bash
1
3
#+end_example

*** What would be output by this code? (pop)
#+begin_example bash
1
2
#+end_example

** Recommended Solution
first one...

#+begin_example bash
1
3
#+end_example

second one...

#+begin_example bash
1
2
#+end_example

To better understand what is happening here, we augment our loop with some additional "debugging" information:

#+begin_example ruby
numbers = [1, 2, 3, 4]
numbers.each_with_index do |number, index|
  p "#{index}  #{numbers.inspect}  #{number}"
  numbers.shift(1)
end
#+end_example

The output is:

#+begin_example bash
"0  [1, 2, 3, 4]  1"
"1  [2, 3, 4]  3"
#+end_example

From this we see that our array is being changed as we go (shortened and shifted), and the loop counter used by ~#each~ is compared against the current length of the array rather than its original length.

In our first example, the removal of the first item in the first pass changes the value found for the second pass.

In our second example, we are shortening the array each pass just as in the first example...but the items removed are beyond the point we are sampling from in the abbreviated loop.

In both cases we see that iterators DO NOT work on a copy of the original array or from stale meta-data (length) about the array. They operate on the original array in real time.
