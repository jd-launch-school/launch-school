* Practice Problem 5
RB101 > Lesson 4 > Practice Problems: Methods and More Methods
https://launchschool.com/lessons/85376b6d/assignments/fd13de08

** Exercise Description
What does ~shift~ do in the following code? How can we find out?

#+begin_example ruby
hash = { a: 'ant', b: 'bear' }
hash.shift
#+end_example

** My Solution
~shift~ removes and returns the first key/value pair of the hash as a two-item array. ~hash.shift~ will return the array ~[ :a, 'ant' ]~, mutating ~hash~ to ~{ b: 'bear' }~. This information can be found by looking at the [[https://docs.ruby-lang.org/en/2.6.0/Hash.html#method-i-shift][Hash#shift Ruby documentation]].

** Recommended Solution
~shift~ destructively removes the first key-value pair in ~hash~ and returns it as a two-item array. If we didn't already know how ~shift~ worked, we could easily learn by checking the [[https://docs.ruby-lang.org/en/2.6.0/Hash.html#method-i-shift][docs for Hash#shift]]. The description for this method confirms our understanding:

#+begin_quote
Removes a key-value pair from /hsh/ and returns it as the two-item array ~[ *key, value* ]~, or the hash’s default value if the hash is empty.
#+end_quote

There are quite a few Ruby methods, and you're not expected to know them all. That's why knowing how to read the Ruby documentation is so important. If you don't know how a method works you can always check the docs.
