require('yaml')
MESSAGES = YAML.load_file('loan_calculator_messages.yml')

def monthly_payment(loan_amount, apr, loan_duration_in_years)
  monthly_interest_rate = apr / 12
  loan_duration_in_months = loan_duration_in_years * 12
  loan_amount * (monthly_interest_rate / (1 - (1 + monthly_interest_rate)**(-loan_duration_in_months)))
end

def prompt(message)
  puts ">>> #{MESSAGES[message]}"
end

def valid_integer?(num)
  /^\d+$/.match(num)
end

def valid_float?(num)
  /^\d*[.]\d*$/.match(num)
end

puts "Welcome!"

# main loop
loop do
  loan_amount = ''
  loop do
    prompt('loan_amount')
    loan_amount = gets.chomp

    if valid_integer?(loan_amount)
      break
    else
      prompt("valid_loan_amount")
    end
  end

  apr = ''
  loop do
    prompt('apr')
    apr = gets.chomp

    if valid_float?(apr)
      break
    else
      prompt("valid_apr")
    end
  end

  loan_duration_in_years = ''
  loop do
    prompt('loan_duration_in_years')
    loan_duration_in_years = gets.chomp

    if valid_integer?(loan_duration_in_years)
      break
    else
      prompt("valid_duration")
    end
  end

  monthly_payment = monthly_payment(loan_amount.to_i,
                                    apr.to_f,
                                    loan_duration_in_years.to_i)

  prompt('calculating')
  puts ">>> #{MESSAGES['monthly_payment']}: $#{monthly_payment.round(2)}/month."

  prompt('again')
  again = gets.chomp.downcase

  break unless again == 'y'
end

prompt('goodbye')
