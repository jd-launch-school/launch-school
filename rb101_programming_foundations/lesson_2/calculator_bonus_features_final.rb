require 'yaml'
MESSAGES = YAML.load_file('calculator_messages_final.yml')

def messages(message, lang=@language)
  MESSAGES[lang][message]
end

def prompt(key)
  message = messages(key)
  puts "=> #{message}"
end

def retrieve_name
  loop do
    name = gets.chomp

    return name if /^[A-Za-z]+$/.match?(name)

    prompt('invalid_name')
    prompt('ask_name')
  end
end

def retrieve_lang
  loop do
    lang = gets.chomp.downcase

    case lang
    when 'english', 'en'
      return 'english'
    when 'espanol', 'es', 'español'
      return 'espanol'
    else
      puts MESSAGES['valid_lang']
    end
  end
end

def valid_number?(num)
  only_digits = /\d/.match(num)
  negative_integer = /^-0*[1-9]+\d*$/.match(num)
  positive = /^\d*[.]?\d*$/.match(num)
  negative_float = /^-\d*[.]\d*[1-9]$/.match(num)
  negative_end_with_decimal = /^-\d*[1-9]\d*[.]$/.match(num)

  only_digits && (positive ||
                  negative_integer ||
                  negative_float ||
                  negative_end_with_decimal)
end

def retrieve_number
  loop do
    number = gets.chomp
    number_as_float = number.to_f

    if valid_number?(number)
      if number_as_float.to_i.to_s == number
        return number.to_i
      else
        return number_as_float
      end
    else
      prompt("invalid_number")
    end
  end
end

def retrieve_operator
  loop do
    operator = gets.chomp

    return operator if %(1 2 3 4).include?(operator)

    prompt('invalid_operator')
  end
end

def retrieve_operator_no_divide
  loop do
    operator = gets.chomp

    return operator if %(1 2 3).include?(operator)

    prompt('invalid_operator_no_divide')
  end
end

def calculate_again?
  loop do
    again = gets.chomp.downcase

    return true if %w(y yes Y Yes s si).include?(again)
    return false if %w(n no N No).include?(again)

    prompt('invalid_calculate_again')
  end
end

# Displays welcome message
system("clear") || system("cls")
puts MESSAGES['welcome']

# Gets preferred language and displays language choice
@language = retrieve_lang
puts "=> #{messages('language')} #{@language.capitalize}."

prompt('press_enter')
gets.chomp

# Gets user's name and displays hello message
system("clear") || system("cls")

prompt('ask_name')
name = retrieve_name
puts "=> #{messages('hello')} #{name.capitalize}!"

prompt('press_enter')
gets.chomp

# Main Loop
loop do
  system("clear") || system("cls")

  # Get Numbers
  prompt('first_number')
  first_number = retrieve_number

  prompt('second_number')
  second_number = retrieve_number

  # Get Operator
  second_number_zero = second_number.to_f == 0.0

  if second_number_zero
    prompt('operator_prompt_no_divide')
  else
    prompt('operator_prompt')
  end

  operator = if second_number_zero
               retrieve_operator_no_divide
             else
               retrieve_operator
             end

  operation_message = case operator
                      when '1'
                        messages('adding')
                      when '2'
                        messages('subtracting')
                      when '3'
                        messages('multiplying')
                      when '4'
                        messages('dividing')
                      end
  puts "=> #{operation_message} #{messages('the_two_numbers')}"

  # Get Result
  result = case operator
           when '1'
             first_number + second_number
           when '2'
             first_number - second_number
           when '3'
             first_number * second_number
           when '4'
             first_number / second_number
           end

  result = [0, 0.0, -0, -0.0].include?(result) ? 0 : result

  puts "=> #{messages('result')} #{result}."

  # Another Calculation?
  prompt('calculate_again')
  break unless calculate_again?
end

# Displays goodbye message
prompt('goodbye')
