require 'yaml'
MESSAGES = YAML.load_file('calculator_messages2.yml')
LANGUAGE = 'en'

def messages(message, lang=LANGUAGE)
  MESSAGES[lang][message]
end

def prompt(key)
  message = messages(key, LANGUAGE)
  Kernel.puts("=> #{message}")
end

def valid_number?(num)
  num.to_i != 0
end

def operation_to_message(operator)
  case operator
  when '1'
    messages("adding")
  when '2'
    messages('subtracting')
  when '3'
    messages('multiplying')
  when '4'
    messages('dividing')
  end
end

prompt("welcome")

name = ''
loop do
  name = Kernel.gets().chomp()

  if name.empty?()
    prompt("valid_name")
  else
    break
  end
end

Kernel.puts("#{messages("hello")} #{name}!")

loop do # main loop
  number1 = ''
  loop do
    prompt("first_number")
    number1 = Kernel.gets().chomp()

    if valid_number?(number1)
      break
    else
      prompt("valid_number")
    end
  end

  number2 = ''
  loop do
    prompt("second_number")
    number2 = Kernel.gets().chomp()

    if valid_number?(number2)
      break
    else
      prompt("valid_number")
    end
  end

  prompt("operator_prompt")

  operator = ''
  loop do
    operator = Kernel.gets().chomp()

    if %w(1 2 3 4).include?(operator)
      break
    else
      prompt("valid_operator")
    end
  end

  Kernel.puts("#{operation_to_message(operator)} #{messages("the_two_numbers")}")

  result = case operator
           when '1'
             number1.to_i() + number2.to_i()
           when '2'
             number1.to_i() - number2.to_i()
           when '3'
             number1.to_i() * number2.to_i()
           when '4'
             number1.to_f() / number2.to_f()
           end

  Kernel.puts("#{messages("result")} #{result}.")

  prompt("again")
  answer = Kernel.gets().chomp()
  break unless answer.downcase.start_with?('y')
end

Kernel.puts(messages("goodbye"))
