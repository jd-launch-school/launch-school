VALID_PLAYER_CHOICES = %w(r rock R ROCK p paper P PAPER s scissors S SCISSORS
                          l lizard L LIZARD k spock K SPOCK q quit Q QUIT)
VALID_COMPUTER_CHOICES = %i(r p s l k)
CONVERSIONS = { r: 'rock', p: 'paper', s: 'scissors', l: 'lizard', k: 'spock' }
WINNING_COMBINATIONS = {
  r: [:s, :l],
  p: [:r, :k],
  s: [:p, :l],
  l: [:p, :k],
  k: [:r, :s]
}
WINNING_SCORE = 5

def prompt(message)
  puts "=> #{message}"
end

def display_header_prompt(score)
  display_score(score)
  prompt("")
  prompt("Choose one: (r)ock, (p)aper, (s)cissors, (l)izard, spoc(k)")
  prompt("You can also choose (q)uit.")
end

def input_to_symbol(input)
  input.match?(/spock/i) ? :k : input[0].downcase.to_sym
end

def retrieve_choice(score)
  loop do
    display_header_prompt(score)
    input = gets.chomp

    return input_to_symbol(input) if VALID_PLAYER_CHOICES.include?(input)

    prompt("That's not a valid choice.")
  end
end

def display_choices(player, computer)
  prompt("You chose #{CONVERSIONS[player]};" \
         " Computer chose #{CONVERSIONS[computer]}.")
end

def win?(first, second)
  WINNING_COMBINATIONS[first].include?(second)
end

def results(player, computer)
  if win?(player, computer)
    ['You Win!', :player]
  elsif win?(computer, player)
    ['Computer Wins!', :computer]
  else
    ["It's a Tie!"]
  end
end

def increment_score(winner, score)
  score[winner] += 1
end

def display_score(score)
  prompt("Score: Player [#{score[:player]}], Computer [#{score[:computer]}]")
end

def reset_score(score)
  score[:player] = 0
  score[:computer] = 0
end

def check_score?(score, player)
  score[player] == WINNING_SCORE
end

def grand_winner?(score)
  check_score?(score, :player) || check_score?(score, :computer)
end

def display_grand_winner(score)
  message = "#{check_score?(score, :player) ? 'You are' : 'Computer is'}" \
            " the GRAND WINNER!"

  prompt("*" * message.length)
  prompt(message)
  prompt("*" * message.length)
  prompt("")
end

def play_again
  loop do
    input = gets.chomp
    answer = input.downcase

    return answer if %w(y yes n no).include?(answer)

    prompt("You must choose either '(y)es' or '(n)o'.")
  end
end

def clear
  system("clear") || system("cls")
end

def continue
  prompt('Press ENTER to continue.')
  gets.chomp
end

clear
prompt("Welcome to Rock, Paper, Scissors, Lizard, Spock!")
prompt("")
prompt("The first player to get #{WINNING_SCORE} wins will" \
       " be the Grand Winner.")
prompt("")
continue

loop do
  score = { player: 0, computer: 0 }

  loop do
    clear

    choice = retrieve_choice(score)
    break if choice == :q

    computer_choice = VALID_COMPUTER_CHOICES.sample
    win_message, winner = results(choice, computer_choice)

    display_choices(choice, computer_choice)
    increment_score(winner, score) if winner
    prompt(win_message)

    grand_winner?(score) ? break : continue
  end

  break unless grand_winner?(score)

  prompt("")
  display_grand_winner(score)
  reset_score(score)

  prompt("Do you want to play again? Choose: '(y)es' or '(n)o'")
  break unless ['y', 'yes'].include?(play_again)
end

prompt("Goodbye. Thanks for playing! See you next time.")
