YR_TO_MONTH = 12
MAX_YEARS = 30
MAX_MONTHS = 360

require 'yaml'
MESSAGES = YAML.load_file('loan_calculator_final.yml')

def messages(message, lang=$language)
  MESSAGES[lang][message]
end

def prompt(key)
  message = messages(key)
  puts "=> #{message}"
end

def retrieve_name
  loop do
    name = gets.chomp

    return name if /^[A-Za-z]+$/.match?(name)

    prompt('invalid_name')
    prompt('ask_name')
  end
end

def retrieve_lang
  loop do
    lang = gets.chomp.downcase

    case lang
    when 'english', 'en'
      return 'english'
    when 'espanol', 'es', 'español'
      return 'espanol'
    when 'chinese', 'ch'
      return 'chinese'
    else
      puts MESSAGES['valid_lang']
    end
  end
end

def valid_amount?(num)
  without_commas = /^[1-9]\d*[.]?\d{0,2}$/.match?(num)
  with_commas = /^[1-9]\d{0,2}([,]\d{3})*([.]\d{0,2})?$/.match?(num)

  without_commas || with_commas
end

def retrieve_loan_amount
  loop do
    loan_amount = gets.chomp

    if valid_amount?(loan_amount)
      return loan_amount.delete(",").to_f
    else
      prompt('invalid_loan_amount')
    end
  end
end

def valid_apr?(num)
  greater_than_equal_to_one = /^[1-9]\d?[.]?\d*%?$/.match(num)
  between_zero_and_one = /(^[.]|^0[.])\d{1,4}%?$/.match(num)
  zero = /^0%?$/.match?(num)

  (greater_than_equal_to_one || between_zero_and_one || zero) &&
    num.to_f <= 99.9999
end

def retrieve_apr
  loop do
    apr = gets.chomp
    apr_as_float = apr.to_f

    if valid_apr?(apr)
      return apr_as_float >= 1 ? apr_as_float / 100 : apr_as_float
    else
      prompt('invalid_apr')
    end
  end
end

def valid_integer?(num)
  num_as_int = num.to_i

  num_as_int.to_s == num && num_as_int > 0 && num_as_int <= 360
end

def valid_year?(num)
  num.to_i <= MAX_YEARS
end

def valid_month?(num)
  num.to_i <= MAX_MONTHS
end

def ends_with_year?(input)
  input.downcase.end_with?("years", "year", "yr", "y",
                           "anos", "ano", "a", "año", "años")
end

def ends_with_month?(input)
  input.downcase.end_with?("months", "month", "mo", "m", "mes", "meses")
end

def determine_duration(duration, label)
  loop do
    if ends_with_month?(label)
      return [duration, "m"]
    elsif ends_with_year?(label)
      return [duration, "y"]
    else
      prompt('invalid_label')
    end

    label = gets.chomp
  end
end

def parse_duration(duration)
  if ends_with_month?(duration) || ends_with_year?(duration)
    duration, label = duration.partition(/[A-Za-z]/)
    determine_duration(duration, label)
  else
    puts "=> #{messages('you_entered')} #{duration}."
    prompt('clarify_duration')
    label = gets.chomp
    determine_duration(duration.to_i, label)
  end
end

def valid_duration?(num)
  regex = %r{
    ^[1-9]\d*\s?
    (months|years|month|year|m|y|mo|yr|mes|meses|a|ano|anos)$
  }ix

  valid_integer?(num) ||
    (regex.match?(num) &&
      ((ends_with_year?(num) && valid_year?(num)) ||
        (ends_with_month?(num) && valid_month?(num))))
end

def duration_as_months(duration)
  duration.to_i * YR_TO_MONTH
end

def retrieve_loan_duration
  loop do
    loan_duration = gets.chomp

    if valid_duration?(loan_duration)
      duration, label = parse_duration(loan_duration)

      return duration.to_i if label == 'm'
      return duration_as_months(duration) if valid_year?(duration)

      prompt('invalid_loan_duration_years')
      prompt('loan_duration')

    else
      prompt('invalid_loan_duration')
    end
  end
end

def continue_prompt
  prompt('continue_prompt')
  gets.chomp
end

def clear_screen
  system("clear") || system("cls")
end

def calculate(loan_amount, monthly_interest_rate, loan_duration_in_months)
  if monthly_interest_rate == 0.0
    loan_amount / loan_duration_in_months
  else
    loan_amount * (monthly_interest_rate /
      (1 - (1 + monthly_interest_rate)**(-loan_duration_in_months)))
  end
end

def calculate_again?
  loop do
    again = gets.chomp.downcase

    return true if %w(y yes Y Yes s si S Si).include?(again)
    return false if %w(n no N No).include?(again)

    prompt('invalid_calculate_again')
  end
end

clear_screen
puts MESSAGES['welcome']

$language = retrieve_lang
puts "=> #{messages('language')} #{$language.capitalize}."
continue_prompt

clear_screen
prompt('ask_name')
name = retrieve_name
puts "=> #{messages('hello')} #{name.capitalize}!"
continue_prompt

loop do
  clear_screen

  prompt('loan_amount')
  loan_amount = retrieve_loan_amount

  prompt('apr')
  apr = retrieve_apr
  monthly_interest_rate = apr / 12

  prompt('loan_duration')
  loan_duration_in_months = retrieve_loan_duration

  monthly_payment = calculate(loan_amount,
                              monthly_interest_rate,
                              loan_duration_in_months)

  prompt('calculating')
  puts "=> #{messages('monthly_payment')} " \
       "$#{format('%#.2f', monthly_payment)} #{messages('per_month')}."

  prompt('calculate_again')
  break unless calculate_again?
end

prompt('goodbye')
