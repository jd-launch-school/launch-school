VALID_CHOICES = %w(r rock p paper s scissors)
CONVERSIONS = { r: 'rock', p: 'paper', s: 'scissors' }

def prompt(message)
  puts "=> #{message}"
end

def retrieve_choice
  loop do
    prompt("Choose one: (r)ock, (p)aper, (s)cissors")
    choice = gets.chomp

    return choice[0].to_sym if VALID_CHOICES.include?(choice)

    prompt("That's not a valid choice.")
  end
end

def display_results(player, computer)
  if (player == :r && computer == :s) ||
     (player == :p && computer == :r) ||
     (player == :s && computer == :p)
    prompt('You Win!')
  elsif player == computer
    prompt("It's a Tie!")
  else
    prompt("Computer Wins!")
  end
end

def play_again
  loop do
    prompt("Do you want to play again? Choose: 'y' or 'n'")
    answer = gets.chomp.downcase

    return answer if %w(y n).include?(answer)

    prompt("You must choose either 'y' or 'n'")
  end
end

def clear
  system("clear") || system("cls")
end

loop do
  clear
  choice = retrieve_choice

  computer_choice = [:r, :p, :s].sample

  prompt("You chose #{CONVERSIONS[choice]}; Computer chose #{CONVERSIONS[computer_choice]}.")
  display_results(choice, computer_choice)

  break unless play_again == 'y'
end

prompt("Goodbye. Thanks for playing! See you next time.")
