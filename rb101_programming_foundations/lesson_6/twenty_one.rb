VALUES = %w(2 3 4 5 6 7 8 9 10 j q k a)
SUITS = %w(◆ ♣ ♥ ♠)
WINNING_SCORE = 21
DEALER_HITS_UNTIL = 17
MIN_CARDS_IN_DECK = 10

def prompt(message)
  puts "=> #{message}"
end

def clear_screen
  system('clear') || system('cls')
end

def header(bank = { balance: 0, bet: 0 })
  clear_screen
  puts "Welcome to Twenty-One!"
  puts "Try to beat the dealer without going over #{WINNING_SCORE}."
  puts "Current Balance: $#{format_amount(bank[:balance])} | " \
       "Current Bet: $#{format_amount(bank[:bet])}"
  puts ''
end

# :reek:TooManyStatements
def display_game(bank, hands, hidden)
  header(bank)
  hand_lines = initialize_hand_lines

  hands.each do |player, hand|
    build_hands!(player, hand, hidden, hand_lines)

    display_hand_header(player, hand, hidden)
    display_hand(hand_lines)

    hand_lines = initialize_hand_lines
  end
end

# rubocop:disable Metrics/MethodLength
def instructions
  clear_screen
  puts <<~INSTRUCTIONS
    BASIC INSTRUCTIONS
    ==================
    Card values:
      Number cards (2-10): the value on the card
      Face cards (j, q, k): value of 10
      Ace (a): either 1 or 11

    The goal is to get one's hand as close to the value of 21 as possible
    without going over 21, by adding up the value of the cards.

    Each player is dealt two cards. The player goes first and can either
    `hit` or `stay`.

    A `hit` adds a card to the player's hand. Choosing to `stay` passes the
    turn to the dealer. The dealer must `hit` until the value of their cards
    is greater than or equal to 17.

    At the end of both turns, whomever has the hand with the highest value wins!

    See https://en.wikipedia.org/wiki/Blackjack for more info.

  INSTRUCTIONS

  prompt "Press ENTER to continue..."
  gets
end
# rubocop:enable Metrics/MethodLength

def initialize_deck
  deck = SUITS.product(VALUES).map do |card|
    { suit: card.first, value: card.last }
  end

  deck.shuffle
end

def initialize_hands
  { dealer: [], player: [] }
end

def initialize_bank
  { balance: 0, bet: 0 }
end

def border_line
  " -------- "
end

def suit_line(suit)
  "| #{suit}   #{suit}  |"
end

def blank_line
  "|        |"
end

def value_line(value)
  value == '10' ? "|   #{value}   |" : "|   #{value.upcase}    |"
end

def initialize_hand_lines
  { border: '', suit: '', blank: '', value: '' }
end

def add_card!(card, hand_lines)
  hand_lines[:border] << border_line
  hand_lines[:suit] << suit_line(card[:suit])
  hand_lines[:blank] << blank_line
  hand_lines[:value] << value_line(card[:value])
end

# :reek:FeatureEnvy
def add_hidden_card!(hand_lines)
  hand_lines[:border] << border_line
  hand_lines[:suit] << "|░░░░░░░░|"
  hand_lines[:blank] << "|░░░░░░░░|"
  hand_lines[:value] << "|░░░░░░░░|"
end

def build_hand_with_hidden_card!(hand, hand_lines)
  add_hidden_card!(hand_lines)
  add_card!(hand.last, hand_lines)
end

def build_hand!(hand, hand_lines)
  hand.each { |card| add_card!(card, hand_lines) }
end

# :reek:ControlParameter
def hide_dealer_card?(player, hidden)
  (player == :dealer) && hidden
end

# :reek:LongParameterList
def build_hands!(player, hand, hidden, hand_lines)
  if hide_dealer_card?(player, hidden)
    build_hand_with_hidden_card!(hand, hand_lines)
  else
    build_hand!(hand, hand_lines)
  end
end

def display_hand_header(player, hand, hidden)
  player = player.capitalize

  if hide_dealer_card?(player, hidden)
    puts "#{player} Hand: "
  else
    puts "#{player} Hand: #{score(hand)}"
  end
end

# :reek:FeatureEnvy
def display_hand(hand_lines)
  border = hand_lines[:border]
  suit = hand_lines[:suit]
  blank = hand_lines[:blank]

  puts border, suit, blank, hand_lines[:value], blank, suit, border
  puts ''
end

# :reek:TooManyStatements
def shuffle_cards!(deck, bank)
  header(bank)
  num_cards = deck.size

  prompt "Only #{num_cards} cards remaining in deck."
  print "Shuffling Deck..."
  sleep(2)

  deck.replace(initialize_deck)
end

# :reek:DuplicateMethodCall
def deal(deck, hands)
  2.times do
    hands[:player] << deck.shift
    hands[:dealer] << deck.shift
  end
end

def ace_value(score)
  score >= 11 ? 1 : 11
end

def ace?(value)
  value == 'a'
end

def digit(value)
  value.match?(/\d/) ? value.to_i : 10
end

def sort_hand(hand)
  hand.sort_by { |card| VALUES.find_index(card[:value]) }
end

def score(hand)
  sort_hand(hand).reduce(0) do |result, card|
    value = card[:value]
    result + (ace?(value) ? ace_value(result) : digit(value))
  end
end

# :reek:TooManyStatements
def blackjack?(hands)
  hands.map do |player, hand|
    ace = /[a]/
    ten = /[10jqk]/

    first_card = hand.first[:value]
    second_card = hand.last[:value]

    blackjack_check = first_card.match?(ace) &&
                      second_card.match?(ten)

    if player == :dealer
      blackjack_check
    else
      blackjack_check || (first_card.match?(ten) && second_card.match?(ace))
    end
  end
end

def hit(deck, hand)
  hand << deck.shift
end

# :reek:TooManyStatements
def player_turn(deck, hands, bank)
  player_hand = hands[:player]

  loop do
    prompt "What do you want to do? [ (h)it | (s)tay ]"
    answer = player_choice(%i(h hit s stay))

    hit(deck, player_hand) if answer == :h
    display_game(bank, hands, true)

    break if answer == :s || score(player_hand) > WINNING_SCORE
  end
end

# :reek:TooManyStatements
def dealer_turn(deck, hands, bank)
  dealer_hand = hands[:dealer]

  loop do
    score = score(dealer_hand)

    break if (score >= DEALER_HITS_UNTIL) || (score > WINNING_SCORE)

    hit(deck, dealer_hand)
    display_game(bank, hands, false)
    sleep(1)
  end
end

def someone_bust?(hands)
  result = false
  hands.each { |_, hand| result = true if score(hand) > WINNING_SCORE }
  result
end

# :reek:TooManyStatements
def determine_winner(hands, blackjacks)
  player_score = score(hands[:player])
  dealer_score = score(hands[:dealer])

  if someone_bust?(hands)
    player_score > WINNING_SCORE ? :dealer : :player

  elsif blackjacks.last || player_score > dealer_score
    :player

  elsif player_score == dealer_score
    :tie

  else
    :dealer
  end
end

# :reek:TooManyStatements
def display_winner(winner, blackjacks, hands)
  loser = winner == :player ? :dealer : :player

  result = ''
  result = "Blackjack! " if blackjacks.any?

  if winner == :tie
    prompt "It's a tie!"
  elsif someone_bust?(hands)
    prompt "#{loser.capitalize} busts with #{score(hands[loser])}!"
  else
    prompt "#{result}#{winner.capitalize} wins with #{score(hands[winner])}!"
  end
end

def display_winnings(result, amount)
  prompt "You #{result} $#{format_amount(amount)}."
end

def increment_balance(bank, amount)
  bank[:balance] += amount
end

def decrement_balance(bank, bet)
  bank[:balance] -= bet
end

def player_wins(bank, blackjacks)
  bet = bank[:bet]
  winning_amount = blackjacks.any? ? (bet * 1.5).to_i : bet
  increment_balance(bank, winning_amount)
  display_winnings('win', winning_amount)
end

def dealer_wins(bank)
  bet = bank[:bet]
  decrement_balance(bank, bet)
  display_winnings('lose', bet)
end

def choices_for_prompt(choices)
  choices.each_with_object([]) do |choice, prompt_choices|
    first_letter = choice[0]
    remaining_letters = choice[1..-1]
    prompt_choices << "(#{first_letter})#{remaining_letters}" if choice.size > 1
  end
end

# :reek:TooManyStatements
def player_choice(choices)
  loop do
    player_choice = gets.chomp.downcase.to_sym
    return player_choice[0].to_sym if choices.include?(player_choice)

    choices_for_prompt = choices_for_prompt(choices).join(' | ')

    prompt 'Not a valid choice.'
    prompt "Please choose [ #{choices_for_prompt} ]."
  end
end

def ask_player?(message, choices, result)
  word_choices = choices.filter { |choice| choice.length > 1 }
  prompt "#{message} [ #{choices_for_prompt(word_choices).join(' | ')} ]"
  player_choice(choices) == result
end

def valid_amount?(num)
  without_commas = /^[1-9]\d*$/.match?(num)
  with_commas = /^[1-9]\d{0,2}([,]\d{3})*$/.match?(num)

  without_commas || with_commas
end

def retrieve_amount
  loop do
    amount = gets.chomp

    if valid_amount?(amount)
      return amount.delete(",").to_i
    else
      prompt "The amount must be a positive number greater than $1.
=> It can be precise to the nearest dollar (0 decimal places).
=> Example: 1,000    (valid)
=>          1000     (valid)
=>          1000.3   (not valid)
=>          1000.35  (not valid)
=>          1000.350 (not valid)"
    end
  end
end

def valid_bet?(balance)
  loop do
    bet = retrieve_amount

    if (balance - bet) < 0
      prompt "You don't have enough money to place this bet."
      prompt "Place another bet."
    else
      return bet
    end
  end
end

def retrieve_balance!(bank)
  prompt 'What would you like your starting balance to be in dollars?'
  bank[:balance] = retrieve_amount
end

def retrieve_bet!(bank)
  prompt 'How much would you like to bet?'
  bank[:bet] = valid_bet?(bank[:balance])
end

def format_amount(amount)
  amount.to_s.reverse.gsub(/(\d{3})(?=\d)/) { |digits| "#{digits}," }.reverse
end

def display_balance(bank, starting_balance)
  puts ''
  prompt "Current Balance: $#{format_amount(bank[:balance])} | " \
         "Starting Balance: $#{format_amount(starting_balance)}"
end

# :reek:TooManyStatements
# :reek:DuplicateMethodCall
def display_results(starting_balance, balance)
  won_or_lost = balance - starting_balance > 0 ? 'won' : 'lost'
  results = (starting_balance - balance).abs

  puts ''
  prompt "Overall Results:"
  prompt "----------------"
  prompt "You started with $#{format_amount(starting_balance)}."
  prompt "You cashed out $#{format_amount(balance)}."

  if results == 0
    prompt "You broke even."
  else
    prompt "You #{won_or_lost} $#{format_amount(results)}!"
  end

  puts ''
end

def player_blackjack_or_bust?(hands, blackjacks)
  score(hands[:player]) > WINNING_SCORE || blackjacks.last
end

def game_round(deck, bank, hands)
  player_turn(deck, hands, bank)
  unless score(hands[:player]) > WINNING_SCORE
    display_game(bank, hands, false)
    sleep(1)
    dealer_turn(deck, hands, bank)
  end
end

header

instructions if ask_player?('Do you want to see the instructions?',
                            %i(y yes n no),
                            :y)
loop do
  deck = initialize_deck
  bank = initialize_bank

  header(bank)
  retrieve_balance!(bank)
  starting_balance = bank[:balance]

  loop do
    hands = initialize_hands
    header(bank)
    retrieve_bet!(bank)
    deal(deck, hands)

    display_game(bank, hands, true)

    blackjacks = blackjack?(hands)
    game_round(deck, bank, hands) unless blackjacks.any?

    if player_blackjack_or_bust?(hands, blackjacks)
      display_game(bank, hands, true)
    else
      display_game(bank, hands, false)
    end

    winner = determine_winner(hands, blackjacks)
    display_winner(winner, blackjacks, hands)

    player_wins(bank, blackjacks) if winner == :player
    dealer_wins(bank) if winner == :dealer

    bank[:bet] = 0

    break prompt 'You lost all of your money!' if bank[:balance] == 0

    display_balance(bank, starting_balance)
    break unless ask_player?('Do you want to play another hand?',
                             %i(y yes n no),
                             :y)

    shuffle_cards!(deck, bank) if deck.size <= MIN_CARDS_IN_DECK
  end

  display_results(starting_balance, bank[:balance])
  break unless ask_player?('What do you want to do?',
                           %i(q quit p play),
                           :p)
end

prompt 'Thanks for playing Twenty-One! Goodbye.'
