* RB101 - Lesson 6
This code was written for the Launch School 101 course, Lesson 6.

** Tic-Tac-Toe
Tic-Tac-Toe is a game that's played on a board that looks like a hash: `#`

[[https://gitlab.com/jd-launch-school/launch-school/blob/master/rb101_programming_foundations/lesson_6/tictactoe_bonus_features.rb][Code]] | [[https://gitlab.com/jd-launch-school/launch-school/blob/master/rb101_programming_foundations/lesson_6/tictactoe_bonus_features.org][Documentation]]

** Twenty-One
Twenty-one is a game that's also known as Blackjack.

Before the hand starts, the player enters the balance of money they want to play with (think of converting cash into chips at a casino) and the amount they want to bet for the upcoming hand.

When the cards are dealt, the player tries to get card values that are as close to 21 as possible. If the player or dealer goes over 21, it's considered a bust, and they lose. If either player gets 21 with the first two cards dealt, this is called a natural Blackjack.

If the player gets a natural Blackjack, they win 1.5x their bet. Otherwise, if the player wins, they win 1x their bet.

The ultimate goal of the game is for the player to stop playing with more money than they started with.

[[https://gitlab.com/jd-launch-school/launch-school/blob/master/rb101_programming_foundations/lesson_6/twenty_one.rb][Code]] | [[https://gitlab.com/jd-launch-school/launch-school/blob/master/rb101_programming_foundations/lesson_6/twenty_one.org][Documentation]]

*** Discussion
The main deck is an array of hashes representing cards. Each card hash contains ~value~ and ~suit~ key/value pairs. This makes it easier to track individual cards as needed and instead of accessing nested arrays three or four levels deep, many times I only needed to access the value like ~hand[:value]~.

The program will reshuffle the deck when the number of cards remaining in the deck is less than or equal to 10. This is an arbitrary number but it seems to work for most cases.

When determining a value for an ace, the program first sorts the hand so that the ace cards are at the end of the array. This way, the digits and face cards are calculated first and then the value of the ace is determined to be either one or eleven.

For betting, the program uses a ~bank~ hash containing ~balance~ and ~bet~ key/value pairs. When the player wins, the ~balance~ is incremented as indicated above. When the player loses, the ~balance~ is decremented by the bet amount. The player can only place a bet that is less than or equal to the current balance.

*** Concerns
**** Repeated code
I feel like there may be some (too much?) repeated code. I worked hard to make sure methods did as little as possible, ideally only one thing. However, I found it difficult to eliminate repetition entirely. I tried to avoid code smells like Feature Envy and Control Parameter wherever possible. I also tried to avoid unnecessary redirection. Avoiding the Control Parameter smell looks like the following.

Instead of a method like this:

#+begin_src ruby :results output :exports both
def display_game(bank, hands, hidden)
  header(bank)

  if hidden
    display_hidden_dealer_card(hands[:dealer])
  else
    display_all_cards(hands[:dealer], :dealer)
  end

  display_all_cards(hands[:player], :player)
end
#+end_src

I broke this into two methods like the following and called each method when needed throughout the program:

#+begin_src ruby :results output :exports both
def display_game(bank, hands)
  header(bank)
  display_all_cards(hands[:dealer], :dealer)
  display_all_cards(hands[:player], :player)
end
#+end_src

#+begin_src ruby :results output :exports both
def display_game_hidden_card(bank, hands)
  header(bank)
  display_hidden_dealer_card(hands[:dealer])
  display_all_cards(hands[:player], :player)
end
#+end_src
