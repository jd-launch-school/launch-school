INITIAL_MARKER = ' '
PLAYER_MARKER = 'X'
COMPUTER_MARKER = 'O'
WINNING_LINES = [[1, 2, 3], [4, 5, 6], [7, 8, 9],
                 [1, 4, 7], [2, 5, 8], [3, 6, 9],
                 [1, 5, 9], [3, 5, 7]]
WINNING_SCORE = 5
FIRST_MOVE = :choose

def prompt(message)
  puts "=> #{message}"
end

def clear_screen
  system('clear') || system('cls')
end

def header_message(score)
  clear_screen
  puts "You're #{PLAYER_MARKER}. Computer is #{COMPUTER_MARKER}."
  puts 'Try to win 5 games to be Grand Winner!'
  puts "Score ---> #{score_as_string(score)}"
end

# :reek:UtilityFunction
def initialize_board
  new_board = {}
  (1..9).each { |num| new_board[num] = INITIAL_MARKER }
  new_board
end

# rubocop:disable Metrics/AbcSize
# :reek:TooManyStatements
# :reek:DuplicateMethodCall
def display_board(board, score)
  header_message(score)
  puts ""
  puts "     |     |"
  puts "  #{board[1]}  |  #{board[2]}  |  #{board[3]}"
  puts "     |     |"
  puts "-----+-----+-----"
  puts "     |     |"
  puts "  #{board[4]}  |  #{board[5]}  |  #{board[6]}"
  puts "     |     |"
  puts "-----+-----+-----"
  puts "     |     |"
  puts "  #{board[7]}  |  #{board[8]}  |  #{board[9]}"
  puts "     |     |"
  puts ""
end
# rubocop:enable Metrics/AbcSize

# :reek:UtilityFunction
# :reek:DuplicateMethodCall
def joiner(available_squares, sep = ', ', word = 'or')
  case available_squares.size
  when (3..)
    "#{available_squares[0..-2].join(sep)}#{sep}#{word}" \
    " #{available_squares.last}"
  when 2
    "#{available_squares.first} #{word} #{available_squares.last}"
  else
    available_squares.first.to_s
  end
end

# :reek:UtilityFunction
def empty_squares(board)
  board.keys.select { |num| board[num] == INITIAL_MARKER }
end

def retrieve_first_player
  puts ''
  prompt 'Who goes first?'
  prompt '[ (c)omputer | (p)layer ]'

  player_choice(%i(c computer p player)) == :c ? :computer : :player
end

def determine_first_player(first_player)
  return first_player if FIRST_MOVE == first_player

  first_player.empty? ? retrieve_first_player : first_player
end

# :reek:UtilityFunction
# :reek:ControlParameter
def alternate_player(player)
  player == :computer ? :player : :computer
end

def game_over?(board)
  someone_won?(board) || board_full?(board)
end

def set_current_player(board, first_player, current_player)
  if game_over?(board)
    first_player
  else
    alternate_player(current_player)
  end
end

# :reek:TooManyStatements
def player_next_move(board)
  available_squares = empty_squares(board)

  loop do
    prompt "Choose a square (#{joiner(available_squares)}):"
    square = gets.chomp.to_i

    return square if available_squares.include?(square)

    prompt "Sorry, that's not a valid choice."
  end
end

# :reek:UtilityFunction
def position_values(board, line)
  line.map { |pos| board[pos] }
end

# :reek:UtilityFunction
def two_markers_in_line?(line_values, marker)
  line_values.include?(INITIAL_MARKER) && line_values.count(marker) == 2
end

# :reek:UtilityFunction
def empty_square_index(line_values)
  line_values.find_index(INITIAL_MARKER)
end

def potential_winning_line(board, marker)
  WINNING_LINES.find do |line|
    two_markers_in_line?(position_values(board, line), marker)
  end
end

def best_move(board, marker)
  potential_win = potential_winning_line(board, marker)

  if potential_win
    line_values = position_values(board, potential_win)
    potential_win[empty_square_index(line_values)]
  end
end

# :reek:UtilityFunction
def open_square(board, square)
  square if board[square] == INITIAL_MARKER
end

def computer_next_move(board)
  open_square(board, 5) ||
    best_move(board, COMPUTER_MARKER) ||
    best_move(board, PLAYER_MARKER) ||
    open_square(board, 1) ||
    empty_squares(board).sample
end

def board_full?(board)
  empty_squares(board).empty?
end

# :reek:UtilityFunction
# :reek:TooManyStatements
# :reek:FeatureEnvy
def detect_winner(board)
  WINNING_LINES.each do |line|
    values = position_values(board, line)
    return :player if values.all?(PLAYER_MARKER)
    return :computer if values.all?(COMPUTER_MARKER)
  end

  nil
end

def someone_won?(board)
  !!detect_winner(board)
end

# :reek:UtilityFunction
def increment_score(winner, score)
  score[winner] += 1
end

# :reek:UtilityFunction
def score_as_string(score)
  "Player: #{score[:player]}, Computer: #{score[:computer]}"
end

# :reek:UtilityFunction
def reset_score(score)
  score[:player] = 0
  score[:computer] = 0
end

# :reek:UtilityFunction
def check_score?(score, player)
  score[player] == WINNING_SCORE
end

def grand_winner?(score)
  check_score?(score, :player) || check_score?(score, :computer)
end

# :reek:DuplicateMethodCall
# :reek:TooManyStatements
def display_grand_winner(score)
  message = "#{check_score?(score, :player) ? 'You are' : 'Computer is'}" \
            " the GRAND WINNER!"

  prompt("")
  prompt("*" * message.length)
  prompt(message)
  prompt("*" * message.length)
  prompt("")
end

# :reek:UtilityFunction
def choices_for_prompt(choices)
  choices.each_with_object([]) do |choice, prompt_choices|
    first_letter = choice[0]
    remaining_letters = choice[1..-1]
    prompt_choices << "(#{first_letter})#{remaining_letters}" if choice.size > 1
  end
end

# :reek:TooManyStatements
def player_choice(choices)
  loop do
    player_choice = gets.chomp.downcase.to_sym
    return player_choice[0].to_sym if choices.include?(player_choice)

    choices_for_prompt = choices_for_prompt(choices).join(' | ')

    prompt 'Not a valid choice.'
    prompt "Please choose [ #{choices_for_prompt} ]."
  end
end

def display_winner(board, score)
  if someone_won?(board)
    winner = detect_winner(board) == :computer ? "Computer" : "Player"
    increment_score(winner.downcase.to_sym, score)
    prompt "#{winner} won!"
  else
    prompt "It's a tie!"
  end
end

# :reek:TooManyStatements
# :reek:DuplicateMethodCall
def display_end_game_options(score)
  puts ''
  prompt "The current score is [#{score_as_string(score)}]"
  prompt 'Do you want to:'
  prompt '(q)uit the game'
  prompt '(r)eset the score and play again'

  if grand_winner?(score)
    player_choice(%i(quit q reset r))
  else
    prompt '(c)ontinue playing the current game'
    player_choice(%i(quit q continue c reset r))
  end
end

# :reek:LongParameterList
# :reek:TooManyStatements
def play_rounds(board, first_player, current_player, score)
  loop do
    display_board(board, score)

    if current_player == :computer
      board[computer_next_move(board)] = COMPUTER_MARKER
    else
      board[player_next_move(board)] = PLAYER_MARKER
    end

    current_player = set_current_player(board, first_player, current_player)
    break if game_over?(board)
  end
end

# :reek:TooManyStatements
def play_the_game_loop(score, first_player)
  loop do
    board = initialize_board
    current_player = first_player

    play_rounds(board, first_player, current_player, score)

    display_board(board, score)
    display_winner(board, score)

    break display_grand_winner(score) if grand_winner?(score)

    prompt 'Do you want to play another game? [ (y)es | (n)o ]'
    break unless player_choice(%i(yes y no n)) == :y
  end
end

score = { player: 0, computer: 0 }
first_player = ''

loop do
  header_message(score)
  first_player = determine_first_player(first_player)

  play_the_game_loop(score, first_player)

  answer = display_end_game_options(score)
  break if answer == :q

  if answer == :r
    reset_score(score)
    first_player = ''
  end
end

prompt 'Thanks for playing Tic Tac Toe! Goodbye.'
