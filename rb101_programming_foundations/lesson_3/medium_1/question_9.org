Question 9
RB101 > Lesson 3 > Practice Problems > Medium 1
https://launchschool.com/lessons/263069da/assignments/6eba600c

Consider these two simple methods:

#+begin_src ruby :results output :exports both
def foo(param = "no")
  "yes"
end

def bar(param = "no")
  param == "no" ? "yes" : "no"
end
#+end_src

What would be the return value of the following method invocation?

#+begin_src ruby :results output :exports both
bar(foo)
#+end_src

* My Solution
The return value is ~"no"~.

Calling ~bar(foo)~ calls the ~foo~ method with a default parameter of ~"no"~ . The ~foo~ method returns ~"yes"~ . The ~bar~ method assigns the ~param~ parameter to ~"yes"~ . The conditional within the ~bar~ method checks to see if ~param~ is equal to ~"no"~ . It's not, so the else clause is returned, which is ~"no"~.

* Recommended Solution
~"no"~

This is because the value returned from the ~foo~ method will always be ~"yes"~ , and ~"yes" == "no"~ will be ~false~.
