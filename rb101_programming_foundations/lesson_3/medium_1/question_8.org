Question 8
RB101 > Lesson 3 > Practice Problems > Medium 1
https://launchschool.com/lessons/263069da/assignments/6eba600c

Method calls can take expressions as arguments. Suppose we define a method called ~rps~ as follows, which follows the classic rules of rock-paper-scissors game, but with a slight twist that it declares whatever hand was used in the tie as the result of that tie.

#+begin_src ruby :results output :exports both
def rps(fist1, fist2)
  if fist1 == "rock"
    (fist2 == "paper") ? "paper" : "rock"
  elsif fist1 == "paper"
    (fist2 == "scissors") ? "scissors" : "paper"
  else
    (fist2 == "rock") ? "rock" : "scissors"
  end
end
#+end_src

What is the result of the following call?

#+begin_example ruby
puts rps(rps(rps("rock", "paper"), rps("rock", "scissors")), "rock")
#+end_example

* My Solution
Breaking this down, evaluating the inner ~rps~ calls first:

#+begin_src ruby :results output :exports both
rps("rock", "paper")
#+end_src

This returns ~paper~.

#+begin_src ruby :results output :exports both
rps("rock", "scissors")
#+end_src

This returns ~rock~ and leaves us with:

#+begin_src ruby :results output :exports both
puts rps(rps("paper", "rock"), "rock")
#+end_src

#+begin_src ruby :results output :exports both
rps("paper", "rock")
#+end_src

This returns ~paper~ and leaves us with:

#+begin_src ruby :results output :exports both
puts rps("paper", "rock")
#+end_src

This prints ~paper~ to the screen.

* Recommended Solution
~paper~

The outermost call is evaluated by determining the result of ~rps(rps("rock", "paper"), rps("rock", "scissors"))~ versus rock. In turn that means we need to evaluate the two separate results of ~rps("rock", "paper")~ and ~rps("rock", "scissors")~ and later combine them by calling ~rps~ on their individual results. Those innermost expressions will return ~"paper"~ and ~"rock"~ , respectively. Calling ~rps~ on that input will return ~"paper"~ . Which finally when evaluated against ~"rock"~ will return ~"paper"~.
