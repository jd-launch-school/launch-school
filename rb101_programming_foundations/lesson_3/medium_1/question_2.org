Question 2
RB101 > Lesson 3 > Practice Problems > Medium 1
https://launchschool.com/lessons/263069da/assignments/6eba600c

The result of the following statement will be an error:

#+begin_example ruby
puts "the value of 40 + 2 is " + (40 + 2)
#+end_example

Why is this and what are two possible ways to fix this?

* My Solution
This results in a ~TypeError~ because it's not possible to concatenate a ~String~ with an ~Integer~. One possible way to fix this is to use string interpolation. Another way is to convert the result of ~40 + 2~ into a ~String~ and then concatenate.

** String Interpolation
#+begin_src ruby :results output :exports both
puts "the value of 40 + 2 is #{40 + 2}"
#+end_src

#+RESULTS:
: the value of 40 + 2 is 42

** Convert to String First
#+begin_src ruby :results output :exports both
puts "the value of 40 + 2 is " + (40 + 2).to_s
#+end_src

#+RESULTS:
: the value of 40 + 2 is 42

* Recommended Solution
This will raise an error ~TypeError: no implicit conversion of Fixnum into String~ because ~(40+2)~ results in an integer and it is being concatenated to a string.

To fix this either call

#+begin_src ruby :results output :exports both
(40+2).to_s
#+end_src

or use string interpolation:
#+begin_src ruby :results output :exports both
puts "the value of 40 + 2 is #{40 + 2}"
#+end_src

