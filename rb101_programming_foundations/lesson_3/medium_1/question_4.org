Question 4
RB101 > Lesson 3 > Practice Problems > Medium 1
https://launchschool.com/lessons/263069da/assignments/6eba600c

Alyssa was asked to write an implementation of a rolling buffer. Elements are added to the rolling buffer and if the buffer becomes full, then new elements that are added will displace the oldest elements in the buffer.

She wrote two implementations saying, "Take your pick. Do you like ~<<~ or ~+~ for modifying the buffer?". Is there a difference between the two, other than what operator she chose to use to add an element to the buffer?

#+begin_src ruby :results output :exports both
def rolling_buffer1(buffer, max_buffer_size, new_element)
  buffer << new_element
  buffer.shift if buffer.size > max_buffer_size
  buffer
end

def rolling_buffer2(input_array, max_buffer_size, new_element)
  buffer = input_array + [new_element]
  buffer.shift if buffer.size > max_buffer_size
  buffer
end
#+end_src

* My Solution
~rolling_buffer1~ is a mutating method. ~buffer~ points to the ~buffer~ object that's passed into the method. ~Array#<<~ and ~Array#shift~ are mutating (destructive) methods, so they change ~buffer~ in the inner and outer scope.

~rolling_buffer2~, on the other hand, assigns ~buffer~ in the inner scope to the ~input_array~ concatenated with ~new_element~. Then, ~Array#shift~, a mutating method, is called on this new ~buffer~ object. This does not mutate the original ~input_array~.

* Recommended Solution
Yes, there is a difference. While both methods have the same return value, in the first implementation, the input argument called ~buffer~ will be modified and will end up being changed after ~rolling_buffer1~ returns. That is, the caller will have the input array that they pass in be different once the call returns. In the other implementation, ~rolling_buffer2~ will not alter the caller's input argument.
