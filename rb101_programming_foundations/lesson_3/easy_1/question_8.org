Question 8
RB101 > Lesson 3: Practice Problems > Easy 1
https://launchschool.com/lessons/263069da/assignments/e2593fe1

If we build an array like this:

#+begin_src ruby :results output :exports both
flintstones = ["Fred", "Wilma"]
flintstones << ["Barney", "Betty"]
flintstones << ["BamBam", "Pebbles"]
#+end_src

We will end up with this "nested" array:

#+begin_src ruby :results output :exports both
["Fred", "Wilma", ["Barney", "Betty"], ["BamBam", "Pebbles"]]
#+end_src

Make this into an un-nested array.

* My Solution
** Using ~Araray#flatten~
#+begin_src ruby :results output :exports both
flintstones = ["Fred", "Wilma"]
flintstones << ["Barney", "Betty"]
flintstones << ["BamBam", "Pebbles"]

p flintstones.flatten
#+end_src

#+RESULTS:
: ["Fred", "Wilma", "Barney", "Betty", "BamBam", "Pebbles"]

** Using ~each~ loop
#+begin_src ruby :results output :exports both
flintstones = ["Fred", "Wilma"]
flintstones << ["Barney", "Betty"]
flintstones << ["BamBam", "Pebbles"]

flattened_flintstones = []
flintstones.each { |item| (item.is_a? Array) ? item.each { |item| flattened_flintstones << item } : flattened_flintstones << item }
p flattened_flintstones
#+end_src

#+RESULTS:
: ["Fred", "Wilma", "Barney", "Betty", "BamBam", "Pebbles"]

* Recommended Solution
#+begin_src ruby :results output :exports both
flintstones.flatten!
#+end_src

Nesting data structures in this way is quite common in Ruby and programming in general. We will explore this in much greater depth in a future Lesson.
