Question 1
RB101 > Lesson 3: Practice Problems > Easy 1
https://launchschool.com/lessons/263069da/assignments/e2593fe1

What would you expect the code below to print out?

#+begin_example ruby
numbers = [1, 2, 2, 3]
numbers.uniq

puts numbers
#+end_example

* My Solution
It will print the original array since the original array is not mutated by ~numbers.uniq~. ~Array#uniq~ is a non-mutating (non-destructive) method.

#+begin_example
1
2
2
3
#+end_example

* Recommended Solution
It prints out

#+begin_example
1
2
2
3
#+end_example

~numbers.uniq~ returned a new ~Array~ object with unique elements, but it did not modify the ~numbers~ object. Further, the ~puts~ method automatically calls ~to_s~ on its argument, and that’s why you see the output like above.

Additional note: had the last line been ~p~ numbers instead, the output would have been ~[1, 2, 2, 3]~ because the ~p~ method automatically calls ~inspect~ on its argument, which gives a different formatting on the output. Furthermore, we could have also done puts ~numbers.inspect~ and the output would have been the same as using the ~p~ method.


