/* Get the Middle Character (7 kyu)
 * https://www.codewars.com/kata/56747fd5cb988479af000028/javascript
 *
 * You are going to be given a word. Your job is to return the middle
 * character of the word. If the word's length is odd, return the middle
 * character. If the word's length is even, return the middle 2 characters.
 *
 * # Input
 *
 * A word (string) of length 0 < str < 1000 (In javascript you may get
 * slightly more than 1000 in some test cases due to an error in the test
 * cases). You do not need to test for this. This is only here to tell you
 * that you do not need to worry about your solution timing out.
 *
 * # Output
 *
 * The middle character(s) of the word represented as a string.
 *
 * PROBLEM:
 * - INPUT: string
 * - OUTPUT: string
 * - RULES:
 *   - if length of word is odd, return middle character
 *   - if length of word is even, return two middle characters
 *
 * ALGORITHM:
 * - get the length of the word
 * - determine if the length is even or odd
 * - IF length is even
 *   - divide length of word by 2
 *   - subtract 1 from result
 *   - use slice starting from result of subtraction to one more
 *     result of division
 *   - return characters at both indexes
 * - ELSE
 *   - divide length of word by 2
 *   - round down to nearest integer
 *     + Math.floor
 *   - return the character at the index
 */

function getMiddle(s) {
  let wordLength = s.length;

  if (wordLength % 2 === 0) {
    let min = wordLength / 2 - 1;
    let max = wordLength / 2 + 1;
    return s.slice(min, max);
  } else {
    return s[Math.floor(wordLength / 2)];
  }
}

console.log(getMiddle("test") === "es");
console.log(getMiddle("testing") === "t");
console.log(getMiddle("middle") === "dd");
console.log(getMiddle("A") === "A");
console.log(getMiddle("Ab") === "Ab");
console.log(getMiddle("Abc") === "b");
console.log(getMiddle("/-0") === "-");
console.log(getMiddle("/ -0") === " -");
