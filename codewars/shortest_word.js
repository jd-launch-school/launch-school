/* Shortest Word (7 kyu)
 * https://www.codewars.com/kata/57cebe1dc6fdc20c57000ac9/train/javascript
 *
 * Given a string of words, return the length of the shortest word(s).
 * String will never be empty and you do not need to account for
 * different data types.
 *
 * PROBLEM
 * - INPUT: string
 * - OUTPUT: number
 *
 * ALGORITHM
 * - split the string into an array of words
 * - transform the array of strings into an array of word lengths
 * - return the minimum length
 */

function findShort(s){
  const LengthsArray = s.split(' ').map(word => word.length);
  let shortest = LengthsArray[0];

  LengthsArray.forEach(length =>{
    if (length < shortest) shortest = length;
  });

  return shortest;
}

// Using spread operator
// function findShort(s){
//   return Math.min(...s.split(' ').map(word => word.length));
// }

console.log(findShort("bitcoin take over the world maybe who knows perhaps") === 3);
console.log(findShort("turns out random test cases are easier than writing out basic ones") === 3);
console.log(findShort("Let's travel abroad shall we") === 2);
console.log(findShort("A let's travel abroad shall we") === 1);
console.log(findShort("Let's travel a abroad shall we") === 1);
