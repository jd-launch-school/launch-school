/* Highest and Lowest (7 kyu)
 * https://www.codewars.com/kata/554b4ac871d6813a03000035/javascript
 *
 * In this little assignment you are given a string of space separated
 * numbers, and have to return the highest and lowest number.
 *
 * Notes:
 * - All numbers are valid Int32, no need to validate them.
 * - There will always be at least one number in the input string.
 * - Output string must be two numbers separated by a single space,
 *   and highest number is first.
 *
 * PROBLEM:
 * - INPUT: string
 * - OUTPUT: string
 *
 * ALGORITHM:
 * - convert the string into an array
 * - convert each element of the array into a number
 * - declare `min` variable initialized to first element of the array
 * - declare `max` variable initialized to last element of the array
 * - iterate over the array starting at the second index thru second to last index
 *   - compare each num in array to min and max
 *     - IF num is greater than max, reassign max to num
 *     - IF num is less than min, reassign min to num
 * - return the string 'max min'
 */

function highAndLow(string) {
  const NumArray = string.split(' ').map(num => Number(num));
  let min = NumArray[0];
  let max = NumArray[NumArray.length - 1];

  NumArray.forEach(num => {
    if (num < min) min = num;
    if (num > max) max = num;
  });

  return `${max} ${min}`;
}

// Alternative Solution
//
// function highAndLow(string) {
//   const NumArray = string.split(' ').map(num => Number(num));
//
//   return `${Math.max(...NumArray)} ${Math.min(...NumArray)}`;
// }

console.log(highAndLow("1 2 3 4 5") === "5 1");
console.log(highAndLow("1 2 -3 4 5") === "5 -3");
console.log(highAndLow("1 9 3 4 -5") === "9 -5");
console.log(highAndLow("1") === "1 1");
