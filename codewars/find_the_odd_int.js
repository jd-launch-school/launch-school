/* Find the odd int (6 kyu)
 * https://www.codewars.com/kata/54da5a58ea159efa38000836/javascript
 *
 * Given an array of integers, find the one that appears an odd
 * number of times.
 *
 * There will always be only one integer that appears an odd number
 * of times.
 *
 * PROBLEM
 * - INPUT: array
 * - OUTPUT: number
 *
 * ALGORITHM
 * - declare 'counts' variable initialized to an empty object
 * - LOOP over the given array
 *   - IF the current number does not already exist as a key of the object
 *     - add it as a key with an initial value of 0
 *   - increment key's value by 1
 * - LOOP over the 'counts' object
 *   - IF the value of the current key is odd
 *     - RETURN the current key
 */

function findOdd(array) {
  const Counts = {};

  array.forEach(num => {
    if (!Counts[num]) Counts[num] = 0;
    Counts[num] += 1;
  });

  for (let num in Counts) {
    if (Counts[num] % 2 === 1) return Number(num);
  }
}

console.log(findOdd([20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5]) === 5);
console.log(findOdd([1,1,2,-2,5,2,4,4,-1,-2,5]) === -1);
console.log(findOdd([20,1,1,2,2,3,3,5,5,4,20,4,5]) === 5);
console.log(findOdd([10]) === 10);
console.log(findOdd([1,1,1,1,1,1,10,1,1,1,1]) === 10);
console.log(findOdd([5,4,3,2,1,5,4,3,2,10,10]) === 1);
