=begin
Count IP Addresses - 5 kyu
https://www.codewars.com/kata/526989a41034285187000de4/ruby

Implement a function that receives two IPv4 addresses, and returns the
number of addresses between them (including the first one, excluding
the last one).

All inputs will be valid IPv4 addresses in the form of strings. The
last address will always be greater than the first one.

PROBLEM:
- INPUT: Two Strings
  + each string will be a valid IPv4 address
  + the second address will always be greater than the first
- OUTPUT: Integer
  + represents number of addresses between first and second
  + include first, exclude second

ALGORITHM:
- initialize a result variable
- split each IP address on the period to create array of strings
- convert each string element to an integer
- reverse the order of the elements to start with lowest position
- LOOP through each element of the first IP address with the index
  + multiply the element at the index in the second IP address
    by 256 raised to the power of the index
  + multiple the current element in first IP address by 256
    raised to the power of the index
  + subtract second calculation from the first
  + add to the result variable
- return the value of the result variable
=end

def ips_between(start, ending)
  first = start.split('.').map(&:to_i).reverse
  second = ending.split('.').map(&:to_i).reverse

  result = 0

  first.each_with_index do |num, index|
    first_addresses = num * (256 ** index)
    second_addresses = second[index] * (256 ** index)
    result += second_addresses - first_addresses
  end

  result
end

p ips_between("10.0.0.0", "10.0.0.50") == 50
p ips_between("10.0.0.0", "10.0.1.0") == 256
p ips_between("20.0.0.10", "20.0.1.0") == 246
p ips_between("170.0.0.10", "170.1.0.0") == 65526
p ips_between("180.0.0.0", "181.0.0.0") == 16777216

