/* Square Every Digit (7 kyu)
 * https://www.codewars.com/kata/546e2562b03326a88e000020/javascript
 *
 * Welcome. In this kata, you are asked to square
 * every digit of a number and concatenate them.
 *
 * For example, if we run 9119 through the function,
 * 811181 will come out, because 9^2 is 81 and 1^2 is 1.
 *
 * Note: The function accepts an integer and returns
 * an integer
 *
 * PROBLEM:
 * - INPUT: number
 * - OUTPUT: number
 *
 * ALGORITHM:
 * - convert the number into a string
 * - split the string into an array of strings
 * - iterate over the array using map to transform the array
 *   - square each element (digit) of the array
 * - join the array back into a string
 * - convert the string back into a number
 * - return resulting number
 */

function squareDigits(num) {
  return Number(String(num).split('').map(digit => digit ** 2).join(''));
}

console.log(squareDigits(9119) === 811181);
console.log(squareDigits(3212) === 9414);
console.log(squareDigits(2112) === 4114);
console.log(squareDigits(0) === 0);
console.log(squareDigits(-0) === 0);
