=begin
IQ Test - 6 kyu
https://www.codewars.com/kata/552c028c030765286c00007d/train/ruby

Bob is preparing to pass IQ test. The most frequent task in this test is
to find out which one of the given numbers differs from the others. Bob
observed that one number usually differs from the others in evenness. Help
Bob — to check his answers, he needs a program that among the given numbers
finds one that is different in evenness, and return a position of this number.

! Keep in mind that your task is to help Bob solve a real IQ test, which
means indexes of the elements start from 1 (not 0)

PROBLEM:
- INPUT: String of integer separated by spaces
- OUTPUT: Integer representing the index of the different element
- WHAT WILL I DO WITH THE INPUT?
  - split the string into an array
  - convert each element in the array into an integer
  - check the first three elements to determine which evenness to look for
  - return the index of the element that matches the evenness we're looking for

DATA STRUCTURE
- String converted to an Array of Integers

ALGORITHM:
- split the given string into an array of integers
  - use `split`
  - use `map(&:to_i)`
- count the number of even items of the first three items
- IF the count is greater than or equal to `2`
  - we want to look for an `odd` integer
- ELSE
  - we want to look for an `even` integer
- return the index of the odd or even integer we're looking for
  - use `index(&:even?)` or `index(&:odd?)`
=end

def iq_test(numbers)
  array = numbers.split.map(&:to_i)

  match = if [array[0], array[1], array[2]].select(&:even?).size >= 2
            'odd'
          else
            'even'
          end

  return array.index(&:odd?) + 1 if match == 'odd'

  array.index(&:even?) + 1
end

p iq_test("2 4 7 8 10") == 3
p iq_test("1 2 1 1") == 2
p iq_test("88 96 66 51 14 88 2 92 18 72 18 88 20 30 4 82 90 100 24 46") == 4
p iq_test("99 100 100") == 1
p iq_test("20 94 56 50 10 98 52 32 14 22 24 60 4 8 98 46 34 68 82 82 98 90 50 20 78 49 52 94 64 36") == 26
