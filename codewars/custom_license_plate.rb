=begin
Custom Licence Plate - 6 kyu
https://www.codewars.com/kata/5bc64f48eba26e79df000031/ruby

In the fictional country of Bugliem, you can ask for a custom licence plate
on your car, provided that it meets the followoing criteria:

- it's 2-8 characters long
- it contains only letters A-Z, numbers 0-9, and dashes -
- letters and numbers are separated by a dash
- it doesn't start or end with a dash, and it doesn't have consecutive dashes
- it's not made up of numbers only

You also have to pay 1000 local units to obtain one, although this will not be tested.

To make the life of the local authorities easier (and get your driving record cleaned),
you need to implement a function that accepts an ascii input string, and returns
the number plate if possible.

You have to replace all special characters and spaces with dashes (consecutive characters
should be replaced with a single dash), and truncate the result to maximum 8 characters
(if longer). If the result complies with the above criteria, return it capitalized; otherwise
return "not possible".

PROBLEM:
- INPUT: String
- OUTPUT: String
  - 2-8 characters
  - only A-Z, 0-9, dash (-)
  - letters and numbers separated by dashes
  - cannot start with dash
  - cannot end with dash
  - must have at least one letter
  - all special characters replaced with a dash
  - no consecutive dashes

ALGORITHM:
- initialize result variable
- convert given String to an array of characters
- LOOP over the array with the index
  + initialize next_char variable
  + IF char is a letter, append uppercased char to result
    - IF next char exists and is a number, append '-' to result
  + ELSIF char is a number, append char to result
    - IF next char exists and is a letter, append '-' to result
  + ELSE, append '-' to result
- convert multiple dashes to one dash
- remove start and end dash
- truncate result to 8 characters
- remove end dash
- RETURN result if it contains at least one letter and is greater than or equal to 2
=end

def license_plate(string)
  result = ''

  string.chars.each_with_index do |char, index|
    next_char = string[index + 1]

    if char.match?(/[A-Za-z]/)
      result << char.upcase
      result << '-' if next_char&.match?(/[0-9]/)
    elsif char.match?(/[0-9]/)
      result << char
      result << '-' if next_char&.match?(/[A-Za-z]/)
    else
      result << '-'
    end
  end

  result = result.gsub(/-+/, '-')
                 .gsub(/^-|-$/, '')[0, 8]
                 .gsub(/-$/, '')

  return result if result.match?(/[A-Za-z]/) && result.size >= 2

  "not possible"
end

p license_plate("mercedes") == "MERCEDES"
p license_plate("anter69") == "ANTER-69"
p license_plate("1st") == "1-ST"
p license_plate("~c0d3w4rs~") == "C-0-D-3"
p license_plate("I'm cool!") == "I-M-COOL"
p license_plate("_-=porsche><911=-_") == "PORSCHE"
p license_plate("audi r2") == "AUDI-R-2"
p license_plate("audi 2r") == "AUDI-2-R"
p license_plate("1, 2- 3 :gO!") == "1-2-3-GO"
p license_plate("<FW+_ZwX5 W1") == 'FW-ZWX-5'
p license_plate("1337") == "not possible"
p license_plate("! @ x & $") == "not possible"
p license_plate("c(") == "not possible"
