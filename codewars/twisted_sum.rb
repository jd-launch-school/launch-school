=begin
Twisted Sum
https://www.codewars.com/kata/527e4141bb2ea5ea4f00072f/ruby - 6 kyu

Find the sum of the digits of all the numbers from `1` to `N` (both ends included).

PROBLEM
- INPUT: Integer
- OUTPUT: Integer
  - sum of all numbers between 1 and N

N = 4
1 + 2 + 3 + 4 = 10

N = 10
1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 1 + 0 = 46

N = 12
1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 1 + 0 + 1 + 1 + 1 + 2 = 51

ALGORITHM
- initialize result variable to 0
- LOOP from 1 to and including the given Integer
  - add the sum of the digits of the current number to result
- return result
=end

def solution(num)
  (1..num).inject { |result, cur| result += cur.digits.sum }
end

p solution(4) == 10
p solution(10) == 46
p solution(12) == 51
