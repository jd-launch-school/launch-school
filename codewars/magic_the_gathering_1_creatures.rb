=begin
Magic The Gathering #1: Creatures - (6 kyu)
https://www.codewars.com/kata/567af2c8b46252f78400004d/train/ruby

Each creature has a power and toughness. We will represent this in an array.
[2, 3] means this creature has a power of 2 and a toughness of 3.

If a creature takes on damage greater than or equal to their toughness, they
die. Each creature only attacks with their power once.

Write a method battle(player1, player2) that takes in 2 arrays of creatures.
Each players' creatures battle each other in order (player1[0] battles the
creature in player2[0]) and so on. If one list of creatures is longer than the
other, those creatures are considered unblocked, and do not battle.

Your method should return a hash with the keys player1 and
player2 that contain the power and toughness of the surviving creatures.

PROBLEM
- INPUT: Two Arrays of Arrays
  - inner arrays are creature's power and toughness
- OUTPUT: Hash
  - keys: player1, player2

ALGORITHM:
- initialize a result variable to Hash
- initialize a counter variable to 0
- LOOP
  - BREAK if both player1[counter].nil? and player2[counter].nil?
  - UNLESS creatures exist for both players
  (player1[counter].nil? || player2[counter].nil?)
    - IF player1's creature's power is less than player2's creature's toughness
      - Append player2's creature to result
    - IF player1's creature's toughness is greater than player2's creature's power
      - Append player1's creature to result
  - ELSE
    - IF the size of player1 Array is greater than size of player2 Array
      - Append player1's creature to result
    - ELSE
      - Append player2's creature to result
  - increment counter by 1
- return result
=end

def battle(player1, player2)
  result = { 'player1' => [], 'player2' => [] }
  counter = 0

  loop do
    creature1 = player1[counter]
    creature2 = player2[counter]

    break if creature1.nil? && creature2.nil?

    unless creature1.nil? || creature2.nil?
      if creature1.first < creature2.last
        result["player2"] << creature2
      end

      if creature1.last > creature2.first
        result["player1"] << creature1
      end
    else
      if player1.size > player2.size
        result["player1"] << creature1
      else
        result["player2"] << creature2
      end
    end

    counter += 1
  end

  result
end

player1 = [[1, 1]]
player2 = [[1, 1]]
p battle(player1, player2) == { 'player1' => [], 'player2' => [] }

player1 = [[2, 2]]
player2 = [[1, 1]]
p battle(player1, player2) == { 'player1' => [[2, 2]], 'player2' => [] }
p battle(player2, player1) == { 'player1' => [], 'player2' => [[2, 2]] }

player1 = [[1, 1], [2, 1], [2, 2], [5, 5]]
player2 = [[1, 2], [1, 2], [3, 3]]
p battle(player1, player2) == { 'player1' => [[5, 5]],
                                'player2' => [[1, 2], [3, 3]] }
p battle(player2, player1) == { 'player1' => [[1, 2], [3, 3]],
                                'player2' => [[5, 5]] }

player1 = []
player2 = [[1, 2], [1, 2], [3, 3]]
p battle(player1, player2) == { 'player1' => [],
                                'player2' => [[1, 2], [1, 2], [3, 3]] }
p battle(player2, player1) == { 'player1' => [[1, 2], [1, 2], [3, 3]],
                                'player2' => [] }

