=begin
Help the bookseller !
https://www.codewars.com/kata/54dc6f5a224c26032800005c/ruby (6 kyu)

Given an inventory (L) with item and count, and a stocklist (M):

L = ["ABART 20", "CDXEF 50", "BKWRK 25", "BTSQZ 89", "DRTYM 60"]
M = ["A", "B", "C", "W"]

Return the total count of the inventory items that start with each letter in M:

(A : 20) - (B : 114) - (C : 50) - (W : 0)

If L or M are empty, return an empty string.

PROBLEM:
- INPUT: Two arrays
  - An array of items in an inventory
  - the items that we want to get a count for

- OUTPUT: String showing total counts

- RULES: If L or M are empty, return an empty string.

ALGORITHM:
- initialize a result array variable
- LOOP through items in stocklist
  - initialize a total variable to 0
  - LOOP through items in the inventory
    - IF the first letter of an inventory item is equal to the stocklist item
      - split the inventory item on spaces into an array
      - add the last element of array converted to an integer to the total
  - append "({stocklist item} : {total})"
- return a joined array separated by spaces and a dash: ex: ' - '
=end

def stock_list(inventory, stocklist)
  return "" if inventory.empty?

  result = []

  stocklist.map do |stock_item|
    total = 0

    inventory.each do |inventory_item|
      if inventory_item[0] == stock_item
        total += inventory_item.split.last.to_i
      end
    end

    result << "(#{stock_item} : #{total})"
  end

  result.join(' - ')
end

inventory = ["ABAR 200", "CDXE 500", "BKWR 250", "BTSQ 890", "DRTY 600"]
p stock_list(inventory, ["A", "B"]) == "(A : 200) - (B : 1140)"

inventory = ["BBAR 150", "CDXE 515", "BKWR 250", "BTSQ 890", "DRTY 600"]
p stock_list(inventory, ["A", "B", "C", "D"]) == "(A : 0) - (B : 1290) - (C : 515) - (D : 600)"

inventory = ["BBAR 150", "CDXE 515", "BKWR 250", "BTSQ 890", "DRTY 600"]
p stock_list(inventory, ["A", "C", "B", "D"]) == "(A : 0) - (C : 515) - (B : 1290) - (D : 600)"

inventory = []
p stock_list(inventory, ["A", "C", "B", "D"]) == ""

inventory = ["BBAR 150", "CDXE 515", "BKWR 250", "BTSQ 890", "DRTY 600"]
p stock_list(inventory, []) == ""
