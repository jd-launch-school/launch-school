=begin
Guess The Gifts! (5 kyu)
https://www.codewars.com/kata/52ae6b6623b443d9090002c8/train/ruby

You will be given a wishlist (array), containing all possible items. Each item is in the format: `{name: "toy car", size: "medium", clatters: "a bit", weight: "medium"}` (Ruby version has an analog hash structure, see example below)

You also get a list of presents (array), you see under the christmas tree, which have the following format each: `{size: "small", clatters: "no", weight: "light"}`

Your task is to create a list of all possible presents you might get.

Rules
- Possible values for `size`: "small", "medium", "large"
- Possible values for `clatters`: "no", "a bit", "yes"
- Possible values for `weight`: "light", "medium", "heavy"
- The return value must be an array of the names of items from your wishlist, e.g. `["Toy Car", "Card Game"]`
- Don't add any item more than once to the result
- The order of names in the returned array doesn't matter
- It's possible, that multiple items from your wish list have the same attribute values. If they match the attributes of one of the presents, add all of them.
=end

=begin
PROBLEM
- INPUT: Two arrays
  - wishlist: Arary of Hashes
  - presents: Array of Hashes
- OUTPUT: Array of the names of presents

ALGORITHM
- Initialize a result variable set to an empty array
- LOOP through presents array
  - LOOP through wishlist array
    - IF size matches in both hashes AND
    - IF clatters matches in both hashes AND
    - IF weight matches in both hashes
      + Append the gift name to the result
- return result array
=end

def guess_gifts(wishlist, presents)
  result = []

  presents.each do |present|
    wishlist.each do |item|
      if present[:size] == item[:size] &&
         present[:clatters] == item[:clatters] &&
         present[:weight] == item[:weight]
        result << item[:name]
      end
    end
  end

  result.uniq
end

wishlist = [
    {:name => "mini puzzle", :size => "small", :clatters => "yes", :weight => "light"},
    {:name => "toy car", :size => "medium", :clatters => "a bit", :weight => "medium"},
    {:name => "card game", :size => "small", :clatters => "no", :weight => "light"}
]

presents = [
    {:size => "medium", :clatters => "a bit", :weight => "medium"},
    {:size => "small", :clatters => "yes", :weight => "light"}
]

p guess_gifts(wishlist, presents) == ['toy car', 'mini puzzle']

wishlist = [
    {:name => "mini puzzle", :size => "small", :clatters => "yes", :weight => "light"},
    {:name => "toy car", :size => "medium", :clatters => "a bit", :weight => "medium"},
    {:name => "card game", :size => "medium", :clatters => "a bit", :weight => "medium"}
]

presents = [
    {:size => "medium", :clatters => "a bit", :weight => "medium"}
]

p guess_gifts(wishlist, presents) == ['toy car', 'card game']

wishlist = [
    {:name => "mini puzzle", :size => "small", :clatters => "yes", :weight => "light"},
    {:name => "toy car", :size => "light", :clatters => "a bit", :weight => "medium"},
    {:name => "card game", :size => "medium", :clatters => "a bit", :weight => "medium"}
]

presents = [
    {:size => "medium", :clatters => "a bit", :weight => "medium"},
    {:size => "medium", :clatters => "a bit", :weight => "medium"}
]

p guess_gifts(wishlist, presents) == ["card game"]
