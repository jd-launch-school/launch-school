# The Supermarket Queue (6 kyu)
https://www.codewars.com/kata/57b06f90e298a7b53d000a86/ruby

## Problem Description
There is a queue for the self-checkout tills at the supermarket.
Your task is to write a function to calculate the total time
required for all the customers to check out!

Input:
  - customers: an array of positive integers representing the
    queue. Each integer represents a customer, and its value is
    the amount of time they require to check out.

  - n: a positive integer, the number of checkout tills.

Output:
  - The function should return an integer, the total time required.

Rules:
  - There is only ONE queue serving many tills
  - The order of the queue NEVER changes
  - The front person in the queue (i.e. the first element in the array/list)
    proceeds to a till as soon as it becomes free
  - You should assume that all the test input will be valid,
    as specified above

## My Solution
```ruby
=begin
PROBLEM:
 - INPUT:
   + customers array
     - order will never change
     - all items are valid positive Integers
   + Integer representing the number of tills
 - OUTPUT:
   - Integer representing the total time required

DATA STRUCTURE:
- Array of Integers:
  + [0, 0] first index is first till, second index is second till, etc...

ALGORITHM:
- create a `Tills` array
  + each item will be zero at first
  + number of items will be equal to `n`
  + ex: [0, 0]
- iterate over the customers array using `each`
  + loop `n` times using `times`
    - if value of the `nth` index of Tills is the minimum
      value in the `Tills` array
      + add customer value to the `nth` index of `Tills`
      + break out of the loop
- return the max of the `Tills` array
=end

def queue_time(customers, n)
  tills = Array.new(n, 0)

  customers.each do |customer|
    n.times do |index|
      break tills[index] += customer if tills[index] == tills.min
    end
  end

  tills.max
end

p queue_time([], 1) == 0
p queue_time([5], 1) == 5
p queue_time([2], 5) == 2
p queue_time([1,2,3,4,5], 1) == 15
p queue_time([1,2,3,4,5], 100) == 5
p queue_time([2,2,3,3,4,4], 2) == 9
p queue_time([5,3,4], 1) == 12
p queue_time([10,2,3,3], 2) == 10
p queue_time([2,3,10], 2) == 12
```
