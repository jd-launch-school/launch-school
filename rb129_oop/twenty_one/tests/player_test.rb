require 'simplecov'
SimpleCov.start

require 'minitest/autorun'
require_relative '../twenty_one.rb'

class PlayerBlackjackTest < MiniTest::Test
  def setup
    @player = Player.new
  end

  def test_for_blackjack_ace_first
    hand = [Card.new('heart', 'a'), Card.new('spade', 'k')]
    @player.hand << hand[0] << hand[1]
    @player.hand_score

    assert_equal(true, @player.blackjack?)
  end

  def test_for_blackjack_ace_last
    hand = [Card.new('heart', 'k'), Card.new('spade', 'a')]
    @player.hand << hand[0] << hand[1]
    @player.hand_score

    assert_equal(true, @player.blackjack?)
  end

  def test_for_blackjack_ace_and_ten_last
    hand = [Card.new('heart', 'a'), Card.new('spade', '10')]
    @player.hand << hand[0] << hand[1]
    @player.hand_score

    assert_equal(true, @player.blackjack?)
  end

  def test_for_blackjack_ten_and_ace_last
    hand = [Card.new('heart', '10'), Card.new('spade', 'a')]
    @player.hand << hand[0] << hand[1]
    @player.hand_score

    assert_equal(true, @player.blackjack?)
  end

  def test_for_blackjack_ace_and_nine_last
    hand = [Card.new('heart', 'a'), Card.new('spade', '9')]
    @player.hand << hand[0] << hand[1]
    @player.hand_score

    assert_equal(false, @player.blackjack?)
  end

  def test_for_blackjack_nine_and_ace_last
    hand = [Card.new('heart', '9'), Card.new('spade', 'a')]
    @player.hand << hand[0] << hand[1]
    @player.hand_score

    assert_equal(false, @player.blackjack?)
  end

  def test_for_blackjack_no_ace
    hand = [Card.new('heart', '2'), Card.new('spade', '9')]
    @player.hand << hand[0] << hand[1]
    @player.hand_score

    assert_equal(false, @player.blackjack?)
  end

  def test_for_blackjack_three_cards_ace_last
    hand = [Card.new('heart', '2'), Card.new('spade', '8'), Card.new('diamond', 'a')]
    @player.hand << hand[0] << hand[1] << hand[2]
    @player.hand_score

    assert_equal(false, @player.blackjack?)
  end

  def test_for_blackjack_three_cards_ace_first
    hand = [Card.new('heart', 'a'), Card.new('spade', '8'), Card.new('diamond', '2')]
    @player.hand << hand[0] << hand[1] << hand[2]
    @player.hand_score

    assert_equal(false, @player.blackjack?)
  end

  def test_for_blackjack_three_cards_ace_last_as_one
    hand = [Card.new('heart', '10'), Card.new('spade', '10'), Card.new('diamond', 'a')]
    @player.hand << hand[0] << hand[1] << hand[2]
    @player.hand_score

    assert_equal(false, @player.blackjack?)
  end
end
