require 'simplecov'
SimpleCov.start

require 'minitest/autorun'
require 'stringio'

require_relative '../twenty_one.rb'

# `simulate_stdin` was based on:
# https://tommaso.pavese.me/2016/05/08/understanding-and-testing-io-in-ruby/
def simulate_stdin(*input)
  io = StringIO.new
  io.puts(input)
  io.rewind

  $stdin = io
  yield
ensure
  $stdin = STDIN
end

class RetrieveOptionsTest < MiniTest::Test
  include Utilities

  def test_with_two_options
    result = retrieve_options([:y, :yes, :n, :no])
    assert_equal("(y)es | (n)o", result)
  end

  def test_with_three_options
    result = retrieve_options([:q, :quit, :r, :reset, :c, :continue])
    assert_equal("(q)uit | (r)eset | (c)ontinue", result)
  end

  def test_with_two_options_with_underscores
    result = retrieve_options([:c, :cash_out, :p, :play_again])
    assert_equal("(c)ash out | (p)lay again", result)
  end
end

class ChoicesForPromptTest < MiniTest::Test
  include Utilities

  def test_with_two_options
    result = choices_for_prompt([:y, :yes, :n, :no])
    assert_equal(["(y)es", "(n)o"], result)
  end

  def test_with_three_options
    result = choices_for_prompt([:q, :quit, :r, :reset, :c, :continue])
    assert_equal(["(q)uit", "(r)eset", "(c)ontinue"], result)
  end

  def test_with_two_options_with_underscores
    result = choices_for_prompt([:c, :cash_out, :p, :play_again])
    assert_equal(["(c)ash_out", "(p)lay_again"], result)
  end
end

class PlayerChoiceTest < MiniTest::Test
  include Utilities
  include Display

  def invalid_choice_test(*args, choices)
    result = capture_io do
      simulate_stdin(args) { player_choice(choices) }
    end

    assert_match /.*Not a valid choice.*/, result.first
  end

  def valid_choice_test(*args, expected, choices)
    result = simulate_stdin(args) { player_choice(choices) }
    assert_equal expected, result
  end

  def test_with_single_lower_case_valid_letter
    valid_choice_test 'y', :y, %i(yes y no n)
  end

  def test_with_lower_case_valid_word
    valid_choice_test 'yes', :y, %i(yes y no n)
  end

  def test_with_single_upper_case_valid_letter
    valid_choice_test 'Y', :y, %i(yes y no n)
  end

  def test_with_upper_case_valid_word
    valid_choice_test 'YES', :y, %i(yes y no n)
  end

  def test_with_lower_case_invalid_word
    invalid_choice_test 'yeserie', 'n', %i(yes y no n)
  end

  def test_with_empty_string_four_choices
    invalid_choice_test '', 'y', %i(yes y no n)
  end

  def test_with_space_four_choices
    invalid_choice_test ' ', 'y', %i(yes y no n)
  end

  def test_with_letters_and_numbers
    invalid_choice_test 'y3s', 'n', %i(yes y no n)
  end

  def test_with_letters_separated_by_spaces
    invalid_choice_test 'y e s', 'n', %i(yes y no n)
  end

  def test_with_number
    invalid_choice_test '1', 'y', %i(yes y no n)
  end

  def test_with_single_lower_case_valid_letter_six_choices
    valid_choice_test 'q', :q, %i(quit q continue cont c reset r)
  end

  def test_with_lower_case_valid_word_six_choices
    valid_choice_test 'quit', :q, %i(quit q continue cont c reset r)
  end

  def test_with_lower_case_valid_middle_word_six_choices
    valid_choice_test 'cont', :c, %i(quit q continue cont c reset r)
  end

  def test_with_lower_case_invalid_word_six_choices
    invalid_choice_test 'quitter', 'c', %i(quit q continue cont c reset r)
  end

  def test_with_lower_case_word_with_spaces_six_choices
    invalid_choice_test ' cont', 'c', %i(quit q continue cont c reset r)
  end

  def test_with_empty_string_six_choices
    invalid_choice_test '', 'c', %i(quit q continue cont c reset r)
  end

  def test_with_space_six_choices
    invalid_choice_test ' ', 'c', %i(quit q continue cont c reset r)
  end

  def test_with_single_lower_case_letter_underscore_choices
    valid_choice_test 'c', :c, %i(cash_out c play_again p)
  end

  def test_with_space_underscore_choices
    invalid_choice_test(' ', 'c', %i(cash_out c play_again p))
  end
end

class ValidAmountTest < MiniTest::Test
  include Utilities

  def test_with_zero
    result = valid_amount?('0')
    assert_equal(false, result)
  end

  def test_with_zero_point_zero
    result = valid_amount?('0.0')
    assert_equal(false, result)
  end

  def test_with_zero_point_zero_zero
    result = valid_amount?('0.00')
    assert_equal(false, result)
  end

  def test_with_only_zeros_after_decimal
    result = valid_amount?('.000')
    assert_equal(false, result)
  end

  def test_with_only_zeros_before_decimal
    result = valid_amount?('000.')
    assert_equal(false, result)
  end

  def test_with_leading_zeros_integer
    result = valid_amount?('0001')
    assert_equal(false, result)
  end

  def test_with_leading_zeros_float
    result = valid_amount?('0001.03')
    assert_equal(false, result)
  end

  def test_with_leading_zeros_float_end_with_decimal
    result = valid_amount?('0001.')
    assert_equal(false, result)
  end

  def test_with_float_leading_zeros_with_zero_before_decimal_end_with_decimal
    result = valid_amount?('00010.')
    assert_equal(false, result)
  end

  def test_with_small_float
    result = valid_amount?('.11')
    assert_equal(false, result)
  end

  def test_with_large_float
    result = valid_amount?('.0000003344')
    assert_equal(false, result)
  end

  def test_with_large_float_leading_zero
    result = valid_amount?('0.0000003344')
    assert_equal(false, result)
  end

  def test_with_float_leading_zeros_before_and_after_decimal
    result = valid_amount?('000.0003')
    assert_equal(false, result)
  end

  def test_with_one
    result = valid_amount?('1')
    assert_equal(true, result)
  end

  def test_with_ten
    result = valid_amount?('10')
    assert_equal(true, result)
  end

  def test_with_ten_float
    result = valid_amount?('10.003')
    assert_equal(false, result)
  end

  def test_with_eleven
    result = valid_amount?('11')
    assert_equal(true, result)
  end

  def test_with_one_thousand
    result = valid_amount?('1,000')
    assert_equal(true, result)
  end

  def test_with_one_thousand_no_comma
    result = valid_amount?('1000')
    assert_equal(true, result)
  end

  def test_with_one_comma
    result = valid_amount?('100,000')
    assert_equal(true, result)
  end

  def test_with_one_comma_no_zeros
    result = valid_amount?('111,111')
    assert_equal(true, result)
  end

  def test_with_integer_no_comma
    result = valid_amount?('100000')
    assert_equal(true, result)
  end

  def test_with_integer_no_zeros_no_comma
    result = valid_amount?('111111')
    assert_equal(true, result)
  end

  def test_with_integer_two_commas
    result = valid_amount?('1,000,000')
    assert_equal(true, result)
  end

  def test_with_integer_two_commas_no_zeros
    result = valid_amount?('1,111,111')
    assert_equal(true, result)
  end

  def test_with_three_commas
    result = valid_amount?('1,000,000,000')
    assert_equal(true, result)
  end

  def test_with_three_commas_no_zeros
    result = valid_amount?('1,111,111,111')
    assert_equal(true, result)
  end

  def test_with_one_digit_negative_integer
    result = valid_amount?('-1')
    assert_equal(false, result)
  end

  def test_with_two_digit_negative_integer
    result = valid_amount?('-11')
    assert_equal(false, result)
  end

  def test_with_three_digit_negative_integer_end_in_zero
    result = valid_amount?('-110')
    assert_equal(false, result)
  end

  def test_with_four_digit_negative_include_zero
    result = valid_amount?('-1101')
    assert_equal(false, result)
  end

  def test_with_four_digit_negative_leading_zeros
    result = valid_amount?('-0001')
    assert_equal(false, result)
  end

  def test_with_negative_float_leading_zero
    result = valid_amount?('-0.1')
    assert_equal(false, result)
  end

  def test_with_negative_float_one_leading_zero_before_and_after_decimal
    result = valid_amount?('-0.033')
    assert_equal(false, result)
  end

  def test_with_negative_float_multiple_leading_zeros_before_and_after_decimal
    result = valid_amount?('-000.0003')
    assert_equal(false, result)
  end

  def test_with_negative_float_no_zeros
    result = valid_amount?('-2.33')
    assert_equal(false, result)
  end

  def test_with_negative_float_include_zero
    result = valid_amount?('-1.01')
    assert_equal(false, result)
  end

  def test_with_negative_two_digit_float_include_zeros
    result = valid_amount?('-10.01')
    assert_equal(false, result)
  end

  def test_with_negative_three_digit_float_include_zeros
    result = valid_amount?('-100.01')
    assert_equal(false, result)
  end

  def test_with_negative_three_digit_float_alternating_zeros
    result = valid_amount?('-100.0101')
    assert_equal(false, result)
  end

  def test_with_variations
    variations = [
      ["-1.", false], ["-11.", false], ["-10.", false], ["-100.", false], ["-1001.", false],
      ["-001.", false], ["-0", false], ["-0.0", false], ["-0.000", false], ["-0.", false],
      ["-", false], [".", false], ["15-", false], [".-", false], ["0001-0002", false],
      ["-0000", false], ["100,000.001", false], ["-100,000", false], ["1000,00", false],
      ["1,000,000.000", false], ["1,111,111.111", false], ["11111,000", false], ["1.", false],
      ["1.0", false], ["1.00", false], ["1.01", false], ["1.10", false], ["1.11", false],
      ["1,000.00", false], ["11.", false], ["10.0", false], ["10.00", false], ["10.01", false],
      ["100.0", false], ["100.00", false], ["101.", false], ["11.00", false], ["11.1", false],
      ["11.10", false], ["11.11", false], ["1,000,000.00", false], ["1,111,111.11", false]
    ]

    variations.each do |variation|
      assert_equal(variation.last, valid_amount?(variation.first))
    end
  end
end

class RetrieveAmountTest < MiniTest::Test
  include Utilities
  include Display

  def valid_choice_test(choice, expected)
    result = simulate_stdin(choice) { retrieve_amount }
    assert_equal expected, result
  end

  def invalid_choice_test(*args)
    result = capture_io do
      simulate_stdin(args) { retrieve_amount }
    end

    assert_match /.*The amount must be a positive.*/, result.first
  end

  def test_valid_choice
    valid_choice_test('1000', 1000)
  end

  def test_valid_choice_with_one_comma
    valid_choice_test('1,000', 1000)
  end

  def test_valid_choice_with_multiple_commas
    valid_choice_test('1,000,000', 1000000)
  end

  def test_invalid_choice
    invalid_choice_test('1000.35', '1')
  end

  def test_invalid_choice_with_commas
    invalid_choice_test('1,000,', '1000')
  end

  def test_invalid_choice_zero
    invalid_choice_test('0', '1')
  end

  def test_invalid_choice_negative
    invalid_choice_test('-1', '1')
  end
end
