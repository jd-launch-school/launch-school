module Utilities
  def ask_player?(message, choices, result)
    prompt "\n#{message} [ #{retrieve_options(choices)} ]"
    player_choice(choices) == result
  end

  def retrieve_options(choices)
    words = choices.select { |choice| choice.length > 1 }
    choices_for_prompt(words).join(' | ').tr('_', ' ')
  end

  def choices_for_prompt(choices)
    choices.each_with_object([]) do |choice, prompt_choices|
      prompt_choices << "(#{choice[0]})#{choice[1..-1]}" if choice.size > 1
    end
  end

  def player_choice(choices)
    loop do
      player_choice = gets.chomp.downcase.to_sym
      return player_choice[0].to_sym if choices.include?(player_choice)

      prompt "\nNot a valid choice.\nPlease choose" \
             " [ #{retrieve_options(choices)} ]."
    end
  end

  def valid_amount?(num)
    without_commas = /^[1-9]\d*$/.match?(num)
    with_commas = /^[1-9]\d{0,2}([,]\d{3})*$/.match?(num)

    without_commas || with_commas
  end

  # rubocop:disable Metrics/MethodLength
  def retrieve_amount
    loop do
      amount = gets.chomp
      return amount.delete(",").to_i if valid_amount?(amount)

      prompt "\nThe amount must be a positive number greater than $1.\n" \
             "It can be precise to the nearest dollar (0 decimal places).\n" \
             "Examples:\n" \
             "   1,000    (valid)\n" \
             "   1000     (valid)\n" \
             "   1000.3   (not valid)\n" \
             "   1000.35  (not valid)\n" \
             "   1000.350 (not valid)\n\n" \
             "Please enter an amount in whole dollars only:\n"
    end
  end
  # rubocop:enable Metrics/MethodLength

  def format_amount(amount)
    amount.to_s.reverse.gsub(/(\d{3})(?=\d)/) { |digits| "#{digits}," }.reverse
  end
end

module Display
  def clear_screen
    system('clear') || system('cls')
  end

  def prompt(message)
    puts message
    print ">> "
  end

  # rubocop:disable Metrics/MethodLength
  def display_instructions
    clear_screen
    prompt <<~INSTRUCTIONS
      BASIC INSTRUCTIONS
      ==================
      Card values:
        Number cards (2-10): the value on the card
        Face cards (j, q, k): value of 10
        Ace (a): either 1 or 11

      The goal is to get one's hand as close to the value of 21 as possible
      without going over 21, by adding up the value of the cards.

      Each player is dealt two cards. The player goes first and can either
      `hit` or `stay`.

      A `hit` adds a card to the player's hand. Choosing to `stay` passes the
      turn to the dealer. The dealer must `hit` until the value of their cards
      is greater than or equal to 17.

      At the end of both turns, whomever has the hand with the highest value wins!
      See https://en.wikipedia.org/wiki/Blackjack for more info.

      Press ENTER to continue...
    INSTRUCTIONS

    gets
  end
  # rubocop:enable Metrics/MethodLength

  def display_header
    clear_screen
    puts "Welcome to Twenty-One!"
    puts "Try to beat the dealer without going over " \
         "#{TwentyOne::WINNING_SCORE}."
    return unless bank

    bet_message = '| ' + bank.bet_message unless bank.bet.zero?
    puts "#{bank.balance_message} #{bet_message}"
  end

  def display_winner
    if winner
      puts "\n#{blackjack_or_win} " \
           "You #{win_or_lose} $#{format_amount(bank.bet)}.\n\n"
    else
      puts "\nIt's a draw.\n\n"
    end
  end

  def win_or_lose
    winner == player ? "WIN" : "LOSE"
  end

  def blackjack_or_win
    if winner.blackjack?
      "*** Blackjack!! #{winner.class} Wins!! ***"
    else
      "#{winner.class} wins!"
    end
  end

  def display_balance
    puts "#{bank.balance_message} | #{bank.starting_balance_message}"
  end

  def display_lost_all_money
    puts "You've lost all of your money."
    puts "Press enter ..."
    gets
  end

  def display_summary
    clear_screen
    puts "Overall Results:"
    puts "----------------"
    puts "You started with $#{format_amount(bank.starting_balance)}."
    puts "You cashed out $#{format_amount(bank.balance)}."
    puts bank.profit_or_loss_message
  end

  def display_goodbye
    puts "\nThanks for playing Twenty One! Goodbye."
  end

  def balance_message
    "Current Balance: $#{format_amount(balance)}"
  end

  def bet_message
    "Current Bet: $#{format_amount(bet)}"
  end

  def starting_balance_message
    "Starting Balance: $#{format_amount(starting_balance)}"
  end

  def won_or_lost
    balance - starting_balance > 0 ? 'WON' : 'LOST'
  end

  def profit_or_loss_message
    if profit_or_loss.zero?
      "You broke even."
    else
      "You #{won_or_lost} $#{format_amount(profit_or_loss)}!"
    end
  end
end

module Hand
  def hand_score
    sorted_hand = hand.sort_by { |card| Card::FACES.find_index(card.face) }
    @score = sorted_hand.reduce(0) do |result, card|
      result + (card.value || (result >= 11 ? 1 : 11))
    end
  end

  def display_hand
    puts "\n#{self.class}'s hand: #{score}"

    lines = build_hand
    puts lines[0, 3], lines.last, lines.reverse[1..-1]
  end

  def busted?
    score > TwentyOne::WINNING_SCORE
  end

  private

  def build_hand
    array = Array.new(4) { [] }

    hand.each do |card|
      array[0] << card.border_line
      array[1] << card.suit_line
      array[2] << card.blank_line
      array[3] << card.face_line
    end

    array.map(&:join)
  end
end

class Participant
  include Hand

  attr_reader :hand, :score

  def initialize
    @hand = []
    @score = 0
  end
end

class Player < Participant
  def blackjack?
    score == 21 && hand.size == 2 && hand.map(&:face).include?('a')
  end
end

class Dealer < Participant
  def display_starting_hand
    puts "\nDealer's hand:"

    lines = build_starting_hand
    puts lines[0, 3], lines.last, lines.reverse[1..-1]
  end

  def blackjack?
    hand.size == 2 && (hand.first.face == 'a' && hand.last.value == 10)
  end

  private

  def build_starting_hand
    array = []
    card = hand.first

    array << [card.border_line, card.border_line]
    array << [card.suit_line, card.back_side]
    array << [card.blank_line, card.back_side]
    array << [card.face_line, card.back_side]

    array.map(&:join)
  end
end

class Deck
  include Display

  attr_reader :cards

  def initialize
    reset
    @cards = cards.shuffle
  end

  def deal_card
    cards.shift
  end

  def reset
    @cards = Card::SUITS.product(Card::FACES).map do |card|
      Card.new(card.first, card.last)
    end
  end

  def shuffle
    clear_screen
    puts "Only #{cards.size} cards remaining in deck."
    puts "Shuffling Deck..."
    reset
    sleep(2)

    @cards = cards.shuffle
  end
end

class Card
  attr_reader :suit, :face, :value

  FACES = %w(2 3 4 5 6 7 8 9 10 j q k a)
  SUITS = %w(◆ ♣ ♥ ♠)

  def initialize(suit, face)
    @suit = suit
    @face = face
    @value = assign_value
  end

  def assign_value
    return 10 if ['j', 'q', 'k'].include?(face)

    return nil if face == 'a'

    face.to_i
  end

  def border_line
    " -------- "
  end

  def suit_line
    "| #{suit}   #{suit}  |"
  end

  def blank_line
    "|        |"
  end

  def face_line
    face == '10' ? "|   #{face}   |" : "|   #{face.upcase}    |"
  end

  def back_side
    "|░░░░░░░░|"
  end
end

# This code smells of :reek:Attribute
class Bank
  include Utilities
  include Display

  attr_accessor :bet, :balance
  attr_reader :starting_balance

  def initialize
    @starting_balance = retrieve_starting_balance
    @balance = starting_balance
    @bet = retrieve_bet
  end

  def increment_balance
    @balance += bet
  end

  def decrement_balance
    @balance -= bet
  end

  def retrieve_starting_balance
    prompt "\nWhat would you like your starting balance to be in dollars?\n"
    retrieve_amount
  end

  def retrieve_bet
    prompt "\nHow much would you like to bet?"

    loop do
      proposed_bet = retrieve_amount
      return proposed_bet unless balance - proposed_bet < 0

      prompt "\nYou don't have enough money to place a" \
             " $#{format_amount(proposed_bet)} bet.\n" \
             "Place a bet that's less than or equal to" \
             " $#{format_amount(balance)}."
    end
  end

  def profit_or_loss
    (balance - starting_balance).abs
  end
end

# This class smells of :reek:TooManyInstanceVariables and :reek:TooManyMethods
class TwentyOne
  include Utilities
  include Display

  attr_reader :bank, :deck, :player, :dealer, :current_player, :winner

  WINNING_SCORE = 21
  DEALER_HITS_UNTIL = 17
  MIN_CARDS_IN_DECK = 10

  def initialize
    clear_screen
    display_header
    display_instructions if ask_player?('Do you want to see the instructions?',
                                        %i(y yes n no),
                                        :y)
    game_setup
  end

  # rubocop:disable Layout/LineLength
  def play
    loop do
      play_round
      display_summary

      break unless ask_player?("Do you want to play with a new balance or quit?",
                               %i(p play q quit),
                               :p)

      reset_bank_and_game
    end
    display_goodbye
  end
  # rubocop:enable Layout/LineLength

  private

  def game_setup
    @player = Player.new
    @dealer = Dealer.new
    @deck = Deck.new
    display_header
    @bank = Bank.new
    @current_player = player
    play
  end

  # rubocop:disable Metrics/MethodLength
  def play_round
    loop do
      deal_cards
      display_game
      participant_turns
      determine_and_display_winner
      break display_lost_all_money if bank.balance.zero?

      break unless ask_player?("Do you want to cash out or play again?",
                               %i(c cash_out p play_again),
                               :p)

      reset_and_retrieve_bet
    end
  end
  # rubocop:enable Metrics/MethodLength

  def deal_cards
    2.times do
      player.hand << deck.deal_card
      dealer.hand << deck.deal_card
    end
  end

  def display_game
    display_header
    calculate_hand_scores

    if current_player == dealer || dealer.blackjack?
      dealer.display_hand
    else
      dealer.display_starting_hand
    end

    player.display_hand
  end

  def calculate_hand_scores
    player.hand_score
    dealer.hand_score
  end

  def participant_turns
    return if dealer.blackjack? || player.blackjack?

    player_turn
    winner_or_dealer_turn
  end

  def player_turn
    loop do
      break @current_player = dealer if ask_player?("What do you want to do?",
                                                    %i(h hit s stay),
                                                    :s)

      player.hand << deck.deal_card
      display_game

      break if player.score > WINNING_SCORE
    end
  end

  def winner_or_dealer_turn
    player.busted? ? @winner = dealer : dealer_turn
  end

  def dealer_turn
    loop do
      display_game
      return if dealer.score >= DEALER_HITS_UNTIL

      dealer.hand << deck.deal_card
      sleep(1)
    end
  end

  def determine_and_display_winner
    @winner = determine_winner unless winner
    adjust_score if winner
    display_winner
    display_balance
  end

  def determine_winner
    dealer.busted? ? player : winner_based_on_score
  end

  def winner_based_on_score
    return player if player.score > dealer.score

    dealer if dealer.score > player.score
  end

  def adjust_score
    if winner == player
      bank.bet = (bank.bet * 1.5).to_i if player.blackjack?
      bank.increment_balance
    else
      bank.decrement_balance
    end
  end

  def reset_and_retrieve_bet
    reset_game
    bank.bet = 0
    display_header
    bank.bet = bank.retrieve_bet
  end

  def reset_bank_and_game
    reset_game
    @bank = nil
    display_header
    @bank = Bank.new
  end

  def reset_game
    deck.shuffle if deck.cards.size <= MIN_CARDS_IN_DECK
    @player = Player.new
    @dealer = Dealer.new
    @current_player = player
    @winner = nil
  end
end

game = TwentyOne.new if __FILE__ == $PROGRAM_NAME
