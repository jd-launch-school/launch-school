* Tic Tac Toe (OOP)
** Assignment: OO Tic Tac Toe
[[https://launchschool.com/lessons/97babc46/assignments/7dcb53f1][(Launch School Reference]])

*** Nouns and Verbs
Short description:

#+begin_quote
Tic Tac Toe is a 2-player board game played on a 3x3 grid. Players take turns marking a square. The first player to mark 3 squares in a row wins.
#+end_quote

#+begin_quote
Nouns: player, board, grid, square
Verbs: play, mark
#+end_quote

Possible classes:

#+begin_quote
Board
Square
Player
 - mark
 - play
#+end_quote

*** Spike
#+begin_src ruby :results output :exports both
class Board
  def initialize
    # we need some way to model the 3x3 grid. Maybe "squares"?
    # what data structure should we use?
    # - array/hash of Square objects?
    # - array/hash of strings or integers?
  end
end

class Square
  def initialize
    # maybe a "status" to keep track of this square's mark?
  end
end

class Player
  def initialize
    # maybe a "marker" to keep track of this player's symbol (i.e. `X` or `O`)
  end

  def mark
  end

  def play
  end
end

class TTTGame
  def play
    display_welcome_message

    loop do
      display_board
      first_player_moves
      break if someone_won? || board_full?

      second_player_moves
      break if someone_won? || board_full?
    end

    display_result
    display_goodbye_message
  end
end

game = TTTGame.new
game.play
#+end_src

** Walk-through: OO TTT Spike
([[https://launchschool.com/lessons/97babc46/assignments/cb07735f][Launch School Reference]])

*** Step 1 - Display a board
#+begin_src ruby :results output :exports both
class TTTGame
  def display_welcome_message
    puts "Welcome to Tic Tac Toe!"
    puts ""
  end

  def display_goodbye_message
    puts "Thanks for playing Tic Tac Toe! Goodbye!"
  end

  def display_board
    puts ""
    puts "     |     |"
    puts "     |     |"
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "     |     |"
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "     |     |"
    puts "     |     |"
    puts ""
  end

  def play
    display_welcome_message
    loop do
      display_board
      break
    end
    display_goodbye_message
  end
end

game = TTTGame.new
game.play
#+end_src

#+RESULTS:
#+begin_example
Welcome to Tic Tac Toe!


     |     |
     |     |
     |     |
-----+-----+-----
     |     |
     |     |
     |     |
-----+-----+-----
     |     |
     |     |
     |     |

Thanks for playing Tic Tac Toe! Goodbye!
#+end_example

*** Step 2 - Setup `Board` and `Square` classes
#+begin_src ruby :results output :exports both
class Board
  INITIAL_MARKER = ' '

  def initialize
    @squares = {}
    (1..9).each { |key| @squares[key] = Square.new(INITIAL_MARKER) }
  end

  def get_square_at(key)
    @squares[key]
  end
end

class Square
  def initialize(marker)
    @marker = marker
  end

  def to_s
    @marker
  end
end

class TTTGame
  attr_reader :board

  def initialize
    @board = Board.new
  end

  def display_welcome_message
    puts "Welcome to Tic Tac Toe!"
    puts ""
  end

  def display_goodbye_message
    puts "Thanks for playing Tic Tac Toe! Goodbye!"
  end

  def display_board
    puts ""
    puts "     |     |"
    puts "  #{board.get_square_at(1)}  |  #{board.get_square_at(2)}  |  #{board.get_square_at(3)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(4)}  |  #{board.get_square_at(5)}  |  #{board.get_square_at(6)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(7)}  |  #{board.get_square_at(8)}  |  #{board.get_square_at(9)}  "
    puts "     |     |"
    puts ""
  end

  def play
    display_welcome_message
    loop do
      display_board
      break
    end
    display_goodbye_message
  end
end

game = TTTGame.new
game.play
#+end_src

#+RESULTS:
#+begin_example
Welcome to Tic Tac Toe!


     |     |
     |     |     
     |     |
-----+-----+-----
     |     |
     |     |     
     |     |
-----+-----+-----
     |     |
     |     |     
     |     |

Thanks for playing Tic Tac Toe! Goodbye!
#+end_example

*** Step 3 - Human Moves
**** Add human moves to `TTTGame#play` method
The next part of the game prompts the user for their square choice and validates that choice. We display the board again to see the human player's choice reflected on the board.

#+begin_src ruby :results output :exports both
def play
  display_welcome_message
  loop do
    display_board
    human_moves       # <--- Added this
    display_board     # <--- Added this
    break
  end
  display_goodbye_message
end
#+end_src

**** Add `TTTGame#human_moves` method
This is where the logic for retrieving and validating the human player's choice lives.

#+begin_src ruby :results output :exports both
def human_moves
  puts "Chose a square between 1-9: "
  square = nil

  loop do
    square = gets.chomp.to_i
    break if (1..9).cover?(square)

    puts "Sorry, that's not a valid choice."
  end

  board.set_square_at(square, human.marker)
end
#+end_src

We prompt the user to make a choice between 1 and 9. We start a loop to retrieve the choice and validate it to make sure the user entered an Integer between 1 and 9. Finally, we call the ~Board#set_square_at~ instance method and pass in the human player's choice ~square~ and the human's marker as arguments.

**** Add `Board#set_square_at`
This is the method in the ~Board~ class that puts the human marker in the chosen square. 

#+begin_src ruby :results output :exports both
def set_square_at(key, marker)
  @squares[key].marker = marker
end
#+end_src

In order for this to work, we need to add ~attr_accessor :marker~ to the ~Square~ class to have access to the built-in setter method.

**** Full Code
#+begin_src ruby :results output :exports both
class Board
  INITIAL_MARKER = ' '

  def initialize
    @squares = {}
    (1..9).each { |key| @squares[key] = Square.new(INITIAL_MARKER) }
  end

  def get_square_at(key)
    @squares[key]
  end

  def set_square_at(key, marker)
    @squares[key].marker = marker
  end
end

class Square
  attr_accessor :marker

  def initialize(marker)
    @marker = marker
  end

  def to_s
    @marker
  end
end

class Player
  attr_reader :marker

  def initialize(marker)
    @marker = marker
  end
end

class TTTGame
  attr_reader :board, :human, :computer

  def initialize
    @board = Board.new
    @human = Player.new("X")
    @computer = Player.new("O")
  end

  def display_welcome_message
    puts "Welcome to Tic Tac Toe!"
    puts ""
  end

  def display_goodbye_message
    puts "Thanks for playing Tic Tac Toe! Goodbye!"
  end

  def display_board
    puts ""
    puts "     |     |"
    puts "  #{board.get_square_at(1)}  |  #{board.get_square_at(2)}  |  #{board.get_square_at(3)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(4)}  |  #{board.get_square_at(5)}  |  #{board.get_square_at(6)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(7)}  |  #{board.get_square_at(8)}  |  #{board.get_square_at(9)}  "
    puts "     |     |"
    puts ""
  end

  def human_moves
    puts "Choose a square between 1-9: "
    square = nil

    loop do
      square = gets.chomp.to_i
      break if (1..9).cover?(square)

      puts "Sorry, that's not a valid choice."
    end

    board.set_square_at(square, human.marker)
  end

  def play
    display_welcome_message
    loop do
      display_board
      human_moves
      display_board
      break
    end
    display_goodbye_message
  end
end

game = TTTGame.new
game.play
#+end_src

*** Step 4 - Computer Moves
**** Add computer moves to `TTTGame#play` method
Now, we want to get the computer player's move.

#+begin_src ruby :results output :exports both
def play
  display_welcome_message
  loop do
    display_board
    human_moves
    display_board
    computer_moves    # <--- Added this
    display_board     # <--- Added this
    break
  end
  display_goodbye_message
end
#+end_src

**** Add `TTTGame#computer_moves` method
This is where the logic for the computer player's move resides.

#+begin_src ruby :results output :exports both
def computer_moves
  board.set_square_at((1..9).to_a.sample, computer.marker)
end
#+end_src

**** Use constants in `TTTGame` class for markers instead of hard-coding markers
#+begin_src ruby :results output :exports both
class TTTGame
  HUMAN_MARKER = "X"
  COMPUTER_MARKER = "O"

  attr_reader :board, :human, :computer

  def initialize
    @board = Board.new
    @human = Player.new(HUMAN_MARKER)
    @computer = Player.new(COMPUTER_MARKER)
  end

  # ... other code
end
#+end_src

**** Full Code
#+begin_src ruby :results output :exports both
class Board
  INITIAL_MARKER = ' '

  def initialize
    @squares = {}
    (1..9).each { |key| @squares[key] = Square.new(INITIAL_MARKER) }
  end

  def get_square_at(key)
    @squares[key]
  end

  def set_square_at(key, marker)
    @squares[key].marker = marker
  end
end

class Square
  attr_accessor :marker

  def initialize(marker)
    @marker = marker
  end

  def to_s
    @marker
  end
end

class Player
  attr_reader :marker

  def initialize(marker)
    @marker = marker
  end
end

class TTTGame
  HUMAN_MARKER = "X"
  COMPUTER_MARKER = "O"

  attr_reader :board, :human, :computer

  def initialize
    @board = Board.new
    @human = Player.new(HUMAN_MARKER)
    @computer = Player.new(COMPUTER_MARKER)
  end

  def display_welcome_message
    puts "Welcome to Tic Tac Toe!"
    puts ""
  end

  def display_goodbye_message
    puts "Thanks for playing Tic Tac Toe! Goodbye!"
  end

  def display_board
    puts ""
    puts "     |     |"
    puts "  #{board.get_square_at(1)}  |  #{board.get_square_at(2)}  |  #{board.get_square_at(3)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(4)}  |  #{board.get_square_at(5)}  |  #{board.get_square_at(6)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(7)}  |  #{board.get_square_at(8)}  |  #{board.get_square_at(9)}  "
    puts "     |     |"
    puts ""
  end

  def human_moves
    puts "Choose a square between 1-9: "
    square = nil

    loop do
      square = gets.chomp.to_i
      break if (1..9).cover?(square)

      puts "Sorry, that's not a valid choice."
    end

    board.set_square_at(square, human.marker)
  end

  def computer_moves
    board.set_square_at((1..9).to_a.sample, computer.marker)
  end

  def play
    display_welcome_message
    loop do
      display_board
      human_moves
      display_board
      computer_moves    # <--- Added this
      display_board     # <--- Added this
      break
    end
    display_goodbye_message
  end
end

game = TTTGame.new
game.play
#+end_src

*** Step 5 - Take turns
**** Engage `TTTGame#play` loop
Now we make the game play loop work correctly. It will continue infinitely for now.

#+begin_src ruby :results output :exports both
def play
  display_welcome_message
  display_board       # <--- Display an empty board

  loop do
    human_moves
    computer_moves
    display_board     # <--- Display board with human and computer choices
  end

  display_goodbye_message
end
#+end_src

**** Add `Board#empty_squares` method
Currently, it's possible for either player to choose a square that already has a marker present. We fix this so only the empty squares are available as choices.

#+begin_src ruby :results output :exports both
class Board
  INITIAL_MARKER = ' '

  # ... other code

  def empty_squares
    @squares.select { |_, square| square.unmarked? }.keys
  end
end
#+end_src

**** Add `Square#unmarked` method
Returns ~true~ if a square is unmarked or does not have a marker in it.

#+begin_src ruby :results output :exports both
class Square
  # ... other code

  def unmarked?
    marker == Board::INITIAL_MARKER
  end
end
#+end_src

**** Modify `TTTGame#human_moves` method to use `Board#empty_squares`
Now we modify ~TTTGame#human_moves~ to use this new ~Board#empty_squares~ method.

#+begin_src ruby :results output :exports both
def human_moves
  puts "Choose a square between (#{board.empty_squares}): "
  square = nil

  loop do
    square = gets.chomp.to_i
    break if board.empty_squares.cover?(square)

    puts "Sorry, that's not a valid choice."
  end

  board.set_square_at(square, human.marker)
end
#+end_src

**** Modify `TTTGame#computer_moves` method to use `Board#empty_squares`
#+begin_src ruby :results output :exports both
def computer_moves
  board.set_square_at(board.empty_squares.to_a.sample, computer.marker)
end
#+end_src

**** Full Code
#+begin_src ruby :results output :exports both
class Board
  INITIAL_MARKER = ' '

  def initialize
    @squares = {}
    (1..9).each { |key| @squares[key] = Square.new(INITIAL_MARKER) }
  end

  def get_square_at(key)
    @squares[key]
  end

  def set_square_at(key, marker)
    @squares[key].marker = marker
  end

  def empty_squares
    @squares.select { |_, square| square.unmarked? }.keys
  end
end

class Square
  attr_accessor :marker

  def initialize(marker)
    @marker = marker
  end

  def to_s
    @marker
  end

  def unmarked?
    marker == Board::INITIAL_MARKER
  end
end

class Player
  attr_reader :marker

  def initialize(marker)
    @marker = marker
  end
end

class TTTGame
  HUMAN_MARKER = "X"
  COMPUTER_MARKER = "O"

  attr_reader :board, :human, :computer

  def initialize
    @board = Board.new
    @human = Player.new(HUMAN_MARKER)
    @computer = Player.new(COMPUTER_MARKER)
  end

  def display_welcome_message
    puts "Welcome to Tic Tac Toe!"
    puts ""
  end

  def display_goodbye_message
    puts "Thanks for playing Tic Tac Toe! Goodbye!"
  end

  def display_board
    puts ""
    puts "     |     |"
    puts "  #{board.get_square_at(1)}  |  #{board.get_square_at(2)}  |  #{board.get_square_at(3)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(4)}  |  #{board.get_square_at(5)}  |  #{board.get_square_at(6)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(7)}  |  #{board.get_square_at(8)}  |  #{board.get_square_at(9)}  "
    puts "     |     |"
    puts ""
  end

  def human_moves
    puts "Choose a square (#{board.empty_squares.join(', ')}): "
    square = nil

    loop do
      square = gets.chomp.to_i
      break if board.empty_squares.include?(square)

      puts "Sorry, that's not a valid choice."
    end

    board.set_square_at(square, human.marker)
  end

  def computer_moves
    board.set_square_at(board.empty_squares.to_a.sample, computer.marker)
  end

  def play
    display_welcome_message
    display_board

    loop do
      human_moves
      computer_moves
      display_board
    end

    display_goodbye_message
  end
end

game = TTTGame.new
game.play
#+end_src

*** Step 6 - Break when board is full
**** Modify `TTTGame#play` loop to add call to `Board#full?` method
We need to be able to break out of the loop when the board is full.
#+begin_src ruby :results output :exports both
def play
  display_welcome_message
  display_board

  loop do
    human_moves
    break if board.full?   # <--- Added

    computer_moves
    break if board.full?   # <--- Added

    display_board
  end

  display_goodbye_message
end
#+end_src

**** Add `Board#full?` class
#+begin_src ruby :results output :exports both
class Board
  # ... other code

  def full?
    empty_squares.empty?
  end
end
#+end_src

**** Clear the screen before every `TTTGame#display_board` method call
#+begin_src ruby :results output :exports both
def display_board
  system 'clear' || system 'cls'
  puts ""
  puts "     |     |"
  puts "  #{board.get_square_at(1)}  |  #{board.get_square_at(2)}  |  #{board.get_square_at(3)}  "
  puts "     |     |"
  puts "-----+-----+-----"
  puts "     |     |"
  puts "  #{board.get_square_at(4)}  |  #{board.get_square_at(5)}  |  #{board.get_square_at(6)}  "
  puts "     |     |"
  puts "-----+-----+-----"
  puts "     |     |"
  puts "  #{board.get_square_at(7)}  |  #{board.get_square_at(8)}  |  #{board.get_square_at(9)}  "
  puts "     |     |"
  puts ""
end
#+end_src

**** Modify `TTTGame#play` method and add `TTTGame#display_result` method
We'd like to display the result of the game after we exit the main game loop.

#+begin_src ruby :results output :exports both
def display_result
  display_board
  puts "The board is full!"
end

def play
  display_welcome_message
  display_board

  loop do
    human_moves
    break if board.full?

    computer_moves
    break if board.full?

    display_board
  end

  display_result           # <--- Added
  display_goodbye_message
end
#+end_src

**** Show human and computer marker in header of `TTTGame#display_board` method
#+begin_src ruby :results output :exports both
def display_board
  system 'clear' || system 'cls'
  puts "You're a #{human.marker}. Computer is a #{computer.marker}."
  puts ""
  puts "     |     |"
  puts "  #{board.get_square_at(1)}  |  #{board.get_square_at(2)}  |  #{board.get_square_at(3)}  "
  puts "     |     |"
  puts "-----+-----+-----"
  puts "     |     |"
  puts "  #{board.get_square_at(4)}  |  #{board.get_square_at(5)}  |  #{board.get_square_at(6)}  "
  puts "     |     |"
  puts "-----+-----+-----"
  puts "     |     |"
  puts "  #{board.get_square_at(7)}  |  #{board.get_square_at(8)}  |  #{board.get_square_at(9)}  "
  puts "     |     |"
  puts ""
end
#+end_src

**** Move `INITIAL_MARKER` constant to `Square` class
Since ~INITIAL_MARKER~ is only used in the ~Square~ class, it's better if it's located there.

#+begin_src ruby :results output :exports both
class Board
  def initialize
    @squares = {}
    (1..9).each { |key| @squares[key] = Square.new }
  end

  # .. other code
end

class Square
  INITIAL_MARKER = ' '

  attr_accessor :marker

  def initialize(marker=INITIAL_MARKER)
    @marker = marker
  end

  def to_s
    @marker
  end

  def unmarked?
    marker == INITIAL_MARKER
  end
end
#+end_src

**** Full Code

#+begin_src ruby :results output :exports both
class Board
  def initialize
    @squares = {}
    (1..9).each { |key| @squares[key] = Square.new }
  end

  def get_square_at(key)
    @squares[key]
  end

  def set_square_at(key, marker)
    @squares[key].marker = marker
  end

  def empty_squares
    @squares.select { |_, square| square.unmarked? }.keys
  end

  def full?
    empty_squares.empty?
  end
end

class Square
  INITIAL_MARKER = ' '

  attr_accessor :marker

  def initialize(marker=INITIAL_MARKER)
    @marker = marker
  end

  def to_s
    @marker
  end

  def unmarked?
    marker == INITIAL_MARKER
  end
end

class Player
  attr_reader :marker

  def initialize(marker)
    @marker = marker
  end
end

class TTTGame
  HUMAN_MARKER = "X"
  COMPUTER_MARKER = "O"

  attr_reader :board, :human, :computer

  def initialize
    @board = Board.new
    @human = Player.new(HUMAN_MARKER)
    @computer = Player.new(COMPUTER_MARKER)
  end

  def display_welcome_message
    puts "Welcome to Tic Tac Toe!"
    puts ""
  end

  def display_goodbye_message
    puts "Thanks for playing Tic Tac Toe! Goodbye!"
  end

  def display_board
    #system 'clear' || system 'cls'
    puts "You're a #{human.marker}. Computer is a #{computer.marker}."
    puts ""
    puts "     |     |"
    puts "  #{board.get_square_at(1)}  |  #{board.get_square_at(2)}  |  #{board.get_square_at(3)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(4)}  |  #{board.get_square_at(5)}  |  #{board.get_square_at(6)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(7)}  |  #{board.get_square_at(8)}  |  #{board.get_square_at(9)}  "
    puts "     |     |"
    puts ""
  end

  def human_moves
    puts "Choose a square (#{board.empty_squares.join(', ')}): "
    square = nil

    loop do
      square = gets.chomp.to_i
      break if board.empty_squares.include?(square)

      puts "Sorry, that's not a valid choice."
    end

    board.set_square_at(square, human.marker)
  end

  def computer_moves
    board.set_square_at(board.empty_squares.to_a.sample, computer.marker)
  end

  def display_result
    display_board
    puts "The board is full!"
  end

  def play
    display_welcome_message
    display_board

    loop do
      human_moves
      break if board.full?

      computer_moves
      break if board.full?

      display_board
    end

    display_result
    display_goodbye_message
  end
end

game = TTTGame.new
game.play
#+end_src

*** Step 7 - Detect winner
**** Modify `TTTGame#play` loop to add call to `Board#someone_won?` method
#+begin_src ruby :results output :exports both
def play
  display_welcome_message
  display_board

  loop do
    human_moves
    break if board.someone_won? || board.full?  # <-- Modified

    computer_moves
    break if board.someone_won? || board.full?  # <-- Modified

    display_board
  end

  display_result
  display_goodbye_message
end
#+end_src

**** Add `Board#someone_won?` and `Board#detect_winner` methods, add `WINNING_LINES` constant
We need to know if someone won, not who won.

#+begin_src ruby :results output :exports both
class Board
  WINNING_LINES = [[1, 2, 3], [4, 5, 6], [7, 8, 9]] +
                  [[1, 4, 7], [2, 5, 8], [3, 6, 9]] +
                  [[1, 5, 9], [3, 5, 7]]

  # ... other code

  def someone_won?
    !!detect_winner
  end

  def detect_winner
    WINNING_LINES.each do |line|
      if @squares[line[0]].marker == TTTGame::HUMAN_MARKER &&
         @squares[line[1]].marker == TTTGame::HUMAN_MARKER &&
         @squares[line[2]].marker == TTTGame::HUMAN_MARKER
        return TTTGame::HUMAN_MARKER
      elsif @squares[line[0]].marker == TTTGame::COMPUTER_MARKER &&
            @squares[line[1]].marker == TTTGame::COMPUTER_MARKER &&
            @squares[line[2]].marker == TTTGame::COMPUTER_MARKER
        return TTTGame::COMPUTER_MARKER
      end
    end

    nil
  end
end
#+end_src

**** Modify `TTTGame#display_result` method to show winner
Currently, the ~TTTGame#display_result~ method always shows a tie message. Let's change it to show the actual winner message.

#+begin_src ruby :results output :exports both
def display_result
  display_board

  return puts "You won!" if board.detect_winner == human.marker
  return puts "Computer won!" if board.detect_winner == computer.marker
  puts "It's a tie!"
end
#+end_src

**** Full Code
#+begin_src ruby :results output :exports both
class Board
  WINNING_LINES = [[1, 2, 3], [4, 5, 6], [7, 8, 9]] +
                  [[1, 4, 7], [2, 5, 8], [3, 6, 9]] +
                  [[1, 5, 9], [3, 5, 7]]

  def initialize
    @squares = {}
    (1..9).each { |key| @squares[key] = Square.new }
  end

  def get_square_at(key)
    @squares[key]
  end

  def set_square_at(key, marker)
    @squares[key].marker = marker
  end

  def empty_squares
    @squares.select { |_, square| square.unmarked? }.keys
  end

  def full?
    empty_squares.empty?
  end

  def someone_won?
    !!detect_winner
  end

  def detect_winner
    WINNING_LINES.each do |line|
      if @squares[line[0]].marker == TTTGame::HUMAN_MARKER &&
         @squares[line[1]].marker == TTTGame::HUMAN_MARKER &&
         @squares[line[2]].marker == TTTGame::HUMAN_MARKER
        return TTTGame::HUMAN_MARKER
      elsif @squares[line[0]].marker == TTTGame::COMPUTER_MARKER &&
            @squares[line[1]].marker == TTTGame::COMPUTER_MARKER &&
            @squares[line[2]].marker == TTTGame::COMPUTER_MARKER
        return TTTGame::COMPUTER_MARKER
      end
    end

    nil
  end
end

class Square
  INITIAL_MARKER = ' '

  attr_accessor :marker

  def initialize(marker=INITIAL_MARKER)
    @marker = marker
  end

  def to_s
    @marker
  end

  def unmarked?
    marker == INITIAL_MARKER
  end
end

class Player
  attr_reader :marker

  def initialize(marker)
    @marker = marker
  end
end

class TTTGame
  HUMAN_MARKER = "X"
  COMPUTER_MARKER = "O"

  attr_reader :board, :human, :computer

  def initialize
    @board = Board.new
    @human = Player.new(HUMAN_MARKER)
    @computer = Player.new(COMPUTER_MARKER)
  end

  def display_welcome_message
    puts "Welcome to Tic Tac Toe!"
    puts ""
  end

  def display_goodbye_message
    puts "Thanks for playing Tic Tac Toe! Goodbye!"
  end

  def display_board
    #system 'clear' || system 'cls'
    puts "You're a #{human.marker}. Computer is a #{computer.marker}."
    puts ""
    puts "     |     |"
    puts "  #{board.get_square_at(1)}  |  #{board.get_square_at(2)}  |  #{board.get_square_at(3)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(4)}  |  #{board.get_square_at(5)}  |  #{board.get_square_at(6)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(7)}  |  #{board.get_square_at(8)}  |  #{board.get_square_at(9)}  "
    puts "     |     |"
    puts ""
  end

  def human_moves
    puts "Choose a square (#{board.empty_squares.join(', ')}): "
    square = nil

    loop do
      square = gets.chomp.to_i
      break if board.empty_squares.include?(square)

      puts "Sorry, that's not a valid choice."
    end

    board.set_square_at(square, human.marker)
  end

  def computer_moves
    board.set_square_at(board.empty_squares.to_a.sample, computer.marker)
  end

def display_result
  display_board

  return puts "You won!" if board.detect_winner == human.marker
  return puts "Computer won!" if board.detect_winner == computer.marker
  puts "It's a tie!"
end

def play
  display_welcome_message
  display_board

  loop do
    human_moves
    break if board.someone_won? || board.full?

    computer_moves
    break if board.someone_won? || board.full?

    display_board
  end

  display_result
  display_goodbye_message
end
end

game = TTTGame.new
game.play
#+end_src

*** Step 8 - Refactor `detect_winner`
**** Simplify `if` statement, add `*_human_marker` methods
#+begin_src ruby :results output :exports both
def count_human_marker(squares)
end

def count_computer_marker(squares)
end

def detect_winner
  WINNING_LINES.each do |line|
    if count_human_marker() == 3
      return TTTGame::HUMAN_MARKER
    elsif count_computer_marker() == 3
      return TTTGame::COMPUTER_MARKER
    end
  end

  nil
end
#+end_src

**** Pass in only the squares we want to count per winning line
#+begin_src ruby :results output :exports both
def count_human_marker(squares)
  squares.collect(&:marker).count(TTTGame::HUMAN_MARKER)
end

def count_computer_marker(squares)
  squares.collect(&:marker).count(TTTGame::COMPUTER_MARKER)
end

def detect_winner
  WINNING_LINES.each do |line|
    if count_human_marker(@squares.values_at(*line)) == 3
      return TTTGame::HUMAN_MARKER
    elsif count_computer_marker(@squares.values_at(*line)) == 3
      return TTTGame::COMPUTER_MARKER
    end
  end

  nil
end
#+end_src

**** Full Code
#+begin_src ruby :results output :exports both
class Board
  WINNING_LINES = [[1, 2, 3], [4, 5, 6], [7, 8, 9]] +
                  [[1, 4, 7], [2, 5, 8], [3, 6, 9]] +
                  [[1, 5, 9], [3, 5, 7]]

  def initialize
    @squares = {}
    (1..9).each { |key| @squares[key] = Square.new }
  end

  def get_square_at(key)
    @squares[key]
  end

  def set_square_at(key, marker)
    @squares[key].marker = marker
  end

  def empty_squares
    @squares.select { |_, square| square.unmarked? }.keys
  end

  def full?
    empty_squares.empty?
  end

  def someone_won?
    !!detect_winner
  end

  def count_human_marker(squares)
    squares.collect(&:marker).count(TTTGame::HUMAN_MARKER)
  end

  def count_computer_marker(squares)
    squares.collect(&:marker).count(TTTGame::COMPUTER_MARKER)
  end

  def detect_winner
    WINNING_LINES.each do |line|
      if count_human_marker(@squares.values_at(*line)) == 3
        return TTTGame::HUMAN_MARKER
      elsif count_computer_marker(@squares.values_at(*line)) == 3
        return TTTGame::COMPUTER_MARKER
      end
    end

    nil
  end
end

class Square
  INITIAL_MARKER = ' '

  attr_accessor :marker

  def initialize(marker=INITIAL_MARKER)
    @marker = marker
  end

  def to_s
    @marker
  end

  def unmarked?
    marker == INITIAL_MARKER
  end
end

class Player
  attr_reader :marker

  def initialize(marker)
    @marker = marker
  end
end

class TTTGame
  HUMAN_MARKER = "X"
  COMPUTER_MARKER = "O"

  attr_reader :board, :human, :computer

  def initialize
    @board = Board.new
    @human = Player.new(HUMAN_MARKER)
    @computer = Player.new(COMPUTER_MARKER)
  end

  def display_welcome_message
    puts "Welcome to Tic Tac Toe!"
    puts ""
  end

  def display_goodbye_message
    puts "Thanks for playing Tic Tac Toe! Goodbye!"
  end

  def display_board
    #system 'clear' || system 'cls'
    puts "You're a #{human.marker}. Computer is a #{computer.marker}."
    puts ""
    puts "     |     |"
    puts "  #{board.get_square_at(1)}  |  #{board.get_square_at(2)}  |  #{board.get_square_at(3)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(4)}  |  #{board.get_square_at(5)}  |  #{board.get_square_at(6)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(7)}  |  #{board.get_square_at(8)}  |  #{board.get_square_at(9)}  "
    puts "     |     |"
    puts ""
  end

  def human_moves
    puts "Choose a square (#{board.empty_squares.join(', ')}): "
    square = nil

    loop do
      square = gets.chomp.to_i
      break if board.empty_squares.include?(square)

      puts "Sorry, that's not a valid choice."
    end

    board.set_square_at(square, human.marker)
  end

  def computer_moves
    board.set_square_at(board.empty_squares.to_a.sample, computer.marker)
  end

  def display_result
    display_board

    return puts "You won!" if board.detect_winner == human.marker
    return puts "Computer won!" if board.detect_winner == computer.marker
    puts "It's a tie!"
  end

  def play
    display_welcome_message
    display_board

    loop do
      human_moves
      break if board.someone_won? || board.full?

      computer_moves
      break if board.someone_won? || board.full?

      display_board
    end

    display_result
    display_goodbye_message
  end
end

game = TTTGame.new
game.play
#+end_src

*** Step 9 - Play again
**** Add play again loop to `TTTGame#play` method
Add an outer loop so the welcome and goodbye messages are only displayed once per game.

#+begin_src ruby :results output :exports both
def play
  display_welcome_message

  loop do                                         # <--- Added
    display_board

    loop do
      human_moves
      break if board.someone_won? || board.full?

      computer_moves
      break if board.someone_won? || board.full?

      display_board
    end

    display_result
    break unless play_again?
    board.reset                                    # <--- Added
  end                                              # <--- Added

  display_goodbye_message
end
#+end_src

**** Add `TTTGame#play_again?` method
#+begin_src ruby :results output :exports both
def play_again?
  answer = nil

  loop do
    puts "Do you want to play again? (y | n)"
    answer = gets.chomp.downcase
    break if %w(y n).include? answer
    puts "Sorry, must be y or n."
  end

  answer == 'y'
end
#+end_src

**** Add `Board#reset` method
#+begin_src ruby :results output :exports both
class Board
  WINNING_LINES = [[1, 2, 3], [4, 5, 6], [7, 8, 9]] +
                  [[1, 4, 7], [2, 5, 8], [3, 6, 9]] +
                  [[1, 5, 9], [3, 5, 7]]

  def initialize
    @squares = {}
    reset
  end

  # ... other code

  def reset
    (1..9).each { |key| @squares[key] = Square.new }
  end
end
#+end_src

**** Full Code
#+begin_src ruby :results output :exports both
class Board
  WINNING_LINES = [[1, 2, 3], [4, 5, 6], [7, 8, 9]] +
                  [[1, 4, 7], [2, 5, 8], [3, 6, 9]] +
                  [[1, 5, 9], [3, 5, 7]]

  def initialize
    @squares = {}
    reset
  end

  def get_square_at(key)
    @squares[key]
  end

  def set_square_at(key, marker)
    @squares[key].marker = marker
  end

  def empty_squares
    @squares.select { |_, square| square.unmarked? }.keys
  end

  def full?
    empty_squares.empty?
  end

  def someone_won?
    !!detect_winner
  end

  def count_human_marker(squares)
    squares.collect(&:marker).count(TTTGame::HUMAN_MARKER)
  end

  def count_computer_marker(squares)
    squares.collect(&:marker).count(TTTGame::COMPUTER_MARKER)
  end

  def detect_winner
    WINNING_LINES.each do |line|
      if count_human_marker(@squares.values_at(*line)) == 3
        return TTTGame::HUMAN_MARKER
      elsif count_computer_marker(@squares.values_at(*line)) == 3
        return TTTGame::COMPUTER_MARKER
      end
    end

    nil
  end

  def reset
    (1..9).each { |key| @squares[key] = Square.new }
  end
end

class Square
  INITIAL_MARKER = ' '

  attr_accessor :marker

  def initialize(marker=INITIAL_MARKER)
    @marker = marker
  end

  def to_s
    @marker
  end

  def unmarked?
    marker == INITIAL_MARKER
  end
end

class Player
  attr_reader :marker

  def initialize(marker)
    @marker = marker
  end
end

class TTTGame
  HUMAN_MARKER = "X"
  COMPUTER_MARKER = "O"

  attr_reader :board, :human, :computer

  def initialize
    @board = Board.new
    @human = Player.new(HUMAN_MARKER)
    @computer = Player.new(COMPUTER_MARKER)
  end

  def display_welcome_message
    puts "Welcome to Tic Tac Toe!"
    puts ""
  end

  def display_goodbye_message
    puts "Thanks for playing Tic Tac Toe! Goodbye!"
  end

  def display_board
    #system 'clear' || system 'cls'
    puts "You're a #{human.marker}. Computer is a #{computer.marker}."
    puts ""
    puts "     |     |"
    puts "  #{board.get_square_at(1)}  |  #{board.get_square_at(2)}  |  #{board.get_square_at(3)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(4)}  |  #{board.get_square_at(5)}  |  #{board.get_square_at(6)}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{board.get_square_at(7)}  |  #{board.get_square_at(8)}  |  #{board.get_square_at(9)}  "
    puts "     |     |"
    puts ""
  end

  def human_moves
    puts "Choose a square (#{board.empty_squares.join(', ')}): "
    square = nil

    loop do
      square = gets.chomp.to_i
      break if board.empty_squares.include?(square)

      puts "Sorry, that's not a valid choice."
    end

    board.set_square_at(square, human.marker)
  end

  def computer_moves
    board.set_square_at(board.empty_squares.to_a.sample, computer.marker)
  end

  def display_result
    display_board

    return puts "You won!" if board.detect_winner == human.marker
    return puts "Computer won!" if board.detect_winner == computer.marker
    puts "It's a tie!"
  end

def play_again?
  answer = nil

  loop do
    puts "Do you want to play again? (y | n)"
    answer = gets.chomp.downcase
    break if %w(y n).include? answer
    puts "Sorry, must be y or n."
  end

  answer == 'y'
end

def play
  display_welcome_message

  loop do
    display_board

    loop do
      human_moves
      break if board.someone_won? || board.full?

      computer_moves
      break if board.someone_won? || board.full?

      display_board
    end

    display_result
    break unless play_again?
    board.reset
  end

  display_goodbye_message
end
end

game = TTTGame.new
game.play
#+end_src

