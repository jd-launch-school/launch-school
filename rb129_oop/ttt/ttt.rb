module Utilities
  def clear_screen
    system('clear') || system('cls')
  end

  # :reek:DuplicateMethodCall
  def joinor(array, sep = ', ', word = 'or')
    if array.size >= 3
      "#{array[0..-2].join(sep)}#{sep}#{word}" \
      " #{array.last}"
    elsif array.size == 2
      "#{array.first} #{word} #{array.last}"
    else
      array.first.to_s
    end
  end
end

module Display
  # This method smells of :reek:DuplicateMethodCall
  def display_welcome_message(max_wins)
    puts "Welcome to Tic Tac Toe!"
    puts ""
    puts "Try to be the first player to win #{max_wins} games!"
    puts ""
    puts "Press enter to continue."
    gets
  end

  def display_play_again_message
    puts "Let's play again!"
    puts ""
  end

  def display_goodbye_message
    puts "Thanks for playing Tic Tac Toe! Goodbye!"
  end
end

class Board
  WINNING_LINES = [[1, 2, 3], [4, 5, 6], [7, 8, 9]] +
                  [[1, 4, 7], [2, 5, 8], [3, 6, 9]] +
                  [[1, 5, 9], [3, 5, 7]]

  def initialize
    @squares = {}
    reset
  end

  def [](position)
    @squares[position]
  end

  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/MethodLength
  # :reek:DuplicateMethodCall
  def draw
    puts "     |     |"
    puts "  #{@squares[1]}  |  #{@squares[2]}  |  #{@squares[3]}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{@squares[4]}  |  #{@squares[5]}  |  #{@squares[6]}  "
    puts "     |     |"
    puts "-----+-----+-----"
    puts "     |     |"
    puts "  #{@squares[7]}  |  #{@squares[8]}  |  #{@squares[9]}  "
    puts "     |     |"
  end
  # rubocop:enable Metrics/MethodLength
  # rubocop:enable Metrics/AbcSize

  def empty_squares
    @squares.select { |_, square| square.unmarked? }.keys
  end

  def someone_won?
    !!winning_marker
  end

  def full?
    empty_squares.empty?
  end

  def potential_winning_lines
    winning_lines.select(&:potential_win?)
  end

  def winning_marker
    winning_lines.find(&:win?)&.winning_marker
  end

  def random_square
    @squares[empty_squares.sample]
  end

  def reset
    (1..9).each { |key| @squares[key] = Square.new }
  end

  private

  def squares_in_line(line)
    line.collect { |position| @squares[position] }
  end

  def winning_lines
    WINNING_LINES.collect do |winning_line|
      Line.new(squares_in_line(winning_line))
    end
  end
end

# This class smells of :reek:Attribute
class Square
  INITIAL_MARKER = ' '

  attr_reader :marker

  def initialize(marker=INITIAL_MARKER)
    @marker = marker
  end

  def to_s
    marker
  end

  def mark(marker)
    @marker = marker if unmarked?
  end

  def unmarked?
    marker == INITIAL_MARKER
  end

  def marked?
    marker != INITIAL_MARKER
  end
end

class Line
  attr_reader :values

  def initialize(line)
    @values = line
  end

  def win?
    num_markers?(3) && identical_markers?
  end

  def potential_win?
    num_markers?(2) && identical_markers?
  end

  def player_markers
    values.select(&:marked?).collect(&:marker)
  end

  def empty_square
    values[all_markers.index(Square::INITIAL_MARKER)]
  end

  def winning_marker
    values.first.marker if win?
  end

  private

  def all_markers
    values.collect(&:marker)
  end

  def num_markers?(num)
    player_markers.size == num
  end

  def identical_markers?
    player_markers.min == player_markers.max
  end
end

class Player
  attr_reader :name, :marker, :score

  def initialize
    @name = retrieve_name
    @score = 0
  end

  def to_s
    name.capitalize
  end

  def increment_score
    @score += 1
  end

  def reset_score
    @score = 0
  end
end

class Human < Player
  include Utilities

  attr_reader :choice

  def initialize
    super
    @marker = retrieve_marker
  end

  def next_move(board)
    retrieve_choice(board.empty_squares)
    board[choice].mark(marker)
  end

  private

  def retrieve_name
    clear_screen
    puts "What is your name?"

    loop do
      input = gets.chomp
      return input if valid_name?(input)

      puts "Sorry, your name must be 2-15 characters from a-z."
    end
  end

  def valid_name?(input)
    regex = /^[a-z]{2,15}$/i
    input.match?(regex)
  end

  def retrieve_marker
    clear_screen
    puts "Enter a character to use as your marker:"

    loop do
      input = gets.chomp
      return input if valid_marker?(input)

      puts "Sorry, your marker cannot be a space and must be 1 character."
    end
  end

  def valid_marker?(input)
    input.size == 1 && input != Square::INITIAL_MARKER
  end

  def retrieve_choice(empty_squares)
    puts "Choose a square (#{joinor(empty_squares)}): "

    loop do
      @choice = gets.chomp.to_i
      break if empty_squares.include?(choice)

      puts "Sorry, that's not a valid choice."
    end
  end
end

# This class smells of :reek:Attribute
class Computer < Player
  attr_accessor :marker

  def initialize
    super
    @marker = 'O'
  end

  def switch_marker
    @marker = 'X'
  end

  def next_move(board)
    board[5].mark(marker) ||
      best_move(board) ||
      board[1].mark(marker) ||
      board.random_square.mark(marker)
  end

  private

  def best_move(board)
    potential_winning_lines = board.potential_winning_lines

    return if potential_winning_lines.empty?

    potential_winning_lines.each do |line|
      if line.player_markers.all?(marker)
        return line.empty_square.mark(marker)
      end
    end

    winning_line = potential_winning_lines.first
    winning_line.empty_square.mark(marker)
  end

  def retrieve_name
    @name = ['Computer', 'Hal', 'Aliza', 'Bambi', '42', 'Kit'].sample
  end
end

# This class smells of :reek:RepeatedConditional
class TTTGame
  include Utilities
  include Display

  MAX_WINS = 2

  attr_reader :board, :human, :computer

  # This method smells of :reek:DuplicateMethodCall
  def initialize
    clear_screen
    display_welcome_message(MAX_WINS)

    @board = Board.new
    @human = Human.new
    @computer = Computer.new
    @computer.switch_marker if human.marker == computer.marker
    @current_marker = human.marker

    play
  end

  # rubocop:disable Metrics/MethodLength
  def play
    clear_screen

    loop do
      play_rounds
      display_result
      winner&.increment_score
      display_score
      display_grand_winner if grand_winner?
      break unless play_again?

      reset
      display_play_again_message
    end

    display_goodbye_message
  end
  # rubocop:enable Metrics/MethodLength

  private

  # This method smells of :reek:DuplicateMethodCall
  def display_board
    puts "#{human} is #{human.marker}. #{computer} is #{computer.marker}."
    display_score
    puts ""
    board.draw
    puts ""
  end

  # This method smells of :reek:DuplicateMethodCall
  def display_grand_winner
    message = " is the GRAND WINNER!"
    star_line = "*" * (winner.name.size + message.size)

    puts ""
    puts star_line
    puts "#{winner}#{message}"
    puts star_line
  end

  def display_result
    clear_screen_and_display_board

    return puts "#{human} won!" if winner == human
    return puts "#{computer} won!" if winner == computer

    puts "It's a tie!"
  end

  def display_score
    puts "#{human}: #{human.score} | #{computer}: #{computer.score}"
  end

  def clear_screen_and_display_board
    clear_screen
    display_board
  end

  def play_rounds
    display_board

    loop do
      current_player_moves
      break if board.someone_won? || board.full?

      clear_screen_and_display_board if human_turn?
    end
  end

  def current_player_moves
    human_turn? ? human.next_move(board) : computer.next_move(board)
    @current_marker = human_turn? ? computer.marker : human.marker
  end

  def human_turn?
    @current_marker == human.marker
  end

  def winner
    winner = board.winning_marker
    return unless winner

    winner == human.marker ? human : computer
  end

  def grand_winner?
    winner&.score == MAX_WINS
  end

  def play_again?
    answer = nil

    loop do
      puts ""
      puts "Do you want to play again? (y | n)"
      answer = gets.chomp.downcase
      break if %w(y n).include? answer

      puts "Sorry, must be y or n."
    end

    answer == 'y'
  end

  def reset_scores
    human.reset_score
    computer.reset_score
  end

  def reset
    reset_scores if grand_winner?
    board.reset
    @current_marker = human.marker
    clear_screen
  end
end

TTTGame.new
