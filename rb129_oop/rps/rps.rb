module Utilities
  def prompt(message)
    puts "=> #{message}"
  end

  def clear_screen
    system('clear') || system('cls')
  end

  def line_format
    "%-3s %-6s %-#{human.name.size}s %-10s %-8s %-10s %s"
  end

  def display_rules
    prompt <<~RULES
      Here are the rules:

      => Choose a move: Rock, Paper, Scissors, Lizard, or Spock.
      => Your opponent will also choose a move.

      => The winner is determined by the following:

      => Scissors cuts Paper
      => Scissors decapitates Lizard
      => Paper covers Rock
      => Paper disproves Spock
      => Rock crushes Scissors
      => Rock crushes Lizard
      => Lizard poisons Spock
      => Lizard eats Paper
      => Spock smashes Scissors
      => Spock vaporizes Rock
    RULES
  end

  def choose_computer_player(computer = nil)
    players = [R2D2.new, Hal.new, Chappie.new, Sonny.new, Number5.new]
    players.delete_if { |player| player.name == computer.name } if computer
    players.sample
  end
end

class Move
  attr_reader :name, :variations

  def >(other_move)
    winning_moves.key?(other_move.name)
  end

  def win_message(other_move)
    "#{self} #{winning_moves[other_move.name]} #{other_move}."
  end

  def to_s
    name.capitalize
  end

  private

  attr_reader :winning_moves
end

class Rock < Move
  def initialize
    @name = 'rock'
    @variations = [name, 'r']
    @winning_moves = { 'scissors' => 'crushes',
                       'lizard' => 'crushes' }
  end
end

class Paper < Move
  def initialize
    @name = 'paper'
    @variations = [name, 'p']
    @winning_moves = { 'rock' => "covers",
                       'spock' => "disproves" }
  end
end

class Scissors < Move
  def initialize
    @name = 'scissors'
    @variations = [name, 's', 'sc']
    @winning_moves = { 'paper' => "cuts",
                       'lizard' => "decapitates" }
  end
end

class Lizard < Move
  def initialize
    @name = 'lizard'
    @variations = [name, 'l']
    @winning_moves = { 'paper' => "eats",
                       'spock' => "poisons" }
  end
end

class Spock < Move
  def initialize
    @name = 'spock'
    @variations = [name, 'sp']
    @winning_moves = { 'scissors' => "smashes",
                       'rock' => "vaporizes" }
  end
end

class Player
  attr_reader :score, :move, :name

  def initialize
    @score = 0
    set_name
  end

  def increment_score
    @score += 1
  end

  def reset_score
    @score = 0
  end

  def upcase
    name.upcase
  end

  private

  MOVES = [Rock.new, Scissors.new, Paper.new, Lizard.new, Spock.new]

  def to_s
    name
  end
end

class Human < Player
  def choose
    loop do
      prompt "Choose (r)ock, (p)aper, (s)cissors, (l)izard, (sp)ock:"
      choice = gets.chomp.downcase

      MOVES.each do |move|
        return @move = move if move.variations.include?(choice)
      end

      prompt "That's not a valid choice.\n"
    end
  end

  private

  include Utilities

  def set_name
    loop do
      prompt "What's your name? (no spaces, only letters, 3-15 characters)"
      input = gets.chomp.capitalize
      break @name = input if valid_name?(input)
      prompt "Sorry, that name is not valid."
    end
  end

  def valid_name?(input)
    regex = /^[a-z]{3,15}$/i
    input.match?(regex)
  end
end

class R2D2 < Player
  def choose
    @move = [MOVES[0], MOVES[-1]].sample
  end

  private

  def set_name
    @name = 'R2D2'
  end
end

class Hal < Player
  def choose
    change = rand(2)
    choice = (MOVES[0, 2] + MOVES[3..-1]).sample

    @move = change == 0 ? MOVES[1] : choice
  end

  private

  def set_name
    @name = 'Hal'
  end
end

class Chappie < Player
  def choose
    change = rand(2)
    choice = MOVES[1, 3].sample

    @move = change == 0 ? MOVES[2] : choice
  end

  private

  def set_name
    @name = 'Chappie'
  end
end

class Sonny < Player
  def choose
    change = rand(2)
    choice = (MOVES[0, 1] + MOVES[2..-1]).sample

    @move = change == 0 ? MOVES[3] : choice
  end

  private

  def set_name
    @name = 'Sonny'
  end
end

class Number5 < Player
  def choose
    @move = MOVES[3..-1].sample
  end

  private

  def set_name
    @name = 'Number 5'
  end
end

class Option
  include Utilities
  attr_reader :names, :exit_loop, :play_again

  def initialize(rps_game)
    @human = rps_game.human
    @rps_game = rps_game
    @exit_loop = false
    @play_again = true
  end

  def action; end

  private

  attr_reader :human, :rps_game
end

class History < Option
  def initialize(rps_game)
    super(rps_game)
    @names = ['h', 'hist', 'history']
  end

  def action
    clear_screen
    prompt format(line_format, "RND", "SCORE", "HUM", "H. MOVE",
                  "COM", "C. MOVE", "WINNER")
    prompt format(line_format, "---", "-----", "---", "-------",
                  "---", "-------", "------")
    rps_game.history.each { |round| prompt round }
  end
end

class Continue < Option
  def initialize(rps_game)
    super(rps_game)
    @names = ['c', 'cont', 'continue']
    @exit_loop = true
  end
end

class NewOpponent < Option
  def initialize(rps_game)
    super(rps_game)
    @names = ['n', 'new']
    @exit_loop = true
  end

  # This method smells of :reek:DuplicateMethodCall
  def action
    rps_game.computer = choose_computer_player(rps_game.computer)
    human.reset_score
    rps_game.history << "Changed to new opponent (#{rps_game.computer.upcase})."
  end
end

class Rules < Option
  def initialize(rps_game)
    super(rps_game)
    @names = ['r', 'rules']
  end

  def action
    clear_screen
    display_rules
  end
end

class Quit < Option
  def initialize(rps_game)
    super(rps_game)
    @names = ['q', 'quit']
    @exit_loop = true
    @play_again = false
  end
end

# This class smells of
# :reek:RepeatedConditional
# :reek:ClassVariable
# :reek:TooManyInstanceVariables
class Round
  include Utilities
  attr_reader :human, :computer, :round_number, :score,
              :winner, :loser, :win_message, :human_move,
              :computer_move

  @@round_number = 0

  def initialize(human, computer)
    @@round_number += 1
    @round_number = @@round_number
    @human = human
    @computer = computer
    @score = "(#{human.score}-#{computer.score})"
    @human_move = human.move.to_s
    @computer_move = computer.move.to_s
    @winner = ''
    round_results
  end

  def display_moves
    clear_screen
    prompt "You chose #{human.move}."
    prompt "#{computer.name} chose #{computer.move}."
    puts
  end

  def display_winner
    prompt win_message if winner
    prompt "You win!" if winner == human
    prompt "#{computer} wins!" if winner == computer
    prompt "It's a tie!" unless winner
  end

  private

  def to_s
    winner_name = winner ? winner.upcase : "TIE"

    format(line_format, round_number, score, human, human_move,
           computer, computer_move, winner_name)
  end

  def increment_score
    winner.increment_score
    @score = "(#{human.score}-#{computer.score})"
  end

  def human_wins?
    human.move > computer.move
  end

  def computer_wins?
    computer.move > human.move
  end

  def determine_winner
    @winner = (human if human_wins?) || (computer if computer_wins?)
  end

  def determine_loser
    @loser = winner == human ? computer : human
  end

  def round_results
    determine_winner
    return unless winner

    determine_loser
    increment_score
    @win_message = winner.move.win_message(loser.move)
  end
end

# This class smells of :reek:Attribute and :reek:TooManyMethods
class RPSGame
  include Utilities
  attr_accessor :computer
  attr_reader :human, :history

  GAMES_TO_WIN = 3

  def initialize
    @computer = choose_computer_player
    @history = []
  end

  def play
    startup_tasks
    game_loop
    display_goodbye_message
  end

  private

  def startup_tasks
    display_title_screen
    clear_screen
    @human = Human.new
  end

  def display_title_screen
    clear_screen
    prompt "Welcome to Rock, Paper, Scissors!"
    prompt "Try to be the first to win #{GAMES_TO_WIN} rounds."
    puts
    display_rules
    puts
    prompt "Press enter when you are ready to continue."
    gets.chomp
  end

  def game_loop
    loop do
      display_header
      human.choose
      computer.choose
      round_tasks
      grand_winner_tasks if grand_winner?
      option = option_tasks
      break unless option.play_again
    end
  end

  def display_header
    clear_screen
    prompt "Hello #{human}!"
    prompt "You are playing against: #{computer.upcase}"
    display_score
    puts
  end

  def display_goodbye_message
    puts
    prompt "Thank you for playing Rock, Paper, Scissors. Goodbye!"
  end

  def display_score
    prompt "#{human}: #{human.score} | #{computer}: #{computer.score}"
  end

  def round_tasks
    round = Round.new(human, computer)
    round.display_moves
    round.display_winner
    display_score
    @history << round
  end

  def grand_winner?
    (human.score == GAMES_TO_WIN) ||
      (computer.score == GAMES_TO_WIN)
  end

  # This method smells of :reek:DuplicateMethodCall
  def display_grand_winner(winner)
    sentence = "#{winner.upcase} is the GRAND WINNER!"

    puts
    prompt "=" * sentence.size
    prompt sentence
    prompt "=" * sentence.size
  end

  # This method smells of :reek:DuplicateMethodCall
  def grand_winner_tasks
    grand_winner = human.score == GAMES_TO_WIN ? human : computer
    display_grand_winner(grand_winner)
    @history << "#{grand_winner.upcase} was the GRAND WINNER."
    human.reset_score
    computer.reset_score
  end

  def options_prompt
    puts
    prompt "What would you like to do?"
    prompt "Options: (c)ontinue | (n)ew opponent | (h)istory | (r)ules | (q)uit"
  end

  def options
    [History.new(self), Continue.new(self),
     NewOpponent.new(self), Rules.new(self),
     Quit.new(self)]
  end

  def invalid_option_prompt
    prompt "That's not a valid option."
    prompt "Please choose 'c', 'n', 'h', 'r', or 'q'.\n"
  end

  def retrieve_option
    loop do
      options_prompt
      answer = gets.chomp.downcase

      chosen_option = options.find { |option| option.names.include?(answer) }

      next invalid_option_prompt unless chosen_option
      return chosen_option
    end
  end

  def option_tasks
    loop do
      option = retrieve_option
      option.action
      return option if option.exit_loop
    end
  end
end

RPSGame.new.play
