Print the Alphabet
Ruby Basics > Strings
https://launchschool.com/exercises/97f49731

#+begin_src ruby :results output :tangle print_the_alphabet.rb
alphabet = 'abcdefghijklmnopqrstuvwxyz'
puts alphabet.split('')
#+end_src

#+RESULTS:
#+begin_example
a
b
c
d
e
f
g
h
i
j
k
l
m
n
o
p
q
r
s
t
u
v
w
x
y
z
#+end_example

* Using *chars*
  #+begin_src ruby :results output :tangle print_the_alphabet.rb
  alphabet = 'abcdefghijklmnopqrstuvwxyz'
  puts alphabet.chars
  #+end_src

  #+RESULTS:
  #+begin_example
  a
  b
  c
  d
  e
  f
  g
  h
  i
  j
  k
  l
  m
  n
  o
  p
  q
  r
  s
  t
  u
  v
  w
  x
  y
  z
  #+end_example

