Ignoring Case
Ruby Basics > Strings
https://launchschool.com/exercises/4d81dbe7

#+begin_src ruby :results output :tangle ignoring_case.rb
name = 'Roger'

puts name.casecmp?('RoGeR')
puts name.casecmp?('DAVE')
#+end_src

#+RESULTS:
: It's True
: false

* Using .casecmp

  #+begin_src ruby :results output :tangle ignoring_case.rb
  name = 'Roger'

  puts name.casecmp('RoGeR') == 0
  puts name.casecmp('DAVE') == 0
  #+end_src

  #+RESULTS:
  : true
  : false
