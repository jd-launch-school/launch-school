Even Numbers
Ruby Basics > Debugging
https://launchschool.com/exercises/fe990481

We want to iterate through the ~numbers~ array and return a new array containing only the even numbers. However, our code isn't producing the expected output. Why not? How can we change it to produce the expected result?

#+begin_src ruby :results output
numbers = [5, 2, 9, 6, 3, 1, 8]

even_numbers = numbers.map do |n|
  n if n.even?
end

p even_numbers # expected output: [2, 6, 8]
#+end_src

#+RESULTS:
: [nil, 2, nil, 6, nil, nil, 8]

~Array#map~ runs ~n if n.even?~ on each of the elements of the ~numbers~ array. If the number is even, then it will add that number to the array. Since there is no code telling what to do if the number is odd, ~nil~ is returned and added to the array in place of the odd number. One way to fix this is to use ~Array#select~ instead:

#+begin_src ruby :results output
numbers = [5, 2, 9, 6, 3, 1, 8]

even_numbers = numbers.select do |n|
  n.even?
end

p even_numbers # expected output: [2, 6, 8]
#+end_src

#+RESULTS:
: [2, 6, 8]
