# That's Odd
# Ruby Basics > Loops 1
# https://launchschool.com/exercises/cbd48261

for i in 1..100
  puts i if i.odd?
end

# Or ...

# for i in 1..100
#   puts i if i % 2 != 0
# end

# Or ...

# for i in 1..100
#   puts i unless i.even?
# end
