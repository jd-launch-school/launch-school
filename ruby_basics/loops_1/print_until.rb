# Print Until
# Ruby Basics > Loops 1
# https://launchschool.com/exercises/edca07c2

numbers = [7, 9, 13, 25, 18]
count = 0

until count == numbers.size
  puts numbers[count]
  count += 1
end

# Or ...

# until numbers.size == 0 do
#   puts numbers[0]
#   numbers.shift
# end

# Or ...

# until numbers.length == 0
#   puts numbers.shift
# end

# Or ...

# until numbers.empty?
#   puts numbers.shift
# end
