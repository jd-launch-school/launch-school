# Count Up
# Ruby Basics > Loops 1
# https://launchschool.com/exercises/0162cb17

count = 1

until count > 10
  puts count
  count += 1
end
