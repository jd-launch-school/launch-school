# Greet Your Friends
# Ruby Basics > Loops 1
# https://launchschool.com/exercises/2c75e278

friends = ['Sarah', 'John', 'Hannah', 'Dave']

for friend in friends
  puts "Hello, #{friend}!"
end
