# Print While
# Ruby Basics > Loops 1
# https://launchschool.com/exercises/6f2f69b0

numbers = []

while numbers.size < 5
  numbers << rand(100)
end

puts numbers
