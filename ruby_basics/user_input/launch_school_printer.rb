number_of_lines = 0

loop do
  puts ">> How many output lines do you want? Enter a number >= 3:"
  number_of_lines = gets.to_i
  break if number_of_lines >= 3
  puts ">> That's not enough lines."
end

number_of_lines.times do |index|
  puts "#{index + 1}: Launch School is the best!"
end
