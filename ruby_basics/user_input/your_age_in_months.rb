puts ">> What is your age (years)?"

age_in_years = $stdin.gets.chomp.to_i
age_in_months = age_in_years * 12

puts "You are #{age_in_months} months old."
