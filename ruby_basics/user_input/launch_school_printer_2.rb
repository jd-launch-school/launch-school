def valid_number?(number_string)
  number_string.to_i.to_s == number_string
end

number_of_lines = 0

loop do
  puts ">> How many output lines do you want? " \
       "Enter a number >= 3 (Q to quit):"

  answer = gets.chomp.downcase
  break if answer == "q"

  if valid_number?(answer)
    number_of_lines = answer.to_i
  else
    puts ">> Invalid Input. Only integers allowed."
    next
  end

  if number_of_lines >= 3
    number_of_lines.times do |index|
      puts "#{index + 1}: Launch School is the best!"
    end
  else
    puts ">> That's not enough lines."
  end
end
