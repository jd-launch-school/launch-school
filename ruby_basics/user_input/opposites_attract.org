Opposites Attract
Ruby Basics > User Input
https://launchschool.com/exercises/84929d91

* Recommended Exercise Solution
  #+begin_src ruby :results output :tangle opposites_attract.rb
  def valid_number?(number_string)
    number_string.to_i.to_s == number_string && number_string.to_i != 0
  end

  def read_number
    loop do
      puts ">> Please enter a positive or negative integer:"
      number = gets.chomp
      return number.to_i if valid_number?(number)
      puts ">> Invalid input. Only non-zero integers allowed."
    end
  end

  first_number = nil
  second_number = nil

  loop do
    first_number = read_number
    second_number = read_number
    break if first_number * second_number < 0
    puts ">> Sorry. One integer must be positive, one must be negative."
    puts ">> Please start over."
  end
  
  sum = first_number + second_number
  puts "#{first_number} + #{second_number} = #{sum}"
  #+end_src

* My initial solution
#+begin_src ruby :results output :tangle opposites_attract.rb
  def valid_number?(number_string)
    number_string.to_i.to_s == number_string && number_string.to_i != 0
  end

  def prompt
    answer = nil
    loop do
      puts ">> Please enter a positive or negative integer:"
      answer = gets.chomp
      break if valid_number?(answer)
      puts ">> Invalid input. Only non-zero integers allowed."
    end
    answer.to_i
  end

  loop do
    first_integer = prompt
    second_integer = prompt
  
    if first_integer.negative? && second_integer.positive? ||
       first_integer.positive? && second_integer.negative?
       result = first_integer + second_integer
       puts "#{first_integer} + #{second_integer} = #{result}"
       break
    end

    puts ">> Sorry. One integer must be positive, one must be negative."
    puts ">> Please start over."
  end
#+end_src
