Your Age in Months
Ruby Basics > User Input
https://launchschool.com/exercises/c21beca3

#+begin_src ruby :results output :tangle your_age_in_months.rb
  puts ">> What is your age (years)?"

  age_in_years = $stdin.gets.chomp.to_i
  age_in_months = age_in_years * 12

  puts "You are #{age_in_months} months old."
#+end_src

* Using a function
  #+begin_src ruby :results output :tangle your_age_in_months.rb
    def years_to_months(years)
      age_in_months = years * 12
      return [age_in_months, "You are #{age_in_months} months old."]
    end

    puts ">> What is your age (years)?"

    age_in_years = $stdin.gets.chomp.to_i
    age_in_months, output = years_to_months(age_in_years)

    puts output
  #+end_src
