choice = nil
loop do
  puts '>> Do you want me to print something? (y/n)'
  choice = $stdin.gets.chomp.downcase
  break if %w(y n).include?(choice)
  puts '>> Invalid Input! Please enter y or n'
end
puts 'something' if choice == 'y'
