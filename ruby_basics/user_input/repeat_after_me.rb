loop do
  puts ">> Type anything you want: (q)uit"
  typed_input = $stdin.gets.chomp

  if typed_input.downcase == 'q'
    puts "Goodbye!"
    break
  end

  puts typed_input
end
