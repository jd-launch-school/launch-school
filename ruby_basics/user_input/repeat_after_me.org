Repeat after me
Ruby Basics > User Input
https://launchschool.com/exercises/eb6c54db

#+begin_src ruby :results output :tangle repeat_after_me.rb
  puts ">> Type anything you want:"
  typed_input = $stdin.gets.chomp
  puts typed_input
#+end_src


* Loop solution
  #+begin_src ruby :results output :tangle repeat_after_me.rb
    loop do
      puts ">> Type anything you want: (q)uit"
      typed_input = $stdin.gets.chomp

      if typed_input.downcase == 'q'
        puts "Goodbye!"
        break
      end

      puts typed_input
    end
  #+end_src
