# Insert Numbers
# Ruby Basics > Loops 2
# https://launchschool.com/exercises/26b01f9a

numbers = []
count = 0

loop do
  count += 1
  puts "Enter any number (#{count} of 5):"
  numbers.push(gets.chomp.to_i)
  break if numbers.size == 5
end

puts numbers
