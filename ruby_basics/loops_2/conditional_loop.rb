# Conditional Loop
# Ruby Basics > Loops 2
# https://launchschool.com/exercises/db8d3706

process_the_loop = [true, false].sample

if process_the_loop
  loop do
    puts "The loop was processed!"
    break
  end
else
  puts "The loop wasn't processed!"
end

# Or ...

# if process_the_loop
#   1.times { puts "The loop was processed!" }
# else
#   puts "The loop was not procesed!"
# end
