# Even or Odd?
# Ruby Basics > Loops 2
# https://launchschool.com/exercises/3a3354f4

count = 1

loop do
  puts "#{count} is #{count.odd? ? 'odd' : 'even'}!"
  break if count == 5
  count += 1
end
