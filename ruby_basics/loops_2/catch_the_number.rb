# Catch the Number
# Ruby Basics > Loops 2
# https://launchschool.com/exercises/f1616791

loop do
  number = rand(100)
  puts number
  break if number.between?(0, 10)
end
