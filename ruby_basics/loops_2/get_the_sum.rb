# Get the Sum
# Ruby Basics > Loops 2
# https://launchschool.com/exercises/b4683f61

loop do
  puts 'What does 2 + 2 equal?'
  answer = gets.chomp.to_i

  if answer == 4
    puts "That's correct!"
    break
  end

  puts "Wrong answer, Try again!"
end
