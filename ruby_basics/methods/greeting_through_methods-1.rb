def get_hello
  "Hello"
end

def get_world
  "World"
end

puts "#{get_hello} #{get_world}"
