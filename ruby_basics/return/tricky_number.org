Tricky Number
Ruby Basics > Return
https://launchschool.com/exercises/79e15f12

#+begin_src ruby :results output :tangle tricky_number.rb
def tricky_number
  if true
    number = 1
  else
    2
  end
end

puts tricky_number
#+end_src

#+RESULTS:
: 1

* My Answer
  1

** Why?
   *puts* prints the return value of the *tricky_number* method to the screen. The *tricky_number* method has a conditional *if* statement. That statement checks to see if *true* is truthy. Since *true* is always truthy, it goes into the else clause and assigns the variable *number* to the integer *1*. An *if* statement retuns the last value executed. Assigning a value to a variable returns the value that was assigned, in this case *1*.
