Counting Sheep (Part 1)
Ruby Basics > Return
https://launchschool.com/exercises/7c5438bd

#+begin_src ruby :results output :tangle counting_sheep_1.rb
  def count_sheep
    5.times do |sheep|
      puts sheep
    end
  end

  puts count_sheep
#+end_src

#+RESULTS:
: 0
: 1
: 2
: 3
: 4
: 5

* My Answer
  0
  1
  2
  3
  4
  5

** Why?
   *puts* prints the return value of the *count_sheep* method to the screen. The *count_sheep* method calls the *Integer#times* method on the integer *5* setting *sheep* as a variable that stores the index of the loop. For each iteration, it prints the value of *sheep* to the screen, printing five times (0 through 4). The *Integer#times* method returns the initial integer given (*5*), which is printed to the screen by the *puts* method because it is the last (only) line of the *count_sheep* method.
