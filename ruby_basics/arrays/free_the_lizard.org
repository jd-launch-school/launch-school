Free the Lizard
Ruby Basics > Arrays
https://launchschool.com/exercises/2ad73dde

#+begin_src ruby :results output :tangle free_the_lizard.rb
pets = ['cat', 'dog', 'fish', 'lizard']
my_pets =  pets[2..3]
my_pets.pop
puts "I have a pet #{my_pets[0]}."
#+end_src

#+RESULTS:
: I have a pet fish.
