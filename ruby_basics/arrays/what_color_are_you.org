What Color Are You?
Ruby Basics > Arrays
https://launchschool.com/exercises/507dbb46

#+begin_src ruby :results output :tangle what_color_are_you.rb
colors = ['red', 'yellow', 'purple', 'green']
colors.each do |color|
  puts "I'm the color #{color}!"
end
#+end_src

#+RESULTS:
: I'm the color red!
: I'm the color yellow!
: I'm the color purple!
: I'm the color green!

* one-liner using *{}* notation
#+begin_src ruby :results output :tangle what_color_are_you.rb
colors = ['red', 'yellow', 'purple', 'green']
colors.each { |color| puts "I'm the color #{color}!" }
#+end_src

#+RESULTS:
: I'm the color red!
: I'm the color yellow!
: I'm the color purple!
: I'm the color green!
